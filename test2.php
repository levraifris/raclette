<?php
 
/* this program will show the protocol value
 *
 */
 
$serverProtocole = filter_input(INPUT_SERVER, 'SERVER_PROTOCOL', FILTER_SANITIZE_STRING);
 
echo 'server protocol : "'.$serverProtocole.'"';
 
answer('function success');
 
sleep(15);
 
exit('ok');



function answer($response)
{
    if (ob_get_level() && ob_get_length() > 0) {
        ob_clean();
    }
    
    if (is_callable('fastcgi_finish_request')) {
        /*
            * This works in Nginx but the next approach not
            */
        echo $response;
        session_write_close();
        fastcgi_finish_request();
        return;
    }
    
    if (is_callable('litespeed_finish_request')) {
        /*
            * This works in Nginx but the next approach not
            */
        echo $response;
        session_write_close();
        litespeed_finish_request();
        return;
    }

    ob_start();
    $serverProtocole = 'HTTP/1.0';
    
    header($serverProtocole.' 200 OK');
    echo $response;
    header("Content-Encoding: none");
    header('Content-Length: '.ob_get_length());
    header('Connection: close');
    ob_end_flush();
    ob_flush();
    flush();
}