{*
/**
 * StorePrestaModules SPM LLC.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 *
 /*
 *
 * @author    StorePrestaModules SPM
 * @category content_management
 * @package blockblog
 * @copyright Copyright StorePrestaModules SPM
 * @license   StorePrestaModules SPM
 */
*}

{extends file='page2.tpl'}






{block name="content_wrapper"}
{* 
{block name="left_column"}
    {if isset($blockblogsidebar_posblog_cat_item_alias) && $blockblogsidebar_posblog_cat_item_alias == 1}
        <div id="left-column" class="col-xs-12 col-sm-4 col-md-3">
            {hook h="displayLeftColumn"}
        </div>
    {/if}
{/block} *}




</div>
</div>
        {if $blockblogis17 == 1}
            <div class="container-fluid bggris">
            <div class="container">
            <nav data-depth="2" class="breadcrumb hidden-sm-down">
            <h1 class="page-heading blog-category-title">{$meta_title|escape:'htmlall':'UTF-8'}</h1>
                <ol itemscope="" itemtype="http://schema.org/BreadcrumbList">
                    <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
                        <a itemprop="item" href="/">
                            <span itemprop="name">Accueil</span>
                        </a>
                        <meta itemprop="position" content="1">
                    </li>
                    <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
                    <a temprop="item" href="https://www.barbecueland.fr/blog" title="blog" class="posted_in"><span itemprop="name">Tous nos conseils & recettes</span></a>
                    <meta itemprop="position" content="2">
                </li>
                    <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
                        <a itemprop="item" href="{if $blockblogurlrewrite_on == 1}{$blockblogcategory_url|escape:'htmlall':'UTF-8'}{$blockblogcategory_id|escape:'htmlall':'UTF-8'}{else}{$blockblogcategory_url|escape:'htmlall':'UTF-8'}{$blockblogcategory_id|escape:'htmlall':'UTF-8'}{/if}">
                            <span itemprop="name">{$meta_title|escape:'htmlall':'UTF-8'}</span>
                        </a>
                        <meta itemprop="position" content="2">
                    </li>
                </ol>
            </nav>
</div></div>
        {/if}




        <div class="container">
        <h1 class="page-heading blog-category-title mobile" style="margin-bottom:0;">{$meta_title|escape:'htmlall':'UTF-8'}</h1>

          <div id="columns_inner">
        <div id="content-wrapper" class="card card-block col-sm-12 " style="padding: 0;">
        {block name="page_content"}
        {capture name=path}
            <a href="{$blockblogcategories_url|escape:'htmlall':'UTF-8'}">
                <span>{l s='Categories' mod='blockblog'}</span>
            </a>
            <span class="navigation-pipe">&gt;</span>
            {$meta_title|escape:'htmlall':'UTF-8'}
        {/capture}

    {* {if $blockblogis16 == 0}
        <h2 class="blog-category-title">{$meta_title|escape:'htmlall':'UTF-8'}</h2>
    {else}
        <h1 class="page-heading blog-category-title">{$meta_title|escape:'htmlall':'UTF-8'}</h1>
    {/if} *}


    <div class="row-custom">
        {if strlen($blog_category.img)>0}
            <div class="col-sm-4-custom">
                <div class="photo-blog">
                    <img class="img-responsive" alt="{$meta_title|escape:'htmlall':'UTF-8'}"
                         src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}{$blockblogpic|escape:'htmlall':'UTF-8'}{$blog_category.img|escape:'htmlall':'UTF-8'}">
                </div>
            </div>

        {/if}

        <div class="col-sm-{if strlen($blog_category.img)>0}8{else}12{/if}-custom">
            {if strlen($blog_category.content)>0}
                <div class="category-content">{$blog_category.content nofilter}</div>
            {/if}
        </div>

    </div>
    <div class="clear"></div>

    <div class="blog-header-toolbar block-data-category">
        {if $count_all > 0}

            <div class="toolbar-top">

                {* <div class="{if $blockblogis16==1}sortTools sortTools16{else}sortTools{/if}">
                    <ul class="actions">
                        <li class="frst">
                            <strong>{l s='Posts' mod='blockblog'}  ( {$count_all|escape:'htmlall':'UTF-8'} )</strong>
                        </li>
                    </ul>

                    {if $blockblogrsson == 1}
                        <ul class="sorter">
                            <li>
				<span>


					<a href="{$blockblogrss_url|escape:'htmlall':'UTF-8'}" title="{l s='RSS Feed' mod='blockblog'}" target="_blank">
                        <img src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/blockblog/views/img/feed.png" alt="{l s='RSS Feed' mod='blockblog'}" />
                    </a>
				</span>
                            </li>

                        </ul>
                    {/if}

                </div> *}

            </div>


            <ul class="blog-posts {if $blockblogblog_layout_typeblog_cat_item_alias == 2}blockblog-grid-view{elseif $blockblogblog_layout_typeblog_cat_item_alias == 3}blockblog-grid-view-second{else}blockblog-list-view{/if}" id="blog-items">
                {assign var=is_category_page value=1}
                {include file="module:blockblog/views/templates/front/list_posts2.tpl"}


            </ul>





            <div class="toolbar-paging">
                <div class="text-align-center" id="page_nav">
                    {$paging nofilter}
                </div>
            </div>
        {else}
            <div class="block-no-items">
                {l s='There are not posts yet' mod='blockblog'}
            </div>
        {/if}

    </div>

        </div>
    {/block}







{/block}


