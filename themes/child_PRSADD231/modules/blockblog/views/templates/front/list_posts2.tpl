{*
/**
 * StorePrestaModules SPM LLC.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 *
 /*
 *
 * @author    StorePrestaModules SPM
 * @category content_management
 * @package blockblog
 * @copyright Copyright StorePrestaModules SPM
 * @license   StorePrestaModules SPM
 */
  *}
  <div itemscope itemtype="http://schema.org/ItemList">

{foreach from=$posts item=post name=myLoop}

    {if $smarty.foreach.myLoop.index%4 == 1}
        {include file="module:blockblog/views/templates/front/card_posts2.tpl" post=$post}
    {elseif $smarty.foreach.myLoop.index%4 == 2}
        {include file="module:blockblog/views/templates/front/card_posts3.tpl" post=$post}
    {elseif $smarty.foreach.myLoop.index%4 == 0 || $smarty.foreach.myLoop.index%4 == 3}
        {include file="module:blockblog/views/templates/front/card_posts1.tpl" post=$post}
    {/if}
{/foreach}

</div>







   




{if $blockblogblog_post_effect != "disable_all_effects"}
{literal}
    <script type="text/javascript">
        document.addEventListener("DOMContentLoaded", function(event) {
            $(document).ready(function(){
                blockblog_init_effects();
            });
        });
    </script>
{/literal}
{/if}