{*
/**
 * StorePrestaModules SPM LLC.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 *
 /*
 *
 * @author    StorePrestaModules SPM
 * @category content_management
 * @package blockblog
 * @copyright Copyright StorePrestaModules SPM
 * @license   StorePrestaModules SPM
 */
*}

{extends file='page2.tpl'}






{block name="content_wrapper"}

{* {block name="left_column"}
    {if isset($blockblogsidebar_posblog_tag_alias) && $blockblogsidebar_posblog_tag_alias == 1}
        <div id="left-column" class="col-xs-12 col-sm-4 col-md-3">
            {hook h="displayLeftColumn"}
        </div>
    {/if}
{/block} *}


<div id="content-wrapper" class="card card-block {if isset($blockblogsidebar_posblog_tag_alias) && $blockblogsidebar_posblog_tag_alias == 1}left-column col-xs-12 col-sm-8 col-md-9{elseif isset($blockblogsidebar_posblog_tag_alias) && $blockblogsidebar_posblog_tag_alias == 2}right-column col-xs-12 col-sm-8 col-md-9{/if}">
    {block name="page_content"}


    


        {capture name=path}
            <a href="{$blockblogtags_url|escape:'htmlall':'UTF-8'}">
                <span>{l s='Blog Tags' mod='blockblog'}</span>
            </a>
            <span class="navigation-pipe">&gt;</span>
            {$meta_title|escape:'htmlall':'UTF-8'}
        {/capture}

    {if $blockblogis16 == 0}
        <h2 class="blog-category-title">{$meta_title|escape:'htmlall':'UTF-8'}</h2>
    {else}
        <h1 class="page-heading blog-category-title">{$meta_title|escape:'htmlall':'UTF-8'}</h1>
    {/if}


    <div class="blog-header-toolbar block-data-category">
        {if $count_all > 0}

           


            <ul class="blog-posts {if $blockblogblog_layout_typeblog_tag_alias == 2}blockblog-grid-view{elseif $blockblogblog_layout_typeblog_tag_alias == 3}blockblog-grid-view-second{else}blockblog-list-view{/if}" id="blog-items">
                {assign var=is_category_page value=1}
                {include file="module:blockblog/views/templates/front/list_posts2.tpl"}



            </ul>





            <div class="toolbar-paging">
                <div class="text-align-center" id="page_nav">
                    {$paging nofilter}
                </div>
            </div>
        {else}
            <div class="block-no-items">
                {l s='There are not posts yet' mod='blockblog'}
            </div>
        {/if}

    </div>

        </div>
    {/block}







{/block}

