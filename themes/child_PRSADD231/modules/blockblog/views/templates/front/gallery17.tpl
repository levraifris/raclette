{*
/**
 * StorePrestaModules SPM LLC.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 *
 /*
 *
 * @author    StorePrestaModules SPM
 * @category content_management
 * @package blockblog
 * @copyright Copyright StorePrestaModules SPM
 * @license   StorePrestaModules SPM
 */
*}

{extends file='page.tpl'}



    {if $blockblogis17 == 1}
        <nav data-depth="2" class="breadcrumb hidden-sm-down">
            <ol itemscope="" itemtype="http://schema.org/BreadcrumbList">
                <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
                    <a itemprop="item" href="{$blockbloggallery_url|escape:'htmlall':'UTF-8'}">
                        <span itemprop="name">{l s='Blog gallery' mod='blockblog'}</span>
                    </a>
                    <meta itemprop="position" content="1">
                </li>

            </ol>
        </nav>

    {/if}

    {capture name=path}
        {$meta_title|escape:'htmlall':'UTF-8'}
    {/capture}


{block name="content_wrapper"}

{* {block name="left_column"}
    {if isset($blockblogsidebar_posblog_gallery_alias) && $blockblogsidebar_posblog_gallery_alias == 1}
        <div id="left-column" class="col-xs-12 col-sm-4 col-md-3">
            {hook h="displayLeftColumn"}
        </div>
    {/if}
{/block} *}


<div id="content-wrapper" class="card card-block {if isset($blockblogsidebar_posblog_gallery_alias) && $blockblogsidebar_posblog_gallery_alias == 1}left-column col-xs-12 col-sm-8 col-md-9{elseif isset($blockblogsidebar_posblog_gallery_alias) && $blockblogsidebar_posblog_gallery_alias == 2}right-column col-xs-12 col-sm-8 col-md-9{/if}">
    {block name="page_content"}


    {if $blockblogis16 == 0}
        <h2>{$meta_title|escape:'htmlall':'UTF-8'}</h2>
    {else}
        <h1 class="page-heading">{$meta_title|escape:'htmlall':'UTF-8'}</h1>
    {/if}

    <div class="blog-header-toolbar">
        {if $count_all > 0}

            <div class="toolbar-top">

                <div class="{if $blockblogis16==1}sortTools sortTools16{else}sortTools{/if}" >
                    <ul class="actions">
                        <li class="frst">
                            <strong>{l s='Gallery' mod='blockblog'}  ( {$count_all|escape:'htmlall':'UTF-8'} )</strong>
                        </li>
                    </ul>
                </div>

            </div>


            <ul class="blog-posts gallery_list" id="blog-items">

                {include file="module:blockblog/views/templates/front/list_gallery.tpl"}



            </ul>


            <div class="toolbar-paging">
                <div class="text-align-center" id="page_nav">
                    {$paging nofilter}
                </div>
            </div>
        {else}
            <div class="block-no-items">
                {l s='There are not items yet' mod='blockblog'}
            </div>
        {/if}

    </div>

        </div>
    {/block}



    {block name="right_column"}
        {if isset($blockblogsidebar_posblog_gallery_alias) && $blockblogsidebar_posblog_gallery_alias == 2}
            <div id="right-column" class="col-xs-12 col-sm-4 col-md-3">
                {hook h="displayRightColumn"}
            </div>
        {/if}

    {/block}




{/block}
