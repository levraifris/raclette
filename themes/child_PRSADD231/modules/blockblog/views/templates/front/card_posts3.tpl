<article class="card3 col-sm-12" itemprop="ItemListElement" itemscope itemtype="http://schema.org/Thing">
    <div class="col-sm-8 imgcard">
    <a title="{$post.title|escape:'htmlall':'UTF-8'}"
    href="{if $blockblogurlrewrite_on == 1}
     {$blockblogpost_url|escape:'htmlall':'UTF-8'}{$post.seo_url|escape:'htmlall':'UTF-8'}
    {else}
     {$blockblogpost_url|escape:'htmlall':'UTF-8'}{$post.id|escape:'htmlall':'UTF-8'}
    {/if}"> <img class="img-responsive" alt="{$post.title|escape:'htmlall':'UTF-8'}"
                             src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}{$blockblogpic|escape:'htmlall':'UTF-8'}{$post.img|escape:'htmlall':'UTF-8'|replace:'-300x300':''}">
    </a> </div>
    <div class="offset-sm-7 col-sm-5 contour">
        <div class="offset-md-1 col-sm-10">
<header><a title="{$post.title|escape:'htmlall':'UTF-8'}"
href="{if $blockblogurlrewrite_on == 1}
 {$blockblogpost_url|escape:'htmlall':'UTF-8'}{$post.seo_url|escape:'htmlall':'UTF-8'}
{else}
 {$blockblogpost_url|escape:'htmlall':'UTF-8'}{$post.id|escape:'htmlall':'UTF-8'}
{/if}"><h2 itemprop="name">{$post.title|escape:'htmlall':'UTF-8'}</h2></a></header>
<p itemprop="description">{$post.seo_keywords}</p>
<a title="{$post.title|escape:'htmlall':'UTF-8'}" class="btn-orange" style="font-size: 15px;padding: 0px 10px;"
href="{if $blockblogurlrewrite_on == 1}
 {$blockblogpost_url|escape:'htmlall':'UTF-8'}{$post.seo_url|escape:'htmlall':'UTF-8'}
{else}
 {$blockblogpost_url|escape:'htmlall':'UTF-8'}{$post.id|escape:'htmlall':'UTF-8'}
{/if}">Lire l'article</a>
        </div>
    </div>
</article>