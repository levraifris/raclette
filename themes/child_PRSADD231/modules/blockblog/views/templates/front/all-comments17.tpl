{*
/**
 * StorePrestaModules SPM LLC.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 *
 /*
 *
 * @author    StorePrestaModules SPM
 * @category content_management
 * @package blockblog
 * @copyright Copyright StorePrestaModules SPM
 * @license   StorePrestaModules SPM
 */
*}

{extends file='page.tpl'}









{block name="content_wrapper"}

{* {block name="left_column"}
    {if isset($blockblogsidebar_posblog_allcom_alias) && $blockblogsidebar_posblog_allcom_alias == 1}
        <div id="left-column" class="col-xs-12 col-sm-4 col-md-3">
            {hook h="displayLeftColumn"}
        </div>
    {/if}
{/block} *}


<div id="content-wrapper" class="card card-block {if isset($blockblogsidebar_posblog_allcom_alias) && $blockblogsidebar_posblog_allcom_alias == 1}left-column col-xs-12 col-sm-8 col-md-9{elseif isset($blockblogsidebar_posblog_allcom_alias) && $blockblogsidebar_posblog_allcom_alias == 2}right-column col-xs-12 col-sm-8 col-md-9{/if}">
    {block name="page_content"}


        {if $blockblogis17 == 1}
            <nav data-depth="2" class="breadcrumb hidden-sm-down">
                <ol itemscope="" itemtype="http://schema.org/BreadcrumbList">
                    <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
                        <a itemprop="item" href="{$blockblogcomments_url|escape:'htmlall':'UTF-8'}">
                            <span itemprop="name">{l s='All Comments' mod='blockblog'}</span>
                        </a>
                        <meta itemprop="position" content="1">
                    </li>

                </ol>
            </nav>

        {/if}


        {capture name=path}
            {$meta_title|escape:'htmlall':'UTF-8'}
        {/capture}


    {if $blockblogis16 == 0}

        <h2 class="background-none">{$meta_title|escape:'htmlall':'UTF-8'}</h2>
    {else}
        <h1 class="page-heading">{$meta_title|escape:'htmlall':'UTF-8'}</h1>
    {/if}



    <div class="blog-header-toolbar">
        {if $count_all > 0}

            <div class="toolbar-top">

                <div class="{if $blockblogis16==1}sortTools sortTools16{else}sortTools{/if}">
                    <ul class="actions">
                        <li class="frst">
                            <strong>{l s='Comments' mod='blockblog'}  ( {$count_all|escape:'htmlall':'UTF-8'} )</strong>
                        </li>
                    </ul>

                </div>

            </div>



            <ul class="blog-posts {if $blockblogblog_layout_typeblog_allcom_alias == 2}blockblog-grid-view{else}blockblog-list-view{/if}" id="blog-items">

                {include file="module:blockblog/views/templates/front/list_allcomments.tpl"}

            </ul>




            <div class="toolbar-paging">
                <div class="text-align-center" id="page_nav">
                    {$paging nofilter}
                </div>
            </div>

        {else}
            <div class="block-no-items">
                {l s='There are not comments yet' mod='blockblog'}
            </div>
        {/if}

    </div>
    {/block}

</div>

    {block name="right_column"}
        {if isset($blockblogsidebar_posblog_allcom_alias) && $blockblogsidebar_posblog_allcom_alias == 2}
            <div id="right-column" class="col-xs-12 col-sm-4 col-md-3">
                {hook h="displayRightColumn"}
            </div>
        {/if}

    {/block}




{/block}

