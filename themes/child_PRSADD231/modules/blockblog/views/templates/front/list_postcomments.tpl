{*
/**
 * StorePrestaModules SPM LLC.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 *
 /*
 *
 * @author    StorePrestaModules SPM
 * @category content_management
 * @package blockblog
 * @copyright Copyright StorePrestaModules SPM
 * @license   StorePrestaModules SPM
 */
*}
{assign var=blockblog_is_snippets_comments value=0}

{foreach from=$comments item=comment name=myLoop}

    {if $category_item.id==1 }  
    <div class="pl-animate {$blockblogblog_comp_effect|escape:'htmlall':'UTF-8'}" {if $blockblog_is_snippets_comments == 1}itemprop="review" itemscope itemtype="http://schema.org/Review"{/if}>
    {if $blockblog_is_snippets_comments == 1} <meta itemprop="itemReviewed" content="{$comment.post_title|escape:'htmlall':'UTF-8'}"/>{/if}
<div class="row-custom" style="display: flex; align-items: center;">
    <div class="col-sm-3 col-xs-6">
                {if $blockblograting_post == 1}
                    <span class="affnot">{$comment.rating}/5</span>
                            <div class="rating-input" {if $blockblog_is_snippets_comments == 1} itemtype="http://schema.org/Rating" itemscope itemprop="reviewRating"{/if}>
                                {if $blockblog_is_snippets_comments == 1}
                                <meta itemprop="ratingValue" content="{$comment.rating|escape:'htmlall':'UTF-8'}"/>
                                <meta itemprop="bestRating" content="5"/>
                                {/if}
                                {if $comment.rating != 0}
                                    {for $foo=0 to 4}
                                        {if $foo < $comment.rating}
                                            <i class="fa fa-star" data-value="{$foo|escape:'htmlall':'UTF-8'}"></i>

                                        {else}
                                            <i class="fa fa-star-o" data-value="{$foo|escape:'htmlall':'UTF-8'}"></i>
                                        {/if}

                                    {/for}

                                {else}

                                    {for $foo=0 to 4}
                                        <i class="fa fa-star-o" data-value="{$foo|escape:'htmlall':'UTF-8'}"></i>
                                    {/for}
                                {/if}
                            </div>
                {/if}
    <p>Publié le <time datetime="{$comment.time_add|escape:'htmlall':'UTF-8'}" class="comment-date">{$comment.time_add|date_format:"%d/%m/%Y"}</time></p>
    <p>Par {$comment.name|escape:'htmlall':'UTF-8'}</p>
    </div>
                    <div class="col-sm-9 col-xs-6">
                    <div class="comment-post" {if $blockblog_is_snippets_comments == 1}itemprop="description"{/if}>{$comment.comment|escape:'htmlall':'UTF-8'}</div>
                    </div>
</div>
    </div>

 
        <div class="clear"></div>
   


        {else}

        <div class="pl-animate {$blockblogblog_comp_effect|escape:'htmlall':'UTF-8'}" {if $blockblog_is_snippets_comments == 1}itemprop="review" itemscope itemtype="http://schema.org/Review"{/if}>
        {if $blockblog_is_snippets_comments == 1} <meta itemprop="itemReviewed" content="{$comment.post_title|escape:'htmlall':'UTF-8'}"/>{/if}
    <div class="row-custom" style="display: flex; align-items: center;">
        <div class="col-sm-12">
        <p><span style="margin-right: 15px;font-weight:600;">{$comment.name|escape:'htmlall':'UTF-8'} </span>Publié le <time datetime="{$comment.time_add|escape:'htmlall':'UTF-8'}" class="comment-date">{$comment.time_add|date_format:"%d/%m/%Y"}</time></p>
                        <div class="comment-post" {if $blockblog_is_snippets_comments == 1}itemprop="description"{/if}>{$comment.comment|escape:'htmlall':'UTF-8'}</div>
    </div>
        </div>
        </div>

     
            <div class="clear"></div>
{/if}


{/foreach}


