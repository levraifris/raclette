{*
/**
 * StorePrestaModules SPM LLC.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 *
 /*
 * 
 * @author    StorePrestaModules SPM
 * @category content_management
 * @package blockblog
 * @copyright Copyright StorePrestaModules SPM
 * @license   StorePrestaModules SPM
 */
*}
{if $product.dwf_idblog}
    {assign var='idart' value=','|explode:$product.dwf_idblog}

    <div id="blockblogposts_block_left_spm"
        class="block {if $blockblogis17 == 1}block-categories{/if} {if $blockblogis16 == 1}blockmanufacturer16{else}blockmanufacturer{/if} blockblog-block">
        <h2 class="title_block {if $blockblogis17 == 1}text-uppercase{/if}">

            NOS CONSEILS & NOS RECETTES

        </h2>
        <div class="block_content">

            {if count($blockblogposts) > 0}
                <div class="items-articles-block">

                    {foreach from=$blockblogposts item=items name=myLoop1}
                        {foreach from=$items.data item=blog name=myLoop}
                            {* <pre>{print_r($blog)}</pre> *}
                            {if $blog.id|in_array:$idart}

                                <div class="current-item-block">

                                    {if strlen($blog.img)>0}
                                        <div class="block-side float-left margin-right-10">
                                        <a class="item-article" title="{$blog.title|escape:'htmlall':'UTF-8'}"
                                    href="{if $blockblogurlrewrite_on == 1}{$blockblogpost_url|escape:'htmlall':'UTF-8'}{$blog.seo_url|escape:'htmlall':'UTF-8'}{else}{$blockblogpost_url|escape:'htmlall':'UTF-8'}{$blog.id|escape:'htmlall':'UTF-8'}{/if}"><img src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}{$blockblogpic|escape:'htmlall':'UTF-8'}{$blog.img|escape:'htmlall':'UTF-8'}"
                                                title="{$blog.title|escape:'htmlall':'UTF-8'}" alt="{$blog.title|escape:'htmlall':'UTF-8'}" /></a>
                                        </div>
                                    {/if}

                                    <div class="block-content">
                                        <a class="item-article" title="{$blog.title|escape:'htmlall':'UTF-8'}"
                                            href="{if $blockblogurlrewrite_on == 1}{$blockblogpost_url|escape:'htmlall':'UTF-8'}{$blog.seo_url|escape:'htmlall':'UTF-8'}{else}{$blockblogpost_url|escape:'htmlall':'UTF-8'}{$blog.id|escape:'htmlall':'UTF-8'}{/if}">{$blog.title|escape:'htmlall':'UTF-8'}</a>
                                    </div>
                                </div>
                            {/if}
                        {/foreach}
                    {/foreach}
                    <p class="block-view-all">
                        <a href="{$blockblogposts_url|escape:'htmlall':'UTF-8'}" title="{l s='View all posts' mod='blockblog'}"
                            class="{if $blockblogis17 == 1}btn btn-orange{/if} button"><b>Plus d'articles</b></a>
                    </p>

                </div>
            {else}
                <div class="block-no-items">
                    {l s='There are not Posts yet.' mod='blockblog'}
                </div>
            {/if}

        </div>
    </div>


{/if}