{**
 * 2007-2017 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2017 PrestaShop SA
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}
 {block name='product_miniature_item'}
	<div class="product-miniature js-product-miniature" data-id-product="{$product.id_product}" data-id-product-attribute="{$product.id_product_attribute}" itemscope 
	
	itemtype="http://schema.org/Product">
	
	  <div class="thumbnail-container">	
	
			{block name='product_thumbnail'}
			{if $product.cover}
			  <a href="{$product.url}" class="thumbnail product-thumbnail">
				<img
			class="img_main"
				  <img src = "{$product.cover.bySize.home_default.url}" alt = "{if !empty($product.cover.legend)}{$product.cover.legend}{else}{$product.name|truncate:30:'...'}{/if}">
			  </a>
	{/if}
	{/block}
					
			   {if $product.has_discount}
				  {hook h='displayProductPriceBlock' product=$product type="old_price"}
				  {if $product.discount_type === 'percentage'}
				  <div class="discount_type_flag">
					<span class="discount-percentage">{$product.discount_percentage}</span>
				  </div>
				  {/if}
			 {/if}
	
		
			</div>
		<div class="product-description">
				   {block name='product_reviews'}
								{* {hook h='displayProductListReviews' product=$product} *}
							{/block} 
						 
	  {block name='product_name'}
				<span class="h3 product-title" itemprop="name"><a href="{$product.url}">{$product.name}</a></span>
				{if $product.quantity > 0}
					<span class="delivery-information stock">En stock</span>
				  {* Out of stock message should not be displayed if customer can't order the product. *}
				   {elseif $product.quantity <= 0 }
					<span class="delivery-information horsstock">Plus en stock</span>
				  {/if}  
			{/block}
			  
			  {block name='product_price_and_shipping'}
					{if $product.show_price}
					  <div class="product-price-and-shipping">
						<span class="sr-only">{l s='Price' d='Shop.Theme.Catalog'}</span>
					  <span itemprop="price" class="price">{$product.price}</span>
						{if $product.has_discount}
						  {hook h='displayProductPriceBlock' product=$product type="old_price"}               
						  <span class="sr-only">{l s='Regular price' d='Shop.Theme.Catalog'}</span>
						  <span class="regular-price">{$product.regular_price}</span>
						{/if}
	
						{hook h='displayProductPriceBlock' product=$product type="before_price"}                     
	
					 {hook h='displayProductPriceBlock' product=$product type='unit_price'}
	
						{hook h='displayProductPriceBlock' product=$product type='weight'}
					  </div>
					{/if}
				  {/block}
				  
			</div>
						
		
					<div class="highlighted-informations{if !$product.main_variants} no-variants{/if} hidden-sm-down">
			
				  {block name='product_variants'}
					{if $product.main_variants}
					  {include file='catalog/_partials/variant-links.tpl' variants=$product.main_variants}
					{/if}
				  {/block}
	
			 
		</div>
	</div>
	{/block}