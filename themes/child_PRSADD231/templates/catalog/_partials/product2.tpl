{**
 * 2007-2017 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2017 PrestaShop SA
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}
<div id="js-product-list">
	<div class="products">
		<ul class="product_list grid gridcount"> <!-- removed product_grid-->
			{foreach from=$listing.products item="product"}
				{block name='product_miniature'}
					<li class="product_item col-xs-12 col-sm-6 col-md-6 col-lg-3">
						{include file='catalog/_partials/miniatures/product-listgrid.tpl' product=$product}
					</li>
				{/block}
			{/foreach}
		</ul>
	</div>
  
	{literal}
		<script>
		$(document).ready(function(){
		$('.grade-stars').each(function(){
			const $ratingComponent = $(this);
			var plein = $(this).attr('data-grade');
			var vide = 5 - plein;
			console.log(plein);
			var i=0;
			var n=0;
			var j=0;
			if (vide>0){
				$(this).append('<div class="star-content star-empty clearfix"></div>');
				
				if (plein>0){
				while (j<plein) {
					$(".star-empty", this).append('<div class="star" style="visibility: hidden;"></div>');
					j++;
				}}

			while (n<vide) {
	  $(".star-empty", this).append('<div class="star"></div>');
	  n++;
			}
			}

			if (plein>0){
				$(this).append('<div class="star-content star-full clearfix"></div>');
			while (i<plein) {
	  $('.star-full',this).append('<div class="star-on"></div>');
	  i++;
			}

			}
			
		  });
		})
		
		</script>
	  {/literal}
	{block name='pagination'}
		{include file='_partials/pagination.tpl' pagination=$listing.pagination}
	{/block}

	<!--<div class="hidden-md-up text-xs-right up">
		<a href="#header" class="btn btn-secondary">
			{l s='Back to top' d='Shop.Theme.Actions'}
			<i class="material-icons">&#xE316;</i>
		</a>
	</div>-->
</div>
