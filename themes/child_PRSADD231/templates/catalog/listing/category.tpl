{**
 * 2007-2017 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2017 PrestaShop SA
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}
{extends file='catalog/listing/product-list.tpl'}

{block name='product_list_header'}
    <div class="block-category card card-block ">
        {if $category.image.large.url}
            <div class="category-cover">
                <img src="{$category.image.large.url}" alt="{$category.image.legend}">
            </div>
        {/if}
        <h1 class="h1">{$category.name}</h1>
        {if $category.description}
            {if !$smarty.server.REQUEST_URI|strstr:'page='}

                <div id="category-description" class="text-muted">{$category.description nofilter}</div>
                {literal}
                    <script>
                        if ($(window).width() < 767) {
                            var haut2 = 160;
                        } else {
                            var haut2 = 118;

                        }
                        var haut = $('div#category-description').height();
                        if (haut > haut2) {
                            $('div#category-description').after(
                                '<p style="color: #ed6736;margin-top: -8px;cursor:pointer;" class="lires">Lire la suite</p>');
                            $('div#category-description').css('height', haut2 + 'px');
                            $('div#category-description').css('overflow', 'hidden');
                            $('.lires').click(function() {
                                $('div#category-description').height('auto');
                                $('.lires').css('display', 'none')
                            })
                        }
                    </script>
                {/literal}
            {/if}
        {/if}
    </div>
{/block}