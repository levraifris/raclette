{**
 * 2007-2017 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2017 PrestaShop SA
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}

 <div class="loader" style="display: none;">
</div>

{literal}
    <script>
    $( document ).ready(function() {
    $('.search-widget form button[type="submit"]').click(function(){
        $('.loader').css('display','block');
        $('.loader img').css('display','block')
    });
    if($(window).width() < 767){
    $('.mm_columns_ul').first().addClass('active');
$('span.arrow.closed').first().removeClass('closed').addClass('opened');
}
    });
    </script>
{/literal}

{block name='header_banner'}
    <div class="header-banner">
        {hook h='displayBanner'}
    </div>
{/block}

{block name='header_nav'}

    <nav class="header-nav">
        <div class="container">

            <div class="hidden-md-down">
                <div class="left-nav">
                    {hook h='displayNav1'}
                </div>

                <div class="right-nav">
                    {hook h='displayNav2'}
                </div>
            </div>

            <div class="hidden-lg-up text-xs-center mobile container">
                <div class="text-xs-left mobile hidden-lg-up mobile-menu">
                    <div class="container menu-container">
                        <div class="menu-icon">
                            <div class="cat-title"> <i class="material-icons menu-open">&#xE5D2;</i></div>
                        </div>

                    </div>
                </div>

                <div class="top-logo" id="_mobile_logo"></div>

                <div class="pull-xs-right" id="_mobile_cart"></div>
                <div class="pull-xs-right" id="_mobile_user_info"></div>
                <div class="clearfix"></div>
            </div>

        </div>
    </nav>
{/block}

{block name='header_top'}
    <div class="header-top">
        <div class="container-fluid">
            <div class="header_logo " id="_desktop_logo">
                <a href="{$urls.base_url}">
                    <img class="logo img-responsive" src="{$shop.logo}" alt="{$shop.name}">
                </a>
            </div>
            {hook h='displayMegaMenu'}
            <div class="header_logo " id="mob_logo">
                <a href="{$urls.base_url}">
                    <img class="logo img-responsive" src="{$shop.logo}" alt="{$shop.name}">
                </a>
            </div>

            {hook h='displayTop'}

        </div>
        
    </div>
    <div class="nav-fullwidth">
        <div class="container">

            {* {hook h='displayNavFullWidth'} *}
            {literal}
                <script>
                    $(document).ready(function() {
                        $('li.mm_menus_li.mm_sub_align_full').each(function() {
                            var lefta = $(this).position();
                            var larg = $('ul.mm_columns_ul', this).width() / 2;
                            var larg2 = $(this).width() / 2;
                            var taill = lefta.left + larg2 - larg;
                            $('ul.mm_columns_ul', this).css('left', taill);
                        });
                    })
                </script>
            {/literal}
        </div>
    </div>
{/block}