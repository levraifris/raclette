{**
 * 2007-2017 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2017 PrestaShop SA
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}
 {if !empty($smarty.get.order)}
        {capture assign='ordering'}order={$smarty.get.order}&{/capture}
    {else}
        {assign var='ordering' value=''}
    {/if}

    {if !empty($smarty.get.resultsPerPage)}
        {assign var='results_per_page' value=$smarty.get.resultsPerPage}
    {else}
        {assign var='results_per_page' value=25}
    {/if}
<nav class="pagination row">
  <div class="col-md-5">
    {block name='pagination_summary'}
    {l s='Showing %from%-%to% of %total% item(s)' d='Shop.Theme.Catalog' sprintf=['%from%' => $pagination.items_shown_from ,'%to%' => $pagination.items_shown_to, '%total%' => $pagination.total_items]}
    {/block}
  </div>
  <div class="col-md-7">
  {block name='pagination_page_list'}
     {if $pagination.should_be_displayed}
     
        <ul class="page-list clearfix text-sm-center">
      {foreach from=$pagination.pages item="page"}
        <li {if $page.current} class="current" {/if}>
          {if $page.type === 'spacer'}
            <span class="spacer">&hellip;</span>
          {else}
            <a
              rel="{if $page.type === 'previous'}prev{elseif $page.type === 'next'}next{else}nofollow{/if}"
              href="{$page.url}"
              class="{if $page.type === 'previous'}previous {elseif $page.type === 'next'}next {/if}{['disabled' => !$page.clickable, 'js-search-link2' => true]|classnames}"
            >
              {if $page.type === 'previous'}
                <i class="material-icons">&#xE314;</i>{l s='Previous' d='Shop.Theme.Actions'}
              {elseif $page.type === 'next'}
                <i class="material-icons">&#xE315;</i>{l s='Next' d='Shop.Theme.Actions'}
              {else}
                {$page.page}
              {/if}
            </a>
          {/if}
        </li>
      {/foreach}
    </ul>
    <div style="width:100%;float: right;" >
      <a rel="nofollow" href="?{$ordering}resultsPerPage=1000" style="float:right;margin-top: 30px;width: fit-content;
    display: inline-block;">
            Voir tous les articles
        </a>
    </div>
  
      {/if}
    {/block}
  </div>
</nav>
