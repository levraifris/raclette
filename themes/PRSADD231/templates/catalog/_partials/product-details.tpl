<div class="tab-pane fade{if !$product.description} in active{/if}"
     id="product-details"
     data-product="{$product.embedded_attributes|json_encode}"
     role="tabpanel"
  >

  {block name='product_cacrac'}
    {if $product.dwf_type_de_produit || $product.dwf_quantite || $product.compatible_avec || $product.dwf_marque_compatible || $product.dwf_particularites  || $product.manufacturer_name}
      <div class="half-carac">
      <h2>Caractéristiques générales</h2>
    {if $product.dwf_type_de_produit}<p><span class='labelcara'>Type de produit : </span>{$product.dwf_type_de_produit}<p>{/if}
    {if $product.dwf_quantite}<p><span class='labelcara'>Quantité : </span>{$product.dwf_quantite}<p>{/if}
    {if $product.compatible_avec}<p><span class='labelcara'>Compatible avec : </span>{$product.compatible_avec}<p>{/if}
    {if $product.manufacturer_name}<p><span class='labelcara'>Marque : </span>{$product.manufacturer_name}<p>{/if}
    {if $product.dwf_marque_compatible}<p><span class='labelcara'>Marque(s) compatible(s) : </span>{$product.dwf_marque_compatible}<p>{/if}
    {if $product.dwf_particularites}<p><span class='labelcara'>Particularités : </span>{$product.dwf_particularites}<p>{/if}
    </div>{/if}


    {if $product.dwf_nombre_de_personnes || $product.dwf_energie || $product.dwf_type_de_gaz || $product.dwf_puissance || $product.dwf_type_de_cuisson || $product.dwf_zone_de_cuisson || $product.dwf_thermostat_reglable || $product.dwf_temperature_maximale_de_cuisson || $product.dwf_rapidite_de_montee_en_temperature || $product.dwf_clapet_d_aeration}
      <div class="half-carac">
      <h2>Cuisson</h2>
  {if $product.dwf_nombre_de_personnes}<p><span class='labelcara'>Nombre de personnes : </span>{$product.dwf_nombre_de_personnes}<p>{/if}
  {if $product.dwf_energie}<p><span class='labelcara'>Energie : </span>{$product.dwf_energie}<p>{/if}
  {if $product.dwf_type_de_gaz}<p><span class='labelcara'>Type de gaz : </span>{$product.dwf_type_de_gaz}<p>{/if}
  {if $product.dwf_puissance}<p><span class='labelcara'>Puissance : </span>{$product.dwf_puissance}<p>{/if}
  {if $product.dwf_type_de_cuisson}<p><span class='labelcara'>Type de cuisson : </span>{$product.dwf_type_de_cuisson}<p>{/if}
  {if $product.dwf_zone_de_cuisson}<p><span class='labelcara'>Zone de cuisson : </span>{$product.dwf_zone_de_cuisson}<p>{/if}
  {if $product.dwf_thermostat_reglable}<p><span class='labelcara'>Thermostat réglable : </span>{$product.dwf_thermostat_reglable}<p>{/if}
  {if $product.dwf_temperature_maximale_de_cuisson}<p><span class='labelcara'>Température maximale de cuisson : </span>{$product.dwf_temperature_maximale_de_cuisson}<p>{/if}
  {if $product.dwf_rapidite_de_montee_en_temperature}<p><span class='labelcara'>Rapidité de montée en température : </span>{$product.dwf_rapidite_de_montee_en_temperature}<p>{/if}
  {if $product.dwf_clapet_d_aeration}<p><span class='labelcara'>Clapet d'aération : </span>{$product.dwf_clapet_d_aeration}<p>{/if}
  </div> {/if} 
    
  {if $product.dwf_nombre_de_grilles_plaques || $product.dwf_grille_maintien_au_chaud || $product.dwf_nombre_de_bruleurs || $product.dwf_systeme_d_allumage || $product.dwf_thermometre || $product.dwf_sonde_de_cuisson || $product.dwf_commande_a_distance || $product.dwf_rechaud_lateral || $product.dwf_minuteur || $product.dwf_crochets_de_suspension || $product.dwf_eclairage}
  <div class="half-carac">
    <h2>Equipement</h2>
  {if $product.dwf_nombre_de_grilles_plaques}<p><span class='labelcara'>Nombre de grilles / plaques : </span>{$product.dwf_nombre_de_grilles_plaques}<p>{/if}
  {if $product.dwf_grille_maintien_au_chaud}<p><span class='labelcara'>Grille maintien au chaud : </span>{$product.dwf_grille_maintien_au_chaud}<p>{/if}
  {if $product.dwf_nombre_de_bruleurs}<p><span class='labelcara'>Nombre de brûleurs : </span>{$product.dwf_nombre_de_bruleurs}<p>{/if}
  {if $product.dwf_systeme_d_allumage}<p><span class='labelcara'>Système d’allumage : </span>{$product.dwf_systeme_d_allumage}<p>{/if}
  {if $product.dwf_thermometre}<p><span class='labelcara'>Thermomètre : </span>{$product.dwf_thermometre}<p>{/if}
  {if $product.dwf_sonde_de_cuisson}<p><span class='labelcara'>Sonde de cuisson : </span>{$product.dwf_sonde_de_cuisson}<p>{/if}
  {if $product.dwf_commande_a_distance}<p><span class='labelcara'>Commande à distance : </span>{$product.dwf_commande_a_distance}<p>{/if}
  {if $product.dwf_rechaud_lateral}<p><span class='labelcara'>Réchaud latéral : </span>{$product.dwf_rechaud_lateral}<p>{/if}
  {if $product.dwf_minuteur}<p><span class='labelcara'>Minuteur : </span>{$product.dwf_minuteur}<p>{/if}
  {if $product.dwf_crochets_de_suspension}<p><span class='labelcara'>Crochets de suspension : </span>{$product.dwf_crochets_de_suspension}<p>{/if}
  {if $product.dwf_eclairage}<p><span class='labelcara'>Eclairage : </span>{$product.dwf_eclairage}<p>{/if}
    </div>{/if}

    {if $product.dwf_matiere || $product.dwf_matiere_de_la_surface_de_cuisson || $product.dwf_matiere_de_la_cuve || $product.dwf_matiere_du_couvercle || $product.dwf_matiere_de_la_tablette_de_travail || $product.dwf_matiere_du_chariot || $product.dwf_coloris}
    <div class="half-carac">
    <h2>Matière et coloris</h2>
  {if $product.dwf_matiere}<p><span class='labelcara'>Matière : </span>{$product.dwf_matiere}<p>{/if}
  {if $product.dwf_matiere_de_la_surface_de_cuisson}<p><span class='labelcara'>Matière de la surface de cuisson : </span>{$product.dwf_matiere_de_la_surface_de_cuisson}<p>{/if}
  {if $product.dwf_matiere_de_la_cuve}<p><span class='labelcara'>Matière de la cuve : </span>{$product.dwf_matiere_de_la_cuve}<p>{/if}
  {if $product.dwf_matiere_du_couvercle}<p><span class='labelcara'>Matière du couvercle : </span>{$product.dwf_matiere_du_couvercle}<p>{/if}
  {if $product.dwf_matiere_de_la_tablette_de_travail}<p><span class='labelcara'>Matière de la tablette de travail : </span>{$product.dwf_matiere_de_la_tablette_de_travail}<p>{/if}
  {if $product.dwf_matiere_du_chariot }<p><span class='labelcara'>Matière du chariot : </span>{$product.dwf_matiere_du_chariot }<p>{/if}
  {if $product.dwf_coloris}<p><span class='labelcara'>Coloris : </span>{$product.dwf_coloris}<p>{/if}
    </div>{/if}
    
    {if $product.dwf_grille_plaque_amovible || $product.dwf_fond_de_cuve_amovible || $product.dwf_compatible_au_lave_vaisselle || $product.dwf_entierement_demontable || $product.dwf_resistance_amovible || $product.dwf_recuperation_des_cendres || $product.dwf_recuperation_de_jus_graisse || $product.dwf_bac_de_reservoir_d_eau}
    <div class="half-carac">
    <h2>Facilité de nettoyage</h2>
  {if $product.dwf_grille_plaque_amovible}<p><span class='labelcara'>Grille/plaque amovible : </span>{$product.dwf_grille_plaque_amovible}<p>{/if}
  {if $product.dwf_fond_de_cuve_amovible}<p><span class='labelcara'>Fond de cuve amovible : </span>{$product.dwf_fond_de_cuve_amovible}<p>{/if}
  {if $product.dwf_compatible_au_lave_vaisselle}<p><span class='labelcara'>Compatible au lave-vaisselle : </span>{$product.dwf_compatible_au_lave_vaisselle}<p>{/if}
  {if $product.dwf_entierement_demontable}<p><span class='labelcara'>Entièrement démontable : </span>{$product.dwf_entierement_demontable}<p>{/if}
  {if $product.dwf_resistance_amovible}<p><span class='labelcara'>Résistance amovible : </span>{$product.dwf_resistance_amovible}<p>{/if}
  {if $product.dwf_recuperation_des_cendres}<p><span class='labelcara'>Récupération des cendres : </span>{$product.dwf_recuperation_des_cendres}<p>{/if}
  {if $product.dwf_recuperation_de_jus_graisse}<p><span class='labelcara'>Récupération de jus/graisse : </span>{$product.dwf_recuperation_de_jus_graisse}<p>{/if}
  {if $product.dwf_bac_de_reservoir_d_eau}<p><span class='labelcara'>Bac de réservoir d’eau : </span>{$product.dwf_bac_de_reservoir_d_eau}<p>{/if}
    </div>{/if}



    {if $product.dwf_mobilite || $product.dwf_chariot_pliable || $product.dwf_pieds_reglables || $product.dwf_couvercle || $product.dwf_pare_vent || $product.dwf_tablette_de_travail || $product.dwf_poignees_de_transport || $product.dwf_espace_de_rangement}
    <div class="half-carac">
    <h2>Agencement</h2>
  {if $product.dwf_mobilite}<p><span class='labelcara'>Mobilité : </span>{$product.dwf_mobilite}<p>{/if}
  {if $product.dwf_chariot_pliable}<p><span class='labelcara'>Chariot pliable : </span>{$product.dwf_chariot_pliable}<p>{/if}
  {if $product.dwf_pieds_reglables}<p><span class='labelcara'>Pieds réglables : </span>{$product.dwf_pieds_reglables}<p>{/if}
  {if $product.dwf_couvercle}<p><span class='labelcara'>Couvercle : </span>{$product.dwf_couvercle}<p>{/if}
  {if $product.dwf_pare_vent}<p><span class='labelcara'>Pare-vent : </span>{$product.dwf_pare_vent}<p>{/if}
  {if $product.dwf_tablette_de_travail}<p><span class='labelcara'>Tablette de travail : </span>{$product.dwf_tablette_de_travail}<p>{/if}
  {if $product.dwf_poignees_de_transport}<p><span class='labelcara'>Poignées de transport : </span>{$product.dwf_poignees_de_transport}<p>{/if}
  {if $product.dwf_espace_de_rangement}<p><span class='labelcara'>Espace de rangement : </span>{$product.dwf_espace_de_rangement}<p>{/if}
    </div>{/if}

    {if $product.dwf_poignee_froide || $product.dwf_voyants_lumineux_de_fonctionnement || $product.dwf_pieds_antiderapants || $product.dwf_systeme_de_securite}
    <div class="half-carac">
    <h2>Sécurité</h2>
  {if $product.dwf_poignee_froide}<p><span class='labelcara'>Poignée froide : </span>{$product.dwf_poignee_froide}<p>{/if}
  {if $product.dwf_voyants_lumineux_de_fonctionnement}<p><span class='labelcara'>Voyants lumineux de fonctionnement : </span>{$product.dwf_voyants_lumineux_de_fonctionnement}<p>{/if}
  {if $product.dwf_pieds_antiderapants}<p><span class='labelcara'>Pieds antidérapants : </span>{$product.dwf_pieds_antiderapants}<p>{/if}
  {if $product.dwf_systeme_de_securite}<p><span class='labelcara'>Système de sécurité : </span>{$product.dwf_systeme_de_securite}<p>{/if}
  </div>{/if}

  {if $product.dwf_diametre || $product.dwf_dimension_de_la_surface_de_cuisson || $product.dwf_dimensions_l_x_h_x_p || $product.dwf_dimension_du_carton_l_x_h_x_p || $product.dwf_poids || $product.dwf_longueur_du_cordon_electrique}
    <div class="half-carac">
    <h2>Dimensions</h2>
  {if $product.dwf_dimension_de_la_surface_de_cuisson}<p><span class='labelcara'>Dimension de la surface de cuisson : </span>{$product.dwf_dimension_de_la_surface_de_cuisson}<p>{/if}
  {if $product.dwf_dimensions_l_x_h_x_p}<p><span class='labelcara'>Dimensions l x h x p : </span>{$product.dwf_dimensions_l_x_h_x_p}<p>{/if}
  {if $product.dwf_diametre}<p><span class='labelcara'>Diamètre : </span>{$product.dwf_diametre}<p>{/if}
  {if $product.dwf_dimension_du_carton_l_x_h_x_p}<p><span class='labelcara'>Dimension du carton l x h x p : </span>{$product.dwf_dimension_du_carton_l_x_h_x_p}<p>{/if}
  {if $product.dwf_poids}<p><span class='labelcara'>Poids : </span>{$product.dwf_poids}<p>{/if}
  {if $product.dwf_longueur_du_cordon_electrique}<p><span class='labelcara'>Longueur du cordon électrique : </span>{$product.dwf_longueur_du_cordon_electrique}<p>{/if}
  </div>{/if}



        {if $product.dwf_livre_avec}
    <div class="half-carac">
      <h2>Contenu du carton</h2>
  <p><span class='labelcara'>Livré avec : </span>{$product.dwf_livre_avec}<p>
    </div>{/if}

    {if $product.dwf_reference_constructeur || $product.dwf_disponibilite_des_pieces_detachees_donnees_fournisseur || $product.dwf_duree_de_la_garantie || $product.dwf_fabrique_en}
    <div class="half-carac">
    <h2>Informations et Services</h2>
  {if $product.dwf_reference_constructeur}<p><span class='labelcara'>Référence constructeur : </span>{$product.dwf_reference_constructeur}<p>{/if}
  {if $product.dwf_disponibilite_des_pieces_detachees_donnees_fournisseur}<p><span class='labelcara'>Disponibilité des pièces détachées (données fournisseur) : </span>{$product.dwf_disponibilite_des_pieces_detachees_donnees_fournisseur}<p>{/if}
  {if $product.dwf_duree_de_la_garantie}<p><span class='labelcara'>Durée de la garantie : </span>{$product.dwf_duree_de_la_garantie}<p>{/if}
  {if $product.dwf_fabrique_en}<p><span class='labelcara'>Fabriqué en : </span>{$product.dwf_fabrique_en}<p>{/if}    
    </div>{/if}
  {/block}


    {literal}
      <script>
      if($(window).innerWidth() >= 768) {
              const numCols = 2;
      const colHeights = Array(numCols).fill(0);
      const container = document.getElementById('product-details');
      Array.from(container.children).forEach((child, i) => {
        const order = i % numCols;
        child.style.order = order;
        colHeights[order] += parseFloat(child.clientHeight);
      });
      const plusmarge= Math.max(...colHeights) + 60;
      container.style.height = plusmarge + 'px';
            };
      </script>
    {/literal}


    
</div>
