
function blogtoggle(){

	if ($(window).width() <= 991){
		$('.lastest_block .blog-row .blog-title').click(function() {
		    $('.lastest_block .blog-row .homeblog-inner').slideToggle("slow");
		    $('.lastest_block .blog-row .blog-title').toggleClass('active');
		  });
	}

}
$(window).resize(function(){blogtoggle();});
$(window).ready(function() {blogtoggle();});







function additionalCarousel(sliderId){
	/*======  curosol For Additional ==== */
	 var tmadditional = $(sliderId);
      tmadditional.owlCarousel({
     	 items : 3, //10 items above 1000px browser width
     	 itemsDesktop : [1199,2], 
     	 itemsDesktopSmall : [991,1], 
     	 itemsTablet: [480,2], 
     	 itemsMobile : [320,1] 
      });
      // Custom Navigation Events
      $(".additional_next").click(function(){
        tmadditional.trigger('owl.next');
      })
      $(".additional_prev").click(function(){
        tmadditional.trigger('owl.prev');
      });
}

 $('.language-selector-wrapper').appendTo('.user-info');
 	$('.currency-selector').appendTo('.user-info');
 	
$(document).ready(function(){

	$('.menu-icon').on('click', function () {
	$('#mobile_top_menu_wrapper').toggleClass('slide');
	$('.menu-icon').toggleClass('active');
	$('body').toggleClass('active');
	$('#page').toggleClass('active');

	  });
	
	bindGrid();
	additionalCarousel("#main #additional-carousel");
	


	$('.cart_block .block_content').on('click', function (event) {
		event.stopPropagation();
	});
	
	$('#product #productCommentsBlock').appendTo('#product #tab-content #rating');
	
	//breadcumb//
	$('h1.h1').prependTo('.breadcrumb .container');
	//breadcumb//
	
		// ---------------- start more menu setting ----------------------

		var max_elem = 5;
		var items = $('.menu ul#top-menu > li');	
		var surplus = items.slice(max_elem, items.length);
		
		surplus.wrapAll('<li class="category more_menu" id="more_menu"><div id="top_moremenu" class="popover sub-menu js-sub-menu collapse"><ul class="top-menu more_sub_menu">');
	
		$('.menu ul#top-menu .more_menu').prepend('<a href="#" class="dropdown-item" data-depth="0"><span class="pull-xs-right hidden-md-up"><span data-target="#top_moremenu" data-toggle="collapse" class="navbar-toggler collapse-icons"><i class="material-icons add">&#xE313;</i><i class="material-icons remove">&#xE316;</i></span></span></span>More</a>');
	
		$('.menu ul#top-menu .more_menu').mouseover(function(){
			$(this).children('div').css('display', 'block');
		})
		.mouseout(function(){
			$(this).children('div').css('display', 'none');
		});
	// ---------------- end more menu setting ----------------------

});








// Add/Remove acttive class on menu active in responsive  
	$('#menu-icon').on('click', function() {
		$(this).toggleClass('active');
	});

// Loading image before flex slider load
	$(window).load(function() { 
		$(".loadingdiv").removeClass("spinner"); 
	});

// Flex slider load
	$(window).load(function() {
		if($('.flexslider').length > 0){ 
			$('.flexslider').flexslider({		
				slideshowSpeed: $('.flexslider').data('interval'),
				pauseOnHover: $('.flexslider').data('pause'),
				  animation: "slide"
			});
		}
	});		

// Scroll page bottom to top
	$(window).scroll(function() {
		if ($(this).scrollTop() > 500) {
			$('.top_button').fadeIn(500);
		} else {
			$('.top_button').fadeOut(500);
		}
	});							
	$('.top_button').click(function(event) {
		event.preventDefault();		
		$('html, body').animate({scrollTop: 0}, 800);
	});



/*======  Carousel Slider For Feature Product ==== */
	
	var tmfeature = $("#feature-carousel");
	tmfeature.owlCarousel({
		items : 4, //10 items above 1000px browser width
		itemsDesktop : [1199,4], 
		itemsDesktopSmall : [991,3], 
		itemsTablet: [767,2], 
		itemsMobile : [479,2] 
	});
	// Custom Navigation Events
	$(".feature_next").click(function(){
		tmfeature.trigger('owl.next');
	})
	$(".feature_prev").click(function(){
		tmfeature.trigger('owl.prev');
	});



/*======  Carousel Slider For Blog ==== */
	var tmblog = $("#blog-carousel");
	var x =  tmblog.children();
		for (i = 0; i < x.length+1 ; i += 2) {
		  	x.slice(i,i+2).wrapAll('<div class="'+ i +'"></div>');
		}
		

	tmblog.owlCarousel({
		items : 1, //10 items above 1000px browser width
		itemsDesktop : [1199,1], 
		itemsDesktopSmall : [991,1], 
		itemsTablet: [767,2], 
		itemsMobile : [742,1] 
	});
	// Custom Navigation Events
	$(".blog_next").click(function(){
		tmblog.trigger('owl.next');
	})
	$(".blog_prev").click(function(){
		tmblog.trigger('owl.prev');
	});



/*======  Carousel Slider For New Product ==== */
	
	var tmnewproduct = $("#newproduct-carousel");
	tmnewproduct.owlCarousel({
		items : 4, //10 items above 1000px browser width
		itemsDesktop : [1199,4], 
		itemsDesktopSmall : [991,3], 
		itemsTablet: [767,2], 
		itemsMobile : [479,2] 
	});
	// Custom Navigation Events
	$(".newproduct_next").click(function(){
		tmnewproduct.trigger('owl.next');
	})
	$(".newproduct_prev").click(function(){
		tmnewproduct.trigger('owl.prev');
	});



/*======  Carousel Slider For Bestseller Product ==== */
	
	var tmbestseller = $("#bestseller-carousel");
	tmbestseller.owlCarousel({
		items : 4, //10 items above 1000px browser width
		itemsDesktop : [1199,4], 
		itemsDesktopSmall : [991,3], 
		itemsTablet: [767,2], 
		itemsMobile : [479,2] 
	});
	// Custom Navigation Events
	$(".bestseller_next").click(function(){
		tmbestseller.trigger('owl.next');
	})
	$(".bestseller_prev").click(function(){
		tmbestseller.trigger('owl.prev');
	});



/*======  Carousel Slider For Special Product ==== */
	var tmspecial = $("#special-carousel");
	tmspecial.owlCarousel({
		items : 4, //10 items above 1000px browser width
		itemsDesktop : [1199,4], 
		itemsDesktopSmall : [991,3], 
		itemsTablet: [767,2], 
		itemsMobile : [479,2] 
	});
	// Custom Navigation Events
	$(".special_next").click(function(){
		tmspecial.trigger('owl.next');
	})
	$(".special_prev").click(function(){
		tmspecial.trigger('owl.prev');
	});


/*======  Carousel Slider For Accessories Product ==== */

	var tmaccessories = $("#accessories-carousel");
	tmaccessories.owlCarousel({
		items : 4, //10 items above 1000px browser width
		itemsDesktop : [1199,4], 
		itemsDesktopSmall : [991,3], 
		itemsTablet: [767,2], 
		itemsMobile : [479,2] 
	});
	// Custom Navigation Events
	$(".accessories_next").click(function(){
		tmaccessories.trigger('owl.next');
	})
	$(".accessories_prev").click(function(){
		tmaccessories.trigger('owl.prev');
	});


/*======  Carousel Slider For Category Product ==== */

	var tmproductscategory = $("#productscategory-carousel");
	tmproductscategory.owlCarousel({
		items : 4, //10 items above 1000px browser width
		itemsDesktop : [1199,4], 
		itemsDesktopSmall : [991,3], 
		itemsTablet: [767,2], 
		itemsMobile : [479,2] 
	});
	// Custom Navigation Events
	$(".productscategory_next").click(function(){
		tmproductscategory.trigger('owl.next');
	})
	$(".productscategory_prev").click(function(){
		tmproductscategory.trigger('owl.prev');
	});


/*======  Carousel Slider For Viewed Product ==== */

	var tmviewed = $("#viewed-carousel");
	tmviewed.owlCarousel({
		items : 4, //10 items above 1000px browser width
		itemsDesktop : [1199,4], 
		itemsDesktopSmall : [991,3], 
		itemsTablet: [767,2], 
		itemsMobile : [479,2] 
	});
	// Custom Navigation Events
	$(".viewed_next").click(function(){
		tmviewed.trigger('owl.next');
	})
	$(".viewed_prev").click(function(){
		tmviewed.trigger('owl.prev');
	});

/*======  Carousel Slider For Crosssell Product ==== */

	var tmcrosssell = $("#crosssell-carousel");
	tmcrosssell.owlCarousel({
		items :4, //10 items above 1000px browser width
		itemsDesktop : [1199,4], 
		itemsDesktopSmall : [991,3], 
		itemsTablet: [767,2], 
		itemsMobile : [479,2] 
	});
	// Custom Navigation Events
	$(".crosssell_next").click(function(){
		tmcrosssell.trigger('owl.next');
	})
	$(".crosssell_prev").click(function(){
		tmcrosssell.trigger('owl.prev');
	});

/*======  curosol For Manufacture ==== */
	 var tmbrand = $("#brand-carousel");
      tmbrand.owlCarousel({
     	 items : 5, //10 items above 1000px browser width
     	 itemsDesktop : [1199,4], 
     	 itemsDesktopSmall : [991,3],
     	 itemsTablet: [767,2], 
     	 itemsMobile : [320,1] 
      });
      // Custom Navigation Events
      $(".brand_next").click(function(){
        tmbrand.trigger('owl.next');
      })
      $(".brand_prev").click(function(){
        tmbrand.trigger('owl.prev');
      });
	  


/*======  Carousel Slider For categorylist ==== */

		var tmcat = $("#tmcategorylist-carousel");
		tmcat.owlCarousel({
		items : 3, //10 items above 1000px browser width
		itemsDesktop : [1199,3], 
		itemsDesktopSmall : [991,3], 
		itemsTablet: [767,2], 
		itemsMobile : [480,1] 

		});
		// Custom Navigation Events
		$(".cat_next").click(function(){
		tmcat.trigger('owl.next');
		})
		$(".cat_prev").click(function(){
		tmcat.trigger('owl.prev');
		});

/*======  Carousel Slider For testimonial block ==== */

		var tmtestimonial = $("#testimonial-carousel");
		tmtestimonial.owlCarousel({
		//autoPlay: true,
		items : 1, //10 items above 1000px browser width
		itemsDesktop : [1199,1], 
		itemsDesktopSmall : [991,1], 
		itemsTablet: [479,1], 
		itemsMobile : [319,1]

		});
		// Custom Navigation Events
      $(".tmtestimonial_next").click(function(){
        tmtestimonial.trigger('owl.next');
      })
      $(".tmtestimonial_prev").click(function(){
        tmtestimonial.trigger('owl.prev');
      });
 
function bindGrid()
{
var view = $.totalStorage("display");

if (view && view != 'grid')
display(view);
else
$('.display').find('li#grid').addClass('selected');

$(document).on('click', '#grid', function(e){
e.preventDefault();
display('grid');
});

$(document).on('click', '#list', function(e){
e.preventDefault();
display('list');	
});	
}

function display(view)
{
	if (view == 'list')
	{
		$('#products ul.product_list').removeClass('grid').addClass('list row');
		$('#products .product_list > li').removeClass('col-xs-12 col-sm-6 col-md-6 col-lg-3').addClass('col-xs-12');
		
		
		$('#products .product_list > li').each(function(index, element) {
			var html = '';
			html = '<div class="product-miniature js-product-miniature" data-id-product="'+ $(element).find('.product-miniature').data('id-product') +'" data-id-product-attribute="'+ $(element).find('.product-miniature').data('id-product-attribute') +'" itemscope itemtype="http://schema.org/Product"><div class="row">';
				html += '<div class="thumbnail-container col-xs-4 col-xs-5 col-md-4">' + $(element).find('.thumbnail-container').html() + '</div>';
				
				html += '<div class="product-description center-block col-xs-4 col-xs-7 col-md-8">';
				html += '<div class="comments_note">'+ $(element).find('.comments_note').html() +'</div>';
					html += '<h3 class="h3 product-title" itemprop="name">'+ $(element).find('h3').html() + '</h3>';
					
					var price = $(element).find('.product-price-and-shipping').html();       // check : catalog mode is enabled
					if (price != null) {
						html += '<div class="product-price-and-shipping">'+ price + '</div>';
					}
					
					html += '<div class="product-detail">'+ $(element).find('.product-detail').html() + '</div>';
					
					var colorList = $(element).find('.highlighted-informations').html();
					if (colorList != null) {
						html += '<div class="highlighted-informations">'+ colorList +'</div>';
					}
					
					html += '<div class="product-block-outer">'+ $(element).find('.product-block-outer').html() +'</div>';
					
				html += '</div>';
			html += '</div></div>';
		$(element).html(html);
		});
		$('.display').find('li#list').addClass('selected');
		$('.display').find('li#grid').removeAttr('class');
		$.totalStorage('display', 'list');
	}
	else
	{
		$('#products ul.product_list').removeClass('list').addClass('grid row');
		$('#products .product_list > li').removeClass('col-xs-12').addClass('col-xs-12 col-sm-6 col-md-6 col-lg-3');
		$('#products .product_list > li').each(function(index, element) {
		var html = '';
		html += '<div class="product-miniature js-product-miniature" data-id-product="'+ $(element).find('.product-miniature').data('id-product') +'" data-id-product-attribute="'+ $(element).find('.product-miniature').data('id-product-attribute') +'" itemscope itemtype="http://schema.org/Product">';
			html += '<div class="thumbnail-container">' + $(element).find('.thumbnail-container').html() +'</div>';
			
			html += '<div class="product-description">';
			html += '<div class="comments_note">'+ $(element).find('.comments_note').html() +'</div>';
				html += '<h3 class="h3 product-title" itemprop="name">'+ $(element).find('h3').html() +'</h3>';
			
				var price = $(element).find('.product-price-and-shipping').html();       // check : catalog mode is enabled
				if (price != null) {
					html += '<div class="product-price-and-shipping">'+ price + '</div>';
				}
				
				html += '<div class="product-detail">'+ $(element).find('.product-detail').html() + '</div>';
					
				var colorList = $(element).find('.highlighted-informations').html();
				if (colorList != null) {
					html += '<div class="highlighted-informations">'+ colorList +'</div>';
				}
				
			html += '</div>';
		html += '</div>';
		$(element).html(html);
		});
		$('.display').find('li#grid').addClass('selected');
		$('.display').find('li#list').removeAttr('class');
		$.totalStorage('display', 'grid');
	}
}







function responsivecolumn(){
	
	if ($(document).width() <= 991){
				
		// ---------------- Fixed header responsive ----------------------
		$(window).bind('scroll', function () {
			if ($(window).scrollTop() > 100) {
				$('.header-nav').addClass('fixed');
					

			} else {
				$('.header-nav').removeClass('fixed');
			}
		});

	}
	if ($(document).width() >= 992){
				
		// ---------------- Fixed header responsive ----------------------
		$(window).bind('scroll', function () {
			if ($(window).scrollTop() > 250) {
				$('.header-top').addClass('fixed');
			} else {
				$('.header-top').removeClass('fixed');
			}
		});
		
	}
	else{
		$('.header-top').removeClass('fixed');
	}
	
	
	if ($(document).width() <= 991)
	{
		$('.container #columns_inner #left-column').appendTo('.container #columns_inner');	
		$('.header-top #search_widget').detach().insertAfter('.header-nav #_mobile_user_info');
		
	}
	else if($(document).width() >= 992)
	{
		$('.container #columns_inner #left-column').prependTo('.container #columns_inner');
		$('.header-nav #search_widget').detach().insertAfter('.header-top #_desktop_user_info');
		
		/*$('#search_widget').detach().insertAfter('.header-top #_desktop_user_info');*/
	}
}

jQuery(window).resize(function() {responsivecolumn();});
jQuery(document).ready(function() {responsivecolumn();});
jQuery(window).scroll(function() {responsivecolumn();});

//sign in toggle
$(document).ready(function(){
	
	 $('#_desktop_user_info, #_mobile_user_info').click(function(event){
		  $(this).toggleClass('active');
		  event.stopPropagation();
		  $(".user-info").slideToggle("fast");
		});
		$(".user-info").on("click", function (event) {
		  event.stopPropagation();
		});
				/*======  Add Item in Menu ==== */
		$('.topdiv #tm_vertical_menu_top ul#top-menu > li,#header #tm_vertical_menu_top ul#top-menu > li').addClass('main');
		var ee1 = 1;
		$('.topdiv #tm_vertical_menu_top ul#top-menu li.main,#header #tm_vertical_menu_top ul#top-menu li.main').each( function() {
			$(this).addClass( 'menu-'+ ee1 );
			ee1 = ee1 + 1;
			
		});		
		
});

$(document).click(function() {
	$(".user-info").slideUp("fast");
	$("#_desktop_user_info").removeClass("active");
		
});

// JS for calling loadMore
		$(document).ready(function () {

			"use strict";	  
			  	var size_li_feat = $("#index #featureProduct .featured_grid li.product_item").size();
			var size_li_new = $("#index #newProduct .newproduct_grid li.product_item").size();
			var size_li_best = $("#index #bestseller .bestseller_grid li.product_item").size();
			var size_li_special = $("#index .special-products .special_grid li.product_item").size();
			
			var x= 8;
			var y= 8;
			var z= 8;
			var a= 8;

			$('#index #featureProduct .featured_grid li.product_item:lt('+x+')').fadeIn('slow');
			$('#index #newProduct .newproduct_grid li.product_item:lt('+y+')').fadeIn('slow');
			$('#index #bestseller .bestseller_grid li.product_item:lt('+z+')').fadeIn('slow');
			$('#index .special-products .special_grid li.product_item:lt('+a+')').fadeIn('slow');
			    	
			    $('.featured_grid .gridcount').click(function () {
			if(x==size_li_feat){	  	
			 $('.featured_grid .gridcount').hide();
			 $('.featured_grid .tm-message').show();
			}else{
			x= (x+4 <= size_li_feat) ? x+4 : size_li_feat;	
			        $('#index #featureProduct .featured_grid li.product_item:lt('+x+')').fadeIn(1000);	
			}
			    });	

			$('.newproduct_grid .gridcount').click(function () {
			if(y==size_li_new){	  
			$('.newproduct_grid .gridcount').hide();
			$('.newproduct_grid .tm-message').show();
			}else{
			y= (y+4 <= size_li_new) ? y+4 : size_li_new;
			        $('#index #newProduct .newproduct_grid li.product_item:lt('+y+')').fadeIn('slow');
			}
			    });	   

			$('.bestseller_grid .gridcount').click(function () {
			if(z==size_li_best){	  
			$('.bestseller_grid .gridcount').hide();
			$('.bestseller_grid .tm-message').show();
			}else{
			z= (z+4 <= size_li_best) ? z+4 : size_li_best;
			        $('#index #bestseller .bestseller_grid li.product_item:lt('+z+')').fadeIn('slow');
			}
			    });
			
				$('.special_grid .gridcount').click(function () {
			if(z==size_li_special){									 
					$('.special_grid .gridcount').hide();
					$('.special_grid .tm-message').show();
			}else{
				z= (z+4 <= size_li_special) ? z+4 : size_li_special;
				$('#index .special-products .special_grid li.product_item:lt('+a+')').fadeIn('slow');
			}
			})
		});


function scrolltab() {
		var tmtabpane = $(".tmcategory-tabs-carousel");
		if ($(window).width() <= 479){
			tmtabpane.owlCarousel({
				itemsMobile : [479,1] 
			});
			// Custom Navigation Events
			$(".tabpane_next").click(function(){
				tmtabpane.trigger('owl.next');
			})
			$(".tabpane_prev").click(function(){
				tmtabpane.trigger('owl.prev');
			});
	}
		
}
$(window).ready(function(){scrolltab();});
$(window).resize(function(){scrolltab();});



function searchtoggle() {
	if($(window).width() > 0){

		$('#header .search_button').click(function(event){
			$(this).toggleClass('active');
			$('#header #search_widget').toggleClass('active');
			event.stopImmediatePropagation();
			$("#header .searchtoggle").slideToggle("fast");
			$('#header .search-widget form input[type="text"]').focus();
		});
			$("#header .searchtoggle").on("click", function (event) {
				event.stopImmediatePropagation();
			});
		}
		else{
			$('#header .search_button,#header .searchtoggle').unbind();
			$('#search_widget').unbind();
			$("#header .searchtoggle").show();
		}
	}
$(window).ready(function() {searchtoggle();});


 