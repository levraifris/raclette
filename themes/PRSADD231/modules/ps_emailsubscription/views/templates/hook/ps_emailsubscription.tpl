{**
 * 2007-2017 PrestaShop
*
* NOTICE OF LICENSE
*
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2017 PrestaShop SA
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
  

<div class="tm_newsletterdiv links">
<div id="newsletter_block_left" class="block container collapse block_content">
	<h2 class="h1 products-section-title text-uppercase">
      {l s='get daily update' d='Shop.Theme.Global'}
  </h2>

    <div class="col-md-7 col-xs-12 block_content">
      <p class="text">{l s='Subscribe to The Newsletter' d='Shop.Theme.Global'}</p>
      <form action="{$urls.pages.index}#footer" method="post">
        
         
          
            <div class="input-wrapper">
              <input type="hidden" name="action" value="0">
              <input
                name="email"
                type="email"
                value="{$value}"
                placeholder="{l s='Enter your Email here....' d='Shop.Forms.Labels'}"
              >
              <input
              class="btn btn-primary pull-xs-right hidden-xs-down"
              name="submitNewsletter"
              type="submit"
              value="{l s='Join Now' d='Shop.Theme.Actions'}"
            >

               <input
              class="btn btn-primary pull-xs-right hidden-sm-up"
              name="submitNewsletter"
              type="submit"
              value="{l s='OK' d='Shop.Theme.Actions'}"
            >
              
            </div>
			
          
            <div class="clearfix"></div>
        
          <div class="col-xs-12 alert-message-box">
              {if $conditions}
                <p class="email_text">{$conditions}</p>
              {/if}
              {if $msg}
                <p class="alert {if $nw_error}alert-danger{else}alert-success{/if}">
                  {$msg}
                </p>
              {/if}
              {if isset($id_module)}
                {hook h='displayGDPRConsent' id_module=$id_module}
              {/if}
          </div>
          </div>

      </form>
    </div>
    </div>

