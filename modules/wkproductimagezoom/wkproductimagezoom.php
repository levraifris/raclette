<?php
/**
* 2010-2021 Webkul.
*
* NOTICE OF LICENSE
*
* All right is reserved,
* Please go through LICENSE.txt file inside our module
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade this module to newer
* versions in the future. If you wish to customize this module for your
* needs please refer to CustomizationPolicy.txt file inside our module for more information.
*
* @author Webkul IN
* @copyright 2010-2021 Webkul IN
* @license LICENSE.txt
*/

class WkProductImageZoom extends Module
{
    protected static $configVars = array("WK_PIZ_LENS_TYPE",
            "WK_PIZ_ZOOM_LEVEL",
            "WK_PIZ_LENS_SIZE_ON_LENS_ZOOM",
            "WK_PIZ_LENS_BORDER_COLOR",
            "WK_PIZ_LENS_BORDER_SIZE",
            "WK_PIZ_WINDOW_HEIGHT",
            "WK_PIZ_WINDOW_WIDTH",
            "WK_PIZ_WINDOW_BORDER_COLOR",
            "WK_PIZ_WINDOW_BORDER_SIZE",
            "WK_PIZ_ZOOM_POSITION"
    );

    public function __construct()
    {
        $this->name = 'wkproductimagezoom';
        $this->author = 'Webkul';
        $this->tab = 'front_office_features';
        $this->version = '4.0.1';
        $this->module_key = '14078deaa71eb7769dc3e879e7d08f06';
        $this->bootstrap = true;
        $this->ps_versions_compliancy = array('min'=>'1.7', 'max'=>_PS_VERSION_);
        parent::__construct();
        $this->confirmUnsinstall = $this->l('Are you sure');
        $this->displayName = $this->l('Prestashop Product Image Zoom');
        $this->description = $this->l('This module is used to zoom the image of the product.');
    }

    /**
     * Register all the hook
     *
     * @return void
     */
    public function registerModuleHook()
    {
        return $this->registerHook(array('actionFrontControllerSetMedia','actionAdminControllerSetMedia'));
    }

    /**
     * installation of the module
     *
     * @return boolean
     */
    public function install()
    {
        if (!parent::install()
            || !$this->registerModuleHook()
            || !$this->defaultConfigure()
        ) {
            return false;
        }
        return true;
    }

    /**
     * uninstall the module
     *
     * @return boolean
     */
    public function uninstall()
    {
        if (!parent::uninstall()
            || !$this->deleteConfigure()
        ) {
            return false;
        }
        return true;
    }

    /**
     *setting up the default configuration
     *
     * @return void
     */
    public function defaultConfigure()
    {
        if (!Configuration::updateValue("WK_PIZ_ZOOM_POSITION", "4")
            || !Configuration::updateValue("WK_PIZ_ZOOM_LEVEL", "4")
            || !Configuration::updateValue("WK_PIZ_LENS_TYPE", "1")
            || !Configuration::updateValue("WK_PIZ_WINDOW_BORDER_SIZE", "4")
            || !Configuration::updateValue("WK_PIZ_LENS_BORDER_SIZE", "2")
            || !Configuration::updateValue("WK_PIZ_WINDOW_HEIGHT", "450")
            || !Configuration::updateValue("WK_PIZ_WINDOW_WIDTH", "400")
        ) {
            return false;
        }
        return true;
    }

    /**
     * delete the configuration from the configuration table
     *
     * @return void
     */
    public function deleteConfigure()
    {
        foreach (self::$configVars as $configValue) {
            if (!Configuration::deleteByName($configValue)) {
                return false;
            }
        }
        return true;
    }

    /**
     * Add js file to the back office for hide and show the required fields
     *
     * @return void
     */
    public function hookActionAdminControllerSetMedia()
    {
        if ('AdminModules' == $this->context->controller->controller_name
            && $this->name == Tools::getValue('configure')
        ) {
            $this->context->controller->addJS(_MODULE_DIR_ .$this->name.'/views/js/wk_admin_product_image_zoom.js');
        }
    }


    /**
     * Add js and css to the front office
     *
     * @return void
     */
    public function hookActionFrontControllerSetMedia()
    {
        if ('product' == $this->context->controller->php_self) {
            $this->context->controller->registerStylesheet(
                'wk_product_image_zoom_css',
                'modules/'.$this->name.'/views/css/wk_product_image_zoom.css'
            );
            $this->context->controller->registerJavascript(
                'wk_product_image_zoom',
                'modules/'.$this->name.'/views/js/wk_product_image_zoom.js'
            );
            $jsVars = array();
            foreach (self::$configVars as $value) {
                $jsVars[Tools::strtolower($value)] = Configuration::get($value);
            }
            Media::addJsDef(
                $jsVars
            );
        }
    }

    /**
     * saving the confguration page
     *
     * @return void
     */
    public function getContent()
    {
        if (Tools::getValue('submit')) {
            if ($this->validateAndSaveConfigValue()) {
                Tools::redirectAdmin(
                    $this->context->link->getAdminLink(
                        'AdminModules',
                        true,
                        array(),
                        array(
                            'conf' => 4,
                            'configure' => $this->name,
                            'token' => Tools::getAdminTokenLite('AdminModules')
                        )
                    )
                );
            }
        }
        return $this->renderForm();
    }

    /**
     * Validating and save the config value.
     *
     * @return boolean
     */
    public function validateAndSaveConfigValue()
    {
        if (!trim(Tools::getValue('WK_PIZ_LENS_BORDER_SIZE'))) {
            $this->context->controller->errors[] = $this->l('Please enter the  width of the border lens.');
        } elseif (!is_numeric(Tools::getValue('WK_PIZ_LENS_BORDER_SIZE'))
           || !((Tools::getValue('WK_PIZ_LENS_BORDER_SIZE') > 0) && (Tools::getValue('WK_PIZ_LENS_BORDER_SIZE') < 10))
        ) {
            $this->context->controller->errors[] =
            $this->l('Border width of the lens must greater than 0 and less than 10px.');
        }
        if (!trim(Tools::getValue('WK_PIZ_WINDOW_HEIGHT'))) {
            $this->context->controller->errors[] = $this->l('Please enter the height of the zoom window.');
        } elseif (!is_numeric(Tools::getValue('WK_PIZ_WINDOW_HEIGHT'))
            || !((Tools::getValue('WK_PIZ_WINDOW_HEIGHT') > 0) && (Tools::getValue('WK_PIZ_WINDOW_HEIGHT') < 600))
        ) {
            $this->context->controller->errors[] =
            $this->l('Height of the window must be greater than 0 and less than 600');
        }
        if (!trim(Tools::getValue('WK_PIZ_WINDOW_WIDTH'))) {
            $this->context->controller->errors[] = $this->l('Please enter the width of the zoom window.');
        } elseif (!is_numeric(Tools::getValue('WK_PIZ_WINDOW_WIDTH'))
            || !((Tools::getValue('WK_PIZ_WINDOW_WIDTH') > 0) && (Tools::getValue('WK_PIZ_WINDOW_WIDTH') < 600))
        ) {
            $this->context->controller->errors[] =
            $this->l('Width of the zoom window must be greater than 0 and less than 600');
        }
        if (!trim(Tools::getValue('WK_PIZ_WINDOW_BORDER_SIZE'))) {
            $this->context->controller->errors[] = $this->l('Please enter width of the border zoom window.');
        } elseif (!is_numeric(Tools::getValue('WK_PIZ_WINDOW_BORDER_SIZE'))
           || !(
               (Tools::getValue('WK_PIZ_WINDOW_BORDER_SIZE') > 0)
                && (Tools::getValue('WK_PIZ_WINDOW_BORDER_SIZE') < 10)
           )
        ) {
            $this->context->controller->errors[] =
            $this->l('Border width of the zoom window must be greater than 0 and less than 10');
        }
        if (empty($this->context->controller->errors)) {
            if ($this->saveConfigFormValues()) {
                return true;
            } else {
                return false;
            }
        }
    }

    /**
     * genrating the form
     *
     * @return void
     */
    protected function renderForm()
    {
        $helper = new HelperForm();
        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $helper->module = $this;
        $helper->default_form_language = $this->context->language->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG', 0);
        $helper->identifier = $this->identifier;
        $helper->submit_action = 'submit';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false)
        .'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->tpl_vars = array(
            'fields_value' => $this->getConfigFormValues(), /* Add values for your inputs */
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id,
        );
        return $helper->generateForm(array($this->getConfigForm()));
    }

    /**
     * return the saved form values
     *
     * @return void
     */
    protected function getConfigFormValues()
    {
        $returnArr = array();
        foreach (self::$configVars as $value) {
            $returnArr[$value] = Configuration::get($value);
        }
        return ($returnArr);
    }

    /**
     * generate the form fields
     *
     * @return void
     */
    protected function getConfigForm()
    {
        return array(
            'form' => array(
                'legend' => array(
                'title' => $this->l('Configuration Product Image Zoom'),
                'icon' => 'icon-cogs',
                ),
                'input' => array(

                    array(
                        'type' => 'select',
                        'label' => $this->l('Zoom position'),
                        'name' => 'WK_PIZ_ZOOM_POSITION',
                        'class'=>'input fixed-width-lg',
                        'options' => array(
                            'query' => array(
                                array('id' => '1', 'value' => $this->l('On image itself')),
                                array('id' => '2', 'value' => $this->l('On lens itself')),
                                array('id' => '3', 'value' => $this->l('Bottom of the image')),
                                array('id' => '4', 'value' => $this->l('Right of the image')),

                            ),
                            'id' => 'id',
                            'name' => 'value',
                        ),
                    ),

                    array(
                        'type' => 'select',
                        'label' => $this->l('Lens type'),
                        'name' => 'WK_PIZ_LENS_TYPE',
                        'class'=>'input fixed-width-lg wk_lens_option',
                        'options' => array(
                            'query' => array(
                                array('id' => '1', 'value' => $this->l('Circle')),
                                array('id' => '2', 'value' => $this->l('Square')),
                                array('id' => '3', 'value' => $this->l('Rounded border')),
                            ),
                            'id' => 'id',
                            'name' => 'value',
                        ),
                    ),

                    array(
                        'type' => 'select',
                        'label' => $this->l('Zoom level'),
                        'name' => 'WK_PIZ_ZOOM_LEVEL',
                        'options' => array(
                            'query' => array(
                                array('id' => '1', 'value' => $this->l('1x')),
                                array('id' => '2', 'value' => $this->l('2x')),
                                array('id' => '3', 'value' => $this->l('3x')),
                                array('id' => '4', 'value' => $this->l('4x')),
                                array('id' => '5', 'value' => $this->l('5x')),
                                array('id' => '6', 'value' => $this->l('6x')),
                                array('id' => '7', 'value' => $this->l('7x')),
                                array('id' => '8', 'value' => $this->l('8x')),
                            ),
                            'id' => 'id',
                            'name' => 'value',
                        ),
                    ),

                    array(
                        'type' => 'select',
                        'label' => $this->l('Lens Size'),
                        'name' => 'WK_PIZ_LENS_SIZE_ON_LENS_ZOOM',
                        'class'=>'input fixed-width-lg wk_zoom_over_lens',
                        'options' => array(
                            'query' => array(
                                array('id' => '1', 'value' => $this->l('1x')),
                                array('id' => '2', 'value' => $this->l('2x')),
                                array('id' => '3', 'value' => $this->l('3x')),
                                array('id' => '4', 'value' => $this->l('4x')),
                                array('id' => '5', 'value' => $this->l('5x')),
                                array('id' => '6', 'value' => $this->l('6x')),
                                array('id' => '7', 'value' => $this->l('7x')),
                                array('id' => '8', 'value' => $this->l('8x')),
                            ),
                            'id' => 'id',
                            'name' => 'value',
                        ),
                    ),

                      array(
                        'type' => 'text',
                        'name' => 'WK_PIZ_LENS_BORDER_SIZE',
                        'label' => $this->l('Lens border width'),
                        'class'=>'input fixed-width-lg wk_lens_option',
                        'hint' => $this->l('Border width should be greater than 0 and less than 10px so that it would look proper on the image.'),
                        'required' => 'true',
                        'suffix' => 'px'
                    ),
                    array(
                        'type' => 'color',
                        'label' => $this->l('Lens border color'),
                        'name' => 'WK_PIZ_LENS_BORDER_COLOR',
                        'class' => 'wk_lens_option_color',
                    ),

                    array(
                        'type' => 'text',
                        'name' => 'WK_PIZ_WINDOW_WIDTH',
                        'label' => $this->l('Zoom window width'),
                        'class'=>'input fixed-width-lg wk_window_option',
                        'hint' => $this->l('Window width must be greater than 0 and less than 600'),
                        'required' => 'true',
                        'suffix' => 'px'
                    ),


                    array(
                        'type' => 'text',
                        'name' => 'WK_PIZ_WINDOW_HEIGHT',
                        'label' => $this->l('Zoom window height'),
                        'class'=>'input fixed-width-lg wk_window_option',
                        'hint' => $this->l('Window height must be greater than 0 and less than 600'),
                        'required' => 'true',
                        'suffix' => 'px'
                    ),

                    array(
                        'type' => 'text',
                        'name' => 'WK_PIZ_WINDOW_BORDER_SIZE',
                        'label' => $this->l('Zoom window border width'),
                        'class'=>'input fixed-width-lg wk_window_option',
                        'hint' => $this->l('Border width of the window must be greater than 0 and less than 10'),
                        'required' => 'true',
                        'suffix' => 'px'
                    ),

                    array(
                        'type' => 'color',
                        'label' => $this->l('Zoom window border color'),
                        'name' => 'WK_PIZ_WINDOW_BORDER_COLOR',
                        'class' => 'wk_window_option_color',
                    ),


                ),
                'submit' => array(
                    'title' => $this->l('Save'),
                ),
            ),
        );
    }

    /**
     * save the form value
     *
     * @return void
     */
    public function saveConfigFormValues()
    {
        foreach (self::$configVars as $value) {
            if (!Configuration::updateValue($value, Tools::getValue($value))) {
                $this->context->controller->errors[] = $this->l('Something went wrong while saving the configuration');
            }
        }
        if (!empty($this->context->controller->errors)) {
            return false;
        }
        return true;
    }
}
