<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{wkproductimagezoom}prestashop>wkproductimagezoom_868e1daafcda0d0e5ae7f68a2b403e77'] = 'Уверены ли вы';
$_MODULE['<{wkproductimagezoom}prestashop>wkproductimagezoom_52b0022f7be596ae0e813105a018073b'] = 'Prestashop Изображение Увеличить';
$_MODULE['<{wkproductimagezoom}prestashop>wkproductimagezoom_07cac47a82a00b91e26e7ba309d5b5d1'] = 'Этот модуль используется для увеличения изображения продукта.';
$_MODULE['<{wkproductimagezoom}prestashop>wkproductimagezoom_c13fe51a15e3878dcb48554bb79c7ab4'] = 'Пожалуйста, введите ширину границы линзы.';
$_MODULE['<{wkproductimagezoom}prestashop>wkproductimagezoom_51874428d7fb8aa2d229dc6c8e02a099'] = 'Ширина границы линза должна больше 0 и меньше, чем 10px.';
$_MODULE['<{wkproductimagezoom}prestashop>wkproductimagezoom_91834bf43d95f49332826dcc32147883'] = 'Пожалуйста, введите высоту окна масштабирования.';
$_MODULE['<{wkproductimagezoom}prestashop>wkproductimagezoom_d4ecf91dfd0fbf121e44e9157fd3f23d'] = 'Высота окна должна быть больше, чем 0 и меньше чем 600';
$_MODULE['<{wkproductimagezoom}prestashop>wkproductimagezoom_c6142ab1483184fb7e003a5fbf846609'] = 'Пожалуйста, введите ширину окна масштабирования.';
$_MODULE['<{wkproductimagezoom}prestashop>wkproductimagezoom_9265d2d1902fabfaf5909ae35180f8ee'] = 'Ширина окна масштабирования должна быть больше 0 и меньше, чем 600';
$_MODULE['<{wkproductimagezoom}prestashop>wkproductimagezoom_60c2bdce77b4a68a1a761e7d0dd4a4db'] = 'Пожалуйста, введите ширину окна границы масштабирования.';
$_MODULE['<{wkproductimagezoom}prestashop>wkproductimagezoom_7462ab235ddec99a3c94d59815333f05'] = 'Ширина рамки окна масштабирования должна быть больше, чем 0, и меньше, чем 10';
$_MODULE['<{wkproductimagezoom}prestashop>wkproductimagezoom_bb97523aa2399ca0c94157188b7b3c4a'] = 'Конфигурация продукта Image Zoom';
$_MODULE['<{wkproductimagezoom}prestashop>wkproductimagezoom_e2d34a0b91dc7f540cd9be0146758f2d'] = 'положение трансфокатора';
$_MODULE['<{wkproductimagezoom}prestashop>wkproductimagezoom_ac49153841a9b341b7c47ce52d348910'] = 'На самом изображении';
$_MODULE['<{wkproductimagezoom}prestashop>wkproductimagezoom_4460fa23cfcb5bd30972ebd0c9a35c8a'] = 'На самом объективе';
$_MODULE['<{wkproductimagezoom}prestashop>wkproductimagezoom_49f23cd965a12b93246c7b5edb648c00'] = 'Нижняя часть изображения';
$_MODULE['<{wkproductimagezoom}prestashop>wkproductimagezoom_e4bb9768ac452f1c6bbe1c0c3bab2bd3'] = 'Право на изображение';
$_MODULE['<{wkproductimagezoom}prestashop>wkproductimagezoom_e5d1165b3fb5fa201cb66e9889b083c1'] = 'Тип объектива';
$_MODULE['<{wkproductimagezoom}prestashop>wkproductimagezoom_30954d90085f6eaaf5817917fc5fecb3'] = 'Круг';
$_MODULE['<{wkproductimagezoom}prestashop>wkproductimagezoom_ceb46ca115d05c51aa5a16a8867c3304'] = 'Площадь';
$_MODULE['<{wkproductimagezoom}prestashop>wkproductimagezoom_1a88ea8fa4dfd6eda9e90bc11e9f0b9a'] = 'Округлые границы';
$_MODULE['<{wkproductimagezoom}prestashop>wkproductimagezoom_e1595dcb20638616f6a850af3904f996'] = 'уровень масштабирования';
$_MODULE['<{wkproductimagezoom}prestashop>wkproductimagezoom_38684612f0c6bb6dfa16da92f4a6878f'] = '1x';
$_MODULE['<{wkproductimagezoom}prestashop>wkproductimagezoom_2204c2b4d60ae2fd5279ec54cfaed2a4'] = '2x';
$_MODULE['<{wkproductimagezoom}prestashop>wkproductimagezoom_c68ebab7a1e74618506a0a1fabe54186'] = '3x';
$_MODULE['<{wkproductimagezoom}prestashop>wkproductimagezoom_7d2ec3b03bb065827164eb47cc7dc37b'] = '4x';
$_MODULE['<{wkproductimagezoom}prestashop>wkproductimagezoom_97e2f596ffc9211fecfee155ad18d643'] = '5x';
$_MODULE['<{wkproductimagezoom}prestashop>wkproductimagezoom_c3caf6ae1153e916acfcba386418dcf7'] = '6х';
$_MODULE['<{wkproductimagezoom}prestashop>wkproductimagezoom_8a1b0284de7d7f43f74ff50788b58df6'] = '7x';
$_MODULE['<{wkproductimagezoom}prestashop>wkproductimagezoom_22d3c538ae9a7893a8cca2d17abd46ce'] = '8х';
$_MODULE['<{wkproductimagezoom}prestashop>wkproductimagezoom_b06b7e8009881a7267241491e59c927b'] = 'Размер объектива';
$_MODULE['<{wkproductimagezoom}prestashop>wkproductimagezoom_48c19ab4dbfe184c339ef14e1ec4ea71'] = 'Объектив Ширина границы';
$_MODULE['<{wkproductimagezoom}prestashop>wkproductimagezoom_04fd46b49eb5a7714c578bf5a66e4729'] = 'Ширина границы должно быть больше 0 и меньше 10px так, что она будет выглядеть собственно на изображении.';
$_MODULE['<{wkproductimagezoom}prestashop>wkproductimagezoom_dc82e101d9daef52a8601f5ca582066a'] = 'Объектив цвет границы';
$_MODULE['<{wkproductimagezoom}prestashop>wkproductimagezoom_a3de72415573502ab5921b557df8aec9'] = 'Ширина окна Увеличить';
$_MODULE['<{wkproductimagezoom}prestashop>wkproductimagezoom_74cbd64563209338a987971116d81b5d'] = 'Ширина окна должна быть больше, чем 0 и меньше чем 600';
$_MODULE['<{wkproductimagezoom}prestashop>wkproductimagezoom_05c95b404542bea0fc0490eb079496c4'] = 'Высота окна Увеличить';
$_MODULE['<{wkproductimagezoom}prestashop>wkproductimagezoom_87f465d78fc006c4b440f15275623c53'] = 'Высота окна должна быть больше, чем 0 и меньше чем 600';
$_MODULE['<{wkproductimagezoom}prestashop>wkproductimagezoom_42f30f42b91e4270584777810b71db98'] = 'Увеличение ширины границы окна';
$_MODULE['<{wkproductimagezoom}prestashop>wkproductimagezoom_67685b9668faae536ee5434919d55e5d'] = 'Ширина границы окна должна быть больше, чем 0, и меньше, чем 10';
$_MODULE['<{wkproductimagezoom}prestashop>wkproductimagezoom_27b24a3823eb52675792a93ccffcd1fa'] = 'Увеличить окно цвет границы';
$_MODULE['<{wkproductimagezoom}prestashop>wkproductimagezoom_c9cc8cce247e49bae79f15173ce97354'] = 'Сохранить';
$_MODULE['<{wkproductimagezoom}prestashop>wkproductimagezoom_7f2f2f7fff5ed31ab8716bd8562874e3'] = 'Что-то пошло не так при сохранении конфигурации';
