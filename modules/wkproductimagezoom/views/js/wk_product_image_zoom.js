/**
* 2010-2021 Webkul.
*
* NOTICE OF LICENSE
*
* All right is reserved,
* Please go through LICENSE.txt file inside our module
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade this module to newer
* versions in the future. If you wish to customize this module for your
* needs please refer to CustomizationPolicy.txt file inside our module for more information.
*
* @author Webkul IN
* @copyright 2010-2021 Webkul IN
* @license LICENSE.txt
*/

$(document).ready(function () {
    (wk_piz_zoom_position);
    if ("1" != wk_piz_zoom_position && "2" != wk_piz_zoom_position) {
        appendZoomWindow();
    }
    initialSetting();
    // it reload the function after any js called.
    $(document).ajaxComplete(function (event, xhr, settings) {
        if ("1" != wk_piz_zoom_position && "2" != wk_piz_zoom_position) {
            appendZoomWindow();
        }
        if (typeof settings.url !== 'undefined') {
            var ajax_data = settings.url;
            var ajax_id_product = ajax_data.match('id_product');
            var ajax_id_customization = ajax_data.match('id_customization');
            if (( ajax_id_product !== null) && ( ajax_id_customization !== null)) {
                initialSetting();
            }
        }
    });

    function initialSetting() {
        // hide the layer on the image
        $('.layer').hide();
        // append the div for the zoom image
        wk_source_img = document.getElementsByClassName("js-qv-product-cover");
        wk_source_img = wk_source_img[0];
        if (wk_source_img) {
            if ("1" == wk_piz_zoom_position) {
                imageZoomOver();
            }
            else {
                imageZoomBeside("js-qv-product-cover", "wk-zoom-result");
            }
        }
    }

    function appendZoomWindow() {
        if ("3" == wk_piz_zoom_position) {
            $('.product-cover').append('<div class = "wk-zoom-result"> </div>');
        }

        if ("4" == wk_piz_zoom_position) {
            $('.product-cover').append('<div class = "wk-zoom-result"> </div>');
            $('.product-cover').css({ "display": "-webkit-inline-box" });
        }
    }

    // zoom over the image itself
    function imageZoomOver() {
        $('.product-cover')
        .css('overflow', 'hidden')
        .on('mouseover', function () {
            $(this).children('.js-qv-product-cover').css({ 'transform': 'scale(' + setLensPropertyForZoomOver() + ')' });
        })
        .on('mouseout', function () {
            $(this).children('.js-qv-product-cover').css({ 'transform': 'scale(1)' });
        })
        .on('mousemove', function (e) {
            $(this).children('.js-qv-product-cover').css({ 'transform-origin': ((e.pageX - $(this).offset().left) / $(this).width()) * 100 + '% ' + ((e.pageY - $(this).offset().top) / $(this).height()) * 100 + '%' });
        })
    }

    // funciton to return the zoom level
    function setLensPropertyForZoomOver() {
        switch (wk_piz_zoom_level) {
            case "1":
                return 1.2;
            case "2":
                return 1.4;
            case "3":
                return 1.6;
            case "4":
                return 1.8;
            case "5":
                return 2;
            case "6":
                return 2.2;
            case "7":
                return 2.4;
            case "8":
                return 2.6;
        }
    }

    // zoom beside the image inside the div
    function imageZoomBeside(imageClassName, resultWindowClassName) {

        var wk_source_img, lens, wk_result_img, cx, cy;
        // setResultZoomProperty();
        wk_source_img = document.getElementsByClassName(imageClassName);
        wk_source_img = wk_source_img[0];

        /*Dynamically creation of the lens*/
        lens = document.createElement("div");
        lens.setAttribute("class", "wk-img-zoom-lens");
        /*insert the lens in the above the image to be zoomed*/
        wk_source_img.parentElement.insertBefore(lens, wk_source_img);
        if ("2" == wk_piz_zoom_position)
        {
            $('.wk-img-zoom-lens').append('<div class = "wk-zoom-result"> </div>');
            wk_result_img = document.getElementsByClassName("wk-zoom-result");
        }
        else {
            wk_result_img = document.getElementsByClassName(resultWindowClassName);
        }
        wk_result_img = wk_result_img[0];
        if ("2" == wk_piz_zoom_position) {
            setLensPropertyForZoomOnLens();
        }
        else {
            setLensPropertyForZoomSide();
        }
        setResultWindowProperty();
        cx = wk_result_img.offsetWidth / lens.offsetWidth;
        cy = wk_result_img.offsetHeight / lens.offsetHeight;

        wk_result_img.style.background = "url('" + wk_source_img.src + "')";
        wk_result_img.style.backgroundSize = (wk_source_img.width * cx) + "px " + (wk_source_img.height * cy) + "px";

        // when there is product image is changes then it will take the latest image.
        $('.js-qv-product-images').click(function () {
            wk_result_img.style.background = "url('" + wk_source_img.src + "')";
            wk_result_img.style.backgroundSize = (wk_source_img.width * cx) + "px " + (wk_source_img.height * cy) + "px";
        });

        $('img.thumb.js-thumb').click(function () {
            var lien=$(this).attr('data-image-large-src');
            $('.wk-zoom-result').css('background','url("'+lien+'")');
            wk_result_img.style.backgroundSize = (wk_source_img.width * cx) + "px " + (wk_source_img.height * cy) + "px";
            
                    });
                    
        // one first time load of the page hide the div of the lens and zoom.
        hideZoomElements();
        $(".product-cover").mouseover(function () {
            showZoomElements();
            lens.addEventListener("mousemove", onLensMove);
            wk_source_img.addEventListener("mousemove", onLensMove);
            lens.addEventListener("touchmove", onLensMove);
            wk_source_img.addEventListener("touchmove", onLensMove);
        });
        $(".product-cover").mouseleave(function () {
            hideZoomElements();
        });

        function showZoomElements() {
            $('.wk-zoom-result').show();
            $('.wk-img-zoom-lens').show();
        }

        function hideZoomElements() {
            $('.wk-zoom-result').css('display', 'none');
            $('.wk-img-zoom-lens').css('display', 'none');
        }

        function onLensMove(e) {
            var pos, x, y;
            e.preventDefault();
            pos = cursorPosition(e);
            x = pos.x - (lens.offsetWidth / 2);
            y = pos.y - (lens.offsetHeight / 2);
            if (x > wk_source_img.width - lens.offsetWidth) { x = wk_source_img.width - lens.offsetWidth; }
            if (x < 0) { x = 0; }
            if (y > wk_source_img.height - lens.offsetHeight) { y = wk_source_img.height - lens.offsetHeight; }
            if (y < 0) { y = 0; }
            lens.style.left = x + "px";
            lens.style.top = y + "px";
            wk_result_img.style.backgroundPosition = "-" + (x * cx) + "px -" + (y * cy) + "px";
        }

        function cursorPosition(e) {
            var a, x = 0, y = 0;
            e = e || window.event;
            a = wk_source_img.getBoundingClientRect();
            x = e.pageX - a.left;
            y = e.pageY - a.top;
            x = x - window.pageXOffset;
            y = y - window.pageYOffset;
            return { x: x, y: y };
        }

        function setLensPropertyForZoomSide() {
            switch (wk_piz_zoom_level) {
                case "1":
                    lens_size = "300px";
                    break;
                case "2":
                    lens_size = "250px";
                    break;
                case "3":
                    lens_size = "200px";
                    break;
                case "4":
                    lens_size = "150px";
                    break;
                case "5":
                    lens_size = "100px";
                    break;
                case "6":
                    lens_size = "60px";
                    break;
                case "7":
                    lens_size = "40px";
                    break;
                case "8":
                    lens_size = "20px";
                    break;
            }

            if ("1" == wk_piz_lens_type) {
                var border_raduis = "50%";
            }
            else if ("3" == wk_piz_lens_type) {
                var border_raduis = "20%";
            } else {
                var border_radius = "0px";
            }
            if (!wk_piz_lens_border_size) {
                wk_piz_lens_border_size = "2";
            }
            $('.wk-img-zoom-lens').css({
                "border-style": "solid",
                "border-color": wk_piz_lens_border_color,
                "border-width": wk_piz_lens_border_size + "px",
                "border-radius": border_raduis,
                "height": lens_size,
                "width": lens_size,
            });
        }

        function setLensPropertyForZoomOnLens()
        {
            switch (wk_piz_lens_size_on_lens_zoom) {
                case "1":
                    lens_size = "20px";
                    break;
                case "2":
                    lens_size = "40px";
                    break;
                case "3":
                    lens_size = "60px";
                    break;
                case "4":
                    lens_size = "100px";
                break;
                case "5":
                    lens_size = "150px";
                    break;
                case "6":
                    lens_size = "200px";
                    break;
                case "7":
                    lens_size = "250px";
                    break;
                case "8":
                    lens_size = "300px";
                    break;
            }
            $('.wk-img-zoom-lens').css({
                "height": lens_size,
                "width": lens_size,
            });
        }

        function setResultWindowProperty() {

            if ("1" != wk_piz_zoom_position && "2" != wk_piz_zoom_position)
            {
                if (!wk_piz_window_height || !wk_piz_window_width) {
                    wk_piz_window_height = "200px";
                    wk_piz_window_width = "200px";
                }

                if (!wk_piz_window_border_size) {
                    wk_piz_window_border_size = "2";
                }

                $('.wk-zoom-result').css({
                    "margin": "auto",
                    "z-index": "3",
                    "border-style": "solid",
                    "border-color": wk_piz_window_border_color,
                    "border-width": wk_piz_window_border_size + "px",
                    "height": wk_piz_window_height + "px",
                    "width": wk_piz_window_width + "px",
                });
            }

            if ("2" == wk_piz_zoom_position)
            {
                if ("1" == wk_piz_lens_type) {
                    var border_raduis = "50%";
                }
                else if ("3" == wk_piz_lens_type) {
                    var border_raduis = "20%";
                } else {
                    var border_radius = "0px";
                }
                $('.wk-zoom-result').css({
                    'transform': "scale("+setLensPropertyForZoomOver()+")",
                    'position': 'absolute',
                    "margin": "auto",
                    "z-index": "3",
                    "border-style": "solid",
                    "border-color": wk_piz_lens_border_color,
                    "border-width": wk_piz_lens_border_size + "px",
                    "border-radius": border_raduis,
                    "height": lens_size,
                    "width": lens_size,
                });
            }
        }
    }
});
