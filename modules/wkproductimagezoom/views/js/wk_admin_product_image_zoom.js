/**
* 2010-2021 Webkul.
*
* NOTICE OF LICENSE
*
* All right is reserved,
* Please go through LICENSE.txt file inside our module
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade this module to newer
* versions in the future. If you wish to customize this module for your
* needs please refer to CustomizationPolicy.txt file inside our module for more information.
*
* @author Webkul IN
* @copyright 2010-2021 Webkul IN
* @license LICENSE.txt
*/

$(document).ready(function () {
    if($('#WK_PIZ_ZOOM_POSITION').val() == 1) {
        $('.wk_lens_option').closest('.form-group').hide();
        $('.wk_zoom_over_lens').closest('.form-group').hide();
        $('.wk_lens_option_color').closest('.form-group').parent().parent().hide();
        $('.wk_window_option').closest('.form-group').hide();
        $('.wk_window_option_color').closest('.form-group').parent().parent().hide();
    }

    else if ($('#WK_PIZ_ZOOM_POSITION').val() == 2) {
        $('.wk_window_option').closest('.form-group').hide();
        $('.wk_window_option_color').closest('.form-group').parent().parent().hide();
        $('.wk_zoom_over_lens').closest('.form-group').show();
    }
    else {
        $('.wk_zoom_over_lens').closest('.form-group').hide();
    }

    $('#WK_PIZ_ZOOM_POSITION').change(function () {
        $('.wk_lens_option').closest('.form-group').show();
        $('.wk_lens_option_color').closest('.form-group').parent().parent().show();
        $('.wk_window_option').closest('.form-group').show();
        $('.wk_window_option_color').closest('.form-group').parent().parent().show();
        $('.wk_zoom_over_lens').closest('.form-group').show();
        $('.wk_zoom_over_lens').closest('.form-group').hide();


        if ($('#WK_PIZ_ZOOM_POSITION').val() == 1) {
            $('.wk_lens_option').closest('.form-group').hide();
            $('.wk_zoom_over_lens').closest('.form-group').hide();
            $('.wk_lens_option_color').closest('.form-group').parent().parent().hide();
            $('.wk_window_option').closest('.form-group').hide();
            $('.wk_window_option_color').closest('.form-group').parent().parent().hide();
        }
        else if ($('#WK_PIZ_ZOOM_POSITION').val() == 2) {
            $('.wk_window_option').closest('.form-group').hide();
            $('.wk_window_option_color').closest('.form-group').parent().parent().hide();
            $('.wk_zoom_over_lens').closest('.form-group').show();

        }
    })
});
