{*
* 2007-2019 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2019 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

<section id="reassurance_block">
    <section class="category_select row">
        <div class="col-lg-2 active"><i class="material-icons" data-id="1">mood</i></div>
        <div class="col-lg-2"><i class="material-icons" data-id="2">local_shipping</i></div>
        <div class="col-lg-2"><i class="material-icons" data-id="3">loop</i></div>
    </section>
    <section class="category_reassurance cat_1 active">
        <div class="row">
            <div class="col-lg-4"><img src="{$img_url}/templates/reassurance/pack1/headset.png"/></div>
            <div class="col-lg-4"><img src="{$img_url}/templates/reassurance/pack1/local-shipping.png"/></div>
            <div class="col-lg-4"><img src="{$img_url}/templates/reassurance/pack1/lock.png"/></div>
        </div>
        <div class="row">
            <div class="col-lg-4"><img src="{$img_url}/templates/reassurance/pack1/loop.png"/></div>
            <div class="col-lg-4"><img src="{$img_url}/templates/reassurance/pack1/loyalty.png"/></div>
            <div class="col-lg-4"><img src="{$img_url}/templates/reassurance/pack1/mood.png"/></div>
        </div>
        <div class="row">
            <div class="col-lg-4"><img src="{$img_url}/templates/reassurance/pack1/payment.png"/></div>
            <div class="col-lg-4"><img src="{$img_url}/templates/reassurance/pack1/public.png"/></div>
            <div class="col-lg-4"><img src="{$img_url}/templates/reassurance/pack1/redeem.png"/></div>
        </div>
        <div class="row">
            <div class="col-lg-4"><img src="{$img_url}/templates/reassurance/pack1/thumb.png"/></div>
            <div class="col-lg-4"><img src="{$img_url}/templates/reassurance/pack1/timer.png"/></div>
            <div class="col-lg-4"><img src="{$img_url}/templates/reassurance/pack1/touch-app.png"/></div>
        </div>
    </section>
    <section class="category_reassurance cat_2">
        <div class="row">
            <div class="col-lg-4"><img src="{$img_url}/templates/reassurance/pack2/carrier.png"/></div>
            <div class="col-lg-4"><img src="{$img_url}/templates/reassurance/pack2/creditcard.png"/></div>
            <div class="col-lg-4"><img src="{$img_url}/templates/reassurance/pack2/gift.png"/></div>
        </div>
        <div class="row">
            <div class="col-lg-4"><img src="{$img_url}/templates/reassurance/pack2/globe.png"/></div>
            <div class="col-lg-4"><img src="{$img_url}/templates/reassurance/pack2/hotline.png"/></div>
            <div class="col-lg-4"><img src="{$img_url}/templates/reassurance/pack2/parcel.png"/></div>
        </div>
        <div class="row">
            <div class="col-lg-4"><img src="{$img_url}/templates/reassurance/pack2/phone.png"/></div>
            <div class="col-lg-4"><img src="{$img_url}/templates/reassurance/pack2/return.png"/></div>
            <div class="col-lg-4"><img src="{$img_url}/templates/reassurance/pack2/satisfaction.png"/></div>
        </div>
        <div class="row">
            <div class="col-lg-4"><img src="{$img_url}/templates/reassurance/pack2/security.png"/></div>
            <div class="col-lg-4"><img src="{$img_url}/templates/reassurance/pack2/support.png"/></div>
            <div class="col-lg-4"><img src="{$img_url}/templates/reassurance/pack2/trust.png"/></div>
        </div>
    </section>
    <section class="category_reassurance cat_3">
        <div class="row">
            <div class="col-lg-4"><img src="{$img_url}/templates/reassurance/pack3/carrier.png"/></div>
            <div class="col-lg-4"><img src="{$img_url}/templates/reassurance/pack3/clock.png"/></div>
            <div class="col-lg-4"><img src="{$img_url}/templates/reassurance/pack3/comment.png"/></div>
        </div>
        <div class="row">
            <div class="col-lg-4"><img src="{$img_url}/templates/reassurance/pack3/gift.png"/></div>
            <div class="col-lg-4"><img src="{$img_url}/templates/reassurance/pack3/globe.png"/></div>
            <div class="col-lg-4"><img src="{$img_url}/templates/reassurance/pack3/location.png"/></div>
        </div>
        <div class="row">
            <div class="col-lg-4"><img src="{$img_url}/templates/reassurance/pack3/payment.png"/></div>
            <div class="col-lg-4"><img src="{$img_url}/templates/reassurance/pack3/return.png"/></div>
            <div class="col-lg-4"><img src="{$img_url}/templates/reassurance/pack3/satisfaction.png"/></div>
        </div>
        <div class="row">
            <div class="col-lg-4"><img src="{$img_url}/templates/reassurance/pack3/security.png"/></div>
            <div class="col-lg-4"><img src="{$img_url}/templates/reassurance/pack3/star.png"/></div>
            <div class="col-lg-4"><img src="{$img_url}/templates/reassurance/pack3/support.png"/></div>
        </div>
    </section>
</div>



