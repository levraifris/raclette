{*
* 2007-2019 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2019 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

<div id="older_module" class="col-xl-10 col-xl-offset-1 col-lg-12 col-lg-offset-0" >
	<div class="row alert alert-warning" role="alert">
		<h4>{l s='WARNING' mod='pscartabandonmentpro'}</h4>
		<p>{l s='This module is an all new version of your Abandoned Cart Reminder Pro Module. By installing this new module, we strongly recommend to disable your previous reminders set with Abandonned Cart Reminder Pro Module version 1.X.X to avoid spamming your customers with several reminders. To Set your new reminders with your new module version 2.X.X, you can still copy and paste your previous email content in the new template configuration panel. Enjoy your brand new module and its awesome features!' mod='pscartabandonmentpro'}</p><br/>
		<p>
			<a class="btn btn-primary" href="{$sOlderModuleURL}" target="_blank">{l s='Older module\'s page configuration' mod='pscartabandonmentpro'}</a>
		</p>
	</div>
</div>