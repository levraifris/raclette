{*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more info2rmation.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2015 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
{if $page.page_name == 'product' }
<div class="encar col-xs-12" style="margin: 20px 0; padding:0;">

    {if !$product.dwf_toutcompris}
        <div class="ttcompris">{$cms_info2s.text nofilter}<span class="material-icons">
                info
            </span></div>
        <div class="modal-ttcompris" style="display: none;">
            <div class="fond"></div>
            <div class="textmod">{$cms_info2s.text3 nofilter}
                <span class="material-icons">
                    close
                </span>
            </div>
        </div>
    {/if}

    {if $product.regular_price > 200}

        {if !$product.dwf_alma}
            <div class="alma">{$cms_info2s.text2 nofilter}<span class="material-icons">
                    info
                </span></div>
            <div class="modal-alma" style="display: none;">
                <div class="fond"></div>
                <div class="textmod">{$cms_info2s.text4 nofilter}
                    <span class="material-icons">
                        close
                    </span>
                </div>
            </div>
        {/if}
    {else}
        {if $product.dwf_alma}
            <div class="alma">{$cms_info2s.text2 nofilter}<span class="material-icons">
                    info
                </span></div>
                <div class="modal-alma" style="display: none;">
                <div class="fond"></div>
                <div class="textmod">{$cms_info2s.text4 nofilter}
                    <span class="material-icons">
                        close
                    </span>
                </div>
            </div>
        {/if}
    {/if}
</div>
    <script>
        $('.ttcompris span.material-icons').click(function() {
            $('.modal-ttcompris').show();
        });

        $('.modal-ttcompris .textmod span.material-icons').click(function() {
            $('.modal-ttcompris').hide();
        });

        $('.modal-ttcompris .fond').click(function() {
            $('.modal-ttcompris').hide();
        });

        $('.alma span.material-icons').click(function() {
            $('.modal-alma').show();
        });

        $('.modal-alma .textmod span.material-icons').click(function() {
            $('.modal-alma').hide();
        });

        $('.modal-alma .fond').click(function() {
            $('.modal-alma').hide();
        });
    </script>
{/if}