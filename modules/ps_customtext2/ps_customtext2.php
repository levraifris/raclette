<?php
/*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more info2rmation.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2015 PrestaShop SA

*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

if (!defined('_PS_VERSION_')) {
    exit;
}

use PrestaShop\PrestaShop\Core\Module\WidgetInterface;

require_once _PS_MODULE_DIR_ . 'ps_customtext2/classes/CustomText2.php';

class ps_customtext2 extends Module implements WidgetInterface
{
    // Equivalent module on PrestaShop 1.6, sharing the same data
    const MODULE_16 = 'blockcmsinfo2';

    private $templateFile;

    public function __construct()
    {
        $this->name = 'ps_customtext2';
        $this->author = 'Florian';
        $this->version = '4.1.0';
        $this->need_instance = 0;

        $this->bootstrap = true;
        parent::__construct();

        Shop::addTableAssociation('info2', array('type' => 'shop'));

        $this->displayName = 'Blocks promo';
        $this->description = 'Gestion du texte des blocks promotionnels';

        $this->ps_versions_compliancy = array('min' => '1.7.4.0', 'max' => _PS_VERSION_);

        $this->templateFile = 'module:ps_customtext2/ps_customtext2.tpl';
    }

    public function install()
    {
        // Remove 1.6 equivalent module to avoid DB issues
        if (Module::isInstalled(self::MODULE_16)) {
            return $this->installFrom16Version();
        }

        return $this->runInstallSteps()
            && $this->installFixtures();
    }

    public function runInstallSteps()
    {
        return parent::install()
            && $this->installDB()
            && $this->registerHook('displayHome')
            && $this->registerHook('actionShopDataDuplication');
    }

    public function installFrom16Version()
    {
        require_once _PS_MODULE_DIR_ . $this->name . '/classes/MigrateData.php';
        $migration = new MigrateData();
        $migration->retrieveOldData();

        $oldModule = Module::getInstanceByName(self::MODULE_16);
        if ($oldModule) {
            $oldModule->uninstall();
        }
        return $this->uninstallDB()
            && $this->runInstallSteps()
            && $migration->insertData();
    }

    public function uninstall()
    {
        return parent::uninstall() && $this->uninstallDB();
    }

    public function installDB()
    {
        $return = true;
        $return &= Db::getInstance()->execute(
            '
                CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'info2` (
                `id_info2` INT UNSIGNED NOT NULL AUTO_INCREMENT,
                PRIMARY KEY (`id_info2`)
            ) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8 ;'
        );

        $return &= Db::getInstance()->execute(
            '
                CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'info2_shop` (
                `id_info2` INT(10) UNSIGNED NOT NULL,
                `id_shop` INT(10) UNSIGNED NOT NULL,
                PRIMARY KEY (`id_info2`, `id_shop`)
            ) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8 ;'
        );

        $return &= Db::getInstance()->execute(
            '
                CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'info2_lang` (
                `id_info2` INT UNSIGNED NOT NULL,
                `id_shop` INT(10) UNSIGNED NOT NULL,
                `id_lang` INT(10) UNSIGNED NOT NULL ,
                `text` text NOT NULL,
                `text2` text NOT NULL,
                `text3` text NOT NULL,
                `text4` text NOT NULL,
                PRIMARY KEY (`id_info2`, `id_lang`, `id_shop`)
            ) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8 ;'
        );

        return $return;
    }

    public function uninstallDB($drop_table = true)
    {
        $ret = true;
        if ($drop_table) {
            $ret &= Db::getInstance()->execute('DROP TABLE IF EXISTS `' . _DB_PREFIX_ . 'info2`')
                && Db::getInstance()->execute('DROP TABLE IF EXISTS `' . _DB_PREFIX_ . 'info2_shop`')
                && Db::getInstance()->execute('DROP TABLE IF EXISTS `' . _DB_PREFIX_ . 'info2_lang`');
        }

        return $ret;
    }

    public function getContent()
    {
        $output = '';

        if (Tools::isSubmit('saveps_customtext2')) {
            if (!Tools::getValue('text_' . (int)Configuration::get('PS_LANG_DEFAULT'), false)) {
                $output = $this->displayError($this->trans('Please fill out all fields.', array(), 'Admin.Notifications.Error')) . $this->renderForm();
            } else {
                $update = $this->processSaveCustomText2();

                if (!$update) {
                    $output = '<div class="alert alert-danger conf error">'
                        . $this->trans('An error occurred on saving.', array(), 'Admin.Notifications.Error')
                        . '</div>';
                }

                $this->_clearCache($this->templateFile);
            }
        }

        return $output . $this->renderForm();
    }

    public function processSaveCustomText2()
    {
        $shops = Tools::getValue('checkBoxShopAsso_configuration', array($this->context->shop->id));
        $text = array();
        $text2 = array();
        $text3 = array();
        $text4 = array();
        $languages = Language::getLanguages(false);

        foreach ($languages as $lang) {
            $text[$lang['id_lang']] = Tools::getValue('text_' . $lang['id_lang']);
            $text2[$lang['id_lang']] = Tools::getValue('text2_' . $lang['id_lang']);
            $text3[$lang['id_lang']] = Tools::getValue('text3_' . $lang['id_lang']);
            $text4[$lang['id_lang']] = Tools::getValue('text4_' . $lang['id_lang']);
        }

        $saved = true;
        foreach ($shops as $shop) {
            Shop::setContext(Shop::CONTEXT_SHOP, $shop);
            $info2 = new CustomText2(Tools::getValue('id_info2', 1));
            $info2->text = $text;
            $info2->text2 = $text2;
            $info2->text3 = $text3;
            $info2->text4 = $text4;

            $saved &= $info2->save();
        }

        return $saved;
    }

    protected function renderForm()
    {
        $default_lang = (int)Configuration::get('PS_LANG_DEFAULT');

        $fields_form = array(
            'tinymce' => true,
            'legend' => array(
                'title' => $this->trans('CMS block', array(), 'Modules.Customtext.Admin'),
            ),
            
                'input' => array(
                    'id_info2' => array(
                        'type' => 'hidden',
                        'name' => 'id_info2'
                    ),
                    array(
                        'type' => 'textarea',
                        'label' => $this->trans('Text block', array(), 'Modules.Customtext.Admin'),
                        'lang' => true,
                        'name' => 'text',
                        'cols' => 40,
                        'rows' => 10,
                        'class' => 'rte',
                        'autoload_rte' => true,
                    ),
                    array(
                        'type' => 'textarea',
                        'label' => 'a',
                        'lang' => true,
                        'name' => 'text2',
                        'cols' => 40,
                        'rows' => 10,
                        'class' => 'rte',
                        'autoload_rte' => true,
                    ),
                    array(
                        'type' => 'textarea',
                        'label' => 'b',
                        'lang' => true,
                        'name' => 'text3',
                        'cols' => 40,
                        'rows' => 10,
                        'class' => 'rte',
                        'autoload_rte' => true,
                    ),
                    array(
                        'type' => 'textarea',
                        'label' => 'c',
                        'lang' => true,
                        'name' => 'text4',
                        'cols' => 40,
                        'rows' => 10,
                        'class' => 'rte',
                        'autoload_rte' => true,
                    ),
                ),
            
            'submit' => array(
                'title' => $this->trans('Save', array(), 'Admin.Actions'),
            ),
            'buttons' => array(
                array(
                    'href' => AdminController::$currentIndex . '&configure=' . $this->name . '&token=' . Tools::getAdminTokenLite('AdminModules'),
                    'title' => $this->trans('Back to list', array(), 'Admin.Actions'),
                    'icon' => 'process-icon-back'
                )
            )
        );

        if (Shop::isFeatureActive() && Tools::getValue('id_info2') == false) {
            $fields_form['input'][] = array(
                'type' => 'shop',
                'label' => $this->trans('Shop association', array(), 'Admin.Global'),
                'name' => 'checkBoxShopAsso_theme'
            );
        }


        $helper = new HelperForm();
        $helper->module = $this;
        $helper->name_controller = 'ps_customtext2';
        $helper->identifier = $this->identifier;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        foreach (Language::getLanguages(false) as $lang) {
            $helper->languages[] = array(
                'id_lang' => $lang['id_lang'],
                'iso_code' => $lang['iso_code'],
                'name' => $lang['name'],
                'is_default' => ($default_lang == $lang['id_lang'] ? 1 : 0)
            );
        }

        $helper->currentIndex = AdminController::$currentIndex . '&configure=' . $this->name;
        $helper->default_form_language = $default_lang;
        $helper->allow_employee_form_lang = $default_lang;
        $helper->toolbar_scroll = true;
        $helper->title = $this->displayName;
        $helper->submit_action = 'saveps_customtext2';


        $helper->fields_value = $this->getFormValues();
        // return $helper->generateForm(array($fields_form));

        return $helper->generateForm(array(array('form' => $fields_form)));
    }

    public function getFormValues()
    {
        $fields_form = array();
        $idShop = $this->context->shop->id;
        $idinfo2 = CustomText2::getCustomTextIdByShop($idShop);

        Shop::setContext(Shop::CONTEXT_SHOP, $idShop);
        $info2 = new CustomText2((int)$idinfo2);

        $fields_form['text'] = $info2->text;
        $fields_form['text2'] = $info2->text2;
        $fields_form['text3'] = $info2->text3;
        $fields_form['text4'] = $info2->text4;

        $fields_form['id_info2'] = $idinfo2;

        return $fields_form;
    }

    public function renderWidget($hookName = null, array $configuration = [])
    {
        if (!$this->isCached($this->templateFile, $this->getCacheId('ps_customtext2'))) {
            $this->smarty->assign($this->getWidgetVariables($hookName, $configuration));
        }

        return $this->fetch($this->templateFile, $this->getCacheId('ps_customtext2'));
    }
    public function getWidgetVariables($hookName = null, array $configuration = [])
    {
        $sql = 'SELECT * FROM `' . _DB_PREFIX_ . 'info2_lang`
            WHERE `id_lang` = ' . (int)$this->context->language->id . ' AND  `id_shop` = ' . (int)$this->context->shop->id;

        return array(
            'cms_info2s' => Db::getInstance()->getRow($sql),
        );
    }

    public function installFixtures()
    {
        $return = true;
        $tabTexts = array(
            array(
                'text' => '<h2>Custom Text Block</h2>
<p><strong class="dark">Lorem ipsum dolor sit amet conse ctetu</strong></p>
<p>Sit amet conse ctetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit.</p>'
            ),
        );

        $shopsIds = Shop::getShops(true, null, true);
        $languages = Language::getLanguages(false);
        $text = array();
        $text2 = array();
        $text3 = array();
        $text4 = array();

        foreach ($tabTexts as $tab) {
            $info2 = new CustomText2();
            foreach ($languages as $lang) {
                $text[$lang['id_lang']] = $tab['text'];
                $text2[$lang['id_lang']] = $tab['text2'];
                $text4[$lang['id_lang']] = $tab['text4'];
                $text3[$lang['id_lang']] = $tab['text3'];
            }
            $info2->text = $text;
            $info2->text2 = $text2;
            $info2->text3 = $text3;
            $info2->text4 = $text4;

            $return &= $info2->add();
        }

        if ($return && sizeof($shopsIds) > 1) {
            foreach ($shopsIds as $idShop) {
                Shop::setContext(Shop::CONTEXT_SHOP, $idShop);
                $info2->text = $text;
                $info2->text2 = $text2;
                $info2->text3 = $text3;
                $info2->text4 = $text4;
                $return &= $info2->save();
            }
        }

        return $return;
    }

    /**
     * Add CustomText2 when adding a new Shop
     *
     * @param array $params
     */
    public function hookActionShopDataDuplication($params)
    {
        if ($info2Id = CustomText2::getCustomTextIdByShop($params['old_id_shop'])) {
            Shop::setContext(Shop::CONTEXT_SHOP, $params['old_id_shop']);
            $oldinfo2 = new CustomText2($info2Id);

            Shop::setContext(Shop::CONTEXT_SHOP, $params['new_id_shop']);
            $newinfo2 = new CustomText2($info2Id, null, $params['new_id_shop']);
            $newinfo2->text = $oldinfo2->text;
            $newinfo2->text2 = $oldinfo2->text2;
            $newinfo2->text3 = $oldinfo2->text3;
            $newinfo2->text4 = $oldinfo2->text4;

            $newinfo2->save();
        }
    }
}
