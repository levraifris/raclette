<?php
/**
 * Social Login
 *
 * @author    BusinessTech.fr - https://www.businesstech.fr
 * @copyright Business Tech 2020 - https://www.businesstech.fr
 * @license   Commercial
 *
 *           ____    _______
 *          |  _ \  |__   __|
 *          | |_) |    | |
 *          |  _ <     | |
 *          | |_) |    | |
 *          |____/     |_|
 */

/*
 * defines constant of module name
 * * uses => set short name of module 
 */
define('_FPC_MODULE_NAME', 'FBPSC');

/*
 * defines set module name
 * uses => on setting name of module 
 */
define('_FPC_MODULE_SET_NAME', 'facebookpsconnect');

/*
 * defines root path of module
 * uses => with all included files  
 */
define('_FPC_PATH_ROOT', _PS_MODULE_DIR_ . _FPC_MODULE_SET_NAME . '/');

/*
 * defines conf path
 * uses => with including conf files in match environment  
 */
define('_FPC_PATH_CONF', _FPC_PATH_ROOT . 'conf/');

/*
 * defines libraries path
 * uses => with all class files  
 */
define('_FPC_PATH_LIB', _FPC_PATH_ROOT . 'lib/');

/*
 * defines sql path
 * uses => with all SQL script  
 */
define('_FPC_PATH_SQL', _FPC_PATH_ROOT . 'sql/');

/*
 * defines common library path
 * uses => to include class files  
 */
define('_FPC_PATH_LIB_COMMON', _FPC_PATH_LIB . 'common/');

/*
 * defines common library path
 * uses => to include class files
 */
define('_FPC_PATH_LIB_RENDER', _FPC_PATH_LIB . 'render/');

/*
 * defines connectors library path
 * uses => to include class files  
 */
define('_FPC_PATH_LIB_CONNECTOR', _FPC_PATH_LIB . 'connectors/');

/*
 * defines voucher library path
 * uses => to include class files
 */
define('_FPC_PATH_LIB_VOUCHER', _FPC_PATH_LIB . 'voucher/');

/*
 * defines dashboard library path
 * uses => to include class files
 */
define('_FPC_PATH_LIB_DASHBOARD', _FPC_PATH_LIB . 'dashboard/');

/*
 * defines connectors library path
 * uses => to include class files  
 */
define('_FPC_PATH_LIB_PROTOCOL', _FPC_PATH_LIB . 'protocols/');

/*
 * defines views folder
 * uses => to include img / css / js / templates files
 */
define('_FPC_PATH_VIEWS', 'views/');

/*
 * defines views folder
 * uses => to include img / css / js / templates files
 */
define('_FPC_PATH_HELPERS', _FPC_PATH_VIEWS . 'helpers/');

/*
 * defines common path tpl
 * uses => to set good absolute path
 */
define('_FPC_TPL_COMMON_PATH', 'admin/');

/*
 * defines common path tpl
 * uses => to set good absolute path
 */
define('_FPC_PATH_RENDER', _FPC_PATH_LIB . 'render/');

/*
 * defines js URL
 * uses => to include js files on templates (use prestashop constant _MODULE_DIR_)  
 */
define('_FPC_URL_JS', _MODULE_DIR_ . _FPC_MODULE_SET_NAME . '/' . _FPC_PATH_VIEWS . 'js/');

/*
 * defines css URL
 * uses => to include css files on templates (use prestashop constant _MODULE_DIR_)  
 */
define('_FPC_URL_CSS', _MODULE_DIR_ . _FPC_MODULE_SET_NAME . '/' . _FPC_PATH_VIEWS . 'css/');

/*
 * defines img path
 * uses => to include all used images  
 */
define('_FPC_PATH_IMG', 'img/');

/*
 * defines admin img URL
 * uses => to include js files on templates (use prestashop constant _MODULE_DIR_)
 */
define('_FPC_URL_IMG_ADMIN', _MODULE_DIR_ . _FPC_MODULE_SET_NAME . '/' . _FPC_PATH_VIEWS . _FPC_PATH_IMG . 'admin');

/*
 * defines img URL
 * uses => to include img files in templates (use Prestashop constant _MODULE_DIR_)  
 */
define('_FPC_URL_IMG', _MODULE_DIR_ . _FPC_MODULE_SET_NAME . '/' . _FPC_PATH_VIEWS . _FPC_PATH_IMG);

/*
 * defines MODULE URL
 * uses => to execute updating of callback review value  
 */
define('_FPC_MODULE_URL', _MODULE_DIR_ . _FPC_MODULE_SET_NAME . '/');

/*
 * defines tpl path name
 * uses => with included templates  
 */
define('_FPC_PATH_TPL_NAME', _FPC_PATH_VIEWS . 'templates/');

/*
 * defines tpl path
 * uses => with included templates  
 */
define('_FPC_PATH_TPL', _FPC_PATH_ROOT . _FPC_PATH_TPL_NAME);

/*
 * defines mails path
 * uses => to get mail template by language
 */
define('_FPC_PATH_MAILS', _FPC_PATH_ROOT . 'mails/');

/*
 * defines header tpl
 * uses => with display admin interface  
 */
define('_FPC_TPL_HEADER', 'header.tpl');

/*
* defines variable for admin ctrl name
 */
define('_FPC_PARAM_CTRL_NAME', 'sController');

/*
* defines variable for admin ctrl name
 * */
define('_FPC_ADMIN_CTRL', 'admin');

/*
 * defines constant of error tpl
 * uses => with display error - transverse tpl  
 */
define('_FPC_TPL_ERROR', 'error.tpl');

/*
 * defines constant of empty tpl
 * uses => with display error - transverse tpl  
 */
define('_FPC_TPL_EMPTY', 'empty.tpl');

/*
 * defines confirm tpl
 * uses => with display admin / hook interface  
 */
define('_FPC_TPL_CONFIRM', 'confirm.tpl');

/*
 * defines facebook button tpl
 * uses => to display connect in hook mode  
 */
define('_FPC_TPL_BUTTON_FB', 'button-facebook.tpl');

/*
 * defines twitter button tpl
 * uses => to display connect in hook mode  
 */
define('_FPC_TPL_BUTTON_TWITTER', 'button-twitter.tpl');

/*
 * defines google button tpl
 * uses => to display connect in hook mode  
 */
define('_FPC_TPL_BUTTON_GOOGLE', 'button-google.tpl');

/*
 * defines paypal button tpl
 * uses => to display connect in hook mode  
 */
define('_FPC_TPL_BUTTON_PAYPAL', 'button-paypal.tpl');

/*
 * defines amazon button tpl
 * uses => to display connect in hook mode
 */
define('_FPC_TPL_BUTTON_AMAZON', 'button-amazon.tpl');

/*
 * defines tpl mail name email template + voucher 1.5
 * uses =>  to send an email while an account is created
 */
define('_FPC_TPL_MAIL_NOTIF_VOUCHER_15', 'account-voucher-15');

/*
 *defines tpl mail name email template 1.5
 * uses =>  to send an email an account is created
 */
define('_FPC_TPL_MAIL_NOTIF_15', 'account-15');

/*
 * defines tpl mail name email template + voucher 1.6
 * uses =>  to send an email while an account is created
 */
define('_FPC_TPL_MAIL_NOTIF_VOUCHER_16', 'account-voucher-16');

/*
 * defines tpl mail name email template 1.6
 * uses =>  to send an email an account is created
 */
define('_FPC_TPL_MAIL_NOTIF_16', 'account-16');

/*
 * defines tpl mail name email template + voucher 1.7
 * uses =>  to send an email while an account is created
 */
define('_FPC_TPL_MAIL_NOTIF_VOUCHER_17', 'account-voucher-17');

/*
 * defines tpl mail name email template 1.7
 * uses =>  to send an email an account is created
 */
define('_FPC_TPL_MAIL_NOTIF_17', 'account-17');

/*
 * defines activate / deactivate debug mode
 * uses => only in debug / programming mode  
 */
define('_FPC_DEBUG', false);

/*
 * defines constant to use or not js on submit action
 * uses => only in debug mode - test checking control on server side  
 */
define('_FPC_USE_JS', true);


/** defines global for CSS management  */
$GLOBALS['FBPSC_POSITION_TOP_CSS'] = array(
    'activate' => '1',
    'border_color' => '#BBBBBB',
    'border_size' => '0',
    'background_color' => '#f3f3f3',
    'text_color' => '#333333',
    'text_size' => '15',
    'padding_top' => '10',
    'padding_left' => '10',
    'padding_bottom' => '10',
    'padding_right' => '10',
    'margin_top' => '0',
    'margin_left' => '0',
    'margin_bottom' => '0',
    'margin_right' => '0'
);

$GLOBALS['FBPSC_POSITION_LEFT_CSS'] = array(
    'activate' => '1',
    'border_color' => '#BBBBBB',
    'border_size' => '0',
    'background_color' => '#f3f3f3',
    'text_color' => '#333333',
    'text_size' => '15',
    'padding_top' => '10',
    'padding_left' => '10',
    'padding_bottom' => '10',
    'padding_right' => '10',
    'margin_top' => '0',
    'margin_left' => '0',
    'margin_bottom' => '0',
    'margin_right' => '0'
);

$GLOBALS['FBPSC_POSITION_RIGHT_CSS'] = array(
    'activate' => '1',
    'border_color' => '#BBBBBB',
    'border_size' => '0',
    'background_color' => '#f3f3f3',
    'text_color' => '#333333',
    'text_size' => '15',
    'padding_top' => '10',
    'padding_left' => '10',
    'padding_bottom' => '10',
    'padding_right' => '10',
    'margin_top' => '0',
    'margin_left' => '0',
    'margin_bottom' => '0',
    'margin_right' => '0'
);
$GLOBALS['FBPSC_POSITION_FOOTER_CSS'] = array(
    'activate' => '1',
    'border_color' => '#BBBBBB',
    'border_size' => '0',
    'background_color' => '#f3f3f3',
    'text_color' => '#333333',
    'text_size' => '15',
    'padding_top' => '10',
    'padding_left' => '10',
    'padding_bottom' => '10',
    'padding_right' => '10',
    'margin_top' => '0',
    'margin_left' => '0',
    'margin_bottom' => '0',
    'margin_right' => '0'
);

$GLOBALS['FBPSC_POSITION_AUTHENTICATION_CSS'] = array(
    'activate' => '1',
    'border_color' => '#BBBBBB',
    'border_size' => '0',
    'background_color' => '#f3f3f3',
    'text_color' => '#333333',
    'text_size' => '15',
    'padding_top' => '10',
    'padding_left' => '10',
    'padding_bottom' => '10',
    'padding_right' => '10',
    'margin_top' => '0',
    'margin_left' => '0',
    'margin_bottom' => '0',
    'margin_right' => '0'
);

$GLOBALS['FBPSC_POSITION_BLOCKUSER_CSS'] = array(
    'activate' => '1',
    'border_color' => '#BBBBBB',
    'border_size' => '0',
    'background_color' => '#f3f3f3',
    'text_color' => '#333333',
    'text_size' => '15',
    'padding_top' => '10',
    'padding_left' => '10',
    'padding_bottom' => '10',
    'padding_right' => '10',
    'margin_top' => '0',
    'margin_left' => '0',
    'margin_bottom' => '0',
    'margin_right' => '0'
);

$GLOBALS['FBPSC_POSITION_ORDER_FUNNEL_CSS'] = array(
    'activate' => '1',
    'border_color' => '#BBBBBB',
    'border_size' => '0',
    'background_color' => '#f3f3f3',
    'text_color' => '#333333',
    'text_size' => '15',
    'padding_top' => '10',
    'padding_left' => '10',
    'padding_bottom' => '10',
    'padding_right' => '10',
    'margin_top' => '0',
    'margin_left' => '0',
    'margin_bottom' => '0',
    'margin_right' => '0'
);

/*
 * defines variable for setting configuration options
 * uses => with install or update action - declare all mandatory values stored by prestashop in module using  
 */
$GLOBALS['FBPSC_CONFIGURATION'] = array(
    'FBPSC_MODULE_VERSION' => '1.0.1',
    'FBPSC_DISPLAY_FB_POPIN' => 0,
    'FBPSC_DISPLAY_BLOCK' => 0,
    'FBPSC_DISPLAY_CUSTOM_TEXT' => 0,
    'FBPSC_DEFAULT_CUSTOMER_GROUP' => 3,
    'FBPSC_API_REQUEST_METHOD' => 'fopen',
    'FBPSC_TEST_CURl_SSL' => 0,
    'FBPSC_SSL_TEST_TODO' => 1,
    'FBPSC_DISPLAY_BLOCK_INFO_ACCOUNT' => 0,
    'FBPSC_DISPLAY_BLOCK_INFO_CART' => 0,
    'FBPSC_ENABLE_VOUCHER' => 0,
    'FBPSC_VOUCHER_CODE_PREFIX' => '',
    'FBPSC_VOUCHER_TYPE' => '',
    'FBPSC_VOUCHER_PERCENT' => '',
    'FBPSC_VOUCHER_AMOUNT' => '',
    'FBPSC_VOUCHER_CURRENCY' => 0,
    'FBPSC_VOUCHER_TAX' => 0,
    'FBPSC_VOUCHER_MAX_QTY' => 0,
    'FBPSC_VOUCHER_MIN_AMOUNT' => 0,
    'FBPSC_VOUCHER_VALIDITY' => 0,
    'FBPSC_HIGHTLIGHT_VOUCHER' => 0,
    'FBPSC_CUMULATE_OTHERS_VOUCHER' => 0,
    'FBPSC_CUMULATE_DISCOUNT_VOUCHER' => 0,
    'FBPSC_VOUCHER_DISPLAY_MODE' => 'none',
    'FBPSC_VOUCHER_DESC' => '',
    'FBPSC_VOUCHER_DISP_TEXT' => '',
    'FBPSC_ENABLE_CUSTOM_TXT' => 0,
    'FBPSC_ACCOUNT_TEXT' => '',
    'FBPSC_ACC_TXT_COLOR' => '#000000',
    'FBPSC_ACC_TXT_SIZE' => '15',
    'FBPSC_ACC_BORDER_COLOR' => '#000000',
    'FBPSC_ACC_BORDER_SIZE' => '2px',
    'FBPSC_ACC_BACK_COLOR' => '#fff',
    'FBPSC_ACC_TEXT_MARGIN_ABOVE' => '20px',
    'FBPSC_ACC_TEXT_MARGIN_BELOW' => '5px',
    'FBPSC_ACC_TEXT_MARGIN_LEFT' => '10px',
    'FBPSC_ACC_TEXT_MARGIN_RIGHT' => '85px',
    'FBPSC_CONNECT_TEXT' => '',
    'FBPSC_VOUCHER_DETAIL' => 0,
    'FBPSC_BUTTON_STYLE' => 'modern',
    'FBPSC_ACC_BTN_POSITION' => 'below',
    'FBPSC_FUN_BTN_POSITION' => 'above',
    'FBPSC_SHORT_BTN_SIZE' => 'large',
    'FBPSC_SHORT_CONNECTOR' => '',
    'FBPSC_OPC_POSITION' => 'above',
    'FBPSC_DISPLAY_BLOCK_CONFIG_FORM' => '',
    'FBPSC_BADGE_TOP_CSS' => serialize($GLOBALS['FBPSC_POSITION_TOP_CSS']),
    'FBPSC_BADGE_FOOTER_CSS' => serialize($GLOBALS['FBPSC_POSITION_FOOTER_CSS']),
    'FBPSC_BADGE_AUTHENTICATION_CSS' => serialize($GLOBALS['FBPSC_POSITION_AUTHENTICATION_CSS']),
    'FBPSC_BADGE_BLOCKUSER_CSS' => serialize($GLOBALS['FBPSC_POSITION_BLOCKUSER_CSS']),
    'FBPSC_BADGE_LEFT_CSS' => serialize($GLOBALS['FBPSC_POSITION_LEFT_CSS']),
    'FBPSC_BADGE_RIGHT_CSS' => serialize($GLOBALS['FBPSC_POSITION_RIGHT_CSS']),
    'FBPSC_BADGE_ORDERFUNNEL_CSS' => serialize($GLOBALS['FBPSC_POSITION_ORDER_FUNNEL_CSS']),
);

/* defines variable to define default voucher description translations */
$GLOBALS['FBPSC_DEFAULT_VOUCHER_DESC'] = array(
    'en' => 'Coupon code for connection with the social network',
    'fr' => 'Code promo pour la connection avec un réseau social',
    'it' => 'Le ultime notizie per il collegamento con il social network',
    'es' => 'Código de descuento para la conexión con la red social',
);

/* defines variable to define default display incentive voucher text translations */
$GLOBALS['FBPSC_DEFAULT_DISPLAY_TEXT'] = array(
    'en' => 'get a discount code when you connect with a social network',
    'fr' => 'obtenez un code promotion en vous connectant avec un des réseaux sociaux',
    'it' => 'ottenere un codice sconto accedendo con un social network',
    'es' => 'obtener un código de descuento inicia la sesión con una red social',
);

/* defines variable to define default account text information translations */
$GLOBALS['FBPSC_DEFAULT_DISPLAY_ACC_TEXT'] = array(
    'en' => 'You can use any of the login buttons above to automatically create an account on our shop..',
    'fr' => 'Liez votre compte PrestaShop à votre profil Facebook en utilisant le bouton Social Login ci-dessous ! .',
    'it' => 'È possibile utilizzare i tasti sotto per accedere la creazione automatica nel nostro negozio.',
    'es' => 'Puede utilizar los botones de login de debajo para crear una cuenta automáticamente en nuestra tienda. .',
);

//Get the display name of the classical position
$GLOBALS['FBPSC_DEFAULT_POSITION_TEXT'] = array(
    'displayTop' => array(
        'en' => 'Top of the page',
        'fr' => 'Haut de page',
        'es' => 'Top of the page',
        'it' => 'Top of the page'
    ),
    'blockUser' => array('en' => 'Block user', 'fr' => 'Bloc utilisateur', 'es' => 'Block user', 'it' => 'Block user'),
    'authentication' => array(
        'en' => 'Login page',
        'fr' => 'Page de login',
        'es' => 'Login page',
        'it' => 'Login page'
    ),
    'displayFooter' => array(
        'en' => 'Bottom of the page',
        'fr' => 'Bas de page',
        'es' => 'Bottom of the page',
        'it' => 'Bottom of the page'
    ),
    'displayLeftColumn' => array(
        'en' => 'Left column',
        'fr' => 'Colonne de gauche',
        'es' => 'Left column',
        'it' => 'Left column'
    ),
    'displayRightColumn' => array(
        'en' => 'Right column',
        'fr' => 'Colonne de droite',
        'es' => 'Right column',
        'it' => 'Right column'
    ),
    'orderFunnel' => array(
        'en' => 'Order funnel',
        'fr' => 'Tunnel de commande',
        'es' => 'Order Funnel',
        'it' => 'Imbuto ordine'
    ),
);


//Get the values for data for the back office
$GLOBALS['FBPSC_DEFAULT_POSITION_ABOVE_TEXT'] = array(
    'en' => 'Above',
    'fr' => 'Au dessus',
    'it' => 'Sopra',
    'es' => 'Encima'
);
$GLOBALS['FBPSC_DEFAULT_POSITION_BELOW_TEXT'] = array(
    'en' => 'Below',
    'fr' => 'Dessous',
    'it' => 'Sotto',
    'es' => 'Abajo'
);
$GLOBALS['FBPSC_DEFAULT_BUTTON_LARGE_TEXT'] = array(
    'en' => 'Large',
    'fr' => 'Grand',
    'it' => 'Grande',
    'es' => 'Grande'
);
$GLOBALS['FBPSC_DEFAULT_BUTTON_SMALL_TEXT'] = array(
    'en' => 'Small',
    'fr' => 'Petit',
    'it' => 'Pequeña',
    'es' => 'Piccolo'
);


$GLOBALS['FBPSC_PAGE_TEXT'] = array(
    'product' => array('en' => 'Product', 'fr' => 'Produit', 'es' => 'Producto', 'it' => 'Prodotto'),
    'cms' => array('en' => 'CMS', 'fr' => 'CMS', 'es' => 'CMS', 'it' => 'CMS'),
    'search' => array('en' => 'Search', 'fr' => 'Recherche', 'es' => 'Buscar', 'it' => 'Ricerca'),
    'cart' => array('en' => 'Cart', 'fr' => 'Panier', 'es' => 'Carro', 'it' => 'Carrello'),
    'category' => array('en' => 'Category', 'fr' => 'Categorie', 'es' => 'Categoria', 'it' => 'Categoria'),
    'manufacturer' => array('en' => 'Manufacturer', 'fr' => 'Fabricant', 'es' => 'Fabricante', 'it' => 'Fabbricante'),
    'promotion' => array('en' => 'Promotion', 'fr' => 'Promotion', 'es' => 'Promoción', 'it' => 'Promozione'),
    'newproducts' => array(
        'en' => 'New products',
        'fr' => 'Nouveaux produits',
        'es' => 'Nuevos productos',
        'it' => 'Nuovi prodotti'
    ),
    'bestsales' => array(
        'en' => 'Best sales',
        'fr' => 'Meilleures ventes',
        'es' => 'Las mejores ventas',
        'it' => 'Le migliori vendite'
    ),
    'home' => array('en' => 'Homepage', 'fr' => 'Homepage', 'es' => 'Homepage', 'it' => 'Homepage'),
    'contactus' => array(
        'en' => 'Contact us',
        'fr' => 'Contactez nous',
        'es' => 'Nuevo Contáctenos',
        'it' => 'Nuovo Contattaci'
    ),
    'stores' => array('en' => 'Stores', 'fr' => 'Boutiques', 'es' => 'Tiendas', 'it' => 'Negozi'),

);


/* defines variable to define default account text information translations */
$GLOBALS['FBPSC_DEFAULT_CONNECTION_TEXT'] = array(
    'en' => 'Connect with',
    'fr' => 'Se connecter avec',
    'it' => 'Connesso con.',
    'es' => 'Conectar con',
);

/* defines variable to define default account text information translations */
$GLOBALS['FBPSC_DEFAULT_AUTH_CONNECT_TEXT'] = array(
    'en' => 'Link your accout a the social network',
    'fr' => 'Liez votre compte à un réseau social',
    'it' => 'Collega il tuo account alla rete sociale',
    'es' => 'Vincular su cuenta a la red social',
);

/* Translations for the buttons' labels */
$GLOBALS['FBPSC_BUTTONS_LABEL'] = array(
    'facebook' => array(
        'en' => 'Login with Facebook',
        'fr' => 'Connexion avec Facebook',
        'it' => 'Accedi con Facebook',
        'es' => 'Iniciar sesión con Facebook',
    ),
    'google' => array(
        'en' => 'Sign in with Google',
        'fr' => 'Connexion avec Google',
        'it' => 'Accedi con Google',
        'es' => 'Iniciar sesión con Google',
    ),
    'amazon' => array(
        'en' => 'Login with Amazon',
        'fr' => 'Connexion avec Amazon',
        'it' => 'Accedi con Amazon',
        'es' => 'Login con Amazon',
    ),
    'paypal' => array(
        'en' => 'Connect with PayPal',
        'fr' => 'Connexion avec PayPal',
        'it' => 'Collegati a PayPal',
        'es' => 'Conectarse con PayPal',
    ),
    'twitter' => array(
        'en' => 'Sign in with Twitter',
        'fr' => 'Connexion avec Twitter',
        'it' => 'Accedi con Twitter',
        'es' => 'Iniciar sesión con Twitter',
    ),
);

/*
 * defines variable for setting hooks
 * uses => in INSTALL / ADMIN / HOOK mode  
 */
$GLOBALS['FBPSC_HOOKS'] = array(
    'header' => array(
        'name' => 'displayHeader',
        'data' => Configuration::get('FBPSC_HEADER'),
        'use' => false,
        'title' => ''
    ),
    'top' => array('name' => 'displayTop', 'data' => Configuration::get('FBPSC_TOP'), 'use' => true, 'title' => ''),
    'account' => array(
        'name' => 'displayCustomerAccount',
        'data' => Configuration::get('FBPSC_ACCOUNT'),
        'use' => false,
        'title' => ''
    ),
    'right' => array(
        'name' => 'displayRightColumn',
        'data' => Configuration::get('FBPSC_RIGHT'),
        'use' => true,
        'title' => ''
    ),
    'left' => array(
        'name' => 'displayLeftColumn',
        'data' => Configuration::get('FBPSC_LEFT'),
        'use' => true,
        'title' => ''
    ),
    'footer' => array(
        'name' => 'displayFooter',
        'data' => Configuration::get('FBPSC_FOOTER'),
        'use' => true,
        'title' => ''
    ),
);

if (version_compare(_PS_VERSION_, '1.7.0', '>')) {
    $GLOBALS['FBPSC_HOOKS'] = array_merge($GLOBALS['FBPSC_HOOKS'], array(
            'checkout' => array(
                'name' => 'displayReassurance',
                'data' => Configuration::get('FBPSC_AUTHENTICATION'),
                'use' => false,
                'title' => ''
            )
        )
    );
}

/*
 * defines variable for setting zones
 * uses => in ADMIN / HOOK mode  
 */
$GLOBALS['FBPSC_ZONE'] = array_merge($GLOBALS['FBPSC_HOOKS'], array(
        'authentication' => array(
            'name' => 'authentication',
            'data' => Configuration::get('FBPSC_AUTHENTICATION'),
            'use' => true,
            'title' => ''
        ),
        'blockUser' => array(
            'name' => 'blockUser',
            'data' => Configuration::get('FBPSC_BLOCKUSER'),
            'use' => true,
            'title' => 'Block user'
        )
    )
);

if (version_compare(_PS_VERSION_, '1.7.0', '>')) {
    $GLOBALS['FBPSC_ZONE'] = array_merge($GLOBALS['FBPSC_ZONE'], array(
            'orderFunnel' => array('name' => 'orderFunnel', 'data' => '', 'use' => true, 'title' => ''),
        )
    );
}

/*
 * defines variable for setting connectors
 * uses => in install / ADMIN / HOOK mode
 */
$GLOBALS['FBPSC_CONNECTORS_LIST'] = array('Facebook', 'Google', 'Paypal', 'Amazon', 'Twitter');

/*
 * defines variable for setting connectors
 * uses => in install / ADMIN / HOOK mode 
 */
$GLOBALS['FBPSC_CONNECTORS'] = array(
    'facebook' => array(
        'title' => '',
        'adminTpl' => 'facebook.tpl',
        'data' => Configuration::get('FBPSC_FACEBOOK'),
        'tpl' => _FPC_TPL_BUTTON_FB,
        'charts' => array('color' => '#577ab2'),
        'cssClass' => 'facebook',
    ),
    'twitter' => array(
        'title' => '',
        'adminTpl' => 'twitter.tpl',
        'data' => Configuration::get('FBPSC_TWITTER'),
        'tpl' => _FPC_TPL_BUTTON_TWITTER,
        'charts' => array('color' => '#00cfee'),
        'cssClass' => 'twitter',
    ),
    'google' => array(
        'title' => '',
        'adminTpl' => 'google.tpl',
        'data' => Configuration::get('FBPSC_GOOGLE'),
        'tpl' => _FPC_TPL_BUTTON_GOOGLE,
        'charts' => array('color' => '#c51f1f'),
        'cssClass' => 'google',
    ),
    'paypal' => array(
        'title' => '',
        'adminTpl' => 'paypal.tpl',
        'data' => Configuration::get('FBPSC_PAYPAL'),
        'tpl' => _FPC_TPL_BUTTON_PAYPAL,
        'charts' => array('color' => '#0070BA'),
        'cssClass' => 'paypal',
    ),
    'amazon' => array(
        'title' => '',
        'adminTpl' => 'amazon.tpl',
        'data' => Configuration::get('FBPSC_AMAZON'),
        'tpl' => _FPC_TPL_BUTTON_AMAZON,
        'charts' => array('color' => '#f5d071'),
        'cssClass' => 'amazon',
    ),
);

/*
 * defines variable for translating js msg
 * uses => with admin interface - declare all displayed error messages
 */
$GLOBALS['FBPSC_JS_MSG'] = array();
