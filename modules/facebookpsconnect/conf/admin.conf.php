<?php
/**
 * Social Login
 *
 * @author    BusinessTech.fr - https://www.businesstech.fr
 * @copyright Business Tech 2020 - https://www.businesstech.fr
 * @license   Commercial
 *
 *           ____    _______
 *          |  _ \  |__   __|
 *          | |_) |    | |
 *          |  _ <     | |
 *          | |_) |    | |
 *          |____/     |_|
 */

require_once(dirname(__FILE__) . '/common.conf.php');

/* defines modules support product id */
define('_FPC_SUPPORT_ID', '8204');

/* defines activate the BT support if false we use the ADDONS support url */
define('_FPC_SUPPORT_BT', false);
//define('_FPC_SUPPORT_BT', true);

/* defines activate the BT support if false we use the ADDONS support url */
define('_FPC_SUPPORT_URL', 'https://addons.prestashop.com/');
//define('_FPC_SUPPORT_URL', 'http://www.businesstech.fr/');;

/*
 * defines admin library path
 * uses => to include class files
 */
define('_FPC_PATH_LIB_ADMIN', _FPC_PATH_LIB . 'admin/');

/*
 * defines constant of connector path tpl
 * uses => to set good absolute path
 */
define('_FPC_TPL_CONNECTOR_PATH', 'connectors-form/');

/*
 * defines admin logs path
 * uses => to write log files
 */
define('_FPC_PATH_LOGS', _FPC_PATH_ROOT . 'logs/');

/*
 * defines admin path tpl
 * uses => to set good absolute path
 */
define('_FPC_TPL_ADMIN_PATH', 'admin/');

/*
 * defines body tpl
 * uses => with display admin interface
 */
define('_FPC_TPL_BODY', 'body.tpl');

/*
 * defines basic settings tpl
 * uses => with display admin interface
 */
define('_FPC_TPL_BASIC_SETTINGS', 'basic-settings.tpl');

/*
 * defines connectors settings tpl
 * uses => with display admin interface
 */
define('_FPC_TPL_CONNECTOR_SETTINGS', 'connector-settings.tpl');

/*
 * defines hook settings tpl
 * uses => with display admin interface
 */
define('_FPC_TPL_HOOK_SETTINGS', 'hook-settings.tpl');

/*
 * defines hook advanced settings tpl
 * uses => with display admin interface
 */
define('_FPC_TPL_HOOK_ADVANCED', 'advanced-hook-settings.tpl');

/*
 * defines hook advanced settings tpl
 * uses => with display admin interface
 */
define('_FPC_TPL_SHORT_CODE', 'shortcode-settings.tpl');

/*
 * defines hook advanced settings tpl
 * uses => with display admin interface
 */
define('_FPC_TPL_SHORT_CODE_FORM', 'shortcode-settings-form.tpl');

/*
 * defines hook settings tpl
 * uses => with display admin interface
 */
define('_FPC_TPL_VOUCHER_SETTINGS', 'voucher-settings.tpl');

/*
 * defines hook settings tpl
 * uses => with display admin interface
 */
define('_FPC_TPL_DASHBOARD', 'dashboard.tpl');

/*
 * defines hook settings tpl
 * uses => with display admin interface
 */
define('_FPC_TPL_CSS_CODE', 'css.tpl');

/*
 * defines hook settings tpl
 * uses => with display admin interface
 */
define('_FPC_TPL_CSS_FORM_CODE', 'css-form.tpl');

/*
 * defines hook form  tpl
 * uses => with display admin interface
 */
define('_FPC_TPL_HOOK_FORM', 'hook-form.tpl');

/*
 * defines curl ssl tpl
 * uses => with display admin interface
 */
define('_FPC_TPL_CURL_SSL', 'curl-ssl.tpl');

/*
 * defines constant of body tpl
 * uses => in display admin interface with connector form
 */
define('_FPC_TPL_PREREQUISITES_CHECK_SETTINGS', 'prerequisites-check.tpl');

/*
 * defines constant of body tpl
 * uses => in display admin interface with connector position form
 */
define('_FPC_TPL_CONNECTION_POSITION_SETTINGS', 'connect-position-settings.tpl');

/*
 * defines constant of body tpl
 * uses => in display admin interface with connector form
 */
define('_FPC_TPL_CONNECTOR_BODY', _FPC_TPL_CONNECTOR_PATH . 'body.tpl');

/*
 * defines constant for external BT API URL
 * uses => with display admin interface
 */
define('_FPC_BT_API_MAIN_URL', 'https://api.businesstech.fr:441/prestashop-modules/');

/*
 * defines constant for external BT API URL
 * uses => with display admin interface
 */
define('_FPC_BT_FAQ_MAIN_URL', 'https://faq.businesstech.fr');

/*
 * defines constant for SQL update file
 * uses => with display admin interface
 */
define('_FPC_CUST_TYPE_SQL_FILE', 'update-customer-type.sql');

/*
 * defines variable for sql update
 * uses => with admin
 */
define('_FPC_CATEGORIES_SQL_FILE', 'update-category-tree.sql');

/*
 * defines variable for sql update
 * uses => with admin
 */
define('_FPC_VOUCHER_ASSOC_SQL_FILE', 'update-voucher-assoc.sql');


/*
 * defines variable for sql update
 * uses => with admin
 */
define('_FPC_CONNECTOR_POSITION_SQL_FILE', 'update-connector-position.sql');


/*
 * defines variable for sql update
 * uses => with admin
 */
define('_FPC_DASHBOARD_SQL_FILE', 'update-dashboard.sql');

/* defines loader gif name */
define('_FPC_LOADER_GIF', 'bx_loader.gif');

$GLOBALS['FBPSC_SQL_UPDATE'] = array(
    'table' => array(
        'categories' => _FPC_CATEGORIES_SQL_FILE,
        'voucherAssoc' => _FPC_VOUCHER_ASSOC_SQL_FILE,
        'connectorPosition' => _FPC_CONNECTOR_POSITION_SQL_FILE,
    ),
    'field' => array(
        array(
            'field' => 'CNT_CUST_TYPE',
            'table' => 'connect',
            'file' => _FPC_CUST_TYPE_SQL_FILE,
            'check' => true,
            'type' => 'Type',
            'value' => "enum('none','facebook','twitter','google','paypal','amazon')"
        ),
        array('field' => 'CTN_DATE_ADD', 'table' => 'connect', 'file' => _FPC_DASHBOARD_SQL_FILE),
    )
);

/*
 * defines variable for charts
 * uses => dashboard
 */
$GLOBALS['FBPSC_CHARTS_PARAMS'] = array(
    'count' => array(
        'id' => 'myChartConnection',
        'title' => array(
            'en' => 'Number of connections per social network',
            'fr' => 'Nombre de connexions par réseau social',
            'es' => 'Número de conexiones por red social',
            'it' => 'Numero di connessioni per social network'
        ),
        'callback' => array('BT_FPCDashboard', 'countSocialConnect')
    ),
    'amount' => array(
        'id' => 'myChartAmount',
        'title' => array(
            'en' => 'Total spent per social network',
            'fr' => 'Montant total dépensé par réseau social',
            'es' => 'Total gastado por red social',
            'it' => 'Totale speso per social network'
        ),
        'callback' => array('BT_FPCDashboard', 'getCustomerSpent')
    ),
);

/*
 * defines variable for button style
 * uses => dashboard
 */
$GLOBALS['FBPSC_BUTTON_STYLE'] = array(
    'classical' => array('en' => 'Classical', 'fr' => 'Classique', 'es' => 'Clásico', 'it' => 'Classico'),
    'modern' => array('en' => 'Modern', 'fr' => 'Moderne', 'es' => 'Moderno', 'it' => 'Moderno'),
);

/*
 * defines variable for position where the button will be display from the HTLM element
 * uses => connector advance form
 */
$GLOBALS['FBPSC_BUTTON_POSITION'] = array(
    'above' => array('en' => 'Above', 'fr' => 'Au dessus', 'es' => 'Por encima', 'it' => 'Sopra'),
    'below' => array('en' => 'Below', 'fr' => 'En dessous', 'es' => 'Por debajo', 'it' => 'Sotto'),
);

/*
 * defines variable for page where the button will be display
 * uses => connector advance form
 */
$GLOBALS['FBPSC_BUTTON_SIZE'] = array(
    'large' => array('en' => 'Large', 'fr' => 'Grand', 'es' => 'Ancho', 'it' => 'Largo'),
    'small' => array('en' => 'Small', 'fr' => 'Petit', 'es' => 'Pequeño', 'it' => 'Piccolo'),
);

/*
 * defines variable for old classical position
 * uses => connector advance form
 */
$GLOBALS['FBPSC_OLD_CLASSICAL_POSITION'] = array('blockUser', 'authentication', 'orderFunnel');

/*
 * defines variable for page where the button will be display
 * uses => connector advance form
 */
$GLOBALS['FBPSC_CONNECTOR_PAGE'] = array(
    'product' => array('en' => 'Product', 'fr' => 'Produit', 'es' => 'Producto', 'it' => 'Prodotto'),
    'cms' => array('en' => 'CMS', 'fr' => 'CMS', 'es' => 'CMS', 'it' => 'CMS'),
    'search' => array('en' => 'Search', 'fr' => 'Recherche', 'es' => 'Buscar', 'it' => 'Ricerca'),
    'cart' => array('en' => 'Cart', 'fr' => 'Panier', 'es' => 'Carro', 'it' => 'Carrello'),
    'category' => array('en' => 'Category', 'fr' => 'Catégorie', 'es' => 'Categoría', 'it' => 'Categoria'),
    'manufacturer' => array('en' => 'Manufacturer', 'fr' => 'Fabricant', 'es' => 'Fabricante', 'it' => 'Fabbricante'),
    'promotion' => array('en' => 'Promotion', 'fr' => 'Promotion', 'es' => 'Promoción', 'it' => 'Promozione'),
    'newproducts' => array(
        'en' => 'New products',
        'fr' => 'Nouveaux produits',
        'es' => 'Nuevos productos',
        'it' => 'Nuovi Prodotti'
    ),
    'bestsales' => array(
        'en' => 'Best sales',
        'fr' => 'Meilleures ventes',
        'es' => 'Las mejores ventas',
        'it' => 'Le migliori vendite'
    ),
    'home' => array('en' => 'Homepage', 'fr' => 'Page d\'accueil', 'es' => 'Homepage', 'it' => 'Homepage'),
    'contactus' => array('en' => 'Contact Us', 'fr' => 'Contactez nous', 'es' => 'Contáctenos', 'it' => 'Contattaci'),
    'stores' => array('en' => 'Shop', 'fr' => 'Boutique', 'es' => 'Negozio', 'it' => 'Tienda'),
    'all' => array(
        'en' => 'All pages',
        'fr' => 'Toutes les pages',
        'es' => 'Todas las páginas',
        'it' => 'Tutte le pagine'
    ),
);

/*
 * defines variable for setting all request params
 * uses => with admin interface
 */
$GLOBALS['FBPSC_REQUEST_PARAMS'] = array(
    'basic' => array('action' => 'update', 'type' => 'basic'),
    'connectorForm' => array('action' => 'display', 'type' => 'connectorForm'),
    'connector' => array('action' => 'update', 'type' => 'connector'),
    'hook' => array('action' => 'update', 'type' => 'hook'),
    'hookForm' => array('action' => 'display', 'type' => 'hookForm'),
    'testssl' => array('action' => 'display', 'type' => 'testssl'),
    'vouchers' => array('action' => 'update', 'type' => 'voucher'),
    'dashboard' => array('action' => 'update', 'type' => 'dashboard'),
    'hookAdvanced' => array('action' => 'update', 'type' => 'hookAdvanced'),
    'hookAdvancedForm' => array('action' => 'display', 'type' => 'hookAdvancedForm'),
    'connectPositionForm' => array('action' => 'display', 'type' => 'connectPositionForm'),
    'connectorPositionUpdate' => array('action' => 'update', 'type' => 'connectorPositionUpdate'),
    'connectPosition' => array('action' => 'update', 'type' => 'connectPosition'),
    'shortCodeList' => array('action' => 'update', 'type' => 'shortCodeList'),
    'shortCode' => array('action' => 'update', 'type' => 'shortCode'),
    'shortCodeForm' => array('action' => 'display', 'type' => 'shortCodeForm'),
    'cssCode' => array('action' => 'update', 'type' => 'cssCode'),
    'cssForm' => array('action' => 'display', 'type' => 'cssForm'),
);
