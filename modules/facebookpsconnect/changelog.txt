Change history for Social Login
---------------------------------------
===========================
Version 3.0.16 (17/07/2020)
===========================

Fixed
-----
- Close on CurlSsl test
- Open button for shortcode
- Display of the shortcode in the modal


===========================
Version 3.0.15 (25/05/2020)
===========================

Added
-----
- Fix for Paypal redirection after login
- Fix display for total spent
- Removed unused code and files
- Use new color picker plugin


===========================
Version 3.0.14 (21/06/2019)
===========================

Added
-----
- Apply new button guidelines
- Option for custom text activation or not

===========================
Version 3.0.13 (10/04/2019)
===========================

Fixed
-----
- Apply new Facebook button style to follow the guidelines


===========================
Version 3.0.12 (14/11/2018)
===========================

Fixed
-----
- Paypal stop giving phone number with scope

Added
-----
- Optimization for FAQ link


===========================
Version 3.0.11 (30/10/2018)
===========================

Fixed
-----
- New way to call the API 3.2 for Facebook, the email permission must be force on the call


===========================
Version 3.0.10 (23/10/2018)
===========================

Fixed
-----
- Cumulable voucher on PS 1.7
- Facebook FAQ topic link
- Some label for Google connector
- New way to use Paypal Api
- Some bad FAQ topic link


===========================
Version 3.0.9 (26/09/2018)
===========================

Fixed
-----
- Use new Google API
- Wording
- Dropdown on interface after AJAX request


===========================
Version 3.0.8 (27/07/2018)
===========================

Fixed
-----
- Fixed the class name used for the session part, it made the connexion impossible as a PHP error was detected

===========================
Version 3.0.7 (24/07/2018)
===========================

Fixed
-----
- Fixed the short code management in order to not override short code together when many are included into the sam2 page

===========================
Version 3.0.6 (02/07/2018)
===========================

Fixed
-----
- Remove a die and var_dump

===========================
Version 3.0.5 (27/02/2018)
===========================


===========================
Version 3.0.4 (14/02/2018)
===========================

Fixed
-----
- Some option wasn't translated well if we didn't use EN / ES / IT / FR languages
- Only display active and configured connectors for adavanced position and shortcode


===========================
Version 3.0.3 (07/02/2018)
===========================

Fixed
-----
- Fix for the new Paypal lastname and firstname managment

===========================
Version 3.0.2 (05/02/2018)
===========================

Fixed
-----
- Save option for advanced position activation
- Hide the advanced position when the option is on NO

===========================
Version 3.0.1 (24/01/2018)
===========================

Fixed
-----
- Add cast on some SQL request for security

===========================
Version 3.0.0 (05//01/2018)
===========================

Added
-----
- Lot of new feature Advanced position / dashboard
===========================
Version 2.3.0 (27/06/2017)
===========================

Fixed
-----
- Notice for Prestashop 1.7.1.0 and more

===========================
Version 2.2.12 (31/05/2017)|
===========================

Fixed
-----
- Js draggable function for PS 1.5
- Compatibility for PHP 7


===========================
Version 2.2.11 (10/05/2017)|
===========================

Fixed
-----
- Some back-office interface bug


===========================
Version 2.2.10 (12/04/2017)|
===========================

Fixed
-----
- Warning for top button position

===========================
Version 2.2.9 (03/04/2017)|
===========================

Fixed
-----
- Button in funnel for PS 1.7
- Compatibility for PHP 7.1

===========================
Version 2.2.8 (30/03/2017)|
===========================

Fixed
-----
- Update to Facebook connector after changes in Facebook API (URL now passed as JSON) which caused blank page


===========================
Version 2.2.7 (23/10/2016)|
===========================
Added
-----
- Stay in the funnel when the login is made from the order step sign in

Fixed
-----
- Bug in the Fancybox for the association the loyaty program was display in


===========================
Version 2.2.6 (05/01/2016)|
===========================
Added
-----
-

Fixed
-----
- Some translate text
- Display bug in front office

===========================
Version 2.2.5 (07/12/2016)|
===========================
Added
-----
-

Fixed
-----
- JS error for PS 1.7.0 + due to JS_DEFER removal

===========================
Version 2.2.4 (16/11/2016)|
===========================
Added
-----
-

Fixed
-----
- 404 page When a customer logout from a specific page like addresse and log again in with the module
- bug fix for Multiboutique when customer aren't shared
- bug fix for email with voucher code

===========================
Version 2.2.3 (19/10/2016)|
===========================
Added
-----
-

Fixed
-----
- id product for support


===========================
Version 2.2.2 (18/10/2016)|
===========================
Added
-----
- Control on the date end for the voucher

Fixed
-----
- Don't show button for voucher code popin

Files
-----

===========================
Version 2.2.1 (12/10/2016)|
===========================
Added
-----
- Fixed for new interface and translate

Fixed
-----
-

Files
-----

===========================
Version 2.2.0 (07/10/2016)|
===========================
Added
-----
- Compatibilty for PS 1.7

Fixed
-----
-

Files
-----

===========================
Version 2.1.1 (28/09/2016)|
===========================
Added
-----
-

Fixed
-----
- Translate

Files
-----


===========================
Version 2.1.0 (23/09/2016)|
===========================
Added
-----
- Tools to customize display colors
- Configuration for each text display in the front office
- Option to add voucher with a connection button, and can be send per email and display on the customer account with a popup

Fixed
-----
-

Files
-----
- base-connector_class.php
- common.conf.php
- views/templates/admin/connectors-form files
- mail-send_class.php



Version 2.0.4 (10/02/2016)
- Updated logo
- Updated API url
- Update icon

Version 2.0.3 (09/11/2015)
- Bug Fix for PS gender ID that has changed their ps_gender table as well as we do not get the matching gender ID.
- Changed the function to check and get the customer ID of the current customer
- Added a stdClass object creation for the temporary address  object, depends on the PHP version, it makes a warning


Version 2.0.2 (15/10/2015)
- Solved the template issue when the module is activated in FO


Version 2.0.1 (01/09/2015)
- Solved the twitter form issue in the customer account => rollback on the intval applied on $iCustomerId (connector-account.tpl => line 167) variable as well as it was an encoded string and not only a numeric value (applied security rule)
- Solved the form connexion URL in the account block, for PS 1.5 and over the controller name isn't the same.


Version 2.0.0 (20/07/2015)
- Added new amazon connector
- Bug Fix for drag and drop with jquery ui 1.10.4 + FireFox 38 => added jquery ui 1.11.4


Version 1.5.5 (09/03/2015)
- Bug fix for email password generation


Version 1.5.4 (09/03/2015)
- Bug fix for Facebook form config
- Change default image with bootstrap images for PS 1.6
- Bug fix for cart and account URL on PS 1.6


Version 1.5.3 (16/02/2015)
- Remove text about CRM in connector-button.tpl
- Update field size for Facebook connector (in the configuration)
- Update Paypal connector (remove CURLOPT_SSLVERSION in function runCurl)


Version 1.5.2 (18/11/2014)
- Small bug fix for box with all button


Version 1.5.1 (17/11/2014)
- Small bug fix for Fancybox in account association


Version 1.5.0 (13/11/2014)
- Bug fix for paypal and the API


Version 1.4.5 (08/09/2014)
- Added small bug fix for displaying the "display block cart text" option when the One Page Checkout is activated


Version 1.4.4 (12/08/2014)
- Block in account page become configurable ( show or not )
- Block in OPC page become configurable
- Layout with bootstrap and tooltips
- Manage Twitter name and lastname with figure.
- Update name and lastname when a customer create an account with twitter
- Send update information by email for Twitter account
- Bugfix Js Drag and Drop
- Bugfix Js Refresh


Version 1.4.3 (27/06/2014)
- Small bug fix for a wrong PHP variable name that displays a bad HTML return when the merchant update his connector's data. The module was still working with this bug, but a wrong error message has been displayed. It's solved


Version 1.4.2 (24/06/2014)
- Added send e-mail feature. An e-mail is sent to the customer who just created his account via a social network.
- Added small feature around customer account creation, we specify the current ID shop even if a customer account has to be unique into the database, with multi-shop or not.


Version 1.4.1 (04/04/2014)
- Update translation FR
- Update $this->version="1.4.1"


Version 1.4.0 (29/03/2014)
- Update design with boostrap
- Update for Prestashop 1.6
- Add Prerequisites Check system
- Add Curl SSL test
- Add warning in connector form
- Update PDF
- Delete elements about System Health
- Update language : fr / en / it / es
- Update CSS for TOP / BOTTOM : width 100%
- Hide Block Customer / Right Column for Prestashop 1.6


Version 1.3.3 (12/02/2014)
- Small bug fix for PS 1.5 with redirect feature on current cart, added code made a rollback on redirect feature, this code has been removed.


Version 1.3.2 (08/02/2014)
- Small bug fix for mobile devices when customer was redirected on the current page after logging. Added a test to make this redirect with PHP and not with Javascript command only.


Version 1.3.1 (08/01/2014)
- Small bug fix for PS 1.4 and 1.5 when back parameter is 'my-account' or 'my-account.php', redirect doesn't work in this case. Changed it for getPageLink function with my-account.php controller


Version 1.3.0 (07/01/2014)
- Added feature to redirect on check-out if necessary.


Version 1.2.2 (23/11/2013)
- Added information about cURL with SSL, Google / PayPal / Twitter require to use cURL with SSL.
- Added ES & IT translations


Version 1.2.1 (25/10/2013)
- small bug fix for testing default customer group setting in modules listing
- change behavior of twitter connector for customer account creation. Require to create an account with a generic e-mail, add test to use this generic account and update e-mail address only and not create a new account on next connexion.


Version 1.2.0 (09/10/2013)
- add default customer group feature
- add default API request method feature (file_get_contents or cURL with SSL methods)
- make the module compatible with one page checkout option


Version 1.1.2 (09/08/2013)
- small bug fix on loading js buttons for authentication page + remove delete js object key in draggableConnector() function, because it still got an issue with smart cache javascript setting


Version 1.1.1 (06/08/2013)
- small bug fix on initialized javascript messages in module-tools_class.php, It was missing 'delete' key for sentence in javascript object in function draggable(). Issue only on IE 8


Version 1.1.0 (16/07/2013)
- Add collect feature from Facebook PS Essentials for activate recording of Likes for all users
- Add system health feature
- small bug fix on PS under 1.4 for loading module's javascript file and set the JQuery conflict


Version 1.0.1 (24/06/2013):
- small bug fix for twitter connector when it hasn't been configured a PHP issue is returned by the base connector. Add test on twitter params with 'empty'
- small bug fix for default group on PS 1.5 as well as group 1 is used to be a guest group so we set the default group to 3 as this group id can't be changed and it is matching to the customers group


Version 1.0.0 (17/06/2013):
- Original release