<?php
/**
 * Social Login
 *
 * @author    BusinessTech.fr - https://www.businesstech.fr
 * @copyright Business Tech 2020 - https://www.businesstech.fr
 * @license   Commercial
 * @version 3.0.16
 *
 *           ____    _______
 *          |  _ \  |__   __|
 *          | |_) |    | |
 *          |  _ <     | |
 *          | |_) |    | |
 *          |____/     |_|
 */

if (!defined('_PS_VERSION_')) {
    exit(1);
}

class FacebookPsConnect extends Module
{
    /**
     * @var array $conf : array of set configuration
     */
    public static $conf = array();

    /**
     * @var int $iCurrentLang : store id of default lang
     */
    public static $iCurrentLang = null;

    /**
     * @var int $sCurrentLang : store iso of default lang
     */
    public static $sCurrentLang = null;

    /**
     * @var obj $oCookie : store cookie obj
     */
    public static $oCookie = null;

    /**
     * @var obj $oModule : obj module itself
     */
    public static $oModule = array();

    /**
     * @var string $sQueryMode : query mode - detect XHR
     */
    public static $sQueryMode = null;

    /**
     * @var string $sBASE_URI : base of URI in prestashop
     */
    public static $sBASE_URI = null;

    /**
     * @var array $aErrors : array get error
     */
    public $aErrors = null;

    /**
     * @var int $iShopId : shop id used for 1.5 and for multi shop
     */
    public static $iShopId = 1;
    /**
     * @var bool $bCompare15 : get compare version for PS 1.5
     */
    public static $bCompare15 = false;

    /**
     * @var bool $bCompare1550 : get compare version for PS 1.5.5.0
     */
    public static $bCompare1550 = false;

    /**
     * @var bool $bCompare16 : get compare version for PS 1.6
     */
    public static $bCompare16 = false;

    /**
     * @var bool $bCompare17 : get compare version for PS 1.7
     */
    public static $bCompare17 = false;

    /**
     * @var bool $bCompare1710 : get compare version for PS 1.7.1.0
     */
    public static $bCompare1710 = false;

    /**
     * @var bool $bCompare1766 : get compare version for PS 1.7.7
     */
    public static $bCompare1766 = false;

    /**
     * @var bool $bCompare1770 : get compare version for PS 1.7.7
     */
    public static $bCompare1770 = false;
    

    /**
     * assigns few information about module and instantiate parent class
     */
    public function __construct()
    {
        require_once(dirname(__FILE__) . '/conf/common.conf.php');

        require_once(_FPC_PATH_LIB . 'warning_class.php');
        require_once(_FPC_PATH_LIB . 'module-tools_class.php');

        //Get shop ID
        self::$iShopId = Context::getContext()->shop->id;
        // get current lang
        self::$iCurrentLang = Context::getContext()->cookie->id_lang;
        // get current iso lang
        self::$sCurrentLang = BT_FPCModuleTools::getLangIso();
        // get cookie obj
        self::$oCookie = Context::getContext()->cookie;

        $this->name = 'facebookpsconnect';
        $this->module_key = 'ffcbc0b08d66e0afb7ed1ed27e0f1492';
        $this->tab = 'social_networks';
        $this->version = '3.0.16';
        $this->author = 'Business Tech';
        $this->need_instance = 0;

        parent::__construct();

        $this->displayName = $this->l('Social Login');
        $this->description = $this->l('Let your customer easily log in via Facebook, Paypal, Amazon, Google or Twitter');
        $this->confirmUninstall = $this->l('Are you sure you want to remove it ? Your Social Login will no longer work. Be careful, all your configuration and your data will be lost');

        // stock itself obj
        self::$oModule = $this;

        // update module version
        $GLOBALS['FBPSC_CONFIGURATION']['FBPSC_MODULE_VERSION'] = $this->version;

        // set base of URI
        self::$sBASE_URI = $this->_path;

        // set title of hooks & connectors
        $GLOBALS['FBPSC_ZONE']['header']['title'] = $this->l('Header');
        $GLOBALS['FBPSC_ZONE']['top']['title'] = $this->l('Top');
        $GLOBALS['FBPSC_ZONE']['account']['title'] = $this->l('Customer account');
        $GLOBALS['FBPSC_ZONE']['checkout']['title'] = $this->l('Checkout funnel for PS 1.7');
        $GLOBALS['FBPSC_ZONE']['left']['title'] = $this->l('Left Column');
        $GLOBALS['FBPSC_ZONE']['right']['title'] = $this->l('Right Column');
        $GLOBALS['FBPSC_ZONE']['footer']['title'] = $this->l('Footer');
        $GLOBALS['FBPSC_ZONE']['authentication']['title'] = $this->l('Authentication page');
        $GLOBALS['FBPSC_CONNECTORS']['facebook']['title'] = $this->l('Facebook sign in');
        $GLOBALS['FBPSC_CONNECTORS']['twitter']['title'] = $this->l('Twitter sign in');
        $GLOBALS['FBPSC_CONNECTORS']['google']['title'] = $this->l('Google sign in');
        $GLOBALS['FBPSC_CONNECTORS']['paypal']['title'] = $this->l('Paypal sign in');
        $GLOBALS['FBPSC_CONNECTORS']['amazon']['title'] = $this->l('Amazon sign in');


        if (version_compare(_PS_VERSION_, '1.6.0', '<')) {
            $GLOBALS['FBPSC_ZONE']['right']['title'] = $this->l('Right Column');
            $GLOBALS['FBPSC_ZONE']['blockUser']['title'] = $this->l('Block Info User');
        }

        self::$bCompare15 = version_compare(_PS_VERSION_, '1.5', '>=');
        self::$bCompare1550 = version_compare(_PS_VERSION_, '1.5.5.0', '>=');
        self::$bCompare16 = version_compare(_PS_VERSION_, '1.6', '>=');
        self::$bCompare17 = version_compare(_PS_VERSION_, '1.7', '>=');
        self::$bCompare1710 = version_compare(_PS_VERSION_, '1.7.1', '>=');
        self::$bCompare1766 = version_compare(_PS_VERSION_, '1.7.6.6', '>=');
        self::$bCompare1770 = version_compare(_PS_VERSION_, '1.7.7', '>=');


        if (!empty(self::$bCompare17) || !empty(self::$bCompare16)) {
            $this->bootstrap = true;
        }

        // get configuration options
        BT_FPCModuleTools::getConfiguration(array(
            'FBPSC_ACCOUNT_TEXT',
            'FBPSC_VOUCHER_DESC',
            'FBPSC_VOUCHER_DISP_TEXT',
            'FBPSC_BADGE_TOP_CSS',
            'FBPSC_BADGE_BLOCKUSER_CSS',
            'FBPSC_BADGE_AUTHENTICATION_CSS',
            'FBPSC_BADGE_FOOTER_CSS',
            'FBPSC_BADGE_ORDERFUNNEL_CSS'
        ));

        // get call mode - Ajax or dynamic - used for clean headers and footer in ajax request
        self::$sQueryMode = Tools::getValue('sMode');
    }

    /**
     * installs all mandatory structure (DB or Files) => sql queries and update values and hooks registered
     *
     * @return bool
     */
    public function install()
    {
        require_once(_FPC_PATH_CONF . 'install.conf.php');
        require_once(_FPC_PATH_LIB_INSTALL . 'install-ctrl_class.php');

        // set return
        $bReturn = true;

        if (!parent::install()
            || !BT_InstallCtrl::run('install', 'sql', _FPC_PATH_SQL . _FPC_INSTALL_SQL_FILE)
            || !BT_InstallCtrl::run('install', 'config')
        ) {
            $bReturn = false;
        }

        return $bReturn;
    }

    /**
     * uninstalls all mandatory structure (DB or Files)
     *
     * @return bool
     */
    public function uninstall()
    {
        require_once(_FPC_PATH_CONF . 'install.conf.php');
        require_once(_FPC_PATH_LIB_INSTALL . 'install-ctrl_class.php');

        // set return
        $bReturn = true;

        if (!parent::uninstall()
            || !BT_InstallCtrl::run('uninstall', 'sql', _FPC_PATH_SQL . _FPC_UNINSTALL_SQL_FILE)
            || !BT_InstallCtrl::run('uninstall', 'config')
        ) {
            $bReturn = false;
        }

        return $bReturn;
    }

    /**
     * manages all data in Back Office
     *
     * @return string
     */
    public function getContent()
    {
        require_once(_FPC_PATH_CONF . 'admin.conf.php');
        require_once(_FPC_PATH_LIB_ADMIN . 'admin-ctrl_class.php');

        // set
        $aUpdateModule = array();

        try {
            // update new module keys
            BT_FPCModuleTools::updateConfiguration();

            // set js msg translation
            BT_FPCModuleTools::translateJsMsg();

            // instantiate admin controller object
            $oAdmin = new BT_AdminCtrl();

            // defines type to execute
            // use case : no key sAction sent in POST mode (no form has been posted => first page is displayed with admin-display.class.php)
            // use case : key sAction sent in POST mode (form or ajax query posted ).
            $sAction = (!Tools::getIsset('sAction') || (Tools::getIsset('sAction') && 'display' == Tools::getValue('sAction'))) ? (Tools::getIsset('sAction') ? Tools::getValue('sAction') : 'display') : Tools::getValue('sAction');

            // make module update only in case of display general admin page
            if ($sAction == 'display' && !Tools::getIsset('sType')) {
                // update module if necessary
                $aUpdateModule = $this->updateModule();
            }

            // execute good action in admin
            // only displayed with key : tpl and assign in order to display good smarty template
            $aDisplay = $oAdmin->run($sAction, array_merge($_GET, $_POST));

            // free memory
            unset($oAdmin);

            if (!empty($aDisplay)) {
                $aDisplay['assign'] = array_merge($aDisplay['assign'], array(
                    'aUpdateErrors' => $aUpdateModule,
                    'oJsTranslatedMsg' => BT_FPCModuleTools::jsonEncode($GLOBALS['FBPSC_JS_MSG']),
                    'bAddJsCss' => true
                ));

                // get content
                $sContent = $this->displayModule($aDisplay['tpl'], $aDisplay['assign']);

                if (!empty(self::$sQueryMode)) {
                    echo $sContent;
                } else {
                    return $sContent;
                }
            } else {
                throw new Exception('action returns empty content', 110);
            }
        } catch (Exception $e) {
            $this->aErrors[] = array('msg' => $e->getMessage(), 'code' => $e->getCode());

            // get content
            $sContent = $this->displayErrorModule();

            if (!empty(self::$sQueryMode)) {
                echo $sContent;
            } else {
                return $sContent;
            }
        }
        // exit clean with XHR mode
        if (!empty(self::$sQueryMode)) {
            exit(0);
        }
    }

    /**
     * displays customized module content on header
     *
     * @return string
     */
    public function hookHeader(array $aParams = null)
    {
        return $this->execHook('display', 'header');
    }

    /**
     * displays customized module content on header
     *
     * @return string
     */
    public function hookDisplayHeader()
    {
        return $this->execHook('display', 'header');
    }

    /**
     * displays customized module content on top
     *
     * @return string
     */
    public function hookTop()
    {
        return $this->execHook('display', 'top');
    }

    /**
     * displays customized module content on top
     *
     * @return string
     */
    public function hookDisplayTop()
    {
        return $this->execHook('display', 'top');
    }

    /**
     * displays snippets for product page on left column
     *
     * @return string
     */
    public function hookDisplayLeftColumn()
    {
        if (!empty(FacebookPsConnect::$conf['FBPSC_DISPLAY_BLOCK'])) {
            return $this->execHook('display', 'left');
        }
    }

    /**
     * displays snippets for product page on left column
     *
     * @return string
     */
    public function hookLeftColumn()
    {
        if (!empty(FacebookPsConnect::$conf['FBPSC_DISPLAY_BLOCK'])) {
            return $this->execHook('display', 'left');
        }
    }

    /**
     * displays snippets for product page on right column
     *
     * @return string
     */
    public function hookDisplayRightColumn()
    {
        if (!empty(FacebookPsConnect::$conf['FBPSC_DISPLAY_BLOCK'])) {
            return $this->execHook('display', 'right');
        }
    }

    /**
     * displays snippets for product page on right column
     *
     * @return string
     */
    public function hookRightColumn()
    {
        return $this->execHook('display', 'right');
    }

    /**
     * displays customized module content on footer
     *
     * @return string
     */
    public function hookFooter()
    {
        return $this->execHook('display', 'footer');
    }

    /**
     * displays customized module content on footer
     *
     * @return string
     */
    public function hookDisplayFooter()
    {
        return $this->execHook('display', 'footer');
    }

    /**
     * displays PS customer account
     *
     * @return string
     */
    public function hookDisplayCustomerAccount()
    {
        return $this->execHook('display', 'account');
    }

    /**
     * display the content generated by the shortcode
     *
     * @param array $aParams
     * @return string
     */
    public function hookDisplayShortCode(array $aParams = null)
    {
        return $this->execHook('display', 'shortCode', $aParams);
    }

    /**
     * displays PS customer account
     *
     * @return string
     */
    public function hookCustomerAccount()
    {
        return $this->execHook('display', 'account');
    }

    /**
     * displays PS customer account form
     *
     * @return string
     */
    public function displayCustomerAccountForm()
    {
        return $this->execHook('display', 'authentication');
    }

    /**
     * displays PS checkout funnel buttons
     *
     * @return string
     */
    public function hookDisplayReassurance()
    {
        return $this->execHook('display', 'checkout17');
    }

    /**
     * displays PS customer account form
     *
     * @return string
     */
    public function createAccountForm()
    {
        return $this->execHook('display', 'authentication');
    }

    /**
     * exec social callback
     *
     * @param array array
     * @return string
     */
    public function hookConnectorCallback(array $aParams)
    {
        return $this->execHook('action', 'callback', $aParams);
    }

    /**
     * connect social connector
     *
     * @param array array
     * @return string
     */
    public function hookConnectorConnect(array $aParams)
    {
        return $this->execHook('action', 'connect', $aParams);
    }

    /**
     * update customer facebook association preferences
     *
     * @param array array
     * @return string
     */
    public function hookCustomerAssociation(array $aParams)
    {
        return $this->execHook('action', 'updateCustomer', $aParams);
    }

    /**
     * update customer e-mail
     *
     * @param array array
     * @return string
     */
    public function hookCustomerEmail(array $aParams)
    {
        return $this->execHook('action', 'updateEmail', $aParams);
    }

    /**
     * displays selected hook content
     *
     * @param string $sHookType
     * @param array $aParams
     * @return string
     */
    private function execHook($sHookType, $sAction, array $aParams = array())
    {
        // include
        require_once(_FPC_PATH_CONF . 'hook.conf.php');
        require_once(_FPC_PATH_LIB_HOOK . 'hook-ctrl_class.php');

        try {
            // define which hook class is executed in order to display good content in good zone in shop
            $oHook = new BT_FPCHookCtrl($sHookType, $sAction);

            // displays good block content
            $aDisplay = $oHook->run($aParams);

            // free memory
            unset($oHook);

            //execute good action in admin
            //only displayed with key : tpl and assign in order to display good smarty template
            if (!empty($aDisplay)) {
                return $this->displayModule($aDisplay['tpl'], $aDisplay['assign']);
            } else {
                throw new Exception('Choosen hook returns empty content', 110);
            }
        } catch (Exception $e) {
            $this->aErrors[] = array('msg' => $e->getMessage(), 'code' => $e->getCode());

            return $this->displayErrorModule();
        }
    }


    /**
     * manages module error
     */
    public function setErrorHandler($iErrno, $sErrstr, $sErrFile, $iErrLine, $aErrContext)
    {
        switch ($iErrno) {
            case E_USER_ERROR:
                $this->aErrors[] = array(
                    'msg' => 'Fatal error <b>' . $sErrstr . '</b>',
                    'code' => $iErrno,
                    'file' => $sErrFile,
                    'line' => $iErrLine,
                    'context' => $aErrContext
                );
                break;
            case E_USER_WARNING:
                $this->aErrors[] = array(
                    'msg' => 'Warning <b>' . $sErrstr . '</b>',
                    'code' => $iErrno,
                    'file' => $sErrFile,
                    'line' => $iErrLine,
                    'context' => $aErrContext
                );
                break;
            case E_USER_NOTICE:
                $this->aErrors[] = array(
                    'msg' => 'Notice <b>' . $sErrstr . '</b>',
                    'code' => $iErrno,
                    'file' => $sErrFile,
                    'line' => $iErrLine,
                    'context' => $aErrContext
                );
                break;
            default:
                $this->aErrors[] = array(
                    'msg' => 'Unknow error <b>' . $sErrstr . '</b>',
                    'code' => $iErrno,
                    'file' => $sErrFile,
                    'line' => $iErrLine,
                    'context' => $aErrContext
                );
                break;
        }
        return $this->displayErrorModule();
    }

    /**
     * displays view
     *
     * @param string $sTplName
     * @param array $aAssign
     * @return string html
     */
    public function displayModule($sTplName, $aAssign)
    {
        if (file_exists(_FPC_PATH_TPL . $sTplName) && is_file(_FPC_PATH_TPL . $sTplName)) {
            // set assign module name
            $aAssign = array_merge($aAssign, array(
                'sModuleName' => Tools::strtolower(_FPC_MODULE_NAME),
                'bDebug' => _FPC_DEBUG,
                'iCompare' => version_compare(_PS_VERSION_, '1.4.1')
            ));

            Context::getContext()->smarty->assign($aAssign);

            return $this->display(__FILE__, _FPC_PATH_TPL_NAME . $sTplName);
        } else {
            throw new Exception('Template "' . $sTplName . '" doesn\'t exists', 120);
        }
    }

    /**
     * displays view with error
     *
     * @param string $sTplName
     * @param array $aAssign
     * @return string html
     */
    public function displayErrorModule()
    {
        Context::getContext()->smarty->assign(
            array(
                'sHomeURI' => BT_FPCModuleTools::truncateUri(),
                'aErrors' => $this->aErrors,
                'sModuleName' => Tools::strtolower(_FPC_MODULE_NAME),
                'bDebug' => _FPC_DEBUG,
            )
        );

        return $this->display(__FILE__, _FPC_PATH_TPL_NAME . _FPC_TPL_COMMON_PATH . _FPC_TPL_ERROR);
    }

    /**
     * updates module as necessary
     *
     * @return array
     */
    public function updateModule()
    {
        require(_FPC_PATH_LIB . 'module-update_class.php');

        // check if update tables
        BT_FpcModuleUpdate::create()->run(array('sType' => 'tables'));

        // check if update fields
        BT_FpcModuleUpdate::create()->run(array('sType' => 'fields'));

        // check if update hooks
        BT_FpcModuleUpdate::create()->run(array('sType' => 'hooks'));

        // check if update templates
        BT_FpcModuleUpdate::create()->run(array('sType' => 'templates'));

        // check if update templates
        BT_FpcModuleUpdate::create()->run(array('sType' => 'position'));

        // Force buttons size to be large to comply with most of the connectors guidelines
        BT_FpcModuleUpdate::create()->run(array('sType' => 'buttonsSize'));

        return BT_FpcModuleUpdate::create()->aErrors;
    }
}
