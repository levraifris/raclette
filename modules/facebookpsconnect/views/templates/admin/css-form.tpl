{*
*
* Social Login
*
* @author BusinessTech.fr
* @copyright Business Tech
*
*           ____    _______
*          |  _ \  |__   __|
*          | |_) |    | |
*          |  _ <     | |
*          | |_) |    | |
*          |____/     |_|
*
*}
<style type="text/css">
	.panel-heading .accordion-toggle:after {
		font-family: FontAwesome;
		content: "\f077";
		float: right;
	}
	.panel-heading .accordion-toggle.collapsed:after {
		content: "\f078";
	}
</style>
{if !empty($aErrors)}
	{include file="`$sErrorInclude`"}
	{* USE CASE - edition review mode *}
{else}
	<div id="connector-position " class="bootstrap col-xs-12" style="width: 860px; height: 900px;">
		<div id="fbpsc">
			<form  style="width: 860px; height: 700px;" class="form-horizontal col-xs-12" method="post" id="fbpscCssForm" name="fbpscCssForm" onsubmit="fbpsc.form('fbpscCssForm', '{$sURI|escape:'htmlall':'UTF-8'}', null, 'fbpscCssForm', 'fbpscCssForm', false, true, '', 'CssForm', 'LoadingCssDiv');return false;">
				<input type="hidden" name="{$sCtrlParamName|escape:'htmlall':'UTF-8'}" value="{$sController|escape:'htmlall':'UTF-8'}" />
				<input type="hidden" name="sAction" value="{$aQueryParams.cssCode.action|escape:'htmlall':'UTF-8'}" />
				<input type="hidden" name="sType" value="{$aQueryParams.cssCode.type|escape:'htmlall':'UTF-8'}" />
				{if !empty($sHook)}
					<input type="hidden" name="sHook" value="{$sHook}" id="sHook" />
					<input type="hidden" name="sHookId" value="{$sHookId}" id="sHook" />
				{/if}

				<h2 class="text-center">{l s='CSS for the hook:' mod='facebookpsconnect'} {$sHook}</h2>

				<div class="clr_10"></div>

				{*USE CASE *}
				<div id="bt_css_config">

				<div class="panel-group" id="accordion">
					<div class="panel panel-default panel-primary">
						<div class="panel-heading">
							<h4 class="panel-title">
								<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
									<i class="fa fa-desktop"></i>&nbsp;{l s='Border management' mod='facebookpsconnect'}
								</a>
							</h4>
						</div>
						<div id="collapseOne" class="panel-collapse collapse in">
							<div class="panel-body">
								<div id="ColorPickerBorder"class="input-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<div class="form-group" >
										<label class="control-label col-xs-12 col-sm-12 col-md-6 col-lg-3">{l s='Border color' mod='facebookpsconnect'} :</label>
										<div class="text-center col-xs-12 col-sm-12 col-md-6 col-lg-3">
											<input type="color" name="sCssBorderColor" {if !empty($sCssBorderColor)} value="{$sCssBorderColor}" {/if} id="sCssBorderColor" class="color mColorPicker" required="required"/>
										</div>
									</div>
								</div>

								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<div class="form-group" >
										<label class="control-label col-xs-12 col-sm-12 col-md-6 col-lg-3">{l s='Border size' mod='facebookpsconnect'} :</label>
										<div class="col-xs-12 col-sm-12 col-md-6 col-lg-3">
											<input type="text" name="sCssBorderSize"  value="{$sCssBorderSize}" id="sCssBorderSize" required="required"/>
										</div>
										<strong>px</strong>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="panel panel-default panel-primary">
						<div class="panel-heading">
							<h4 class="panel-title">
								<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
									<i class="fa fa-text-height"></i>&nbsp;{l s='Background' mod='facebookpsconnect'}
								</a>
							</h4>
						</div>
						<div id="collapseTwo" class="panel-collapse collapse">
							<div class="panel-body">
								<div id="ColorPickerBackground"class="input-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<div class="form-group" >
										<label class="control-label col-xs-12 col-sm-12 col-md-6 col-lg-3">{l s='Background color' mod='facebookpsconnect'} :</label>
										<div class="text-center col-xs-12 col-sm-12 col-md-6 col-lg-3">
											<input type="color" name="sCssBackgroundColor" {if !empty($sCssBackgroundColor)} value="{$sCssBackgroundColor}" {/if} id="sCssBackgroundColor" class="color mColorPicker" required="required"/>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
                    {if !empty($bDisplayCustomText)}
						<div class="panel panel-default panel-primary">
							<div class="panel-heading">
								<h4 class="panel-title">
									<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
										<i class="fa fa-text-height"></i>&nbsp;{l s='Text' mod='facebookpsconnect'}
									</a>
								</h4>
							</div>
								<div id="collapseThree" class="panel-collapse collapse">
								<div class="panel-body">
									<div class="form-group">
										<label class="control-label col-lg-3"><span class="label-tooltip" title data-original-title="{l s='Select "No" if you want to hide the custom text for this position' mod='facebookpsconnect'}" data-toggle="tooltip"><b>{l s='Activate the custom text display' mod='facebookpsconnect'}</b></span> :</label>
										<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
											<span class="switch prestashop-switch fixed-width-md">
												<input type="radio" name="bDisplayBlock" id="bDisplayBlock_on" {if $bDisplayBlock == 1} checked="checked" {/if} value="1" />
												<label for="bDisplayBlock_on" class="radioCheck">
													{l s='Yes' mod='facebookpsconnect'}
												</label>
												<input type="radio" name="bDisplayBlock" id="bDisplayBlock_off" {if $bDisplayBlock == 0} checked="checked" {/if} value="0" />
												<label for="bDisplayBlock_off" class="radioCheck">
													{l s='No' mod='facebookpsconnect'}
												</label>
												<a class="slide-button btn"></a>
											</span>
										</div>
										<span class="label-tooltip" data-toggle="tooltip" title data-original-title="{l s='Select "No" if you want to hide the custom text for this position' mod='facebookpsconnect'}">&nbsp;<span class="icon-question-sign"></span></span>
									</div>
									<div id="ColorPickerText"class="input-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
										<div class="form-group" >
											<label class="control-label col-xs-12 col-sm-12 col-md-6 col-lg-3">{l s='Text color' mod='facebookpsconnect'} :</label>
											<div class="text-center col-xs-12 col-sm-12 col-md-6 col-lg-3">
												<input type="color" class="aoColorPickerFancy" name="sCssTextColor" {if !empty($sCssTextColor)} value="{$sCssTextColor}" {/if} id="sCssTextColor" class="color mColorPicker" required="required"/>
											</div>
										</div>
									</div>

									<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
										<div class="form-group" >
											<label class="control-label col-xs-12 col-sm-12 col-md-6 col-lg-3">{l s='Text size' mod='facebookpsconnect'} :</label>
											<div class="col-xs-12 col-sm-12 col-md-6 col-lg-3">
												<input type="text"  name="sCssTextSize"  value="{$sCssTextSize}" id="sCssTextSize" required="required"/>
											</div>
											<strong>px</strong>
										</div>
									</div>
								</div>
							</div>
						</div>
                    {/if}

					<div class="panel panel-default panel-primary">
						<div class="panel-heading">
							<h4 class="panel-title">
								<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseFour">
									<i class="fa fa-arrows-alt"></i>&nbsp;{l s='Padding' mod='facebookpsconnect'}
								</a>
							</h4>
						</div>
						<div id="collapseFour" class="panel-collapse collapse">
							<div class="panel-body">
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<div class="form-group" >
										<label class="control-label col-xs-12 col-sm-12 col-md-6 col-lg-3">{l s='Padding top' mod='facebookpsconnect'} :</label>
										<div class="col-xs-12 col-sm-12 col-md-6 col-lg-3">
											<input type="text"  name="sCssPaddingTop"  value="{$sCssPaddingTop}"  id="sCssPaddingTop" required="required"/>
										</div>
										<strong>px</strong>
									</div>
								</div>

								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<div class="form-group" >
										<label class="control-label col-xs-12 col-sm-12 col-md-6 col-lg-3">{l s='Padding right' mod='facebookpsconnect'} :</label>
										<div class="col-xs-12 col-sm-12 col-md-6 col-lg-3">
											<input type="text"  name="sCssPaddingRight"  value="{$sCssPaddingRight}"  id="sCssPaddingRight" required="required"/>
										</div>
										<strong>px</strong>
									</div>
								</div>

								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<div class="form-group" >
										<label class="control-label col-xs-12 col-sm-12 col-md-6 col-lg-3">{l s='Padding bottom' mod='facebookpsconnect'} :</label>
										<div class="col-xs-12 col-sm-12 col-md-6 col-lg-3">
											<input type="text"  name="sCssPaddingBottom"  value="{$sCssPaddingBottom}"  id="sCssPaddingBottom" required="required"/>
										</div>
										<strong>px</strong>
									</div>
								</div>

								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<div class="form-group" >
										<label class="control-label col-xs-12 col-sm-12 col-md-6 col-lg-3">{l s='Padding left' mod='facebookpsconnect'} :</label>
										<div class="col-xs-12 col-sm-12 col-md-6 col-lg-3">
											<input type="text"  name="sCssPaddingLeft"  value="{$sCssPaddingLeft}" id="sCssPaddingLeft" required="required"/>
										</div>
										<strong>px</strong>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="panel panel-default panel-primary">
					<div class="panel-heading">
						<h4 class="panel-title">
							<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseFive">
								<i class="fa fa-arrows-h"></i>&nbsp;{l s='Margin' mod='facebookpsconnect'}
							</a>
						</h4>
					</div>
					<div id="collapseFive" class="panel-collapse collapse">
						<div class="panel-body">
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<div class="form-group" >
									<label class="control-label col-xs-12 col-sm-12 col-md-6 col-lg-3">{l s='Margin top' mod='facebookpsconnect'} :</label>
									<div class="col-xs-12 col-sm-12 col-md-6 col-lg-3">
										<input type="text"  name="sCssMarginTop"  value="{$sCssMarginTop}"  id="sCssMarginTop" required="required"/>
									</div>
									<strong>px</strong>
								</div>
							</div>

							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<div class="form-group" >
									<label class="control-label col-xs-12 col-sm-12 col-md-6 col-lg-3">{l s='Margin right' mod='facebookpsconnect'} :</label>
									<div class="col-xs-12 col-sm-12 col-md-6 col-lg-3">
										<input type="text"  name="sCssMarginRight" value="{$sCssMarginRight}" id="sCssMarginRight" required="required"/>
									</div>
									<strong>px</strong>
								</div>
							</div>

							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<div class="form-group" >
									<label class="control-label col-xs-12 col-sm-12 col-md-6 col-lg-3">{l s='Margin bottom' mod='facebookpsconnect'} :</label>
									<div class="col-xs-12 col-sm-12 col-md-6 col-lg-3">
										<input type="text"  name="sCssMarginBottom"  value="{$sCssMarginBottom}"  id="sCssMarginBottom" required="required"/>
									</div>
									<strong>px</strong>
								</div>
							</div>

							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<div class="form-group" >
									<label class="control-label col-xs-12 col-sm-12 col-md-6 col-lg-3">{l s='Margin left' mod='facebookpsconnect'} :</label>
									<div class="col-xs-12 col-sm-12 col-md-6 col-lg-3">
										<input type="text"  name="sCssMarginLeft"  value="{$sCssMarginLeft}"  id="sCssMarginLeft" required="required"/>
									</div>
									<strong>px</strong>
								</div>
							</div>
						</div>
					</div>
				</div>
				</div>
			</div>

			<div class="clr_10"></div>
			<div class="clr_hr"></div>
			<div class="clr_10"></div>

			<div class="row">
				<div class="col-xs-8">
					<div id="{$sModuleName|escape:'htmlall':'UTF-8'}CssFormError"></div>
				</div>
				<div class="col-xs-4">
					<p style="text-align: center !important;">
						<button  name="{$sModuleName|escape:'htmlall':'UTF-8'}CommentButton" class="btn btn-default pull-right"onclick="fbpsc.form('fbpscCssForm', '{$sURI|escape:'htmlall':'UTF-8'}', null, 'fbpscCssForm', 'fbpscCssForm', false, true, '', 'CssForm', 'LoadingCssDiv');return false;" ><i class="icon icon-save icon-2x"></i><br/>{l s='Save' mod='facebookpsconnect'}</button>
					</p>
				</div>
			</div>
		</form>

			<div id="LoadingCssDiv" style="display: none;">
				<div class="alert alert-info">
					<p style="text-align: center !important;"><img src="{$sLoadingImg|escape:'htmlall':'UTF-8'}" alt="Loading" /></p><div class="clr_20"></div>
					<p style="text-align: center !important;">{l s='Your configuration updating is in progress...' mod='facebookpsconnect'}</p>
				</div>
			</div>
		</div>
	</div>
{/if}

{literal}
	<script type="text/javascript">

	$(document).ready(function () {
		$(function() {
			$(".label-tooltip").tooltip();
		});


		});
	</script>
{/literal}