{*
*
* Social Login
*
* @author BusinessTech.fr
* @copyright Business Tech
*
*           ____    _______
*          |  _ \  |__   __|
*          | |_) |    | |
*          |  _ <     | |
*          | |_) |    | |
*          |____/     |_|
*
*}

<h3><i class="fa fa-eye"></i>&nbsp;&nbsp;{l s='Design management in native hooks'  mod='facebookpsconnect'}</h3>

<div class="form-group">
	<div class="col-xs-12"}>

	<p class="alert alert-info">
		{l s='In this tab you can configure the connectors block design for each native hook by clicking on the icon:' mod='facebookpsconnect'}&nbsp;&nbsp;<i class="fa fa-2x fa-edit"></i>
	</p>
	<table style="width: 100%;" id="fbpsctabs" class="table table-condensed table-hover" cellpadding="0" cellspacing="0">
		<thead>
		<tr class="nodrag nodrop">
			<th class="center"><b>{l s='Hook name' mod='facebookpsconnect'}</b></th>
			<th class="center"><b>{l s='Edit CSS' mod='facebookpsconnect'}</b></th>
		</tr>
		</thead>
		<tbody>
		{foreach from=$aHooks name=hook key=hName item=hValue}
			{if $hValue.name|escape:'htmlall':'UTF-8' != 'Checkout funnel for PS 1.7' && $hValue.name != 'displayRightColumn' &&  $hValue.name != 'displayLeftColumn'}
				{if $hValue.status}
					<tr id="tr_{$smarty.foreach.connector.iteration|intval}">
						<td class="center">
							<div class="badge badge-info"><i class="icon icon-anchor"></i>&nbsp;{$hValue.title|escape:'htmlall':'UTF-8'}</div>
						</td>
						<td class="center">
							<a id="hookEdit" href="{$sURI}&sAction={$aQueryParams.cssForm.action|escape:'htmlall':'UTF-8'}&sType={$aQueryParams.cssForm.type|escape:'htmlall':'UTF-8'}&sHookId={$hName|escape:'htmlall':'UTF-8'}" class="btn btn-mini btn-success fancybox.ajax"><i class="icon icon-edit fancybox.ajax"></i> </a>
						</td>
					</tr>
				{/if}
			{/if}
		{/foreach}
		</tbody>
	</table>
</div>
</div>
<div class="clr_20"></div>


{literal}
	<script type="text/javascript">
		$(document).ready(function()
		{
			$("a#hookEdit").fancybox({
				'hideOnContentClick' : false
				});
		});
	</script>
{/literal}
