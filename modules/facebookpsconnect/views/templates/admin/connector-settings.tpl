{*
*
* Social Login
*
* @author BusinessTech.fr
* @copyright Business Tech
*
*           ____    _______
*          |  _ \  |__   __|
*          | |_) |    | |
*          |  _ <     | |
*          | |_) |    | |
*          |____/     |_|
*
*}

<h3><i class="fa fa-facebook-official"></i>&nbsp;&nbsp;{l s='Social login buttons configuration'  mod='facebookpsconnect'}</h3>

<div class="form-group" id="fbpsc">
	<div class="col-xs-12">
		<table id="fbpsctabs" class="table table-responsive" cellpadding="0" cellspacing="0">
			<thead>
			<tr class="nodrag nodrop">
				<th class="center"><b>{l s='Connector name' mod='facebookpsconnect'}</b></th>
				<th class="center"><b>{l s='Is the connector configured?' mod='facebookpsconnect'}</b></th>
				<th class="center"><b>{l s='Is the connector activated?' mod='facebookpsconnect'}</b></th>
				<th class="center"><b>{l s='Associated hook(s)' mod='facebookpsconnect'}</b></th>
				<th class="center"><b>{l s='Edit' mod='facebookpsconnect'}</b></th>
			</tr>
			</thead>
			<tbody>
			{foreach from=$aConnectors name=connector key=cName item=cValue}
				<tr id="tr_{$smarty.foreach.connector.iteration|intval}">
					<td class="center"  >
						<div class="clr_10"></div>
						{if $sButtonStyle == 'modern'}
							<a class="btn-connect btn-block-connect btn-social btn-{$cValue.cssClass|escape:'htmlall':'UTF-8'}" >
                                {if $cValue.cssClass == 'google'}
                                    <span class="btn-google-icon"></span>
                                {else}
								    <span class="fa fa-{$cValue.cssClass|escape:'htmlall':'UTF-8'}{if $cValue.cssClass == 'facebook'}-square{/if}"></span>
                                {/if}
								<span class='btn-title-connect'>{$cValue.cssClass|escape:'htmlall':'UTF-8'|ucfirst|escape:'htmlall':'UTF-8'}</span>
							</a>
						{else}
							<p class="ao_bt_fpsc {if $cValue.cssClass|escape:'htmlall':'UTF-8' == 'facebook'}ao_bt_fpsc_facebook{elseif $cValue.cssClass|escape:'htmlall':'UTF-8' == 'amazon'}ao_bt_fpsc_amazon{elseif $cValue.cssClass|escape:'htmlall':'UTF-8' == 'google'}ao_bt_fpsc_google{elseif $cValue.cssClass|escape:'htmlall':'UTF-8' == 'paypal'}ao_bt_fpsc_paypal{elseif $cValue.cssClass|escape:'htmlall':'UTF-8' == 'twitter'}ao_bt_fpsc_twitter{/if}" >
								<span class='picto'></span>
								<span class='title'>{$cValue.cssClass|escape:'htmlall':'UTF-8'|ucfirst|escape:'htmlall':'UTF-8'}</span>
							</p>
						{/if}
					</td>
					<td class="center {if $cValue.data === false}warning{else}success{/if}" >
						{if $cValue.data === false}
							<b>{l s='Not yet' mod='facebookpsconnect'}</b>
						{else}
							<b>{l s='Yes' mod='facebookpsconnect'}</b>
						{/if}
					</td>
					<td class="center {if  empty($cValue.data.activeConnector)}warning{else}success{/if}">
						{if  empty($cValue.data.activeConnector)}
							<b>{l s='Not yet' mod='facebookpsconnect'}</b>
						{else}
							<b>{l s='Yes' mod='facebookpsconnect'}</b>
						{/if}
					</td>
					<td class="center">
						{if !empty($cValue.hooks)}
							{foreach from=$cValue.hooks name=hook key=key item=hValue}<div class="badge"> <i class="fa fa-anchor"></i>&nbsp;{$hValue|escape:'htmlall':'UTF-8'}</div>&nbsp;{/foreach}
						{else}
							<div class="clr_10"></div>
							<p class="alert alert-warning">
								{l s='No associated hook' mod='facebookpsconnect'}
							</p>
						{/if}
					</td>
					<td class="center">
						<a id="ConnectorEdit" class="btn btn-mini btn-success fancybox.ajax" href="{$sURI|escape:"html"}&sAction={$aQueryParams.connectorForm.action|escape:'htmlall':'UTF-8'}&sType={$aQueryParams.connectorForm.type|escape:'htmlall':'UTF-8'}&iConnectorId={$cValue.cssClass|escape:'htmlall':'UTF-8'}"><i class="icon icon-edit"></i> </a>	
					</td>
				</tr>
			{/foreach}
			</tbody>
		</table>
	</div>
</div>

<div class="clr_20"></div>

{literal}
<script type="text/javascript">
	$(document).ready(function()
	{
		$("a#ConnectorEdit").fancybox({
			'hideOnContentClick' : false,
		});
	});
</script>
{/literal}
