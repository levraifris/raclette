{*
*
* Social Login
*
* @author BusinessTech.fr
* @copyright Business Tech
*
*           ____    _______
*          |  _ \  |__   __|
*          | |_) |    | |
*          |  _ <     | |
*          | |_) |    | |
*          |____/     |_|
*
*}

{* ADMIN DISPLAY *}

<script type="text/javascript" src="{$smarty.const._FPC_URL_JS|escape:'htmlall':'UTF-8'}Chart.js"></script>
<script type="text/javascript" src="{$smarty.const._FPC_URL_JS|escape:'htmlall':'UTF-8'}jquery-ui-1.11.4.min.js"></script>
<script type="text/javascript" src="{$smarty.const._FPC_URL_JS|escape:'htmlall':'UTF-8'}module.js"></script>
<script type="text/javascript" src="{$smarty.const._FPC_URL_JS|escape:'htmlall':'UTF-8'}/colorpicker/colorpicker.js"></script>


<link rel="stylesheet" type="text/css" href="{$smarty.const._FPC_URL_CSS|escape:'htmlall':'UTF-8'}admin.css">
<link rel="stylesheet" type="text/css" href="{$smarty.const._FPC_URL_CSS|escape:'htmlall':'UTF-8'}bootstrap-social.css">
<link rel="stylesheet" type="text/css" href="{$smarty.const._FPC_URL_CSS|escape:'htmlall':'UTF-8'}font-awesome.css">
<link rel="stylesheet" type="text/css" href="{$smarty.const._FPC_URL_CSS|escape:'htmlall':'UTF-8'}hook.css">
<link rel="stylesheet" type="text/css" href="{$smarty.const._FPC_URL_CSS|escape:'htmlall':'UTF-8'}/colorpicker/colorpicker.css">
<script type="text/javascript">
	// instantiate object
	var {$sModuleName|escape:'htmlall':'UTF-8'} = {$sModuleName|escape:'htmlall':'UTF-8'} || new FpcModule('{$sModuleName|escape:'htmlall':'UTF-8'}');

	// get errors translation
	{if !empty($oJsTranslatedMsg)}
	{$sModuleName|escape:'htmlall':'UTF-8'}.msgs = {$oJsTranslatedMsg};
	{/if}

	{if isset($iCompare) && $iCompare == -1}{$sModuleName|escape:'htmlall':'UTF-8'}.oldVersion = true;{/if}

	// set URL of admin img
	{$sModuleName|escape:'htmlall':'UTF-8'}.sImgUrl = '{$smarty.const._FPC_URL_IMG|escape:'htmlall':'UTF-8'}';

	// set URL of admin img
	{$sModuleName|escape:'htmlall':'UTF-8'}.sAdminImgUrl = '{$smarty.const._PS_ADMIN_IMG_|escape:'htmlall':'UTF-8'}';

	// set URL of module's web service
	{if !empty($sModuleURI)}
	{$sModuleName|escape:'htmlall':'UTF-8'}.sWebService = '{$sModuleURI}';
	{/if}

	$(function() {
		$(".label-tooltip").tooltip();
	});
</script>