{*
*
* Social Login
*
* @author BusinessTech.fr
* @copyright Business Tech
*
*           ____    _______
*          |  _ \  |__   __|
*          | |_) |    | |
*          |  _ <     | |
*          | |_) |    | |
*          |____/     |_|
*
*}

<div class="bootstrap">
	<div id="fbpsc">
		<div id="{$sModuleName|escape:'htmlall':'UTF-8'}ConfigureHook">
			<h2 class="text-center"><i class="fa fa-anchor"></i>&nbsp; {l s='Hook' mod='facebookpsconnect'} : {$aHook.name|escape:'htmlall':'UTF-8'}</h2>
			<div class="clr clr_10"></div>
			<div class="clr clr_hr"></div>
			<div class="clr clr_10"></div>
			<p class="alert alert-info">{l s='Simply drag and drop the desired connectors from the left-side list to the right-side drop area. You can then re-order them as desired by dragging them, or delete them by clicking the trash icon.' mod='facebookpsconnect'}</p>
			<div id="{$sModuleName|escape:'htmlall':'UTF-8'}DraggableConnector">
				<p class="text-center"><strong class="connectorTitle">{l s='Available connectors' mod='facebookpsconnect'}</strong></p>
				{if $bOneSet}
					<ul class="fbpscconnectorlist">
						{foreach from=$aConnectors name=connector key=cId item=cValue}
							{if !empty($cValue.data.activeConnector)}
								{assign var="bSetConnector" value=false}
								{if !empty($aHook.data.connectors)}
									{foreach from=$aHook.data.connectors|unserialize name=hook key=cSetId item=cTitle}
										{if $cId == $cSetId}
											{assign var="bSetConnector" value=true}
										{/if}
									{/foreach}
								{/if}
								{if !$bSetConnector}
									<li id="{$cId}" class="fbpscdragli">
										{if $cId != 'google'}
											<a class="btn btn-block btn-social btn-{$cId}">
												<span class="fa fa-{$cId}"></span>  {$cValue.title|escape:'htmlall':'UTF-8'}
											</a>
										{else}
											<a class="btn-connect btn-block-connect btn-social btn-google btn-google-drag">
                                                <span class="btn-google-icon"></span>
                                				<span>{$cValue.title|escape:'htmlall':'UTF-8'}</span> 
											</a>
										{/if}
									</li>
								{/if}
							{/if}
						{/foreach}
					</ul>
				{else}
					{l s='Please configure one widget at least' mod='facebookpsconnect'}
				{/if}
			</div>

			<div id="{$sModuleName|escape:'htmlall':'UTF-8'}DroppableConnector">
				<p class="text-center"><strong class="connectorTitle">{l s='Active connectors' mod='facebookpsconnect'}</strong></p>
				<ul id="{$sModuleName|escape:'htmlall':'UTF-8'}Sortable">
					{if !empty($aHook.data.connectors)}
						{foreach from=$aHook.data.connectors|unserialize name=hook key=cId item=cTitle}
							<li id="{$cId}" class="fbpscsortli">
								<div class="row">
									<div class="col-xs-10">
										<a class="btn btn-block btn-social btn-{$cId}">
                                            {if $cId == 'google'}
                                                <span class="btn-google-icon"></span>
                                            {else}
											    <span class="fa fa-{$cId}{if $cId == 'facebook'}-square{/if}"></span>
                                            {/if}
                                            {$cTitle|escape:'htmlall':'UTF-8'}
										</a>
									</div>
									<div class="col-xs-2">
										<button class="btn btn-default btn-mini {$sModuleName|escape:'htmlall':'UTF-8'}Garbage" onclick="{$sModuleName|escape:'htmlall':'UTF-8'}.deleteConnector($('#{$cId}'));$('#{$cId}').draggable();"> <i class="fa fa-trash"/></button>
									</div>
								</div>
							</li>
						{/foreach}
					{/if}
				</ul>
			</div>
			{if $bOneSet}
				<p class="clear">&nbsp;</p>
				<center>
					<input type="button" class="button btn btn-lg btn-success" name="{$sModuleName|escape:'htmlall':'UTF-8'}HookButton" value="{l s='Update' mod='facebookpsconnect'}" onclick="{$sModuleName|escape:'htmlall':'UTF-8'}.updateHook('{$sURI}&sAction={$aQueryParams.hook.action|escape:'htmlall':'UTF-8'}&sType={$aQueryParams.hook.type|escape:'htmlall':'UTF-8'}&sHookName={$aHook.hookTechnicalName}&sHookId={$aHook.id}', '{$sModuleName|escape:'htmlall':'UTF-8'}Sortable', '{$sModuleName|escape:'htmlall':'UTF-8'}HookList', '{$sModuleName|escape:'htmlall':'UTF-8'}HookList', true);return false;" />
				</center>
					{/if}
			<div id="{$sModuleName|escape:'htmlall':'UTF-8'}HookError">
				<div class="form-error"></div>
			</div>
		</div>

		{literal}
		<script type="text/javascript">
			// set draggable
			{/literal}{$sModuleName|escape:'htmlall':'UTF-8'}{literal}.draggableConnector();

			// set sortable
			{/literal}{$sModuleName|escape:'htmlall':'UTF-8'}{literal}.sortableConnector();

			$(function() {
				$( ".fbpscsortli" ).draggable();
			});

		</script>
		{/literal}
	</div>
</div>


