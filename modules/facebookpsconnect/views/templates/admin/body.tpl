{*
*
* Social Login
*
* @author BusinessTech.fr
* @copyright Business Tech
*
*           ____    _______
*          |  _ \  |__   __|
*          | |_) |    | |
*          |  _ <     | |
*          | |_) |    | |
*          |____/     |_|
*
*}

{* HEADER *}
{include file="`$sHeaderInclude`" iBackOffice=true}
{* /HEADER *}

{* USE CASE - module update not ok  *}
{if !empty($aUpdateErrors)}
	<div class="error">
		{foreach from=$aUpdateErrors name=condition key=nKey item=aError}
			<h3>{l s='An error occured while SQL was executed for ' mod='facebookpsconnect'} {if isset($aError.table)}{l s='table' mod='facebookpsconnect'} "{$aError.table|escape:'htmlall':'UTF-8'}" {else}{l s='field' mod='facebookpsconnect'} "{$aError.field|escape:'htmlall':'UTF-8'}" {l s='in table' mod='facebookpsconnect'} "{$aError.linked|escape:'htmlall':'UTF-8'}"{/if} </h3>
			<ol>
				<li>{l s='SQL file' mod='facebookpsconnect'} : {$aError.file|escape:'htmlall':'UTF-8'}</li>
			</ol>
		{/foreach}
		<p>{l s='Please reload this page and try again to update the SQL tables and fields or see with your web host why you are getting an SQL error' mod='facebookpsconnect'}.</p>
	</div>
{* USE CASE - display configuration ok *}
{else}
	<script>
		var id_language = Number({$iCurrentLang|intval});
		{literal}
		// load the last jquery UI js for FireFox with sortable description
		var sUserAgent = window.navigator.userAgent;

		if (sUserAgent.search("Firefox/38") != -1) {
			var element = document.createElement("script");
			element.src = "{/literal}{$smarty.const._FPC_URL_JS}{literal}jquery-ui-1.11.4.min.js";
			document.body.appendChild(element);
		}
		{/literal}
	</script>
	<div id="{$sModuleName|escape:'htmlall':'UTF-8'}" class="bootstrap">

		<div class="clr_20"></div>
		<img class="module_banner" src="{$smarty.const._FPC_URL_IMG|escape:'htmlall':'UTF-8'}admin/bt-logo-admin.jpg" alt="Social Login" />
		<div class="clr_20"></div>

		<div class="row">
			<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
				<div class="list-group workTabs">
					{* <a class="list-group-item" id="tab-0"><span class="fa fa-home"></span>&nbsp;&nbsp;{l s='Welcome' mod='facebookpsconnect'}</a> *}
					<a class="list-group-item {if $iTestSsl != 0 || $iCurlSslCheck != 2}active{/if}" id="tab-1"><span class="fa fa-check"></span>&nbsp;&nbsp;{l s='Prerequisites check' mod='facebookpsconnect'}</a>
					<a class="list-group-item {if $iTestSsl == 0 && $iCurlSslCheck == 2}active{/if}" id="tab-8"><span class="fa fa-dashboard"></span>&nbsp;&nbsp;{l s='Dashboard' mod='facebookpsconnect'}</a>
					<a class="list-group-item" id="tab-2"><span class="fa fa-heart"></span>&nbsp;&nbsp;{l s='Basic settings' mod='facebookpsconnect'}</a>
					<a class="list-group-item" id="tab-3"><span class="fa fa-plug"></span>&nbsp;&nbsp;{l s='Social login buttons configuration' mod='facebookpsconnect'}</a>
					<a class="list-group-item" id="tab-4"><span class="fa fa-anchor"></span>&nbsp;&nbsp;{l s='Connectors position management (native hooks)' mod='facebookpsconnect'}</a>
					<a class="list-group-item" id="tab-9"><span class="fa fa-eye"></span>&nbsp;&nbsp;{l s='Design management (native hooks)' mod='facebookpsconnect'}</a>
					<a class="list-group-item" id="tab-5"><span class="fa fa-camera"></span>&nbsp;&nbsp;{l s='Position advanced settings' mod='facebookpsconnect'}</a>
					<a class="list-group-item" id="tab-6"><span class="fa fa-code"></span>&nbsp;&nbsp;{l s='Position advanced settings for pro (shortcode)' mod='facebookpsconnect'}</a>
					<a class="list-group-item" id="tab-7"><span class="fa fa-shopping-cart"></span>&nbsp;&nbsp;{l s='Incentive voucher' mod='facebookpsconnect'}</a>
				</div>

				{*more tools*}
				<div class="list-group">
					<a class="list-group-item" target="_blank" href="{$smarty.const._FPC_BT_FAQ_MAIN_URL|escape:'htmlall':'UTF-8'}/{$sFaqLang|escape:'htmlall':'UTF-8'}/product/61-social-login"><span class="icon-info-circle"></span>&nbsp;&nbsp;{l s='Online FAQ' mod='facebookpsconnect'}</a>
					<a class="list-group-item" target="_blank" href="{$sContactUs|escape:'htmlall':'UTF-8'}"><span class="icon-user"></span>&nbsp;&nbsp;{l s='Contact support' mod='facebookpsconnect'}</a>
				</div>

				{* rate *}
				<div class="list-group">
					<a class="list-group-item" target="_blank" href="{$sRateUrl|escape:'htmlall':'UTF-8'}"><i class="icon-star" style="color: #fbbb22;"></i>&nbsp;&nbsp;{l s='Rate me' mod='facebookpsconnect'}</a>
				</div>

				{*module version*}
				<div class="list-group"">
					<a class="list-group-item" href="#"><span class="icon icon-info"></span>&nbsp;&nbsp;{l s='Version' mod='facebookpsconnect'} : {$sModuleVersion}</a>
				</div>

			</div>
			<div class="col-xs-12 col-sm-10 col-md-10 col-lg-10">
				<div class="tab-content">
					{if !empty($bMultiShop)}
					<div class="alert alert-danger">
						{l s='First of all, you cannot configure your module in the "all shops" or "group mode", you should select one of your shop before moving on into the configuration' mod='facebookpsconnect'}
					</div>
					{else}
                        {* <div id="content-tab-0" class="tab-pane panel in active information">
                            <h3><i class=icon-home></i>&nbsp;&nbsp;{l s='Welcome'  mod='facebookpsconnect'}</h3>

                            <div class="alert alert-success">
                                {l s='Welcome and thank you for purchasing our module. Please read the documentation carefully ("Documentation" and "Online FAQ" tabs)' mod='facebookpsconnect'}
                            </div>

                            <div class="row">
                                <div class="col-xs-12">
                                    <a target="blank" href="{$sCrossSellingUrl}"><img class="bt-effect img-responsive" src="{$sCrossSellingImg}"/></a>
                                </div>
                            </div>

                        </div> *}

						<div id="content-tab-1" class="tab-pane panel {if $iTestSsl != 0 || $iCurlSslCheck != 2}in active{/if}">
							<div id="{$sModuleName|escape:'htmlall':'UTF-8'}">
								{include file="`$sPrerequisitesCheck`"}
							</div>
						</div>

						<div id="content-tab-2" class="tab-pane panel">
							{if $iTestSsl == 0 && $iCurlSslCheck == 2}
								<div id="BasicSettings">
									{include file="`$sBasicsInclude`"}
								</div>
								<div id="loadingBasicsDiv" style="display: none;">
									<div class="alert alert-info">
										<p style="text-align: center !important;"><img src="{$sLoadingImg|escape:'htmlall':'UTF-8'}" alt="Loading" /></p><div class="clr_20"></div>
										<p style="text-align: center !important;">{l s='Your update configuration is in progress' mod='facebookpsconnect'}</p>
									</div>
								</div>
							{else}
								<div class="clr_20"></div>
								<div class="alert alert-danger">
									{l s='Please first go to the "Prerequisites Check" tab and run the cURL test in order to access the basic settings configuration.' mod='facebookpsconnect'}
								</div>
							{/if}
						</div>

						<div id="content-tab-3" class="tab-pane panel">
							{if $iTestSsl == 0 && $iCurlSslCheck == 2}
								<div id="{$sModuleName|escape:'htmlall':'UTF-8'}ConnectorList">
									{include file="`$sConnectorInclude`"}
								</div>
							{else}
								<div class="clr_20"></div>
								<div class="alert alert-danger">
									{l s='Please first go to the "Prerequisites Check" tab and run the cURL test in order to access the basic settings configuration.' mod='facebookpsconnect'}
								</div>
							{/if}
						</div>

						<div id="content-tab-4" class="tab-pane panel">
							{if $iTestSsl == 0 && $iCurlSslCheck == 2}
								<div id="{$sModuleName|escape:'htmlall':'UTF-8'}HookList">
									{include file="`$sHookInclude`"}
								</div>
								<div id="loadingHookDiv" style="display: none;">
									<div class="alert alert-info">
										<p style="text-align: center !important;"><img src="{$sLoadingImg|escape:'htmlall':'UTF-8'}" alt="Loading" /></p><div class="clr_20"></div>
										<p style="text-align: center !important;">{l s='Your update configuration is in progress' mod='facebookpsconnect'}</p>
									</div>
								</div>
							{else}
								<div class="clr_20"></div>
								<div class="alert alert-danger">
									{l s='Please first go to the "Prerequisites Check" tab and run the cURL test in order to access the basic settings configuration.' mod='facebookpsconnect'}
								</div>
							{/if}
						</div>

						<div id="content-tab-5" class="tab-pane panel">
							{if $iTestSsl == 0 && $iCurlSslCheck == 2}
								<div id="{$sModuleName|escape:'htmlall':'UTF-8'}HookAdvanced">
									{include file="`$sHookAdvancedInclude`"}
								</div>
								<div id="loadingHookAdvancedDiv" style="display: none;">
									<div class="alert alert-info">
										<p style="text-align: center !important;"><img src="{$sLoadingImg|escape:'htmlall':'UTF-8'}" alt="Loading" /></p><div class="clr_20"></div>
										<p style="text-align: center !important;">{l s='Your update configuration is in progress' mod='facebookpsconnect'}</p>
									</div>
								</div>
							{else}
								<div class="clr_20"></div>
								<div class="alert alert-danger">
									{l s='Please first go to the "Prerequisites Check" tab and run the cURL test in order to access the basic settings configuration.' mod='facebookpsconnect'}
								</div>
							{/if}
						</div>

						<div id="content-tab-6" class="tab-pane panel">
							{if $iTestSsl == 0 && $iCurlSslCheck == 2}
								<div id="{$sModuleName|escape:'htmlall':'UTF-8'}ShortCode">
									{include file="`$sShortCodeInclude`"}
								</div>
								<div id="loadingShortCode" style="display: none;">
									<div class="alert alert-info">
										<p style="text-align: center !important;"><img src="{$sLoadingImg|escape:'htmlall':'UTF-8'}" alt="Loading" /></p><div class="clr_20"></div>
										<p style="text-align: center !important;">{l s='Your update configuration is in progress' mod='facebookpsconnect'}</p>
									</div>
								</div>
							{else}
								<div class="clr_20"></div>
								<div class="alert alert-danger">
									{l s='Please first go to the "Prerequisites Check" tab and run the cURL test in order to access the basic settings configuration.' mod='facebookpsconnect'}
								</div>
							{/if}
						</div>

						<div id="content-tab-7" class="tab-pane panel">
							<div id="VouchersSettingsBlock">
								{include file="`$sVoucherInclude`"}
							</div>
							<div id="loadingVoucherDiv" style="display: none;">
								<div class="alert alert-info">
									<p style="text-align: center !important;"><img src="{$sLoadingImg|escape:'htmlall':'UTF-8'}" alt="Loading" /></p><div class="clr_20"></div>
									<p style="text-align: center !important;">{l s='Your update configuration is in progress' mod='facebookpsconnect'}</p>
								</div>
							</div>
						</div>

						<div id="content-tab-8" class="tab-pane panel {if $iTestSsl == 0 && $iCurlSslCheck == 2}in active{/if}">
							<div id="DashboardSettingsBlock">
								{include file="`$sDashboardInclude`"}
							</div>
							<div id="loadingDashboardDiv" style="display: none;">
								<div class="alert alert-info">
									<p style="text-align: center !important;"><img src="{$sLoadingImg|escape:'htmlall':'UTF-8'}" alt="Loading" /></p><div class="clr_20"></div>
									<p style="text-align: center !important;">{l s='Your update configuration is in progress' mod='facebookpsconnect'}</p>
								</div>
							</div>
						</div>

						<div id="content-tab-9" class="tab-pane panel">
							<div id="CssCodeBlock">
								{include file="`$sCssInclude`"}
							</div>
						</div>
					{/if}
				</div>
			</div>
		</div>

		</div>
	</div>

{literal}
	<script type="text/javascript">
		$(document).ready(function() {
			$('#content').removeClass('nobootstrap');
			$('#content').addClass('bootstrap');
			$(".workTabs a").click(function(e) {
				e.preventDefault();
				// currentId is the current workTabs id
				var currentId = $(".workTabs a.active").attr('id').substr(4);
				// id is the wanted workTabs id
				var id = $(this).attr('id').substr(4);

				if ($(this).attr("id") != $(".workTabs a.active").attr('id')) {
					$(".workTabs a[id='tab-"+currentId+"']").removeClass('active');
					$("#content-tab-"+currentId).hide();
					$(".workTabs a[id='tab-"+id+"']").addClass('active');
					$("#content-tab-"+id).show();
				}
			});
			$(".workTabs a.active").click();

			$('.label-tooltip, .help-tooltip').tooltip();
			$('.dropdown-toggle').dropdown();
			{/literal}{if !empty($bDisplayAdvice)}{literal}
			$("a#bt_disp-advice").fancybox({
				'hideOnContentClick' : false
			});
			$('#bt_disp-advice').trigger('click');
			{/literal}{/if}{literal}
		});
	</script>
{/literal}{/if}
