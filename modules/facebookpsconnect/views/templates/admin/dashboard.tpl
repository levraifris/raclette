{*
*
* Social Login
*
* @author BusinessTech.fr
* @copyright Business Tech
*
*           ____    _______
*          |  _ \  |__   __|
*          | |_) |    | |
*          |  _ <     | |
*          | |_) |    | |
*          |____/     |_|
*
*}

<form class="form-horizontal" action="{$sURI|escape:'htmlall':'UTF-8'}" method="post" id="fbpscDashboardForm" name="fbpscDashboardForm" {if $smarty.const._FPC_USE_JS == true}onsubmit="javascript: fbpsc.form('fbpscDashboardForm', '{$sURI|escape:'htmlall':'UTF-8'}', null, 'fbpscDashboardForm', 'fbpscDashboardForm', false, false, oBasicCallBack, 'Vouchers', 'loadingDashboardDiv');return false;"{/if}>

<input type="hidden" name="sAction" value="{$aQueryParams.dashboard.action|escape:'htmlall':'UTF-8'}" />
<input type="hidden" name="sType" value="{$aQueryParams.dashboard.type|escape:'htmlall':'UTF-8'}" />

<h3><i class="fa fa-dashboard"></i>&nbsp;&nbsp;{l s='Dashboard'  mod='facebookpsconnect'}</h3>

{if !empty($bUpdate)}
	{include file="`$sConfirmInclude`"}
{elseif !empty($aErrors)}
	{include file="`$sErrorInclude`"}
{/if}

	<div class="clr_20"></div>

	<div class="alert alert-info">
		{l s='Use the dashboard below to have information about :' mod='facebookpsconnect'}
		<br/>
		<br/>
		<ul>
		<li>{l s='The total amount spent per used connector' mod='facebookpsconnect'}</li>
		<li>{l s='The information about the customers who used this or that connector' mod='facebookpsconnect'}</li>
		<li>{l s='The number of connections per connector (if there was at least one connection on two different connectors)' mod='facebookpsconnect'}</li>
		</ul>
	</div>

	<div class="clr_20"></div>

	<h3><i class="icon icon-filter"></i> &nbsp;{l s='Filters' mod='facebookpsconnect'}</h3>

	<div class="clr_10"></div>
	<div class="clr_hr"></div>
	<div class="clr_10"></div>

	<div class="form-group">

		{*Filter for connectors*}
		<label class="control-label col-lg-1"><span class="label-tooltip" data-toggle="tooltip" title data-original-title="{l s='If you select "All connectors" the dashboard will display some charts' mod='facebookpsconnect'}"><b>{l s='Connector' mod='facebookpsconnect'}</b></span> :</label>
		<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
			<select name="social-network"  class="center" id="">
				<option value="">{l s='All connectors'  mod='facebookpsconnect'}</option>
				{foreach from=$aConnectorsDash item=sConnector key=key}
					<option value="{$sConnector}" {if $sCurrentSocialFilter == $sConnector} selected {/if}>{$sConnector}</option>
				{/foreach}
			</select>
		</div>

		{*Filter for date from*}
		<label class="control-label col-lg-1"><span class="label-tooltip" data-toggle="tooltip" title data-original-title="{l s='You can filter by date of account creation with a social login button' mod='facebookpsconnect'}"><b>{l s='Date' mod='facebookpsconnect'}</b></span> :</label>
		<div class="col-xs-12 col-sm-12 col-md-2 col-lg-3">
			<div class="input-group col-xs-6 pull-left">
				<input type="text" class="date-picker-from" id="datePickerFrom" name="date-add_from" placeholder="{l s='From' mod='facebookpsconnect'}" {if !empty($sFilterDateFrom)}value="{$sFilterDateFrom}"{/if}>
					<span class="input-group-addon">
						<i class="icon-calendar"></i>
					</span>
			</div>

			<div class="input-group col-xs-6 pull-right">
				<input type="text" class="date-picker-to" id="datePickerTo" name="date-add_to" placeholder="{l s='To' mod='facebookpsconnect'}" {if !empty($sFilterDateTo)}value="{$sFilterDateTo}"{/if}>
					<span class="input-group-addon">
						<i class="icon-calendar"></i>
					</span>
			</div>
		</div>

		<button class="btn btn-info btn-lg pull-right" onclick="fbpsc.form('fbpscDashboardForm', '{$sURI|escape:'htmlall':'UTF-8'}', null, 'DashboardSettingsBlock', 'DashboardSettingsBlock', false, false, null, 'Vouchers', 'loadingVoucherDiv');return false;"><i class="icon icon-search"></i>&nbsp;{l s='Search' mod='facebookpsconnect'}</button>
	</div>

	<div class="clr_hr"></div>
	<div class="clr_10"></div>

	<table class="table table-striped col-xs-4 bt-table-info">
		<thead class="bt_dashboard_header">
			<th class="center"><b>{l s='Social network' mod='facebookpsconnect'}</b></th>
			<th class="center"></th>
			<th class="center"><b>{l s='Total spent' mod='facebookpsconnect'}</b></th>
			<th class="center center"><b>{l s='Customers information' mod='facebookpsconnect'}</b></th>
		</thead>
		<tbody>
			<tr>
				<td class="bt-table-info-filter center col-xs-2">
					<div class="filter">
					</div>
				</td>
				<td class="col-xs-3 center">
					<div class="row col-xs-12 center">
					</div>
				</td>
				<td></td>
				<td>
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-11 col-lg-11">
							<div class="FormError" id="fbpscDashboardError"></div>
						</div>
						<div class="col-xs-12 col-sm-12 col-md-1 col-lg-1">
						</div>
					</div>
				</td>
			</tr>
			{if !empty($aDashboard)}
				{section loop=$aDashboard name=sDashboard}
				<tr>
					<td class="center">
						<div class="clr_10"></div>

						{if $sButtonStyle == 'modern'}
							<a class="btn-connect btn-block-connect btn-social {if $aDashboard[sDashboard]['CNT_CUST_TYPE'] == 'facebook'}btn-facebook{elseif $aDashboard[sDashboard]['CNT_CUST_TYPE'] == 'amazon'}btn-amazon{elseif $aDashboard[sDashboard]['CNT_CUST_TYPE'] == 'google'}btn-google{elseif $aDashboard[sDashboard]['CNT_CUST_TYPE'] == 'paypal'}btn-paypal{elseif $aDashboard[sDashboard]['CNT_CUST_TYPE'] == 'twitter'}btn-twitter{/if}" >
								<span class="fa fa-{$aDashboard[sDashboard]['CNT_CUST_TYPE']}"></span>
								<span class='btn-title-connect'>{$aDashboard[sDashboard]['CNT_CUST_TYPE']|ucfirst|escape:'htmlall':'UTF-8'}</span>
							</a>
						{else}
							<p class="ao_bt_fpsc {if $aDashboard[sDashboard]['CNT_CUST_TYPE'] == 'facebook'}ao_bt_fpsc_facebook{elseif $aDashboard[sDashboard]['CNT_CUST_TYPE'] == 'amazon'}ao_bt_fpsc_amazon{elseif $aDashboard[sDashboard]['CNT_CUST_TYPE'] == 'google'}ao_bt_fpsc_google{elseif $aDashboard[sDashboard]['CNT_CUST_TYPE'] == 'paypal'}ao_bt_fpsc_paypal{elseif $aDashboard[sDashboard]['CNT_CUST_TYPE'] == 'twitter'}ao_bt_fpsc_twitter{/if}" >
								<span class='picto'></span>
								<span class='title'>{$aDashboard[sDashboard]['CNT_CUST_TYPE']|ucfirst|escape:'htmlall':'UTF-8'}</span>
							</p>
						{/if}
					</td>
					<td class="center">
					</td>
					<td class="center">
						{foreach from=$aSocialAmount item=SocialAmount}
							{if $SocialAmount.CNT_CUST_TYPE == $aDashboard[sDashboard]['CNT_CUST_TYPE']}
								<span class="badge badge-info">{$SocialAmount.sValue}</span>
							{/if}
						{/foreach}
					</td>
					<td class="center" style="cursor: pointer !important;" onclick="$('#bt_customer-detail_{$smarty.section.sDashboard.iteration|intval}').toggle(800);">
						<div class="row">
							<a class="btn btn-success btn-md center">&nbsp;<i class="icon icon-zoom-in"></i>&nbsp; {l s='View customers' mod='facebookpsconnect'}</a>
						</div>
					</td>
				</tr>
				<tr id="bt_customer-detail_{$smarty.section.sDashboard.iteration|intval}" class="bt_customer_details" style="display: none;">
					<td colspan="4">
						<table class="table table-bordered">
							<div class="clr_10"></div>
							<thead class="{if $aDashboard[sDashboard]['CNT_CUST_TYPE'] == 'facebook'}facebook_table{elseif $aDashboard[sDashboard]['CNT_CUST_TYPE'] == 'amazon'}amazon_table{elseif $aDashboard[sDashboard]['CNT_CUST_TYPE'] == 'google'}google_table{elseif $aDashboard[sDashboard]['CNT_CUST_TYPE'] == 'paypal'}paypal_table{elseif $aDashboard[sDashboard]['CNT_CUST_TYPE'] == 'twitter'}twitter_table{/if}">
								<th class="center">{l s='First name' mod='facebookpsconnect'}</th>
								<th class="center">{l s='Last name' mod='facebookpsconnect'}</th>
								<th class="center">{l s='E-mail' mod='facebookpsconnect'}</th>
								<th class="center">{l s='Account creation date' mod='facebookpsconnect'}</th>
							</thead>
							<tbody>
								{foreach from=$aCustomerInfo item=CustomerInfo}
									{if $CustomerInfo.social == $aDashboard[sDashboard]['CNT_CUST_TYPE']}
								<tr>
									<td class="center">{$CustomerInfo.firstname|escape:'htmlall':'UTF-8'}</td>
									<td class="center">{$CustomerInfo.lastname|escape:'htmlall':'UTF-8'}</td>
									<td class="center">{$CustomerInfo.email|escape:'htmlall':'UTF-8'}</td>
									<td class="center">{$CustomerInfo.date_add|escape:'htmlall':'UTF-8'}</td>
								</tr>
									{/if}
								{/foreach}
							</tbody>
						</table>
					</td>
				</tr>
			{/section}
		{else}
			<tr>
				<td class="list-empty" colspan="7">
					<div class="list-empty-msg bt-search-no-result">
						<i class="icon-warning-sign list-empty-icon">
							{l s='No record found' mod='facebookpsconnect'}
						</i>
					</div>
				</td>
			</tr>
		{/if}
		</tbody>
	</table>

	<div class="clr_10"></div>
	<div class="clr_hr"></div>
	<div class="clr_10"></div>

	{if !empty($aDashboard) && $aDashboard|count > 1}
		<div class="row">
			<div class="col-xs-6">
				<canvas id="myChartConnection" style="height:100vh; width:100vw"></canvas>
			</div>
			<div class="col-xs-6">
				<canvas id="myChartAmount" style="height:100vh; width:100vw"></canvas>
			</div>
		</div>
	{/if}
{literal}
	<script type="text/javascript">
		// handle cart rule type
		$(document).ready(function()
		{
			$(".date-picker-from").datepicker({
				dateFormat: 'yy-mm-dd'
			});

			$('#date-picker-from').datepicker('setDate', '2017-01-10');

			$(".date-picker-to").datepicker({
				dateFormat: 'yy-mm-dd'
			});

			$(".icon-question-sign").tooltip();
			$(".label-tooltip").tooltip();

		});

		//Charts generator
		{/literal}{if !empty($aCharts)}{literal}
			//Get all available charts
			{/literal}{foreach from=$aCharts item=aChart}{literal}

				var aData = [];
				var aLabel = [];
				var aColor = [];

				// Get the values for the charts
				{/literal}{foreach from=$aChart.data item=aData}{literal}
					aData.push({/literal}'{$aData.sValue|floatval}'{literal})
					aLabel.push({/literal}'{$aData.CNT_CUST_TYPE}'{literal})
					aColor.push({/literal}'{$aChartColor[$aData.CNT_CUST_TYPE]['charts']['color']}'{literal})
				{/literal}{/foreach}{literal}


				//Build the charts with the Js function
				fbpsc.getCharts({/literal}'{$aChart.id}'{literal},aData ,aLabel ,aColor, {/literal}'{$aChart.title}'{literal});

			{/literal}{/foreach}{literal}
		{/literal}{/if}{literal}

	</script>
{/literal}

</form>



