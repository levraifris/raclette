{*
*
* Social Login
*
* @author BusinessTech.fr
* @copyright Business Tech
*
*           ____    _______
*          |  _ \  |__   __|
*          | |_) |    | |
*          |  _ <     | |
*          | |_) |    | |
*          |____/     |_|
*
*}

{literal}
<script type="text/javascript">
	$(document).ready(function()
	{
		$("a#hookEdit").fancybox({
			'hideOnContentClick' : false,
			'autoDimensions' : true
		});
	});
</script>
{/literal}
<h3><i class="fa fa-anchor"></i>&nbsp;&nbsp;{l s='Connectors position management (native hooks)'  mod='facebookpsconnect'}</h3>
<div class="form-group">
	<div class="col-xs-12"}>
		<table style="width: 100%;" id="fbpsctabs" class="table table-condensed table-hover" cellpadding="0" cellspacing="0">
			<thead>
			<tr class="nodrag nodrop">
				<th class="center"><b>{l s='Hook name' mod='facebookpsconnect'}</b></th>
				<th class="center"><b>{l s='Connectors added to the hook' mod='facebookpsconnect'}</b></th>
				<th class="center"><b>{l s='Edit' mod='facebookpsconnect'}</b></th>
			</tr>
			</thead>
			<tbody>
			{foreach from=$aHooksPosition name=hook key=hName item=hValue}
				{if $hValue.name|escape:'htmlall':'UTF-8' != 'Checkout funnel for PS 1.7'}
					{if $hValue.status}
						<tr id="tr_{$smarty.foreach.connector.iteration|intval}">
							<td class="center">
								<div class="badge badge-info"><i class="icon icon-anchor"></i>&nbsp;{$hValue.title|escape:'htmlall':'UTF-8'}</div>
							</td>
							<td class="center">
                              {if $hValue.data.connectors|unserialize != ''  && !empty($hValue.data.connectors|unserialize)}
									<div class="clr_10"></div>
									{foreach from=$hValue.data.connectors|unserialize name=connector key=name item=title}
										{if $sButtonStyle == 'modern'}
											<a class="btn-connect btn-block-connect btn-social btn-{$name|escape:'htmlall':'UTF-8'}">
                                                {if $name == 'google'}
                                                    <span class="btn-google-icon"></span>
                                                {else}
												    <span class="fa fa-{$name}{if $name == 'facebook'}-square{/if}"></span>
                                                {/if}
												<span class='btn-title-connect'>{$name|ucfirst|escape:'htmlall':'UTF-8'}</span>
											</a>
										{else}
											<p class="ao_bt_fpsc {if $name == 'facebook'}ao_bt_fpsc_facebook{elseif $name == 'amazon'}ao_bt_fpsc_amazon{elseif $name == 'google'}ao_bt_fpsc_google{elseif $name == 'paypal'}ao_bt_fpsc_paypal{elseif $name == 'twitter'}ao_bt_fpsc_twitter{/if}" >
												<span class='picto'></span>
												<span class='title'>{$name|ucfirst|escape:'htmlall':'UTF-8'}</span>
											</p>
										{/if}
									{/foreach}
								{else}
									<div class="clr_10"></div>
									<p class="alert alert-warning">
										{l s='No connector added' mod='facebookpsconnect'}
									</p>
								{/if}
							</td>
							<td class="center">
								<a id="hookEdit" href="{$sURI}&sAction={$aQueryParams.hookForm.action|escape:'htmlall':'UTF-8'}&sType={$aQueryParams.hookForm.type|escape:'htmlall':'UTF-8'}&sHookId={$hName|escape:'htmlall':'UTF-8'}" class="btn btn-mini btn-success fancybox.ajax"><i class="icon icon-edit"></i> </a>
							</td>
						</tr>
					{/if}
				{/if}
			{/foreach}
			</tbody>
		</table>
	</div>
</div>
<div class="clr_20"></div>
