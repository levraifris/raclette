{*
*
* Social Login
*
* @author BusinessTech.fr
* @copyright Business Tech
*
*           ____    _______
*          |  _ \  |__   __|
*          | |_) |    | |
*          |  _ <     | |
*          | |_) |    | |
*          |____/     |_|
*
*}

<form class="form-horizontal" action="{$sURI|escape:'htmlall':'UTF-8'}" method="post" id="fbpscHookAdvancedForm" name="fbpscHookAdvancedForm" {if $smarty.const._FPC_USE_JS == true}onsubmit="javascript: fbpsc.form('fbpscHookAdvancedForm', '{$sURI|escape:'htmlall':'UTF-8'}', null, 'fbpscHookAdvanced', 'fbpscHookAdvanced', false, false, oBasicCallBack, 'Hook', 'loadingHookAdvancedDiv');return false;"{/if}>
	<input type="hidden" name="sAction" value="{$aQueryParams.hookAdvanced.action|escape:'htmlall':'UTF-8'}" />
	<input type="hidden" name="sType" value="{$aQueryParams.hookAdvanced.type|escape:'htmlall':'UTF-8'}" />


	<h3><i class="fa fa-camera"></i>&nbsp;&nbsp;{l s='Position advanced settings: native hooks'  mod='facebookpsconnect'}</h3>

	{if !empty($bUpdate)}
		{include file="`$sConfirmInclude`"}
	{elseif !empty($aErrors)}
		{include file="`$sErrorInclude`"}
	{/if}

	<div class="form-group">
		<label class="control-label col-lg-3"><span class="label-tooltip" data-toggle="tooltip" title data-original-title="{l s='This option lets you select the position of the buttons on the login page' mod='facebookpsconnect'}"><b>{l s='Connectors position on login page' mod='facebookpsconnect'}</b></span> :</label>
		<div class="col-xs-12 col-sm-12 col-md-2 col-lg-4">
			<select id="LoginPagePosition" name="LoginPagePosition">
				<option value="above" {if $sBtnPosition == 'above'} selected="selected" {/if}>{l s='Above the login form'  mod='facebookpsconnect'}</option>
				<option value="below" {if $sBtnPosition == 'below'} selected="selected" {/if}>{l s='Below the login form'  mod='facebookpsconnect'}</option>
			</select>
			<div class="help-block">
				<i class="fa fa-warning"></i> {l s='Only available if you added connectors on the login page' mod='facebookpsconnect'}
			</div>
		</div>
		<span class="label-tooltip" data-toggle="tooltip" title data-original-title="{l s='This option lets you select the position of the buttons on the login page' mod='facebookpsconnect'}">&nbsp;<span class="icon-question-sign"></span></span>
	</div>


	{if !empty($bVersion17)}
		<div class="form-group">
			<label class="control-label col-lg-3"><span class="label-tooltip" data-toggle="tooltip" title data-original-title="{l s='This option lets you select the position of the buttons on the checkout funnel' mod='facebookpsconnect'}"><b>{l s='Connectors position on order funnel' mod='facebookpsconnect'}</b></span> :</label>
			<div class="col-xs-12 col-sm-12 col-md-2 col-lg-4">
				<select id="LoginPagePosition17" name="LoginPagePosition17">
					<option value="above" {if $sBtnPosition17 == 'above'} selected="selected" {/if}>{l s='Above the login form'  mod='facebookpsconnect'}</option>
					<option value="below" {if $sBtnPosition17 == 'below'} selected="selected" {/if}>{l s='Below the login form'  mod='facebookpsconnect'}</option>
				</select>
				<div class="help-block">
					<i class="fa fa-warning"></i> {l s='Only available if you added connectors on the order funnel page' mod='facebookpsconnect'}
				</div>
			</div>
			<span class="label-tooltip" data-toggle="tooltip" title data-original-title="{l s='This option lets you select the position of the buttons on the checkout funnel' mod='facebookpsconnect'}">&nbsp;<span class="icon-question-sign"></span></span>
		</div>
	{/if}

	{if !empty($bOnePageCheckOut)}
		<div class="form-group">
			<label class="control-label col-lg-3"><span class="label-tooltip" data-toggle="tooltip" title data-original-title="{l s='This option lets you select the position on the One Page Checkout' mod='facebookpsconnect'}"><b>{l s='Position on the one page checkout' mod='facebookpsconnect'}</b></span> :</label>
			<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
				<select id="OpcPosition" name="OpcPosition">
					<option value="above" {if $sOpcPosition == 'above'} selected="selected" {/if}>{l s='Above the form'  mod='facebookpsconnect'}</option>
					<option value="below" {if $sOpcPosition == 'below'} selected="selected" {/if}>{l s='Below the form'  mod='facebookpsconnect'}</option>
				</select>
			</div>
		</div>
	{/if}

	<div class="center">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-11 col-lg-11">
				<div class="FormError" id="fbpscBasicFormError"></div>
			</div>
			<div class="col-xs-12 col-sm-12 col-md-1 col-lg-1">
				<button class="btn btn-default pull-right" onclick="fbpsc.form('fbpscHookAdvancedForm', '{$sURI|escape:'htmlall':'UTF-8'}', null, 'fbpscHookAdvanced', 'fbpscHookAdvanced', false, false, null, 'Hook', 'loadingHookAdvancedDiv');return false;"><i class="process-icon-save"></i>{l s='Save' mod='facebookpsconnect'}</button>
			</div>
		</div>
	</div>

</form>

<div class="clr_20"></div>
<h3><i class="icon icon-bookmark"></i>&nbsp; {l s='Position advanced settings: use of HTML elements'  mod='facebookpsconnect'}</h3>

<p class="alert alert-info">
	{l s='If no native prestashop hooks are suitable for you to position your connectors, the module allows you to increase the positioning possibilities by using your pages HTML elements. You can indeed decide on which HTML element of a given page you want to add the connectors. Click on « Add new position » to configure a new position for your connectors.' mod='facebookpsconnect'}
	<br/>
	<br/>
	{l s='NOTE: This feature needs some technical knowledge so if you are not comfortable with this, we advise you to ask your technical contact to help you.' mod='facebookpsconnect'}
	<br/>
	<br/>
	<a class="badge badge-info" href="{$smarty.const._FPC_BT_FAQ_MAIN_URL|escape:'htmlall':'UTF-8'}/{$sFaqLang|escape:'htmlall':'UTF-8'}/faq/169" target="_blank"><i class="icon icon-link"></i>&nbsp;{l s='FAQ about the HTML elements use' mod='facebookpsconnect'}</a>
</p>

<div class="clr_10"></div>
<a id="handlePosition" class="pull-right fancybox.ajax btn btn-md btn-success" href="{$sURI|escape:'htmlall':'UTF-8'}&{$sCtrlParamName|escape:'htmlall':'UTF-8'}={$sController|escape:'htmlall':'UTF-8':'UTF-8'}&sAction={$aQueryParams.connectPositionForm.action|escape:'htmlall':'UTF-8'}&sType={$aQueryParams.connectPositionForm.type|escape:'htmlall':'UTF-8'}"><span class="icon-plus-circle"></span>&nbsp;{l s='Add new position' mod='facebookpsconnect'}</a>
<div class="clr_10"></div>



{if !empty($aCustomPosition)}

	<div class="btn-actions">
		<div class="btn btn-default btn-mini" id="customCheck" onclick="return fbpsc.selectAll('input.fbpscCustomPosition', 'check');"><span class="icon-plus-square"></span>&nbsp;{l s='Check All' mod='facebookpsconnect'}</div>

		&nbsp;-&nbsp;

		<div class="btn btn-default btn-mini" id="categoryUnCheck" onclick="return fbpsc.selectAll('input.fbpscCustomPosition', 'uncheck');"><span class="icon-minus-square"></span>&nbsp;{l s='Uncheck All' mod='facebookpsconnect'}</div>

		&nbsp;-&nbsp;

		<div class="btn btn-success btn-mini" name="bt_BulkActvation" id="customCheck"
		           onclick="check = confirm('{l s='Are you sure you want to activate all position selected' mod='facebookpsconnect'} ?');if(!check)return false;
				           iPosId = fbpsc.getBulkCheckBox('bt_bulkCustomPosition-box');
				           $('#loadingHookAdvancedDiv').show();
				           fbpsc.hide('fbpscHookAdvanced');
				           fbpsc.ajax('{$sURI|escape:'htmlall':'UTF-8'}', '{$sCtrlParamName|escape:'htmlall':'UTF-8'}={$sController|escape:'htmlall':'UTF-8'}&sAction={$aQueryParams.connectPosition.action|escape:'htmlall':'UTF-8'}&sType={$aQueryParams.connectPosition.type|escape:'htmlall':'UTF-8'}&iPositionId='+iPosId+'&sActionType=statusUpdateActionBulk&iStatus=1', 'fbpscHookAdvanced', 'fbpscHookAdvanced', null, null, 'loadingHookAdvancedDiv');">
			<span class="fa fa-check"></span>&nbsp;{l s='Activate selection' mod='facebookpsconnect'}
		</div>

		&nbsp;-&nbsp;

		<div class="btn btn-warning btn-mini" name="bt_BulkActvation" id="customCheck"
		     onclick="check = confirm('{l s='Are you sure you want to deativate all position selected' mod='facebookpsconnect'} ?');if(!check)return false;
				     iPosId = fbpsc.getBulkCheckBox('bt_bulkCustomPosition-box');
				     $('#loadingHookAdvancedDiv').show();
				     fbpsc.hide('fbpscHookAdvanced');
				     fbpsc.ajax('{$sURI|escape:'htmlall':'UTF-8'}', '{$sCtrlParamName|escape:'htmlall':'UTF-8'}={$sController|escape:'htmlall':'UTF-8'}&sAction={$aQueryParams.connectPosition.action|escape:'htmlall':'UTF-8'}&sType={$aQueryParams.connectPosition.type|escape:'htmlall':'UTF-8'}&iPositionId='+iPosId+'&sActionType=statusUpdateActionBulk&iStatus=0', 'fbpscHookAdvanced', 'fbpscHookAdvanced', null, null, 'loadingHookAdvancedDiv');">
			<span class="fa fa-remove"></span>&nbsp;{l s='Deactivate selection' mod='facebookpsconnect'}
		</div>

		&nbsp;-&nbsp;

		<div class="btn btn-warning btn-danger" name="bt_BulkActvation" id="customCheck"
		     onclick="check = confirm('{l s='Are you sure you want to delete all position selected' mod='facebookpsconnect'} ?');if(!check)return false;
				     iPosId = fbpsc.getBulkCheckBox('bt_bulkCustomPosition-box');
				     $('#loadingHookAdvancedDiv').show();
				     fbpsc.hide('fbpscHookAdvanced');
				     fbpsc.ajax('{$sURI|escape:'htmlall':'UTF-8'}', '{$sCtrlParamName|escape:'htmlall':'UTF-8'}={$sController|escape:'htmlall':'UTF-8'}&sAction={$aQueryParams.connectPosition.action|escape:'htmlall':'UTF-8'}&sType={$aQueryParams.connectPosition.type|escape:'htmlall':'UTF-8'}&iPositionId='+iPosId+'&sActionType=deleteBulk', 'fbpscHookAdvanced', 'fbpscHookAdvanced', null, null, 'loadingHookAdvancedDiv');">
			<span class="fa fa-trash"></span>&nbsp;{l s='Delete selection' mod='facebookpsconnect'}
		</div>


		<div class="clr_20"></div>
	</div>

	<table class="table table-bordered" id="bt_position_table">
		<thead>
				<th class="text-center"><b></b></th>
				<th class="text-center"><b>{l s='Page'  mod='facebookpsconnect'}</b></th>
				<th class="text-center"><b>{l s='Position name'  mod='facebookpsconnect'}</b></th>
				<th class="text-center"><b>{l s='HTML element'  mod='facebookpsconnect'}</b></th>
				<th class="text-center"><b>{l s='Buttons position'  mod='facebookpsconnect'}</b></th>
				{* <th class="text-center"><b>{l s='Buttons size'  mod='facebookpsconnect'}</b></th> *}
				<th class="text-center"><b>{l s='Connectors'  mod='facebookpsconnect'}</b></th>
				<th class="text-center">
					<b>{l s='Status'  mod='facebookpsconnect'}</b>
					<span class="label-tooltip" data-toggle="tooltip" title data-original-title="{l s='Click on the icon below to change the status' mod='facebookpsconnect'}">&nbsp;<span class="icon-question-sign"></span></span>
				</th>
				<th class="text-center"><b>{l s='Actions'  mod='facebookpsconnect'}</b></th>
				<th style="text-align: center;"><a id="handlePosition" class="fancybox.ajax btn btn-mini btn-success" href="{$sURI|escape:'htmlall':'UTF-8'}&{$sCtrlParamName|escape:'htmlall':'UTF-8'}={$sController|escape:'htmlall':'UTF-8':'UTF-8'}&sAction={$aQueryParams.connectPositionForm.action|escape:'htmlall':'UTF-8'}&sType={$aQueryParams.connectPositionForm.type|escape:'htmlall':'UTF-8'}"><span class="icon-plus-circle"></span></a></th>
		</thead>
		<tbody class="text-center">
			{foreach from=$aCustomPositionAdvanced item=customPosition}
				<tr>
					<td><input name="bt_bulkCustomPosition-box" type="checkbox" class="fbpscCustomPosition" value="{$customPosition.id}"/></td>
					<td>{$customPosition.data.page}</td>
					<td>{$customPosition.name}</td>
					<td>{$customPosition.data.html}</td>
					<td>{$customPosition.data.position}</td>
					{* <td>{$customPosition.data.size}</td> *}
					<td class="col-xs-2">
						<div class="clr_10"></div>
						{if !empty($customPosition.data.connectors)}
							{foreach from=$customPosition.data.connectors|unserialize item=connector}
								{if $sButtonStyle != 'modern'}
									<p class="ao_bt_fpsc ao_bt_fpsc_{$connector|escape:'htmlall':'UTF-8'}">
										<span class='picto'></span>
										<span class='title'>{$connector|ucfirst|escape:'htmlall':'UTF-8'}</span>
									</p>
								{else}
									<a class="btn-connect btn-block-connect btn-social btn-{$connector|escape:'htmlall':'UTF-8'}">
                                        {if $connector == 'google'}
                                            <span class="btn-google-icon"></span>
                                        {else}
										    <span class="fa fa-{$connector}{if $connector == 'facebook'}-square{/if}"></span>
                                        {/if}
										<span class='btn-title-connect'>{$connector|ucfirst|escape:'htmlall':'UTF-8'}</span>
									</a>
								{/if}
							{/foreach}
						{else}
							<p class="alert alert-warning">
								{l s='No connector added' mod='facebookpsconnect'}
							</p>
						{/if}
					</td>
					<td class="{if $customPosition.status == 1} success {else} danger{/if}">
						{if $customPosition.status == 1}
							{*Use case deactivate *}
							<a href="#"><i class="icon icon-2x icon-check-circle color_success" title="{l s='deactivate' mod='facebookpsconnect'}" onclick="check = confirm('{l s='Are you sure to want to deactivate this position' mod='facebookpsconnect'} ? {l s='It will be deactivate' mod='facebookpsconnect'}');if(!check)return false;$('#loadingHookAdvancedDiv').show();fbpsc.hide('fbpscHookAdvanced');fbpsc.ajax('{$sURI|escape:'htmlall':'UTF-8'}', '{$sCtrlParamName|escape:'htmlall':'UTF-8'}={$sController|escape:'htmlall':'UTF-8'}&sAction={$aQueryParams.connectPosition.action|escape:'htmlall':'UTF-8'}&sType={$aQueryParams.connectPosition.type|escape:'htmlall':'UTF-8'}&iPositionId={$customPosition.id|intval}&sActionType=statusUpdate&iStatus=0', 'fbpscHookAdvanced', 'fbpscHookAdvanced', null, null, 'loadingHookAdvancedDiv');" ></i></a>
						{else}
							{*Use case activate*}
							<a href="#"><i class="icon icon-2x icon-remove-circle color_danger" title="{l s='deactivate' mod='facebookpsconnect'}" onclick="check = confirm('{l s='Are you sure to want to activate this position' mod='facebookpsconnect'} ? {l s='It will be activate' mod='facebookpsconnect'}');if(!check)return false;$('#loadingHookAdvancedDiv').show();fbpsc.hide('fbpscHookAdvanced');fbpsc.ajax('{$sURI|escape:'htmlall':'UTF-8'}', '{$sCtrlParamName|escape:'htmlall':'UTF-8'}={$sController|escape:'htmlall':'UTF-8'}&sAction={$aQueryParams.connectPosition.action|escape:'htmlall':'UTF-8'}&sType={$aQueryParams.connectPosition.type|escape:'htmlall':'UTF-8'}&iPositionId={$customPosition.id|intval}&sActionType=statusUpdate&iStatus=1', 'fbpscHookAdvanced', 'fbpscHookAdvanced', null, null, 'loadingHookAdvancedDiv');" ></i></a>
						{/if}
					</td>
					<td>
					<a href="#"><i class="icon-trash btn btn-mini btn-danger" title="{l s='delete' mod='facebookpsconnect'}" onclick="check = confirm('{l s='Are you sure to want to delete this position' mod='facebookpsconnect'} ? {l s='It will be definitely removed from your database' mod='facebookpsconnect'}');if(!check)return false;$('#loadingHookAdvancedDiv').show();fbpsc.hide('fbpscHookAdvanced');fbpsc.ajax('{$sURI|escape:'htmlall':'UTF-8'}', '{$sCtrlParamName|escape:'htmlall':'UTF-8'}={$sController|escape:'htmlall':'UTF-8'}&sAction={$aQueryParams.connectPosition.action|escape:'htmlall':'UTF-8'}&sType={$aQueryParams.connectPosition.type|escape:'htmlall':'UTF-8'}&iPositionId={$customPosition.id|intval}&sActionType=delete', 'fbpscHookAdvanced', 'fbpscHookAdvanced', null, null, 'loadingHookAdvancedDiv');" ></i></a>
						<a id="handlePosition" class="fancybox.ajax btn btn-mini btn-info" href="{$sURI|escape:'htmlall':'UTF-8'}&{$sCtrlParamName|escape:'htmlall':'UTF-8'}={$sController|escape:'htmlall':'UTF-8':'UTF-8'}&sAction={$aQueryParams.connectPositionForm.action|escape:'htmlall':'UTF-8'}&sType={$aQueryParams.connectPositionForm.type|escape:'htmlall':'UTF-8'}&iPositionId={$customPosition.id|intval}"><span class="icon-edit"></span></a></th>
					</td>
					<td></td>
				</tr>
			{/foreach}
		</tbody>

	</table>
{/if}
{literal}
<script type="text/javascript">

	$("a#handlePosition").fancybox({
		'hideOnContentClick' : false,
		'scrolling' : 'auto',
		'heigth' : 350
	})

	$(function() {
		$(".label-tooltip").tooltip();
	});
</script>
{/literal}
