{*
*
* Social Login
*
* @author BusinessTech.fr
* @copyright Business Tech
*
*           ____    _______
*          |  _ \  |__   __|
*          | |_) |    | |
*          |  _ <     | |
*          | |_) |    | |
*          |____/     |_|
*
*}

{literal}
<script type="text/javascript">
	$(function() {
		$(".label-tooltip").tooltip();
	});
</script>
{/literal}
<div class="form-horizontal bootstrap" style="width: 900px;">

	<div class="clr_10"></div>
	<h4><i class="fa fa-desktop"></i> {l s='Display' mod='facebookpsconnect'}</h4>
	<div class="clr_hr"></div>
	<div class="clr_10"></div>

	<div class="form-group" id="bootstrap-bouton">
		<label class="control-label col-lg-3"><span class="label-tooltip" data-toggle="tooltip" title data-original-title="{l s='Select "Yes" if you want to enable the connector display' mod='facebookpsconnect'}"><b>{l s='Activate' mod='facebookpsconnect'}</b></span> :</label>
		<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
				<span class="switch prestashop-switch fixed-width-md">
					<input type="radio" name="activeConnector" id="activeConnector_on" value="true" {if $aConnector.data.activeConnector == true}checked="checked"{/if} />
					<label for="activeConnector_on" class="radioCheck">
						{l s='Yes' mod='facebookpsconnect'}
					</label>
					<input type="radio" name="activeConnector" id="activeConnector_off" value="false" {if $aConnector.data.activeConnector == false}checked="checked"{/if} />
					<label for="activeConnector_off" class="radioCheck">
						{l s='No' mod='facebookpsconnect'}
					</label>
					<a class="slide-button btn"></a>
				</span>
		</div>
		<span class="label-tooltip" data-toggle="tooltip" title data-original-title="{l s='Select "Yes" if you want to enable the connector display' mod='facebookpsconnect'}">&nbsp;<span class="icon-question-sign"></span></span>
	</div>
	
	<div class="clr_10"></div>
	<h4><i class="fa fa-twitter"></i> {l s='Twitter application information' mod='facebookpsconnect'}</h4>
	<div class="clr_hr"></div>
	<div class="clr_10"></div>

	<p class="alert alert-info">
		{l s='To know how to fill out the form below follow the FAQ link below:' mod='facebookpsconnect'}
		<br/>
		<br/>
		<a class="text-center badge badge-info" target="_blank" href="{$smarty.const._FPC_BT_FAQ_MAIN_URL|escape:'htmlall':'UTF-8'}/{$sFaqLang|escape:'htmlall':'UTF-8'}/faq/88"><span class="fa fa-info-circle"></span>&nbsp;{l s='How to create my Twitter app ?' mod='facebookpsconnect'}</a>
	</p>


	<div class="form-group">
		<label class="control-label col-xs-3"><b>{l s='Consumer Key' mod='facebookpsconnect'} :</b></label>
		<div class="col-xs-12 col-sm-12 col-md-5 col-lg-5">
			<div class="col-xs-12 col-sm-12 col-md-5 col-lg-12">
				<input type="text" name="id" id="id" size="60" value="{if isset($aConnector.data.id)}{$aConnector.data.id}{/if}"  />
			</div>
		</div>
	</div>

	{*Secret id*}
	<div class="form-group">
		<label class="control-label col-xs-3"><b>{l s='Consumer Secret' mod='facebookpsconnect'} :</b></label>
		<div class="col-xs-12 col-sm-12 col-md-5 col-lg-5">
			<div class="col-xs-12 col-sm-12 col-md-5 col-lg-12">
				<input type="text" name="secret" id="secret" size="60" value="{if isset($aConnector.data.secret)}{$aConnector.data.secret|escape:'htmlall':'UTF-8'}{/if}"  />
			</div>
		</div>
	</div>


	{*Call Back*}
	<div class="form-group">
		<label class="control-label col-xs-12 col-sm-12 col-md-5 col-lg-3"><span class="label-tooltip" data-toggle="tooltip" title data-original-title="{l s='This is the URL you have to set in your Twitter application' mod='facebookpsconnect'}"><b>{l s='Callback URL' mod='facebookpsconnect'}</b> :</label>
		<div class="input-group col-xs-6 pull-left">
			<input type="text" name="callback" id="callback" size="60" value="{if isset($aConnector.data.callback)}{$aConnector.data.callback}{else}{$sCbkUri}{/if}" />
		</div>
		<button type="button" class="btn btn-default btn-copy js-tooltip js-copy" data-toggle="tooltip" data-placement="bottom" data-copy="{if isset($aConnector.data.callback)}{$aConnector.data.callback}{else}{$sCbkUri}{/if}" title="{l s='Copy to clipboard' mod='facebookpsconnect'}">&nbsp;<i class="fa fa-copy"></i>&nbsp;{l s='Copy to clipboard' mod='facebookpsconnect'} </button>
		<span class="label-tooltip" data-toggle="tooltip" title data-original-title="{l s='This is the URL you have to set in your Twitter application' mod='facebookpsconnect'}">&nbsp;<span class="icon-question-sign"></span></span>
	</div>

	{*Display*}
	{*<div class="form-group">*}
		{*<label class="control-label col-xs-3"><span class="label-tooltip" data-toggle="tooltip" title data-original-title="{l s='The style to display button' mod='facebookpsconnect'}"><b>{l s='Display style' mod='facebookpsconnect'}</b></span> :</label>*}
		{*<div class="col-xs-12 col-sm-12 col-md-5 col-lg-5">*}
			{*<div class="col-xs-12 col-sm-12 col-md-5 col-lg-9">*}
				{*<select name="display">*}
					{*<option value="inline" {if isset($aWidget.data.display)}{if $aWidget.data.display == 'inline'}selected="selected"{/if}{else}selected="selected"{/if}>{l s='inline' mod='facebookpsconnect'}</option>*}
					{*<option value="block" {if isset($aWidget.data.display) && $aWidget.data.display == 'block'}selected="selected"{/if}>{l s='block' mod='facebookpsconnect'}</option>*}
				{*</select>*}
			{*</div>*}
			{*<span class="label-tooltip" data-toggle="tooltip" title data-original-title="{l s='The style to display button' mod='facebookpsconnect'}">&nbsp;<span class="icon-question-sign"></span></span>*}
		{*</div>*}
	{*</div>*}
</div>