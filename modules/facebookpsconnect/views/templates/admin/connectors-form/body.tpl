{*
*
* Social Login
*
* @author BusinessTech.fr
* @copyright Business Tech
*
*           ____    _______
*          |  _ \  |__   __|
*          | |_) |    | |
*          |  _ <     | |
*          | |_) |    | |
*          |____/     |_|
*
*}
<div id="fbpsc" class="bootstrap">
	<a class="btn-connect btn-connect btn-block btn-{$aConnector.cssClass}">
		<i class="fa fa-{$aConnector.cssClass}"></i> {$aConnector.title|upper} {l s='CONFIGURATION' mod='facebookpsconnect'}
	</a>
	{if $iTestCurlSsl != 2}
		{if $iConnectorId == 'facebook' && $iTestCurlSsl == 0 && $iApiRequestMethod == ''}
			<div class="alert alert-danger" id="{$sModuleName|escape:'htmlall':'UTF-8'}ConnectorError">
				{l s='Facebook needs to have a connection method selected. Please select a connection method in the "Basics" tab.' mod='facebookpsconnect'}
			</div>
		{elseif $iConnectorId == 'facebook' && $iTestCurlSsl == 0 && ( $iApiRequestMethod == ''  || $iApiRequestMethod == 'curl')}
			<div class="alert alert-danger" id="{$sModuleName|escape:'htmlall':'UTF-8'}ConnectorError">
				{l s='You have selected the "PHP CURL LIBRARY" connection method, but have not yet completed the cURL test. Please run the cURL test in the "Prerequisites Check" tab.' mod='facebookpsconnect'}
			</div>
		{elseif $iConnectorId != 'facebook' && $iTestCurlSsl == 0}
			<div class="alert alert-danger" id="{$sModuleName|escape:'htmlall':'UTF-8'}ConnectorError">
				{l s='You need check your cURL over SSL configuration. Please run the cURL test in the "Prerequisites Check" tab.' mod='facebookpsconnect'}
			</div>
		{elseif $iTestCurlSsl == 1}
			<div class="alert alert-danger" id="{$sModuleName|escape:'htmlall':'UTF-8'}ConnectorError">
				{l s=' The cURL over SSL test failed, you will need to contact your webhost, as the module needs cURL over SSL enabled.' mod='facebookpsconnect'}
			</div>
		{/if}
	{/if}
	<form action="{$sURI}" method="post" id="{$sModuleName|escape:'htmlall':'UTF-8'}ConnectorForm" name="{$sModuleName|escape:'htmlall':'UTF-8'}ConnectorForm" onsubmit="{$sModuleName|escape:'htmlall':'UTF-8'}.form('{$sModuleName|escape:'htmlall':'UTF-8'}ConnectorForm', '{$sURI}', '', '{$sModuleName|escape:'htmlall':'UTF-8'}ConnectorList', '{$sModuleName|escape:'htmlall':'UTF-8'}ConnectorList', false, true, null, 'Connector');return false;">
		<input type="hidden" name="sAction" value="{$aQueryParams.connector.action|escape:'htmlall':'UTF-8'}" />
		<input type="hidden" name="sType" value="{$aQueryParams.connector.type|escape:'htmlall':'UTF-8'}" />
		<input type="hidden" name="iConnectorId" value="{$iConnectorId}" />
		<div class="plugin_form">
			{include file="`$sTplToInclude`"}
		</div>
		<br/>
		<center>
			<span><input class="button btn btn-success btn-lg" type="button" id="{$sModuleName|escape:'html':'UTF-8'}ConfigureConnector" name="{$sModuleName|escape:'html':'UTF-8'}ConfigureConnector" value="{l s='Update' mod='facebookpsconnect'}" onclick="{$sModuleName|escape:'htmlall':'UTF-8'}.form('{$sModuleName|escape:'htmlall':'UTF-8'}ConnectorForm', '{$sURI}', '', '{$sModuleName|escape:'htmlall':'UTF-8'}ConnectorList', '{$sModuleName|escape:'htmlall':'UTF-8'}ConnectorList', false, true, null, 'Connector');return false;" /></span>
		</center>
		<br/>
	</form>
</div>

<script type="text/javascript">
	function copyToClipboard(text, el) {
		var copyTest = document.queryCommandSupported('copy');
		var elOriginalText = el.attr('data-original-title');

		if (copyTest === true) {
			var copyTextArea = document.createElement("textarea");
			copyTextArea.value = text;
			document.body.appendChild(copyTextArea);
			copyTextArea.select();
			try {
				var successful = document.execCommand('copy');
				var msg = successful ? 'Copied!' : 'Whoops, not copied!';
				el.attr('data-original-title', msg).tooltip('show');
			} catch (err) {
				console.log('Oops, unable to copy');
			}
			document.body.removeChild(copyTextArea);
			el.attr('data-original-title', elOriginalText);
		} else {
			// Fallback if browser doesn't support .execCommand('copy')
			window.prompt("Copy to clipboard: Ctrl+C or Command+C, Enter", text);
		}
	}

	$(document).ready(function() {
		// Initialize
		// ---------------------------------------------------------------------

		// Tooltips
		// Requires Bootstrap 3 for functionality
		$('.js-tooltip').tooltip();

		// Copy to clipboard
		// Grab any text in the attribute 'data-copy' and pass it to the
		// copy function
		$('.js-copy').click(function() {
			var text = $(this).attr('data-copy');
			var el = $(this);
			copyToClipboard(text, el);
		});
	});
</script>













