{*
*
* Social Login
*
* @author BusinessTech.fr
* @copyright Business Tech
*
*           ____    _______
*          |  _ \  |__   __|
*          | |_) |    | |
*          |  _ <     | |
*          | |_) |    | |
*          |____/     |_|
*
*}
<form class="form-horizontal" action="{$sURI|escape:'htmlall':'UTF-8'}" method="post" id="fbpscVouchersForm" name="fbpscVouchersForm" {if $smarty.const._FPC_USE_JS == true}onsubmit="javascript: fbpsc.form('fbpscVouchersForm', '{$sURI|escape:'htmlall':'UTF-8'}', null, 'fbpscVouchersForm', 'fbpscVouchersForm', false, false, oBasicCallBack, 'Vouchers', 'loadingVoucherDiv');return false;"{/if}>

	<input type="hidden" name="sAction" value="{$aQueryParams.vouchers.action|escape:'htmlall':'UTF-8'}" />
	<input type="hidden" name="sType" value="{$aQueryParams.vouchers.type|escape:'htmlall':'UTF-8'}" />

	<h3><i class="fa fa-shopping-cart"></i>&nbsp;&nbsp;{l s='Incentive voucher configuration'  mod='facebookpsconnect'}</h3>

	{if !empty($bUpdate)}
		{include file="`$sConfirmInclude`"}
	{elseif !empty($aErrors)}
		{include file="`$sErrorInclude`"}
	{/if}


	<div class="form-group">
		<div class="col-lg-12">
			<div class="alert alert-info">{l s='This section lets you offer your customers incentives for connecting with a social network. They will be able to use it on their next purchase.' mod='facebookpsconnect'}
			<br />
			<br />
			{l s='Moreover, note that this offer information will be available in all account creation e-mails, if you activate those.' mod='facebookpsconnect'}</div>
		</div>
	</div>

	<div class="clr_20"></div>

	{* ENABLE VOUCHER *}
	<div class="form-group" id="bootstrap-bouton">
	<label class="control-label col-xs-12 col-sm-12 col-md-2 col-lg-2"><span class="label-tooltip" data-toggle="tooltip" title data-original-title="{l s='Select "Yes" to configure the incentive offer' mod='facebookpsconnect'}"><strong>{l s='Configure an incentive voucher' mod='facebookpsconnect'}</strong></span> :</label>
		<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
				<span class="switch prestashop-switch fixed-width-md">
					<input type="radio" name="fbpscEnableVoucher" id="fbpscEnableVoucher_on" value="true" {if !empty($sEnableVouchers)}checked="checked"{/if}
					       onclick="fbpsc.changeSelect('fbpscEnableVoucher', 'fbpscDivVoucher', null, null, true, true);"/>
					<label for="fbpscEnableVoucher_on" class="radioCheck">
						{l s='Yes' mod='facebookpsconnect'}
					</label>
					<input type="radio" name="fbpscEnableVoucher" id="fbpscEnableVoucher_off" value="false" {if empty($sEnableVouchers)}checked="checked"{/if}
					       onclick="fbpsc.changeSelect('fpcEnableVoucher','fbpscDivVoucher', null, null, true, false);"/>
					<label for="fbpscEnableVoucher_off" class="radioCheck">
						{l s='No' mod='facebookpsconnect'}
					</label>
					<a class="slide-button btn"></a>
				</span>

		</div>
		<span class="label-tooltip" data-toggle="tooltip" title data-original-title="{l s='Select "Yes" to configure the incentive offer' mod='facebookpsconnect'}">&nbsp;<span class="icon-question-sign"></span></span>
	</div>

	{* IF ENABLE VOUCHER *}
	<div id="fbpscDivVoucher" style="display: {if !empty($sEnableVouchers)}block{else}none{/if};">

		{* VOUCHER PREFIX CODE *}
		<div class="form-group">
			<label class="control-label col-xs-12 col-sm-12 col-md-2 col-lg-2 required"><span class="label-tooltip" data-toggle="tooltip" title data-original-title="{l s='This will only be the prefix of the code. It will be used for the first part of the voucher code which the customer will type in during check-out to receive his discount. The module will add a second part for the code to be unique. The prefix MUST BE AT LEAST 3 characters long.' mod='facebookpsconnect'}"><strong>{l s='Code' mod='facebookpsconnect'}</strong></span> :</label>
			<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
					<input type="text" size="5" maxlength="5" name="fbpscPrefixCode" value="{if !empty($sVoucherCodePrefix)}{$sVoucherCodePrefix|escape:'htmlall':'UTF-8'}{/if}" id="fbpscPrefixCode" />
					<p class="help-block pull-left">
						<i class="icon-warning-sign text-primary">&nbsp;</i>{l s='Invalid characters: numbers and' mod='facebookpsconnect'}
						<br />
						{literal}!<>,;?=+()@#"�{}_$%:{/literal}
					</p>
				</div>
				<span class="label-tooltip" data-toggle="tooltip" title data-original-title="{l s='This will only be the prefix of the code. It will be used for the first part of the voucher code which the customer will type in during check-out to receive his discount. The module will add a second part for the code to be unique. The prefix MUST BE AT LEAST 3 characters long.' mod='facebookpsconnect'}">&nbsp;<span class="icon-question-sign"></span></span>
			</div>
		</div>

		{* VOUCHER'S TYPE *}
		<div class="form-group">
			<label class="control-label col-lg-2"><strong>{l s='Type' mod='facebookpsconnect'}</strong> :</label>
			<div class="col-lg-3">
				<select name="fbpscDiscountType" id="fbpscDiscountType" class="col-lg-8">
					<option value="none" {if empty($sVoucherType)}selected="selected"{/if}>{l s='None' mod='facebookpsconnect'}</option>
					<option value="percentage" {if !empty($sVoucherType) && $sVoucherType == 'percentage'}selected="selected"{/if}>{l s='Discount on order (%)' mod='facebookpsconnect'}</option>
					<option value="amount" {if !empty($sVoucherType) && $sVoucherType == 'amount'}selected="selected"{/if}>{l s='Discount on order (amount)' mod='facebookpsconnect'}</option>
				</select>
			</div>
		</div>
		{* PERCENT *}
		<div id="apply_discount_percent_div" class="form-group" style="display: {if !empty($sVoucherType) && $sVoucherType == 'percentage'}block{else}none{/if};">
			<label class="control-label col-lg-2 required"><span class="label-tooltip" data-toggle="tooltip" title data-original-title="{l s='Either the monetary amount or the %, depending on "Type" selected above' mod='facebookpsconnect'}"><strong>{l s='Value' mod='facebookpsconnect'}</strong></span></label>
			<div class="col-lg-3">
				<div class="input-group col-lg-3" style="float: left;">
					<span class="input-group-addon">%</span>
					<input type="text" id="fbpscVoucherPercent" class="input-mini" name="fbpscVoucherPercent" value="{if !empty($sVoucherPercent)}{$sVoucherPercent|escape:'htmlall':'UTF-8'}{else}0{/if}">
				</div>
				<span class="label-tooltip" data-toggle="tooltip" title data-original-title="{l s='Either the monetary amount or the %, depending on "Type" selected above' mod='facebookpsconnect'}">&nbsp;<span class="icon-question-sign"></span></span>
				<div style="clear: both;"></div><span class="help-block"><i class="icon-warning-sign text-primary"></i> {l s='Does not apply to the shipping costs' mod='facebookpsconnect'}</span>
			</div>
		</div>
		{* AMOUNT *}
		<div id="apply_discount_amount_div" class="form-group" style="display: {if !empty($sVoucherType) && $sVoucherType == 'amount'}block{else}none{/if};">
			<label class="control-label col-lg-2 required"><span class="label-tooltip" data-toggle="tooltip" title data-original-title="{l s='Either the monetary amount or the %, depending on "Type" selected above' mod='facebookpsconnect'}"><strong>{l s='Value' mod='facebookpsconnect'}</strong></span></label>
			<div class="col-lg-5">
				<div class="row fixed-width-xxl" style="float: left;">
					<div class="col-lg-3">
						<input type="text" id="fbpscVoucherAmount" name="fbpscVoucherAmount" value="{if !empty($sVoucherAmount)}{$sVoucherAmount|escape:'htmlall':'UTF-8'}{else}0{/if}" onchange="this.value = this.value.replace(/,/g, '.');">
					</div>
					<div class="col-xs-3">
						<select id="id_currency" name="fbpscCurrencyId">
							{foreach from=$aCurrencies name=currency key=iKey item=aCurrency}
								<option value="{$aCurrency.id_currency|intval}" {if !empty($iVoucherCurrency) && $iVoucherCurrency == $aCurrency.id_currency}selected="selected"{/if} >{$aCurrency.sign|escape:'htmlall':'UTF-8'}</option>
							{/foreach}
						</select>
					</div>
					<div class="col-xs-6">
						<select id="id_tax" name="fbpscTax">
							<option value="0" {if !empty($sVoucherTax) && $sVoucherTax == 0}selected="selected"{/if} >{l s='Tax Excluded' mod='facebookpsconnect'}</option>
							<option value="1" {if !empty($sVoucherTax) && $sVoucherTax == 1}selected="selected"{/if} >{l s='Tax Included' mod='facebookpsconnect'}</option>
						</select>
					</div>
				</div>
				<span class="label-tooltip" data-toggle="tooltip" title data-original-title="{l s='Either the monetary amount or the %, depending on "Type" selected above' mod='facebookpsconnect'}">&nbsp;<span class="icon-question-sign"></span></span>
			</div>
		</div>

		{* VOUCHER'S DESCRIPTION *}
		<div id="fbpscDivFeaturesDisplay">

		<div class="form-group">
			<label class="control-label col-xs-12 col-sm-12 col-md-2 col-lg-2">
				<span class="label-tooltip" title="{l s='Enter the description of the code. For example: voucher code for connecting with social network' mod='facebookpsconnect'}"><b>{l s='Coupon description' mod='facebookpsconnect'}</b></span> :
			</label>
			<div id="homecat" class="col-xs-12 col-sm-12 col-md-5 col-lg-5" >
				{foreach from=$aLangs item=aLang}
					<div id="fbpscVoucherDesc_{$aLang.id_lang|intval}" class="translatable-field row lang-{$aLang.id_lang|intval}" {if $aLang.id_lang|intval != $iCurrentLang}style="display:none"{/if}>
						<div class="col-xs-10 col-sm-10 col-md-10 col-lg-10">
							<input type="text" id="fbpscVoucherDesc_{$aLang.id_lang|intval}" name="fbpscVoucherDesc_{$aLang.id_lang|intval}" {if !empty($aVouchersDesc)}{foreach from=$aVouchersDesc key=idLang item=sLangTitle}{if $idLang == $aLang.id_lang} value="{$sLangTitle|escape:'htmlall':'UTF-8'}"{/if}{/foreach}{/if} />
						</div>
						<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
							<button type="button" class="btn btn-default dropdown-toggle" tabindex="-1" data-toggle="dropdown">{$aLang.iso_code|escape:'htmlall':'UTF-8'}&nbsp;<i class="icon-caret-down"></i></button>
							<ul class="dropdown-menu">
								{foreach from=$aLangs item=aLang}
									<li><a href="javascript:hideOtherLanguage({$aLang.id_lang|intval});" tabindex="-1">{$aLang.name|escape:'htmlall':'UTF-8'}</a></li>
								{/foreach}
							</ul>
						</div>
					</div>
				{/foreach}
			</div>
		</div>

			<div id="bt_categories" style="{if !empty($sEnableVouchers)}block{else}none{/if};">
				<div class="form-group">
					<label class="control-label col-xs-12 col-sm-12 col-md-2 col-lg-2"><span class="label-tooltip" data-toggle="tooltip" title data-original-title="{l s='Check the categories for which the discount is valid' mod='facebookpsconnect'}"><strong>{l s='Categories' mod='facebookpsconnect'}</strong></span></label>
					<div class="col-xs-12 col-sm-12 col-md-5 col-lg-4">
						<div class="btn-actions">
							<div class="btn btn-default btn-mini" id="categoryCheck" onclick="return fbpsc.selectAll('input.categoryBox', 'check');"><span class="icon-plus-square"></span>&nbsp;{l s='Check All' mod='facebookpsconnect'}</div> - <div class="btn btn-default btn-mini" id="categoryUnCheck" onclick="return fbpsc.selectAll('input.categoryBox', 'uncheck');"><span class="icon-minus-square"></span>&nbsp;{l s='Uncheck All' mod='facebookpsconnect'}</div>
							<div class="clr_10"></div>
							<div class="clr_hr"></div>
							<div class="clr_10"></div>
						</div>
						<table class="table  table-striped table-responsive " style="width: 100%;">
							{foreach from=$aFormatCat name=category key=iKey item=aCat}
								<tr class="alt_row">
									<td>
										{$aCat.id_category|intval}
									</td>
									<td>
										<input type="checkbox" name="bt_category-box[]" class="categoryBox" id="bt_category-box_{$aCat.iNewLevel|intval}" value="{$aCat.id_category|intval}" {if !empty($aCat.bCurrent)}checked="checked"{/if} />
									</td>
									<td>
										<span class="icon icon-folder{if !empty($aCat.bCurrent)}-open{/if}" style="margin-left: {$aCat.iNewLevel|intval}5px;"></span>&nbsp;&nbsp;<span style="font-size:12px;">{$aCat.name|escape:'htmlall':'UTF-8'}</span>
									</td>
								</tr>
							{/foreach}
						</table>
					</div>
				</div>
			</div>

			{* MAXIMUM NUMBER OF VOUCHER *}
			<div class="form-group">
				<label class="control-label col-xs-12 col-sm-12 col-md-2 col-lg-2"><span class="label-tooltip" data-toggle="tooltip" title data-original-title="{l s='Select the maximum number of vouchers a single customer can receive for' mod='facebookpsconnect'}"><strong>{l s='Maximum quantity available' mod='facebookpsconnect'}</strong></span></label>
				<div class="col-xs-12 col-sm-12 col-md-5 col-lg-5">
					<div class="col-xs-12 col-sm-12 col-md-5 col-lg-5">
						<input type="text" id="fbpscMaximumQte" name="fbpscMaximumQte" value="{if !empty($sVoucherMaxQty)}{$sVoucherMaxQty|intval}{else}0{/if}" onchange="this.value = this.value.replace(/,/g, '.');">
					</div>
					<span class="label-tooltip" data-toggle="tooltip" title data-original-title="{l s='Select the maximum number of vouchers a single customer can receive for' mod='facebookpsconnect'}>&nbsp;<span class="icon-question-sign"></span></span>
				</div>
			</div>

			{* MINIMUM VOUCHER'S AMOUNT *}
			<div class="form-group">
				<label class="control-label col-xs-12 col-sm-12 col-md-2 col-lg-2"><span class="label-tooltip" data-toggle="tooltip" title data-original-title="{l s='Enter the minimum purchase amount for the voucher to be valid. Enter 0 if there is no minimum' mod='facebookpsconnect'}"><strong>{l s='Minimum order / purchase amount' mod='facebookpsconnect'}</strong></span></label>
				<div class="col-xs-12 col-sm-12 col-md-5 col-lg-5">
					<div class="col-xs-12 col-sm-12 col-md-5 col-lg-5">
						<input type="text" size="15" id="fbpscMinimum" name="fbpscMinimum" value="{if !empty($sVoucherMinAmount)}{$sVoucherMinAmount|intval}{else}0{/if}" onkeyup="javascript:this.value = this.value.replace(/,/g, '.'); " />
					</div>
					<span class="label-tooltip" data-toggle="tooltip" title data-original-title="{l s='Enter the minimum purchase amount for the voucher to be valid. Enter 0 if there is no minimum' mod='facebookpsconnect'}">&nbsp;<span class="icon-question-sign"></span></span>
				</div>
			</div>

			{* VOUCHER'S VALIDITY *}
			<div class="form-group">
				<label class="control-label col-xs-12 col-sm-12 col-md-2 col-lg-2"><span class="label-tooltip" data-toggle="tooltip" title data-original-title="{l s='Numbers of days for availability date. 365 days (1 year) is standard practice' mod='facebookpsconnect'}"><strong>{l s='Validity' mod='facebookpsconnect'}</strong></span></label>
				<div class="col-xs-12 col-sm-12 col-md-5 col-lg-5">
						<div class="col-xs-12 col-sm-12 col-md-5 col-lg-5">
							<input type="text" size="3" name="fbpscValidity" id="fbpscValidity" value="{if !empty($iVoucherValidity)}{$iVoucherValidity|intval}{else}365{/if}" />
							<i class="icon-warning-sign text-primary">&nbsp;</i>{l s='In days' mod='facebookpsconnect'}
						</div>
						<span class="label-tooltip" data-toggle="tooltip" title data-original-title="{l s='Numbers of days for availability date. 365 days (1 year) is standard practice' mod='facebookpsconnect'}">&nbsp;<span class="icon-question-sign"></span></span>
				</div>
			</div>

			{* HIGHLIGHT VOUCHER IN CUSTOMER'S CART *}
			<div class="form-group">
				<label class="control-label col-xs-12 col-sm-12 col-md-2 col-lg-2"><span class="label-tooltip" data-toggle="tooltip" title data-original-title="{l s='Select "Yes" to display the voucher in the cart summary if it is not yet in the cart' mod='facebookpsconnect'}"><strong>{l s='Highlight' mod='facebookpsconnect'}</strong></span> :</label>
				<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
					<span class="switch prestashop-switch fixed-width-lg">
						<input type="radio" name="fbpscHighlight" id="fbpscHighlight_on" value="true" {if !empty($sVoucherHighlight)}checked="checked"{/if}  />
						<label for="fbpscHighlight_on" class="radioCheck">
							{l s='Yes' mod='facebookpsconnect'}
						</label>
						<input type="radio" name="fbpscHighlight" id="fbpscHighlight_off" value="false" {if empty($sVoucherHighlight)}checked="checked"{/if} />
						<label for="fbpscHighlight_off" class="radioCheck">
							{l s='No' mod='facebookpsconnect'}
						</label>
						<a class="slide-button btn"></a>
					</span>
				</div>
				<span class="label-tooltip" data-toggle="tooltip" title data-original-title="{l s='Select "Yes" to display the voucher in the cart summary if it is not yet in the cart' mod='facebookpsconnect'}">&nbsp;<span class="icon-question-sign"></span></span>
			</div>

			{* CUMULATIVE VOUCHER WITH OTHERS *}
			{if empty($bVersion17)}
				<div class="form-group">
					<label class="control-label col-xs-12 col-sm-12 col-md-2 col-lg-2"><strong>{l s='Can be combined with other vouchers' mod='facebookpsconnect'}</strong> :</label>
					<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
						<span class="switch prestashop-switch fixed-width-lg">
							<input type="radio" name="fbpscCumulativeOther" id="fbpscCumulativeOther_on" value="true" {if !empty($sVoucherCumulateOthers)}checked="checked"{/if} />
							<label for="fbpscCumulativeOther_on" class="radioCheck">
								{l s='Yes' mod='facebookpsconnect'}
							</label>
							<input type="radio" name="fbpscCumulativeOther" id="fbpscCumulativeOther_off" value="false" {if empty($sVoucherCumulateOthers)}checked="checked"{/if} />
							<label for="fbpscCumulativeOther_off" class="radioCheck">
								{l s='No' mod='facebookpsconnect'}
							</label>
							<a class="slide-button btn"></a>
						</span>
					</div>
				</div>
			{/if}

			{* CUMULATIVE VOUCHER WITH PRICE REDUCTION *}
			<div class="form-group">
				{if empty($bVersion17)}
					<label class="control-label col-lg-2"><strong>{l s='Can be combined with price reductions' mod='facebookpsconnect'}</strong> :</label>
				{else}
					<label class="control-label col-lg-2"><strong>{l s='Can not be combined with price reductions or cart rules' mod='facebookpsconnect'}</strong> :</label>
				{/if}
				<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
					<span class="switch prestashop-switch fixed-width-lg">
						<input type="radio" name="fbpscCumulativeReduc" id="fbpscCumulativeReduc_on" value="true"{if !empty($sVoucherCumulateDiscount)}checked="checked""{/if} />
						<label for="fbpscCumulativeReduc_on" class="radioCheck">
							{l s='Yes' mod='facebookpsconnect'}
						</label>
						<input type="radio" name="fbpscCumulativeReduc" id="fbpscCumulativeReduc_off" value="false" {if empty($sVoucherCumulateDiscount)}checked="checked"{/if} />
						<label for="fbpscCumulativeReduc_off" class="radioCheck">
							{l s='No' mod='facebookpsconnect'}
						</label>
						<a class="slide-button btn"></a>
					</span>
				</div>
			</div>

		{*PREFERENCES OF CODE DISPLAY*}
		<div class="clr_20"></div>
		<h3><i class="icon-desktop"></i>&nbsp;&nbsp;{l s='Preferences of display' mod='facebookpsconnect'}</h3>
		<div class="clr_20"></div>


		<div class="form-group">
				<label class="control-label col-xs-12 col-sm-12 col-md-2 col-lg-2"><b>{l s='Conditions of code display' mod='facebookpsconnect'}</b> :</label>
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-4">
					<select id="fbpscVoucherDisplayMode" name="fbpscVoucherDisplayMode">
						<option value="none" {if (!empty($sDisplayMode) && $sDisplayMode == "none")}selected="selected" {/if} >{l s='Do not display' mod='facebookpsconnect'}</option>
						<option value="email" {if (!empty($sDisplayMode) && $sDisplayMode == "email")}selected="selected" {/if} >{l s='Only send the voucher code by e-mail' mod='facebookpsconnect'}</option>
						<option value="popup" {if (!empty($sDisplayMode) && $sDisplayMode == "popup")}selected="selected" {/if} >{l s='Display code when the customer uses a social button to log in' mod='facebookpsconnect'}</option>
						<option value="both" {if (!empty($sDisplayMode) && $sDisplayMode == "both")}selected="selected" {/if} >{l s='Send by e-mail + display when the customer uses a social button' mod='facebookpsconnect'}</option>
					</select>
				</div>
			</div>
			
			<div class="form-group" id="bootstrap-bouton">
				<label class="control-label col-xs-12 col-sm-12 col-md-2 col-lg-2"><b>{l s='Add a custom text to explain the offer above social login buttons' mod='facebookpsconnect'}</b></span> :</label>
				<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
					<span class="switch prestashop-switch fixed-width-md">
						<input type="radio" name="fbpscEnableCustomVoucherText" id="fbpscEnableCustomVoucherText_on" value="true" {if !empty($bCustomVoucerText)}checked="checked"{/if}
							   onclick="fbpsc.changeSelect('fbpscEnableCustomVoucherText', 'fbpscDivVoucherCustomText', null, null, true, true);"/>
						<label for="fbpscEnableCustomVoucherText_on" class="radioCheck">
							{l s='Yes' mod='facebookpsconnect'}
						</label>
						<input type="radio" name="fbpscEnableCustomVoucherText" id="fbpscEnableCustomVoucherText_off" value="false" {if empty($bCustomVoucerText)}checked="checked"{/if}
							   onclick="fbpsc.changeSelect('fbpscEnableCustomVoucherText','fbpscDivVoucherCustomText', null, null, true, false);"/>
						<label for="fbpscEnableCustomVoucherText_off" class="radioCheck">
							{l s='No' mod='facebookpsconnect'}
						</label>
						<a class="slide-button btn"></a>
					</span>
				</div>
			</div>

				<div class="form-group" id="fbpscDivVoucherCustomText" {if empty($bCustomVoucerText)}style="display: none"{/if}>
					<label class="control-label col-xs-12 col-sm-12 col-md-2 col-lg-2"><span class="label-tooltip" data-toggle="tooltip" title data-original-title="{l s='This is displayed in all module blocks and in the account page' mod='facebookpsconnect'}"><strong>{l s='Text to display' mod='facebookpsconnect'}</strong></span> :</label>
					<div class="col-xs-12 col-sm-12 col-md-7 col-lg-5">
						{foreach from=$aLangs item=aLang}
							<div id="fbpscDisplayVoucherText_{$aLang.id_lang|intval}" class="translatable-field row lang-{$aLang.id_lang|intval}" {if $aLang.id_lang != $iCurrentLang}style="display:none"{/if}>
								<div class="col-xs-10 col-sm-10 col-md-10 col-lg-10">
									<input type="text" id="fbpscDisplayVoucherText_{$aLang.id_lang|intval}" name="fbpscDisplayVoucherText_{$aLang.id_lang|intval}" {if !empty($aDisplayText)}{foreach from=$aDisplayText key=idLang item=sLangTitle}{if $idLang == $aLang.id_lang} value="{$sLangTitle|escape:'htmlall':'UTF-8'}"{/if}{/foreach}{/if} />
								</div>
								<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
									<button type="button" class="btn btn-default dropdown-toggle" tabindex="-1" data-toggle="dropdown">{$aLang.iso_code|escape:'htmlall':'UTF-8'}&nbsp;<i class="icon-caret-down"></i></button>
									<ul class="dropdown-menu">
										{foreach from=$aLangs item=aLang}
											<li><a href="javascript:hideOtherLanguage({$aLang.id_lang|intval});" tabindex="-1">{$aLang.name|escape:'htmlall':'UTF-8'}</a></li>
										{/foreach}
									</ul>
								</div>
							</div>
						{/foreach}
					</div>
					<span class="label-tooltip" data-toggle="tooltip" title data-original-title="{l s='This is displayed in all module blocks and in the account page' mod='facebookpsconnect'}>&nbsp;<span class="icon-question-sign"></span></span>
				</div>
			
			<div class="clr_20"></div>
		</div>
	</div>


	<div class="clr_20"></div>
	<div class="clr_hr"></div>
	<div class="clr_20"></div>

	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-11 col-lg-11">
			<div class="FormError" id="fbpscVouchersError"></div>
		</div>
		<div class="col-xs-12 col-sm-12 col-md-1 col-lg-1">
			<button class="btn btn-default pull-right" onclick="fbpsc.form('fbpscVouchersForm', '{$sURI|escape:'htmlall':'UTF-8'}', null, 'VouchersSettingsBlock', 'VouchersSettingsBlock', false, false, null, 'Vouchers', 'loadingVoucherDiv');return false;"><i class="process-icon-save"></i>{l s='Save' mod='facebookpsconnect'}</button>
		</div>
	</div>

	{literal}
	<script type="text/javascript">
		// handle cart rule type
		$("#fbpscDiscountType").bind('change', function (event)
				{
					$("#fbpscDiscountType option:selected").each(function ()
					{
						switch ($(this).val()) {
							case 'percentage' :
								$("#apply_discount_percent_div").slideDown();
								$("#apply_discount_amount_div").slideUp();
								$("#fbpscDivFeaturesDisplay").slideDown();
								break;
							case 'amount' :
								$("#apply_discount_percent_div").slideUp();
								$("#apply_discount_amount_div").slideDown();
								$("#fbpscDivFeaturesDisplay").slideDown();
								break;
							default:
								$("#apply_discount_percent_div").slideUp();
								$("#apply_discount_amount_div").slideUp();
								$("#fbpscDivFeaturesDisplay").slideUp();
								break;
						}
					});
				}
		).change();


		$(document).ready(function()
		{
			$(".icon-question-sign").tooltip();
			$(".label-tooltip").tooltip();
			$('.dropdown-toggle').dropdown();
		});
	</script>
{/literal}

</form>



