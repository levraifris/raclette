{*
*
* Social Login
*
* @author BusinessTech.fr
* @copyright Business Tech
*
*           ____    _______
*          |  _ \  |__   __|
*          | |_) |    | |
*          |  _ <     | |
*          | |_) |    | |
*          |____/     |_|
*
*}
{if !empty($aErrors)}
	{include file="`$sErrorInclude`"}
	{* USE CASE - edition review mode *}
{else}
	<style type="text/css">
		.panel-heading .accordion-toggle:after {
			font-family: FontAwesome;
			content: "\f077";
			float: right;
		}
		.panel-heading .accordion-toggle.collapsed:after {
			content: "\f078";
		}
	</style>

	<div id="connector-position " class="bootstrap col-xs-12" style="width: 860px; height: 800px !important;">
		<div id="fbpsc">
			{if $aConnectors.facebook.data != false || $aConnectors.google.data != false || $aConnectors.amazon.data != false || $aConnectors.paypal.data != false || $aConnectors.twitter.data != false}
				<form class="form-horizontal col-xs-12" method="post" id="bt_form-connector-position" name="bt_form-connector-position" onsubmit="fbpsc.form('bt_form-connector-position', '{$sURI|escape:'htmlall':'UTF-8'}', null, 'fbpscHookAdvanced', 'fbpscHookAdvanced', false, true, '', 'CustomTag', 'loadingPositionDiv');return false;">
				<input type="hidden" name="{$sCtrlParamName|escape:'htmlall':'UTF-8'}" value="{$sController|escape:'htmlall':'UTF-8'}" />
				<input type="hidden" name="sAction" value="{$aQueryParams.connectorPositionUpdate.action|escape:'htmlall':'UTF-8'}" />
				<input type="hidden" name="sType" value="{$aQueryParams.connectorPositionUpdate.type|escape:'htmlall':'UTF-8'}" />
				{if !empty($aPositionData)}
					<input type="hidden" name="PositionId" value="{$iPositionId|intval}" id="PositionId" />
				{/if}

				<div class="clr_20"></div>

				<div id="{$sModuleName|escape:'htmlall':'UTF-8'}CustomTagError"></div>

				{if !empty($aPositionData)}
					<h2 class="text-center">{l s='Update custom position' mod='facebookpsconnect'} : "{$sPositionName}" </h2>
				{else}
					<h2 class="text-center">{l s='Add new custom position' mod='facebookpsconnect'}</h2>
				{/if}

				<div class="clr_10"></div>
				<div class="clr_hr"></div>
				<div class="clr_10"></div>

				<div class="panel-group" id="accordion">
					<div class="panel panel-default panel-primary">
						<div class="panel-heading">
							<h4 class="panel-title">
								<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse1">
									<i class="fa fa-cog"></i>&nbsp; {l s='General settings' mod='facebookpsconnect'}
								</a>
							</h4>
						</div>
						<div id="collapse1" class="panel-collapse collapse in">
							<div class="panel-body">
								<div class="alert alert-info">
									{l s='Choose a page and right-click where you want the connectors to appear. Select "Inspect" and identify the HTML element that matches the best with the wanted position.' mod='facebookpsconnect'}
								</div>

								<div class="form-group">
									<label class="control-label col-lg-3"><span class="label-tooltip" title data-original-title="{l s='Select "Yes" if you want to activate this custom position' mod='facebookpsconnect'}" data-toggle="tooltip"><b>{l s='Activate the position' mod='facebookpsconnect'}</b></span> :</label>
									<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
										<span class="switch prestashop-switch fixed-width-md">
											<input type="radio" name="fbpscPageConnectorsActivate" id="fbpscPageConnectorsActivate_on" {if $sPositionStatus == 1} checked="checked" {/if} value="1" />
											<label for="fbpscPageConnectorsActivate_on" class="radioCheck">
												{l s='Yes' mod='facebookpsconnect'}
											</label>
											<input type="radio" name="fbpscPageConnectorsActivate" id="fbpscPageConnectorsActivate_off" {if $sPositionStatus == 0} checked="checked" {/if} value="0" />
											<label for="fbpscPageConnectorsActivate_off" class="radioCheck">
												{l s='No' mod='facebookpsconnect'}
											</label>
											<a class="slide-button btn"></a>
										</span>
									</div>
									<span class="label-tooltip" data-toggle="tooltip" title data-original-title="{l s='Select "Yes" if you want to activate this custom position' mod='facebookpsconnect'}">&nbsp;<span class="icon-question-sign"></span></span>
								</div>

								<div class="form-group" >
									<label class="control-label col-xs-12 col-sm-12 col-md-6 col-lg-3"><span class="label-tooltip" data-toggle="tooltip" title data-original-title="{l s='Give a name to this position. This name will allow you to find this custom position in the list. Please note that the module will automatically replace all blank spaces with underscores and add an ID to make this name unique' mod='facebookpsconnect'}"><strong>{l s='Position name' mod='facebookpsconnect'}</strong></span> :</label>
									<div class="col-xs-12 col-sm-12 col-md-6 col-lg-4">
										<input type="text" name="fbpscPositionName" {if !empty($sPositionName)} value="{$sPositionName}" {/if} id="fbpscPositionName" required="required"/>
									</div>
									<span class="label-tooltip" data-toggle="tooltip" title data-original-title="{l s='Give a name to this position. This name will allow you to find this custom position in the list. Please note that the module will automatically replace all blank spaces with underscores and add an ID to make this name unique' mod='facebookpsconnect'}">&nbsp;<span class="icon-question-sign"></span></span>
								</div>

								<div class="form-group">
									<label class="control-label col-lg-3"><span class="label-tooltip" data-toggle="tooltip" title data-original-title="{l s='Select the type of pages on which you want to add the connectors' mod='facebookpsconnect'}"><b>{l s='Page type' mod='facebookpsconnect'}</b></span> :</label>
									<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
										<select id="fbpscPageConnectors" name="fbpscPageConnectors">
											{foreach from=$pages key=key item=page}
												<option value="{$key}" {if !empty($aPositionData) && $aPositionData.page == $key} selected="selected" {/if}>{if $page.$sCurrentIso != ''}{$page.$sCurrentIso}{else}{$page.en}{/if}</option>
											{/foreach}
										</select>
									</div>
									<span class="label-tooltip" data-toggle="tooltip" title data-original-title="{l s='Select the type of pages on which you want to add the connectors' mod='facebookpsconnect'}">&nbsp;<i class="icon-question-sign"></i></span>
								</div>

								<div class="form-group" >
									<label class="control-label col-xs-12 col-sm-12 col-md-6 col-lg-3"><span class="label-tooltip" data-toggle="tooltip" title data-original-title="{l s='Write the name of the HTML element on which you want to add the connectors. This is a little bit technical, if you don\'t know how to identify this HTML element, you should contact your webmaster. Please note that if you want to use a "class" you have to add a point before the element name (for example .product-prices) and if you want to use an "id" you have to add an hashtag before it (for example #content-wrapper)' mod='facebookpsconnect'}"><strong>{l s='HTML element name' mod='facebookpsconnect'}</strong></span> :</label>
									<div class="col-xs-12 col-sm-12 col-md-6 col-lg-3">
										<input type="text" name="fbpscHtmlElement" {if !empty($aPositionData) && !empty($aPositionData.html)} value="{$aPositionData.html}" {/if} id="fbpscHtmlElement" required="required"/>
									</div>
									<span class="label-tooltip" data-toggle="tooltip" title data-original-title="{l s='Write the name of the HTML element on which you want to add the connectors. This is a little bit technical, if you don\'t know how to identify this HTML element, you should contact your webmaster. Please note that if you want to use a "class" you have to add a point before the element name (for example .product-prices) and if you want to use an "id" you have to add an hashtag before it (for example #content-wrapper)' mod='facebookpsconnect'}">&nbsp;<span class="icon-question-sign"></span></span>
								</div>

								<div class="form-group">
									<label class="control-label col-lg-3"><span class="label-tooltip" data-toggle="tooltip" title data-original-title="{l s='Specify the buttons position relative to the element. It can be above or below' mod='facebookpsconnect'}"><b>{l s='Buttons position' mod='facebookpsconnect'}</b></span> :</label>
									<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
										<select id="fbpscPageConnectorsPosition" name="fbpscPageConnectorsPosition">
											{foreach from=$positions key=key item=position}
												<option value="{$key}" {if !empty($aPositionData) && $aPositionData.position == $key} selected="selected" {/if}>{if $position.$sCurrentIso != ''}{$position.$sCurrentIso}{else}{$position.en}{/if}</option>
											{/foreach}
										</select>
									</div>
									<span class="label-tooltip" data-toggle="tooltip" title data-original-title="{l s='Specify the buttons position relative to the element. It can be above or below' mod='facebookpsconnect'}">&nbsp;<span class="icon-question-sign"></span></span>
								</div>
							</div>
						</div>
					</div>
					<div class="panel panel-default panel-primary">
						<div class="panel-heading" >
							<h4 class="panel-title">
								<a  class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse2">
									<i class="fa fa-check"></i>&nbsp; {l s='Select connectors to add' mod='facebookpsconnect'}</a>
							</h4>
						</div>
						<div id="collapse2" class="panel-collapse collapse">
							<div class="panel-body">
								<div class="clr_10"></div>

								<p class="alert alert-info">
									{l s='Check the social networks buttons you want to add' mod='facebookpsconnect'}
								</p>

								<div class="btn-actions">
									<div class="btn btn-default btn-mini" id="categoryCheck" onclick="return fbpsc.selectAll('input.categoryBox', 'check');"><span class="icon-plus-square"></span>&nbsp;{l s='Check All' mod='facebookpsconnect'}</div> - <div class="btn btn-default btn-mini" id="categoryUnCheck" onclick="return fbpsc.selectAll('input.categoryBox', 'uncheck');"><span class="icon-minus-square"></span>&nbsp;{l s='Uncheck All' mod='facebookpsconnect'}</div>
									<div class="clr_10"></div>
								</div>

								<div class="center-block">
									{foreach from=$aConnectors item=aConnector key=key}
										{if $aConnector.data != false}
											<span class="col-xs-12">
												<input class="col-xs-1 categoryBox" type="checkbox" name="connector[{$key}]" value="{$key}" {if $key|in_array:$aConnectorsActive}checked{/if}/></td>
													<a class="btn-connect btn-block-connect btn-social {if $key == 'facebook'}btn-facebook{elseif $key == 'amazon'}btn-amazon{elseif $key == 'google'}btn-google{elseif $key == 'paypal'}btn-paypal{elseif $key == 'twitter'}btn-twitter{/if}" >
                                                        {if $key == 'google'}
                                                            <span class="btn-google-icon"></span>
                                                        {else}
    														<span class="fa fa-{$key}{if $key == 'facebook'}-square{/if}"></span>
                                                        {/if}
														<span class='btn-title-connect'>{$key|ucfirst|escape:'htmlall':'UTF-8'}</span>
													</a>
											</span>
										{/if}
									{/foreach}
								</div>
							</div>
						</div>
					</div>
					<div class="panel panel-default panel-primary">
						<div class="panel-heading">
							<h4 class="panel-title">
								<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse3">
									<i class="fa fa-css3"></i>&nbsp; {l s='Configure the CSS' mod='facebookpsconnect'}
								</a>
							</h4>
						</div>

						<div id="collapse3" class="panel-collapse collapse">
							<div class="panel-body">

							<div class="clr_10"></div>

							<div id="bt_css_config">
								<div class="panel-group" id="sub_accordion">
									<div class="panel panel-default">
										<div class="panel-heading">
											<h4 class="panel-title">
												<a  class="accordion-toggle" data-toggle="collapse" data-parent="#sub_accordion" href="#collapseOne">
													<i class="fa fa-desktop"></i>&nbsp;{l s='Border management' mod='facebookpsconnect'}
												</a>
											</h4>
										</div>
										<div id="collapseOne" class="panel-collapse collapse in">
											<div class="panel-body">
												<div id="ColorPickerBorder"class="input-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
													<div class="form-group" >
														<label class="control-label col-xs-12 col-sm-12 col-md-6 col-lg-3">{l s='Border color' mod='facebookpsconnect'} :</label>
														<div class="text-center col-xs-12 col-sm-12 col-md-6 col-lg-3">
															<input type="color" name="sCssBorderColor"  value="{if !empty({$aConnectorsCss.border_color})}{{$aConnectorsCss.border_color}}{else}#f6f6f6{/if}"  class="color mColorPicker" id="sCssBorderColor" required="required"/>
														</div>
													</div>
												</div>

												<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
													<div class="form-group" >
														<label class="control-label col-xs-12 col-sm-12 col-md-6 col-lg-3">{l s='Border size' mod='facebookpsconnect'} :</label>
														<div class="col-xs-12 col-sm-12 col-md-6 col-lg-3">
															<input type="text" name="sCssBorderSize"  value="{if $aConnectorsCss.border_size == ''}0{else}{$aConnectorsCss.border_size}{/if}" id="sCssBorderSize" required="required"/>
														</div>
														<strong>px</strong>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="panel panel-default">
										<div class="panel-heading" >
											<h4 class="panel-title">
												<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#sub_accordion" href="#collapseTwo">
													<i class="fa fa-image"></i>&nbsp;{l s='Background' mod='facebookpsconnect'}
												</a>
											</h4>
										</div>
										<div id="collapseTwo" class="panel-collapse collapse">
											<div class="panel-body">
												<div id="ColorPickerBackground"class="input-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
													<div class="form-group" >
														<label class="control-label col-xs-12 col-sm-12 col-md-6 col-lg-3">{l s='Background color' mod='facebookpsconnect'} :</label>
														<div class="text-center col-xs-12 col-sm-12 col-md-6 col-lg-3">
															<input type="color" name="sCssBackgroundColor"  value="{if !empty($aConnectorsCss.background_color)}{$aConnectorsCss.background_color}{else}#f3f3f3{/if}" class="color mColorPicker" id="sCssBackgroundColor" required="required"/>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									{if !empty($bDisplayCustomText)}
										<div class="panel panel-default">
										<div class="panel-heading">
											<h4 class="panel-title">
												<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#sub_accordion" href="#collapseThree">
													<i class="fa fa-text-height"></i>&nbsp;{l s='Text' mod='facebookpsconnect'}
												</a>
											</h4>
										</div>
										<div id="collapseThree" class="panel-collapse collapse">
											<div class="panel-body">

											<div class="form-group">
												<label class="control-label col-lg-3"><span class="label-tooltip" title data-original-title="{l s='Select "No" if you want to hide the custom text for this position' mod='facebookpsconnect'}" data-toggle="tooltip"><b>{l s='Activate the custom text display' mod='facebookpsconnect'}</b></span> :</label>
													<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
														<span class="switch prestashop-switch fixed-width-md">
															<input type="radio" name="bDisplayBlock" id="bDisplayBlock_on" {if $aConnectorsCss.activate == 1} checked="checked" {/if} value="1" />
															<label for="bDisplayBlock_on" class="radioCheck">
																{l s='Yes' mod='facebookpsconnect'}
															</label>
															<input type="radio" name="bDisplayBlock" id="bDisplayBlock_off" {if $aConnectorsCss.activate == 0} checked="checked" {/if} value="0"/>
															<label for="bDisplayBlock_off" class="radioCheck">
																{l s='No' mod='facebookpsconnect'}
															</label>
															<a class="slide-button btn"></a>
														</span>
													</div>
												<span class="label-tooltip" data-toggle="tooltip" title data-original-title="{l s='Select "No" if you want to hide the custom text for this position' mod='facebookpsconnect'}">&nbsp;<i class="icon-question-sign"></i></span>
											</div>

												<div id="ColorPickerText"class="input-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
													<div class="form-group" >
														<label class="control-label col-xs-12 col-sm-12 col-md-6 col-lg-3">{l s='Text color' mod='facebookpsconnect'} :</label>
														<div class="text-center col-xs-12 col-sm-12 col-md-6 col-lg-3">
															<input type="color" class="aoColorPickerFancy" name="sCssTextColor" value="{if !empty($aConnectorsCss.text_color)} {$aConnectorsCss.text_color}{else}#333333{/if}" class="color mColorPicker" id="sCssTextColor" required="required"/>
														</div>
													</div>
												</div>

												<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
													<div class="form-group" >
														<label class="control-label col-xs-12 col-sm-12 col-md-6 col-lg-3">{l s='Text size' mod='facebookpsconnect'} :</label>
														<div class="col-xs-12 col-sm-12 col-md-6 col-lg-3">
															<input type="text"  name="sCssTextSize"  value="{if $aConnectorsCss.text_size == ''}15{else}{$aConnectorsCss.text_size}{/if}" id="sCssTextSize" required="required"/>
														</div>
														<strong>px</strong>
													</div>
												</div>
											</div>
										</div>
									</div>
									{/if}
									<div class="panel panel-default">
										<div class="panel-heading" >
											<h4 class="panel-title">
												<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#sub_accordion" href="#collapseFour">
													<i class="fa fa-arrows-alt"></i>&nbsp;{l s='Padding' mod='facebookpsconnect'}
												</a>
											</h4>
										</div>
										<div id="collapseFour" class="panel-collapse collapse">
											<div class="panel-body">
												<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
													<div class="form-group" >
														<label class="control-label col-xs-12 col-sm-12 col-md-6 col-lg-3">{l s='Padding top' mod='facebookpsconnect'} :</label>
														<div class="col-xs-12 col-sm-12 col-md-6 col-lg-3">
															<input type="text"  name="sCssPaddingTop"  value="{if $aConnectorsCss.padding_top == ''}10{else}{$aConnectorsCss.padding_top}{/if}"  id="sCssPaddingTop" required="required"/>
														</div>
														<strong>px</strong>
													</div>
												</div>

												<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
													<div class="form-group" >
														<label class="control-label col-xs-12 col-sm-12 col-md-6 col-lg-3">{l s='Padding right' mod='facebookpsconnect'} :</label>
														<div class="col-xs-12 col-sm-12 col-md-6 col-lg-3">
															<input type="text"  name="sCssPaddingRight"  value="{if $aConnectorsCss.padding_right == ''}10{else}{$aConnectorsCss.padding_right}{/if}"  id="sCssPaddingRight" required="required"/>
														</div>
														<strong>px</strong>
													</div>
												</div>

												<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
													<div class="form-group" >
														<label class="control-label col-xs-12 col-sm-12 col-md-6 col-lg-3">{l s='Padding bottom' mod='facebookpsconnect'} :</label>
														<div class="col-xs-12 col-sm-12 col-md-6 col-lg-3">
															<input type="text"  name="sCssPaddingBottom"  value="{if $aConnectorsCss.padding_bottom == ''}10{else}{$aConnectorsCss.padding_bottom}{/if}"  id="sCssPaddingBottom" required="required"/>
														</div>
														<strong>px</strong>
													</div>
												</div>

												<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
													<div class="form-group" >
														<label class="control-label col-xs-12 col-sm-12 col-md-6 col-lg-3">{l s='Padding left' mod='facebookpsconnect'} :</label>
														<div class="col-xs-12 col-sm-12 col-md-6 col-lg-3">
															<input type="text"  name="sCssPaddingLeft"  value="{if $aConnectorsCss.padding_left == ''}10{else}{$aConnectorsCss.padding_left}{/if}" id="sCssPaddingLeft" required="required"/>
														</div>
														<strong>px</strong>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="panel panel-default">
										<div class="panel-heading" >
											<h4 class="panel-title">
												<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#sub_accordion" href="#collapseFive">
													<i class="fa fa-arrows-h"></i>&nbsp;{l s='Margin' mod='facebookpsconnect'}
												</a>
											</h4>
										</div>
										<div id="collapseFive" class="panel-collapse collapse">
											<div class="panel-body">
												<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
													<div class="form-group" >
														<label class="control-label col-xs-12 col-sm-12 col-md-6 col-lg-3">{l s='Margin top' mod='facebookpsconnect'} :</label>
														<div class="col-xs-12 col-sm-12 col-md-6 col-lg-3">
															<input type="text"  name="sCssMarginTop"  value="{if $aConnectorsCss.margin_top == ''}0{else}{$aConnectorsCss.margin_top}{/if}"  id="sCssMarginTop" required="required"/>
														</div>
														<strong>px</strong>
													</div>
												</div>

												<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
													<div class="form-group" >
														<label class="control-label col-xs-12 col-sm-12 col-md-6 col-lg-3">{l s='Margin right' mod='facebookpsconnect'} :</label>
														<div class="col-xs-12 col-sm-12 col-md-6 col-lg-3">
															<input type="text"  name="sCssMarginRight" value="{if $aConnectorsCss.margin_right == ''}0{else}{$aConnectorsCss.margin_right}{/if}" id="sCssMarginRight" required="required"/>
														</div>
														<strong>px</strong>
													</div>
												</div>

												<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
													<div class="form-group" >
														<label class="control-label col-xs-12 col-sm-12 col-md-6 col-lg-3">{l s='Margin bottom' mod='facebookpsconnect'} :</label>
														<div class="col-xs-12 col-sm-12 col-md-6 col-lg-3">
															<input type="text"  name="sCssMarginBottom"  value="{if $aConnectorsCss.margin_bottom == ''}0{else}{$aConnectorsCss.margin_bottom}{/if}"  id="sCssMarginBottom" required="required"/>
														</div>
														<strong>px</strong>
													</div>
												</div>

												<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
													<div class="form-group" >
														<label class="control-label col-xs-12 col-sm-12 col-md-6 col-lg-3">{l s='Margin left' mod='facebookpsconnect'} :</label>
														<div class="col-xs-12 col-sm-12 col-md-6 col-lg-3">
															<input type="text"  name="sCssMarginLeft"  value="{if $aConnectorsCss.margin_left == ''}0{else}{$aConnectorsCss.margin_left}{/if}"  id="sCssMarginLeft" required="required"/>
														</div>
														<strong>px</strong>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

				<div class="clr_20"></div>

				<p style="text-align: center !important;">
						<input type="button" name="{$sModuleName|escape:'htmlall':'UTF-8'}CommentButton" class="btn btn-success btn-lg" value="{if !empty($aPositionData)}{l s='Modify' mod='facebookpsconnect'}{else}{l s='Add' mod='facebookpsconnect'}{/if}" onclick="fbpsc.form('bt_form-connector-position', '{$sURI|escape:'htmlall':'UTF-8'}', null, 'fbpscHookAdvanced', 'fbpscHookAdvanced', false, true, '', 'CustomTag', 'loadingPositionDiv');return false;" />
				</p>
			</form>
			{else}
				<div class="alert alert-danger">
                    {l s='You have not configured any connectors. Please go to the "Social login buttons configuration" tab and configure at least one.' mod='facebookpsconnect'}
				</div>
			{/if}
			<div id="loadingPositionDiv" style="display: none;">
				<div class="alert alert-info">
					<p style="text-align: center !important;"><img src="{$sLoadingImg|escape:'htmlall':'UTF-8'}" alt="Loading" /></p><div class="clr_20"></div>
					<p style="text-align: center !important;">{l s='Your configuration updating is in progress...' mod='facebookpsconnect'}</p>
				</div>
			</div>
		</div>
	</div>
	</div>
{/if}
