{*
*
* Social Login
*
* @author BusinessTech.fr
* @copyright Business Tech
*
*           ____    _______
*          |  _ \  |__   __|
*          | |_) |    | |
*          |  _ <     | |
*          | |_) |    | |
*          |____/     |_|
*
*}

<div class="clr_20"></div>
<h3><i class="icon icon-bookmark"></i>&nbsp; {l s='Position advanced settings for pro: shortcode use'  mod='facebookpsconnect'}</h3>

<p class="alert alert-info">
    {l s='If the previous connectors positioning possibilities do not suit you and you want to completely customize their position, the module gives you the piece of code to add directly into the template of your theme. This is reserved for people who have technical knowledge. Do not hesitate to ask your webmaster to help you.' mod='facebookpsconnect'}
    <br/>
    <br/>
	{l s='Click on « Generate new shortcode » to generate the shortcode.' mod='facebookpsconnect'}
	<br/>
    <br/>
    <a class="badge badge-info" href="{$smarty.const._FPC_BT_FAQ_MAIN_URL|escape:'htmlall':'UTF-8'}/{$sFaqLang|escape:'htmlall':'UTF-8'}/faq/170" target="_blank"><i class="icon icon-link"></i>&nbsp;&nbsp;{l s='FAQ about the shortcode tool' mod='facebookpsconnect'}</a>
</p>

<div class="clr_10"></div>
<a id="handlePosition" class="pull-right fancybox.ajax btn btn-md btn-success" href="{$sURI|escape:'htmlall':'UTF-8'}&{$sCtrlParamName|escape:'htmlall':'UTF-8'}={$sController|escape:'htmlall':'UTF-8':'UTF-8'}&sAction={$aQueryParams.shortCodeForm.action|escape:'htmlall':'UTF-8'}&sType={$aQueryParams.shortCodeForm.type|escape:'htmlall':'UTF-8'}"><span class="icon-plus-circle"></span>&nbsp;{l s='Generate new shortcode' mod='facebookpsconnect'}</a>
<div class="clr_10"></div>

{if !empty($aShortCode)}
<div class="btn-actions">
    <div class="btn btn-default btn-mini" id="customCheck" onclick="return fbpsc.selectAll('input.fbpscCustomPosition', 'check');"><span class="icon-plus-square"></span>&nbsp;{l s='Check All' mod='facebookpsconnect'}</div>
    &nbsp;-&nbsp;
    <div class="btn btn-default btn-mini" id="categoryUnCheck" onclick="return fbpsc.selectAll('input.fbpscCustomPosition', 'uncheck');"><span class="icon-minus-square"></span>&nbsp;{l s='Uncheck All' mod='facebookpsconnect'}</div>
    -
    <div class="btn btn-warning btn-danger" name="bt_BulkActvation" id="customCheck"
         onclick="check = confirm('{l s='Are you sure you want to delete all position selected' mod='facebookpsconnect'} ?');if(!check)return false;
             iPosId = fbpsc.getBulkCheckBox('bt_bulkCustomPosition-box');
             $('#loadingHookAdvancedDiv').show();
             fbpsc.hide('fbpscShortCode');
             fbpsc.ajax('{$sURI|escape:'htmlall':'UTF-8'}', '{$sCtrlParamName|escape:'htmlall':'UTF-8'}={$sController|escape:'htmlall':'UTF-8'}&sAction={$aQueryParams.shortCodeList.action|escape:'htmlall':'UTF-8'}&sType={$aQueryParams.shortCodeList.type|escape:'htmlall':'UTF-8'}&iPositionId='+iPosId+'&sActionType=deleteBulk', 'fbpscShortCode', 'fbpscShortCode', null, null, 'loadingHookAdvancedDiv');">
        <span class="fa fa-trash"></span>&nbsp;{l s='Delete selection' mod='facebookpsconnect'}
    </div>
    <div class="clr_20"></div>
</div>

<table class="table table-bordered" id="bt_position_table">
    <thead>
    <th class="text-center col-xs-1"><b></b></th>
    <th class="text-center col-xs-1"><b>{l s='Shortcode name'  mod='facebookpsconnect'}</b></th>
    {* <th class="text-center col-xs-1"><b>{l s='Buttons size'  mod='facebookpsconnect'}</b></th> *}
    <th class="text-center col-xs-2"><b>{l s='Connectors'  mod='facebookpsconnect'}</b></th>
    <th class="text-center col-xs-4"><b>{l s='Shortcode to copy'  mod='facebookpsconnect'}</b></th>
    <th class="text-center"><b>{l s='Actions'  mod='facebookpsconnect'}</b></th>
	<th style="text-align: center;"><a id="handlePosition" class="fancybox.ajax btn btn-mini btn-success" href="{$sURI|escape:'htmlall':'UTF-8'}&{$sCtrlParamName|escape:'htmlall':'UTF-8'}={$sController|escape:'htmlall':'UTF-8':'UTF-8'}&sAction={$aQueryParams.shortCodeForm.action|escape:'htmlall':'UTF-8'}&sType={$aQueryParams.shortCodeForm.type|escape:'htmlall':'UTF-8'}"><span class="icon-plus-circle"></span></a></th>
    </thead>
    <tbody class="text-center">
	{foreach from=$aShortCode item=customPosition}
        <tr>
            <td><input name="bt_bulkCustomPosition-box" type="checkbox" class="fbpscCustomPosition" value="{$customPosition.id}"/></td>
            <td>{$customPosition.name|replace:'_':' '}</td>
            {* <td>{$customPosition.data.size}</td> *}
            <td>
                <div class="clr_10"></div>
                {if !empty($customPosition.data.connectors)}
		            {foreach from=$customPosition.data.connectors item=connector}
			            {if $sButtonStyle != 'modern'}
                            <p class="ao_bt_fpsc ao_bt_fpsc_{$connector|escape:'htmlall':'UTF-8'}">
                                <span class='picto'></span>
                                <span class='title'>{$connector|ucfirst|escape:'htmlall':'UTF-8'}</span>
                            </p>
			            {else}
                            <a class="btn-connect btn-block-connect btn-social btn-{$connector|escape:'htmlall':'UTF-8'}">
                                {if $connector == 'google'}
                                    <span class="btn-google-icon"></span>
                                {else}
                                    <span class="fa fa-{$connector|escape:'htmlall':'UTF-8'}{if $connector == 'facebook'}-square{/if}"></span>
                                {/if}
                                <span class='btn-title-connect'>{$connector|ucfirst|escape:'htmlall':'UTF-8'}</span>
                            </a>
			            {/if}
		            {/foreach}
	            {else}
                    <p class="alert alert-warning">
			            {l s='No connector added' mod='facebookpsconnect'}
                    </p>
	            {/if}
            </td>
            <td>
                <textarea name="fbpscShortCode" id="fbpscShortCode" cols="30" rows="5" disabled="disabled">{$customPosition.data.js}</textarea>

                <div class="clr_10"></div>
	            <button type="button" class="btn btn-info pull-right btn-copy js-tooltip js-copy" data-toggle="tooltip" data-placement="bottom" data-copy="{$customPosition.data.js}" title="{l s='Copy to clipboard' mod='facebookpsconnect'}">&nbsp;<i class="fa fa-copy"></i>&nbsp;{l s='Copy to clipboard' mod='facebookpsconnect'} </button>
	            <div class="clr_10"></div>

            </td>
            <td>
                <a href="#"><i class="icon-trash btn btn-mini btn-danger" title="{l s='delete' mod='facebookpsconnect'}" onclick="check = confirm('{l s='Are you sure to want to delete this position' mod='facebookpsconnect'} ? {l s='It will be definitely removed from your database' mod='facebookpsconnect'}');if(!check)return false;$('#loadingHookAdvancedDiv').show();fbpsc.hide('fbpscShortCode');fbpsc.ajax('{$sURI|escape:'htmlall':'UTF-8'}', '{$sCtrlParamName|escape:'htmlall':'UTF-8'}={$sController|escape:'htmlall':'UTF-8'}&sAction={$aQueryParams.shortCodeList.action|escape:'htmlall':'UTF-8'}&sType={$aQueryParams.shortCodeList.type|escape:'htmlall':'UTF-8'}&iPositionId={$customPosition.id|intval}&sActionType=delete', 'fbpscShortCode', 'fbpscShortCode', null, null, 'loadingHookAdvancedDiv');" ></i></a>
                <a id="handlePosition" class="fancybox.ajax btn btn-mini btn-info" href="{$sURI|escape:'htmlall':'UTF-8'}&{$sCtrlParamName|escape:'htmlall':'UTF-8'}={$sController|escape:'htmlall':'UTF-8':'UTF-8'}&sAction={$aQueryParams.shortCodeForm.action|escape:'htmlall':'UTF-8'}&sType={$aQueryParams.shortCodeForm.type|escape:'htmlall':'UTF-8'}&iPositionId={$customPosition.id|intval}"><span class="icon-edit"></span></a></th>
            </td>
            <td></td>
        </tr>
	{/foreach}
    </tbody>

</table>
{/if}
{literal}
<script type="text/javascript">
    $("a#handlePosition").fancybox({
        'hideOnContentClick' : false,
        'scrolling' : 'auto',
        'heigth' : 350
    })

    $(function() {
        $(".label-tooltip").tooltip();
    });

    function copyToClipboard(text, el) {
        var copyTest = document.queryCommandSupported('copy');
        var elOriginalText = el.attr('data-original-title');

        if (copyTest === true) {
	        var copyTextArea = document.createElement("textarea");
	        copyTextArea.value = text;
	        document.body.appendChild(copyTextArea);
	        copyTextArea.select();
	        try {
		        var successful = document.execCommand('copy');
		        var msg = successful ? 'Copied!' : 'Whoops, not copied!';
		        el.attr('data-original-title', msg).tooltip('show');
	        } catch (err) {
		        console.log('Oops, unable to copy');
	        }
	        document.body.removeChild(copyTextArea);
	        el.attr('data-original-title', elOriginalText);
        } else {
	        // Fallback if browser doesn't support .execCommand('copy')
	        window.prompt("Copy to clipboard: Ctrl+C or Command+C, Enter", text);
        }
    }

    $(document).ready(function() {
        // Initialize
        // ---------------------------------------------------------------------

        // Tooltips
        // Requires Bootstrap 3 for functionality
        $('.js-tooltip').tooltip();

        // Copy to clipboard
        // Grab any text in the attribute 'data-copy' and pass it to the
        // copy function
        $('.js-copy').click(function() {
	        var text = $(this).attr('data-copy');
	        var el = $(this);
	        copyToClipboard(text, el);
        });
    });
</script>
{/literal}
