{*
*
* Social Login
*
* @author BusinessTech.fr
* @copyright Business Tech
*
*           ____    _______
*          |  _ \  |__   __|
*          | |_) |    | |
*          |  _ <     | |
*          | |_) |    | |
*          |____/     |_|
*
*}

<h3><i class="fa fa-check"></i>&nbsp;&nbsp;{l s='Prerequisistes'  mod='facebookpsconnect'}</h3>

<div class="form-horizontal">
	<div class="form-group">
		<div class="col-xs-12">
			<div class="alert alert-info">
				{l s='This section lets you verify that your store is properly configured and meets all necessary technical requirements to qualify for Social Login program and for the module to function properly.'  mod='facebookpsconnect'}
			</div>
		</div>
	</div>

	<div class="form-group">
		<div class="col-xs-12">
			{if !empty($sCheckCurlInit)}
				<div class="alert alert-danger">
					{l s='Social Login needs the cURL PHP extension, please install and / or activate the extension before continuing with the configuration' mod='facebookpsconnect'}
				</div>
			{else}
				<div class="alert alert-success">
					{l s='cURL PHP extension is enabled' mod='facebookpsconnect'}
				</div>
			{/if}
		</div>
	</div>

	<div class="form-group">
		<div class="col-xs-12">
			{if !empty($sCheckAllowUrl)}
				<div class="alert alert-danger" >
					{l s='Social Login needs the ALLOW_URL_FOPEN PHP directive if cURL is not installed, please activate the directive before continuing with the configuration' mod='facebookpsconnect'}
				</div>
			{else}
				<div class="alert alert-success" >
					{l s='ALLOW_URL_FOPEN is enabled' mod='facebookpsconnect'}
				</div>
			{/if}
		</div>
	</div>

	{if !empty($bDevMode) && !empty($bVersion17)}
		<div class="form-group">
			<div class="col-xs-12">
					<div class="alert alert-danger" >
						{l s='Debug mode is enabled. For technical reasons the login buttons will not be displayed as long as the debug mode remains active.'  mod='facebookpsconnect'}
					</div>
			</div>
		</div>
	{/if}

	<div class="form-group">
		<div class="col-xs-12">
			{if !empty($sCheckGroup)}
				<div class="alert alert-success" >
					{l s='Default customer group is selected' mod='facebookpsconnect'}
				</div>
			{else}
				<div class="alert alert-danger" >
					{l s='The default customer group is not filled out, please choose your default group before continuing with the configuration' mod='facebookpsconnect'}
				</div>
			{/if}
		</div>
	</div>

	{if !empty($bOnePageCheckOut) && !empty($bTwitterActif)}
		<div class="form-group">
			<div class="col-xs-12">
				<div class="alert alert-danger">
					<p>{l s='If you have activated the Twitter connector and you are using the "One Page Checkout" feature, then you have to know that the Twitter connector doesn\'t get the right customer info. In this case, the customer will have to connect again via the Twitter button on PrestaShop to fill out the information, regarding first name, last name.' mod='facebookpsconnect'}</p>
					<p>{l s='For better order management, we recommend not using the Twitter connector and "One Page Checkout" together.' mod='facebookpsconnect'}</p>
				</div>
			</div>
		</div>
	{/if}

	{* Use - case ssl *}
	<div class="clr_10"></div>
	<h3><i class="icon icon-globe"></i>&nbsp;{l s='Test cURL with SSL' mod='facebookpsconnect'}</h3>

	<div class="form-group">
		<div class="col-xs-12">
			{if !empty($sCheckCurlInit)}
				<div class="alert alert-danger" >
					<h3>{l s='Before you can test, you need to have the cURL PHP extension installed and enabled !' mod='facebookpsconnect'}</h3>
				</div>
			{else}
				<div class="alert alert-info">
					{l s='Social networks other than Facebook use the OAuth system and so "cURL over SSL" MUST be enabled on your server. If you encounter connection problems to the social networks, you will have to contact your web host as the module needs "cURL over SSL"'  mod='facebookpsconnect'}
					<br/>
					<br/>
					<a class="btn btn-success fancybox.ajax" id="CurlSsl" href="{$sURI}&sAction={$aQueryParams.testssl.action|escape:'htmlall':'UTF-8'}&sType={$aQueryParams.testssl.type|escape:'htmlall':'UTF-8'}">{l s='Click to check' mod='facebookpsconnect'}</a>
				</div>
			{literal}
			<script type="text/javascript">
				$(document).ready(function()
				{
					$("a#CurlSsl").fancybox({
						'hideOnContentClick' : false,
						'scrolling' : 'no',
						'autoDimensions' : true
					});
				});
			</script>
			{/literal}
			{/if}
		</div>
	</div>

	<div class="clr_20"></div>
</div>




