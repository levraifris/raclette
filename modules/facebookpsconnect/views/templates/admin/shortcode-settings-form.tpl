{*
*
* Social Login
*
* @author BusinessTech.fr
* @copyright Business Tech
*
*           ____    _______
*          |  _ \  |__   __|
*          | |_) |    | |
*          |  _ <     | |
*          | |_) |    | |
*          |____/     |_|
*
*}

<style type="text/css">
	.panel-heading .accordion-toggle:after {
		font-family: FontAwesome;
		content: "\f077";
		float: right;
	}
	.panel-heading .accordion-toggle.collapsed:after {
		content: "\f078";
	}
</style>

<div id="connector-position " class="bootstrap col-xs-12" style="width: 860px; height: 800px !important;">
	<div id="fbpsc" class="form-group">


        {if $aConnectors.facebook.data != false || $aConnectors.google.data != false || $aConnectors.amazon.data != false || $aConnectors.paypal.data != false || $aConnectors.twitter.data != false}
			<form class="form-horizontal col-xs-12" method="post" id="fbpscShortCodeForm" name="fbpscShortCodeForm" onsubmit="fbpsc.form('fbpscShortCodeForm', '{$sURI|escape:'htmlall':'UTF-8'}', null, 'fbpscShortCode', 'fbpscShortCode', false, true, '', 'CustomShortCode', 'loadingShortCode');return false;">
				<input type="hidden" name="{$sCtrlParamName|escape:'htmlall':'UTF-8'}" value="{$sController|escape:'htmlall':'UTF-8'}" />
				<input type="hidden" name="sAction" value="{$aQueryParams.shortCode.action|escape:'htmlall':'UTF-8'}" />
				<input type="hidden" name="sType" value="{$aQueryParams.shortCode.type|escape:'htmlall':'UTF-8'}" />

				{if !empty($iPositionId)}
					<input type="hidden" name="iPositionId" value="{$iPositionId}" />
				{/if}

				<div class="clr_10"></div>
				<h2 class="text-center">{l s='Shortcode configuration' mod='facebookpsconnect'}</h2>
				<div class="clr_hr"></div>
				<div class="clr_10"></div>

				<div class="panel-group" id="accordion">
					<div class="panel panel-default panel-primary">
						<div class="panel-heading">
							<h4 class="panel-title">
								<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse1">
									<i class="fa fa-cog"></i>&nbsp; {l s='General settings' mod='facebookpsconnect'}
								</a>
							</h4>
						</div>
						<div id="collapse1" class="panel-collapse collapse in">
							<div class="panel-body">

								<div class="form-group" >
									<label class="control-label col-xs-12 col-sm-12 col-md-6 col-lg-3"><span class="label-tooltip" data-placement="bottom" data-toggle="tooltip" title data-original-title="{l s='Give a name to this position. This name will allow you to find this custom position in the list. Please note that the module will automatically replace all blank spaces with underscores and add an ID to make this name unique' mod='facebookpsconnect'}"><strong>{l s='Position name' mod='facebookpsconnect'}</strong></span> :</label>
									<div class="col-xs-12 col-sm-12 col-md-6 col-lg-3">
										<input type="text" name="fbpscPositionName" {if !empty($sPositionName)} value="{$sPositionName}" {/if} id="fbpscPositionName" required="required"/>
									</div>
									<span class="label-tooltip" data-placement="bottom" data-toggle="tooltip" title data-original-title="{l s='Give a name to this position. This name will allow you to find this custom position in the list. Please note that the module will automatically replace all blank spaces with underscores and add an ID to make this name unique' mod='facebookpsconnect'}">&nbsp;<span class="icon-question-sign"></span></span>
								</div>

								<div class="btn-actions">
									<div class="btn btn-default btn-mini" id="categoryCheck" onclick="return fbpsc.selectAll('input.categoryBox', 'check');"><span class="icon-plus-square"></span>&nbsp;{l s='Check All' mod='facebookpsconnect'}</div> - <div class="btn btn-default btn-mini" id="categoryUnCheck" onclick="return fbpsc.selectAll('input.categoryBox', 'uncheck');"><span class="icon-minus-square"></span>&nbsp;{l s='Uncheck All' mod='facebookpsconnect'}</div>
									<div class="clr_10"></div>
								</div>
								<div class="clr_10"></div>
								<div class="center-block"></div>

                                {foreach from=$aConnectors item=aConnector key=key}
                                    {if $aConnector.data != false}
                                        {foreach from =$aConnector.data|unserialize item=job key=keyConnector}
                                            {if $keyConnector == 'activeConnector' }
                                                <span class="col-xs-12">
                                                    <input class="col-xs-1 categoryBox" type="checkbox" name="connector[{$key}]" value="{$key}" {if $key|in_array:$aConnectorsActive}checked{/if}/></td>
                                                        <a class="btn-connect btn-block-connect btn-social {if $key == 'facebook'}btn-facebook{elseif $key == 'amazon'}btn-amazon{elseif $key == 'google'}btn-google{elseif $key == 'paypal'}btn-paypal{elseif $key == 'twitter'}btn-twitter{/if}" >
                                                            <span class="fa fa-{$key}{if $key == 'facebook'}-square{/if}"></span>
                                                            <span class='btn-title-connect'>{$key|ucfirst|escape:'htmlall':'UTF-8'}</span>
                                                        </a>
                                                </span>
                                            {/if}
                                        {/foreach}
                                    {/if}
                                {/foreach}
							</div>
						</div>
					</div>

					<div class="panel panel-default panel-primary">
						<div class="panel-heading">
							<h4 class="panel-title">
								<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse2">
									<i class="fa fa-css3"></i>&nbsp; {l s='Configure the css' mod='facebookpsconnect'}
								</a>
							</h4>
						</div>
						<div id="collapse2" class="panel-collapse collapse">
							<div class="panel-body">
								<div id="bt_css_config">
									<div class="panel-group" id="sub_accordion">
										<div class="panel panel-default">
											<div class="panel-heading">
												<h4 class="panel-title">
													<a  class="accordion-toggle" data-toggle="collapse" data-parent="#sub_accordion" href="#collapseOne">
														<i class="fa fa-desktop"></i>&nbsp;{l s='Border management' mod='facebookpsconnect'}
													</a>
												</h4>
											</div>
											<div id="collapseOne" class="panel-collapse collapse in">
												<div class="panel-body">
													<div id="ColorPickerBorder"class="input-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
														<div class="form-group" >
															<label class="control-label col-xs-12 col-sm-12 col-md-6 col-lg-3">{l s='Border color' mod='facebookpsconnect'} :</label>
															<div class="text-center col-xs-12 col-sm-12 col-md-6 col-lg-3">
																<input type="color" name="sCssBorderColor"  value="{if !empty({$aConnectorsCss.border_color})}{{$aConnectorsCss.border_color}}{else}#f6f6f6{/if}"  class="color mColorPicker" id="sCssBorderColor" required="required"/>
															</div>
														</div>
													</div>

													<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
														<div class="form-group" >
															<label class="control-label col-xs-12 col-sm-12 col-md-6 col-lg-3">{l s='Border size' mod='facebookpsconnect'} :</label>
															<div class="col-xs-12 col-sm-12 col-md-6 col-lg-3">
																<input type="text" name="sCssBorderSize"  value="{if $aConnectorsCss.border_size == ''}0{else}{$aConnectorsCss.border_size}{/if}" id="sCssBorderSize" required="required"/>
															</div>
															<strong>px</strong>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="panel panel-default">
											<div class="panel-heading" >
												<h4 class="panel-title">
													<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#sub_accordion" href="#collapseTwo">
														<i class="fa fa-image"></i>&nbsp;{l s='Background' mod='facebookpsconnect'}
													</a>
												</h4>
											</div>
											<div id="collapseTwo" class="panel-collapse collapse">
												<div class="panel-body">
													<div id="ColorPickerBackground"class="input-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
														<div class="form-group" >
															<label class="control-label col-xs-12 col-sm-12 col-md-6 col-lg-3">{l s='Background color' mod='facebookpsconnect'} :</label>
															<div class="text-center col-xs-12 col-sm-12 col-md-6 col-lg-3">
																<input type="color" name="sCssBackgroundColor"  value="{if !empty($aConnectorsCss.background_color)}{$aConnectorsCss.background_color}{else}#f3f3f3{/if}" class="color mColorPicker" id="sCssBackgroundColor" required="required"/>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
                                        {if !empty($bDisplayCustomText)}
											<div class="panel panel-default">
												<div class="panel-heading">
													<h4 class="panel-title">
														<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#sub_accordion" href="#collapseThree">
															<i class="fa fa-text-height"></i>&nbsp;{l s='Text' mod='facebookpsconnect'}
														</a>
													</h4>
												</div>
												<div id="collapseThree" class="panel-collapse collapse">
													<div class="panel-body">

														<div class="form-group">
															<label class="control-label col-lg-3"><span class="label-tooltip" title data-original-title="{l s='Select "No" if you want to hide the custom text for this position' mod='facebookpsconnect'}" data-toggle="tooltip"><b>{l s='Activate the custom text display' mod='facebookpsconnect'}</b></span> :</label>
															<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
															<span class="switch prestashop-switch fixed-width-md">
																<input type="radio" name="bDisplayBlock" id="bDisplayBlock_on" {if $aConnectorsCss.activate == 1} checked="checked" {/if} value="1" />
																<label for="bDisplayBlock_on" class="radioCheck">
																	{l s='Yes' mod='facebookpsconnect'}
																</label>
																<input type="radio" name="bDisplayBlock" id="bDisplayBlock_off" {if $aConnectorsCss.activate == 0} checked="checked" {/if} value="0"/>
																<label for="bDisplayBlock_off" class="radioCheck">
																	{l s='No' mod='facebookpsconnect'}
																</label>
																<a class="slide-button btn"></a>
															</span>
															</div>
															<span class="label-tooltip" data-toggle="tooltip" title data-original-title="{l s='Select "No" if you want to hide the custom text for this position' mod='facebookpsconnect'}">&nbsp;<span class="icon-question-sign"></span></span>
														</div>

														<div id="ColorPickerText"class="input-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
															<div class="form-group" >
																<label class="control-label col-xs-12 col-sm-12 col-md-6 col-lg-3">{l s='Text color' mod='facebookpsconnect'} :</label>
																<div class="text-center col-xs-12 col-sm-12 col-md-6 col-lg-3">
																	<input type="color" class="aoColorPickerFancy" name="sCssTextColor" value="{if !empty($aConnectorsCss.text_color)} {$aConnectorsCss.text_color}{else}#333333{/if}" class="color mColorPicker" id="sCssTextColor" required="required"/>
																</div>
															</div>
														</div>

														<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
															<div class="form-group" >
																<label class="control-label col-xs-12 col-sm-12 col-md-6 col-lg-3">{l s='Text size' mod='facebookpsconnect'} :</label>
																<div class="col-xs-12 col-sm-12 col-md-6 col-lg-3">
																	<input type="text"  name="sCssTextSize"  value="{if $aConnectorsCss.text_size == ''}15{else}{$aConnectorsCss.text_size}{/if}" id="sCssTextSize" required="required"/>
																</div>
																<strong>px</strong>
															</div>
														</div>
													</div>
												</div>
											</div>
										{/if}
										<div class="panel panel-default">
											<div class="panel-heading" >
												<h4 class="panel-title">
													<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#sub_accordion" href="#collapseFour">
														<i class="fa fa-arrows-alt"></i>&nbsp;{l s='Padding' mod='facebookpsconnect'}
													</a>
												</h4>
											</div>
											<div id="collapseFour" class="panel-collapse collapse">
												<div class="panel-body">
													<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
														<div class="form-group" >
															<label class="control-label col-xs-12 col-sm-12 col-md-6 col-lg-3">{l s='Padding top' mod='facebookpsconnect'} :</label>
															<div class="col-xs-12 col-sm-12 col-md-6 col-lg-3">
																<input type="text"  name="sCssPaddingTop"  value="{if $aConnectorsCss.padding_top == ''}10{else}{$aConnectorsCss.padding_top}{/if}"  id="sCssPaddingTop" required="required"/>
															</div>
															<strong>px</strong>
														</div>
													</div>

													<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
														<div class="form-group" >
															<label class="control-label col-xs-12 col-sm-12 col-md-6 col-lg-3">{l s='Padding right' mod='facebookpsconnect'} :</label>
															<div class="col-xs-12 col-sm-12 col-md-6 col-lg-3">
																<input type="text"  name="sCssPaddingRight"  value="{if $aConnectorsCss.padding_right == ''}10{else}{$aConnectorsCss.padding_right}{/if}"  id="sCssPaddingRight" required="required"/>
															</div>
															<strong>px</strong>
														</div>
													</div>

													<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
														<div class="form-group" >
															<label class="control-label col-xs-12 col-sm-12 col-md-6 col-lg-3">{l s='Padding bottom' mod='facebookpsconnect'} :</label>
															<div class="col-xs-12 col-sm-12 col-md-6 col-lg-3">
																<input type="text"  name="sCssPaddingBottom"  value="{if $aConnectorsCss.padding_bottom == ''}10{else}{$aConnectorsCss.padding_bottom}{/if}"  id="sCssPaddingBottom" required="required"/>
															</div>
															<strong>px</strong>
														</div>
													</div>

													<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
														<div class="form-group" >
															<label class="control-label col-xs-12 col-sm-12 col-md-6 col-lg-3">{l s='Padding left' mod='facebookpsconnect'} :</label>
															<div class="col-xs-12 col-sm-12 col-md-6 col-lg-3">
																<input type="text"  name="sCssPaddingLeft"  value="{if $aConnectorsCss.padding_left == ''}10{else}{$aConnectorsCss.padding_left}{/if}" id="sCssPaddingLeft" required="required"/>
															</div>
															<strong>px</strong>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="panel panel-default">
											<div class="panel-heading" >
												<h4 class="panel-title">
													<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#sub_accordion" href="#collapseFive">
														<i class="fa fa-arrows-h"></i>&nbsp;{l s='Margin' mod='facebookpsconnect'}
													</a>
												</h4>
											</div>
											<div id="collapseFive" class="panel-collapse collapse">
												<div class="panel-body">
													<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
														<div class="form-group" >
															<label class="control-label col-xs-12 col-sm-12 col-md-6 col-lg-3">{l s='Margin top' mod='facebookpsconnect'} :</label>
															<div class="col-xs-12 col-sm-12 col-md-6 col-lg-3">
																<input type="text"  name="sCssMarginTop"  value="{if $aConnectorsCss.margin_top == ''}0{else}{$aConnectorsCss.margin_top}{/if}"  id="sCssMarginTop" required="required"/>
															</div>
															<strong>px</strong>
														</div>
													</div>

													<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
														<div class="form-group" >
															<label class="control-label col-xs-12 col-sm-12 col-md-6 col-lg-3">{l s='Margin right' mod='facebookpsconnect'} :</label>
															<div class="col-xs-12 col-sm-12 col-md-6 col-lg-3">
																<input type="text"  name="sCssMarginRight" value="{if $aConnectorsCss.margin_right == ''}0{else}{$aConnectorsCss.margin_right}{/if}" id="sCssMarginRight" required="required"/>
															</div>
															<strong>px</strong>
														</div>
													</div>

													<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
														<div class="form-group" >
															<label class="control-label col-xs-12 col-sm-12 col-md-6 col-lg-3">{l s='Margin bottom' mod='facebookpsconnect'} :</label>
															<div class="col-xs-12 col-sm-12 col-md-6 col-lg-3">
																<input type="text"  name="sCssMarginBottom"  value="{if $aConnectorsCss.margin_bottom == ''}0{else}{$aConnectorsCss.margin_bottom}{/if}"  id="sCssMarginBottom" required="required"/>
															</div>
															<strong>px</strong>
														</div>
													</div>

													<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
														<div class="form-group" >
															<label class="control-label col-xs-12 col-sm-12 col-md-6 col-lg-3">{l s='Margin left' mod='facebookpsconnect'} :</label>
															<div class="col-xs-12 col-sm-12 col-md-6 col-lg-3">
																<input type="text"  name="sCssMarginLeft"  value="{if $aConnectorsCss.margin_left == ''}0{else}{$aConnectorsCss.margin_left}{/if}"  id="sCssMarginLeft" required="required"/>
															</div>
															<strong>px</strong>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="panel panel-default panel-primary">
						<div class="panel-heading">
							<h4 class="panel-title">
								<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse3">
									<i class="fa fa-code"></i>&nbsp; {l s='Shortcode to copy/paste in the template' mod='facebookpsconnect'}
								</a>
							</h4>
						</div>
						<div id="collapse3" class="panel-collapse collapse ">
							<div class="panel-body">

									<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
										<div class="form-group" >
											<label class="control-label col-xs-12 col-sm-12 col-md-6 col-lg-3"><span class="label-tooltip"><strong>{l s='Shortcode' mod='facebookpsconnect'}</strong></span> :</label>
											<div class="col-xs-12 col-sm-12 col-md-6 col-lg-9">
												<textarea disabled="disabled" name="fbpscShortCode" id="fbpscShortCode" cols="5" rows="10" placeholder="{l s='Please click on "Save" to generate the shortcode' mod='facebookpsconnect'}">{if !empty($sJsCode)}{$sJsCode}{/if}</textarea>
											</div>
										</div>
									</div>
							</div>

							</div>
						</div>
					</div>

				</div>

				<p style="text-align: center !important;">
					<button  name="{$sModuleName|escape:'htmlall':'UTF-8'}CommentButton" class="btn btn-default pull-right"onclick="fbpsc.form('fbpscShortCodeForm', '{$sURI|escape:'htmlall':'UTF-8'}', null, 'fbpscShortCode', 'fbpscShortCode', false, true, '', 'CustomShortCode', 'loadingShortCodeDiv');return false;" ><i class="icon icon-save icon-2x"></i><br/>{l s='Save' mod='facebookpsconnect'}</button>
				</p>
			</form>
		{else}
			<div class="alert alert-danger">
				{l s='You have not configured any connectors. Please go to the "Social login buttons configuration" tab and configure at least one.' mod='facebookpsconnect'}
			</div>
		{/if}

		<div id="loadingShortCodeDiv" style="display: none;">
			<div class="alert alert-info">
				<p style="text-align: center !important;"><img src="{$sLoadingImg|escape:'htmlall':'UTF-8'}" alt="Loading" /></p><div class="clr_20"></div>
				<p style="text-align: center !important;">{l s='Your configuration updating is in progress...' mod='facebookpsconnect'}</p>
			</div>
		</div>
   </div>

</div>
{literal}
	<script type="text/javascript">

		$(function() {
			$(".label-tooltip").tooltip();
		});


	</script>
{/literal}
