{*
*
* Social Login
*
* @author BusinessTech.fr
* @copyright Business Tech
*
*           ____    _______
*          |  _ \  |__   __|
*          | |_) |    | |
*          |  _ <     | |
*          | |_) |    | |
*          |____/     |_|
*
*}

<div id="fbpsc">
	<form class="form-horizontal" action="{$sURI|escape:'htmlall':'UTF-8'}" method="post" id="fbpscBasicForm" name="fbpscBasicForm" {if $smarty.const._FPC_USE_JS == true}onsubmit="javascript: fbpsc.form('fbpscBasicForm', '{$sURI|escape:'htmlall':'UTF-8'}', null, 'BasicSettings', 'BasicSettings', false, false, oBasicCallBack, 'Basics', 'loadingBasicsDiv');return false;"{/if}>
		<input type="hidden" name="sAction" value="{$aQueryParams.basic.action|escape:'htmlall':'UTF-8'}" />
		<input type="hidden" name="sType" value="{$aQueryParams.basic.type|escape:'htmlall':'UTF-8'}" />

		<h3><i class="fa fa-heart"></i>&nbsp;&nbsp;{l s='Social Login Basic Settings'  mod='facebookpsconnect'}</h3>

		{if !empty($bUpdate)}
			{include file="`$sConfirmInclude`"}
		{elseif !empty($aErrors)}
			{include file="`$sErrorInclude`"}
		{/if}

		<div class="form-group">
			<label class="control-label col-lg-3"><span class="label-tooltip" data-toggle="tooltip"><b>{l s='Display custom text' mod='facebookpsconnect'}</b></span> :</label>
			<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
				<span class="switch prestashop-switch fixed-width-md">
					<input type="radio" name="fbpscDisplayCustomText" id="fbpscDisplayCustomText_on" value="true" onclick="javascript: fbpsc.changeSelect('custom_text', 'custom_text', null, null, true, true);" {if !empty($bDisplayCustomText)}checked="checked"{/if} />
					<label for="fbpscDisplayCustomText_on" class="radioCheck">
						{l s='Yes' mod='facebookpsconnect'}
					</label>
					<input type="radio" name="fbpscDisplayCustomText" id="fbpscDisplayCustomText_off" value="false" onclick="javascript: fbpsc.changeSelect('custom_text', 'custom_text', null, null, true, false);" {if empty($bDisplayCustomText)}checked="checked"{/if} />
					<label for="fbpscDisplayCustomText_off" class="radioCheck">
						{l s='No' mod='facebookpsconnect'}
					</label>
					<a class="slide-button btn"></a>
				</span>
			</div>
		</div>

		<div id="custom_text" style="display: {if !empty($bDisplayCustomText)} block{else} none{/if}">
		{* Use - case custom text *}
			<div class="form-group" id="fbpscDefaultConnectText">
				<label class="control-label col-xs-12 col-sm-12 col-md-2 col-lg-3"><span class="label-tooltip" data-toggle="tooltip" title data-original-title="{l s='This is the text that will be displayed above connection buttons. Note that you will be able to activate or not its display depending on connectors position (in the "Design management" tab)' mod='facebookpsconnect'}"><strong>{l s='My custom connection text' mod='facebookpsconnect'}</strong></span> :</label>
				<div class="col-xs-12 col-sm-12 col-md-5 col-lg-5">
					{foreach from=$aLangs item=aLang}
						<div id="fbpscDivAccountCustomText_{$aLang.id_lang|intval}" class="translatable-field row lang-{$aLang.id_lang|intval}" {if $aLang.id_lang != $iCurrentLang}style="display:none"{/if}>
							<div class="col-xs-10 col-sm-10 col-md-11 col-lg-11">
								<input type="text" class="fbpscDefaultConnectText" id="fbpscDefaultConnectText_{$aLang.id_lang|intval}" name="fbpscDefaultConnectText_{$aLang.id_lang|intval}" {if !empty($aConnectionText)}{foreach from=$aConnectionText key=idLang item=sLangConnectText}{if $idLang == $aLang.id_lang} value="{$sLangConnectText|escape:'htmlall':'UTF-8'}"{/if}{/foreach}{/if} />
							</div>
							<div class="col-xs-2 col-sm-2 col-md-1 col-lg-1">
								<button type="button" class="btn btn-default dropdown-toggle" tabindex="-1" data-toggle="dropdown">{$aLang.iso_code|escape:'htmlall':'UTF-8'}&nbsp;<i class="icon-caret-down"></i></button>
								<ul class="dropdown-menu">
									{foreach from=$aLangs item=aLang}
										<li><a href="javascript:hideOtherLanguage({$aLang.id_lang|intval});" tabindex="-1">{$aLang.name|escape:'htmlall':'UTF-8'}</a></li>
									{/foreach}
								</ul>
							</div>
						</div>
					{/foreach}
				</div>
				<span class="label-tooltip" data-toggle="tooltip" title data-original-title="{l s='This is the text that will be displayed above connection buttons. Note that you will be able to activate or not its display depending on connectors position (in the "Design management" tab)' mod='facebookpsconnect'}>&nbsp;<span class="icon-question-sign"></span></span>
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-lg-3"><span class="label-tooltip" data-toggle="tooltip" title data-original-title="{l s='This will display a login block with the standard e-mail / password fields in the right or left column of your website, and will also add the social network login buttons. However, NOTE THAT for this you need to then associate the proper login buttons to either the "Right Column" or "Left Column" in the "Connectors position management (native hooks)" tab of this module configuration.' mod='facebookpsconnect'}"><b>{l s='Display login block in right/left column' mod='facebookpsconnect'}</b></span> :</label>
			<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
				<span class="switch prestashop-switch fixed-width-md">
					<input type="radio" name="fbpscDisplayBlock" id="fbpscDisplayBlock_on" value="true" {if !empty($bDisplayBlock)}checked="checked"{/if} />
					<label for="fbpscDisplayBlock_on" class="radioCheck">
						{l s='Yes' mod='facebookpsconnect'}
					</label>
					<input type="radio" name="fbpscDisplayBlock" id="fbpscDisplayBlock_off" value="false" {if empty($bDisplayBlock)}checked="checked"{/if} />
					<label for="fbpscDisplayBlock_off" class="radioCheck">
						{l s='No' mod='facebookpsconnect'}
					</label>
					<a class="slide-button btn"></a>
				</span>
			</div>
			<span class="label-tooltip" data-toggle="tooltip" title data-original-title="{l s='This will display a login block with the standard e-mail / password fields in the right or left column of your website, and will also add the social network login buttons. However, NOTE THAT for this you need to then associate the proper login buttons to either the "Right Column" or "Left Column" in the "Connectors position management (native hooks)" tab of this module configuration.' mod='facebookpsconnect'}">&nbsp;<span class="icon-question-sign"></span></span>
		</div>

		<div class="form-group">
			<label class="control-label col-lg-3"><span class="label-tooltip" data-toggle="tooltip" title data-original-title="{l s='Select the default customer group which will be associated with a customer who will create his account through a social connector' mod='facebookpsconnect'}"><b>{l s='Default customer group' mod='facebookpsconnect'}</b></span> :</label>
			<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
				<select id="fbpscDefaultGroup" name="fbpscDefaultGroup">
					{foreach from=$aGroups name=group key=key item=aGroup}
						<option value="{$aGroup.id_group}" {if $aGroup.id_group == $iDefaultCustomerGroup}selected="selected"{/if}>{$aGroup.name|escape:'htmlall':'UTF-8'}</option>
					{/foreach}
				</select>
			</div>
			<span class="label-tooltip" data-toggle="tooltip" title data-original-title="{l s='Select the default customer group which will be associated with a customer who will create his account through a social connector' mod='facebookpsconnect'}">&nbsp;<span class="icon-question-sign"></span></span>
		</div>

		<div class="form-group">
			<label class="control-label col-lg-3"><span class="label-tooltip" data-toggle="tooltip" title data-original-title="{l s='The value should be "PHP cURL library mode" but before selecting a value, PLEASE READ the blue info block below. Do not forget that when you get an error such as: "an internal server error" or "token is empty" or "state doesn\'t match", this may come from your connect method that is not allowed with HTTPS protocol' mod='facebookpsconnect'}"><b>{l s='Select your connect method to social networks' mod='facebookpsconnect'}</b></span> :</label>
			<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
				<select id="fbpscApiRequestType" name="fbpscApiRequestType">
					<option value="">...</option>
					{foreach from=$aApiCallMethod name=group key=key item=aMethod}
						<option value="{$aMethod.type}" {if $aMethod.type == $sApiRequestType}selected="selected"{/if} {if empty($aMethod.active)}disabled="disabled"{/if}>{$aMethod.name|escape:'htmlall':'UTF-8'}{if empty($aMethod.active)} - {l s='not enabled on your server' mod='facebookpsconnect'}{/if}</option>
					{/foreach}
				</select>
			</div>
			<span class="label-tooltip" data-toggle="tooltip" title data-original-title="{l s='The value should be "PHP cURL library mode" but before selecting a value, PLEASE READ the blue info block below. Do not forget that when you get an error such as: "an internal server error" or "token is empty" or "state doesn\'t match", this may come from your connect method that is not allowed with HTTPS protocol' mod='facebookpsconnect'}">&nbsp;<span class="icon-question-sign"></span></span>
		</div>

		<div class="form-group">
			<label class="control-label col-lg-3"></label>
			<div class="col-xs-12 col-sm-12 col-md-5 col-lg-5">
				<div class="{if $iCompare == -1}fbpscHint{else}alert alert-info{/if} clear" style="display: block !important;">
					{l s='Social networks other than Facebook use OAuth system, so "cURL over SSL" must be enabled on your server. If you encounter connection problems to the social networks, you will need to contact your web host as the module needs cURL over SSL. Once you have checked that "cURL over SSL" is enabled on your server, select the value "PHP cURL library mode" for the option above'  mod='facebookpsconnect'}.
				</div>
			</div>
		</div>

		<div id="bt_btn_style_classical" style="display: none">
			<label class="control-label col-lg-3"></label>

			<div class="col-xs-6">
				<div class="clr_10"></div>

				{foreach from=$aConnectorsList item=aConnector key=key}
					<p class="ao_bt_fpsc {if $key == 'facebook'}ao_bt_fpsc_facebook{elseif $key == 'amazon'}ao_bt_fpsc_amazon{elseif $key == 'google'}ao_bt_fpsc_google{elseif $key == 'paypal'}ao_bt_fpsc_paypal{elseif $key == 'twitter'}ao_bt_fpsc_twitter{/if}" >
						<span class='picto'></span>
						<span class='title'>{$key|ucfirst|escape:'htmlall':'UTF-8'}</span>
					</p>
				{/foreach}
				</div>
		</div>

		<div id="bt_btn_style_modern" style="display: none">
			<label class="control-label col-lg-3"></label>

			<div class="col-xs-9">
				<div class="clr_10"></div>
					{foreach from=$aConnectorsList item=aConnector key=key}
						<a class="btn-connect btn-block-connect btn-social {if $key == 'facebook'}btn-facebook{elseif $key == 'amazon'}btn-amazon{elseif $key == 'google'}btn-google{elseif $key == 'paypal'}btn-paypal{elseif $key == 'twitter'}btn-twitter{/if}" >
							<span class="fa fa-{$key}"></span>
							<span class='btn-title-connect'>{$key|ucfirst|escape:'htmlall':'UTF-8'}</span>
						</a>
					{/foreach}
			</div>
		</div>

		<div class="center">
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-11 col-lg-11">
					<div class="FormError" id="fbpscBasicFormError"></div>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-1 col-lg-1">
					<button class="btn btn-default pull-right" onclick="fbpsc.form('fbpscBasicForm', '{$sURI|escape:'htmlall':'UTF-8'}', null, 'BasicSettings', 'BasicSettings', false, false, null, 'Basic', 'loadingBasicsDiv');return false;"><i class="process-icon-save"></i>{l s='Save' mod='facebookpsconnect'}</button>
				</div>
			</div>
		</div>

	</form>


</div>
	<div class="clr_20"></div>
	<div class="adminErrors" id="fbpscBasicError"></div>

