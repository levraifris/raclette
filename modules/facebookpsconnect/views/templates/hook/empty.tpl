{*
*
* Social Login
*
* @author BusinessTech.fr
* @copyright Business Tech
*
*           ____    _______
*          |  _ \  |__   __|
*          | |_) |    | |
*          |  _ <     | |
*          | |_) |    | |
*          |____/     |_|
*
*}

{if !empty($bFancyClose)}
	<script type="text/javascript">
		$('#fancybox-loading').hide().remove();
		$('#fancybox-overlay').hide().remove();
		$('#fancybox-tmp').hide().remove();
		$('#fancybox-wrap').hide().remove();
	</script>
{/if}