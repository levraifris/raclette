{*
*
* Social Login
*
* @author BusinessTech.fr
* @copyright Business Tech
*
*           ____    _______
*          |  _ \  |__   __|
*          | |_) |    | |
*          |  _ <     | |
*          | |_) |    | |
*          |____/     |_|
*
*}

<div class="bootstrap">
	{if !empty($sHtmlCode)}
		{$sHtmlCode nofilter}
	{/if}
</div>