{*
*
* Social Login
*
* @author BusinessTech.fr
* @copyright Business Tech
*
*           ____    _______
*          |  _ \  |__   __|
*          | |_) |    | |
*          |  _ <     | |
*          | |_) |    | |
*          |____/     |_|
*
*}

{if !empty($bPS17)}
	<script type="text/javascript" src="{$PathJsJquery}"></script>
	<script type="text/javascript" src="{$PathJsModule}"></script>
{/if}


<script type="text/javascript">
	// instantiate object
	var {$sModuleName|escape:'htmlall':'UTF-8'} = {$sModuleName|escape:'htmlall':'UTF-8'} || new FpcModule('{$sModuleName|escape:'htmlall':'UTF-8'}');

	// get errors translation
	{if !empty($oJsTranslatedMsg)}
	{$sModuleName|escape:'htmlall':'UTF-8'}.msgs = {$oJsTranslatedMsg nofilter};
	{/if}

	{if isset($iCompare) && $iCompare == -1}{$sModuleName|escape:'htmlall':'UTF-8'}.oldVersion = true;{/if}

	// set URL of admin img
	{$sModuleName|escape:'htmlall':'UTF-8'}.sImgUrl = '{$smarty.const._FPC_URL_IMG|escape:'htmlall'}';

	// set URL of admin img
	{$sModuleName|escape:'htmlall':'UTF-8'}.sAdminImgUrl = '{$smarty.const._PS_ADMIN_IMG_|escape:'htmlall'}';

	// set URL of module's web service
	{if !empty($sModuleURI)}
	{$sModuleName|escape:'htmlall':'UTF-8'}.sWebService = '{$sModuleURI|escape:'htmlall':'UTF-8'}';
	{/if}


</script>
