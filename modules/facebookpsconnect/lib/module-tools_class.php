<?php
/**
 * Social Login
 *
 * @author    BusinessTech.fr - https://www.businesstech.fr
 * @copyright Business Tech 2020 - https://www.businesstech.fr
 * @license   Commercial
 *
 *           ____    _______
 *          |  _ \  |__   __|
 *          | |_) |    | |
 *          |  _ <     | |
 *          | |_) |    | |
 *          |____/     |_|
 */

class BT_FPCModuleTools
{
    /**
     * returns good translated errors
     */
    public static function translateJsMsg()
    {
        $GLOBALS['FBPSC_JS_MSG']['id'] = FacebookPsConnect::$oModule->l('You have not filled out the application ID',
            'module-tools_class');
        $GLOBALS['FBPSC_JS_MSG']['secret'] = FacebookPsConnect::$oModule->l('You have not filled out the application Secret',
            'module-tools_class');
        $GLOBALS['FBPSC_JS_MSG']['htmlElement'] = FacebookPsConnect::$oModule->l('You have not filled out the html element',
            'module-tools_class');
        $GLOBALS['FBPSC_JS_MSG']['positionName'] = FacebookPsConnect::$oModule->l('You have not filled out the name field',
            'module-tools_class');
        $GLOBALS['FBPSC_JS_MSG']['padding'] = FacebookPsConnect::$oModule->l('You have not filled out the padding element or this isn\'t an INTEGER',
            'module-tools_class');
        $GLOBALS['FBPSC_JS_MSG']['margin'] = FacebookPsConnect::$oModule->l('You have not filled out the margin element or this isn\'t an INTEGER',
            'module-tools_class');
        $GLOBALS['FBPSC_JS_MSG']['callback'] = FacebookPsConnect::$oModule->l('You have not filled out the application callback',
            'module-tools_class');
        $GLOBALS['FBPSC_JS_MSG']['scope'] = FacebookPsConnect::$oModule->l('You have not filled out the scope of App permissions',
            'module-tools_class');
        $GLOBALS['FBPSC_JS_MSG']['developerKey'] = FacebookPsConnect::$oModule->l('You have not filled out the developer Key',
            'module-tools_class');
        $GLOBALS['FBPSC_JS_MSG']['socialEmail'] = FacebookPsConnect::$oModule->l('You have not filled out your e-mail',
            'module-tools_class');
        $GLOBALS['FBPSC_JS_MSG']['delete'] = FacebookPsConnect::$oModule->l('Delete', 'module-tools_class');
        $GLOBALS['FBPSC_JS_MSG']['prefixCode'] = FacebookPsConnect::$oModule->l('You have to set the prefix code',
            'module-tools_class');
        $GLOBALS['FBPSC_JS_MSG']['voucherAmount'] = FacebookPsConnect::$oModule->l('You have to set the voucher amount',
            'module-tools_class');
        $GLOBALS['FBPSC_JS_MSG']['voucherPercent'] = FacebookPsConnect::$oModule->l('You have to set the voucher percent',
            'module-tools_class');
        $GLOBALS['FBPSC_JS_MSG']['apiType'] = FacebookPsConnect::$oModule->l('You have to select a connection method',
            'module-tools_class');
        $GLOBALS['FBPSC_JS_MSG']['defaultText'] = FacebookPsConnect::$oModule->l('You have to fill out the default text',
            'module-tools_class');
    }

    /**
     * update new keys in new module version
     */
    public static function updateConfiguration()
    {
        // check to update new module version
        foreach ($GLOBALS['FBPSC_CONFIGURATION'] as $sKey => $mVal) {
            // use case - not exists
            if (Configuration::get($sKey) === false) {
                // update key/ value
                Configuration::updateValue($sKey, $mVal);
            }
        }
    }

    /**
     * set all constant module in ps_configuration
     *
     * @param int $iShopId
     */
    public static function getConfiguration(array $aOptionListToUnserialize = null, $iShopId = null)
    {
        // get configuration options
        if (null !== $iShopId && is_numeric($iShopId)) {
            FacebookPsConnect::$conf = Configuration::getMultiple(array_keys($GLOBALS['FBPSC_CONFIGURATION']), null,
                null, $iShopId);
        } else {
            FacebookPsConnect::$conf = Configuration::getMultiple(array_keys($GLOBALS['FBPSC_CONFIGURATION']));
        }

        if (!empty($aOptionListToUnserialize)
            && is_array($aOptionListToUnserialize)
        ) {
            foreach ($aOptionListToUnserialize as $sOption) {
                if (!empty(FacebookPsConnect::$conf[strtoupper($sOption)])
                    && is_string(FacebookPsConnect::$conf[strtoupper($sOption)])
                    && !is_numeric(FacebookPsConnect::$conf[strtoupper($sOption)])
                ) {
                    FacebookPsConnect::$conf[strtoupper($sOption)] = unserialize(FacebookPsConnect::$conf[strtoupper($sOption)]);
                }
            }
        }
    }

    /**
     *  get the position name formatted for the the back-office display
     *
     * @param bool $sPositionName
     * @return string
     */
    public static function getPositionName($sPositionName)
    {
        foreach ($GLOBALS['FBPSC_DEFAULT_POSITION_TEXT'] as $sKey => $sHookName) {
            if (FacebookPsConnect::$sCurrentLang == 'en' || FacebookPsConnect::$sCurrentLang == 'es' || FacebookPsConnect::$sCurrentLang == 'it' || FacebookPsConnect::$sCurrentLang == 'fr') {
                $sLang = FacebookPsConnect::$sCurrentLang;
            } else {
                $sLang = 'en';
            }
            if (!empty($sPositionName)
                && $sKey == $sPositionName) {
                if ($sHookName[$sLang]) {
                    return $sHookName[$sLang];
                } else {
                    return $sHookName['en'];
                }
            }
        }
    }


    /**
     *  get the simple name for the advance position name
     *
     * @param string $sPositionName
     * @param string $sDelimiter
     * @param int $iReturn element to return
     * @return string
     */
    public static function getAdvancedPositionName($sPositionName, $sDelimiter, $iReturn)
    {
        $sSimpleName = explode($sDelimiter, $sPositionName);

        if (!empty($sSimpleName)
            && is_int($iReturn)
            && !empty($sSimpleName[$iReturn])) {

            return $sSimpleName[$iReturn];
        }
    }

    /**
     * get unserialized data by connector
     *
     * @param bool $bGetHook
     * @param bool $bIsWidgetSet
     * @return bool
     */
    public static function getConnectorData($bGetHook = false, $bIsConnectorSet = false)
    {
        // check one connector set minimum
        $bOneSet = false;

        // use case - check if some connectors are configured
        foreach ($GLOBALS['FBPSC_CONNECTORS'] as $sName => &$aVal) {
            // detect if connector is configured and stored
            if (false !== $aVal['data']) {
                // unserialize datas

                if (is_string($aVal['data'])) {
                    $aVal['data'] = unserialize($aVal['data']);
                }

                if (!empty($aVal['data']['activeConnector'])) {
                    $bOneSet = true;
                }
            }
        }
        if ($bIsConnectorSet) {
            return $bOneSet;
        }
    }

    /**
     * get unserialized data by hook
     */
    public static function getHookData()
    {
        if (!empty($GLOBALS['FBPSC_ZONE'])) {
            // use case - check if some connectors are configured
            foreach ($GLOBALS['FBPSC_ZONE'] as $sHookId => &$aVal) {
                // detect if connector is configured and stored
                if (!empty($aVal['data'])) {
                    if (false !== $aVal['data']) {
                        // unserialize data
                        $aVal['data'] = unserialize($aVal['data']);
                    }
                }
            }
        }
    }

    /**
     * unserialize data of connector or hook
     *
     * @param string
     * @return mixed
     */
    public static function unserializeData($sId, $sGlobalType = 'connector')
    {
        $sType = ($sGlobalType == 'connector') ? 'CONNECTORS' : 'ZONE';

        if (false !== $GLOBALS['FBPSC_' . $sType][$sId]['data']) {
            // unserialize data
            $GLOBALS['FBPSC_' . $sType][$sId]['data'] = unserialize($GLOBALS['FBPSC_' . $sType][$sId]['data']);
        }
    }

    /**
     * defines if the language is active
     *
     * @param mixed $mLang
     * @return bool
     */
    public static function isActiveLang($mLang)
    {
        if (is_numeric($mLang)) {
            $sField = 'id_lang';
        } else {
            $sField = 'iso_code';
            $mLang = strtolower($mLang);
        }

        $mResult = Db::getInstance()->getValue('SELECT count(*) FROM `' . _DB_PREFIX_ . 'lang` WHERE active = 1 AND `' . bqSQL($sField) . '` = "' . pSQL($mLang) . '"');

        return !empty($mResult) ? true : false;
    }

    /**
     * build the voucher sentence for a good display based on the configuration
     *
     * @param aVoucher $sIsoCode
     * @return string
     */
    public static function buildVoucherSentence($aVoucher)
    {
        //build the information for the voucher
        if (!empty($aVoucher['reduction_percent'])) {
            $sVoucherInfoText = $aVoucher['reduction_percent'] . ' %';
        }

        if (!empty($aVoucher['reduction_amount'])) {
            //get currency sign
            if (!empty($aVoucher['reduction_currency'])) {
                $oCurrency = new Currency((int)$aVoucher['reduction_currency']);

                if (is_object($oCurrency)) {
                    //get tax included / excluded tax
                    $sTaxText = empty($aVoucher['reduction_tax']) ? FacebookPsConnect::$oModule->l('Tax excluded',
                        'base-connector_class') : FacebookPsConnect::$oModule->l('Tax included',
                        'base-connector_class');

                    $$sVoucherInfoText = $aVoucher['reduction_amount'] . $oCurrency->sign . ' ' . $sTaxText;
                }
                unset($oCurrency);
            }
        }

        return $sVoucherInfoText;
    }


    /**
     * set good iso lang
     *
     * @return string
     */
    public static function getLangIso()
    {
        // get iso lang
        $sIsoLang = Language::getIsoById(FacebookPsConnect::$iCurrentLang);

        if (false === $sIsoLang) {
            $sIsoLang = 'en';
        }
        return $sIsoLang;
    }

    /**
     * return Lang id from iso code
     *
     * @param string $sIsoCode
     * @return int
     */
    public static function getLangId($sIsoCode, $iDefaultId = null)
    {
        // get iso lang
        $iLangId = Language::getIdByIso($sIsoCode);

        if (empty($iLangId) && $iDefaultId !== null) {
            $iLangId = $iDefaultId;
        }
        return $iLangId;
    }

    /**
     * returns current currency sign or id
     *
     * @param string $sField : field name has to be returned
     * @param string $iCurrencyId : currency id
     * @return mixed : string or array
     */
    public static function getCurrency($sField = null, $iCurrencyId = null)
    {
        // set
        $mCurrency = null;

        // get currency id
        if (null === $iCurrencyId) {
            $iCurrencyId = Configuration::get('PS_CURRENCY_DEFAULT');
        }

        $aCurrency = Currency::getCurrency($iCurrencyId);

        if ($sField !== null) {
            switch ($sField) {
                case 'id_currency' :
                    $mCurrency = $aCurrency['id_currency'];
                    break;
                case 'name' :
                    $mCurrency = $aCurrency['name'];
                    break;
                case 'iso_code' :
                    $mCurrency = $aCurrency['iso_code'];
                    break;
                case 'iso_code_num' :
                    $mCurrency = $aCurrency['iso_code_num'];
                    break;
                case 'sign' :
                    $mCurrency = $aCurrency['sign'];
                    break;
                case 'conversion_rate' :
                    $mCurrency = $aCurrency['conversion_rate'];
                    break;
                default:
                    $mCurrency = $aCurrency;
                    break;
            }
        }

        return $mCurrency;
    }

    /**
     * returns timestamp
     *
     * @param string $sDate
     * @param string $sType
     * @return mixed : bool or int
     */
    public static function getTimeStamp($sDate, $sType = 'en')
    {
        // set variable
        $iTimeStamp = false;

        // get date
        $aTmpDate = explode(' ', str_replace(array('-', '/', ':'), ' ', $sDate));

        if (count($aTmpDate) > 1) {
            if ($sType == 'en') {
                $iTimeStamp = mktime(0, 0, 0, $aTmpDate[0], $aTmpDate[1], $aTmpDate[2]);
            } elseif ($sType == 'db') {
                $iTimeStamp = mktime(0, 0, 0, $aTmpDate[1], $aTmpDate[2], $aTmpDate[0]);
            } else {
                $iTimeStamp = mktime(0, 0, 0, $aTmpDate[1], $aTmpDate[0], $aTmpDate[2]);
            }
        }
        // destruct
        unset($aTmpDate);

        return $iTimeStamp;
    }


    /**
     * returns a formatted date
     *
     * @param int $iTimestamp
     * @param mixed $mLocale
     * @return string
     */
    public static function formatTimestamp($iTimestamp, $sTemplate = null, $mLocale = false)
    {
        // set
        $sDate = '';

        if ($mLocale !== false) {
            if (null === $sTemplate) {
                $sTemplate = '%d %h. %Y';
            }
            // set date with locale format
            $sDate = strftime($sTemplate, $iTimestamp);
        } else {
            switch ($sTemplate) {
                case 'connect' :
                    $sDate = date('d', $iTimestamp)
                        . ' '
                        . (!empty($GLOBALS['FBPSC_MONTH'][FacebookPsConnect::$sCurrentLang]) ? $GLOBALS['FBPSC_MONTH'][FacebookPsConnect::$sCurrentLang]['long'][date('n',
                            $iTimestamp)] : date('M', $iTimestamp))
                        . ' '
                        . date('Y', $iTimestamp);
                    break;
                default:
                    // set date with matching month or with default language
                    $sDate = date('d', $iTimestamp)
                        . ' '
                        . (!empty($GLOBALS['FBPSC_MONTH'][FacebookPsConnect::$sCurrentLang]) ? $GLOBALS['FBPSC_MONTH'][FacebookPsConnect::$sCurrentLang]['short'][date('n',
                            $iTimestamp)] : date('M', $iTimestamp))
                        . '. '
                        . date('Y', $iTimestamp);
                    break;
            }
        }
        return $sDate;
    }


    /**
     * returns formatted URI for page name type
     *
     * @return mixed
     */
    public static function getPageName()
    {
        $sScriptName = '';

        Tools::getValue('controller');
        // use case - script name filled
        if (!empty($_SERVER['SCRIPT_NAME'])) {
            $sScriptName = $_SERVER['SCRIPT_NAME'];
        } // use case - php_self filled
        elseif ($_SERVER['PHP_SELF']) {
            $sScriptName = $_SERVER['PHP_SELF'];
        } // use case - default script name
        else {
            $sScriptName = 'index.php';
        }
        return substr(basename($sScriptName), 0, strpos(basename($sScriptName), '.'));
    }


    /**
     * returns account page link
     *
     * @return string link
     */
    public static function getAccountPageLink()
    {
        return Context::getContext()->link->getPageLink('my-account.php');
    }

    /**
     * returns order page link
     *
     * @return string link
     */
    public static function getOrderPageLink()
    {
        return Context::getContext()->link->getPageLink('order.php');
    }


    /**
     * return oco page configuration
     *
     * @return string link
     */
    public static function getOcoPageConfiguration()
    {
        return Context::getContext()->link->getModuleLink('pm_oneclickorder', 'configure', array(), true);
    }


    /**
     * returns template path
     *
     * @param string $sTemplate
     * @return string
     */
    public static function getTemplatePath($sTemplate)
    {
        // set
        $mTemplatePath = null;

        $mTemplatePath = FacebookPsConnect::$oModule->getTemplatePath($sTemplate);

        return $mTemplatePath;
    }

    /**
     * returns link object
     *
     * @return obj
     */
    public static function getLinkObj()
    {
        return Context::getContext()->link;
    }


    /**
     * returns product image
     *
     * @param obj $oProduct
     * @param string $sImageType
     * @return obj
     */
    public static function getProductImage(Product &$oProduct, $sImageType)
    {
        $sImgUrl = '';

        if (Validate::isLoadedObject($oProduct)) {
            // use case - get Image
            $aImage = Image::getCover($oProduct->id);

            if (!empty($aImage)) {
                // get image url
                $sImgUrl = self::getLinkObj()->getImageLink($oProduct->link_rewrite,
                    $oProduct->id . '-' . $aImage['id_image'], $sImageType);

                // use case - get valid IMG URI before  Prestashop 1.4
                $sImgUrl = self::detectHttpUri($sImgUrl);
            }
        }
        return $sImgUrl;
    }

    /**
     * detects and returns available URI - resolve Prestashop compatibility
     *
     * @param string $sURI
     * @param string $sForceDomain
     * @return mixed
     */
    public static function detectHttpUri($sURI, $sForceDomain = '')
    {
        // use case - only with relative URI
        if (!strstr($sURI, 'http')) {
            $sURI = 'http' . (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off' ? 's' : '') . '://' . ($sForceDomain != '' ? $sForceDomain : $_SERVER['HTTP_HOST']) . $sURI;
        }
        return $sURI;
    }

    /**
     * truncate current request_uri in order to delete params : sAction and sType
     *
     * @category tools collection
     * @param mixed: string or array $mNeedle
     * @return mixed
     */
    public static function truncateUri($mNeedle = '&sAction')
    {
        // set tmp
        $aQuery = is_array($mNeedle) ? $mNeedle : array($mNeedle);

        // get URI
        $sURI = $_SERVER['REQUEST_URI'];

        foreach ($aQuery as $sNeedle) {
            $sURI = strstr($sURI, $sNeedle) ? substr($sURI, 0, strpos($sURI, $sNeedle)) : $sURI;
        }
        return $sURI;
    }

    /**
     * detects available method and apply json encode
     *
     * @return string
     */
    public static function jsonEncode($aData)
    {
        if (method_exists('Tools', 'jsonEncode')) {
            $aData = Tools::jsonEncode($aData);
        } elseif (function_exists('json_encode')) {
            $aData = json_encode($aData);
        } else {
            if (is_null($aData)) {
                return 'null';
            }
            if ($aData === false) {
                return 'false';
            }
            if ($aData === true) {
                return 'true';
            }
            if (is_scalar($aData)) {
                $aData = addslashes($aData);
                $aData = str_replace("\n", '\n', $aData);
                $aData = str_replace("\r", '\r', $aData);
                $aData = preg_replace('{(</)(script)}i', "$1'+'$2", $aData);
                return "'$aData'";
            }
            $isList = true;
            for ($i = 0, reset($aData); $i < count($aData); $i++, next($aData)) {
                if (key($aData) !== $i) {
                    $isList = false;
                    break;
                }
            }
            $result = array();

            if ($isList) {
                foreach ($aData as $v) {
                    $result[] = self::json_encode($v);
                }
                $aData = '[ ' . join(', ', $result) . ' ]';
            } else {
                foreach ($aData as $k => $v) {
                    $result[] = self::json_encode($k) . ': ' . self::json_encode($v);
                }
                $aData = '{ ' . join(', ', $result) . ' }';
            }
        }

        return $aData;
    }

    /**
     * detects available method and apply json decode
     *
     * @return mixed
     */
    public static function jsonDecode($aData)
    {
        if (method_exists('Tools', 'jsonDecode')) {
            $aData = Tools::jsonDecode($aData);
        } elseif (function_exists('json_decode')) {
            $aData = json_decode($aData);
        }
        return $aData;
    }

    /**
     * check if specific module and module's vars are available
     *
     * @param int $sModuleName
     * @param array $aCheckedVars
     * @param bool $bObjReturn
     * @return mixed : true or false or obj
     */
    public static function isInstalled($sModuleName, array $aCheckedVars = array(), $bObjReturn = false)
    {
        $mReturn = false;

        // use case - check module is installed in DB
        if (Module::isInstalled($sModuleName)) {
            $oModule = Module::getInstanceByName($sModuleName);

            if (!empty($oModule)) {
                // check if module is activated
                $aActivated = Db::getInstance()->ExecuteS('SELECT id_module as id, active FROM ' . _DB_PREFIX_ . 'module WHERE name = "' . pSQL($sModuleName) . '" AND active = 1');

                if (!empty($aActivated[0]['active'])) {
                    $mReturn = true;

                    $aActivated = Db::getInstance()->ExecuteS('SELECT * FROM ' . _DB_PREFIX_ . 'module_shop WHERE id_module = ' . pSQL($aActivated[0]['id']) . ' AND id_shop = ' . Context::getContext()->shop->id);

                    if (empty($aActivated)) {
                        $mReturn = false;
                    }


                    if ($mReturn) {
                        if (!empty($aCheckedVars)) {
                            foreach ($aCheckedVars as $sVarName) {
                                $mVar = Configuration::get($sVarName);

                                if (empty($mVar)) {
                                    $mReturn = false;
                                }
                            }
                        }
                    }
                }
            }
            if ($mReturn && $bObjReturn) {
                $mReturn = $oModule;
            }
            unset($oModule);
        }
        return $mReturn;
    }

    /**
     * check if the product is a valid obj
     *
     * @param int $iProdId
     * @param int $iLangId
     * @param bool $bObjReturn
     * @param bool $bAllProperties
     * @return mixed : true or false
     */
    public static function isProductObj($iProdId, $iLangId, $bObjReturn = false, $bAllProperties = false)
    {
        // set
        $bReturn = false;

        $oProduct = new Product($iProdId, $bAllProperties, $iLangId);

        if (Validate::isLoadedObject($oProduct)) {
            $bReturn = true;
        }

        return !empty($bObjReturn) && $bReturn ? $oProduct : $bReturn;
    }

    /**
     * write breadcrumbs of product for category
     *
     * @category tools collection
     * @param int $iCatId
     * @param int $iCatId
     * @return string
     */
    public static function getProductPath($iCatId, $iLangId)
    {
        $oCategory = new Category($iCatId);

        return (Validate::isLoadedObject($oCategory) ? str_replace('>', ' &gt; ',
            strip_tags(self::getPath((int)($oCategory->id), (int)($iLangId)))) : '');
    }

    /**
     * write breadcrumbs of product for category
     *
     * Forced to redo the function from Tools here as it works with cookie
     * for language, not a passed parameter in the function
     *
     * @param int $iCatId
     * @param int $iCatId
     * @param string $sPath
     * @return string
     */
    public static function getPath($iCatId, $iLangId, $sPath = '')
    {
        $mReturn = '';

        if ($iCatId == 1) {
            $mReturn = $sPath;
        } else {
            // get pipe
            $sPipe = Configuration::get('PS_NAVIGATION_PIPE');

            if (empty($sPipe)) {
                $sPipe = '>';
            }

            $sFullPath = '';

            $aCurrentCategory = Db::getInstance()->getRow('
                SELECT id_category, level_depth, nleft, nright
                FROM ' . _DB_PREFIX_ . 'category
                WHERE id_category = ' . (int)$iCatId
            );

            if (isset($aCurrentCategory['id_category'])) {
                $sQuery = '
                    SELECT c.id_category, cl.name, cl.link_rewrite
                    FROM ' . _DB_PREFIX_ . 'category c';

                Shop::addSqlAssociation('category', 'c', false);

                $sQuery .= ' LEFT JOIN ' . _DB_PREFIX_ . 'category_lang cl ON (cl.id_category = c.id_category AND cl.`id_lang` = ' . (int)($iLangId) . (version_compare(_PS_VERSION_,
                        '1.5', '>') ? Shop::addSqlRestrictionOnLang('cl') : '') . ')';

                $sQuery .= '
                    WHERE c.nleft <= ' . (int)$aCurrentCategory['nleft'] . ' AND c.nright >= ' . (int)$aCurrentCategory['nright'] . ' AND cl.id_lang = ' . (int)($iLangId) . ' AND c.id_category != 1
                    ORDER BY c.level_depth ASC
                    LIMIT ' . (int)$aCurrentCategory['level_depth'];

                $aCategories = Db::getInstance()->ExecuteS($sQuery);

                $iCount = 1;
                $nCategories = count($aCategories);

                foreach ($aCategories as $aCategory) {
                    $sFullPath .=
                        htmlentities($aCategory['name'], ENT_NOQUOTES, 'UTF-8') .
                        (($iCount++ != $nCategories OR !empty($sPath)) ? '<span class="navigation-pipe">' . $sPipe . '</span>' : '');
                }
                $mReturn = $sFullPath . $sPath;
            }
        }
        return $mReturn;
    }


    /**
     * process categories to generate tree of them
     *
     * @param array $aCategories
     * @param array $aIndexedCat
     * @param array $aCurrentCat
     * @param int $iCurrentIndex
     * @param int $iDefaultId
     * @return array
     */
    public static function recursiveCategoryTree(
        array $aCategories,
        array $aIndexedCat,
        $aCurrentCat,
        $iCurrentIndex = 1,
        $iDefaultId = null
    ) {
        // set variables
        static $_aTmpCat;
        static $_aFormatCat;

        if ($iCurrentIndex == 1) {
            $_aTmpCat = null;
            $_aFormatCat = null;
        }

        if (!isset($_aTmpCat[$aCurrentCat['infos']['id_parent']])) {
            $_aTmpCat[$aCurrentCat['infos']['id_parent']] = 0;
        }
        $_aTmpCat[$aCurrentCat['infos']['id_parent']] += 1;

        // calculate new level
        $aCurrentCat['infos']['iNewLevel'] = $aCurrentCat['infos']['level_depth'] + (version_compare(_PS_VERSION_,
                '1.5.0') != -1 ? 0 : 1);

        // calculate type of gif to display - displays tree in good
        $aCurrentCat['infos']['sGifType'] = (count($aCategories[$aCurrentCat['infos']['id_parent']]) == $_aTmpCat[$aCurrentCat['infos']['id_parent']] ? 'f' : 'b');

        // calculate if checked
        if (in_array($iCurrentIndex, $aIndexedCat)) {
            $aCurrentCat['infos']['bCurrent'] = true;
        } else {
            $aCurrentCat['infos']['bCurrent'] = false;
        }

        // define classname with default cat id
        $aCurrentCat['infos']['mDefaultCat'] = ($iDefaultId === null) ? 'default' : $iCurrentIndex;

        $_aFormatCat[] = $aCurrentCat['infos'];

        if (isset($aCategories[$iCurrentIndex])) {
            foreach ($aCategories[$iCurrentIndex] as $iCatId => $aCat) {
                if ($iCatId != 'infos') {
                    self::recursiveCategoryTree($aCategories, $aIndexedCat, $aCategories[$iCurrentIndex][$iCatId],
                        $iCatId);
                }
            }
        }
        return $_aFormatCat;
    }

    /**
     * detect priority order to fill description : long description / short description / meta description
     *
     * @param array $aData
     * @return string
     */
    public static function manageProductDesc(array $aData)
    {
        // set
        $sDesc = '';

        if (!empty(FacebookPsConnect::$conf['FBPSC_SORT_DESC'])) {
            $aDescPosition = unserialize(FacebookPsConnect::$conf['FBPSC_SORT_DESC']);
        } else {
            $aDescPosition = array('meta', 'short', 'long');
        }

        foreach ($aDescPosition as $sOrder) {
            switch ($sOrder) {
                case 'meta' :
                    if (empty($sDesc) && !empty($aData['meta_description'])) {
                        $sDesc = $aData['meta_description'];
                    }
                    break;
                case 'short' :
                    if (empty($sDesc) && !empty($aData['description_short'])) {
                        $sDesc = $aData['description_short'];
                    }
                    break;
                case 'long' :
                    if (empty($sDesc) && !empty($aData['description'])) {
                        $sDesc = $aData['description'];
                    }
                    break;
                default:
                    break;
            }
        }
        $sDesc = strip_tags($sDesc);

        return $sDesc;
    }

    /**
     * detect if customer is logged and returns the customer ID
     *
     * @return int
     */
    public static function getCustomerId()
    {
        $iCustomerId = 0;

        $cookie = Context::getContext()->customer;

        // Check if user is logged
        if ($cookie->isLogged()) {
            if (!empty(FacebookPsConnect::$oCookie->id_customer)) {
                $iCustomerId = FacebookPsConnect::$oCookie->id_customer;
            }
        }

        return $iCustomerId;
    }

    /**
     * detect if cURL is installed or if file_get_contents is allowed with allow_url_fopen
     *
     * @param $sUrl
     * @param $aParams
     * @return int
     */
    public static function fileGetContent($sUrl, array $aParams = null)
    {
        $sContent = '';

        if ('curl' == FacebookPsConnect::$conf['FBPSC_API_REQUEST_METHOD']) {
            $rCurl = curl_init();

            if (empty($aParams)) {
                $aParams = array(
                    CURLOPT_URL => $sUrl,
                    CURLOPT_SSL_VERIFYPEER => ((strstr($sUrl, 'https://')) ? true : false),
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_HEADER => 0,
                    CURLOPT_VERBOSE => false,
                );
            }
            curl_setopt_array($rCurl, $aParams);

            $sContent = @curl_exec($rCurl);

            curl_close($rCurl);
        } elseif ('fopen' == FacebookPsConnect::$conf['FBPSC_API_REQUEST_METHOD']) {
            $sContent = @file_get_contents($sUrl);
        }

        return $sContent;
    }

    /**
     * do an HTTP request
     *
     * @param $sUrl
     * @param $aParams
     * @return int
     */
    public static function execHttpRequest($sUrl, array $aParams = null)
    {
        // set HTTP options
        $aOptions = array(
            'http' => array(),
        );

        // define the method to use
        $aOptions['http']['method'] = !empty($aParams['method']) ? $aParams['method'] : 'POST';

        // check all parameters
        if (!empty($aParams['header'])
            && is_string($aParams['header'])
        ) {
            $aOptions['http']['header'] = $aParams['header'];
        }
        if (!empty($aParams['user_agent'])
            && is_string($aParams['user_agent'])
        ) {
            $aOptions['http']['user_agent'] = $aParams['user_agent'];
        }
        if (!empty($aParams['content'])
            && is_array($aParams['content'])
        ) {
            $aOptions['http']['content'] = http_build_query($aParams['content']);
        }
        if (!empty($aParams['proxy'])
            && is_string($aParams['proxy'])
        ) {
            $aOptions['http']['proxy'] = $aParams['proxy'];
        }
        if (!empty($aParams['request_fulluri'])
            && is_bool($aParams['request_fulluri'])
        ) {
            $aOptions['http']['request_fulluri'] = $aParams['request_fulluri'];
        }
        if (!empty($aParams['follow_location'])
            && is_integer($aParams['follow_location'])
            && in_array($aParams['follow_location'], array(0, 1))
        ) {
            $aOptions['http']['follow_location'] = $aParams['follow_location'];
        }
        if (!empty($aParams['max_redirects'])
            && is_integer($aParams['max_redirects'])
        ) {
            $aOptions['http']['max_redirects'] = $aParams['max_redirects'];
        }
        if (!empty($aParams['protocol_version'])
            && is_float($aParams['protocol_version'])
        ) {
            $aOptions['http']['protocol_version'] = $aParams['protocol_version'];
        }
        if (!empty($aParams['timeout'])
            && is_float($aParams['timeout'])
        ) {
            $aOptions['http']['timeout'] = $aParams['timeout'];
        }
        if (!empty($aParams['ignore_errors'])
            && is_bool($aParams['ignore_errors'])
        ) {
            $aOptions['http']['ignore_errors'] = $aParams['ignore_errors'];
        }

        $cContext = stream_context_create($aOptions);

        return (
        file_get_contents($sUrl, false, $cContext)
        );
    }

    /**
     * returns the matching requested translations
     *
     * @param string $sSerializedVar
     * @param string $sGlobalVar
     * @return array
     */
    public static function getDefaultTranslations($sSerializedVar, $sGlobalVar)
    {
        $aTranslations = array();

        if (!empty(FacebookPsConnect::$conf[strtoupper($sSerializedVar)])) {
            $aTranslations = is_string(FacebookPsConnect::$conf[strtoupper($sSerializedVar)]) ? unserialize(FacebookPsConnect::$conf[strtoupper($sSerializedVar)]) : FacebookPsConnect::$conf[strtoupper($sSerializedVar)];
        } else {
            foreach ($GLOBALS['FBPSC_' . strtoupper($sGlobalVar)] as $sIsoCode => $sTranslation) {
                $iLangId = BT_FPCModuleTools::getLangId($sIsoCode);

                if ($iLangId) {
                    // get Id by iso
                    $aTranslations[$iLangId] = $sTranslation;
                }
            }
        }

        return $aTranslations;
    }

    /**
     * check if multi-shop is activated and if the group or global context is used
     *
     * @return bool
     */
    public static function checkGroupMultiShop()
    {
        return Configuration::get('PS_MULTISHOP_FEATURE_ACTIVE') && empty(FacebookPsConnect::$oCookie->shopContext);
    }


    /**
     * returns current page type
     */
    public static function detectCurrentPage()
    {
        $sCurrentTypePage = '';

        // use case - home page
        if (Tools::getValue('controller') == 'index') {
            $sCurrentTypePage = 'home';
        } // use case - search results page
        elseif (Tools::getValue('controller') == 'search' && empty(Context::getContext()->controller->module)) {
            $sCurrentTypePage = 'search';
        } // use case - order page
        elseif ((Tools::getValue('controller') == 'order'
                || Tools::getValue('controller') == 'orderopc')
            && Tools::getValue('step') == false
        ) {
            $sCurrentTypePage = 'cart';
        } // use case - category page
        elseif (Tools::getvalue('id_category')) {
            $sCurrentTypePage = 'category';
        } // use case - product page
        elseif (Tools::getvalue('id_product')) {
            $sCurrentTypePage = 'product';
        } elseif (Tools::getValue('controller') == 'manufacturer') {
            $sCurrentTypePage = 'manufacturer';
        } elseif (Tools::getValue('controller') == 'pricesdrop') {
            $sCurrentTypePage = 'promotion';
        } elseif (Tools::getValue('controller') == 'newproducts') {
            $sCurrentTypePage = 'newproducts';
        } elseif (Tools::getValue('controller') == 'bestsales') {
            $sCurrentTypePage = 'bestsales';
        } elseif (Tools::getValue('controller') == 'cart') {
            $sCurrentTypePage = 'cart';
        } elseif (Tools::getValue('controller') == 'contact') {
            $sCurrentTypePage = 'contactus';
        } elseif (Tools::getValue('controller') == 'stores') {
            $sCurrentTypePage = 'stores';
        } elseif (Tools::getValue('controller') == 'cms') {
            $sCurrentTypePage = 'cms';
        } elseif (Tools::getValue('controller') == 'authentication') {
            $sCurrentTypePage = 'authentication';
        } // other
        else {
            $sCurrentTypePage = 'other';
        }

        return $sCurrentTypePage;
    }

    /**
     * loop and manage CSS
     *
     * @param array $aData : params
     * @return array
     */
    public static function loopCssGenerate($aData)
    {
        require_once(_FPC_PATH_LIB_RENDER . 'render-connectors_class.php');
        require_once(_FPC_PATH_LIB_RENDER . 'iterator-render-connectors_class.php');

        $oRenderIterator = new IteratorRenderConnector($aData);
        $aCssExported = array();

        foreach ($oRenderIterator as $sKey => $aValue) {
            $sCssContent = RenderConnectors::extractCss($aValue, Tools::strtolower(_FPC_MODULE_NAME));
            if (!empty($sCssContent)) {
                $aCssExported[] = $sCssContent;
            }
        }

        RenderConnectors::writeCssFile($aCssExported, 'connectors', 'facebookpsconnect/views/css/');
    }


    /**
     * get the FAQ lang
     *
     * @param string $sLangIso
     */
    public static function getFaqLang($sLangIso)
    {
        $sLang = '';

        if ($sLangIso == 'en'|| $sLangIso == 'fr'){
            $sLang = $sLangIso;
        }
        else {
            $sLang = 'en';
        }

        return $sLang;
    }
}