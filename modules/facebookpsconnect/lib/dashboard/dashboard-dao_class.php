<?php
/**
 * Social Login
 *
 * @author    BusinessTech.fr - https://www.businesstech.fr
 * @copyright Business Tech 2020 - https://www.businesstech.fr
 * @license   Commercial
 *
 *           ____    _______
 *          |  _ \  |__   __|
 *          | |_) |    | |
 *          |  _ <     | |
 *          | |_) |    | |
 *          |____/     |_|
 */

class BT_FPCDashboardDao
{

    /**
     * search the data according to the fitler
     *
     * @param int $iCustomerId
     * @param string $sSocial
     * @param string $sDateAddFrom
     * @param string $sDateAddTo
     * @param string $sDateUpdFrom
     * @param string $sDateUpdTo
     * @return array
     */
    public static function search($sSocial, $sDateAddFrom, $sDateAddTo)
    {
        $sQuery = 'SELECT * FROM ' . _DB_PREFIX_ . bqSQL(strtolower(_FPC_MODULE_NAME)) . '_connect';

        // Use case for all or not all social network
        $sQuery .= ($sSocial != null) ? ' WHERE CNT_CUST_TYPE = "' . pSQL($sSocial) . '"' : ' WHERE (CNT_CUST_TYPE = "facebook" OR CNT_CUST_TYPE = "google" OR CNT_CUST_TYPE = "twitter" OR CNT_CUST_TYPE = "amazon" OR CNT_CUST_TYPE = "paypal")';

        // Start - Uses cases date search
        if (!empty($sDateAddFrom)) {
            $sQuery .= ' AND CTN_DATE_ADD >= "' . pSQL($sDateAddFrom) . '"';
        }

        if (!empty($sDateAddTo)) {
            $sQuery .= ' AND CTN_DATE_ADD <= "' . pSQL($sDateAddTo) . '"';
        }

        $sQuery .= 'GROUP BY CNT_CUST_TYPE';

        // End - Uses cases date search
        return Db::getInstance()->ExecuteS($sQuery);
    }


    /**
     * count the the number of records
     * @param string $sSocial
     * @param string $sDateAddFrom
     * @param string $sDateAddTo
     */
    public static function countSocialConnect($sSocial, $sDateAddFrom, $sDateAddTo)
    {
        $sQuery = 'SELECT CNT_CUST_TYPE, COUNT(*) as sValue FROM ' . _DB_PREFIX_ . bqSQL(strtolower(_FPC_MODULE_NAME)) . '_connect';

        // Use case for all or not all social network
        $sQuery .= ($sSocial != null) ? ' WHERE CNT_CUST_TYPE = "' . pSQL($sSocial) . '"' : ' WHERE (CNT_CUST_TYPE = "facebook" OR CNT_CUST_TYPE = "google" OR CNT_CUST_TYPE = "twitter" OR CNT_CUST_TYPE = "amazon" OR CNT_CUST_TYPE = "paypal")';

        // Start - Uses cases date search
        if (!empty($sDateAddFrom)) {
            $sQuery .= 'AND CTN_DATE_ADD > "' . pSQL($sDateAddFrom) . '"';
        }

        if (!empty($sDateAddTo)) {
            $sQuery .= 'AND CTN_DATE_ADD < "' . pSQL($sDateAddTo) . '"';
        }

        $sQuery .= 'GROUP BY CNT_CUST_TYPE';

        return Db::getInstance()->ExecuteS($sQuery);
    }


    /**
     * get the customers ids
     * @param string $sSocial
     * @param string $sDateAddFrom
     * @param string $sDateAddTo
     * @return array
     */
    public static function getCustomerIds($sSocial, $sDateAddFrom, $sDateAddTo)
    {
        $sQuery = 'SELECT DISTINCT CNT_CUST_ID, CNT_CUST_TYPE FROM ' . _DB_PREFIX_ . bqSQL(strtolower(_FPC_MODULE_NAME)) . '_connect';

        // Use case for all or not all social network
        $sQuery .= ($sSocial != null) ? ' WHERE CNT_CUST_TYPE = "' . pSQL($sSocial) . '"' : ' WHERE (CNT_CUST_TYPE = "facebook" OR CNT_CUST_TYPE = "google" OR CNT_CUST_TYPE = "twitter" OR CNT_CUST_TYPE = "amazon" OR CNT_CUST_TYPE = "paypal")';

        // Start - Uses cases date search
        if (!empty($sDateAddFrom)) {
            $sQuery .= ' AND CTN_DATE_ADD >= "' . pSQL($sDateAddFrom) . '"';
        }

        if (!empty($sDateAddTo)) {
            $sQuery .= ' AND CTN_DATE_ADD <= "' . pSQL($sDateAddTo) . '"';
        }

        // End - Uses cases date search
        return Db::getInstance()->ExecuteS($sQuery);
    }

    /**
     * get the customers ids
     * @param int $iCustomerId
     * @return array
     */

    public static function getCustomerTotalSpent()
    {
        $sQuery = 'SELECT CNT_CUST_TYPE , SUM(total_paid_real / conversion_rate) as sValue FROM ' . _DB_PREFIX_ . 'orders o';
        $sQuery .= ' LEFT JOIN ' . _DB_PREFIX_ . bqSQL(strtolower(_FPC_MODULE_NAME)) . '_connect c ON (c.`CNT_CUST_ID` = o.`id_customer`)';
        $sQuery .= ' AND o.valid = 1';
        $sQuery .= ' GROUP BY CNT_CUST_TYPE';

        return Db::getInstance()->ExecuteS($sQuery);
    }
}
