<?php
/**
 * Social Login
 *
 * @author    BusinessTech.fr - https://www.businesstech.fr
 * @copyright Business Tech 2020 - https://www.businesstech.fr
 * @license   Commercial
 *
 *           ____    _______
 *          |  _ \  |__   __|
 *          | |_) |    | |
 *          |  _ <     | |
 *          | |_) |    | |
 *          |____/     |_|
 */

require_once(_FPC_PATH_LIB_DASHBOARD . 'dashboard-dao_class.php');

class BT_FPCDashboard
{
    /**
     * search the information for the dashboard according to the filter
     * @param string $sSocial
     * @param string $sDateAddFrom
     * @param string $sDateAddTo
     */
    public static function searchData($sSocial, $sDateAddFrom = null, $sDateAddTo = null)
    {
        return BT_FPCDashboardDao::search($sSocial, $sDateAddFrom, $sDateAddTo);
    }

    /**
     * count the number of connection for each social networks
     * @param string $sSocial
     * @param string $sDateAddFrom
     * @param string $sDateAddTo
     */
    public static function countSocialConnect($sSocial, $sDateAddFrom = null, $sDateAddTo = null)
    {
        return BT_FPCDashboardDao::countSocialConnect($sSocial, $sDateAddFrom, $sDateAddTo);
    }

    /**
     * count the number of connection for each social networks
     * @param string $sSocial
     * @param string $sDateAddFrom
     * @param string $sDateAddTo
     */
    public static function getCustomerSpent($sSocial, $sDateAddFrom, $sDateAddTo)
    {

        $aAmountPerCustomer = BT_FPCDashboardDao::getCustomerTotalSpent();
        $aFinalAmountPerCustomer = array();

        foreach ($aAmountPerCustomer as $mAmountPerCustomer) {
            //Redefine the array with only values in association with a connector
            // Very important for the customer account without link of the module
            if (!empty($mAmountPerCustomer['CNT_CUST_TYPE'])) {
                $mAmountPerCustomer['sValue'] = Tools::displayPrice((float)$mAmountPerCustomer['sValue'], 1);
                array_push($aFinalAmountPerCustomer, $mAmountPerCustomer);
            }

        }
        unset($aAmountPerCustomer);

        return $aFinalAmountPerCustomer;
    }
}
