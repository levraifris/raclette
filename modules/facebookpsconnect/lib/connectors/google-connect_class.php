<?php
/**
 * Social Login
 *
 * @author    BusinessTech.fr - https://www.businesstech.fr
 * @copyright Business Tech 2020 - https://www.businesstech.fr
 * @license   Commercial
 *
 *           ____    _______
 *          |  _ \  |__   __|
 *          | |_) |    | |
 *          |  _ <     | |
 *          | |_) |    | |
 *          |____/     |_|
 */


if (!defined('_PS_VERSION_')) {
    exit(1);
}


/**
 * declare Google Exception class
 */
class BT_GoogleException extends BT_ConnectorException
{
}

class BT_GoogleConnect extends BT_BaseConnector
{
    /**
     * get Google client obj
     *
     * @param obj $oGoogleClient
     */
    protected $oGoogleClient = null;

    /**
     * get Google oauth obj
     *
     * @param obj $oGoogleOAuth
     */
    protected $oGoogleOAuth = null;

    /**
     * get Google Auth URL
     *
     * @param string $sAuthUrl
     */
    protected $sAuthUrl;

    /**
     * magic method assign connector keys
     *
     * @param array $aParams
     */
    public function __construct(array $aParams)
    {

        // include abstract connector
        require_once(_FPC_PATH_LIB_CONNECTOR . 'google/vendor/autoload.php');

        if (!empty($aParams['id'])
            && !empty($aParams['secret'])
            && !empty($aParams['developerKey'])
            && !empty($aParams['callback'])
        ) {
            $this->consumer_id = $aParams['id'];
            $this->consumer_secret = $aParams['secret'];
            $this->developer_key = $aParams['developerKey'];
            $this->oauth_callback = $aParams['callback'];

            // set user
            parent::setUser();
        } else {
            throw new BT_GoogleException(FacebookPsConnect::$oModule->l('Invalid connector keys',
                'google-connect_class'), 540);
        }
    }

    /**
     * magic method assign connector keys
     *
     * @param string $sName
     * @param mixed $mValue
     */
    public function __set($sName, $mValue)
    {
        switch ($sName) {
            case 'consumer_id':
            case 'sConsumerId' :
                $this->consumer_id = is_string($mValue) ? $mValue : null;
                break;
            case 'consumer_secret':
            case 'sConsumerSecret' :
                $this->consumer_secret = is_string($mValue) ? $mValue : null;
                break;
            case 'developer_key':
            case 'sDeveloperKey' :
                $this->developer_key = is_string($mValue) ? $mValue : null;
                break;
            case 'oauth_callback':
            case 'sOauthCallback' :
                $this->oauth_callback = is_string($mValue) ? $mValue : null;
                break;
            default:
                throw new BT_GoogleException(FacebookPsConnect::$oModule->l('Invalid set keys ',
                        'google-connect_class') . $sName, 541);
                break;
        }
    }

    /**
     * magic method return connector keys
     *
     * @param string $sName
     */
    public function __get($sName)
    {
        switch ($sName) {
            case 'consumer_key' :
            case 'sConsumerKey' :
                return $this->consumer_key;
                break;
            case 'consumer_secret' :
            case 'sConsumerSecret' :
                return $this->consumer_secret;
                break;
            case 'developer_key' :
            case 'sDeveloperKey' :
                return $this->developer_key;
                break;
            case 'oauth_callback' :
            case 'sOauthCallback' :
                return $this->oauth_callback;
                break;
            default:
                break;
        }
        return null;
    }

    /**
     * method check if code is valid or not, and either redirect on Google interface or log the customer by creating his account if necessary
     *
     * @param array $aParams
     * @return string
     */
    public function connect(array $aParams = null)
    {
        // Instantiate Google objects
        $this->getGoogleObj();

        if (empty($aParams['code'])) {
            // Redirect to Google Interface
            $this->redirect();
        } else {
            $this->oGoogleClient->authenticate($aParams['code']);
            $aToken = $this->oGoogleClient->getAccessToken();

            // Test is access token is defined
            if (!empty($aToken['access_token'])) {
                self::$oSession->set('code', $aToken['access_token']);
                $this->oGoogleClient->setAccessToken(self::$oSession->get('code'));

                // set create status
                $bCreateStatus = true;
                $bCreatePs = false;
                $bCreateSocial = false;

                // get user info
                $aGoogleAccount = $this->oGoogleOAuth->userinfo->get();

                // set data
                $this->oUser->id = $aGoogleAccount['id'];
                $this->oUser->email = $aGoogleAccount['email'];
                $this->oUser->customerId = 0;

                // Verify that both the first and last name were returned by checking if there is a space in the string
                if (strstr($aGoogleAccount['name'], ' ')) {
                    $aName = explode(' ', $aGoogleAccount['name']);
                } else {
                    // If there isn't, use the provided name input as both the first and last name
                    $aName = array();
                    $aName[0] = $aGoogleAccount['name'];
                    $aName[1] = $aGoogleAccount['name'];
                }

                // set name
                $this->oUser->first_name = $aName[0];
                $this->oUser->last_name = $aName[1];

                // set birthday
                if (!empty($aGoogleAccount['birthday'])) {
                    // format date for PS customer table
                    $this->oUser->birthday = $aGoogleAccount['birthday'];
                }

                // test if user already exist in social table
                $bCreateSocial = !parent::existSocialAccount($this->oUser->id);

                // test if user already exist in PS table
                $bCreatePs = !parent::existPsAccount($this->oUser->email);

                // use case - social account exist
                if (!$bCreateSocial) {
                    // use case - create new PS account and have to delete old social account
                    if (!empty($bCreatePs)) {
                        parent::deleteSocialAccount($this->oUser->id);
                        $bCreateSocial = true;
                    }
                } // social account do not exist and PS account exists
                elseif (empty($bCreatePs)) {
                    $this->oUser->customerId = Customer::customerExists($this->oUser->email, true);
                }

                // use case - if one of 2 accounts has to be created at least
                if (!empty($bCreatePs) || !empty($bCreateSocial)) {
                    // create customer in 1 or 2 tables
                    $bCreateStatus = parent::createCustomer($bCreatePs, $bCreateSocial, $this->sConnectorName);
                }

                // use case  - create status valid
                if ($bCreateStatus) {
                    // load customer
                    parent::loadCustomer($this->oUser->id);

                    return $this->login();
                } else {
                    throw new BT_GoogleException(FacebookPsConnect::$oModule->l('Internal server error. Account creation processing is unavailable',
                        'google-connector_class'), 542);
                }

            } else {
                throw new BT_GoogleException(FacebookPsConnect::$oModule->l('Unable to get access token',
                    'google-connect_class'), 544);
            }
        }
    }

    /**
     * method sign the oauth token and call connect()
     *
     * @param array $aParams
     * @return string
     */
    public function callback(array $aParams = null)
    {
        // instantiate Google objects
        $this->getGoogleObj();

        // Check if received token is correct
        if (empty($aParams['code'])) {
            throw new BT_GoogleException(FacebookPsConnect::$oModule->l('Can\'t get valid token code from Google!',
                'google-connect_class'), 544);
        }

        return $this->connect(array('code' => $aParams['code']));
    }

    /**
     * method redirect on google interface
     */
    protected function redirect()
    {
        $this->setAuthUrl();
        if (empty($this->sAuthUrl)) {
            throw new BT_GoogleException(FacebookPsConnect::$oModule->l('Authentication URL is not valid!',
                'google-connect_class'), 545);
        }
        header('Location:' . $this->sAuthUrl);
        exit(0);
    }


    /**
     * method instantiates Google classes : Client & OAuth Objects
     */
    protected function getGoogleObj()
    {
        // Instantiates Google Client Object
        $this->oGoogleClient = new Google_Client();

        $this->oGoogleClient->setApplicationName('Facebook Connect');
        $this->oGoogleClient->setClientId($this->consumer_id);
        $this->oGoogleClient->setClientSecret($this->consumer_secret);
        $this->oGoogleClient->setRedirectUri($this->oauth_callback);
        $this->oGoogleClient->setDeveloperKey($this->developer_key);
        $this->oGoogleClient->setIncludeGrantedScopes(true);
        $this->oGoogleClient->addScope(['email', 'profile', 'openid']);

        // Instantiates Google OAuth Object
        $this->oGoogleOAuth = new Google_Service_Oauth2($this->oGoogleClient);
    }


    /**
     * method create the url needed by the client to authenticate
     */
    protected function setAuthUrl()
    {
        $this->sAuthUrl = $this->oGoogleClient->createAuthUrl();
    }
}
