<?php
/**
 * Social Login
 *
 * @author    BusinessTech.fr - https://www.businesstech.fr
 * @copyright Business Tech 2020 - https://www.businesstech.fr
 * @license   Commercial
 *
 *           ____    _______
 *          |  _ \  |__   __|
 *          | |_) |    | |
 *          |  _ <     | |
 *          | |_) |    | |
 *          |____/     |_|
 */

class BT_FPCHookDisplay implements BT_IFpcHook
{
    /**
     * @var bool $bProcessHookAndConnector : detect if hook and connectors have been processed
     */
    static protected $bProcessHookAndConnector = null;

    /**
     * @var array $aBtnPosition : define all the positions we can have on all pages
     */
    static protected $aBtnPosition = array();

    /**
     * @var bool $bConnectorsActive : detect if one connector is active at least
     */
    static protected $bConnectorsActive = null;

    /**
     * @var string $sCurrentURI : get current URI
     */
    static protected $sCurrentURI = '';

    /**
     * @var string $sModuleURI : get Module web service URI
     */
    protected $sModuleURI = null;

    /**
     * @var string $sHookType : define hook type
     */
    protected $sHookType = null;

    /**
     * @var int $iCustomerLogged : get customer ID if is logged
     */
    protected $iCustomerLogged = null;

    /**
     * Magic Method __construct assigns few information about hook
     *
     * @param string
     */
    public function __construct($sHookType)
    {
        // set hook type
        $this->sHookType = $sHookType;

        $this->iCustomerLogged = BT_FPCModuleTools::getCustomerId();
    }

    /**
     * execute hook
     *
     * @param array $aParams
     * @return array
     */
    public function run(array $aParams = array())
    {
        require_once(_FPC_PATH_LIB . 'module-dao_class.php');
        require_once(_FPC_PATH_LIB_RENDER . 'facade-render-connectors_class.php');

        // set variables
        $aDisplayHook = array();

        // set module URI
        $this->sModuleURI = _FPC_MODULE_URL . 'ws-' . _FPC_MODULE_SET_NAME . '.php';

        // process hooks and connectors
        $this->processConnectorAndHook();

        // get current URL
        if (empty(self::$sCurrentURI)) {
            self::$sCurrentURI = urlencode($this->getCurrentUrl());
        }

        switch ($this->sHookType) {
            case 'header' :
                // use case - display in header
                $aDisplayHook = call_user_func(array($this, 'displayHeader'), array($aParams));
                break;
            case 'account' :
                // use case - display in account
                $aDisplayHook = call_user_func(array($this, 'displayAccount'));
                break;
            case 'top' :
                // use case - display connect buttons in top
                $aDisplayHook = call_user_func_array(array($this, 'displayTop'), array($aParams));
                break;
            case 'left' :
            case 'right' :
                // use case - display block connect in  left or right column
                $aDisplayHook = call_user_func_array(array($this, 'displayBlock'), array($aParams));
                break;
            case 'footer' :
                // use case - display connect buttons in footer
                $aDisplayHook = call_user_func_array(array($this, 'displayFooter'), array($aParams));
                break;
            case 'checkout17' :
                // use case - display connect buttons in footer
                $aDisplayHook = call_user_func_array(array($this, 'displayCheckout17'), array($aParams));
                break;
            case 'shortCode' :
                // use case - display connect buttons in footer
                $aDisplayHook = call_user_func_array(array($this, 'displayShortCode'), array($aParams));
                break;
            default :
                break;
        }

        // use case - generic assign
        if (!empty($aDisplayHook['assign'])) {
            if (!empty(FacebookPsConnect::$conf['FBPSC_API_REQUEST_METHOD'])) {
                $aDisplayHook['assign'] = array_merge($aDisplayHook['assign'], $this->_assign());
            } else {
                $aDisplayHook['assign'] = array('bDisplay' => false);
            }
        }

        return $aDisplayHook;
    }

    /**
     * assigns transverse data
     *
     * @return array
     */
    private function _assign()
    {
        // set smarty variables
        return array(
            'sModuleURI' => $this->sModuleURI,
            'iCurrentLang' => intval(FacebookPsConnect::$iCurrentLang),
            'sCurrentLang' => FacebookPsConnect::$sCurrentLang,
            'bCustomerLogged' => $this->iCustomerLogged,
            'bHookDisplay' => true,
            'bEnableVouchers' => FacebookPsConnect::$conf['FBPSC_ENABLE_VOUCHER'],
            'bEnableCustomVoucherText' => FacebookPsConnect::$conf['FBPSC_ENABLE_CUSTOM_TXT'],
            'aVoucherCustomText' => BT_FPCModuleTools::getDefaultTranslations('FBPSC_VOUCHER_DISP_TEXT',
                'DEFAULT_DISPLAY_TEXT'),
            'aLangs' => Language::getLanguages(),
            'sCurrentLang' => FacebookPsConnect::$oCookie->id_lang,
            'bDevMode' => _PS_MODE_DEV_,
            'bVersion17' => FacebookPsConnect::$bCompare17,
            'bVersion16' => FacebookPsConnect::$bCompare16,
            'bVersion15' => FacebookPsConnect::$bCompare15,
            'sButtonStyle' => FacebookPsConnect::$conf['FBPSC_BUTTON_STYLE'],
            'bDisplayCustomText' => FacebookPsConnect::$conf['FBPSC_DISPLAY_CUSTOM_TEXT'],
        );

    }

    /**
     * unserialize connector and hook content
     *
     * @return array
     */
    private function processConnectorAndHook()
    {
        if (self::$bProcessHookAndConnector === null) {
            // unserialize connectors and hooks data
            BT_FPCModuleTools::getHookData();
            self::$bConnectorsActive = BT_FPCModuleTools::getConnectorData(false, true);

            foreach ($GLOBALS['FBPSC_CONNECTORS'] as $sName => &$aConnector) {
                $aConnector['tpl'] = BT_FPCModuleTools::getTemplatePath(_FPC_PATH_TPL_NAME . _FPC_TPL_HOOK_PATH . $aConnector['tpl']);
            }
            self::$bProcessHookAndConnector = true;
        }
    }

    /**
     * returns current URL
     *
     * @return string
     */
    private function getCurrentUrl()
    {
        $sLink = '';

        $bOco = Tools::getValue('oco');

        // check if OPC deactivated and get redirect on authentication page
        if (Tools::getValue('controller') == 'authentication'
            && Tools::getIsset('back')
        ) {
            // Use case for Connect + OCO module
            $sLink = !empty($bOco) ? BT_FPCModuleTools::getOcoPageConfiguration() : BT_FPCModuleTools::getAccountPageLink();
        } // check if OPC URL
        elseif (
            Tools::getValue('controller') == 'orderopc'
            || Tools::getValue('controller') == 'order'
        ) {
            $sLink = $_SERVER['REQUEST_URI'];
        } // my account page
        else {
            $sLink = BT_FPCModuleTools::getAccountPageLink();
        }

        return $sLink;
    }

    /**
     * add to header JS and CSS
     *
     * @return array
     */
    private function displayHeader($aParams = array())
    {
        // set
        $aAssign = array();

        require_once(_FPC_PATH_LIB . 'module-dao_class.php');
        require_once(_FPC_PATH_LIB_RENDER . 'render-connectors_class.php');

        //Get the button information
        self::$aBtnPosition = BT_FPCModuleDao::getConnectorPositions();

        // set js msg translation
        BT_FPCModuleTools::translateJsMsg();

        $aAssign['oJsTranslatedMsg'] = BT_FPCModuleTools::jsonEncode($GLOBALS['FBPSC_JS_MSG']);

        Context::getContext()->controller->addCSS(_FPC_URL_CSS . 'hook.css');
        Context::getContext()->controller->addCSS(_FPC_URL_CSS . 'bootstrap-social.css');
        Context::getContext()->controller->addCSS(_FPC_URL_CSS . 'font-awesome.css');
        Context::getContext()->controller->addCSS(_FPC_URL_CSS . 'jquery.fancybox-1.3.4.css');
        Context::getContext()->controller->addJS(_FPC_URL_JS . 'module.js');
        Context::getContext()->controller->addJS(_FPC_URL_JS . 'jquery/jquery.fancybox-1.3.4.js');

        // Use case to add CSS file for connectors
        RenderConnectors::getCssFile('connectors', _FPC_URL_CSS);

        $aAssign['bAddJsCss'] = false;

        // USE case - To handle the remove of the JS defer function
        $aAssign['bPS17'] = FacebookPsConnect::$bCompare17;

        if (!empty(FacebookPsConnect::$bCompare17)) {
            $aAssign['PathJsJquery'] = _FPC_URL_JS . 'jquery-1.11.0.min.js';
            $aAssign['PathJsModule'] = _FPC_URL_JS . 'module.js';
        }

        return array('tpl' => _FPC_TPL_HOOK_PATH . _FPC_TPL_HEADER, 'assign' => $aAssign);
    }

    /**
     * displays connector buttons
     *
     * @param array $aParams
     * @return array
     */
    private function displayTop(array $aParams)
    {
        $aAssign = array();
        $aAssign['sHtmlCode'] = FacadeRenderConnectors::loopConnector(self::$aBtnPosition, 'render', 'displayTop', null,
            self::$sCurrentURI, $this->sModuleURI, $this->iCustomerLogged);

        return array('tpl' => _FPC_TPL_HOOK_PATH . _FPC_TPL_CONNECTOR_BUTTONS_JS, 'assign' => $aAssign);
    }


    /**
     * displays connector buttons
     *
     * @param array $aParams
     * @return array
     */
    private function displayCheckout17(array $aParams)
    {
        $aAssign = array();
        $aAssign['sHtmlCode'] = '';

        if (BT_FPCModuleTools::detectCurrentPage() == 'cart'
            && (isset(Context::getContext()->controller->page_name) && Context::getContext()->controller->page_name == 'checkout')
            || (isset(Context::getContext()->controller->php_self) && Context::getContext()->controller->php_self == 'cart')) {
            $aAssign['sHtmlCode'] = FacadeRenderConnectors::loopConnector(self::$aBtnPosition, 'render', 'orderFunnel',
                null, self::$sCurrentURI, $this->sModuleURI, $this->iCustomerLogged);
        }

        return array('tpl' => _FPC_TPL_HOOK_PATH . _FPC_TPL_CONNECTOR_BUTTONS_JS, 'assign' => $aAssign);
    }

    /**
     * displays connector buttons for shortcode
     *
     * @param array $aParams
     * @return array
     */
    private function displayShortCode(array $aParams)
    {
        $aAssign = array();
        $aAssign['sHtmlCode'] = '';

        $sShortCodeName = Tools::getValue('sShortCodeName');

        //Get only the shortcode values for better performances
        $aData = BT_FPCModuleDao::getConnectorPositions('shortcode');


        if (!empty($aData)) {
            $aAssign['sHtmlCode'] = FacadeRenderConnectors::loopConnector($aData, 'render', $sShortCodeName, null,
                self::$sCurrentURI, $this->sModuleURI, $this->iCustomerLogged, false);
        }

        return array('tpl' => _FPC_TPL_HOOK_PATH . _FPC_TPL_CONNECTOR_BUTTONS_JS, 'assign' => $aAssign);
    }


    /**
     * display connectors block
     *
     * @param array $aParams
     * @return array
     */
    private function displayBlock(array $aParams)
    {
        $aAssign = array();
        $aAssign['sHtmlCode'] = FacadeRenderConnectors::loopConnector(self::$aBtnPosition, 'render',
            'displayLeftColumn', null, self::$sCurrentURI, $this->sModuleURI, $this->iCustomerLogged);


        // To manage on template per prestashop version : this is better for the template reading
        if (empty(FacebookPsConnect::$bCompare16)) {
            $sTemplateVersion = _FPC_TPL_ACCOUNT_BLOCK_15;
        } elseif (empty(FacebookPsConnect::$bCompare17)) {
            $sTemplateVersion = _FPC_TPL_ACCOUNT_BLOCK_16;
        } else {
            $sTemplateVersion = _FPC_TPL_ACCOUNT_BLOCK_17;
        }

        return array('tpl' => _FPC_TPL_HOOK_PATH . $sTemplateVersion, 'assign' => $aAssign);
    }


    /**
     * displays buttons in footer
     *
     * @param array $aParams
     * @return array
     */
    private function displayFooter(array $aParams)
    {
        $aAssign = array();

        $aAssign['sHtmlCode'] = FacadeRenderConnectors::loopConnector(self::$aBtnPosition, 'render', 'displayFooter',
            null, self::$sCurrentURI, $this->sModuleURI, $this->iCustomerLogged);

        $aAssign['sHtmlCode'] .= FacadeRenderConnectors::loopConnector(self::$aBtnPosition, 'render', null, 'advanced',
            self::$sCurrentURI, $this->sModuleURI, $this->iCustomerLogged);

        return array('tpl' => _FPC_TPL_HOOK_PATH . _FPC_TPL_CONNECTOR_BUTTONS_JS, 'assign' => $aAssign);
    }

    /**
     * displays fancybox if customer do not use a social connector to link his PS account
     *
     * @param array $aParams
     * @return array
     */
    private function displayAccount()
    {
        require_once(_FPC_PATH_LIB_VOUCHER . 'voucher-dao_class.php');

        $aVoucher = BT_FpcVoucherDao::getvoucherAssocInfo((int)$this->iCustomerLogged);

        $sToday = time();
        $sDateTo = strtotime($aVoucher['date_to']);

        $aAssign = array(
            'iCustomerId' => $this->iCustomerLogged,
            'bUseJqueryUI' => true,
            'sDisplayVoucher' => FacebookPsConnect::$conf['FBPSC_VOUCHER_DISPLAY_MODE'],
            'sVoucherDateTo' => $aVoucher['date_to'],
            'sVoucherCode' => $aVoucher['code'],
            'iVoucherQuantity' => $aVoucher['quantity'],
            'bVoucherAvailable' => ($sDateTo > $sToday) ? true : false,
            'bActive' => $aVoucher['active'],
        );


        $aAssign['bDisplay'] = false;

        // if one of connectors is active at least
        if (self::$bConnectorsActive) {
            require_once(_FPC_PATH_LIB . 'module-dao_class.php');

            // include abstract connector
            require_once(_FPC_PATH_LIB_CONNECTOR . 'base-connector_class.php');

            // get connector options
            $aParams = $GLOBALS['FBPSC_CONNECTORS']['twitter']['data'];

            // test if twitter is already configured
            if (!empty($aParams)) {
                // get connector
                $oConnector = BT_BaseConnector::get('twitter', $aParams);

                // check if customer is already logged from FB connector
                if ($oConnector->existSocialAccount($aAssign['iCustomerId'], 'ps')
                    && strstr(FacebookPsConnect::$oCookie->email, 'twitter.com')
                ) {
                    $aAssign['iCustomerId'] = md5('FBPSCtwitter' . $aAssign['iCustomerId']);
                    $aAssign['sConnector'] = 'twitter';
                    $aAssign['bTwitterCustomerExist'] = true;
                    $aAssign['bDisplay'] = true;
                }
                unset($oConnector);
            }
        }

        $aAssign['sModuleURI'] = _FPC_MODULE_URL . 'ws-' . _FPC_MODULE_SET_NAME . '.php';

        return array('tpl' => _FPC_TPL_HOOK_PATH . _FPC_TPL_CONNECTOR_ACCOUNT, 'assign' => $aAssign);
    }
}
