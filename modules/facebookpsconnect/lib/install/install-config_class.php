<?php
/**
 * Social Login
 *
 * @author    BusinessTech.fr - https://www.businesstech.fr
 * @copyright Business Tech 2020 - https://www.businesstech.fr
 * @license   Commercial
 *
 *           ____    _______
 *          |  _ \  |__   __|
 *          | |_) |    | |
 *          |  _ <     | |
 *          | |_) |    | |
 *          |____/     |_|
 */


class BT_InstallConfig implements BT_IInstall
{
    /**
     * install of module
     *
     * @param mixed $mParam
     * @return bool $bReturn : true => validate install, false => invalidate install
     */
    public static function install($mParam = null)
    {
        // declare return
        $bReturn = true;

        // log jam to debug appli
        if (defined('_FPC_LOG_JAM_CONFIG') && _FPC_LOG_JAM_CONFIG) {
            $bReturn = _FPC_LOG_JAM_CONFIG;
        } else {
            if (empty($mParam['bHookOnly'])) {
                // update each constant used in module admin & display
                foreach ($GLOBALS['FBPSC_CONFIGURATION'] as $sKeyName => $mVal) {
                    if (!Configuration::updateValue($sKeyName, $mVal)) {
                        $bReturn = false;
                    }
                }
            }
            if (empty($mParam['bConfigOnly'])) {
                // register each hooks
                foreach ($GLOBALS['FBPSC_HOOKS'] as $aHook) {
                    if (!self::isHookInstalled($aHook['name'], FacebookPsConnect::$oModule->id)) {
                        if (!FacebookPsConnect::$oModule->registerHook($aHook['name'])) {
                            $bReturn = false;
                        }
                    }
                }
            }
        }
        unset($mParam);

        return $bReturn;
    }

    /**
     * uninstall of module
     *
     * @param mixed $mParam
     * @return bool $bReturn : true => validate uninstall, false => invalidate uninstall / uninstall admin tab
     */
    public static function uninstall($mParam = null)
    {
        // set return execution
        $bReturn = true;

        // log jam to debug appli
        if (defined('_FPC_LOG_JAM_CONFIG') && _FPC_LOG_JAM_CONFIG) {
            $bReturn = _FPC_LOG_JAM_CONFIG;
        } else {
            // delete global config
            foreach ($GLOBALS['FBPSC_CONFIGURATION'] as $sKeyName => $mVal) {
                if (!Configuration::deleteByName($sKeyName)) {
                    $bReturn = false;
                }
            }
            // delete related connectors config
            foreach ($GLOBALS['FBPSC_CONNECTORS'] as $sKeyName => $mVal) {
                // delete widget configuration
                if (!Configuration::deleteByName('FBPSC_' . strtoupper($sKeyName))) {
                    $bReturn = false;
                }
            }
            // delete related zone config
            foreach ($GLOBALS['FBPSC_ZONE'] as $sKeyName => $mVal) {
                // delete hooks configuration
                if (!Configuration::deleteByName('FBPSC_' . strtoupper($sKeyName))) {
                    $bReturn = false;
                }
            }
        }
        unset($mParam);

        return $bReturn;
    }

    /**
     * check if specific module is hooked to a specific hook
     *
     * @param string $sHookName
     * @param int $iModuleId
     * @return int
     */
    public static function isHookInstalled($sHookName, $iModuleId)
    {
        return FacebookPsConnect::$oModule->isRegisteredInHook($sHookName);
    }
}
