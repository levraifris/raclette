<?php
/**
 * Social Login
 *
 * @author    BusinessTech.fr - https://www.businesstech.fr
 * @copyright Business Tech 2020 - https://www.businesstech.fr
 * @license   Commercial
 *
 *           ____    _______
 *          |  _ \  |__   __|
 *          | |_) |    | |
 *          |  _ <     | |
 *          | |_) |    | |
 *          |____/     |_|
 */

class BT_FpcVoucher
{
    /**
     * @var bool $bProcess : define if process or not
     */
    protected $bProcess = null;

    /**
     * generate voucher
     *
     * @param string $sConnectorName
     * @param int $iCustomerId
     */
    public static function generateVoucher($sConnectorName, $iCustomerId)
    {
        require_once(_FPC_PATH_LIB . 'module-dao_class.php');
        require_once(_FPC_PATH_LIB_VOUCHER . 'voucher-dao_class.php');

        // check if the option is activated
        if (!empty(FacebookPsConnect::$conf['FBPSC_ENABLE_VOUCHER'])) {
            $oVoucher = new CartRule;

            // get the good discount name for the current lang
            $oVoucher->name = BT_FPCModuleTools::getDefaultTranslations('FBPSC_VOUCHER_DESC', '_DEFAULT_VOUCHER_DESC');

            // get the catagories selected in the module configuration
            $aSelectCategory = BT_FPCModuleDao::getFpcCategories(FacebookPsConnect::$iShopId);

            //build the voucher type amount or percentage
            if (FacebookPsConnect::$conf['FBPSC_VOUCHER_TYPE'] == 'percentage') {
                if (!empty(FacebookPsConnect::$conf['FBPSC_VOUCHER_PERCENT'])) {
                    $oVoucher->reduction_percent = (float)FacebookPsConnect::$conf['FBPSC_VOUCHER_PERCENT'];
                }

            } elseif (FacebookPsConnect::$conf['FBPSC_VOUCHER_TYPE'] == 'amount') {
                if (!empty(FacebookPsConnect::$conf['FBPSC_VOUCHER_AMOUNT'])) {
                    $oVoucher->reduction_amount = (float)FacebookPsConnect::$conf['FBPSC_VOUCHER_AMOUNT'];
                    $oVoucher->reduction_tax = FacebookPsConnect::$conf['FBPSC_VOUCHER_TAX'];
                    $oVoucher->reduction_currency = FacebookPsConnect::$conf['FBPSC_VOUCHER_CURRENCY'];
                }
            }

            // build the voucher code
            $oVoucher->code = FacebookPsConnect::$conf['FBPSC_VOUCHER_CODE_PREFIX'] . '-' . $sConnectorName . '-' . $iCustomerId . rand(0,
                    999);

            // associate the id customer to the voucher
            $oVoucher->id_customer = (int)$iCustomerId;

            // set the maximum voucher quantity per user
            $oVoucher->quantity_per_user = (int)FacebookPsConnect::$conf['FBPSC_VOUCHER_MAX_QTY'];

            // set the general quantity
            $oVoucher->quantity = 1;

            // handle the cumulable
            if(empty(FacebookPsConnect::$bCompare17)) {
                $oVoucher->cart_cumulable = (int)FacebookPsConnect::$conf['FBPSC_CUMULATE_OTHERS_VOUCHER'];
            }

            // handle the cart rule restriction
            $oVoucher->cart_rule_restriction = (int)FacebookPsConnect::$conf['FBPSC_CUMULATE_DISCOUNT_VOUCHER'];

            // handle the product restriction
            if (!empty($aSelectCategory)) {
                $oVoucher->product_restriction = 1;
            }

            // set the partial use
            $oVoucher->partial_use = 0;

            // handle the minimum amount purchase
            $oVoucher->minimum_amount = FacebookPsConnect::$conf['FBPSC_VOUCHER_MIN_AMOUNT'];
            //$oVoucher->minimum_amount_currency = ;

            // activate the voucher
            $oVoucher->active = 1;

            // handle the highlight
            $oVoucher->highlight = FacebookPsConnect::$conf['FBPSC_HIGHTLIGHT_VOUCHER'];

            // handle validity date
            $oVoucher->date_from = date('Y-m-d H:i:s', $_SERVER['REQUEST_TIME']);
            $oVoucher->date_to = date('Y-m-d H:i:s', mktime(date('H'), date('i'), date('s'), date('m'),
                date('d') + intval(FacebookPsConnect::$conf['FBPSC_VOUCHER_VALIDITY']), date('Y')));

            // set transaction
            Db::getInstance()->Execute('BEGIN');

            // use case - adding succeed
            $bInsert = $oVoucher->add(true, false);

            if ($bInsert) {
                Db::getInstance()->Execute('COMMIT');
            } // failure
            else {
                Db::getInstance()->Execute('ROLLBACK');
            }

            if (!empty($aSelectCategory)) {
                BT_FpcVoucherDao::addProductRule($oVoucher->id, 1, 'categories', $aSelectCategory);
            }

            if (!empty($oVoucher->id) && !empty($iCustomerId)) {
                BT_FpcVoucherDao::addVoucherAssoc($oVoucher->id, $iCustomerId);
            }
        }
    }

    /**
     * start transaction
     *
     * @return bool
     */
    public function beginTransaction()
    {
        if (!$this->isInnoDb()) {
            return true;
        }

        return (bool)Db::getInstance()->Execute('BEGIN');
    }

    /**
     * rollbacks query
     *
     * @return bool
     */
    public function rollBack()
    {
        if (!$this->isInnoDb()) {
            return true;
        }

        return (bool)Db::getInstance()->Execute('ROLLBACK');
    }

    /**
     * commits query
     *
     * @return bool
     */
    public function commit()
    {
        if (!$this->isInnoDb()) {
            return true;
        }

        return (bool)Db::getInstance()->Execute('COMMIT');
    }
}
