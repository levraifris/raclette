<?php
/**
 * Social Login
 *
 * @author    BusinessTech.fr - https://www.businesstech.fr
 * @copyright Business Tech 2020 - https://www.businesstech.fr
 * @license   Commercial
 *
 *           ____    _______
 *          |  _ \  |__   __|
 *          | |_) |    | |
 *          |  _ <     | |
 *          | |_) |    | |
 *          |____/     |_|
 */
class BT_FPCModuleDao
{
    /**
     * check if customer account already exists
     *
     * @param int $iCustomerId
     * @param string $sEmail
     * @return bool
     */
    public static function updateCustomerEmail($iCustomerId, $sEmail)
    {
        $sQuery = 'UPDATE ' . _DB_PREFIX_ . 'customer SET email = "' . pSQL($sEmail) . '"'
            . ' WHERE `active` = 1 AND `id_customer` = ' . (int)$iCustomerId
            . ' AND `deleted` = 0  AND `is_guest` =  0';

        // execute
        return Db::getInstance()->Execute($sQuery);
    }

    /**
     * update the customer name
     *
     * @param int $iCustomerId
     * @param string $sName
     * @return bool
     */
    public static function updateCustomerName($iCustomerId, $sName)
    {
        $sQuery = 'UPDATE ' . _DB_PREFIX_ . 'customer SET lastname = "' . pSQL($sName) . '"'
            . ' WHERE `active` = 1 AND `id_customer` = ' . (int)$iCustomerId
            . ' AND `deleted` = 0 AND `is_guest` =  0';

        // execute
        return Db::getInstance()->Execute($sQuery);
    }

    /**
     * update the customer
     *
     * @param int $iCustomerId
     * @param string $sFirstName
     * @return bool
     */
    public static function updateCustomerFirstName($iCustomerId, $sFirstName)
    {
        $sQuery = 'UPDATE ' . _DB_PREFIX_ . 'customer SET firstname = "' . pSQL($sFirstName) . '"'
            . ' WHERE `active` = 1 AND `id_customer` = ' . (int)$iCustomerId
            . ' AND `deleted` = 0  AND `is_guest` =  0';

        // execute
        return Db::getInstance()->Execute($sQuery);
    }

    /**
     * update the customer
     *
     * @param int $iCustomerId
     * @param string $sFirstName
     * @return bool
     */
    public static function updateCustomerPassword($iCustomerId, $sPassword)
    {
        $sPassword = Tools::encrypt($sPassword);
        $sQuery = 'UPDATE ' . _DB_PREFIX_ . 'customer SET passwd = "' . pSQL($sPassword) . '"'
            . ' WHERE `active` = 1 AND `id_customer` = ' . (int)$iCustomerId
            . ' AND `deleted` = 0 AND `is_guest` =  0';

        // execute
        return Db::getInstance()->Execute($sQuery);
    }


    /**
     * add customer FB account association to deactivate the reminder
     *
     * @param int $iShopId
     * @param int $iCustId
     * @return bool
     */
    public static function addCustomerAssociationStatus($iShopId, $iCustId)
    {
        $sQuery = 'INSERT INTO ' . _DB_PREFIX_ . strtolower(_FPC_MODULE_NAME) . '_customer_assoc (CCA_SHOP_ID, CCA_CUST_ID) '
            . 'VALUES("' . pSQL($iShopId) . '","' . pSQL($iCustId) . '")';

        return Db::getInstance()->Execute($sQuery);
    }

    /**
     * add a callback for review
     *
     * @param int $iShopId
     * @param int $iCustId
     * @return bool
     */
    public static function existCustomerAssociationStatus($iShopId, $iCustId)
    {
        $sQuery = 'SELECT COUNT(*) as nb FROM  ' . _DB_PREFIX_ . strtolower(_FPC_MODULE_NAME) . '_customer_assoc  '
            . 'WHERE CCA_CUST_ID = ' . (int)$iCustId . ' AND CCA_SHOP_ID = ' . (int)$iShopId . ' ';

        $aNb = Db::getInstance()->ExecuteS($sQuery);

        return !empty($aNb[0]['nb']) ? true : false;
    }

    /**
     * collect data about customer via social network
     *
     * @param int $iCustId
     * @param int $sSocialId
     * @param int $iShopId
     * @param string $sSocialType
     * @param string $sAction
     * @param string $sType
     * @param int $iObjId
     * @return bool
     */
    public static function collectSocialData($iCustId, $sSocialId, $iShopId, $sSocialType, $sAction, $sType, $iObjId)
    {
        $sQuery = 'INSERT INTO ' . _DB_PREFIX_ . strtolower(_FPC_MODULE_NAME) . '_collect (COL_CUST_ID, COL_SOCIAL_ID, COL_SHOP_ID, COL_SOCIAL_TYPE, COL_ACTION, COL_TYPE, COL_OBJ_ID, COL_DATE_ADD) '
            . 'VALUES(' . (int)$iCustId . ', ' . (int)$sSocialId . ', ' . (int)$iShopId . ', "' . pSQL($sSocialType) . '", "' . pSQL($sAction) . '", "' . pSQL($sType) . '", ' . (int)$iObjId . ', now())';

        return Db::getInstance()->Execute($sQuery);
    }

    /**
     * detect if the object is already registered
     *
     * @param int $iShopId
     * @param int $iCustId
     * @param string $sSocialId
     * @param string $sSocialType
     * @param string $sAction
     * @param string $sType
     * @param int $iObjId
     * @return bool
     */
    public static function existSocialData($iShopId, $iCustId, $sSocialId, $sSocialType, $sAction, $sType, $iObjId)
    {
        $sQuery = 'SELECT count(*) as nb FROM ' . _DB_PREFIX_ . strtolower(_FPC_MODULE_NAME) . '_collect '
            . ' WHERE (COL_CUST_ID = ' . (int)$iCustId . ' OR COL_SOCIAL_ID = "' . pSQL($sSocialId) . '" )'
            . ' AND COL_SHOP_ID = ' . (int)$iShopId
            . ' AND COL_SOCIAL_TYPE = "' . pSQL($sSocialType) . '"'
            . ' AND COL_ACTION = "' . pSQL($sAction) . '"'
            . ' AND COL_TYPE = "' . pSQL($sType) . '"'
            . ' AND COL_OBJ_ID = ' . (int)$iObjId;

        $aNb = Db::getInstance()->ExecuteS($sQuery);

        return !empty($aNb[0]['nb']) ? true : false;
    }

    /**
     * returns categories to export
     *
     * @param int $iShopId
     * @return array
     */
    public static function getFpcCategories($iShopId)
    {
        // set
        $aCategories = array();

        // get categories
        $aResult = Db::getInstance()->ExecuteS('SELECT * FROM `' . _DB_PREFIX_ . strtolower(_FPC_MODULE_NAME) . '_categories`' . (!empty(FacebookPsConnect::$bCompare15) ? ' WHERE `id_shop` = ' . (int)$iShopId : ''));

        if (!empty($aResult)) {
            foreach ($aResult as $aCat) {
                $aCategories[] = $aCat['id_category'];
            }
        }

        return $aCategories;
    }

    /**
     * delete the previous selected categories
     *
     * @param int $iShopId
     * @return bool
     */
    public static function deleteCategories($iShopId)
    {
        return Db::getInstance()->Execute('DELETE FROM `' . _DB_PREFIX_ . strtolower(_FPC_MODULE_NAME) . '_categories`' . (!empty(FacebookPsConnect::$bCompare15) ? ' WHERE `id_shop` = ' . (int)$iShopId : ''));
    }

    /**
     * insert a category in our table gmc_categories
     *
     * @param int $iCategoryId
     * @param int $iShopId
     * @return bool
     */
    public static function insertCategory($iCategoryId, $iShopId)
    {
        return Db::getInstance()->Execute('INSERT INTO `' . _DB_PREFIX_ . strtolower(_FPC_MODULE_NAME) . '_categories` (`id_category`, `id_shop`) values (' . (int)$iCategoryId . ', ' . (int)$iShopId . ')');
    }

    /**
     * update the login date in the table for the dashboard
     *
     * @param int $iCustomerId
     * @return bool
     */
    public static function updateCustomerLoginDate($iCustomerId)
    {
        $iDate = time();

        $sQuery = 'UPDATE ' . _DB_PREFIX_ . 'fbpsc_connect SET CTN_DATE_UPD = FROM_UNIXTIME(' . (int)$iDate . ')'
            . ' WHERE`CNT_CUST_ID` = ' . (int)$iCustomerId
            . ' AND CNT_SHOP_ID = ' . (int)FacebookPsConnect::$iShopId;

        // execute
        return Db::getInstance()->Execute($sQuery);
    }

    /**
     * add the connector position
     *
     * @param string $sPositionName
     * @param string $sPostionType
     * @param int $iStatus
     * @param int $iShopId
     * @param array $aData
     * @return bool
     */
    public static function addConnectorPosition($sPositionName, $sPostionType, $iStatus, $iShopId, $aData)
    {
        $sQuery = 'INSERT INTO `' . _DB_PREFIX_ . strtolower(_FPC_MODULE_NAME) . '_connector_position` (`name`, `type`, `status`, `id_shop`, `data`)'
            . ' values ("' . pSQL($sPositionName) . '", "' . pSQL($sPostionType) . '", ' . (int)$iStatus . ', ' . (int)$iShopId . ',  "' . pSQL(serialize($aData)) . '")';

        return Db::getInstance()->Execute($sQuery);
    }

    /**
     * update the position data from the form
     *
     * @param string $sName
     * @param string $sType
     * @param int $iStatus
     * @param int $sShopId
     * @param string $sConnectors
     * @param array $aData
     * @param int $iPositionId
     * @return bool
     */
    public static function updateConnectorPosition($sName, $sType, $iStatus, $sShopId, $aData, $iPositionId)
    {
        $sQuery = 'UPDATE `' . _DB_PREFIX_ . strtolower(_FPC_MODULE_NAME) . '_connector_position`'
            . ' SET `status` = ' . (int)$iStatus . ','
            . ' `name` = "' . pSQL($sName) . '",'
            . ' `type` = "' . pSQL($sType) . '",'
            . ' `status` = ' . (int)$iStatus . ','
            . ' `id_shop` = ' . (int)$sShopId . ','
            . ' `data` =  "' . pSQL(serialize($aData)) . '"'
            . ' WHERE `id` = ' . (int)$iPositionId;

        return Db::getInstance()->Execute($sQuery);
    }

    /**
     * get the connector position
     *
     * @return bool
     */
    public static function getConnectorPositionData()
    {
        $sQuery = 'SELECT * FROM `' . _DB_PREFIX_ . strtolower(_FPC_MODULE_NAME) . '_connector_position`';

        return Db::getInstance()->ExecuteS($sQuery);
    }

    /**
     * get the connector position
     *
     * @param string $sName
     * @param string $sType
     * @return bool
     */
    public static function getConnectorShortCodeData($sPositionName, $sType)
    {
        $sQuery = 'SELECT * FROM `' . _DB_PREFIX_ . strtolower(_FPC_MODULE_NAME) . '_connector_position`'
            . ' WHERE `type` = "' . pSQL($sType) . '" AND `name` = "' . pSQL($sPositionName) . '"';

        return Db::getInstance()->getRow($sQuery);
    }

    /**
     * get the connector position
     *
     ** @return bool
     */
    public static function getConnectorShortCode()
    {
        $sQuery = 'SELECT * FROM `' . _DB_PREFIX_ . strtolower(_FPC_MODULE_NAME) . '_connector_position`'
            . ' WHERE `type` = "shortcode"';

        return Db::getInstance()->ExecuteS($sQuery);
    }

    /**
     * get the connector position for a specific ID
     *
     * @param string $iPositionId
     * @return bool
     */
    public static function getConnectorPositionDataById($iPositionId)
    {
        $sQuery = 'SELECT * FROM `' . _DB_PREFIX_ . strtolower(_FPC_MODULE_NAME) . '_connector_position`'
            . ' WHERE `id` = ' . (int)$iPositionId;

        return Db::getInstance()->getRow($sQuery);
    }

    /**
     * get the connector position for a specific position Type
     *
     * @param string $sPositionType
     * @param array $aPositionName
     * @return array
     */
    public static function getConnectorPositions(
        $sPositionType = '',
        $aOldClassicalHookInclude = array(),
        $aOldClassicalHookExclude = array()
    ) {
        $sQuery = 'SELECT * FROM `' . _DB_PREFIX_ . strtolower(_FPC_MODULE_NAME) . '_connector_position`';

        if (!empty($sPositionType)) {
            $sQuery .= ' WHERE `type` = "' . pSQL($sPositionType) . '"';
        }

        // Use case to include the old classical hook became advanced
        if (is_array($aOldClassicalHookInclude)
            && $aOldClassicalHookInclude !== null
        ) {
            foreach ($aOldClassicalHookInclude as $sKey => $sValue) {
                $sQuery .= ' OR name= "' . pSQL($sValue) . '"';
            }
        }

        // Use case to exclude the old classical hook became advanced when we export the
        if (is_array($aOldClassicalHookExclude)
            && $aOldClassicalHookExclude !== null
            && $sPositionType == 'advanced'
        ) {
            foreach ($aOldClassicalHookExclude as $sKey => $sValue) {
                $sQuery .= ' AND name <> "' . pSQL($sValue) . '"';
            }
        }

        $aPositions = Db::getInstance()->ExecuteS($sQuery);

        $aFormattedPositions = array();

        if (!empty($aPositions)) {
            foreach ($aPositions as $aPosition) {
                $aFormattedPositions[$aPosition['name']] = $aPosition;
                $aFormattedPositions[$aPosition['name']]['data'] = unserialize($aPosition['data']);
            }
        }

        return $aFormattedPositions;
    }

    /**
     * get the connector position for a specific position Type
     *
     * @param string $sPositionName
     * @return bool
     */
    public static function getConnectorByPosition($sPositionName)
    {
        $sQuery = 'SELECT * FROM `' . _DB_PREFIX_ . strtolower(_FPC_MODULE_NAME) . '_connector_position`'
            . ' WHERE `name` = "' . pSQL($sPositionName) . '"';

        return Db::getInstance()->getRow($sQuery);
    }


    /**
     * get the connectors
     *
     * @return bool
     */
    public static function getConnectors()
    {
        $sQuery = 'SELECT * FROM `' . _DB_PREFIX_ . strtolower(_FPC_MODULE_NAME) . '_connector_position`';

        return Db::getInstance()->ExecuteS($sQuery);
    }


    /**
     *  Update the data for a hook/position
     *
     * @param string $sData
     * @param int $iPositionId
     * @return bool
     */
    public static function updateHookData($sData, $iPositionId)
    {
        $sQuery = 'UPDATE `' . _DB_PREFIX_ . strtolower(_FPC_MODULE_NAME) . '_connector_position`'
            . ' SET `data` = "' . pSQL(serialize($sData)) . '"'
            . ' WHERE `id` = ' . (int)$iPositionId;

        return Db::getInstance()->Execute($sQuery);
    }

    /**
     * get the connector position
     *
     * @param int $iPositionId
     * @return bool
     */
    public static function deleteConnectorPosition($iPositionId)
    {
        $sQuery = 'DELETE FROM `' . _DB_PREFIX_ . strtolower(_FPC_MODULE_NAME) . '_connector_position` where id=' . (int)$iPositionId;

        return Db::getInstance()->Execute($sQuery);
    }

    /**
     * update connector status
     *
     * @param int $iPositionId
     * @param int $iStatus
     * @return bool
     */
    public static function updateConnectorStatus($iPositionId, $iStatus)
    {
        $sQuery = 'UPDATE `' . _DB_PREFIX_ . strtolower(_FPC_MODULE_NAME) . '_connector_position`'
            . ' SET `status` = ' . (int)$iStatus
            . ' WHERE `id` = ' . (int)$iPositionId;

        return Db::getInstance()->Execute($sQuery);
    }
}
