<?php
/**
 * Social Login
 *
 * @author    BusinessTech.fr - https://www.businesstech.fr
 * @copyright Business Tech 2020 - https://www.businesstech.fr
 * @license   Commercial
 *
 *           ____    _______
 *          |  _ \  |__   __|
 *          | |_) |    | |
 *          |  _ <     | |
 *          | |_) |    | |
 *          |____/     |_|
 */


class BT_AdminUpdate implements BT_IAdmin
{
    /**
     * update all tabs content of admin page
     *
     * @param array $aParam
     * @return array
     */
    public function run(array $aParam = null)
    {
        require_once(_FPC_PATH_LIB . 'module-dao_class.php');
        require_once(_FPC_PATH_LIB_RENDER . 'iterator-render-connectors_class.php');

        // set variables
        $aDisplayInfo = array();

        // get type
        $aParam['sType'] = empty($aParam['sType']) ? '' : $aParam['sType'];

        switch ($aParam['sType']) {
            case 'basic'        : // use case - update snippets settings
            case 'connector'    : // use case - update connector settings
            case 'hook'         : // use case - update hook
            case 'hookForm'         : // use case - update hook
            case 'hookAdvanced'         : // use case - update general hook form
            case 'connectorPositionUpdate' : // use case - update advance position hook form
            case 'connectPosition' : // use case - update advance position hook
            case 'voucher'      : // use case - update voucher
            case 'dashboard'      : // use case - update dashboard
            case 'shortCode'      : // use case - update shortcode
            case 'shortCodeList'      : // use case - update shortcode
            case 'cssCode'      : // use case - update cssCode
                // execute match function
                $aDisplayInfo = call_user_func_array(array($this, 'update' . ucfirst($aParam['sType'])),
                    array($aParam));
                break;
            default :
                break;
        }

        return $aDisplayInfo;
    }

    /**
     * update basic settings
     *
     * @param array $aPost
     * @return array
     */
    private function updateBasic(array $aPost)
    {
        // clean headers
        @ob_end_clean();

        // set
        $aUpdate = array();

        try {
            // use case - set the default connection text
            $this->updateLang($aPost, 'fbpscDefaultConnectText', 'FBPSC_CONNECT_TEXT');

            // use case - check display block
            $bDisplayBlock = (Tools::getIsset(strtolower(_FPC_MODULE_NAME) . 'DisplayBlock') && Tools::getValue(strtolower(_FPC_MODULE_NAME) . 'DisplayBlock') == 'true') ? true : false;

            if (!Configuration::updateValue('FBPSC_DISPLAY_BLOCK', $bDisplayBlock)) {
                throw new Exception(FacebookPsConnect::$oModule->l('An error occurred during block display update',
                        'admin-update_class') . '.', 111);
            }

            // use case - check display block
            $bDisplayCustomText= (Tools::getIsset(strtolower(_FPC_MODULE_NAME) . 'DisplayCustomText') && Tools::getValue(strtolower(_FPC_MODULE_NAME) . 'DisplayCustomText') == 'true') ? true : false;

            if (!Configuration::updateValue('FBPSC_DISPLAY_CUSTOM_TEXT', $bDisplayCustomText)) {
                throw new Exception(FacebookPsConnect::$oModule->l('An error occurred during block display update',
                        'admin-update_class') . '.', 111);
            }

            // use case - check display account information block
            $bDisplayBlockInfoAccount = (Tools::getIsset(strtolower(_FPC_MODULE_NAME) . 'DisplayBlockInfoAccount') && Tools::getValue(strtolower(_FPC_MODULE_NAME) . 'DisplayBlockInfoAccount') == 'true') ? true : false;

            if (!Configuration::updateValue('FBPSC_DISPLAY_BLOCK_INFO_ACCOUNT', $bDisplayBlockInfoAccount)) {
                throw new Exception(FacebookPsConnect::$oModule->l('An error occurred during block display update',
                        'admin-update_class') . '.', 112);
            }

            // use case - check display cart information block
            $bDisplayBlockInfoCart = (Tools::getIsset(strtolower(_FPC_MODULE_NAME) . 'DisplayBlockInfoCart') && Tools::getValue(strtolower(_FPC_MODULE_NAME) . 'DisplayBlockInfoCart') == 'true') ? true : false;

            if (!Configuration::updateValue('FBPSC_DISPLAY_BLOCK_INFO_CART', $bDisplayBlockInfoCart)) {
                throw new Exception(FacebookPsConnect::$oModule->l('An error occurred during block display update',
                        'admin-update_class') . '.', 113);
            }

            // use case - set  default customer group
            if (Tools::getIsset(strtolower(_FPC_MODULE_NAME) . 'DefaultGroup')) {
                $iDefaultCustGroup = Tools::getValue(strtolower(_FPC_MODULE_NAME) . 'DefaultGroup');

                if (is_numeric($iDefaultCustGroup)) {
                    if (!Configuration::updateValue('FBPSC_DEFAULT_CUSTOMER_GROUP', $iDefaultCustGroup)) {
                        throw new Exception(FacebookPsConnect::$oModule->l('An error occurred during default customer group update',
                                'admin-update_class') . '.', 114);
                    }
                } else {
                    throw new Exception(FacebookPsConnect::$oModule->l('Default customer group is not a numeric',
                            'admin-update_class') . '.', 115);
                }
            }
            // use case - set  default API request method
            if (Tools::getIsset(strtolower(_FPC_MODULE_NAME) . 'ApiRequestType')) {
                $iDefaultApiMethod = Tools::getValue(strtolower(_FPC_MODULE_NAME) . 'ApiRequestType');

                if (!empty($iDefaultApiMethod)) {
                    if (!Configuration::updateValue('FBPSC_API_REQUEST_METHOD', $iDefaultApiMethod)) {
                        throw new Exception(FacebookPsConnect::$oModule->l('An error occurred during default API request method update',
                                'admin-update_class') . '.', 116);
                    }
                }
            }

            // use case - set button style
            if (Tools::getIsset(strtolower(_FPC_MODULE_NAME) . 'ButtonStyle')) {
                $sButtonStyle = Tools::getValue(strtolower(_FPC_MODULE_NAME) . 'ButtonStyle');

                if (!Configuration::updateValue('FBPSC_BUTTON_STYLE', $sButtonStyle)) {
                    throw new Exception(FacebookPsConnect::$oModule->l('An error occurred during button style update update',
                            'admin-update_class') . '.', 125);
                }
            }

        } catch (Exception $e) {
            $aUpdate['aErrors'][] = array('msg' => $e->getMessage(), 'code' => $e->getCode());
        }

        // get configuration options
        BT_FPCModuleTools::getConfiguration();

        // require admin configure class - to factorise
        require_once(_FPC_PATH_LIB_ADMIN . 'admin-display_class.php');

        // get run of admin display in order to display first page of admin with css settings updated
        $aData = BT_AdminDisplay::create()->run(array('sType' => 'basic'));

        // use case - empty error and updating status
        $aData['assign'] = array_merge($aData['assign'], array(
            'iActiveTab' => 1,
            'bUpdate' => (empty($aUpdate['aErrors']) ? true : false),
        ), $aUpdate);

        //force XHR
        FacebookPsConnect::$sQueryMode = 'xhr';

        // destruct
        unset($aUpdate);

        return $aData;
    }

    /**
     * update configuration of connector
     *
     * @param array $aPost
     * @return array
     */
    private function updateConnector(array $aPost)
    {
        // set
        $aAssign = array();

        // clean
        @ob_end_clean();

        // use case - check connector ID
        if (!isset($aPost['iConnectorId'])
            || (isset($aPost['iConnectorId'])
            && !array_key_exists($aPost['iConnectorId'], $GLOBALS['FBPSC_CONNECTORS']))
        ) {
            throw new Exception('Connector ID isn\'t valid', 160);
        }

        // set
        $aData = array();

        $bActive = (Tools::getIsset('activeConnector') && Tools::getValue('activeConnector') == 'true') ? true : false;

        // use case - active elt
        if (empty($sPostType)) {
            if (!empty($bActive)) {

                $aData['activeConnector'] = $bActive;
            }
            // use case - app ID elt
            if (!empty($aPost['id'])) {
                $aData['id'] = trim(strip_tags($aPost['id']));
            }
            // use case - secret elt
            if (!empty($aPost['secret'])) {
                $aData['secret'] = trim(strip_tags($aPost['secret']));
            }
            // use case - callback elt
            if (!empty($aPost['callback'])) {
                $aData['callback'] = strip_tags($aPost['callback']);
            }
            // use case - scope elt
            if (!empty($aPost['scope'])) {
                $aData['scope'] = strip_tags($aPost['scope']);
            }
            // use case - permissions elt
            if (isset($aPost['permissions'])) {
                $aData['permissions'] = $aPost['permissions'] == 'on' ? true : false;
            }
            // use case - developer key elt
            if (!empty($aPost['developerKey'])) {
                $aData['developerKey'] = strip_tags($aPost['developerKey']);
            }
            // use case - permissions elt
            if (!empty($aPost['permissions'])) {
                $aData['permissions'] = strip_tags($aPost['permissions']);
            }

            // use case - display elt
            if (!empty($aPost['display'])) {
                $aData['display'] = strip_tags($aPost['display']);
            }


            if (empty($sPostType)) {

                // update connector data
                if (!Configuration::updateValue('FBPSC_' . strtoupper($aPost['iConnectorId']), serialize($aData))) {
                    throw new Exception('Update data of connector "' . $aPost['iConnectorId'] . '" doesn\'t work', 168);
                }
            }
        }

        return array('tpl' => _FPC_TPL_ADMIN_PATH . _FPC_TPL_CONFIRM, 'assign' => $aAssign);
    }

    /**
     * update hook
     *
     * @param array $aPost
     * @return array
     */
    private function updateHook(array $aPost)
    {
        // clean headers
        @ob_end_clean();

        // set
        $aAssign = array();

        $aOldClassicalHookInclude = array('blockUser', 'authentication', 'orderFunnel');

        $aHookData = BT_FPCModuleDao::getConnectorPositions('classical', $aOldClassicalHookInclude);

        // use case - check hook ID
        if (!isset($aPost['sHookId'])
        ) {
            throw new Exception('Hook ID isn\'t valid', 170);
        } // use case - check connector list
        elseif (!isset($aPost['sConnectorList'])) {
            throw new Exception('Connector list isn\'t valid', 171);
        } else {
            // set variable
            $aTmpConnector = array();

            if (!empty($aPost['sConnectorList'])) {
                // get list of added connector to the hook
                $aConnectorList = explode(',', $aPost['sConnectorList']);

                // loop on each connector in order to verify id
                foreach ($aConnectorList as $sId) {
                    if (!array_key_exists($sId, $GLOBALS['FBPSC_CONNECTORS'])) {
                        throw new Exception('"' . $sId . '"' . FacebookPsConnect::$oModule->l('Connector isn\'t valid'),
                            172);
                    } else {
                        // get data by connector
                        $aTmpConnector[$sId] = $GLOBALS['FBPSC_CONNECTORS'][$sId]['title'];
                    }
                }
            }

            //get the data from the current hook ID
            $aHookData = BT_FPCModuleDao::getConnectorPositionDataById((int)$aHookData[$aPost['sHookName']]['id']);
            $aTmpData = unserialize($aHookData['data']);

            $aData = array(
                'page' => $aTmpData['page'],
                'position' => $aTmpData['position'],
                'style' => $aTmpData['position'],
                'html' => $aTmpData['html'],
                // 'size' => $aTmpData['size'],
                'size' => 'large',
                'connectors' => serialize($aTmpConnector),
                'css' => $aTmpData['css'],
                'js' => '',
            );

            //Update the table with the connectors updated
            BT_FPCModuleDao::updateHookData($aData, (int)$aHookData['id']);

            if (!Configuration::updateValue(_FPC_MODULE_NAME . '_' . strtoupper($aPost['sHookName']),
                serialize($aTmpConnector))) {
                throw new Exception('Update data of hook "' . $aPost['sHookName'] . '" doesn\'t work', 173);
            }

            unset($aTmpConnector);
            unset($aConnectorList);
        }
        return array('tpl' => _FPC_TPL_ADMIN_PATH . _FPC_TPL_CONFIRM, 'assign' => $aAssign);
    }

    /**
     * update general hook form
     *
     * @param array $aPost
     * @return array
     */
    private function updateHookAdvanced(array $aPost)
    {
        // clean headers
        @ob_end_clean();

        $aUpdate = array();

        // To use DAO class
        require_once(_FPC_PATH_LIB . 'module-dao_class.php');

        try {

            // use case - the position on the login form
            $sBtnAccountPosition = Tools::getValue('LoginPagePosition');
            $aHook = BT_FPCModuleDao::getConnectorByPosition('authentication');
            $aHookData = unserialize($aHook['data']);

            $aData = array(
                'page' => $aHookData['page'],
                'position' => $sBtnAccountPosition,
                'style' => $aHookData['position'],
                'html' => $aHookData['html'],
                // 'size' => $aHookData['size'],
                'size' => 'large',
                'connectors' => $aHookData['connectors'],
                'css' => $aHookData['css'],
                'js' => '',
            );
            //Update the table values
            BT_FPCModuleDao::updateHookData($aData, (int)$aHook['id']);
            unset($sBtnAccountPosition);
            unset($aHook);
            unset($aHookData);

            $sBtnFunnelPosition = Tools::getValue('LoginPagePosition17');
            if (!Configuration::updateValue('FBPSC_FUN_BTN_POSITION', $sBtnFunnelPosition)) {
                throw new Exception(FacebookPsConnect::$oModule->l('An error occurred during funnel button position update',
                        'admin-update_class') . '.', 181);
            }
            unset($sBtnAccountPosition);

            $sOpcPosition = Tools::getValue('OpcPosition');
            if (!Configuration::updateValue('FBPSC_OPC_POSITION', $sOpcPosition)) {
                throw new Exception(FacebookPsConnect::$oModule->l('An error occurred during opc button position update',
                        'admin-update_class') . '.', 182);
            }
            unset($sOpcPosition);

        } catch (Exception $e) {
            $aUpdate['aErrors'][] = array('msg' => $e->getMessage(), 'code' => $e->getCode());
        }

        // get configuration options
        BT_FPCModuleTools::getConfiguration();

        // require admin configure class - to factorise
        require_once(_FPC_PATH_LIB_ADMIN . 'admin-display_class.php');

        // get run of admin display in order to display first page of admin with basic settings updated
        $aData = BT_AdminDisplay::create()->run(array('sType' => 'hookAdvanced'));

        // use case - empty error and updating status
        $aData['assign'] = array_merge($aData['assign'], array(
            'iActiveTab' => 1,
            'bUpdate' => (empty($aUpdate['aErrors']) ? true : false),
        ), $aUpdate);

        //force XHR
        FacebookPsConnect::$sQueryMode = 'xhr';

        // destruct
        unset($aUpdate);

        return $aData;
    }


    /**
     * update connector position values
     *
     * @param array $aPost
     * @return array
     */
    private function updateConnectorPositionUpdate(array $aPost)
    {
        // clean headers
        @ob_end_clean();

        // To use DAO class
        require_once(_FPC_PATH_LIB . 'module-dao_class.php');

        // set
        $aAssign = array();

        $bUpdated = false;

        $iStatus = Tools::getValue('fbpscPageConnectorsActivate');
        $sPage = Tools::getValue('fbpscPageConnectors');
        $sHtmlElement = Tools::getValue('fbpscHtmlElement');
        $sPosition = Tools::getValue('fbpscPageConnectorsPosition');
        // $sSize = Tools::getValue('fbpscPageConnectorsSize');
        $sPositionId = Tools::getValue('PositionId');
        $aConnectors = Tools::getValue('connector');
        $sPositionName = Tools::getValue('fbpscPositionName');

        $aCss['activate'] = Tools::getValue('bDisplayBlock');
        $aCss['border_color'] = Tools::getValue('sCssBorderColor');
        $aCss['border_size'] = Tools::getValue('sCssBorderSize');
        $aCss['background_color'] = Tools::getValue('sCssBackgroundColor');
        $aCss['text_color'] = Tools::getValue('sCssTextColor');
        $aCss['text_size'] = Tools::getValue('sCssTextSize');
        $aCss['padding_top'] = Tools::getValue('sCssPaddingTop');
        $aCss['padding_right'] = Tools::getValue('sCssPaddingRight');
        $aCss['padding_bottom'] = Tools::getValue('sCssPaddingBottom');
        $aCss['padding_left'] = Tools::getValue('sCssPaddingLeft');
        $aCss['margin_top'] = Tools::getValue('sCssMarginTop');
        $aCss['margin_right'] = Tools::getValue('sCssMarginRight');
        $aCss['margin_bottom'] = Tools::getValue('sCssMarginBottom');
        $aCss['margin_left'] = Tools::getValue('sCssMarginLeft');

        //Format the input data array
        $aData = array(
            'page' => $sPage,
            'html' => $sHtmlElement,
            'position' => $sPosition,
            'style' => 'modern',
            // 'size' => $sSize,
            'size' => 'large',
            'connectors' => serialize($aConnectors),
            'css' => serialize($aCss),
            'js' => '',
        );

        // Remove blank space to avoid bug on the CSS generation
        $sPositionName = str_replace(' ', '_', $sPositionName);

        try {
            if (empty($sPositionId)) {

                // Add the connector position information in the database
                if (BT_FPCModuleDao::addConnectorPosition((String)$sPositionName, 'advanced', $iStatus,
                    FacebookPsConnect::$iShopId, $aData)) {
                    $bUpdated = true;
                }
            } else {
                if (BT_FPCModuleDao::updateConnectorPosition((String)$sPositionName, 'advanced', $iStatus,
                    FacebookPsConnect::$iShopId, $aData, (int)$sPositionId)) {
                    $bUpdated = true;
                }
            }

            // Use case if the table is updated the css file is updated
            if (!empty($bUpdated)) {
                $aPositionData = BT_FPCModuleDao::getConnectorPositions();

                BT_FPCModuleTools::loopCssGenerate($aPositionData);
            }

        } catch (Exception $e) {
            $aAssign['aErrors'][] = array('msg' => $e->getMessage(), 'code' => $e->getCode());
        }

        // check update OK
        $aAssign['bUpdate'] = empty($aAssign['aErrors']) ? true : false;
        $aAssign['sErrorInclude'] = BT_FPCModuleTools::getTemplatePath(_FPC_PATH_TPL_NAME . _FPC_TPL_ADMIN_PATH . _FPC_TPL_ERROR);

        // force xhr mode
        FacebookPsConnect::$sQueryMode = 'xhr';

        return array(
            'tpl' => _FPC_TPL_ADMIN_PATH . _FPC_TPL_CONFIRM,
            'assign' => $aAssign,
        );
    }

    /**
     * update general hook form
     *
     * @param array $aPost
     * @return array
     */
    private function updateConnectPosition(array $aPost)
    {
        // clean headers
        @ob_end_clean();

        // set
        $aData = array();

        try {
            require_once(_FPC_PATH_LIB . 'module-dao_class.php');

            // get parameters for request
            $iPositionId = Tools::getValue('iPositionId');
            $sAction = Tools::getValue('sActionType');
            $iStatus = Tools::getValue('iStatus');

            switch ($sAction) {
                case 'delete' :
                    if (!empty($iPositionId)) {
                        BT_FPCModuleDao::deleteConnectorPosition((int)$iPositionId);
                    }
                    break;
                case 'deleteBulk' :
                    if (!empty($iPositionId)) {

                        //Manage the string returned by the js function
                        $aPositionId = explode(",", $iPositionId);

                        if (is_array($aPositionId)) {
                            foreach ($aPositionId as $sPosition) {
                                BT_FPCModuleDao::deleteConnectorPosition((int)$sPosition);
                            }
                        }
                    }
                    break;
                case 'statusUpdate' :
                    if (!empty($iPositionId) && ($iStatus == 1 || $iStatus == 0)) {
                        BT_FPCModuleDao::updateConnectorStatus((int)$iPositionId, (int)$iStatus);
                    }
                    break;
                case 'statusUpdateActionBulk' :
                    if (!empty($iPositionId) && ($iStatus == 1 || $iStatus == 0)) {

                        //Manage the string returned by the js function
                        $aPositionId = explode(",", $iPositionId);

                        if (is_array($aPositionId)) {
                            foreach ($aPositionId as $sPosition) {
                                BT_FPCModuleDao::updateConnectorStatus((int)$sPosition, (int)$iStatus);
                            }
                        }

                    }
                    break;
                default :
                    break;
            }

            unset($iPositionId);
        } catch (Exception $e) {
            $aData['aErrors'][] = array('msg' => $e->getMessage(), 'code' => $e->getCode());
        }

        // get configuration options
        BT_FPCModuleTools::getConfiguration();

        // require admin configure class - to factorise
        require_once(_FPC_PATH_LIB_ADMIN . 'admin-display_class.php');

        // get run of admin display in order to display first page of admin with basics settings updated
        $aDisplay = BT_AdminDisplay::create()->run(array('sType' => 'hookAdvanced'));

        // use case - empty error and updating status
        $aDisplay['assign'] = array_merge($aDisplay['assign'], array(
            'bUpdate' => (empty($aData['aErrors']) ? true : false),
        ), $aData);

        // destruct
        unset($aData);

        return $aDisplay;
    }

    /**
     * update voucher
     *
     * @param array $aPost
     * @return array
     */
    private function updateVoucher(array $aPost)
    {
        require_once(_FPC_PATH_LIB . 'module-dao_class.php');

        // clean headers
        @ob_end_clean();

        // set
        $aUpdate = array();

        try {
            // use case - check display fancy popin for asking to associate FB account with PS
            $bEnableVoucher = (Tools::getIsset('fbpscEnableVoucher') && Tools::getValue('fbpscEnableVoucher') == 'true') ? true : false;

            if (!Configuration::updateValue('FBPSC_ENABLE_VOUCHER', $bEnableVoucher)) {
                throw new Exception(FacebookPsConnect::$oModule->l('An error occurred during FB association voucher enable update',
                        'admin-update_class') . '.', 180);
            }

            if ($bEnableVoucher) {
                // Voucher code
                $sVoucherCode = Tools::getValue('fbpscPrefixCode');
                if (!Configuration::updateValue('FBPSC_VOUCHER_CODE_PREFIX', $sVoucherCode)) {
                    throw new Exception(FacebookPsConnect::$oModule->l('An error occurred during voucher code name update',
                            'admin-update_class') . '.', 181);
                }
                unset($sVoucherCode);

                // Voucher type
                $sVoucherType = Tools::getValue('fbpscDiscountType');
                if (!Configuration::updateValue('FBPSC_VOUCHER_TYPE', $sVoucherType)) {
                    throw new Exception(FacebookPsConnect::$oModule->l('An error occurred during voucher type update',
                            'admin-update_class') . '.', 182);
                }
                unset($sVoucherType);

                // voucher percent
                $sVoucherPercent = Tools::getValue('fbpscVoucherPercent');
                if (!Configuration::updateValue('FBPSC_VOUCHER_PERCENT', $sVoucherPercent)) {
                    throw new Exception(FacebookPsConnect::$oModule->l('An error occurred during voucher percent update',
                            'admin-update_class') . '.', 183);
                }
                unset($sVoucherPercent);

                // voucher amount
                $sVoucherAmount = Tools::getValue('fbpscVoucherAmount');
                if (!Configuration::updateValue('FBPSC_VOUCHER_AMOUNT', $sVoucherAmount)) {
                    throw new Exception(FacebookPsConnect::$oModule->l('An error occurred during voucher amount update',
                            'admin-update_class') . '.', 184);
                }
                unset($sVoucherPercent);

                // voucher currency
                $sVoucherCurrency = Tools::getValue('fbpscCurrencyId');
                if (!Configuration::updateValue('FBPSC_VOUCHER_CURRENCY', $sVoucherCurrency)) {
                    throw new Exception(FacebookPsConnect::$oModule->l('An error occurred during voucher currency update',
                            'admin-update_class') . '.', 185);
                }
                unset($sVoucherCurrency);

                // voucher tax
                $sVoucherTax = Tools::getValue('fbpscTax');
                if (!Configuration::updateValue('FBPSC_VOUCHER_TAX', $sVoucherTax)) {
                    throw new Exception(FacebookPsConnect::$oModule->l('An error occurred during voucher tax update',
                            'admin-update_class') . '.', 185);
                }
                unset($sVoucherTax);

                // voucher name
                $this->updateLang($aPost, 'fbpscVoucherDesc', 'FBPSC_VOUCHER_DESC', false,
                    FacebookPsConnect::$oModule->l('home category name', 'admin-update_class'));

                // Category tree
                $aCategoryBox = Tools::getValue('bt_category-box');

                if (!empty($aCategoryBox)) {
                    // delete previous categories
                    BT_FPCModuleDao::deleteCategories(FacebookPsConnect::$iShopId);

                    foreach ($aCategoryBox as $iCatId) {
                        // insert
                        BT_FPCModuleDao::insertCategory($iCatId, FacebookPsConnect::$iShopId);
                    }
                }
                unset($aCategoryBox);

                // voucher min qty
                $sVoucherMinQty = Tools::getValue('fbpscMaximumQte');
                if (!Configuration::updateValue('FBPSC_VOUCHER_MAX_QTY', $sVoucherMinQty)) {
                    throw new Exception(FacebookPsConnect::$oModule->l('An error occurred during voucher min qty update',
                            'admin-update_class') . '.', 187);
                }
                unset($sVoucherMinQty);

                // voucher min amount
                $sVoucherMinAmount = Tools::getValue('fbpscMinimum');
                if (!Configuration::updateValue('FBPSC_VOUCHER_MIN_AMOUNT', $sVoucherMinAmount)) {
                    throw new Exception(FacebookPsConnect::$oModule->l('An error occurred during voucher min amount update',
                            'admin-update_class') . '.', 188);
                }
                unset($sVoucherMinQty);

                // voucher validity
                $sVoucherValidity = Tools::getValue('fbpscValidity');
                if (!Configuration::updateValue('FBPSC_VOUCHER_VALIDITY', $sVoucherValidity)) {
                    throw new Exception(FacebookPsConnect::$oModule->l('An error occurred during voucher validity update',
                            'admin-update_class') . '.', 190);
                }
                unset($sVoucherValidity);

                // use case - check display hightlight the voucher code
                $bHightLightVoucher = (Tools::getIsset('fbpscHighlight') && Tools::getValue('fbpscHighlight') == 'true') ? true : false;

                if (!Configuration::updateValue('FBPSC_HIGHTLIGHT_VOUCHER', $bHightLightVoucher)) {
                    throw new Exception(FacebookPsConnect::$oModule->l('An error occurred during FB association hightlight voucher enable update',
                            'admin-update_class') . '.', 191);
                }

                // use case - check display hightlight the voucher code
                $bCumulateOthersVoucher = (Tools::getIsset('fbpscCumulativeOther') && Tools::getValue('fbpscCumulativeOther') == 'true') ? true : false;

                if (!Configuration::updateValue('FBPSC_CUMULATE_OTHERS_VOUCHER', $bCumulateOthersVoucher)) {
                    throw new Exception(FacebookPsConnect::$oModule->l('An error occurred during FB association cumulate others voucher enable update',
                            'admin-update_class') . '.', 192);
                }

                // use case - check display hightlight the voucher code
                $bCumulateOthersDiscount = (Tools::getIsset('fbpscCumulativeReduc') && Tools::getValue('fbpscCumulativeReduc') == 'true') ? true : false;

                if (!Configuration::updateValue('FBPSC_CUMULATE_DISCOUNT_VOUCHER', $bCumulateOthersDiscount)) {
                    throw new Exception(FacebookPsConnect::$oModule->l('An error occurred during FB association cumulate others discount enable update',
                            'admin-update_class') . '.', 193);
                }

                // display mode
                $sVoucherDisplayMode = Tools::getValue('fbpscVoucherDisplayMode');
                if (!Configuration::updateValue('FBPSC_VOUCHER_DISPLAY_MODE', $sVoucherDisplayMode)) {
                    throw new Exception(FacebookPsConnect::$oModule->l('An error occurred during voucher display mode update',
                            'admin-update_class') . '.', 194);
                }
                unset($sVoucherDisplayMode);

                // display text
                $this->updateLang($aPost, 'fbpscDisplayVoucherText', 'FBPSC_VOUCHER_DISP_TEXT', false,
                    FacebookPsConnect::$oModule->l('display text', 'admin-update_class'));

                // use case - check display hightlight the voucher code
                $bEnableVoucherText = (Tools::getIsset('fbpscEnableCustomVoucherText') && Tools::getValue('fbpscEnableCustomVoucherText') == 'true') ? true : false;

                if (!Configuration::updateValue('FBPSC_ENABLE_CUSTOM_TXT', $bEnableVoucherText)) {
                    throw new Exception(FacebookPsConnect::$oModule->l('An error occurred during FB activation for custom text update',
                            'admin-update_class') . '.', 193);
                }
            }
        } catch (Exception $e) {
            $aUpdate['aErrors'][] = array('msg' => $e->getMessage(), 'code' => $e->getCode());
        }

        // get configuration options
        BT_FPCModuleTools::getConfiguration();

        // force XHR
        FacebookPsConnect::$sQueryMode = 'xhr';

        // require admin configure class - to factorise
        require_once(_FPC_PATH_LIB_ADMIN . 'admin-display_class.php');

        // get run of admin display in order to display first page of admin with basic settings updated
        $aData = BT_AdminDisplay::create()->run(array('sType' => 'voucher'));

        // use case - empty error and updating status
        $aData['assign'] = array_merge($aData['assign'], array(
            'iActiveTab' => 1,
            'bUpdate' => (empty($aUpdate['aErrors']) ? true : false),
        ), $aUpdate);

        // destruct
        unset($aUpdate);

        return $aData;
    }

    /**
     * update dashboard
     *
     * @param array $aPost
     * @return array
     */
    private function updateDashboard(array $aPost)
    {
        require_once(_FPC_PATH_LIB_DASHBOARD . 'dashboard_class.php');

        // clean headers
        @ob_end_clean();

        //Get the values from the form and
        $sSocial = Tools::strtolower(Tools::getValue('social-network'));
        $sDateAddFrom = Tools::getValue('date-add_from');
        $sDateAddTo = Tools::getValue('date-add_to');
        $aFinalCustomerInfo = array();

        // set
        $aUpdate = array();
        try {

            //manage the params data for charts
            $aParams = array($sSocial, $sDateAddFrom, $sDateAddTo);

            foreach ($GLOBALS['FBPSC_CHARTS_PARAMS'] as $aCharts) {
                if (FacebookPsConnect::$sCurrentLang == 'en'
                    || FacebookPsConnect::$sCurrentLang == 'fr'
                    || FacebookPsConnect::$sCurrentLang == 'es'
                    || FacebookPsConnect::$sCurrentLang == 'it'
                ) {
                    $sTitle = $aCharts['title'][FacebookPsConnect::$sCurrentLang];
                } else {
                    $sTitle = $aCharts['title']['en'];
                }

                $aChartsOutput[] = array(
                    'data' => call_user_func_array($aCharts['callback'], $aParams),
                    'id' => $aCharts['id'],
                    'title' => $sTitle
                );
            }

            //Get the customer ID for connectors with the module
            $aCustomerIdsPerSocial = BT_FPCDashboardDao::getCustomerIds($sSocial, $sDateAddFrom, $sDateAddTo);

            //Manage the customer information for the dashboard table
            foreach ($aCustomerIdsPerSocial as $mCustomer) {
                $oCustomer = new Customer ((int)$mCustomer['CNT_CUST_ID']);
                $aFinalCustomerInfo[] = array(
                    'lastname' => $oCustomer->lastname,
                    'firstname' => $oCustomer->firstname,
                    'email' => $oCustomer->email,
                    'date_add' => $oCustomer->date_add,
                    'social' => $mCustomer['CNT_CUST_TYPE']
                );

            }
            unset($aCustomerIdsPerSocial);
            unset($oCustomer);

            $aUpdate = array(
                'aDashboard' => BT_FPCDashboard::searchData($sSocial, $sDateAddFrom, $sDateAddTo),
                'aSocialCount' => BT_FPCDashboard::countSocialConnect($sSocial, $sDateAddFrom, $sDateAddTo),
                'aSocialAmount' => BT_FPCDashboard::getCustomerSpent($sSocial, $sDateAddFrom, $sDateAddTo),
                'aCharts' => $aChartsOutput,
                'aChartColor' => $GLOBALS['FBPSC_CONNECTORS'],
                'sCurrentSocialFilter' => !empty($sSocial) ? Tools::ucfirst($sSocial) : '',
                'sFilterDateFrom' => $sDateAddFrom,
                'sFilterDateTo' => $sDateAddTo,
                'aCustomerInfo' => $aFinalCustomerInfo,
                'sCurrency' => Currency::getCurrency(Configuration::get('PS_CURRENCY_DEFAULT')),
            );
            

        } catch (Exception $e) {
            $aUpdate['aErrors'][] = array('msg' => $e->getMessage(), 'code' => $e->getCode());
        }

        // get configuration options
        BT_FPCModuleTools::getConfiguration();

        // force XHR
        FacebookPsConnect::$sQueryMode = 'xhr';

        // require admin configure class - to factorise
        require_once(_FPC_PATH_LIB_ADMIN . 'admin-display_class.php');

        // get run of admin display in order to display first page of admin with basic settings updated
        $aData = BT_AdminDisplay::create()->run(array('sType' => 'dashboard'));

        // use case - empty error and updating status
        $aData['assign'] = array_merge($aData['assign'], array(
            'bUpdate' => (empty($aUpdate['aErrors']) ? true : false),
        ), $aUpdate);

        // destruct
        unset($aUpdate);

        return $aData;
    }


    /**
     * update shortcode preferencies
     *
     * @param array $aPost
     * @return array
     */
    private function updateShortCode(array $aPost)
    {
        // clean headers
        @ob_end_clean();

        $aData = array();
        $bUpdateCss = false;

        //Get the values
        $sShortCodeName = Tools::getValue('fbpscPositionName');
        $aSelectedConnectors = Tools::getValue('connector');

        $aCss['activate'] = Tools::getValue('bDisplayBlock');
        $aCss['border_color'] = Tools::getValue('sCssBorderColor');
        $aCss['border_size'] = Tools::getValue('sCssBorderSize');
        $aCss['background_color'] = Tools::getValue('sCssBackgroundColor');
        $aCss['text_color'] = Tools::getValue('sCssTextColor');
        $aCss['text_size'] = Tools::getValue('sCssTextSize');
        $aCss['padding_top'] = Tools::getValue('sCssPaddingTop');
        $aCss['padding_right'] = Tools::getValue('sCssPaddingRight');
        $aCss['padding_bottom'] = Tools::getValue('sCssPaddingBottom');
        $aCss['padding_left'] = Tools::getValue('sCssPaddingLeft');
        $aCss['margin_top'] = Tools::getValue('sCssMarginTop');
        $aCss['margin_right'] = Tools::getValue('sCssMarginRight');
        $aCss['margin_bottom'] = Tools::getValue('sCssMarginBottom');
        $aCss['margin_left'] = Tools::getValue('sCssMarginLeft');

        $sShortCodeName = str_replace(' ', '_', $sShortCodeName);

        //Generate the shortcode
        $sShortCode = "{* Start shortcode for " . $sShortCodeName . " *}<div id=\"sl_connector_" . $sShortCodeName . "\"></div>{literal}<script type=\"text/javascript\">"
            . " $(document).ready("
            . "function(){fbpsc.getConnectors('$sShortCodeName', '$sShortCodeName')"
            . ";});"
            . "</script>{/literal}"
            . "{* End shortcode for " . $sShortCodeName . " *}";

        try {
            // Use case for update
            if (!empty($aPost['iPositionId'])) {
                // Get the hook data
                $aHookData = unserialize(BT_FPCModuleDao::getConnectorPositionDataById((int)$aPost['iPositionId'])['data']);

                $aData = array(
                    'page' => 'all',
                    'position' => 'shortcode',
                    'html' => 'shortcode',
                    'style' => $aHookData['position'],
                    // 'size' => Tools::getValue('fbpscPageConnectorsSize'),
                    'size' => 'large',
                    'connectors' => $aSelectedConnectors,
                    'css' => serialize($aCss),
                    'js' => htmlentities($sShortCode),
                );

                //Update the table values
                if (BT_FPCModuleDao::updateConnectorPosition((String)$sShortCodeName, 'shortcode', 1,
                    FacebookPsConnect::$iShopId, $aData, (int)$aPost['iPositionId'])) {
                    $bUpdateCss = true;
                }
            } else {

                $aData = array(
                    'page' => 'all',
                    'position' => 'shortcode',
                    'html' => 'shortcode',
                    'style' => 'shortcode',
                    // 'size' => Tools::getValue('fbpscPageConnectorsSize'),
                    'size' => 'large',
                    'connectors' => $aSelectedConnectors,
                    'css' => serialize($aCss),
                    'js' => htmlentities($sShortCode),
                );

                $bUpdateCss = true;
                BT_FPCModuleDao::addConnectorPosition((String)$sShortCodeName, 'shortcode', 1,
                    FacebookPsConnect::$iShopId, $aData);
            }

            //Generate the CSS f
            if (!empty($bUpdateCss)) {
                $aPositionData = BT_FPCModuleDao::getConnectorPositions();

                BT_FPCModuleTools::loopCssGenerate($aPositionData);
            }

        } catch (Exception $e) {
            $aAssign['aErrors'][] = array('msg' => $e->getMessage(), 'code' => $e->getCode());
        }

        // check update OK
        $aAssign['bUpdate'] = empty($aAssign['aErrors']) ? true : false;
        $aAssign['sErrorInclude'] = BT_FPCModuleTools::getTemplatePath(_FPC_PATH_TPL_NAME . _FPC_TPL_ADMIN_PATH . _FPC_TPL_ERROR);

        // force xhr mode
        FacebookPsConnect::$sQueryMode = 'xhr';

        return array(
            'tpl' => _FPC_TPL_ADMIN_PATH . _FPC_TPL_CONFIRM,
            'assign' => $aAssign,
        );
    }


    /**
     * update shortcode preferences
     *
     * @param array $aPost
     * @return array
     */
    private function updateShortCodeList(array $aPost)
    {
        // clean headers
        @ob_end_clean();

        // set
        $aData = array();

        try {

            require_once(_FPC_PATH_LIB . 'module-dao_class.php');

            // get parameters for request
            $iPositionId = Tools::getValue('iPositionId');
            $sAction = Tools::getValue('sActionType');

            switch ($sAction) {
                case 'delete' :
                    if (!empty($iPositionId)) {
                        BT_FPCModuleDao::deleteConnectorPosition((int)$iPositionId);
                    }
                    break;
                case 'deleteBulk' :
                    if (!empty($iPositionId)) {

                        //Manage the string returned by the js function
                        $aPositionId = explode(",", $iPositionId);

                        if (is_array($aPositionId)) {
                            foreach ($aPositionId as $sPosition) {
                                BT_FPCModuleDao::deleteConnectorPosition((int)$sPosition);
                            }
                        }
                    }
                    break;
                default :
                    break;
            }

            unset($iPositionId);
        } catch (Exception $e) {
            $aData['aErrors'][] = array('msg' => $e->getMessage(), 'code' => $e->getCode());
        }

        // get configuration options
        BT_FPCModuleTools::getConfiguration();

        // require admin configure class - to factorise
        require_once(_FPC_PATH_LIB_ADMIN . 'admin-display_class.php');

        // get run of admin display in order to display first page of admin with basics settings updated
        $aDisplay = BT_AdminDisplay::create()->run(array('sType' => 'shortCode'));

        // use case - empty error and updating status
        $aDisplay['assign'] = array_merge($aDisplay['assign'], array(
            'bUpdate' => (empty($aData['aErrors']) ? true : false),
        ), $aData);

        // destruct
        unset($aData);

        return $aDisplay;
    }

    /**
     * update css code  preferences
     *
     * @param array $aPost
     * @return array
     */
    private function updateCssCode(array $aPost)
    {
        // clean headers
        @ob_end_clean();

        $aCss = array();

        try {
            $aCss['activate'] = Tools::getValue('bDisplayBlock');
            $aCss['border_color'] = Tools::getValue('sCssBorderColor');
            $aCss['border_size'] = Tools::getValue('sCssBorderSize');
            $aCss['background_color'] = Tools::getValue('sCssBackgroundColor');
            $aCss['text_color'] = Tools::getValue('sCssTextColor');
            $aCss['text_size'] = Tools::getValue('sCssTextSize');
            $aCss['padding_top'] = Tools::getValue('sCssPaddingTop');
            $aCss['padding_right'] = Tools::getValue('sCssPaddingRight');
            $aCss['padding_bottom'] = Tools::getValue('sCssPaddingBottom');
            $aCss['padding_left'] = Tools::getValue('sCssPaddingLeft');
            $aCss['margin_top'] = Tools::getValue('sCssMarginTop');
            $aCss['margin_right'] = Tools::getValue('sCssMarginRight');
            $aCss['margin_bottom'] = Tools::getValue('sCssMarginBottom');
            $aCss['margin_left'] = Tools::getValue('sCssMarginLeft');

            // Get the hook data
            $aHookData = unserialize(BT_FPCModuleDao::getConnectorPositionDataById((int)$aPost['sHookId'])['data']);

            $aData = array(
                'page' => $aHookData['page'],
                'position' => $aHookData['position'],
                'html' => $aHookData['html'],
                'style' => $aHookData['position'],
                // 'size' => $aHookData['size'],
                'size' => 'large',
                'connectors' => $aHookData['connectors'],
                'css' => serialize($aCss),
                'js' => '',
            );

            //Update the table values
            if (BT_FPCModuleDao::updateHookData($aData, (int)$aPost['sHookId'])) {
                $aPositionData = BT_FPCModuleDao::getConnectorPositions();

                BT_FPCModuleTools::loopCssGenerate($aPositionData);
            }
        } catch (Exception $e) {
            $aAssign['aErrors'][] = array('msg' => $e->getMessage(), 'code' => $e->getCode());
        }

        // check update OK
        $aAssign['bUpdate'] = empty($aAssign['aErrors']) ? true : false;
        $aAssign['sErrorInclude'] = BT_FPCModuleTools::getTemplatePath(_FPC_PATH_TPL_NAME . _FPC_TPL_ADMIN_PATH . _FPC_TPL_ERROR);

        // force xhr mode
        FacebookPsConnect::$sQueryMode = 'xhr';

        return array(
            'tpl' => _FPC_TPL_ADMIN_PATH . _FPC_TPL_CONFIRM,
            'assign' => $aAssign,
        );
    }


    /**
     * check and update lang of multi-language fields
     *
     * @param array $aPost : params
     * @param string $sFieldName : field name linked to the translation value
     * @param string $sGlobalName : name of GLOBAL variable to get value
     * @param bool $bCheckOnly
     * @param string $sErrorDisplayName
     * @return array
     */
    private function updateLang(array $aPost, $sFieldName, $sGlobalName, $bCheckOnly = false, $sErrorDisplayName = '')
    {
        // check title in each active language
        $aLangs = array();

        foreach (Language::getLanguages() as $nKey => $aLang) {
            if (empty($aPost[$sFieldName . '_' . $aLang['id_lang']])) {
                $aLangs[$aLang['id_lang']] = $GLOBALS['FBPSC_DEFAULT_CONNECTION_TEXT']['en'];
            } else {
                $aLangs[$aLang['id_lang']] = strip_tags(str_replace('\'', '', $aPost[$sFieldName . '_' . $aLang['id_lang']]));
            }
        }
        if (!$bCheckOnly) {
            // update titles
            if (!Configuration::updateValue($sGlobalName, serialize($aLangs))) {
                $sException = FacebookPsConnect::$oModule->l('An error occurred during', 'admin-update_class')
                    . ' " ' . $sGlobalName . ' " '
                    . FacebookPsConnect::$oModule->l('update', 'admin-update_class')
                    . '.';
                throw new Exception($sException, 596);
            }
        }
        return $aLangs;
    }

    /**
     * set singleton
     *
     * @return obj
     */
    public static function create()
    {
        static $oUpdate;

        if (null === $oUpdate) {
            $oUpdate = new BT_AdminUpdate();
        }
        return $oUpdate;
    }
}
