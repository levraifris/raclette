<?php
/**
 * Social Login
 *
 * @author    BusinessTech.fr - https://www.businesstech.fr
 * @copyright Business Tech 2020 - https://www.businesstech.fr
 * @license   Commercial
 *
 *           ____    _______
 *          |  _ \  |__   __|
 *          | |_) |    | |
 *          |  _ <     | |
 *          | |_) |    | |
 *          |____/     |_|
 */

class BT_AdminCtrl
{
    /**
     * execute abstract derived admin object
     *
     * @param string $sAdminType : type of interface to display
     * @param array $aRequest : request
     * @return array $aDisplay : empty => false / not empty => true
     */
    public function run($sAdminType, $aRequest)
    {
        // set
        $aDisplay = array();

        // include interface
        require_once(_FPC_PATH_LIB_ADMIN . 'i-admin.php');

        switch ($sAdminType) {
            case 'display' :
                // include matched admin object
                require_once(_FPC_PATH_LIB_ADMIN . 'admin-display_class.php');

                $oAdminType = BT_AdminDisplay::create();

                // get configuration options
                BT_FPCModuleTools::getConfiguration(array(
                    'FBPSC_BADGE_TOP_CSS',
                    'FBPSC_BADGE_FOOTER_CSS',
                    'FBPSC_BADGE_AUTHENTICATION_CSS',
                    'FBPSC_BADGE_BLOCKUSER_CSS'
                ));
                break;
            case 'update'   : // update basic settings /
                // include matched admin object
                require_once(_FPC_PATH_LIB_ADMIN . 'admin-update_class.php');

                $oAdminType = BT_AdminUpdate::create();
                break;
            default :
                $oAdminType = false;
                break;
        }

        // process data to use in view (tpl)
        if (!empty($oAdminType)) {
            $aDisplay = $oAdminType->run($aRequest);

            // destruct
            unset($oAdminType);
        }

        return $aDisplay;
    }
}
