<?php
/**
 * Social Login
 *
 * @author    BusinessTech.fr - https://www.businesstech.fr
 * @copyright Business Tech 2020 - https://www.businesstech.fr
 * @license   Commercial
 *
 *           ____    _______
 *          |  _ \  |__   __|
 *          | |_) |    | |
 *          |  _ <     | |
 *          | |_) |    | |
 *          |____/     |_|
 */


class BT_AdminDisplay implements BT_IAdmin
{
    /**
     * display all configured data admin tabs
     * @param array $aParam
     * @return array
     */
    public function run(array $aParam = null)
    {
        require_once(_FPC_PATH_LIB . 'module-dao_class.php');

        // set variables
        $aDisplayInfo = array();

        // get type
        $aParam['sType'] = empty($aParam['sType']) ? 'tabs' : $aParam['sType'];

        switch ($aParam['sType']) {
            case 'tabs' :               // use case - display first page with all tabs
            case 'basic' :              // use case - display basic settings page
            case 'connectors' :         // use case - display connector settings page
            case 'connectorForm' :      // use case - display connector form
            case 'hooks' :              // use case - display hook settings page
            case 'hookForm' :           // use case - display hook form
            case 'hookAdvanced' :           // use case - display hook advanced
            case 'hookAdvancedForm' :           // use case - display hook advanced form
            case 'testssl' :            // use case - display curl ssl
            case 'voucher' :       // use case - display voucher settings page
            case 'dashboard' :       // use case - display dashboard settings page
            case 'connectPositionForm' :       // use case - display form for advance position fancybox settings page
            case 'shortCode' :       // use case - display list for short code management
            case 'shortCodeForm' :       // use case - display form for short code management
            case 'cssCode' :       // use case - display form for css management
            case 'cssForm' :       // use case - display form for css management
                // execute match function
                $aDisplayInfo = call_user_func_array(array($this, 'display' . ucfirst($aParam['sType'])),
                    array($aParam));
                break;
            default :
                break;
        }
        // use case - generic assign
        if (!empty($aDisplayInfo)) {
            $aDisplayInfo['assign']['bMultiShop'] = BT_FPCModuleTools::checkGroupMultiShop();
            $aDisplayInfo['assign'] = array_merge($aDisplayInfo['assign'], $this->assign());
        }

        return $aDisplayInfo;
    }

    /**
     * assigns transverse data
     *
     * @return array
     */
    private function assign()
    {
        // set smarty variables
        return array(
            'sURI' => BT_FPCModuleTools::truncateUri(array('&iPage', '&sAction')),
            'aQueryParams' => $GLOBALS['FBPSC_REQUEST_PARAMS'],
            'sCtrlParamName' => _FPC_PARAM_CTRL_NAME,
            'sController' => _FPC_ADMIN_CTRL,
            'iDefaultLang' => intval(FacebookPsConnect::$iCurrentLang),
            'sDefaultLang' => FacebookPsConnect::$sCurrentLang,
            'sHeaderInclude' => BT_FPCModuleTools::getTemplatePath(_FPC_PATH_TPL_NAME . _FPC_TPL_ADMIN_PATH . _FPC_TPL_HEADER),
            'sErrorInclude' => BT_FPCModuleTools::getTemplatePath(_FPC_PATH_TPL_NAME . _FPC_TPL_ADMIN_PATH . _FPC_TPL_ERROR),
            'sConfirmInclude' => BT_FPCModuleTools::getTemplatePath(_FPC_PATH_TPL_NAME . _FPC_TPL_ADMIN_PATH . _FPC_TPL_CONFIRM),
            'bVersion16' => FacebookPsConnect::$bCompare16,
            'bUseJqueryUI' => (FacebookPsConnect::$bCompare16 == false? true : false),
            'bTwitterActif' => (!empty($GLOBALS['FBPSC_CONNECTORS']['twitter']['data']['activeConnector'])) ? 1 : 2,
            'iApiRequestMethod' => FacebookPsConnect::$conf['FBPSC_API_REQUEST_METHOD'],
            'sLoadingImg' => _FPC_URL_IMG . 'admin/' . _FPC_LOADER_GIF,
            'sModuleVersion' => FacebookPsConnect::$oModule->version,
            'iCurrentLang' => intval(FacebookPsConnect::$iCurrentLang),
            'sFaqLang' => BT_FPCModuleTools::getFaqLang(FacebookPsConnect::$sCurrentLang),
            'aFacebookCount' => '',
            'aAmazonCount' => '',
            'aTwitterCount' => '',
            'aGoogleCount' => '',
            'aPaypalCount' => '',
            'sCurrentIso' => Language::getIsoById(FacebookPsConnect::$iCurrentLang),
            'sButtonStyle' => FacebookPsConnect::$conf['FBPSC_BUTTON_STYLE'],
            'bVersion17' => FacebookPsConnect::$bCompare17
        );
    }

    /**
     * displays admin's first page with all tabs
     *
     * @param array $aPost
     * @return array
     */
    private function displayTabs(array $aPost)
    {
        $iSupportToUse = _FPC_SUPPORT_BT;

        // set smarty variables
        $aAssign = array(
            'sCurrentIso' => Language::getIsoById(FacebookPsConnect::$iCurrentLang),
            'sDocUri' => _MODULE_DIR_ . _FPC_MODULE_SET_NAME . '/',
            'sDocName' => 'readme_' . ((FacebookPsConnect::$sCurrentLang == 'fr') ? 'fr' : 'en') . '.pdf',
            'sContactUs' => 'http://www.businesstech.fr/' . ((FacebookPsConnect::$sCurrentLang == 'fr') ? 'fr/contactez-nous' : 'en/contact-us'),
            'sTs' => time(),
            'bAddJsCss' => true,
            'bHideConfiguration' => BT_FPCWarning::create()->bStopExecution,
            'sCrossSellingUrl' => !empty($iSupportToUse) ? _FPC_SUPPORT_URL . '?utm_campaign=internal-module-ad&utm_source=banniere&utm_medium=' . _FPC_MODULE_SET_NAME : _FPC_SUPPORT_URL . FacebookPsConnect::$sCurrentLang . '/6_business-tech',
            'sCrossSellingImg' => (FacebookPsConnect::$sCurrentLang == 'fr') ? _FPC_URL_IMG . 'admin/module_banner_cross_selling_FR.jpg' : _FPC_URL_IMG . 'admin/module_banner_cross_selling_EN.jpg',
            'sContactUs' => !empty($iSupportToUse) ? _FPC_SUPPORT_URL . ((FacebookPsConnect::$sCurrentLang == 'fr') ? 'fr/contactez-nous' : 'en/contact-us') : _FPC_SUPPORT_URL . ((FacebookPsConnect::$sCurrentLang == 'fr') ? 'fr/ecrire-au-developpeur?id_product=' . _FPC_SUPPORT_ID : 'en/write-to-developper?id_product=' . _FPC_SUPPORT_ID),
            'sRateUrl' => !empty($iSupportToUse) ? _FPC_SUPPORT_URL . ((FacebookPsConnect::$sCurrentLang == 'fr') ? 'fr/modules-prestashop-reseaux-sociaux-facebook/30-facebook-ps-connect-paypal-access-google-twitter-0656272557829.html' : 'en/prestashop-modules-social-networks-facebook/30-facebook-ps-connect-paypal-access-google-0656272557829.html') : _FPC_SUPPORT_URL . ((FacebookPsConnect::$sCurrentLang == 'fr') ? '/fr/ratings.php' : '/en/ratings.php'),
        );

        // use case - get display data of basic settings
        $aData = $this->displayBasic($aPost);

        $aAssign = array_merge($aAssign, $aData['assign']);

        // use case - get display data of connector settings
        $aData = $this->displayConnectors($aPost);

        $aAssign = array_merge($aAssign, $aData['assign']);

        // use case - get display data of hook settings
        $aData = $this->displayHooks($aPost);

        $aAssign = array_merge($aAssign, $aData['assign']);

        // use case - get display data of advanced hook settings
        $aData = $this->displayHookAdvanced($aPost);

        $aAssign = array_merge($aAssign, $aData['assign']);

        // use case - get display data of prerequisistes settings
        $aData = $this->displayPrerequisitesCheck($aPost);

        $aAssign = array_merge($aAssign, $aData['assign']);

        // use case - get display data of voucher
        $aData = $this->displayVoucher($aPost);

        $aAssign = array_merge($aAssign, $aData['assign']);

        // use case - get display data of voucher
        $aData = $this->displayDashboard($aPost);

        $aAssign = array_merge($aAssign, $aData['assign']);

        // use case - get display data of voucher
        $aData = $this->displayShortCode($aPost);

        $aAssign = array_merge($aAssign, $aData['assign']);

        // use case - get display data of voucher
        $aData = $this->displayConnectorForm($aPost);

        $aAssign = array_merge($aAssign, $aData['assign']);

        // use case - get display data of css form configuration
        $aData = $this->displayCssCode($aPost);

        $aAssign = array_merge($aAssign, $aData['assign']);

        // assign all included templates files
        $aAssign['sBasicsInclude'] = BT_FPCModuleTools::getTemplatePath(_FPC_PATH_TPL_NAME . _FPC_TPL_ADMIN_PATH . _FPC_TPL_BASIC_SETTINGS);
        $aAssign['sConnectorInclude'] = BT_FPCModuleTools::getTemplatePath(_FPC_PATH_TPL_NAME . _FPC_TPL_ADMIN_PATH . _FPC_TPL_CONNECTOR_SETTINGS);
        $aAssign['sHookInclude'] = BT_FPCModuleTools::getTemplatePath(_FPC_PATH_TPL_NAME . _FPC_TPL_ADMIN_PATH . _FPC_TPL_HOOK_SETTINGS);
        $aAssign['sPrerequisitesCheck'] = BT_FPCModuleTools::getTemplatePath(_FPC_PATH_TPL_NAME . _FPC_TPL_ADMIN_PATH . _FPC_TPL_PREREQUISITES_CHECK_SETTINGS);
        $aAssign['sVoucherInclude'] = BT_FPCModuleTools::getTemplatePath(_FPC_PATH_TPL_NAME . _FPC_TPL_ADMIN_PATH . _FPC_TPL_VOUCHER_SETTINGS);
        $aAssign['sDashboardInclude'] = BT_FPCModuleTools::getTemplatePath(_FPC_PATH_TPL_NAME . _FPC_TPL_ADMIN_PATH . _FPC_TPL_DASHBOARD);
        $aAssign['sHookAdvancedInclude'] = BT_FPCModuleTools::getTemplatePath(_FPC_PATH_TPL_NAME . _FPC_TPL_ADMIN_PATH . _FPC_TPL_HOOK_ADVANCED);
        $aAssign['sShortCodeInclude'] = BT_FPCModuleTools::getTemplatePath(_FPC_PATH_TPL_NAME . _FPC_TPL_ADMIN_PATH . _FPC_TPL_SHORT_CODE);
        $aAssign['sCssInclude'] = BT_FPCModuleTools::getTemplatePath(_FPC_PATH_TPL_NAME . _FPC_TPL_ADMIN_PATH . _FPC_TPL_CSS_CODE);
        $aAssign['iTestSsl'] = FacebookPsConnect::$conf['FBPSC_SSL_TEST_TODO'];
        $aAssign['iCurlSslCheck'] = FacebookPsConnect::$conf['FBPSC_TEST_CURl_SSL'];


        return array(
            'tpl' => _FPC_TPL_ADMIN_PATH . _FPC_TPL_BODY,
            'assign' => $aAssign,
        );
    }

    /**
     * displays snippets settings
     *
     * @param array $aPost
     * @return array
     */
    private function displayBasic(array $aPost)
    {
        $aAssign = array();

        if (FacebookPsConnect::$sQueryMode == 'xhr') {
            // clean header
            @ob_end_clean();
        }

        $aAssign['bVerion15_16'] = true;

        // set smarty variables
        $aAssign = array(
            'aConnectionText' => BT_FPCModuleTools::getDefaultTranslations('FBPSC_CONNECT_TEXT','DEFAULT_CONNECTION_TEXT'),
            'bDisplayBlock' => FacebookPsConnect::$conf['FBPSC_DISPLAY_BLOCK'],
            'bDisplayCustomText' => FacebookPsConnect::$conf['FBPSC_DISPLAY_CUSTOM_TEXT'],
            'bDisplayBlockInfoAccount' => FacebookPsConnect::$conf['FBPSC_DISPLAY_BLOCK_INFO_ACCOUNT'],
            'bDisplayBlockInfoCart' => FacebookPsConnect::$conf['FBPSC_DISPLAY_BLOCK_INFO_CART'],
            'bOnePageCheckOut' => Configuration::get('PS_ORDER_PROCESS_TYPE'),
            'iDefaultCustomerGroup' => FacebookPsConnect::$conf['FBPSC_DEFAULT_CUSTOMER_GROUP'],
            'sApiRequestType' => FacebookPsConnect::$conf['FBPSC_API_REQUEST_METHOD'],
            'aApiCallMethod' => array(
                array(
                    'type' => 'fopen',
                    'name' => FacebookPsConnect::$oModule->l('Native PHP file_get_contents', 'admin-display_class'),
                    'active' => ini_get('allow_url_fopen')
                ),
                array(
                    'type' => 'curl',
                    'name' => FacebookPsConnect::$oModule->l('PHP cURL library mode', 'admin-display_class'),
                    'active' => function_exists('curl_init')
                )
            ),
            'aLangs' => Language::getLanguages(),
            'bEnableVouchers' => FacebookPsConnect::$conf['FBPSC_ENABLE_VOUCHER'],
            'bEnableCustomVoucherText' => FacebookPsConnect::$conf['FBPSC_ENABLE_CUSTOM_TXT'],
            'aVoucherCustomText' => BT_FPCModuleTools::getDefaultTranslations('FBPSC_VOUCHER_DISP_TEXT',
                'DEFAULT_DISPLAY_TEXT'),
            'aLangs' => Language::getLanguages(),
            'sCurrentLang' => FacebookPsConnect::$iCurrentLang,
            'sCurrentIso' => Language::getIsoById(FacebookPsConnect::$iCurrentLang),
            'aButtonStyle' => $GLOBALS['FBPSC_BUTTON_STYLE'],
            'aConnectorsList' => $GLOBALS['FBPSC_CONNECTORS'],
        );


        $aAssign['aGroups'] = Group::getGroups(FacebookPsConnect::$iCurrentLang, FacebookPsConnect::$iShopId);


        return array(
            'tpl' => _FPC_TPL_ADMIN_PATH . _FPC_TPL_BASIC_SETTINGS,
            'assign' => $aAssign,
        );
    }

    /**
     * displays connectors list
     *
     * @param array $aPost
     * @return array
     */
    private function displayConnectors(array $aPost)
    {
        // set
        $aAssign = array();

        require_once(_FPC_PATH_LIB . 'module-dao_class.php');
        require_once(_FPC_PATH_LIB_RENDER . 'facade-render-connectors_class.php');

        $aConnectorData = BT_FPCModuleDao::getConnectorPositions('classical', $GLOBALS['FBPSC_OLD_CLASSICAL_POSITION']);

        $aHooks = FacadeRenderConnectors::loopConnector($aConnectorData, 'extract', null);

        // Loop on buttons configuraiton
        foreach ($GLOBALS['FBPSC_CONNECTORS'] as $sName => &$aButton) {
            $aButton['data'] = unserialize($aButton['data']);

            // Loop on the all position to identify where the current button is used in order to create the button hook list
            foreach ($aHooks as $sKey => $aValues) {
                if (!empty($aValues['connectors']) && is_string($aValues['connectors'])) {
                    $aValues['connectors'] = unserialize($aValues['connectors']);

                    foreach ($aValues['connectors'] as $sConnectorName => $sLabel) {
                        if ($sConnectorName == $aButton['cssClass']) {
                            $aButton['hooks'][] = BT_FPCModuleTools::getPositionName($aValues['name']);
                        }
                    }
                }
            }
            $aConnectors[] = $aButton;
        }

        if (FacebookPsConnect::$sQueryMode == 'xhr') {
            // clean header
            @ob_end_clean();
        }

        // unserialize connector data
        BT_FPCModuleTools::getConnectorData(true);

        // set smarty variables
        $aAssign = array(
            'aConnectors' => $aConnectors,
            'iDefaultLang' => intval(FacebookPsConnect::$iCurrentLang),
            'aDefaultLang' => Language::getLanguage(intval(FacebookPsConnect::$iCurrentLang)),
            'bVersion15_16' => FacebookPsConnect::$bCompare16,
            'sButtonStyle' => FacebookPsConnect::$conf['FBPSC_BUTTON_STYLE'],

        );

        return array('tpl' => _FPC_TPL_ADMIN_PATH . _FPC_TPL_CONNECTOR_SETTINGS, 'assign' => $aAssign);
    }


    /**
     * displays connector form
     *
     * @param array $aPost
     * @return array
     */
    private function displayConnectorForm(array $aPost)
    {
        // set
        $aAssign = array();

        // clean header
        @ob_end_clean();

        // get connector id
        $iConnectorId = Tools::getValue('iConnectorId');

        // use case - only configure with good connector id
        if ($iConnectorId && array_key_exists($iConnectorId, $GLOBALS['FBPSC_CONNECTORS'])) {
            // get unserialized connector data
            BT_FPCModuleTools::unserializeData($iConnectorId, 'connector');

            // set smarty variables
            $aAssign = array(
                'iConnectorId' => $iConnectorId,
                'iDefaultLang' => intval(FacebookPsConnect::$iCurrentLang),
                'aDefaultLang' => Language::getLanguage(intval(FacebookPsConnect::$iCurrentLang)),
                'aConnector' => $GLOBALS['FBPSC_CONNECTORS'][$iConnectorId],
                'sCbkUri' => BT_FPCModuleTools::detectHttpUri(_FPC_MODULE_URL . $iConnectorId . '-callback.php',
                    Configuration::get('PS_SHOP_DOMAIN')),
                'iTestCurlSsl' => FacebookPsConnect::$conf['FBPSC_TEST_CURl_SSL'],
                'iApiRequestMethod' => FacebookPsConnect::$conf['FBPSC_API_REQUEST_METHOD'],
            );

            // set tpl to include
            $aAssign['sTplToInclude'] = BT_FPCModuleTools::getTemplatePath(_FPC_PATH_TPL_NAME . _FPC_TPL_ADMIN_PATH . _FPC_TPL_CONNECTOR_PATH . $aAssign['aConnector']['adminTpl']);

            // clean footer
            FacebookPsConnect::$sQueryMode = 'xhr';
        }

        return array('tpl' => _FPC_TPL_ADMIN_PATH . _FPC_TPL_CONNECTOR_BODY, 'assign' => $aAssign);
    }

    /**
     * displays hooks list
     *
     * @param array $aPost
     * @return array
     */
    private function displayHooks(array $aPost)
    {
        if (FacebookPsConnect::$sQueryMode == 'xhr') {
            // clean header
            @ob_end_clean();
        }
        $bVersion15_16 = true;

        // unserialize hook data
        BT_FPCModuleTools::getHookData();

        // Use case to manage a title
        $aHook = BT_FPCModuleDao::getConnectorPositions('classical', $GLOBALS['FBPSC_OLD_CLASSICAL_POSITION']);

        foreach ($aHook as $sKey => $sValue) {
            $aHook[$sKey]['title'] = BT_FPCModuleTools::getPositionName($sValue['name']);
        }

        // set smarty variables
        $aAssign = array(
            'aHooksPosition' => $aHook,
            'iDefaultLang' => intval(FacebookPsConnect::$iCurrentLang),
            'bVersion15_16' => $bVersion15_16,
        );

        return array('tpl' => _FPC_TPL_ADMIN_PATH . _FPC_TPL_HOOK_SETTINGS, 'assign' => $aAssign);
    }

    /**
     * displays hook form
     *
     * @param array $aPost
     * @return array
     */
    private function displayHookForm(array $aPost)
    {
        // set
        $aAssign = array();

        // clean header
        @ob_end_clean();

        // get hook id
        $sHookId = Tools::getValue('sHookId');
        $aHookData = BT_FPCModuleDao::getConnectorPositions('classical', $GLOBALS['FBPSC_OLD_CLASSICAL_POSITION']);

        $aHookData[$sHookId]['hookTechnicalName'] = $aHookData[$sHookId]['name'];
        $aHookData[$sHookId]['name'] = BT_FPCModuleTools::getPositionName($aHookData[$sHookId]['name']);

        // set smarty variables
        $aAssign = array(
            'sHookId' => $sHookId,
            'iDefaultLang' => intval(FacebookPsConnect::$iCurrentLang),
            'bOneSet' => BT_FPCModuleTools::getConnectorData(false, true),
            'aHook' => $aHookData[$sHookId],
            'aConnectors' => $GLOBALS['FBPSC_CONNECTORS'],
            'bDisplayCustomText' => FacebookPsConnect::$conf['FBPSC_DISPLAY_CUSTOM_TEXT'],
        );

        // clean footer
        FacebookPsConnect::$sQueryMode = 'xhr';

        return array('tpl' => _FPC_TPL_ADMIN_PATH . _FPC_TPL_HOOK_FORM, 'assign' => $aAssign);
    }

    /**
     * displays hook advanced parameters
     *
     * @param array $aPost
     * @return array
     */
    private function displayHookAdvanced(array $aPost)
    {
        // To use DAO class
        require_once(_FPC_PATH_LIB . 'module-dao_class.php');

        $aPositionData = BT_FPCModuleDao::getConnectorPositions('advanced', null,
            $GLOBALS['FBPSC_OLD_CLASSICAL_POSITION']);

        //rebuild the data to get the simple name
        foreach ($aPositionData as $sKey => $aData) {
            $aPositionData[$sKey]['name'] = str_replace('_', ' ', $aData['name']);

            if ($aPositionData[$sKey]['data']['position'] == 'above') {
                $aPositionData[$sKey]['data']['position'] = isset($GLOBALS['FBPSC_DEFAULT_POSITION_ABOVE_TEXT'][FacebookPsConnect::$sCurrentLang]) ? $GLOBALS['FBPSC_DEFAULT_POSITION_ABOVE_TEXT'][FacebookPsConnect::$sCurrentLang] : $GLOBALS['FBPSC_DEFAULT_POSITION_ABOVE_TEXT']['en'];
            }
            if ($aPositionData[$sKey]['data']['position'] == 'below') {
                $aPositionData[$sKey]['data']['position'] = isset($GLOBALS['FBPSC_DEFAULT_POSITION_BELOW_TEXT'][FacebookPsConnect::$sCurrentLang]) ? $GLOBALS['FBPSC_DEFAULT_POSITION_BELOW_TEXT'][FacebookPsConnect::$sCurrentLang] : $GLOBALS['FBPSC_DEFAULT_POSITION_BELOW_TEXT']['en'];
            }
            // Keep legacy code in case we want to reintroduce the small buttons feature
            /* if ($aPositionData[$sKey]['data']['size'] == 'large') {
                $aPositionData[$sKey]['data']['size'] = isset($GLOBALS['FBPSC_DEFAULT_BUTTON_LARGE_TEXT'][FacebookPsConnect::$sCurrentLang]) ? $GLOBALS['FBPSC_DEFAULT_BUTTON_LARGE_TEXT'][FacebookPsConnect::$sCurrentLang] : $GLOBALS['FBPSC_DEFAULT_BUTTON_LARGE_TEXT']['en'];
            }
            if ($aPositionData[$sKey]['data']['size'] == 'small') {
                $aPositionData[$sKey]['data']['size'] = isset($GLOBALS['FBPSC_DEFAULT_BUTTON_SMALL_TEXT'][FacebookPsConnect::$sCurrentLang]) ? $GLOBALS['FBPSC_DEFAULT_BUTTON_SMALL_TEXT'][FacebookPsConnect::$sCurrentLang] : $GLOBALS['FBPSC_DEFAULT_BUTTON_SMALL_TEXT']['en'];
            }*/
            $aPositionData[$sKey]['data']['size'] = isset($GLOBALS['FBPSC_DEFAULT_BUTTON_LARGE_TEXT'][FacebookPsConnect::$sCurrentLang]) ? $GLOBALS['FBPSC_DEFAULT_BUTTON_LARGE_TEXT'][FacebookPsConnect::$sCurrentLang] : $GLOBALS['FBPSC_DEFAULT_BUTTON_LARGE_TEXT']['en'];

            $aPositionData[$sKey]['data']['page'] = isset($GLOBALS['FBPSC_PAGE_TEXT'][$aPositionData[$sKey]['data']['page']][FacebookPsConnect::$sCurrentLang]) ? $GLOBALS['FBPSC_PAGE_TEXT'][$aPositionData[$sKey]['data']['page']][FacebookPsConnect::$sCurrentLang] : $GLOBALS['FBPSC_PAGE_TEXT'][$aPositionData[$sKey]['data']['page']]['en'];
        }

        // set smarty variables
        $aAssign = array(
            'sBtnPosition' => FacebookPsConnect::$conf['FBPSC_ACC_BTN_POSITION'],
            'sBtnPosition17' => FacebookPsConnect::$conf['FBPSC_FUN_BTN_POSITION'],
            'aCustomPositionAdvanced' => $aPositionData,
            'bOnePageCheckOut' => Configuration::get('PS_ORDER_PROCESS_TYPE'),
            'sOpcPosition' => Configuration::get('FBPSC_OPC_POSITION'),
            'aCustomPosition' => $aPositionData,
        );

        return array('tpl' => _FPC_TPL_ADMIN_PATH . _FPC_TPL_HOOK_ADVANCED, 'assign' => $aAssign);
    }

    /**
     * displays result test Curl with Ssl
     * Set Global for Test the result Curl SSL
     *
     * @param array $aPost
     * @return array
     */
    private function displayTestSsl(array $aPost)
    {
        //set
        $aAssign = array();

        //clean header
        @ob_end_clean();

        //init curl connexion
        $ch = curl_init('https://google.fr');

        //transfer test
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        // exec curl
        curl_exec($ch);

        // error test and set error message
        if (curl_errno($ch) == 1) {
            $aAssign['iCurlSslCheck'] = false;
            Configuration::updateValue('FBPSC_TEST_CURl_SSL', 1);
        } else {
            $aAssign['iCurlSslCheck'] = true;
            Configuration::updateValue('FBPSC_TEST_CURl_SSL', 2);
            Configuration::updateValue('FBPSC_SSL_TEST_TODO', 0);
        }

        //close curl connexion
        curl_close($ch);

        //clean footer
        FacebookPsConnect::$sQueryMode = 'xhr';

        return array('tpl' => _FPC_TPL_ADMIN_PATH . _FPC_TPL_CURL_SSL, 'assign' => $aAssign);
    }

    /**
     * displays prerequisites check
     *
     * @param array $aPost
     * @return array
     */

    private function displayPrerequisitesCheck(array $aPost)
    {
        $aAssign = array(
            'sValidImgUrl' => _FPC_URL_IMG . 'admin/icon-valid.png',
            'sInvalidImgUrl' => _FPC_URL_IMG . 'admin/icon-invalid.png',
            'sCheckCurlInit' => BT_FPCWarning::create()->run('function', 'curl_init'),
            'sCheckAllowUrl' => BT_FPCWarning::create()->run('directive', 'allow_url_fopen'),
            'sCheckGroup' => BT_FPCWarning::create()->run('configuration', '_DEFAULT_CUSTOMER_GROUP'),
            'bDevMode' => _PS_MODE_DEV_,
        );

        return array('tpl' => _FPC_TPL_ADMIN_PATH . _FPC_TPL_PREREQUISITES_CHECK_SETTINGS, 'assign' => $aAssign);
    }

    /**
     * voucher settings
     *
     * @param array $aPost
     * @return array
     */
    private function displayVoucher(array $aPost)
    {
        $aAssign = array();

        if (FacebookPsConnect::$sQueryMode == 'xhr') {
            // clean header
            @ob_end_clean();
        }

        require_once(_FPC_PATH_LIB . 'module-dao_class.php');
        //for category tree
        $aCategories = Category::getCategories(intval(FacebookPsConnect::$iCurrentLang), false);
        $aIndexedCategories = BT_FPCModuleDao::getFpcCategories(FacebookPsConnect::$iShopId);
        $aStartCategories = current($aCategories);
        $aFirst = current($aStartCategories);
        $iStart = 1;

        $aAssign = array(
            'sEnableVouchers' => FacebookPsConnect::$conf['FBPSC_ENABLE_VOUCHER'],
            'sVoucherCodePrefix' => FacebookPsConnect::$conf['FBPSC_VOUCHER_CODE_PREFIX'],
            'sVoucherType' => FacebookPsConnect::$conf['FBPSC_VOUCHER_TYPE'],
            'sVoucherPercent' => FacebookPsConnect::$conf['FBPSC_VOUCHER_PERCENT'],
            'sVoucherAmount' => FacebookPsConnect::$conf['FBPSC_VOUCHER_AMOUNT'],
            'sVoucherTax' => FacebookPsConnect::$conf['FBPSC_VOUCHER_TAX'],
            'iVoucherCurrency' => FacebookPsConnect::$conf['FBPSC_VOUCHER_CURRENCY'],
            'aCurrencies' => Currency::getCurrencies(),
            'aVouchersDesc' => FacebookPsConnect::$conf['FBPSC_VOUCHER_DESC'],
            'aHomeCat' => BT_FPCModuleTools::recursiveCategoryTree($aCategories, array(),
                current(current($aCategories)), 1),
            'aFormatCat' => BT_FPCModuleTools::recursiveCategoryTree($aCategories, $aIndexedCategories, $aFirst,
                $iStart, null, true),
            'sVoucherMaxQty' => FacebookPsConnect::$conf['FBPSC_VOUCHER_MAX_QTY'],
            'sVoucherMinAmount' => FacebookPsConnect::$conf['FBPSC_VOUCHER_MIN_AMOUNT'],
            'iVoucherValidity' => FacebookPsConnect::$conf['FBPSC_VOUCHER_VALIDITY'],
            'sVoucherHighlight' => FacebookPsConnect::$conf['FBPSC_HIGHTLIGHT_VOUCHER'],
            'sVoucherCumulateOthers' => FacebookPsConnect::$conf['FBPSC_CUMULATE_OTHERS_VOUCHER'],
            'sVoucherCumulateDiscount' => FacebookPsConnect::$conf['FBPSC_CUMULATE_DISCOUNT_VOUCHER'],
            'sDisplayMode' => FacebookPsConnect::$conf['FBPSC_VOUCHER_DISPLAY_MODE'],
            'bCustomVoucerText' => FacebookPsConnect::$conf['FBPSC_ENABLE_CUSTOM_TXT'],
            'aDisplayText' => FacebookPsConnect::$conf['FBPSC_VOUCHER_DISP_TEXT'],
            'aLangs' => Language::getLanguages(),
            'sCurrentLang' => FacebookPsConnect::$iCurrentLang,
            'bDisplayCustomText' => FacebookPsConnect::$conf['FBPSC_DISPLAY_CUSTOM_TEXT'],
        );

        // use case - detect if home category name has been filled
        $aAssign['aVouchersDesc'] = BT_FPCModuleTools::getDefaultTranslations('FBPSC_VOUCHER_DESC',
            'DEFAULT_VOUCHER_DESC');
        $aAssign['aDisplayText'] = BT_FPCModuleTools::getDefaultTranslations('FBPSC_VOUCHER_DISP_TEXT',
            'DEFAULT_DISPLAY_TEXT');

        foreach ($aAssign['aLangs'] as $aLang) {
            if (!isset($aAssign['aVouchersDesc'][$aLang['id_lang']])) {
                $aAssign['aVouchersDesc'][$aLang['id_lang']] = $GLOBALS['FBPSC_DEFAULT_VOUCHER_DESC']['en'];
            }
            if (!isset($aAssign['aDisplayText'][$aLang['id_lang']])) {
                $aAssign['aDisplayText'][$aLang['id_lang']] = $GLOBALS['FBPSC_DEFAULT_DISPLAY_TEXT']['en'];
            }
        }

        return array(
            'tpl' => _FPC_TPL_ADMIN_PATH . _FPC_TPL_VOUCHER_SETTINGS,
            'assign' => $aAssign,
        );
    }

    /**
     * dashboard settings
     *
     * @param array $aPost
     * @return array
     */
    private function displayDashboard(array $aPost)
    {

        $aAssign = array(
            'aConnectorsDash' => $GLOBALS['FBPSC_CONNECTORS_LIST'],
            'sCurrentSocialFilter' => !empty($sSocial) ? Tools::ucfirst($sSocial) : '',
        );

        if (FacebookPsConnect::$sQueryMode == 'xhr') {
            // clean header
            @ob_end_clean();
        }

        return array(
            'tpl' => _FPC_TPL_ADMIN_PATH . _FPC_TPL_DASHBOARD,
            'assign' => $aAssign,
        );
    }


    /**
     * displays snippets settings
     *
     * @param array $aPost
     * @return array
     */
    private function displayConnectPositionForm(array $aPost)
    {
        @ob_end_clean();

        require_once(_FPC_PATH_LIB . 'module-dao_class.php');

        $iPositionId = Tools::getValue('iPositionId');

        $aCustomPosition = !empty($iPositionId) ? BT_FPCModuleDao::getConnectorPositionDataById($iPositionId) : '';

        $aData = unserialize($aCustomPosition['data']);

        // set smarty variables
        $aAssign = array(
            'iPositionId' => $aCustomPosition['id'],
            'sPositionStatus' => $aCustomPosition['status'],
            'sPositionName' => str_replace('_', ' ', $aCustomPosition['name']),
            'pages' => $GLOBALS['FBPSC_CONNECTOR_PAGE'],
            'positions' => $GLOBALS['FBPSC_BUTTON_POSITION'],
            // 'sizes' => $GLOBALS['FBPSC_BUTTON_SIZE'],
            'aPositionData' => unserialize($aCustomPosition['data']),
            'sButtonStyle' => $aCustomPosition['style'],
            'aConnectors' => $GLOBALS['FBPSC_CONNECTORS'],
            'aConnectorsActive' => unserialize($aData['connectors']),
            'aConnectorsCss' => unserialize($aData['css']),
            'bDisplayCustomText' => FacebookPsConnect::$conf['FBPSC_DISPLAY_CUSTOM_TEXT'],
        );

        // force xhr mode
        FacebookPsConnect::$sQueryMode = 'xhr';

        return array(
            'tpl' => _FPC_TPL_ADMIN_PATH . _FPC_TPL_CONNECTION_POSITION_SETTINGS,
            'assign' => $aAssign,
        );
    }

    /**
     * displays snippets settings
     *
     * @param array $aPost
     * @return array
     */
    private function displayShortCode(array $aPost)
    {

        $aAssign = array();

        // To use DAO class
        require_once(_FPC_PATH_LIB . 'module-dao_class.php');

        $aPositionData = BT_FPCModuleDao::getConnectorPositions('shortcode');

        foreach ($aPositionData as $sKey => $aData) {
            if ($aPositionData[$sKey]['data']['position'] == 'above') {
                $aPositionData[$sKey]['data']['position'] = isset($GLOBALS['FBPSC_DEFAULT_POSITION_ABOVE_TEXT'][FacebookPsConnect::$sCurrentLang]) ? $GLOBALS['FBPSC_DEFAULT_POSITION_ABOVE_TEXT'][FacebookPsConnect::$sCurrentLang] : $GLOBALS['FBPSC_DEFAULT_POSITION_ABOVE_TEXT']['en'];
            }
            if ($aPositionData[$sKey]['data']['position'] == 'below') {
                $aPositionData[$sKey]['data']['position'] = isset($GLOBALS['FBPSC_DEFAULT_POSITION_BELOW_TEXT'][FacebookPsConnect::$sCurrentLang]) ? $GLOBALS['FBPSC_DEFAULT_POSITION_BELOW_TEXT'][FacebookPsConnect::$sCurrentLang] : $GLOBALS['FBPSC_DEFAULT_POSITION_BELOW_TEXT']['en'];
            }
            if ($aPositionData[$sKey]['data']['size'] == 'large') {
                $aPositionData[$sKey]['data']['size'] = isset($GLOBALS['FBPSC_DEFAULT_BUTTON_LARGE_TEXT'][FacebookPsConnect::$sCurrentLang]) ? $GLOBALS['FBPSC_DEFAULT_BUTTON_LARGE_TEXT'][FacebookPsConnect::$sCurrentLang] : $GLOBALS['FBPSC_DEFAULT_BUTTON_LARGE_TEXT']['en'];
            }
            if ($aPositionData[$sKey]['data']['size'] == 'small') {
                $aPositionData[$sKey]['data']['size'] = isset($GLOBALS['FBPSC_DEFAULT_BUTTON_SMALL_TEXT'][FacebookPsConnect::$sCurrentLang]) ? $GLOBALS['FBPSC_DEFAULT_BUTTON_SMALL_TEXT'][FacebookPsConnect::$sCurrentLang] : $GLOBALS['FBPSC_DEFAULT_BUTTON_SMALL_TEXT']['en'];
            }
        }

        // set smarty variables
        $aAssign = array(
            'aShortCode' => $aPositionData,
        );

        if (FacebookPsConnect::$sQueryMode == 'xhr') {
            // clean header
            @ob_end_clean();
        }

        return array(
            'tpl' => _FPC_TPL_ADMIN_PATH . _FPC_TPL_SHORT_CODE,
            'assign' => $aAssign,
        );
    }

    /**
     * displays snippets settings
     *
     * @param array $aPost
     * @return array
     */
    private function displayShortCodeForm(array $aPost)
    {
        // clean header
        @ob_end_clean();

        //Build the according to the button used on the main inteface
        $iPositionId = Tools::getValue('iPositionId');

        $aCustomPosition = !empty($iPositionId) ? BT_FPCModuleDao::getConnectorPositionDataById($iPositionId) : '';
        $aData = unserialize($aCustomPosition['data']);

        //rebuild the data to get the simple name
        $aAssign = array(
            'iPositionId' => $iPositionId,
            'sPositionName' => str_replace('_', ' ', $aCustomPosition['name']),
            // 'sizes' => $GLOBALS['FBPSC_BUTTON_SIZE'],
            'aPositionData' => $aData,
            'aConnectors' => $GLOBALS['FBPSC_CONNECTORS'],
            'aConnectorsActive' => $aData['connectors'],
            'aConnectorsCss' => unserialize($aData['css']),
            'sJsCode' => strip_tags($aData['js']),
            'bDisplayCustomText' => FacebookPsConnect::$conf['FBPSC_DISPLAY_CUSTOM_TEXT'],
        );

        // force xhr mode
        FacebookPsConnect::$sQueryMode = 'xhr';

        return array(
            'tpl' => _FPC_TPL_ADMIN_PATH . _FPC_TPL_SHORT_CODE_FORM,
            'assign' => $aAssign,
        );
    }


    /**
     * displays snippets settings
     *
     * @param array $aPost
     * @return array
     */
    private function displayCssCode(array $aPost)
    {

        // Use case to manage a title
        $aHook = BT_FPCModuleDao::getConnectorPositions('classical', $GLOBALS['FBPSC_OLD_CLASSICAL_POSITION']);

        foreach ($aHook as $sKey => $sValue) {
            $aHook[$sKey]['title'] = BT_FPCModuleTools::getPositionName($sValue['name']);
        }

        $aAssign = array(
            'aHooks' => $aHook,
        );

        return array('tpl' => _FPC_TPL_ADMIN_PATH . _FPC_TPL_CSS_CODE, 'assign' => $aAssign);
    }

    /**
     * displays snippets settings
     *
     * @param array $aPost
     * @return array
     */
    private function displayCssForm(array $aPost)
    {
        // clean header
        @ob_end_clean();

        $aHookData = BT_FPCModuleDao::getConnectorPositions('classical', $GLOBALS['FBPSC_OLD_CLASSICAL_POSITION']);

        $aCss = unserialize($aHookData[$aPost['sHookId']]['data']['css']);

        $aAssign = array(
            'aCssDefault' => $GLOBALS['FBPSC_POSITION_' . strtoupper($aPost['sHookId']) . '_CSS'],
            'sHook' => BT_FPCModuleTools::getPositionName($aHookData[$aPost['sHookId']]['name']),
            'sHookId' => $aHookData[$aPost['sHookId']]['id'],
            'bDisplayBlock' => $aCss['activate'],
            'sCssBorderColor' => $aCss['border_color'],
            'sCssBorderSize' => $aCss['border_size'],
            'sCssBackgroundColor' => $aCss['background_color'],
            'sCssTextColor' => $aCss['text_color'],
            'sCssTextSize' => $aCss['text_size'],
            'sCssPaddingTop' => $aCss['padding_top'],
            'sCssPaddingRight' => $aCss['padding_right'],
            'sCssPaddingBottom' => $aCss['padding_bottom'],
            'sCssPaddingLeft' => $aCss['padding_left'],
            'sCssMarginTop' => $aCss['margin_top'],
            'sCssMarginRight' => $aCss['margin_right'],
            'sCssMarginBottom' => $aCss['margin_bottom'],
            'sCssMarginLeft' => $aCss['margin_left'],
            'bDisplayCustomText' => FacebookPsConnect::$conf['FBPSC_DISPLAY_CUSTOM_TEXT'],
        );

        // force xhr mode
        FacebookPsConnect::$sQueryMode = 'xhr';

        return array('tpl' => _FPC_TPL_ADMIN_PATH . _FPC_TPL_CSS_FORM_CODE, 'assign' => $aAssign);
    }

    /**
     * set singleton
     *
     * @return obj
     */
    public static function create()
    {
        static $oDisplay;

        if (null === $oDisplay) {
            $oDisplay = new BT_AdminDisplay();
        }
        return $oDisplay;
    }
}
