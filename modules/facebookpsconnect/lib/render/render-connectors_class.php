<?php
/**
 * Social Login
 *
 * @author    BusinessTech.fr - https://www.businesstech.fr
 * @copyright Business Tech 2020 - https://www.businesstech.fr
 * @license   Commercial
 *
 *           ____    _______
 *          |  _ \  |__   __|
 *          | |_) |    | |
 *          |  _ <     | |
 *          | |_) |    | |
 *          |____/     |_|
 */

class RenderConnectors
{
    /**
     * @var int $aData : define array of data
     */
    private $aData = array();

    /**
     *
     */
    public function __construct($aParams = array())
    {
        require_once(_FPC_PATH_LIB . 'module-dao_class.php');

        $this->checkData($aParams);
    }

    /**
     * check the data from database for a type of position
     *
     * @param array $aParam
     * @return array
     */
    public static function checkData($aParams)
    {
        $counter = 0;
        $aData = array();

        foreach ($aParams as $sKey => $aCurrentPositionData) {
            //Build the data
            $aData[$counter]['id'] = $aCurrentPositionData['id'];
            $aData[$counter]['name'] = $aCurrentPositionData['name'];
            $aData[$counter]['type'] = $aCurrentPositionData['type'];
            $aData[$counter]['status'] = $aCurrentPositionData['status'];
            $aData[$counter]['page'] = $aCurrentPositionData['data']['page'];
            $aData[$counter]['position'] = $aCurrentPositionData['data']['position'];

            // USE case for HTML element
            if (!empty($aCurrentPositionData['data']['html'])) {
                $aData[$counter]['html'] = $aCurrentPositionData['data']['html'];
            }

            $aData[$counter]['style'] = $aCurrentPositionData['data']['style'];
            $aData[$counter]['size'] = $aCurrentPositionData['data']['size'];
            $aData[$counter]['connectors'] = $aCurrentPositionData['data']['connectors'];
            $aData[$counter]['css'] = $aCurrentPositionData['data']['css'];
            $aData[$counter]['js'] = $aCurrentPositionData['data']['js'];

            $counter++;
        }

        return $aData;
    }

    /**
     * generate the css file
     *
     * @param array $aData
     * @param array $sModulePrefix
     * @return string
     */
    public static function extractCss($aData, $sModulePrefix)
    {
        $sCssContent = '';

        if (!empty($aData['css']) && is_string($aData['css'])) {
            $aDataCss = unserialize($aData['css']);

            if (!empty($aDataCss)
                && is_array($aDataCss)) {
                //Get values
                $sTextColor = !empty($aDataCss['text_color']) ? $aDataCss['text_color'] : '#333';
                $sFontSize = !empty($aDataCss['text_size']) ? $aDataCss['text_size'] . 'px' : '15px';
                $sBackgroundColor = !empty($aDataCss['background_color']) ? $aDataCss['background_color'] : '#f3f3f3';
                $sBorderSize = !empty($aDataCss['border_size']) ? $aDataCss['border_size'] . 'px' : '2px';
                $sBorderColor = !empty($aDataCss['border_color']) ? $aDataCss['border_color'] : '#f3f3f3';

                //Add the css content
                $sCssContent .= '#' . $sModulePrefix . '.' . $aData['name'] . '{';
                $sCssContent .= 'display: inline-block;';
                $sCssContent .= 'z-index: 999999;';
                $sCssContent .= 'text-align: center !important;';
                $sCssContent .= 'width: 100%;';
                $sCssContent .= 'padding:' . $aDataCss['padding_top'] . 'px ' . $aDataCss['padding_right'] . 'px ' . $aDataCss['padding_bottom'] . 'px ' . $aDataCss['padding_left'] . 'px;';
                $sCssContent .= 'margin:' . $aDataCss['margin_top'] . 'px ' . $aDataCss['margin_right'] . 'px ' . $aDataCss['margin_bottom'] . 'px ' . $aDataCss['margin_left'] . 'px;';
                $sCssContent .= 'color:' . $sTextColor . ';';
                $sCssContent .= 'font-size:' . $sFontSize . ';';
                $sCssContent .= 'background:' . $sBackgroundColor . ';';
                $sCssContent .= 'border:' . $sBorderSize . ' solid ' . $sBorderColor . ';';
                $sCssContent .= '}' . "\n";

                unset($sTextColor);
                unset($sFontSize);
                unset($sBackgroundColor);
                unset($sBorderSize);
                unset($sBorderColor);
            }
        }

        return $sCssContent;

    }

    /**
     * write the css file
     *
     * @param array $aData
     * @param string $sFilename
     * @param string path
     * @param bool $bMinify
     * @param string $sExtension
     * @return bool
     */
    public static function writeCssFile($aData, $sFilename, $sPath, $bMinify = false, $sExtension = '.css')
    {
        $sFile = _PS_MODULE_DIR_ . $sPath . $sFilename . $sExtension;

        try {
            if (!file_put_contents($sFile, $aData)) {
                throw new Exception(FacebookPsConnect::$oModule->l('Error during the css file write',
                        'admin-update_class') . '.', 111);
            }
        } catch (Exception $e) {
            $aData['aErrors'][] = array('msg' => $e->getMessage(), 'code' => $e->getCode());
        }
    }


    /**
     * get the css file
     *
     * @param string $sFilename
     * @param string path
     * @param bool $bMinify
     * @param string $sExtension
     * @return html
     */
    public static function getCssFile($sFilename, $sPath, $bMinify = false, $sExtension = '.css')
    {
        // Build the css file path
        $sCssFile = $sPath . $sFilename . $sExtension;

        if (!empty($sCssFile)) {
            Context::getContext()->controller->addCSS($sCssFile);
        }
    }

    /**
     * return the HTML content for the hook
     * @param array $aData
     * @param string $sPositionName
     * @param string $sType
     * @param string $sBackUri
     * @param string $sModuleUri
     * @param bool $bJs
     * @return string
     */
    public static function display($aData, $sPositionName = null, $sType = null, $sBackUri, $sModuleUri, $bJs = true)
    {
        $sContent = '';
        $sBtnDivHtml = '';

        if (!empty($aData)
            && is_array($aData)
            && !empty($aData['status'])
            && (!empty($sPositionName)
            || !empty($sType))
        ) {
            $bDisplay = true;

            // Use for dynamic position to only apply the render on the good page
            $bSpecificPage = false;

            // Use case - we check the position name if is set in DB and if is different we don't display anything
            if ($sPositionName !== null) {
                if (!isset($aData['name'])
                    || (isset($aData['name'])
                    && $aData['name'] != $sPositionName)
                ) {
                    $bDisplay = false;
                }
            }

            // Use case - we check the type name  if is set in DB and if is different we don't display anything
            if ($sType !== null) {
                if (!isset($aData['type'])
                    || (isset($aData['type'])
                    && $aData['type'] != $sType)
                ) {
                    $bDisplay = false;
                }

                // Use case to use the connectors only on the good page and special case for block user
                if ($aData['page'] != BT_FPCModuleTools::detectCurrentPage()
                    && $aData['page'] != 'all'
                ) {
                    $bDisplay = false;
                }
            }

            if (!empty($bDisplay)) {
                //Use case for the position
                $aConnectionText = BT_FPCModuleTools::getDefaultTranslations('FBPSC_CONNECT_TEXT',
                    'DEFAULT_CONNECTION_TEXT');
                $iLangId = Context::getContext()->cookie->id_lang;
                $aButtons = (is_string($aData['connectors'])) ? unserialize($aData['connectors']) : $aData['connectors'];
                $aConnectors = $GLOBALS['FBPSC_CONNECTORS'];
                $bEnableVouchers = FacebookPsConnect::$conf['FBPSC_ENABLE_VOUCHER'];
                $bEnableCustomVoucherText = FacebookPsConnect::$conf['FBPSC_ENABLE_CUSTOM_TXT'];
                $aVoucherCustomText = BT_FPCModuleTools::getDefaultTranslations('FBPSC_VOUCHER_DISP_TEXT',
                    'DEFAULT_DISPLAY_TEXT');

                //Generate the $sBtnDivHtml content
                if (!empty($aButtons)
                    && !empty($aData['name'])
                    && !empty($iLangId)
                ) {
                    // Append a specific class if we are displaying the buttons with an advanced position
                    $sContainerClasses = (String)$aData['name'];
                    if ($sType == 'advanced') {
                        $sContainerClasses .= ' advanced';
                    }

                    $sBtnDivHtml .= '<div id="fbpsc" class="' . $sContainerClasses . '">';

                    $bDisplayText = unserialize($aData['css'])['activate'];

                    if (!empty($bDisplayText)) {

                        if(!empty(FacebookPsConnect::$conf['FBPSC_DISPLAY_CUSTOM_TEXT'])) {
                            $sBtnDivHtml .= $aConnectionText[$iLangId];
                        }

                        // Use case for voucher display text
                        if (!empty($bEnableVouchers)
                            && !empty($bEnableCustomVoucherText)
                            && !empty($aVoucherCustomText)
                        ) {
                            $sBtnDivHtml .= ' ' . $aVoucherCustomText[$iLangId];
                        }

                        $sBtnDivHtml .= '<div class="clr_10"></div>';
                    }
                    unset($bDisplayText);

                    // Use case for the buttons display and test if we have connector
                    if (!empty($aButtons)) {
                        // Loop on connectors configured for the current position
                        foreach ($aButtons as $sKey => $sValue) {
                            // Use case for active connector
                            if (isset($aConnectors[$sKey]['data']['activeConnector'])
                                && $aConnectors[$sKey]['data']['activeConnector'] == true
                                && !empty($sBackUri)
                                && !empty($sModuleUri)
                            ) {
                                $sBtnDivHtml .= self::getButtonStyle($sKey, $aData['size'], $sBackUri, $sModuleUri,
                                    $bJs);
                            }
                        }
                    }

                    $sBtnDivHtml .= '</div>';

                    // Use case : classical and advanced position
                    if (!empty($bJs)) {
                        $sPosition = $aData['position'] == 'below' ? 'append' : 'prepend';
                        $sHtmlElement = (!empty($aData['html'])) ? $aData['html'] : 'body';

                        //Use case for order funnel
                        if ($aData['name'] == 'orderFunnel') {
                            $sPosition = FacebookPsConnect::$conf['FBPSC_FUN_BTN_POSITION'] == 'below' ? 'append' : 'prepend';

                            //Use case if the element doesn't exist
                            if (empty($aData['html'])) {
                                $sHtmlElement = '#checkout-personal-information-step';
                            }
                        }

                        //Get the rendering data
                        $sContent = '<script type="text/javascript">' . "\n";
                        $sContent .= '$(\'' . $sHtmlElement . '\').' . $sPosition . '(\'' . $sBtnDivHtml . '\');' . "\n";
                        $sContent .= '</script>' . "\n";
                    } // Use case : shortcode
                    else {
                        $sContent = $sBtnDivHtml;
                    }
                }
            }
        }

        return $sContent;
    }

    /**
     * return the HTML to manage the good button style and size
     *
     * @param string $sConnectorName
     * @param string $sButtonSize
     * @param string $sBackUri
     * @param string $sModuleUri
     * @param bool $bUseJs
     * @return string
     */
    private static function getButtonStyle($sConnectorName, $sButtonSize, $sBackUri, $sModuleUri, $bUseJs = true)
    {
        $sHtmlBtnStyle = '';
        $sButtonStyle = FacebookPsConnect::$conf['FBPSC_BUTTON_STYLE'];
        // Retrieve current iso code from context, or use English as fallback
        $sCurrentIsoLang = Context::getContext()->language->iso_code;
        if (empty($sCurrentIsoLang)) {
            $sCurrentIsoLang = 'en';
        }

        if (!empty($sConnectorName)
            && !empty($sButtonSize)
        ) {
            $sHtmlBtnStyle = empty(FacebookPsConnect::$bCompare15) && (!empty(FacebookPsConnect::$bCompare16) || !empty(FacebookPsConnect::$bCompare17)) ? '<a ' : '<p ';

            // Use case to add the href + onclick
            $sUrl = urldecode($sModuleUri . '?sAction=connect&sType=plugin&connector=' . $sConnectorName . '&back=' . $sBackUri);

            // If we aren't in shortcode
            if (!empty($bUseJs)) {
                $sHtmlBtnStyle .= addslashes('href="javascript:void(0)" onclick="javascript:popupWin = window.open(\'' . $sUrl . '\', \'login\', \'location,width=600,height=600,top=0\');popupWin.focus();" ');
            } else {
                $sHtmlBtnStyle .= 'href="javascript:void(0)" onclick="javascript:popupWin = window.open(\'' . $sUrl . '\', \'login\', \'location,width=600,height=600,top=0\');popupWin.focus();" ';
            }

            //Start the Html class attribute
            $sHtmlBtnStyle .= 'class="';

            // Use case on the style of the button
            if ($sButtonStyle == 'classical') {
                $sHtmlBtnStyle .= 'ao_bt_fpsc ao_bt_fpsc_' . $sConnectorName;
            } // Use case for modern and large button. Facebook has to be enforced as a large button due to their guidelines
            elseif ($sButtonStyle == 'modern'
                && ($sButtonSize == 'large' || in_array($sConnectorName, array('google', 'facebook')))
            ) {
                $sHtmlBtnStyle .= 'btn-connect btn-block-connect btn-social';
            } // Use case for modern and small button
            elseif ($sButtonStyle == 'modern'
                && $sButtonSize == 'small'
            ) {
                $sHtmlBtnStyle .= 'btn';
            }

            // Use case for 1.5 and the boostrap CSS plugin
            if (!empty(FacebookPsConnect::$bCompare15) && !in_array($sConnectorName, array('facebook', 'google'))) {
                $sHtmlBtnStyle .= ' btn-mini-fbpsc';
            }

            $sHtmlBtnStyle .= ' btn-' . $sConnectorName . '">';

            // Due to Facebook guidelines, the FA icon might differ from the other ones
            if ($sConnectorName == 'facebook') {
                $sHtmlBtnStyle .= '<span class="fa fa-' . $sConnectorName . '-square"></span>';
            } elseif ($sConnectorName == 'google') {
                $sHtmlBtnStyle .= '<span class="btn-google-icon"></span>';
            } else {
                $sHtmlBtnStyle .= '<span class="fa fa-' . $sConnectorName . '"></span>';
            }

            // If we have a specific label for this button in the current lang, use it
            // Otherwise, use the connector name as the fallback value
            if (isset($GLOBALS['FBPSC_BUTTONS_LABEL'][$sConnectorName][$sCurrentIsoLang])) {
                $sButtonLabel = $GLOBALS['FBPSC_BUTTONS_LABEL'][$sConnectorName][$sCurrentIsoLang];
            } else {
                $sButtonLabel = $GLOBALS['FBPSC_BUTTONS_LABEL'][$sConnectorName]['en'];
            }

            if ($sButtonStyle == 'modern'
                && ($sButtonSize == 'large' || in_array($sConnectorName, array('google', 'facebook')))
            ) {
                $sHtmlBtnStyle .= '<span class="btn-title-connect">' . $sButtonLabel . '</span> ';
            }

            $sHtmlBtnStyle .= empty(FacebookPsConnect::$bCompare15) && (!empty(FacebookPsConnect::$bCompare16) || !empty(FacebookPsConnect::$bCompare17)) ? '</a>' : '</p>';
        }

        return $sHtmlBtnStyle;
    }
}
