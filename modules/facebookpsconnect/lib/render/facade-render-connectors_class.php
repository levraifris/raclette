<?php
/**
 * Social Login
 *
 * @author    BusinessTech.fr - https://www.businesstech.fr
 * @copyright Business Tech 2020 - https://www.businesstech.fr
 * @license   Commercial
 *
 *           ____    _______
 *          |  _ \  |__   __|
 *          | |_) |    | |
 *          |  _ <     | |
 *          | |_) |    | |
 *          |____/     |_|
 */

class FacadeRenderConnectors
{
    /**
     *  loop on connectors
     *
     * @param array $aBtnPosition
     * @param string $sPosition
     * @param string $sAction
     * @param string $sType
     * @param string $sCurrentUri
     * @param string $sModuleUri
     * @param bool $bLogged
     * @param bool $bJs -> by default true because we return the HTML into the JS code except for shortcode
     * @return mixed
     */
    public static function loopConnector(
        $aBtnPosition,
        $sAction = 'render',
        $sPosition = null,
        $sType = null,
        $sCurrentUri = null,
        $sModuleUri = null,
        $bLogged = false,
        $bJs = true
    ) {
        require_once(_FPC_PATH_LIB_RENDER . 'iterator-render-connectors_class.php');

        $mContent = '';

        if (empty($bLogged)) {
            $oRenderIterator = new IteratorRenderConnector($aBtnPosition);

            $mContent = self::{$sAction}($oRenderIterator, $sPosition, $sType, $sCurrentUri, $sModuleUri, $bJs);
        }

        return $mContent;
    }


    /**
     *  render connector positions
     *
     * @param object $oRender
     * @param string $sPosition
     * @param string $sAction
     * @param string $sType
     * @param string $sCurrentUri
     * @param string $sModuleUri
     * @param bool $bJs
     * @return mixed
     */
    private static function render(
        IteratorRenderConnector $oRender,
        $sPosition = null,
        $sType = null,
        $sCurrentUri = null,
        $sModuleUri = null,
        $bJs = true
    ) {
        $sContent = '';

        foreach ($oRender as $sKey => $aValue) {
            $sContent .= RenderConnectors::display($aValue, $sPosition, $sType, $sCurrentUri, $sModuleUri, $bJs);
        }

        return $sContent;
    }


    /**
     *  extract
     *
     * @param object $oRender
     * @param string $sPosition
     * @param string $sAction
     * @param string $sType
     * @param string $sCurrentUri
     * @param string $sModuleUri
     * @param bool $bJs
     * @return mixed
     */
    private static function extract(
        IteratorRenderConnector $oRender,
        $sPosition = null,
        $sType = null,
        $sCurrentUri = null,
        $sModuleUri = null,
        $bJs = true
    ) {
        $aContent = array();

        foreach ($oRender as $sKey => $aValue) {
            $aContent[] = $aValue;
        }

        return $aContent;
    }
}