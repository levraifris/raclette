<?php
/**
 * Social Login
 *
 * @author    BusinessTech.fr - https://www.businesstech.fr
 * @copyright Business Tech 2020 - https://www.businesstech.fr
 * @license   Commercial
 *
 *           ____    _______
 *          |  _ \  |__   __|
 *          | |_) |    | |
 *          |  _ <     | |
 *          | |_) |    | |
 *          |____/     |_|
 */

class IteratorRenderConnector implements Iterator
{
    /**
     * @var int $iPosition : define the initial Position
     */
    private $iPosition = 0;

    /**
     * @var int $aData : define array of data
     */
    private $aData = array();


    /**
     *
     */
    public function __construct($aData = array())
    {
        require_once(_FPC_PATH_LIB_RENDER . 'render-connectors_class.php');

        if (!empty($aData)) {
            $aDataFormatted = RenderConnectors::checkData($aData);
        }

        if (!empty($aDataFormatted)) {
            $this->aData = $aDataFormatted;
            $this->position = 0;
        }
    }

    /**
     * restart iterator
     */
    public function rewind()
    {
        $this->iPosition = 0;
    }

    /**
     * get current position
     */
    public function current()
    {
        return $this->aData[$this->iPosition];
    }

    /**
     * get current key
     */
    public function key()
    {
        return $this->iPosition;
    }

    /**
     * get next element
     */
    public function next()
    {
        ++$this->iPosition;
    }

    /**
     * check if the current position is valid
     */
    public function valid()
    {
        return isset($this->aData[$this->iPosition]);
    }
}
