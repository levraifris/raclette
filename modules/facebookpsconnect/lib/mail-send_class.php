<?php
/**
 * Social Login
 *
 * @author    BusinessTech.fr - https://www.businesstech.fr
 * @copyright Business Tech 2020 - https://www.businesstech.fr
 * @license   Commercial
 *
 *           ____    _______
 *          |  _ \  |__   __|
 *          | |_) |    | |
 *          |  _ <     | |
 *          | |_) |    | |
 *          |____/     |_|
 */

class BT_FpcMailSend
{
    /**
     * @var bool $bProcess : define if process or not
     */
    protected $bProcess = null;

    /**
     * execute hook
     *
     * @param string $sType
     * @param array $aParams
     * @return bool
     */
    public function run($sType, array $aParams)
    {
        $bSend = false;
        $this->bProcess = false;

        switch ($sType) {
            case 'customerAccountNotification' : // use case - send a notification to the customer
                $aParams = $this->processCustomerNotification($aParams);
                break;
            case 'customerTwitterNotification' : // use case - send a notification to the customer
                $aParams = $this->processCustomerTwitterNotification($aParams);
                break;
            case 'customerAccountNotificationWithVoucher' : // use case - send a voucher notification to the customer
                $aParams = $this->processCustomerNotificationWithVoucher($aParams);
                break;
            default :
                break;
        }

        // use case - only if process true
        if ($this->bProcess) {

            $bSend = Mail::send($aParams['isoId'], $aParams['tpl'], $aParams['subject'], $aParams['vars'],
                $aParams['email'], null, null, null, null, null, $aParams['tpl_path']);
        }

        return $bSend;
    }


    /**
     * process data for sending an e-mail notification to the customer once his account is created
     *
     * @param array $aData
     * @return array
     */
    private function processCustomerNotification(array $aData)
    {
        $aParams = array();

        // set the path for default email folder
        $aParams['tpl_path'] = _FPC_PATH_MAILS;

        //manage the good template for email
        if (!empty(FacebookPsConnect::$bCompare15)
            && empty(FacebookPsConnect::$bCompare16)
            && empty(FacebookPsConnect::$bCompare17)) {
            $sTemplateVersion = _FPC_TPL_MAIL_NOTIF_15;
        } elseif (!empty(FacebookPsConnect::$bCompare15)
            && !empty(FacebookPsConnect::$bCompare16)
            && empty(FacebookPsConnect::$bCompare17)) {
            $sTemplateVersion = _FPC_TPL_MAIL_NOTIF_16;
        } elseif (!empty(FacebookPsConnect::$bCompare15)
            && !empty(FacebookPsConnect::$bCompare16)
            && !empty(FacebookPsConnect::$bCompare17)) {
            $sTemplateVersion = _FPC_TPL_MAIL_NOTIF_17;
        }

        // set the subject / email / lang ID
        $aParams['subject'] = FacebookPsConnect::$oModule->l('Your account has been created', 'mail-send_class');
        $aParams['email'] = $aData['email'];
        $aParams['isoId'] = !empty($aData['isoId']) ? $aData['isoId'] : Configuration::get('PS_LANG_DEFAULT');
        $aParams['tpl'] = $sTemplateVersion;
        $aParams['vars'] = array(
            '{email}' => $aData['email'],
            '{passwd}' => $aData['password'],
            '{firstname}' => $aData['firstname'],
            '{lastname}' => $aData['lastname'],
            '{social}' => $aData['social'],
        );

        $this->bProcess = true;

        return $aParams;
    }

    /**
     * process data for sending an e-mail notification to the customer once his account is created and send the voucher code
     *
     * @param array $aData
     * @return array
     */
    private function processCustomerNotificationWithVoucher(array $aData)
    {
        $aParams = array();

        // set the path for module email folder
        $aParams['tpl_path'] = _FPC_PATH_MAILS;

        //manage the good template for email
        if (!empty(FacebookPsConnect::$bCompare15)
            && empty(FacebookPsConnect::$bCompare16)
            && empty(FacebookPsConnect::$bCompare17)) {
            $sTemplateVersion = _FPC_TPL_MAIL_NOTIF_VOUCHER_15;
        } elseif (!empty(FacebookPsConnect::$bCompare15)
            && !empty(FacebookPsConnect::$bCompare16)
            && empty(FacebookPsConnect::$bCompare17)) {
            $sTemplateVersion = _FPC_TPL_MAIL_NOTIF_VOUCHER_16;
        } elseif (!empty(FacebookPsConnect::$bCompare15)
            && !empty(FacebookPsConnect::$bCompare16)
            && !empty(FacebookPsConnect::$bCompare17)) {
            $sTemplateVersion = _FPC_TPL_MAIL_NOTIF_VOUCHER_16;
        }

        // set the subject / email / lang ID
        $aParams['subject'] = FacebookPsConnect::$oModule->l('Your account has been created + discount code',
            'mail-send_class');
        $aParams['email'] = $aData['email'];
        $aParams['isoId'] = !empty($aData['isoId']) ? $aData['isoId'] : Configuration::get('PS_LANG_DEFAULT');
        $aParams['tpl'] = $sTemplateVersion;
        $aParams['vars'] = array(
            '{email}' => $aData['email'],
            '{passwd}' => $aData['password'],
            '{firstname}' => $aData['firstname'],
            '{lastname}' => $aData['lastname'],
            '{voucherCode}' => $aData['voucherCode'],
            '{validityShare}' => $aData['voucherUntilDate'],
            '{voucherInfoText}' => $aData['voucherInfoText'],
            '{social}' => $aData['social'],
        );

        $this->bProcess = true;

        return $aParams;
    }

    /**
     * send customer information after update with twitter connector
     *
     * @param array $aData
     * @return array
     */
    private function processCustomerTwitterNotification($aData)
    {

        // set the path for default email folder
        $aParams['tpl_path'] = _PS_MAIL_DIR_;

        // set the subject / email / lang ID
        $aParams['subject'] = FacebookPsConnect::$oModule->l('Your account has been updated', 'mail-send_class');
        $aParams['email'] = $aData['email'];
        $aParams['isoId'] = !empty($aData['isoId']) ? $aData['isoId'] : Configuration::get('PS_LANG_DEFAULT');
        $aParams['tpl'] = 'account';
        $aParams['vars'] = array(
            '{email}' => $aData['email'],
            '{passwd}' => $aData['password'],
            '{firstname}' => $aData['firstname'],
            '{lastname}' => $aData['lastname'],
        );

        $this->bProcess = true;

        return $aParams;
    }


    /**
     * set singleton
     *
     * @return obj
     */
    public static function create()
    {
        static $oMailSend;

        if (null === $oMailSend) {
            $oMailSend = new BT_FpcMailSend();
        }
        return $oMailSend;
    }
}