<?php
/**
 * Social Login
 *
 * @author    BusinessTech.fr - https://www.businesstech.fr
 * @copyright Business Tech 2020 - https://www.businesstech.fr
 * @license   Commercial
 *
 *           ____    _______
 *          |  _ \  |__   __|
 *          | |_) |    | |
 *          |  _ <     | |
 *          | |_) |    | |
 *          |____/     |_|
 */

class BT_FpcModuleUpdate
{
    /**
     * @var $aErrors : store errors
     */
    public $aErrors = array();


    /**
     * execute required function
     * @param $aParam
     * @since 3.0.14 Update button size to large for compliancy with almost all providers
     */
    public function run(array $aParam = null)
    {
        // get type
        $aParam['sType'] = empty($aParam['sType']) ? 'tables' : $aParam['sType'];

        switch ($aParam['sType']) {
            case 'tables': // use case - update tables
            case 'fields': // use case - update fields
            case 'hooks': // use case - update hooks
            case 'templates': // use case - update templates
            case 'position': // use case - update position
            case 'buttonsSize': // use case - update button size
                // execute match function
                call_user_func_array(array($this, 'update' . ucfirst($aParam['sType'])), array($aParam));
                break;
            default:
                break;
        }
    }

    /**
     * update tables if required
     *
     * @param array $aParam
     */
    private function updateTables(array $aParam)
    {
        // set transaction
        Db::getInstance()->Execute('BEGIN');

        if (!empty($GLOBALS['FBPSC_SQL_UPDATE']['table'])) {
            // loop on each elt to update SQL
            foreach ($GLOBALS['FBPSC_SQL_UPDATE']['table'] as $sTable => $sSqlFile) {
                // execute query
                $bResult = Db::getInstance()->ExecuteS('SHOW TABLES LIKE "' . _DB_PREFIX_ . bqSQL(strtolower(_FPC_MODULE_NAME)) . '_' . $sTable . '"');

                // if empty - update
                if (empty($bResult)) {
                    require_once(_FPC_PATH_CONF . 'install.conf.php');
                    require_once(_FPC_PATH_LIB_INSTALL . 'install-ctrl_class.php');

                    // use case - KO update
                    if (!BT_InstallCtrl::run('install', 'sql', _FPC_PATH_SQL . $sSqlFile)) {
                        $this->aErrors[] = array('table' => $sTable, 'file' => $sSqlFile);
                    }
                }
            }
        }

        if (empty($this->aErrors)) {
            Db::getInstance()->Execute('COMMIT');
        } else {
            Db::getInstance()->Execute('ROLLBACK');
        }
    }


    /**
     * update fields if required
     *
     * @param array $aParam
     */
    private function updateFields(array $aParam)
    {
        // set transaction
        Db::getInstance()->Execute('BEGIN');

        if (!empty($GLOBALS['FBPSC_SQL_UPDATE']['field'])) {
            // loop on each elt to update SQL
            foreach ($GLOBALS['FBPSC_SQL_UPDATE']['field'] as $sFieldName => $aOption) {
                // execute query
                $aResult = Db::getInstance()->ExecuteS('SHOW COLUMNS FROM ' . _DB_PREFIX_ . bqSQL(strtolower(_FPC_MODULE_NAME)) . '_' . $aOption['table'] . ' LIKE "' . $aOption['field'] . '"');

                // use case - column exists but we need to test and change the column's definition
                if (!empty($aResult)
                    && !empty($aOption['check'])
                    && !empty($aOption['type'])
                    && !empty($aOption['value'])
                ) {
                    if (!empty($aResult[0])) {
                        if ($aResult[0]['Type'] != $aOption['value']) {
                            $aResult = array();
                        }
                    }
                }

                // if empty - update
                if (empty($aResult)) {
                    require_once(_FPC_PATH_CONF . 'install.conf.php');
                    require_once(_FPC_PATH_LIB_INSTALL . 'install-ctrl_class.php');

                    // use case - KO update
                    if (!BT_InstallCtrl::run('install', 'sql', _FPC_PATH_SQL . $aOption['file'])) {
                        $aErrors[] = array(
                            'field' => $aOption['field'],
                            'linked' => $aOption['table'],
                            'file' => $aOption['file']
                        );
                    }
                }
            }
        }

        if (empty($this->aErrors)) {
            Db::getInstance()->Execute('COMMIT');
        } else {
            Db::getInstance()->Execute('ROLLBACK');
        }
    }

    /**
     * update hooks if required
     *
     * @category admin collection
     * @param array $aParam
     */
    private function updateHooks(array $aParam)
    {
        require_once(_FPC_PATH_CONF . 'install.conf.php');
        require_once(_FPC_PATH_LIB_INSTALL . 'install-ctrl_class.php');

        // use case - hook register ko
        if (!BT_InstallCtrl::run('install', 'config', array('bHookOnly' => true))) {
            $this->aErrors[] = array(
                'table' => 'ps_hook_module',
                'file' => FacebookPsConnect::$oModule->l('register hooks KO')
            );
        }
    }


    /**
     * update templates if required
     *
     * @param array $aParam
     */
    private function updateTemplates(array $aParam)
    {
        require_once(_FPC_PATH_LIB_COMMON . 'dir-reader.class.php');

        // get templates files
        $aTplFiles = BT_SLDirReader::create()->run(array(
            'path' => _FPC_PATH_TPL,
            'recursive' => true,
            'extension' => 'tpl',
            'subpath' => true
        ));

        if (!empty($aTplFiles)) {
            global $smarty;

            if (method_exists($smarty, 'clearCompiledTemplate')) {
                $smarty->clearCompiledTemplate();
            } elseif (method_exists($smarty, 'clear_compiled_tpl')) {
                foreach ($aTplFiles as $aFile) {
                    $smarty->clear_compiled_tpl($aFile['filename']);
                }
            }
        }
    }

    /**
     * Update classical position in the new table for the 3.0.x version if required
     *
     * @param array $aParam
     */
    private function updatePosition(array $aParam)
    {
        require_once(_FPC_PATH_LIB . 'module-dao_class.php');
        $aData = array();

        $aPositionData = BT_FPCModuleDao::getConnectorPositionData();

        if (empty($aPositionData)) {
            foreach ($GLOBALS['FBPSC_ZONE'] as $skey => $aPosition) {
                // Mange the css recovery for the classical position
                if (!empty($aPosition['use'])) {

                    $sCss = serialize(FacebookPsConnect::$conf['FBPSC_BADGE_' . strtoupper($skey) . '_CSS']);

                    // Init data
                    $sHtmlElement = '';
                    $sPage = '';
                    $sPosition = 'below';

                    // Use case for the htmlElement when a classical position become advance
                    if ($aPosition['name'] == 'authentication') {
                        $sHtmlElement = !empty(FacebookPsConnect::$bCompare17) ? '.login-form' : '.center_column';
                        $sPage = 'authentication';
                    } elseif ($aPosition['name'] == 'blockUser') {
                        $sHtmlElement = !empty(FacebookPsConnect::$bCompare17) ? '.search-widget' : '.shopping_cart';
                        $sPage = 'all';
                    } // Use case for the left right column
                    elseif ($aPosition['name'] == 'displayLeftColumn' || $aPosition['name'] == 'displayRightColumn') {
                        $sHtmlElement = '.displayColumn';
                    } // Use case for PS 1.7 + order funnel position
                    elseif (!empty(FacebookPsConnect::$bCompare17) && $aPosition['name'] == 'OrderFunnel') {
                        $sPage = 'cart';
                        $sHtmlElement = '#checkout-personal-information-step';
                        $sPosition = 'above';
                    } // Use case for the displayTop
                    elseif ($aPosition['name'] == 'displayTop') {
                        $sPosition = 'above';
                    }

                    $aData = array(
                        'page' => !empty($sPage) ? $sPage : 'other',
                        'position' => $sPosition,
                        'style' => 'modern',
                        'html' => $sHtmlElement,
                        // Keep legacy code for possible future reintegration
                        // 'size' => $aPosition['name'] == 'blockUser' ? 'small' : 'large',
                        'size' => 'large',
                        'connectors' => $aPosition['data'],
                        'css' => $sCss,
                        'js' => '',
                    );

                    // Use case to transform 2 positions in advanced positions
                    if ($aPosition['name'] == 'blockUser'
                        || $aPosition['name'] == 'authentication'
                        || $aPosition['name'] == 'orderFunnel'
                    ) {
                        $sType = 'advanced';
                    } else {
                        $sType = 'classical';
                    }
                    //Add the values for the new position system
                    BT_FPCModuleDao::addConnectorPosition($aPosition['name'], $sType, 1, FacebookPsConnect::$iShopId,
                        $aData);
                }

                // Use case fill out the css file on the first installation
                $aPositionData = BT_FPCModuleDao::getConnectorPositions();
                BT_FPCModuleTools::loopCssGenerate($aPositionData);
            }
        } else {
            $aPositionData = BT_FPCModuleDao::getConnectorPositions();
            BT_FPCModuleTools::loopCssGenerate($aPositionData);
        }
    }

    /**
     * Updates all the existing buttons sizes to "large"  in order to comply with most of the guidelines
     *
     * @since 3.0.14
     * @throws Exception In case we either can't fetch the connector position data, or can't update one of its rows
     * @return void
     */
    private function updateButtonsSize()
    {
        require_once(_FPC_PATH_LIB . 'module-dao_class.php');

        // Start by retrieving all the connector position we know of, to iterate on them
        $connectorPositionsData = BT_FPCModuleDao::getConnectorPositionData();
        if (empty($connectorPositionsData)) {
            throw new \Exception(FacebookPsConnect::$oModule->l(
                'An error occurred while fetching the connector position data for a buttons size update',
                'module-update_class'
            ) . '.', 200);
        }

        foreach ($connectorPositionsData as $connectorPosition) {
            // Start by decoding the stored data for this position
            $decodedPositionData = unserialize($connectorPosition['data']);
            // Force large position
            $decodedPositionData['size'] = 'large';

            // Update the DB row with the new size
            if (!BT_FPCModuleDao::updateHookData($decodedPositionData, (int)$connectorPosition['id'])) {
                throw new \Exception(FacebookPsConnect::$oModule->l(
                    'An error occurred while saving a button size forced update to large',
                    'module-update_class'
                ) . '.', 201);
            }
        }
    }

    /**
     * returns errors
     *
     * @return array
     */
    public function getErrors()
    {
        return $this->aErrors;
    }

    /**
     * manages singleton
     *
     * @return array
     */
    public static function create()
    {
        static $oModuleUpdate;

        if (null === $oModuleUpdate) {
            $oModuleUpdate = new BT_FpcModuleUpdate();
        }
        return $oModuleUpdate;
    }
}
