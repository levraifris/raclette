<?php
/**
 * Social Login
 *
 * @author    BusinessTech.fr - https://www.businesstech.fr
 * @copyright Business Tech 2020 - https://www.businesstech.fr
 * @license   Commercial
 *
 *           ____    _______
 *          |  _ \  |__   __|
 *          | |_) |    | |
 *          |  _ <     | |
 *          | |_) |    | |
 *          |____/     |_|
 */

require_once(dirname(__FILE__) . '/../../config/config.inc.php');
require_once(dirname(__FILE__) . '/../../init.php');
require_once(dirname(__FILE__) . '/facebookpsconnect.php');

// instantiate
$oModule = new FacebookPsConnect();

// execute amazon connector
echo $oModule->hookConnectorCallback(array(
    'connector' => 'amazon',
    'activecallback' => true,
    'code' => Tools::getValue('code'),
    'state' => Tools::getValue('state')
));
