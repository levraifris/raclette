<?php
/**
 * Social Login
 *
 * @author    BusinessTech.fr - https://www.businesstech.fr
 * @copyright Business Tech 2020 - https://www.businesstech.fr
 * @license   Commercial
 *
 *           ____    _______
 *          |  _ \  |__   __|
 *          | |_) |    | |
 *          |  _ <     | |
 *          | |_) |    | |
 *          |____/     |_|
 */

require_once(dirname(__FILE__) . '/../../config/config.inc.php');
require_once(dirname(__FILE__) . '/../../init.php');
require_once(dirname(__FILE__) . '/facebookpsconnect.php');

// get type of content to display
$sAction = Tools::getIsset('sAction') ? Tools::getValue('sAction') : '';
$sType = Tools::getIsset('sType') ? Tools::getValue('sType') : '';

$sUseCase = $sAction . $sType;

// instantiate
$oModule = new FacebookPsConnect();

switch ($sUseCase) {
    case 'connectplugin':
        // exec matched connector
        echo $oModule->hookConnectorConnect(array_merge($_GET, $_POST));
        break;
    case 'updatecustomer':
        // collect FB data
        echo $oModule->hookCustomerAssociation($_POST);
        break;
    case 'updateemail':
        // collect FB data
        echo $oModule->hookCustomerEmail($_POST);
        break;
    case 'displayshortCode':
        /* display the connectors with the shortCode */
        echo $oModule->hookDisplayShortCode($_POST);
        break;
    default:
        break;
}
