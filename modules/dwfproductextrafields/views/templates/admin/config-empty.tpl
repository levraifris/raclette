{**
 *   2009-2019 ohmyweb!
 *
 *   @author	ohmyweb <contact@ohmyweb.fr>
 *   @copyright 2009-2019 ohmyweb!
 *   @license   Proprietary - no redistribution without authorization
*}

<div class="config-container">
    <input type="hidden" name="config" value="" />
    <div id="cef_config" class="col-xs-12 col-lg-6">
    </div>
</div>
