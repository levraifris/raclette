<?php
/**
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to a commercial license from SARL Ether Creation
 * Use, copy, modification or distribution of this source file without written
 * license agreement from the SARL Ether Creation is strictly forbidden.
 * In order to obtain a license, please contact us: contact@ethercreation.com
 * ...........................................................................
 * INFORMATION SUR LA LICENCE D'UTILISATION
 *
 * L'utilisation de ce fichier source est soumise a une licence commerciale
 * concedee par la societe Ether Creation
 * Toute utilisation, reproduction, modification ou distribution du present
 * fichier source sans contrat de licence ecrit de la part de la SARL Ether Creation est
 * expressement interdite.
 * Pour obtenir une licence, veuillez contacter la SARL Ether Creation a l'adresse: contact@ethercreation.com
 * ...........................................................................
 *
 * @author    Ether Creation SARL <contact@ethercreation.com>
 * @copyright 2008-2021 Ether Creation SARL
 * @license   Commercial license
 * International Registered Trademark & Property of Ether Creation SARL
 */

require_once dirname(__FILE__) . '/../../config/config.inc.php';
require_once dirname(__FILE__) . '/class/catalog.class.php';
use ecicdiscountpro\Catalog;

require_once dirname(__FILE__) . '/class/price.class.php';
ignore_user_abort(true);

$paramHelp = Tools::getValue('help', null);
if (!is_null($paramHelp)) {
    $help = array(
        'ec_token' => array(
            'fr' => 'Token du module. Obligatoire.',
            'en' => 'Module\'s token. Required.'
            ),
        'gid' => array(
            'fr' => 'Groupe de client dont on veut les règles de prix.',
            'en' => 'Group id we wish to create the specific price rules for'
            ),
    );
    Catalog::answer(Tools::jsonEncode($help));
    exit();
}

$token = Tools::getValue('ec_token', 1);
if ($token != Catalog::getInfoEco('ECO_TOKEN')) {
    header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
    header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
    header('Cache-Control: no-store, no-cache, must-revalidate');
    header('Cache-Control: post-check=0, pre-check=0', false);
    header('Pragma: no-cache');
    Tools::redirect('Location: ../');
    exit();
}

$logger = Catalog::logStart('price');
$prefix = 'ECI_PGP_';

$stopTime = time() + 5;
$listStages = array(
    'STARTED',
    'SETPRODGROUPPRICE',
);


// paramètres
$paramAll = Tools::getValue('all', null);
$all = is_null($paramAll) ? false : true;

$paramJobID = Tools::getValue('jid', null);

$paramGid = Tools::getValue('gid', null);
$gid = is_null($paramGid) ? false : (int) $paramGid;

$paramNbCron = Tools::getValue('nbC');
$nbCron = (int) $paramNbCron;

$paramAct = Tools::getValue('act');
$action = (Catalog::jGet($prefix . 'ACT_' . $gid) === 'die') ? 'die' : ((empty($paramAct)) ? 'go' : $paramAct);

$paramSpy = Tools::getValue('spy');
$spy = (empty($paramSpy)) ? false : true;

$paramSpy2 = Tools::getValue('spytwo');
$spy2 = (empty($paramSpy2)) ? false : true;

$paramKill = Tools::getValue('kill', null);
$kill = is_null($paramKill) ? false : true;

//link
$eci_base_uri = implode('/', explode('\\', (((Configuration::get('PS_SSL_ENABLED') == 1) && (Configuration::get('PS_SSL_ENABLED_EVERYWHERE') == 1)) ? 'https://' : 'http://' ) .
    Tools::getShopDomain() . __PS_BASE_URI__ . str_replace(_PS_ROOT_DIR_ . DIRECTORY_SEPARATOR, '', dirname(__FILE__) . '/')));
$ts = preg_replace('/0\.([0-9]{6}).*? ([0-9]+)/', '$2$1', microtime());
$jid = is_null($paramJobID) ? $ts : $paramJobID;
$params = '?ec_token=' . $token . '&ts=' . $ts . '&jid=' . $jid;
$base_uri = $eci_base_uri . basename(__FILE__) . $params;


// kill
if ($kill) {
    if ($gid) {
        if (Catalog::jGet($prefix . 'STATE_' . $gid) != 'done') {
            Catalog::jUpdateValue($prefix . 'ACT_' . $gid, 'die');
        }
    }
    exit('licensetokill');
}


// fournisseur invalide
if (!$gid) {
    exit('nogid');
}


// espion
if ($spy) {
    Catalog::answer('spy');
    sleep(9);
    $state = Catalog::jGet($prefix . 'STATE_' . $gid);
    $progress = Catalog::jGet($prefix . 'PROGRESS_' . $gid);
    if ($nbCron == $progress) {
        if ($spy2) {
            if ($state != 'done') {
                Catalog::jUpdateValue($prefix . 'STATE_' . $gid, 'still');
            }
        } else {
            Catalog::followLink($base_uri . '&spy=1&spytwo=1&gid=' . $gid . '&nbC=' . $progress);
        }
    } else {
        Catalog::followLink($base_uri . '&spy=1&gid=' . $gid . '&nbC=' . $progress);
    }
    exit('bondwillreturn');
}


// abandon ou initialisation
$etat = Catalog::jGet($prefix . 'STATE_' . $gid);
$starting = ((bool) $token) & ((bool) $paramGid) & ($paramSpy === false) & ($paramNbCron === false) & ($paramKill === null) & ($paramAct === false);
if (!$starting && ($action === 'die')) {
    // abandon demandé par un kill
    Catalog::jUpdateValue($prefix . 'STATE_' . $gid, 'done');
    Catalog::jUpdateValue($prefix . 'END_TIME_' . $gid, date('Y-m-d H:i:s'));
    Catalog::jUpdateValue($prefix . 'ACT_' . $gid, 'go');
    exit('dead');
}
if ($starting && ($etat === 'running')) {
    // tentative de double lancement à éviter
    $progress = Catalog::jGet($prefix . 'PROGRESS_' . $gid);
    // envoi d'espion pour déjouer un plantage de serveur pendant une mise à jour
    Catalog::followLink($base_uri . '&spy=1&gid=' . $gid . '&nbC=' . (int) $progress);
    exit('nodoubleplease');
}
if (!$starting && ($etat === 'still')) {
    // un espion a pensé à tort qu'on était planté mais on est là !
    Catalog::jUpdateValue($prefix . 'STATE_' . $gid, 'running');
    Catalog::followLink($base_uri . '&spy=1&gid=' . $gid . '&nbC=' . $nbCron);
}
if ($starting) {
    // initialisation du process
    $aShops = Shop::getShops(true, null, true);
    asort($aShops);
    $listShops = array_values($aShops);
    Catalog::jUpdateValue($prefix . 'START_TIME_' . $gid, date('Y-m-d H:i:s'));
    Catalog::jUpdateValue($prefix . 'END_TIME_' . $gid, '');
    Catalog::jUpdateValue($prefix . 'SHOPS_TODO_' . $gid, implode(',', $listShops));
    Catalog::jUpdateValue($prefix . 'SHOP_' . $gid, reset($listShops));
    Catalog::jUpdateValue($prefix . 'STAGE_' . $gid, $listStages['1']);
    Catalog::jUpdateValue($prefix . 'PROGRESS_' . $gid, 0);
    Catalog::jUpdateValue($prefix . 'PROGRESSMAX_' . $gid, 0);
    Catalog::jUpdateValue($prefix . 'LOOPS_' . $gid, 0);
    Catalog::jUpdateValue($prefix . 'STATE_' . $gid, 'running');
    Catalog::jUpdateValue($prefix . 'ACT_' . $gid, 'go');
    Catalog::jUpdateValue($prefix . 'MESSAGE_' . $gid, '');
    // lancement de l'espion
    Catalog::followLink($base_uri . '&spy=1&gid=' . $gid . '&nbC=0');
} else {
    Catalog::jUpdateValue($prefix . 'PROGRESS_' . $gid, $nbCron);
}
$listShopsTodo = explode(',', Catalog::jGet($prefix . 'SHOPS_TODO_' . $gid));
$id_shop = (int) Catalog::jGet($prefix . 'SHOP_' . $gid);
$stage = Catalog::jGet($prefix . 'STAGE_' . $gid);


// gestion des reprises, ruptures, fin
if ($action === 'next') {
    $action = 'go';
    Catalog::jUpdateValue($prefix . 'ACT_' . $gid, 'go');

    $numStage = array_search($stage, $listStages, true);
    $keys = array_keys($listStages);
    $next = $nextKey = false;
    foreach ($keys as $key) {
        if ($next) {
            $nextKey = $key;
            break;
        }
        if ($numStage == $key) {
            $next = true;
        }
    }

    if ($nextKey) {
        $stage = $listStages[$nextKey];
        $nbCron = 0;
    } else {
        $ShopJustDone = array_shift($listShopsTodo);
        if (count($listShopsTodo) > 0) {
            $id_shop = reset($listShopsTodo);
            $stage = $listStages['1'];
            $nbCron = 0;
        } else {
            Catalog::jUpdateValue($prefix . 'SHOPS_TODO_' . $gid, '');
            Catalog::jUpdateValue($prefix . 'STATE_' . $gid, 'done');
            Catalog::jUpdateValue($prefix . 'END_TIME_' . $gid, date('Y-m-d H:i:s'));
            exit('alldone');
        }
    }
    Catalog::jUpdateValue($prefix . 'SHOPS_TODO_' . $gid, implode(',', $listShopsTodo));
    Catalog::jUpdateValue($prefix . 'SHOP_' . $gid, $id_shop);
    Catalog::jUpdateValue($prefix . 'STAGE_' . $gid, $stage);
    Catalog::jUpdateValue($prefix . 'LOOPS_' . $gid, 0);
    Catalog::jUpdateValue($prefix . 'PROGRESS_' . $gid, 0);
    Catalog::jUpdateValue($prefix . 'PROGRESSMAX_' . $gid, 0);
}


// aiguillage
Catalog::answer($stage);
$function = Tools::strtolower($stage);
if (function_exists($function)) {
    $reps = $function($prefix, $gid, $id_shop, $stopTime, $nbCron);
    if ($reps === true) {
        Catalog::jUpdateValue($prefix . 'ACT_' . $gid, 'next');
        Catalog::followLink($base_uri . '&gid=' . $gid . '&nbC=0&act=next' . ($all ? '&all' : ''));
    } elseif (is_numeric($reps)) {
        Catalog::jUpdateValue($prefix . 'LOOPS_' . $gid, Catalog::jGet($prefix . 'LOOPS_' . $gid) + 1);
        Catalog::followLink($base_uri . '&gid=' . $gid . '&nbC=' . $reps . ($all ? '&all' : ''));
    }
} else {
    exit('done');
}

if ($reps !== true && (!is_numeric($reps))) {
    Catalog::jUpdateValue($prefix . 'MESSAGE_' . $gid, date('Y-m-d H:i:s ') . var_export($reps, true));
    Catalog::logInfo(
        $logger,
        'setProdGP, ' . $gid . ', stage ' . $stage . ', ' . var_export($reps, true)
    );
}

exit('bye');


function started()
{
    echo 'Task successfully started. ';

    return true;
}

function setprodgroupprice($prefix, $gid, $id_shop, $stopTime, $nbCron)
{
    //find all products
    $liste_prods = Db::getInstance()->executeS(
        'SELECT DISTINCT `reference_parent`
        FROM `' . _DB_PREFIX_ . 'eci_price_spe`
        WHERE `id_group` = ' . (int) $gid . '
        AND `id_shop` = ' . (int) $id_shop
    );

    $jobLine = (int) $nbCron;
    foreach ($liste_prods as $line) {
        if (time() > $stopTime && $jobLine > $nbCron) {
            return $jobLine;
        }
        $jobLine++;

        if (empty($line)) {
            continue;
        }

        $price = new ImporterPrice();
        $price->addProductGroupPrice($line['reference_parent'], $gid, $id_shop);

        if (0 === ($jobLine % 50)) {
            Catalog::jUpdateValue($prefix . 'PROGRESS_' . $gid, $jobLine);
        }
    }

    return true;
}
