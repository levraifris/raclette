<?php
/**
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to a commercial license from SARL Ether Creation
 * Use, copy, modification or distribution of this source file without written
 * license agreement from the SARL Ether Creation is strictly forbidden.
 * In order to obtain a license, please contact us: contact@ethercreation.com
 * ...........................................................................
 * INFORMATION SUR LA LICENCE D'UTILISATION
 *
 * L'utilisation de ce fichier source est soumise a une licence commerciale
 * concedee par la societe Ether Creation
 * Toute utilisation, reproduction, modification ou distribution du present
 * fichier source sans contrat de licence ecrit de la part de la SARL Ether Creation est
 * expressement interdite.
 * Pour obtenir une licence, veuillez contacter la SARL Ether Creation a l'adresse: contact@ethercreation.com
 * ...........................................................................
 *
 * @author    Ether Creation SARL <contact@ethercreation.com>
 * @copyright 2008-2021 Ether Creation SARL
 * @license   Commercial license
 * International Registered Trademark & Property of Ether Creation SARL
 */

require_once dirname(__FILE__) . '/../../config/config.inc.php';
require_once dirname(__FILE__) . '/class/catalog.class.php';
use ecicdiscountpro\Catalog;

ignore_user_abort(true);

$paramHelp = Tools::getValue('help', null);
if (!is_null($paramHelp)) {
    $help = array(
        'ec_token' => array(
            'fr' => 'Token du module. Obligatoire.',
            'en' => 'Module\'s token. Required.'
            ),
        'id_order' => array(
            'fr' => 'Commande à envoyer. Obligatoire.',
            'en' => 'Order to be sent. Required.'
            ),
        'delay' => array(
            'fr' => 'Temps avant envoi de la commande. Obligatoire.',
            'en' => 'Delay before sending. Required.'
            ),
    );
    exit(Tools::jsonEncode($help));
}

$token = Tools::getValue('ec_token', 1);
if ($token != Catalog::getInfoEco('ECO_TOKEN')) {
    header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
    header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
    header('Cache-Control: no-store, no-cache, must-revalidate');
    header('Cache-Control: post-check=0, pre-check=0', false);
    header('Pragma: no-cache');
    Tools::redirect('Location: ../');
    exit();
}

//$logger = Catalog::logStart('delayorder');

// parameters
$paramNbCron = Tools::getValue('nbC', null);
$nbCron = is_null($paramNbCron) ? 0 : (int) $paramNbCron;

$paramOrder = Tools::getValue('id_order', null);
$id_order = $paramOrder;

$paramDelay = Tools::getValue('delay', null);
$delay = is_null($paramDelay) ? 0 : (int) $paramDelay;

$paramJobID = Tools::getValue('jid', null);

$ps_base_uri = implode('/', explode('\\', (((Configuration::get('PS_SSL_ENABLED') == 1) && (Configuration::get('PS_SSL_ENABLED_EVERYWHERE') == 1)) ? 'https://' : 'http://' ) .
    Tools::getShopDomain() . __PS_BASE_URI__));
$eci_base_uri = $ps_base_uri . str_replace(_PS_ROOT_DIR_ . DIRECTORY_SEPARATOR, '', dirname(__FILE__) . '/');
$ts = preg_replace('/0\.([0-9]{6}).*? ([0-9]+)/', '$2$1', microtime());
$jid = is_null($paramJobID) ? $ts : $paramJobID;
$params = 'ec_token=' . $token . '&ts=' . $ts . '&jid=' . $jid;
$base_uri = $eci_base_uri . basename(__FILE__) . '?' . $params;
$ajax_uri = $ps_base_uri . 'index.php?fc=module&module=eci'.Catalog::getConnecteurName().'&controller=ajax&' . $params;


//Catalog::logInfo(
//    $logger,
//    'delayorder '
//    . $nbCron . ','
//    . $id_order . ','
//    . $delay
//);


Catalog::answer('waitloop');

if ($nbCron) {
    sleep(5);
    $delay -= 5;
} else {
    $delay -= 1;
}

if (0 > $delay) {
    Catalog::followLink($ajax_uri . '&majsel=27&idc=' . $id_order);
} else {
    Catalog::followLink($base_uri . '&nbC=' . ($nbCron + 1) . '&delay=' . $delay . '&id_order=' . $id_order);
}

exit('bye');
