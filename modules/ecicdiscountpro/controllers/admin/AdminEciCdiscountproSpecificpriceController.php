<?php
/**
 * 2007-2017 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to a commercial license from SARL Ether Création
 * Use, copy, modification or distribution of this source file without written
 * license agreement from the SARL Ether Création is strictly forbidden.
 * In order to obtain a license, please contact us: contact@ethercreation.com
 * ...........................................................................
 * INFORMATION SUR LA LICENCE D'UTILISATION
 *
 * L'utilisation de ce fichier source est soumise a une licence commerciale
 * concedee par la societe Ether Création
 * Toute utilisation, reproduction, modification ou distribution du present
 * fichier source sans contrat de licence ecrit de la part de la SARL Ether Création est
 * expressement interdite.
 * Pour obtenir une licence, veuillez contacter la SARL Ether Création a l'adresse: contact@ethercreation.com
 * ...........................................................................
 *
 * @author    Ether Création SARL <contact@ethercreation.com>
 * @copyright 2008-2018 Ether Création SARL
 * @license   Commercial license
 * International Registered Trademark & Property of PrestaShop Ether Création SARL
 */

require_once dirname(__FILE__) . '/../../class/catalog.class.php';
require_once dirname(__FILE__) . '/../../gen/cdiscountpro/class/eccdiscountpro.php';
use ecicdiscountpro\Catalog;

class AdminEciCdiscountproSpecificpriceController extends ModuleAdminController
{
    public function __construct()
    {
        parent::__construct();
        $this->fournisseur  = 'cdiscountpro';
        $this->bootstrap = true;
        $this->table = 'specific_price';
        $this->identifier   = 'id_specific_price';
        $this->className = 'SpecificPrice';
        $this->context = Context::getContext();
        $this->id_lang = $this->context->language->id;
        $this->iso_code = $this->context->language->iso_code;
        $this->id_shop = $this->context->shop->id;
        $this->default_form_language = $this->id_lang;
        $this->_defaultOrderBy = 'id_specific_price';
        $this->orderBy = 'id_specific_price';
        $this->_default_pagination = 20;
        $this->_pagination = array(5, 10, 20, 50, 100, 300, 1000);
        $this->allow_export = true;
        $this->list_no_link = true;

//        $id_reserved_rule = $this->getReservedRuleId();
//        $this->_where = ' AND id_specific_price_rule = ' . (int) $id_reserved_rule;

        $this->fields_list = array(
            'id_specific_price' => array(
                'title' => $this->l('ID'),
                'align' => 'text-center',
                'class' => 'fixed-width-xs'
            ),
            'id_product' => array(
                'title' => $this->l('Product'),
                'align' => 'center',
                'class' => 'fixed-width-xs'
            ),
            'id_product_attribute' => array(
                'title' => $this->l('Attribute'),
                'align' => 'center',
                'class' => 'fixed-width-xs'
            ),
            'id_shop' => array(
                'title' => $this->l('Shop'),
                'align' => 'center',
                'class' => 'fixed-width-xs'
            ),
            'id_currency' => array(
                'title' => $this->l('Currency'),
                'align' => 'center',
                'class' => 'fixed-width-xs'
            ),
            'id_country' => array(
                'title' => $this->l('Country'),
                'align' => 'center',
                'class' => 'fixed-width-xs'
            ),
            'id_group' => array(
                'title' => $this->l('Group'),
                'align' => 'center',
                'class' => 'fixed-width-xs'
            ),
            'id_customer' => array(
                'title' => $this->l('Customer'),
                'align' => 'center',
                'class' => 'fixed-width-xs'
            ),
            'price' => array(
                'title' => $this->l('Price'),
                'align' => 'center',
                'class' => 'fixed-width-xs'
            ),
            'reduction' => array(
                'title' => $this->l('Reduction'),
                'align' => 'center',
                'class' => 'fixed-width-xs'
            ),
            'reduction_type' => array(
                'title' => $this->l('Reduction type'),
                'align' => 'center',
                'class' => 'fixed-width-xs'
            ),
            'reduction_tax' => array(
                'title' => $this->l('Reduction tax'),
                'align' => 'center',
                'class' => 'fixed-width-xs'
            ),
            'from_quantity' => array(
                'title' => $this->l('From quantity'),
                'align' => 'center',
                'class' => 'fixed-width-xs'
            ),
            'from' => array(
                'title' => $this->l('Date from'),
                'align' => 'right',
                'type' => 'datetime'
            ),
            'to' => array(
                'title' => $this->l('Date to'),
                'align' => 'right',
                'type' => 'datetime'
            ),
        );

        $this->actions = array('delete');

        $this->bulk_actions = array(
            'deleteSelection' => array(
                'text' => $this->l('Delete selection'),
                'icon' => 'icon-remove text-danger'
            ),
        );
    }

    public function initToolbar()
    {
        parent::initToolbar();
        $this->toolbar_btn['delete'] = array(
            'short' => 'Erase',
            'desc' => $this->l('Erase all'),
            'js' => 'if (confirm(\''.$this->l('Are you sure?').'\')) document.location = \''.Tools::safeOutput($this->context->link->getAdminLink('AdminEci' . Tools::ucfirst($this->fournisseur) . 'Specificprice')).'&amp;token='.$this->token.'&amp;deleteall=1\';'
        );
        unset($this->toolbar_btn['new']);
    }

    public function initProcess()
    {
        parent::initProcess();

        if (Tools::isSubmit('deleteall')) {
            if (empty($this->tabAccess['edit']) || $this->tabAccess['edit'] === '1') {
                $this->action = 'delete_all';
            } else {
                $this->errors[] = Tools::displayError('You do not have permission to edit this.');
            }
        } elseif (Tools::isSubmit('delete')) {
            if (empty($this->tabAccess['edit']) || $this->tabAccess['edit'] === '1') {
                $this->action = 'delete';
            } else {
                $this->errors[] = Tools::displayError('You do not have permission to edit this.');
            }
//        } elseif (Tools::isSubmit('updatelog')) {
//            $this->display = '';
//            $this->action = '';
        }
    }

    public function processDelete()
    {
        $class = $this->className;
        $obj = new $class($this->id_object);
        if (!Validate::isLoadedObject($obj)) {
            $this->errors[] = Tools::displayError('An error occurred while deleting this specific price.');
        }
        if (!$obj->delete()) {
            $this->errors[] = Tools::displayError('An error occurred while deleting this specific price.');
        }
        Tools::redirectAdmin(self::$currentIndex.'&token='.$this->token);
    }

    public function processDeleteAll()
    {
        $reserved_pricerule_id = $this->getReservedRuleId();

        if (Db::getInstance()->execute('DELETE FROM ' . _DB_PREFIX_ . 'specific_price WHERE id_specific_price_rule = ' . (int) $reserved_pricerule_id)) {
            Tools::redirectAdmin(self::$currentIndex.'&token='.$this->token);
        }
    }

    public function getReservedRuleId()
    {
        //vérification de la configuration et sinon génération+suppression de la règle
        $genClass = 'Ec' . $this->fournisseur;
        $ec_four = new $genClass($this->id_shop);
        $rule_name = 'Eci'.$this->fournisseur.' Tarifs règle réservée';
        $reserved_pricerule_id = 0;
        if (empty($ec_four->config['reserved_pricerule_id'])) {
            $id_rule = Db::getInstance()->getValue('SELECT id_specific_price_rule  FROM '._DB_PREFIX_.'specific_price_rule WHERE name LIKE "'.pSQL($rule_name).'"');
            if (!$id_rule) {
                Db::getInstance()->insert(
                    'specific_price_rule',
                    array(
                        'name'              =>  pSQL($rule_name),
                        'id_shop'           =>  (int) $this->id_shop,
                        'id_country'        =>  (int) 0,
                        'id_currency'       =>  (int) 0,
                        'id_group'          =>  (int) 0,
                        'from_quantity'     =>  (int) 1,
                        'price'             =>  (float) -1,
                        'reduction'         =>  (float) 0,
                        'reduction_tax'     =>  (int) 0,
                        'reduction_type'    =>  pSQL('percentage'),
                        'from'              =>  date('Y-m-d H:i:s'),
                        'to'                =>  date('Y-m-d H:i:s')
                    )
                );
                $id_rule = Db::getInstance()->Insert_ID();
                Db::getInstance()->delete(
                    'specific_price_rule',
                    'id_specific_price_rule = ' . (int) $id_rule
                );
            } else {
                $rule = new SpecificPriceRule($id_rule);
                $rule->delete();
            }
            Catalog::setECIConfigValue('reserved_pricerule_id', $id_rule, 0, $this->fournisseur);

            $reserved_pricerule_id = $id_rule;
        } else {
            $reserved_pricerule_id = $ec_four->config['reserved_pricerule_id'];
        }

        return $reserved_pricerule_id;
    }

    protected function processBulkDeleteSelection()
    {
        $result = true;
        if (is_array($this->boxes) && !empty($this->boxes)) {
            $result &= Db::getInstance()->execute('DELETE FROM ' . _DB_PREFIX_ . 'specific_price WHERE id_specific_price IN ("' . implode('","', array_map('intval', $this->boxes)) . '")');

            if (!$result) {
                $this->errors[] = Tools::displayError('An error occurred while deleting this selection.');
            }
        } else {
            $this->errors[] = Tools::displayError('You must select at least one element to delete.');
        }

        if (!count($this->errors)) {
            Tools::redirectAdmin(self::$currentIndex.'&token='.$this->token);
        }

        return $result;
    }
}
