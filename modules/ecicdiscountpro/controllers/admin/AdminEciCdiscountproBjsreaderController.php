<?php
/**
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to a commercial license from SARL Ether Creation
 * Use, copy, modification or distribution of this source file without written
 * license agreement from the SARL Ether Creation is strictly forbidden.
 * In order to obtain a license, please contact us: contact@ethercreation.com
 * ...........................................................................
 * INFORMATION SUR LA LICENCE D'UTILISATION
 *
 * L'utilisation de ce fichier source est soumise a une licence commerciale
 * concedee par la societe Ether Creation
 * Toute utilisation, reproduction, modification ou distribution du present
 * fichier source sans contrat de licence ecrit de la part de la SARL Ether Creation est
 * expressement interdite.
 * Pour obtenir une licence, veuillez contacter la SARL Ether Creation a l'adresse: contact@ethercreation.com
 * ...........................................................................
 *
 * @author    Ether Creation SARL <contact@ethercreation.com>
 * @copyright 2008-2021 Ether Creation SARL
 * @license   Commercial license
 * International Registered Trademark & Property of Ether Creation SARL
 */

require_once dirname(__FILE__).'/../../class/catalog.class.php';
require_once dirname(__FILE__).'/../../class/bigjson.php';
use ecicdiscountpro\Catalog;
use ecicdiscountpro\Bigjson;

class AdminEciCdiscountproBjsreaderController extends ModuleAdminController
{
    public function __construct()
    {
        parent::__construct();
        $this->fournisseur = "cdiscountpro";
        $this->table = "ecibjs" . $this->fournisseur;
        $this->list_id = $this->table;
        $this->bootstrap = true;
        $this->context = Context::getContext();
        $this->id_lang = $this->context->language->id;
        $this->id_shop = $this->context->shop->id;
        $this->default_form_language = $this->id_lang;
        $this->_default_pagination = 50;
        $this->_pagination = array(5, 10, 20, 50, 100, 300, 1000);
        $this->list_no_link = true;
        
        $this->bjs_autoIndex = true; //change to false if BJS runs out of memory in query

        $this->bjsfile = Tools::getValue($this->table.'file');
        $file_changed = false;
        if ($this->bjsfile && isset($this->context->cookie->{$this->table.'file'})) {
            if ($this->bjsfile != $this->context->cookie->{$this->table.'file'}) {
                $file_changed = true;
            }
        }
        if (empty($this->bjsfile)) {
            if (isset($this->context->cookie->{$this->table.'file'})) {
                $this->bjsfile = $this->context->cookie->{$this->table.'file'};
            } else {
                $this->bjsfile = '';
            }
        }

        $this->bjsfields = Tools::getValue($this->table.'fields');
        if (empty($this->bjsfields)) {
            if (isset($this->context->cookie->{$this->table.'fields'})) {
                $this->bjsfields = unserialize($this->context->cookie->{$this->table.'fields'});
            } else {
                $this->bjsfields = array();
            }
        }

        $this->bjsfilter = false;
        $this->bjswhere = array();
        if (Tools::getIsset('submitFilter')) {
            foreach ($this->bjsfields as $field) {
                $val = Tools::getValue($this->table.'Filter_'.$field);
                if (Tools::strlen($val)) {
                    $this->bjswhere[] = array($field, $val, Bigjson::IN + Bigjson::CI);
                    $this->bjsfilter = true;
                }
            }
        }
        if (!$this->bjswhere && isset($this->context->cookie->{$this->table.'where'})) {
            $this->bjswhere = unserialize($this->context->cookie->{$this->table.'where'});
            if (is_array($this->bjswhere) && $this->bjswhere) {
                $this->bjsfilter = true;
            } else {
                $this->bjswhere = array();
            }
        }

        $this->current_page = (int) Tools::getValue('submitFilter'.$this->table);
        if (empty($this->current_page)) {
            if (isset($this->context->cookie->{'submitFilter'.$this->table})) {
                $this->current_page = $this->context->cookie->{'submitFilter'.$this->table};
            }
        }
        if (!$this->current_page) {
            $this->current_page = 1;
        }

        if ($file_changed) {
            $this->bjsfields = array();
            $this->bjsfilter = false;
            $this->bjswhere = array();
            $this->current_page = 1;
        }
        if (Tools::getIsset('submitReset'.$this->table)) {
            $this->bjsfilter = false;
            $this->bjswhere = array();
            $this->current_page = 1;
        }

        $this->selected_pagination = Tools::getValue($this->table.'_pagination');
        //$this->selected_pagination = Tools::getValue('selected_pagination');
        if (empty($this->selected_pagination)) {
            if (isset($this->context->cookie->{$this->table.'selected_pagination'})) {
                $this->selected_pagination = $this->context->cookie->{$this->table.'selected_pagination'};
            } else {
                $this->selected_pagination = $this->_default_pagination;
            }
        }
        $this->_default_pagination = $this->selected_pagination;

        $this->context->cookie->{$this->table.'file'} = $this->bjsfile;
        $this->context->cookie->{$this->table.'fields'} = serialize($this->bjsfields);
        $this->context->cookie->{$this->table.'where'} = serialize($this->bjswhere);
        $this->context->cookie->{'submitFilter'.$this->table} = $this->current_page;
        $this->context->cookie->{$this->table.'selected_pagination'} = $this->selected_pagination;

        $this->fields_list = array();
    }

    public function initContent()
    {
        parent::initContent();

        $content = $this->renderForm();

        $this->context->smarty->assign(
            array(
                'content' => $content.$this->content,
            )
        );
    }

    public function renderForm()
    {
        $list_files = array();
        $list_txt = glob(Catalog::FILES_PATH . '*.txt');
        $list_json = glob(Catalog::FILES_PATH . '*.json');
        foreach ($list_txt as $txt_file) {
            if (in_array(preg_replace('/.txt$/', '.json', $txt_file), $list_json)) {
                $list_files[] = array('name' => basename($txt_file, '.txt'));
            }
        }

        $keys_query = array();
        if ($this->bjsfile) {
            try {
                $bjs = new Bigjson($this->bjsfile, Catalog::FILES_PATH);
                $bjs->autoIndex = $this->bjs_autoIndex;
                foreach ($bjs->keys as $key) {
                    $keys_query[] = array('name' => $key);
                }
            } catch (Exception $e) {
                Catalog::logError(Catalog::logStart('BJS controller'), 'Bug with file ' . $this->bjsfile . ' : ' . $e->getMessage());
                $this->bjsfile = '';
            }
        }

        $fields_form = array(
            'form' => array(
                'legend' => array('title' => $this->l('File parameters'), 'icon' => 'icon-cogs'),
                'input' => array(
                    array(
                        'type' => 'select',
                        'name' => $this->table.'file',
                        'label'=> $this->l('File'),
                        'desc' => $this->bjsfile ? $this->l('File date') . date(' : Y-m-d H:i:s', filemtime(Catalog::FILES_PATH . $this->bjsfile . Bigjson::DX)) : '',
                        'required' => true,
                        'options' => array(
                            'query' => $list_files,
                            'id' => 'name',
                            'name' => 'name',
                        )
                    ),
                ),
                'submit' => array('title' => $this->l('Save'),)
            ),
        );

        if ($this->bjsfile && $keys_query) {
            $fields_form['form']['input'][] = array(
                'type' => 'select',
                'name' => $this->table.'fields[]',
                'label'=> $this->l('Fields'),
                'multiple' => true,
                'required' => true,
                'options' => array(
                    'query' => $keys_query,
                    'id' => 'name',
                    'name' => 'name',
                )
            );
        }

        $helper = new HelperForm();
        $helper->show_toolbar = false;
        $helper->default_form_language = $this->id_lang;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
        $this->fields_form = array();
        $helper->submit_action = 'enregistrer';
        $helper->fields_value = array(
            $this->table.'file' => $this->bjsfile,
            $this->table.'fields[]' => $this->bjsfields,
        );
        $helper->tpl_vars = array(
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->id_lang
        );

        return $helper->generateForm(array($fields_form));
    }

    public function renderList()
    {
        if ($this->bjsfile) {
            try {
                $products = new Bigjson($this->bjsfile, Catalog::FILES_PATH);
                $products->autoIndex = $this->bjs_autoIndex;
            } catch (Exception $e) {
                $this->bjsfile = '';
            }
        }
        if ($this->bjsfile) {
            $keys = $this->bjsfields ? $this->bjsfields : $products->keys;
            if ($this->bjsfilter) {
                $this->_listTotal = $products->select(
                    $keys,
                    $this->bjswhere,
                    ($this->current_page - 1) * $this->selected_pagination,
                    $this->selected_pagination,
                    true
                );
                $this->_list = $products->select(
                    $keys,
                    $this->bjswhere,
                    ($this->current_page - 1) * $this->selected_pagination,
                    $this->selected_pagination
                );
            } else {
                $this->_listTotal = $products->nLines;
                $this->_list = array();
                $products->go(($this->current_page - 1) * $this->selected_pagination);
                while (($item = $products->read()) && (count($this->_list) < $this->selected_pagination)) {
                    $this->_list[] = $item;
                }
            }
        } else {
            $this->_list = array();
            $this->_listTotal = 0;
        }

        if ($this->bjsfields) {
            foreach ($this->bjsfields as $field_name) {
                $this->fields_list[$field_name] = array(
                    'title' => $this->l(Tools::ucfirst($field_name)),
                    'type' => 'text',
                    'search' => true,
                    'orderby' => false,
                    'align' => 'center',
                    'class' => 'nofollow'
                );
            }
            $this->identifier = reset($this->bjsfields);
        } else {
            $this->fields_list = array();
            $this->identifier = 'id';
        }

        Hook::exec('action'.$this->controller_name.'ListingResultsModifier', array(
            'list' => &$this->_list,
            'list_total' => &$this->_listTotal,
        ));

        $helper = new HelperList();

        $this->setHelperDisplay($helper);

        $helper->_default_pagination = $this->_default_pagination;
        $helper->_pagination = $this->_pagination;
        $helper->tpl_vars = $this->getTemplateListVars();
        $helper->tpl_delete_link_vars = $this->tpl_delete_link_vars;

        // For compatibility reasons, we have to check standard actions in class attributes
        foreach ($this->actions_available as $action) {
            if (!in_array($action, $this->actions) && isset($this->$action) && $this->$action) {
                $this->actions[] = $action;
            }
        }

        $helper->is_cms = $this->is_cms;

        foreach ($this->_list as $id => $line) {
            foreach ($line as $key => $value) {
                if (is_array($value)) {
                    $value = Tools::jsonEncode($value);
                }
                $this->_list[$id][$key] = preg_replace('/\v/', '', $value);
            }
        }
        unset($helper->toolbar_btn['new']);
        $list = $helper->generateList($this->_list, $this->fields_list);

        return $list;
    }
}
