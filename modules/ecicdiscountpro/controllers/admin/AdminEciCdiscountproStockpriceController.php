<?php
/**
 * 2007-2017 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to a commercial license from SARL Ether Création
 * Use, copy, modification or distribution of this source file without written
 * license agreement from the SARL Ether Création is strictly forbidden.
 * In order to obtain a license, please contact us: contact@ethercreation.com
 * ...........................................................................
 * INFORMATION SUR LA LICENCE D'UTILISATION
 *
 * L'utilisation de ce fichier source est soumise a une licence commerciale
 * concedee par la societe Ether Création
 * Toute utilisation, reproduction, modification ou distribution du present
 * fichier source sans contrat de licence ecrit de la part de la SARL Ether Création est
 * expressement interdite.
 * Pour obtenir une licence, veuillez contacter la SARL Ether Création a l'adresse: contact@ethercreation.com
 * ...........................................................................
 *
 * @author    Ether Création SARL <contact@ethercreation.com>
 * @copyright 2008-2018 Ether Création SARL
 * @license   Commercial license
 * International Registered Trademark & Property of PrestaShop Ether Création SARL
 */

class AdminEciCdiscountproStockpriceController extends ModuleAdminController
{
    public function __construct()
    {
        parent::__construct();
        $this->fournisseur = 'cdiscountpro';
        $this->bootstrap = true;
        $this->table = 'eci_stock';
        $this->identifier = 'id_product';
        $this->context = Context::getContext();
        $this->id_lang = $this->context->language->id;
        $this->iso_code = $this->context->language->iso_code;
        $this->id_shop = $this->context->shop->id;
        $this->default_form_language = $this->id_lang;
        $this->_defaultOrderBy = 'id_product';
        $this->orderBy = 'id_product';
        $this->_default_pagination = 20;
        $this->_pagination = array(5, 10, 20, 50, 100, 300, 1000);
        $this->allow_export = true;
        $this->list_no_link = true;
        
        $this->_where = ' AND fournisseur = "'.pSQL($this->fournisseur).'"';
        $this->_where .= ' AND id_shop IN ("'.implode('","', Shop::getContextListShopID()).'")';
        
        $shops = Shop::getShops();
        $shop_names = [];
        foreach ($shops as $shop) {
            $shop_names[$shop['id_shop']] = $shop['name'];
        }
        
        $depots = Db::getInstance()->executes('SELECT DISTINCT location FROM '._DB_PREFIX_.'eci_stock WHERE 1 '.$this->_where);
        $depots_names = [];
        foreach ($depots as $depot) {
            $depots_names[$depot['location']] = $depot['location'];
        }

        $this->fields_list = [
            'id_product' => [
                'title' => $this->l('Product'),
                'align' => 'center',
                'callback' => 'getProductLink',
            ],
            'id_pa' => [
                'title' => $this->l('Attribute'),
                'align' => 'center',
            ],
            'reference' => [
                'title' => $this->l('Reference'),
            ],
//            'fournisseur' => [
//                'title' => $this->l('Supplier'),
//            ],
            'id_shop' => [
                'title' => $this->l('Shop'),
                'align' => 'center',
                'type' => 'select',
                'list' => $shop_names,
                'filter_key' => 'id_shop',
                'filter_type' => 'int',
                'callback' => 'displayShop',
            ],
            'qt' => [
                'title' => $this->l('Quantity'),
                'align' => 'center',
            ],
            'location' => [
                'title' => $this->l('Location'),
                'type' => 'select',
                'list' => $depots_names,
                'filter_key' => 'location',
                'filter_type' => 'select',
            ],
            'price' => [
                'title' => $this->l('Wholesale price'),
                'type' => 'price',
            ],
            'pmvc' => [
                'title' => $this->l('Price'),
                'type' => 'price',
            ],
            'upd_flag' => [
                'title' => $this->l('Stock up to date'),
                'align' => 'center',
                'type' => 'bool',
                'callback' => 'displayUpdFlag',
            ],
            'prx_flag' => [
                'title' => $this->l('Price used'),
                'align' => 'center',
                'type' => 'select',
                'list' => [0 => $this->l('No'), 1 => $this->l('Yes'), 2 => $this->l('Protected')],
                'filter_key' => 'prx_flag',
                'filter_type' => 'int',
                'callback' => 'displayPrxFlag',
            ],
        ];
    }
    
    public function initToolbar()
    {
        parent::initToolbar();
        
        unset($this->toolbar_btn['new']);
    }

    
    public function getProductLink($id_product)
    {
        if (empty($id_product)) {
            return;
        }

        return '<a href="' . $this->context->link->getAdminLink('adminproducts') . '&updateproduct&id_product=' . $id_product.'" target="_blank">' . $id_product . '</a>';
    }
    
    public function displayShop($id_shop)
    {
        $shop = new Shop((int)$id_shop);
        
        return $shop->name;
    }
    
    public function displayUpdFlag($value)
    {
        return ($value ? '<i style="color:#72c279;" class="icon-check"></i>' : '<i style="color:#e08f95;" class="icon-remove"></i>');
    }
    
    public function displayPrxFlag($value)
    {
        switch ($value) {
            case 0:
                return $this->l('No');
            case 1:
                return $this->l('Yes');
            default:
                return $this->l('Protected');
        }
    }
}
