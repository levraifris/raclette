<?php
/**
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to a commercial license from SARL Ether Creation
 * Use, copy, modification or distribution of this source file without written
 * license agreement from the SARL Ether Creation is strictly forbidden.
 * In order to obtain a license, please contact us: contact@ethercreation.com
 * ...........................................................................
 * INFORMATION SUR LA LICENCE D'UTILISATION
 *
 * L'utilisation de ce fichier source est soumise a une licence commerciale
 * concedee par la societe Ether Creation
 * Toute utilisation, reproduction, modification ou distribution du present
 * fichier source sans contrat de licence ecrit de la part de la SARL Ether Creation est
 * expressement interdite.
 * Pour obtenir une licence, veuillez contacter la SARL Ether Creation a l'adresse: contact@ethercreation.com
 * ...........................................................................
 *
 * @author    Ether Creation SARL <contact@ethercreation.com>
 * @copyright 2008-2021 Ether Creation SARL
 * @license   Commercial license
 * International Registered Trademark & Property of Ether Creation SARL
 */

//require_once dirname(__FILE__) . '/../../class/seo.class.php'; // class in namespace
//use ecicdiscountpro\Seo; // if use class in namespace
require_once dirname(__FILE__) . '/../../class/eciseo.class.php'; // class without namespace

class AdminEciCdiscountproSeoController extends ModuleAdminController
{
    public function __construct()
    {
        $this->fournisseur  = 'cdiscountpro';
        $this->bootstrap    = true;
        $this->table        = 'eci_seo';
        $this->identifier   = 'id_seo';
        //$this->className    = 'ecicdiscountpro\Seo'; // if use class with namespace
        $this->className    = 'Seo'.$this->fournisseur; // if use class without namespace

        Shop::addTableAssociation($this->table, array('type' => 'shop'));
        Shop::addTableAssociation($this->table . '_lang', array('type' => 'fk_shop'));

        parent::__construct();
        $this->context      = Context::getContext();
        $this->id_lang      = $this->context->language->id;
        $this->id_shop      = $this->context->shop->id;

        $this->_select .= ' b.seo_name as name, b.seo_description as description, b.seo_description_short as description_short, b.seo_meta_title as meta_title, b.seo_meta_description as meta_description, sa.active as active';
        $this->_join = ' JOIN '._DB_PREFIX_.'eci_seo_shop sa ON (a.id_seo = sa.id_seo AND sa.id_shop = '.(int)$this->id_shop.')';
        $this->_join .= ' LEFT JOIN '._DB_PREFIX_.'eci_seo_lang b ON (b.id_seo = a.id_seo AND b.id_lang = '.(int)$this->id_lang.' AND b.id_shop = '.(int)$this->id_shop.')';
        $this->_where = ' AND a.fournisseur = "'. pSQL($this->fournisseur) . '"';

        //data to the grid of the "view" action
        $this->fields_list = array(
            'id_seo'       => array(
                'title' => $this->l('ID'),
                'type'  => 'int',
                'align' => 'center',
                'class' => 'fixed-width-xs',
            ),
            'name'     => array(
                'title' => $this->l('Name'),
                'type'  => 'text',
            ),
            'description'     => array(
                'title' => $this->l('Description'),
                'type'  => 'text',
            ),
            'description_short'     => array(
                'title' => $this->l('Short description'),
                'type'  => 'text',
            ),
            'meta_title'     => array(
                'title' => $this->l('Meta Title'),
                'type'  => 'text',
            ),
            'meta_description'     => array(
                'title' => $this->l('Meta description'),
                'type'  => 'text',
            ),
            'active'   => array(
                'title'  => $this->l('Status'),
                'active' => 'status',
                'type'   => 'bool',
                'align'  => 'text-center',
                'class'  => 'fixed-width-sm'
            ),
/*            'fournisseur'     => array(
                'title' => $this->l('Supplier'),
                'type'  => 'text',
            )*/
        );

//        $this->actions = array('edit', 'delete', 'view', 'details', 'machin');
//        $this->actions = array('edit', 'delete', 'view');
        $this->actions = array('edit', 'delete');

        $this->simple_header = false;

        $this->bulk_actions = array(
            'delete' => array(
                'text'    => $this->l('Delete selected'),
                'icon'    => 'icon-trash',
                'confirm' => $this->l('Delete selected items ?'),
            )
        );

        if (false !== Tools::getValue('addeci_seo') || false !== Tools::getValue('updateeci_seo')) {
            $this->displayInformation($this->l('Here are the tags you can use :') . ' {ref}, {manufacturer}, {categ}, {manufacturerPS}, {categPS}, {caracPS: XXX}');
            $this->displayInformation($this->l('Example :') . ' {categPS}_{ref}_{manufacturer}_{caracPS: Couleur}');
        }
        //fields to add/edit form
        $this->fields_form = array(
            'legend' => array(
                'title' => $this->l('SEO Values'),
            ),
            'input'  => array(
                'id_shop'   => array(
                    'type'     => 'shop',
                    'label'    => $this->l('Shop'),
                    'name'     => 'id_shop',
                ),
                'seo_name'   => array(
                    'type'     => 'text',
                    'label'    => $this->l('Name'),
                    'name'     => 'seo_name',
                    'hint'     => $this->l('must contain') . ' {name}',
                    'default_value'     => '{name}',
                    'required' => true,
                    'lang' => true
                ),
                'seo_description'   => array(
                    'type'     => 'text',
                    'label'    => $this->l('Description'),
                    'name'     => 'seo_description',
                    'hint'     => $this->l('must contain') . ' {description}',
                    'default_value'     => '{description}',
                    'required' => true,
                    'lang' => true
                ),
                'seo_description_short'   => array(
                    'type'     => 'text',
                    'label'    => $this->l('Short description'),
                    'name'     => 'seo_description_short',
                    'hint'     => $this->l('must contain') . ' {description_short}',
                    'default_value'     => '{description_short}',
                    'required' => true,
                    'lang' => true
                ),
                'seo_meta_title'   => array(
                    'type'     => 'text',
                    'label'    => $this->l('Meta title'),
                    'name'     => 'seo_meta_title',
                    'hint'     => $this->l('must contain') . ' {meta_title}',
                    'default_value'     => '{meta_title}',
                    'required' => true,
                    'lang' => true
                ),
                'seo_meta_description'   => array(
                    'type'     => 'text',
                    'label'    => $this->l('Meta description'),
                    'name'     => 'seo_meta_description',
                    'hint'     => $this->l('must contain') . ' {meta_description}',
                    'default_value'     => '{meta_description}',
                    'required' => true,
                    'lang' => true
                ),
                'active' => array(
                    'type'   => 'switch',
                    'label'  => $this->l('Active'),
                    'name'   => 'active',
                    'required' => true,
                    'shop'   => true,
                    'values' => array(
                        array(
                            'id'    => 'active_on',
                            'value' => 1,
                            'label' => $this->l('Yes'),
                        ),
                        array(
                            'id'    => 'active_off',
                            'value' => 0,
                            'label' => $this->l('No'),
                        ),
                    ),
                ),
                'fournisseur'   => array(
                    'type'     => 'hidden',
                    'label'    => $this->l('Supplier'),
                    'name'     => 'fournisseur',
                    'default_value'     => $this->fournisseur,
                    'required' => true,
                    'shop'     => true
                ),
            ),
            'submit' => array(
                'title' => $this->l('Save'),
            ),
        );
        $this->fields_value['fournisseur'] = $this->fournisseur;
    }

    public function initContent()
    {
        if (is_null(Shop::getContextShopID())) {
            $this->errors[] = $this->l('You must select a store to use this tab.');
            return;
        }

        parent::initContent();
    }

    public function setMedia($isNewTheme = false)
    {
        parent::setMedia();
        $this->addJqueryUi('ui.widget');
        $this->addJqueryPlugin('tagify');
    }
}
