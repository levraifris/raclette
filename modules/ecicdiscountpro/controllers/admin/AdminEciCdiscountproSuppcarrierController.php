<?php
/**
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to a commercial license from SARL Ether Creation
 * Use, copy, modification or distribution of this source file without written
 * license agreement from the SARL Ether Creation is strictly forbidden.
 * In order to obtain a license, please contact us: contact@ethercreation.com
 * ...........................................................................
 * INFORMATION SUR LA LICENCE D'UTILISATION
 *
 * L'utilisation de ce fichier source est soumise a une licence commerciale
 * concedee par la societe Ether Creation
 * Toute utilisation, reproduction, modification ou distribution du present
 * fichier source sans contrat de licence ecrit de la part de la SARL Ether Creation est
 * expressement interdite.
 * Pour obtenir une licence, veuillez contacter la SARL Ether Creation a l'adresse: contact@ethercreation.com
 * ...........................................................................
 *
 * @author    Ether Creation SARL <contact@ethercreation.com>
 * @copyright 2008-2021 Ether Creation SARL
 * @license   Commercial license
 * International Registered Trademark & Property of Ether Creation SARL
 */

//require_once dirname(__FILE__) . '/../../class/suppcarrier.class.php'; // class in namespace
//use ecicdiscountpro\Suppcarrier; // if use class in namespace
require_once dirname(__FILE__) . '/../../class/catalog.class.php';
require_once dirname(__FILE__) . '/../../class/ecisuppcarrier.class.php';
use ecicdiscountpro\Catalog;

class AdminEciCdiscountproSuppcarrierController extends ModuleAdminController
{
    public function __construct()
    {
        $this->fournisseur  = 'cdiscountpro';
        $this->bootstrap    = true;
        $this->table        = 'eci_suppcarrier';
        $this->identifier   = 'id_suppcarrier';
        //$this->className    = 'ecicdiscountpro\Suppcarrier'; // if use class with namespace
        $this->className    = 'Suppcarrier'.$this->fournisseur;

        Shop::addTableAssociation($this->table . '_lang', array('type' => 'lang'));
        parent::__construct();

        $this->id_lang      = $this->context->language->id;

        $this->_select .= ' b.delay as delay';
        $this->_join .= ' LEFT JOIN '._DB_PREFIX_.'eci_suppcarrier_lang b ON (b.id_suppcarrier = a.id_suppcarrier AND b.id_lang = '. (int)$this->id_lang . ')';
        $this->_where = ' AND a.fournisseur LIKE "'. pSQL($this->fournisseur) . '"';

        //data to the grid of the "view" action
        $this->fields_list = array(
            'id_suppcarrier'       => array(
                'title' => $this->l('ID'),
                'type'  => 'int',
                'align' => 'center',
                'class' => 'fixed-width-xs',
            ),
            'name'     => array(
                'title' => $this->l('Name'),
                'type'  => 'text',
            ),
            'delay'     => array(
                'title' => $this->l('Delay'),
                'type'  => 'text',
            ),
            'use_real'     => array(
                'title' => $this->l('Use PS shipping cost'),
                'align' => 'text-center',
                'type' => 'bool',
                'callback' => 'printReal',
                'orderby' => false
            ),
            'use_increment'     => array(
                'title' => $this->l('Add value to shipping cost'),
                'align' => 'text-center',
                'type' => 'bool',
                'callback' => 'printIncr',
                'orderby' => false
            ),
            'increment_replaces'     => array(
                'title' => $this->l('Replace shipping cost'),
                'align' => 'text-center',
                'type' => 'bool',
                'callback' => 'printRepl',
                'orderby' => false
            ),
            'increment_value'     => array(
                'title' => $this->l('Value of increment'),
                'type'  => 'price',
            ),
            'increment_type'     => array(
                'title' => $this->l('Type of increment'),
                'type'  => 'text',
            ),
            'active'   => array(
                'title'  => $this->l('Status'),
                'type' => 'bool',
                'active' => 'status',
                'align'  => 'text-center',
                'class'  => 'fixed-width-sm'
            )
        );

        $this->actions = array('edit', /*'delete'*/);

        $this->simple_header = false;

        $this->bulk_actions = array(
//            'delete' => array(
//                'text'    => $this->l('Delete selected'),
//                'icon'    => 'icon-trash',
//                'confirm' => $this->l('Delete selected items ?'),
//            )
        );

        //fields to add/edit form
        $this->fields_form = array(
            'legend' => array(
                'title' => $this->l('Carrier'),
            ),
            'input'  => array(
                'name'   => array(
                    'type'     => 'text',
                    'label'    => $this->l('Name'),
                    'name'     => 'name',
                    'required' => true,
                    'col'      => '4',
                ),
                'delay'   => array(
                    'type'     => 'text',
                    'label'    => $this->l('Shipping delay'),
                    'name'     => 'delay',
                    'required' => true,
                    'col'      => '4',
                    'lang'     => true
                ),
                'active' => array(
                    'type'   => 'switch',
                    'label'  => $this->l('Active'),
                    'name'   => 'active',
                    'is_bool' => true,
                    'required' => true,
                    'values' => array(
                        array(
                            'id'    => 'active_on',
                            'value' => 1,
                            'label' => $this->l('Yes'),
                        ),
                        array(
                            'id'    => 'active_off',
                            'value' => 0,
                            'label' => $this->l('No'),
                        ),
                    ),
                ),
                'use_real'   => array(
                    'type'     => 'switch',
                    'label'    => $this->l('Use PS shipping cost'),
                    'name'     => 'use_real',
                    'is_bool' => true,
                    'required' => true,
                    'values' => array(
                        array(
                            'id'    => 'real_on',
                            'value' => 1,
                            'label' => $this->l('Yes'),
                        ),
                        array(
                            'id'    => 'real_off',
                            'value' => 0,
                            'label' => $this->l('No'),
                        ),
                    ),
                ),
                'use_increment'   => array(
                    'type'     => 'switch',
                    'label'    => $this->l('Add value to shipping cost'),
                    'name'     => 'use_increment',
                    'is_bool' => true,
                    'required' => true,
                    'values' => array(
                        array(
                            'id'    => 'incr_on',
                            'value' => 1,
                            'label' => $this->l('Yes'),
                        ),
                        array(
                            'id'    => 'incr_off',
                            'value' => 0,
                            'label' => $this->l('No'),
                        ),
                    ),
                ),
                'increment_replaces'   => array(
                    'type'     => 'switch',
                    'label'    => $this->l('Replace shipping cost'),
                    'name'     => 'increment_replaces',
                    'is_bool' => true,
                    'required' => true,
                    'values' => array(
                        array(
                            'id'    => 'repl_on',
                            'value' => 1,
                            'label' => $this->l('Yes'),
                        ),
                        array(
                            'id'    => 'repl_off',
                            'value' => 0,
                            'label' => $this->l('No'),
                        ),
                    ),
                ),
                'increment_value'   => array(
                    'type'     => 'text',
                    'label'    => $this->l('Value of increment'),
                    'name'     => 'increment_value',
                    'col'      => '4',
                ),
                'increment_type'   => array(
                    'type'     => 'select',
                    'label'    => $this->l('Type of increment'),
                    'name'     => 'increment_type',
                    'options' => array(
                        'query' => array(
                            array('id' => 'percentage', 'name' => $this->l('percentage')),
                            array('id' => 'amount', 'name' => $this->l('amount')),
                        ),
                        'id' => 'id',
                        'name' => 'name'
                    ),
                ),
                //good way : may be edited but not created this way
                'id_carrier'   => array(
                    'type'     => 'hidden',
                    'label'    => $this->l('Carrier'),
                    'name'     => 'id_carrier',
                    'required' => true,
                ),
                //just for tests : helps to create a linked carrier
//                'id_carrier'   => array(
//                    'type'     => 'select',
//                    'label'    => $this->l('Carrier'),
//                    'name'     => 'id_carrier',
//                    'options' => array(
//                        'query' => Carrier::getCarriers($this->id_lang, true, false, false, null, Carrier::ALL_CARRIERS),
//                        'id' => 'id_carrier',
//                        'name' => 'name'
//                    ),
//                    'required' => true,
//                ),
                'fournisseur'   => array(
                    'type'     => 'hidden',
                    'label'    => $this->l('Supplier'),
                    'name'     => 'fournisseur',
                    'default_value'     => $this->fournisseur,
                    'required' => true,
                ),
            ),
            'submit' => array(
                'title' => $this->l('Save'),
            ),
        );
        $this->fields_value['fournisseur'] = $this->fournisseur;

        $this->fields_options = array(
            'general' => array(
                'title' =>    $this->l('Reinstall carriers'),
                'icon' => 'icon-envelope',
                'fields' =>    array(
                    'reinstall_carriers' => array(
                        'title' => $this->l('Click the button to reinstall carriers. They will not be reset.'),
                        'no_multishop_checkbox' => true,
                        'updateOption' => 'reinstall_carriers',
                        'auto_value' => false,
                        'value' => 42,
                        'default' => 42,
                        'type' => 'none'
                    )
                ),
                'submit' => array('title' => $this->l('Reinstall'))
            )
        );
    }

    public function initContent()
    {
        parent::initContent();
    }

    public function initToolbar()
    {
        parent::initToolbar();

        unset($this->toolbar_btn['new']);
    }

    public function setMedia($isNewTheme = false)
    {
        parent::setMedia();
        $this->addJqueryUi('ui.widget');
        $this->addJqueryPlugin('tagify');
    }

    public function printReal($value, $suppcarrier)
    {
        return '<a class="list-action-enable '.($value ? 'action-enabled' : 'action-disabled').'" href="index.php?'.htmlspecialchars('tab=AdminEciCdiscountproSuppcarrier&id_suppcarrier='
            .(int)$suppcarrier['id_suppcarrier'].'&changeRealVal&token='.Tools::getAdminTokenLite('AdminEciCdiscountproSuppcarrier')).'">
				'.($value ? '<i class="icon-check"></i>' : '<i class="icon-remove"></i>').
            '</a>';
    }

    public function printIncr($value, $suppcarrier)
    {
        return '<a class="list-action-enable '.($value ? 'action-enabled' : 'action-disabled').'" href="index.php?'.htmlspecialchars('tab=AdminEciCdiscountproSuppcarrier&id_suppcarrier='
            .(int)$suppcarrier['id_suppcarrier'].'&changeIncrVal&token='.Tools::getAdminTokenLite('AdminEciCdiscountproSuppcarrier')).'">
				'.($value ? '<i class="icon-check"></i>' : '<i class="icon-remove"></i>').
            '</a>';
    }

    public function printRepl($value, $suppcarrier)
    {
        return '<a class="list-action-enable '.($value ? 'action-enabled' : 'action-disabled').'" href="index.php?'.htmlspecialchars('tab=AdminEciCdiscountproSuppcarrier&id_suppcarrier='
            .(int)$suppcarrier['id_suppcarrier'].'&changeReplVal&token='.Tools::getAdminTokenLite('AdminEciCdiscountproSuppcarrier')).'">
				'.($value ? '<i class="icon-check"></i>' : '<i class="icon-remove"></i>').
            '</a>';
    }

    public function initProcess()
    {
        parent::initProcess();

        if (Tools::isSubmit('changeRealVal') && $this->id_object) {
            //if ($this->tabAccess['edit'] === '1') {
                $this->action = 'change_real_val';
            //} else {
            //    $this->errors[] = Tools::displayError('You do not have permission to edit this.');
            //}
        } elseif (Tools::isSubmit('changeIncrVal') && $this->id_object) {
            //if ($this->tabAccess['edit'] === '1') {
                $this->action = 'change_incr_val';
            //} else {
            //    $this->errors[] = Tools::displayError('You do not have permission to edit this.');
            //}
        } elseif (Tools::isSubmit('changeReplVal') && $this->id_object) {
            //if ($this->tabAccess['edit'] === '1') {
                $this->action = 'change_repl_val';
            //} else {
            //    $this->errors[] = Tools::displayError('You do not have permission to edit this.');
            //}
        } elseif (Tools::isSubmit('updatesuppcarrier')) {
            $this->display = '';
            $this->action = '';
        }
    }

    public function processChangeRealVal()
    {
        $class = $this->className;
        $suppcarrier = new $class($this->id_object);
        if (!Validate::isLoadedObject($suppcarrier)) {
            $this->errors[] = Tools::displayError('An error occurred while updating suppcarrier information.');
        }
        $suppcarrier->use_real = $suppcarrier->use_real ? false : true;
        if (!$suppcarrier->update()) {
            $this->errors[] = Tools::displayError('An error occurred while updating suppcarrier information.');
        }
        Tools::redirectAdmin(self::$currentIndex.'&token='.$this->token);
    }

    public function processChangeIncrVal()
    {
        $class = $this->className;
        $suppcarrier = new $class($this->id_object);
        if (!Validate::isLoadedObject($suppcarrier)) {
            $this->errors[] = Tools::displayError('An error occurred while updating suppcarrier information.');
        }
        $suppcarrier->use_increment = $suppcarrier->use_increment ? false : true;
        if (!$suppcarrier->update()) {
            $this->errors[] = Tools::displayError('An error occurred while updating suppcarrier information.');
        }
        Tools::redirectAdmin(self::$currentIndex.'&token='.$this->token);
    }

    public function processChangeReplVal()
    {
        $class = $this->className;
        $suppcarrier = new $class($this->id_object);
        if (!Validate::isLoadedObject($suppcarrier)) {
            $this->errors[] = Tools::displayError('An error occurred while updating suppcarrier information.');
        }
        $suppcarrier->increment_replaces = $suppcarrier->increment_replaces ? false : true;
        if (!$suppcarrier->update()) {
            $this->errors[] = Tools::displayError('An error occurred while updating suppcarrier information.');
        }
        Tools::redirectAdmin(self::$currentIndex.'&token='.$this->token);
    }

    public function updateOptionReinstallCarriers($value = '')
    {
        $this->fields_options['general']['fields']['reinstall_carriers']['value'] = $value;
        $res = true;
        if (42 === $value) {
            $catalog = new Catalog();
            if (!is_null($cars = $catalog->setCarriers($this->fournisseur)) & !$cars) {
                $this->errors[] = $this->l('Carriers were not successfully reinstalled');
                $res = false;
            }
        } else {
            $this->errors[] = $this->l('There was an error in the option value : ' . $value);
            $res = false;
        }

        return $res;
    }
}
