<?php
/**
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to a commercial license from SARL Ether Creation
 * Use, copy, modification or distribution of this source file without written
 * license agreement from the SARL Ether Creation is strictly forbidden.
 * In order to obtain a license, please contact us: contact@ethercreation.com
 * ...........................................................................
 * INFORMATION SUR LA LICENCE D'UTILISATION
 *
 * L'utilisation de ce fichier source est soumise a une licence commerciale
 * concedee par la societe Ether Creation
 * Toute utilisation, reproduction, modification ou distribution du present
 * fichier source sans contrat de licence ecrit de la part de la SARL Ether Creation est
 * expressement interdite.
 * Pour obtenir une licence, veuillez contacter la SARL Ether Creation a l'adresse: contact@ethercreation.com
 * ...........................................................................
 *
 * @author    Ether Creation SARL <contact@ethercreation.com>
 * @copyright 2008-2021 Ether Creation SARL
 * @license   Commercial license
 * International Registered Trademark & Property of Ether Creation SARL
 */

require_once dirname(__FILE__) . '/../../class/catalog.class.php';
use ecicdiscountpro\Catalog;

class AdminEciCdiscountproFeatureController extends ModuleAdminController
{
    public function __construct()
    {
        parent::__construct();
        $this->fournisseur = 'cdiscountpro';
        $this->bootstrap = true;
        $this->context = Context::getContext();
        $this->id_lang = $this->context->language->id;
        $this->id_shop = $this->context->shop->id;
        $this->default_form_language = $this->id_lang;
        $this->_defaultOrderBy = 'id';
        $this->orderBy = 'id';
        $this->_default_pagination = 50;
        $this->_pagination = array(5, 10, 20, 50, 100, 300, 1000);
    }

    public function initContent()
    {
        if (is_null(Shop::getContextShopID())) {
            $this->errors[] = $this->l('You must select a store to use this tab.');
            return;
        }

        parent::initContent();
        $catalog = new Catalog($this->context);
        $param = $catalog->getFeaturesParam($this->fournisseur);
        
        $this->context->smarty->assign(
            array(
                'ec_token' => Catalog::getInfoEco('ECO_TOKEN'),
                'eci_baseDir' => __PS_BASE_URI__,
                'eci_ajaxlink' => $this->context->link->getModuleLink('eci' . $this->fournisseur, 'ajax'),
                'fournisseur' => $this->fournisseur,
                'id_shop' => $this->id_shop
            )
        );
        $content = $this->context->smarty->fetch(_PS_MODULE_DIR_ . 'eci' . $this->fournisseur . '/views/templates/admin/displaymessage.tpl');
        $this->context->smarty->assign(array('content' => $content.$param.$this->content));
    }

    public function renderList()
    {
        return parent::renderList();
    }

    public function setMedia($isNewTheme = false)
    {
        parent::setMedia();
        $this->addJs(
            _PS_MODULE_DIR_ .'eci' . $this->fournisseur . '/views/js/feature.js',
            'all'
        );
        $this->addCss(
            _PS_MODULE_DIR_ .'eci' . $this->fournisseur . '/views/css/feature.css',
            'all'
        );
    }
}
