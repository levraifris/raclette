<?php
/**
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to a commercial license from SARL Ether Creation
 * Use, copy, modification or distribution of this source file without written
 * license agreement from the SARL Ether Creation is strictly forbidden.
 * In order to obtain a license, please contact us: contact@ethercreation.com
 * ...........................................................................
 * INFORMATION SUR LA LICENCE D'UTILISATION
 *
 * L'utilisation de ce fichier source est soumise a une licence commerciale
 * concedee par la societe Ether Creation
 * Toute utilisation, reproduction, modification ou distribution du present
 * fichier source sans contrat de licence ecrit de la part de la SARL Ether Creation est
 * expressement interdite.
 * Pour obtenir une licence, veuillez contacter la SARL Ether Creation a l'adresse: contact@ethercreation.com
 * ...........................................................................
 *
 * @author    Ether Creation SARL <contact@ethercreation.com>
 * @copyright 2008-2021 Ether Creation SARL
 * @license   Commercial license
 * International Registered Trademark & Property of Ether Creation SARL
 */

require_once dirname(__FILE__) . '/../../class/catalog.class.php';
require_once dirname(__FILE__) . '/../../class/ecitranslations.class.php';
use ecicdiscountpro\Catalog;
use ecicdiscountpro\EciTranslations;

class AdminEciCdiscountproTaskController extends ModuleAdminController
{

    public function __construct()
    {
        parent::__construct();
        $this->fournisseur = "cdiscountpro";
        $this->bootstrap = true;
        $this->context = Context::getContext();
        $this->id_lang = $this->context->language->id;
        $this->iso_code = $this->context->language->iso_code;
        $this->id_shop = $this->context->shop->id;
        $this->default_form_language = $this->context->language->id;

        if (is_null(Shop::getContextShopID())) {
            $this->errors[] = $this->l('You must select a store to use this tab.');
            return;
        }

        $this->table = 'eci_jobs_history';
        $this->identifier = 'id_eci_jobs_history';
        $this->_defaultOrderBy = 'jid';
        $this->order_by = 'jid';
        $this->_orderWay = 'DESC';
        $this->_default_pagination = 50;
        $this->_pagination = array(5, 10, 20, 50, 100, 300, 1000);
        $this->allow_export = true;
        $this->_select = ' b.name';
        $this->_join = ' LEFT JOIN ' . _DB_PREFIX_ . 'eci_cpanel AS b ON (b.prefix LIKE CONCAT("ECI_", a.task) AND b.suffix = a.fournisseur)';
        $this->_where = 'AND a.fournisseur = "' . pSQL($this->fournisseur) . '"';
        $this->list_no_link = true;

        $this->fields_list = array(
//            'id' => array(
//                'title' => $this->l('ID'),
//                'type'  => 'int',
//                'align' => 'text-center',
//                'class' => 'fixed-width-xs'
//            ),
            'jid' => array(
                'title' => $this->l('JID'),
                'type'  => 'int',
                'align' => 'text-center',
                'class' => 'fixed-width-xs'
            ),
            'task' => array(
                'title' => $this->l('Task'),
                'type'  => 'text',
                'align' => 'text-center',
                'class' => 'fixed-width-xs'
            ),
            'name' => array(
                'title' => $this->l('Task'),
                'type'  => 'text',
                'callback' => 'translateTaskName',
                'search' => false,
                'align' => 'text-center',
            ),
            'state' => array(
                'title' => $this->l('State'),
                'type'  => 'text',
                'align' => 'text-center',
            ),
            'ts' => array(
                'title' => $this->l('Date'),
                'type'  => 'datetime',
                'align' => 'text-center',
            ),
            'msg' => array(
                'title' => $this->l('Message'),
                'type'  => 'text',
                'align' => 'text-center',
                'class' => 'wordwrap',
            ),
//            'fournisseur' => array(
//                'title' => $this->l('Supplier'),
//                'type'  => 'text',
//                'align' => 'text-center',
//                'class' => 'nofollow'
//            ),
        );

        $this->fields_options = array(
            'general' => array(
                'title' =>    $this->l('Reinstall tasks'),
                'icon' => 'icon-cogs',
                'fields' =>    array(
                    'reinstall_tasks' => array(
                        'title' => $this->l('Click the button to reinstall tasks to fit the module setup. The links will not be changed'),
                        'no_multishop_checkbox' => true,
                        'updateOption' => 'reinstall_tasks',
                        'auto_value' => false,
                        'value' => 42,
                        'default' => 42,
                        'type' => 'none',
                    )
                ),
                'submit' => array('title' => $this->l('Reinstall'))
            )
        );
    }

    public function translateTaskName($value)
    {
        $t = EciTranslations::getInstance()->translate($value, 'catalogmom', $this->iso_code);
        if ($value == $t) {
            $t = EciTranslations::getInstance()->translate($value, 'ec' . $this->fournisseur . 'mom', $this->iso_code);
        }

        return ($t != $value) ? $t : $value;
    }

    public function initContent()
    {
        parent::initContent();

        $listTasks = Catalog::getTasks();
        foreach ($listTasks as &$task) {
            $task['name'] = EciTranslations::getInstance()->translate(
                $task['name'],
                ($task['specific'] ? 'ec' . $this->fournisseur . 'mom' : 'catalogmom'),
                $this->iso_code
            );
        }

        $this->context->smarty->assign(
            array(
                'listTasks' => $listTasks,
                'ec_token' => Catalog::getInfoEco('ECO_TOKEN'),
                'eci_baseDir' => __PS_BASE_URI__,
                'eci_ajaxlink' => $this->context->link->getModuleLink('eci' . $this->fournisseur, 'ajax'),
                'fournisseur' => $this->fournisseur,
                'id_shop' => $this->id_shop
            )
        );

        $content = $this->context->smarty->fetch(_PS_MODULE_DIR_ . 'eci' . $this->fournisseur . '/views/templates/admin/displaymessage.tpl');
        $param = $this->context->smarty->fetch(_PS_MODULE_DIR_ . 'eci' . $this->fournisseur . '/views/templates/admin/EcCpanels.tpl');
        $this->context->smarty->assign(array(
            'content' => $content . $param . $this->content,
        ));
    }

    public function initToolbar()
    {
        parent::initToolbar();
        unset($this->toolbar_btn['new']);
    }

    public function setMedia($isNewTheme = false)
    {
        parent::setMedia();
        $this->addJs(
            _PS_MODULE_DIR_ . 'eci' . $this->fournisseur . '/views/js/tasks.js',
            'all'
        );
        $this->addCss(
            _PS_MODULE_DIR_ .'eci' . $this->fournisseur . '/views/css/tasks.css',
            'all'
        );
    }

    public function updateOptionReinstallTasks($value = '')
    {
        $this->fields_options['general']['fields']['reinstall_tasks']['value'] = $value;
        $res = true;
        if (42 === $value) {
            Catalog::clearTasksCon($this->fournisseur);
            $catalog = new Catalog();
            if (!$catalog->setTasksCon($this->fournisseur)) {
                $this->errors[] = $this->l('Tasks were not successfully reinstalled');
                $res = false;
            }
        } else {
            $this->errors[] = $this->l('There was an error in the option value : ' . $value);
            $res = false;
        }

        return $res;
    }
}
