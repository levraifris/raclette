<?php
/**
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to a commercial license from SARL Ether Creation
 * Use, copy, modification or distribution of this source file without written
 * license agreement from the SARL Ether Creation is strictly forbidden.
 * In order to obtain a license, please contact us: contact@ethercreation.com
 * ...........................................................................
 * INFORMATION SUR LA LICENCE D'UTILISATION
 *
 * L'utilisation de ce fichier source est soumise a une licence commerciale
 * concedee par la societe Ether Creation
 * Toute utilisation, reproduction, modification ou distribution du present
 * fichier source sans contrat de licence ecrit de la part de la SARL Ether Creation est
 * expressement interdite.
 * Pour obtenir une licence, veuillez contacter la SARL Ether Creation a l'adresse: contact@ethercreation.com
 * ...........................................................................
 *
 * @author    Ether Creation SARL <contact@ethercreation.com>
 * @copyright 2008-2021 Ether Creation SARL
 * @license   Commercial license
 * International Registered Trademark & Property of Ether Creation SARL
 */

require_once dirname(__FILE__) . '/../../class/catalog.class.php';
require_once dirname(__FILE__) . '/../../class/eciapiauth.class.php';
require_once dirname(__FILE__) . '/../../class/apitools.php';
use ecicdiscountpro\Catalog;
use ecicdiscountpro\ApiTools;

class AdminEciCdiscountproApiauthController extends ModuleAdminController
{
    public function __construct()
    {
        $this->fournisseur  = 'cdiscountpro';
        $this->bootstrap    = true;
        $this->table        = 'eci_apiauth';
        $this->identifier   = 'id_apiauth';
        //$this->className    = 'ecicdiscountpro\ApiAuth'; // if use class with namespace
        $this->className    = 'Apiauth'.$this->fournisseur;

        parent::__construct();

        $this->_where = ' AND a.fournisseur LIKE "'. pSQL($this->fournisseur) . '"';

        //data to the grid of the "view" action
        $this->fields_list = array(
            'id_apiauth'       => array(
                'title' => $this->l('ID'),
                'type'  =>  'int',
                'align' =>  'center',
                'class' =>  'fixed-width-xs noclic',
            ),
            'user'     => array(
                'title' =>  $this->l('User'),
                'type'  =>  'text',
                'class' =>  'noclic',
            ),
            'authtoken'     => array(
                'title' =>  $this->l('Token'),
                'type'  =>  'text',
                'class' =>  'noclic',
            ),
            'authorizations'     => array(
                'title' =>  $this->l('Authorizations'),
                'type'  =>  'text',
                'class' =>  'noclic',
            ),
            'active'   => array(
                'title' =>  $this->l('Status'),
                'type'  =>  'bool',
                'active'=>  'status',
                'align' =>  'text-center',
                'class' =>  'fixed-width-sm'
            )
        );

        $this->actions = array('edit', 'delete', 'copy');

        $this->simple_header = false;

        $this->bulk_actions = array(
            'delete' => array(
                'text'    => $this->l('Delete selected'),
                'icon'    => 'icon-trash',
                'confirm' => $this->l('Delete selected items ?'),
            )
        );
        
        $methodes_request = ApiTools::getApiMethodes();
        foreach ($methodes_request as &$methode) {
            $methode = ['id' => $methode['name'], 'name' => $methode['name'] . ' ' . $methode['parameters']];
        }
        
        //fields to add/edit form
        $this->fields_form = array(
            'legend' => array(
                'title' => $this->l('API Authorizations'),
            ),
            'input'  => array(
                'user'   => array(
                    'type'     => 'text',
                    'label'    => $this->l('User'),
                    'name'     => 'user',
                    'required' => true,
                    'col'      => '4',
                ),
                'authtoken'   => array(
                    'type'     => 'text',
                    'label'    => $this->l('Token'),
                    'name'     => 'authtoken',
                    'suffix'   => 'generate',
                    'required' => true,
                    'col'      => '4',
                ),
                'authorizations'    => array(
                    'type'          => 'select',
                    'multiple'      => 'true',
                    'label'         => $this->l('Authorizations'),
                    'name'          => 'authorizations[]',
                    'options'       => array(
                        'query'     => $methodes_request,
                        'id'        => 'id',
                        'name'      => 'name',
                    ),
                    'required'      => true,
                    'col'           => '8',
                ),
                'active' => array(
                    'type'   => 'switch',
                    'label'  => $this->l('Active'),
                    'name'   => 'active',
                    'is_bool' => true,
                    'required' => true,
                    'values' => array(
                        array(
                            'id'    => 'active_on',
                            'value' => 1,
                            'label' => $this->l('Yes'),
                        ),
                        array(
                            'id'    => 'active_off',
                            'value' => 0,
                            'label' => $this->l('No'),
                        ),
                    ),
                ),
                'fournisseur'   => array(
                    'type'     => 'hidden',
                    'label'    => $this->l('Supplier'),
                    'name'     => 'fournisseur',
                    'default_value'     => $this->fournisseur,
                    'required' => true,
                ),
            ),
            'submit' => array(
                'title' => $this->l('Save'),
            ),
        );
        $this->fields_value['fournisseur'] = $this->fournisseur;
    }

    public function initContent()
    {
        parent::initContent();
    }

    public function initToolbar()
    {
        parent::initToolbar();
    }

    public function setMedia($isNewTheme = false)
    {
        parent::setMedia();
        $this->addJqueryUi('ui.widget');
        $this->addJqueryPlugin('tagify');
        $this->addJs(_PS_MODULE_DIR_ . 'eci' . $this->fournisseur . '/views/js/apiedit.js', 'all');
    }

    public function initProcess()
    {
        parent::initProcess();
    }
    
    public function renderForm()
    {
        if (!empty($this->object->authorizations) && is_array($this->object->authorizations)) {
            $this->fields_value['authorizations[]'] = $this->object->authorizations;
        }
        
        return parent::renderForm();
    }

    public function displayCopyLink($token, $id, $name)
    {
        return '<a class="copier" href="' . self::$currentIndex.'&token='.$this->token . '&action=copy&id_apiauth=' . $id . '" title="' . $this->l('Copy') . '"><i class="icon-copy"></i> ' . $this->l('Copy') . '</a>';
    }
   
    public function processCopy()
    {
        $id_apiauth = (int) Tools::getValue('id_apiauth');
        $api_auth = new $this->className($id_apiauth);
        $new_api_auth = $api_auth->duplicateObject();
        $new_api_auth->authtoken = md5(time());
        $new_api_auth->save();
        $this->redirect_after = self::$currentIndex.'&token='.$this->token;
    }
}
