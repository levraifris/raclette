<?php
/**
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to a commercial license from SARL Ether Creation
 * Use, copy, modification or distribution of this source file without written
 * license agreement from the SARL Ether Creation is strictly forbidden.
 * In order to obtain a license, please contact us: contact@ethercreation.com
 * ...........................................................................
 * INFORMATION SUR LA LICENCE D'UTILISATION
 *
 * L'utilisation de ce fichier source est soumise a une licence commerciale
 * concedee par la societe Ether Creation
 * Toute utilisation, reproduction, modification ou distribution du present
 * fichier source sans contrat de licence ecrit de la part de la SARL Ether Creation est
 * expressement interdite.
 * Pour obtenir une licence, veuillez contacter la SARL Ether Creation a l'adresse: contact@ethercreation.com
 * ...........................................................................
 *
 * @author    Ether Creation SARL <contact@ethercreation.com>
 * @copyright 2008-2021 Ether Creation SARL
 * @license   Commercial license
 * International Registered Trademark & Property of Ether Creation SARL
 */

require_once dirname(__FILE__).'/../../class/catalog.class.php';
require_once dirname(__FILE__).'/../../ecicdiscountpro.php';
use ecicdiscountpro\Catalog;

class AdminEciCdiscountproCategoryController extends ModuleAdminController
{
    public function __construct()
    {
        parent::__construct();
        $this->fournisseur = 'cdiscountpro';
        $this->bootstrap = true;
        $this->table = 'eci_category_shop';
        $this->identifier = 'id';

        $this->context = Context::getContext();
        $this->id_lang = $this->context->language->id;
        $this->id_shop = $this->context->shop->id;
        $this->default_form_language = $this->id_lang;
        $this->default_iso_code = Language::getIsoById(Configuration::get('PS_LANG_DEFAULT'));

        $this->_select = ' cl.name as clname';
        $this->_join = 'LEFT JOIN ' . _DB_PREFIX_ . 'category_lang cl ON (a.id_category = cl.id_category AND cl.id_shop = ' . (int) $this->id_shop . ' AND cl.id_lang = ' . (int) $this->id_lang. ')';
        $this->_where = 'AND a.id_shop = '.(int)$this->id_shop.' AND fournisseur = "'.pSQL($this->fournisseur).'" AND a.iso_code = "' . pSQL($this->default_iso_code) . '"';

        $this->_defaultOrderBy = 'name';
        $this->orderBy = 'name';

        $this->_pagination = array(5, 10, 20, 50, 100, 300, 1000);
        $this->_default_pagination = 50;
        $this->addRowAction('edit');

        $this->current_page = (int) Tools::getValue('submitFilter'.$this->table);
        if (empty($this->current_page)) {
            if (isset($this->context->cookie->{'submitFilter'.$this->table})) {
                $this->current_page = $this->context->cookie->{'submitFilter'.$this->table};
            }
        }
        if (!$this->current_page) {
            $this->current_page = 1;
        }
        $this->context->cookie->{'submitFilter'.$this->table} = $this->current_page;

        $this->fields_list = array();
        $this->fields_list['id'] = array(
            'title' => $this->l('ID'),
            'type' => 'int',
            'search' => true,
            'orderby' => true,
            'align' => 'center',
            'class' => 'showmatchform fixed-width-xs id_cateco',
        );
/*        $this->fields_list['fournisseur'] = array(
                'title' => $this->l('Supplier'),
                'type' => 'text',
                'search' => true,
                'orderby' => false,
                'class' => 'showmatchform',
        );*/
        $this->fields_list['name'] = array(
                'title' => $this->l('Supplier Category'),
                'type' => 'text',
                'search' => true,
                'orderby' => true,
                'filter_key' => 'a!name',
                'class' => 'showmatchform name_cateco',
        );
        $this->fields_list['id_category'] = array(
                'title' => $this->l('id_category'),
                'type' => 'int',
                'search' => false,
                'class' => 'showmatchform id_catpsold hidden',
        );
        $this->fields_list['clname'] = array(
                'title' => $this->l('Category'),
                'type' => 'text',
                'search' => true,
                'orderby' => true,
                'filter_key' => 'cl!name',
                'class' => 'showmatchform',
        );
        $this->fields_list['import'] = array(
                'title' => $this->l('Import All'),
                'type' => 'bool',
                'callback' => 'printImport',
                'search' => true,
                'orderby' => true,
                'align' => 'text-center',
                'class' => 'fixed-width-xs',
        );
        $this->fields_list['blacklist'] = array(
                'title' => $this->l('Blacklist'),
                'type' => 'bool',
                'callback' => 'printBlacklist',
                'search' => true,
                'orderby' => true,
                'align' => 'text-center',
                'class' => 'fixed-width-xs',
        );

        $this->bulk_actions = array(
            'markImportSel' => array(
                'text'    => $this->l('Mark selected categories for import'),
                'icon'    => 'icon-share',
            ),
            'markDeleteSel' => array(
                'text'    => $this->l('Unmark selected categories for import'),
                'icon'    => 'icon-remove',
            ),
            'blackListSel' => array(
                'text'    => $this->l('Blacklist selection'),
                'icon'    => 'icon-ban',
            ),
            'whiteListSel' => array(
                'text'    => $this->l('Whitelist selection'),
                'icon'    => 'icon-check',
            ),
        );
    }

    public function printImport($value, $category)
    {
        return '<a class="list-action-enable '.($value ? 'action-enabled' : 'action-disabled')
            .'" href="index.php?'.htmlspecialchars('tab=AdminEciCdiscountproCategory&id='
            .urlencode($category['id']).'&oldimportvalue='.$value.'&changeImportVal&token='
            .Tools::getAdminTokenLite('AdminEciCdiscountproCategory')).'">'
            .($value ? '<i class="icon-check"></i>' : '<i class="icon-remove"></i>').'</a>';
    }

    public function printBlacklist($value, $category)
    {
        return '<a class="list-action-enable '.($value ? 'action-enabled' : 'action-disabled')
            .'" href="index.php?'.htmlspecialchars('tab=AdminEciCdiscountproCategory&id='
            .urlencode($category['id']).'&oldblacklistvalue='.$value.'&changeBlacklistVal&token='
            .Tools::getAdminTokenLite('AdminEciCdiscountproCategory')).'">'
            .($value ? '<i class="icon-ban"></i>' : '<i class="icon-remove"></i>').'</a>';
    }

    public function initContent()
    {
        if (is_null(Shop::getContextShopID())) {
            $this->errors[] = $this->l('You must select a store to use this tab.');
            return;
        }

        parent::initContent();

        $id_lang = Configuration::get('PS_LANG_DEFAULT');
        Shop::setContext(Shop::CONTEXT_SHOP, $this->id_shop);

        $root = Category::getRootCategory($id_lang);
        $liste_cats = array();
        $nest = version_compare(_PS_VERSION_, '1.6', '>=') ? Category::getNestedCategories($root->id, $id_lang, false, null, true) : Catalog::getNestedCategories($this->id_shop, $id_lang);
        Catalog::catNestToList($nest, $liste_cats);
        $this->context->smarty->assign(array(
            'fournisseur' => $this->fournisseur,
            'ec_token' => Catalog::getInfoEco('ECO_TOKEN'),
            'eci_baseDir' => __PS_BASE_URI__,
            'category_list' => $liste_cats,
            'id_shop' => (int) $this->id_shop));
        $content = $this->context->smarty->fetch(_PS_MODULE_DIR_ . 'eci' . $this->fournisseur . '/views/templates/admin/matchcat.tpl');
        $this->context->smarty->assign(array('content' => $content.$this->content));
    }

    public function renderList()
    {
        if (!($this->fields_list && is_array($this->fields_list))) {
            return false;
        }
        $this->getList($this->id_lang);
        $helper = new HelperList();

        // Empty list is ok
        if (!is_array($this->_list)) {
            $this->displayWarning($this->l('Bad SQL query', 'Helper').'<br />'.htmlspecialchars($this->_list_error));
            return false;
        }

        $this->setHelperDisplay($helper);
        $helper->_default_pagination = $this->_default_pagination;
        $helper->_pagination = $this->_pagination;
        $helper->tpl_vars = $this->getTemplateListVars();
        $helper->tpl_delete_link_vars = $this->tpl_delete_link_vars;

        $helper->is_cms = $this->is_cms;
        $helper->sql = $this->_listsql;

        foreach ($this->_list as $key => $value) {
            $this->_list[$key]['name'] = str_replace('::>>::', ' > ', $this->_list[$key]['name']);
        }
        unset($helper->toolbar_btn['new']);
        $list = $helper->generateList($this->_list, $this->fields_list);

        return $list;
    }

    public function initProcess()
    {
        parent::initProcess();

        if (Tools::isSubmit('changeBlacklistVal') && Tools::isSubmit('id') && Tools::isSubmit('oldblacklistvalue')) {
            //if ($this->tabAccess['edit'] === '1') {
                $this->id = Tools::getValue('id');
                $this->oldblacklistvalue = Tools::getValue('oldblacklistvalue');
                $this->action = 'change_blacklist_val';
            //} else {
            //    $this->errors[] = Tools::displayError('You do not have permission to edit this.');
            //}
        } elseif (Tools::isSubmit('changeImportVal') && Tools::isSubmit('id') && Tools::isSubmit('oldimportvalue')) {
            //if ($this->tabAccess['edit'] === '1') {
                $this->id = Tools::getValue('id');
                $this->oldimportvalue = Tools::getValue('oldimportvalue');
                $this->action = 'change_import_val';
            //} else {
            //    $this->errors[] = Tools::displayError('You do not have permission to edit this.');
            //}
        } elseif (Tools::isSubmit('submitEciCdiscountproMatchCat') && Tools::isSubmit('id_cateco') && Tools::isSubmit('id_catps')) {
            //if ($this->tabAccess['edit'] === '1') {
                $this->id_cateco = Tools::getValue('id_cateco');
                $this->id_catps = Tools::getValue('id_catps');
                $this->action = 'match_cat';
            //} else {
            //    $this->errors[] = Tools::displayError('You do not have permission to edit this.');
            //}
        }
    }

    public function processChangeBlacklistVal()
    {
        $new_value = (1 - $this->oldblacklistvalue);
        $ret = true;
        $catalog = new Catalog($this->context);
        if ($new_value) {
            $ret &= $catalog->categoryBlacklistTree($this->fournisseur, $this->id, 1);
        } else {
            $path = Catalog::categoryGetPathById($this->id);
            if ($path) {
                $paths = Catalog::categoryPathToPaths($path);
                foreach ($paths as $path2) {
                    $id = $catalog->categoryGetIdByPath($path2);
                    $ret &= $catalog->categoryBlacklist($this->fournisseur, $id, 0);
                }
            }
        }
        if ($ret) {
            $ret &= $catalog->setSelection($this->fournisseur);
        }

        if (!$ret) {
            $this->errors[] = Tools::displayError('An error occurred while updating category information.');
        }
        Tools::redirectAdmin(self::$currentIndex.'&token='.$this->token.'&submitFilter'.$this->table.'='.$this->current_page);
    }

    public function processChangeImportVal()
    {
        $new_value = (1 - $this->oldimportvalue);
        $catalog = new Catalog($this->context);
        $ret = true;
        $ret &= Db::getInstance()->update(
            'eci_category_shop',
            array('import' => (int) $new_value),
            'id = ' . (int) $this->id
        );
        if ($ret) {
            $catalog->setSelection($this->fournisseur);
        }
        if (!$ret) {
            $this->errors[] = Tools::displayError('An error occurred while updating category information.');
        }
        Tools::redirectAdmin(self::$currentIndex.'&token='.$this->token.'&submitFilter'.$this->table.'='.$this->current_page);
    }

    public function processMatchCat()
    {
        if (!Db::getInstance()->update(
            'eci_category_shop',
            array('id_category' => (int) $this->id_catps),
            'id = '.(int)$this->id_cateco
        )) {
            $this->errors[] = Tools::displayError('An error occurred while updating category information.');
        }
        Tools::redirectAdmin(self::$currentIndex.'&token='.$this->token.'&submitFilter'.$this->table.'='.$this->current_page);
    }

    protected function processBulkMarkImportSel()
    {
        $result = true;
        if (is_array($this->boxes) && !empty($this->boxes)) {
            $result &= Db::getInstance()->update(
                'eci_category_shop',
                array('import' => 1),
                'id IN ("' . implode('", "', $this->boxes) . '")'
            );
            if ($result) {
                $catalog = new Catalog($this->context);
                $result &= $catalog->setSelection($this->fournisseur);
                //redirect
                //$this->redirect_after = self::$currentIndex.'&token='.$this->token; // not this link !
            } else {
                $this->errors[] = Tools::displayError('An error occurred while marking this selection for import.');
            }
        } else {
            $this->errors[] = Tools::displayError('You must select at least one element to mark.');
        }

        return $result;
    }

    protected function processBulkMarkDeleteSel()
    {
        $result = true;
        if (is_array($this->boxes) && !empty($this->boxes)) {
            $result &= Db::getInstance()->update(
                'eci_category_shop',
                array('import' => 0),
                'id IN ("' . implode('", "', $this->boxes) . '")'
            );
            if ($result) {
                $catalog = new Catalog($this->context);
                $result &= $catalog->setSelection($this->fournisseur);
                //redirect
                //$this->redirect_after = self::$currentIndex.'&token='.$this->token; // not this link !
            } else {
                $this->errors[] = Tools::displayError('An error occurred while marking this selection for delete.');
            }
        } else {
            $this->errors[] = Tools::displayError('You must select at least one element to mark.');
        }

        return $result;
    }

    protected function processBulkBlackListSel()
    {
        $result = true;
        if (is_array($this->boxes) && !empty($this->boxes)) {
            $catalog = new Catalog($this->context);
            //get refs and update catalog table
            foreach ($this->boxes as $id) {
                $result &= $catalog->categoryBlacklistTree($this->fournisseur, $id, 1);
            }
            if ($result) {
                $result &= $catalog->setSelection($this->fournisseur);
                //redirect
                //$this->redirect_after = self::$currentIndex.'&token='.$this->token; // not this link !
            } else {
                $this->errors[] = Tools::displayError('An error occurred while blacklisting this selection.');
            }
        } else {
            $this->errors[] = Tools::displayError('You must select at least one element to blacklist.');
        }

        return $result;
    }

    protected function processBulkWhiteListSel()
    {
        $result = true;
        if (is_array($this->boxes) && !empty($this->boxes)) {
            $catalog = new Catalog($this->context);
            //get refs and update catalog table
            foreach ($this->boxes as $id) {
                $path = Catalog::categoryGetPathById($id);
                if (!$path) {
                    continue;
                }
                $paths = Catalog::categoryPathToPaths($path);
                foreach ($paths as $path2) {
                    $id2 = $catalog->categoryGetIdByPath($path2);
                    $result &= $catalog->categoryBlacklist($this->fournisseur, $id2, 0);
                }
            }
            if ($result) {
                $result &= $catalog->setSelection($this->fournisseur);
                //redirect
                //$this->redirect_after = self::$currentIndex.'&token='.$this->token; // not this link !
            } else {
                $this->errors[] = Tools::displayError('An error occurred while whitelisting this selection.');
            }
        } else {
            $this->errors[] = Tools::displayError('You must select at least one element to whitelist.');
        }

        return $result;
    }

    public function setMedia($isNewTheme = false)
    {
        parent::setMedia();
        $this->addJs(
            _PS_MODULE_DIR_ .'eci' . $this->fournisseur . '/views/js/category.js',
            'all'
        );
    }
}
