<?php
/**
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to a commercial license from SARL Ether Creation
 * Use, copy, modification or distribution of this source file without written
 * license agreement from the SARL Ether Creation is strictly forbidden.
 * In order to obtain a license, please contact us: contact@ethercreation.com
 * ...........................................................................
 * INFORMATION SUR LA LICENCE D'UTILISATION
 *
 * L'utilisation de ce fichier source est soumise a une licence commerciale
 * concedee par la societe Ether Creation
 * Toute utilisation, reproduction, modification ou distribution du present
 * fichier source sans contrat de licence ecrit de la part de la SARL Ether Creation est
 * expressement interdite.
 * Pour obtenir une licence, veuillez contacter la SARL Ether Creation a l'adresse: contact@ethercreation.com
 * ...........................................................................
 *
 * @author    Ether Creation SARL <contact@ethercreation.com>
 * @copyright 2008-2021 Ether Creation SARL
 * @license   Commercial license
 * International Registered Trademark & Property of Ether Creation SARL
 */

require_once dirname(__FILE__) . '/../../class/catalog.class.php';
require_once dirname(__FILE__) . '/../../gen/cdiscountpro/class/eccdiscountpro.php';
use ecicdiscountpro\Catalog;

class AdminEciCdiscountproCommandeController extends ModuleAdminController
{
    protected $statuses_array = array();

    public function __construct()
    {
        parent::__construct();
        $this->fournisseur = "cdiscountpro";
        $this->bootstrap = true;
        $this->table = 'order';
        $this->identifier = 'id_order';
        $this->context = Context::getContext();
        $this->id_shop = $this->context->shop->id;
        $this->id_lang = $this->context->language->id;
        $this->default_form_language = $this->id_lang;
        $this->_defaultOrderBy = 'id_order';
        $this->_orderBy = 'id_order';
        $this->_orderWay = 'DESC';
        $this->_use_found_rows = true;
        $this->_default_pagination = 50;
        $this->_pagination = array(5, 10, 20, 50, 100, 300, 1000);
        $this->allow_export = true;
        $this->list_no_link = true;
        $send_order_to_gen_only = (bool) Catalog::getClassConstant('eci'.$this->fournisseur.'\Catalog', 'SEND_ORDER_TO_GEN_ONLY');
        $this->simulation_available = false;
        try {
            $sendorder = new ReflectionMethod('Ec'.$this->fournisseur, 'sendOrder');
            $parameters = $sendorder->getParameters();
            if (isset($parameters[1]) && 'simulation' === $parameters[1]->name) {
                $this->simulation_available = true;
            }
        } catch (ReflectionException $e) {
            // no sendOrder
        }

        $this->_select = 'a.id_currency,
            CONCAT(LEFT(c.`firstname`, 1), \'. \', c.`lastname`) AS `customer`,
            osl.`name` AS `osname`,
            os.`color`,
            IF(a.valid, 1, 0) badge_success,
            IF(0=eci.visible, 1, 0) already_sent,
            eci.`info`';

        $this->_join = 'LEFT JOIN `'._DB_PREFIX_.'customer` c ON (c.`id_customer` = a.`id_customer`)';
        $this->_join .= ' LEFT JOIN `'._DB_PREFIX_.'order_state` os ON (os.`id_order_state` = a.`current_state`)';
        $this->_join .= ' LEFT JOIN `'._DB_PREFIX_.'order_state_lang` osl ON (os.`id_order_state` = osl.`id_order_state` AND osl.`id_lang` = '.(int)$this->id_lang.')';
        $this->_join .= ' LEFT JOIN `'._DB_PREFIX_.'eci_export_com` eci ON (eci.`id_order` = a.`id_order`)';
        $this->_where = ' AND a.`id_shop` = ' . (int) $this->id_shop;
        if (!$send_order_to_gen_only) {
            $this->_join .= ' LEFT JOIN `'._DB_PREFIX_.'order_detail` od ON (od.`id_order` = a.`id_order`)';
            $this->_join .= ' LEFT JOIN `'._DB_PREFIX_.'eci_product_shop` ecips ON (ecips.`id_product` = od.`product_id`)';
            $this->_where .= ' AND ecips.fournisseur = "' . pSQL($this->fournisseur) . '"';
            $this->_where .= ' AND ecips.id_shop = ' . (int) $this->id_shop;
        }
        $this->_group = ' GROUP BY a.id_order';

        $this->fromPSOrder = 'AdminOrders' == Tools::getValue('controller');
        if ($this->fromPSOrder) {
            $this->token = Tools::getAdminTokenLite('AdminOrders');
        }

        $id_order = Tools::getValue('action', false) ? null : Tools::getValue('id_order', null);
        if (isset($id_order)) {
            $this->_where .= ' AND a.id_order = ' . (int) $id_order;
        }

        $this->toolbar_title = $this->l('Orders') . ' ' . Catalog::getFriendlyName();

        $statuses = OrderState::getOrderStates((int)$this->id_lang);
        foreach ($statuses as $status) {
            $this->statuses_array[$status['id_order_state']] = $status['name'];
        }

        $this->fields_list = array();
        $this->fields_list['id_order'] = array(
            'title' => $this->l('ID'),
            'type' => 'int',
            'search' => !$this->fromPSOrder,
            'orderby' => !$this->fromPSOrder,
            'align' => 'center',
            'class' => 'fixed-width-xs',
        );
        $this->fields_list['reference'] = array(
            'title' => $this->l('Reference'),
            'search' => !$this->fromPSOrder,
            'orderby' => !$this->fromPSOrder,
            'filter_key' => 'a!reference',
            'callback' => 'getOrderLink',
            'callback_object' => $this,
        );
        $this->fields_list['info'] = array(
            'title' => $this->l('External reference'),
            'filter_key' => 'eci!info',
            'search' => !$this->fromPSOrder,
            'orderby' => !$this->fromPSOrder,
        );
        $this->fields_list['customer'] = array(
            'title' => $this->l('Customer'),
            'havingFilter' => true,
            'search' => !$this->fromPSOrder,
            'orderby' => !$this->fromPSOrder,
        );
        $this->fields_list['total_paid_tax_incl'] = array(
            'title' => $this->l('Total'),
            'type' => 'price',
            'currency' => true,
            'callback' => 'setOrderCurrency',
            'badge_success' => true,
            'search' => !$this->fromPSOrder,
            'orderby' => !$this->fromPSOrder,
        );
        $this->fields_list['payment'] = array(
            'title' => $this->l('Payment'),
            'search' => !$this->fromPSOrder,
            'orderby' => !$this->fromPSOrder,
        );
        $this->fields_list['osname'] = array(
            'title' => $this->l('Status', 'Admin.Global'),
            'type' => 'select',
            'color' => 'color',
            'list' => $this->statuses_array,
            'filter_key' => 'os!id_order_state',
            'filter_type' => 'int',
            'order_key' => 'osname',
            'search' => !$this->fromPSOrder,
            'orderby' => !$this->fromPSOrder,
        );
        $this->fields_list['date_add'] = array(
            'title' => $this->l('Date'),
            'type' => 'datetime',
            'filter_key' => 'a!date_add',
            'search' => !$this->fromPSOrder,
            'orderby' => !$this->fromPSOrder,
        );
        $this->fields_list['already_sent'] = array(
            'title' => $this->l('Already sent'),
            'type' => 'bool',
            'callback' => 'printSent',
            'search' => !$this->fromPSOrder,
            'orderby' => !$this->fromPSOrder,
            'havingFilter' => true,
            'align' => 'text-center',
        );

        $this->bulk_actions = array(
            'sendSelection' => array(
                'text' => $this->l('Send selection'),
                'icon' => 'icon-send text-success'
            ),
        );

        $this->actions = [
            'sendOrder',
        ];
        if ($this->simulation_available && !$this->fromPSOrder) {
            $this->actions[] = 'simulOrder';
        }
    }

    public function getOrderLink($order_reference, $tr)
    {
        if (empty($tr['id_order'])) {
            return;
        }

        if ($this->fromPSOrder) {
            return $order_reference;
        }

        return '<a href="' . $this->context->link->getAdminLink('AdminOrders') . '&vieworder&id_order=' . $tr['id_order'] . '" target="_blank">' . $order_reference . '</a>';
    }

    public function printSent($value)
    {
        return ($value ? '<i style="color:#72c279;" class="icon-check"></i>' : '<i style="color:#e08f95;" class="icon-remove"></i>');
    }

    public function setOrderCurrency($echo, $tr)
    {
        $order = new Order($tr['id_order']);
        return Tools::displayPrice($echo, (int)$order->id_currency);
    }

    public function displaySendOrderLink($token, $id, $name)
    {
        if ($this->fromPSOrder) {
            return '<a class="btn btn-default" href="' . $this->context->link->getAdminLink('AdminEci' . Tools::ucfirst($this->fournisseur) . 'Commande') . '&action=sendOrder&id_order=' . $id . '" title="' . $this->l('Send order') . '"><i class="icon-send"></i> ' . $this->l('Send order') . '</a>';
        }

        return '<a class="btn btn-default" href="' . self::$currentIndex.'&token='.$this->token . '&action=sendOrder&id_order=' . $id . '" title="' . $this->l('Send order') . '"><i class="icon-send"></i> ' . $this->l('Send order') . '</a>';
    }

    public function displaySimulOrderLink($token, $id, $name)
    {
        if ($this->fromPSOrder) {
            return '<a class="btn" href="' . $this->context->link->getAdminLink('AdminEci' . Tools::ucfirst($this->fournisseur) . 'Commande') . '&action=simulOrder&id_order=' . $id . '" title="' . $this->l('Simulate order') . '"><i class="icon-eye"></i> ' . $this->l('Simulate order') . '</a>';
        }

        return '<a class="btn" href="' . self::$currentIndex.'&token='.$this->token . '&action=simulOrder&id_order=' . $id . '" title="' . $this->l('Simulate order') . '"><i class="icon-eye"></i> ' . $this->l('Simulate order') . '</a>';
    }

    public function initToolbar()
    {
        parent::initToolbar();
        unset($this->toolbar_btn['new']);
    }

    public function renderList()
    {
        $hide_tabs = Catalog::getClassConstant('ec'.$this->fournisseur, 'HIDE_TABS', true);
        if (is_array($hide_tabs) && in_array('Orders', $hide_tabs)) {
            return;
        }

        if (!($this->fields_list && is_array($this->fields_list))) {
            return false;
        }
        $this->getList($this->id_lang);
        
        if (empty($this->_list)) {
            return;
        }

        $helper = new HelperList();

        // Empty list is ok
        if (!is_array($this->_list)) {
            $this->displayWarning($this->l('Bad SQL query', 'Helper').'<br />'.htmlspecialchars($this->_list_error));
            if (($column = preg_replace('/.*Column \'([^\']+)\'.*/', '$1', $this->_list_error))) {
                $this->context->cookie->{$this->getCookieFilterPrefix().$this->table.'Filter_'.$column} = '';
            }
            return false;
        }

        $this->setHelperDisplay($helper);
        $helper->_default_pagination = $this->_default_pagination;
        $helper->_pagination = $this->_pagination;
        $helper->tpl_vars = $this->getTemplateListVars();
        $helper->tpl_delete_link_vars = $this->tpl_delete_link_vars;

        // For compatibility reasons, we have to check standard actions in class attributes
        foreach ($this->actions_available as $action) {
            if (!in_array($action, $this->actions) && isset($this->$action) && $this->$action) {
                $this->actions[] = $action;
            }
        }

        $helper->is_cms = $this->is_cms;
        $helper->sql = $this->_listsql;
        foreach ($this->_list as $key => $value) {
            $this->_list[$key]['fournisseur'] = $this->fournisseur;
            if (Catalog::getInfoFourn($value['id_order'], $this->fournisseur)) {
                $this->_list[$key]['etat'] = '1';
            } else {
                $this->_list[$key]['etat'] = '0';
            }
        }
        $list = $helper->generateList($this->_list, $this->fields_list);

        return $list;
    }

    public function processSendOrder()
    {
        $result = true;
        $id_order = Tools::getValue('id_order');
        if ($id_order) {
            //send order directly
            $res_json = Catalog::synchroManuelOrder($id_order, 1, $this->fournisseur);
            if (is_null($res = json_decode($res_json, true)) || !is_array($res) || !in_array($this->fournisseur, $res)) {
                $result = false;
            }
            if (!$result) {
                $this->errors[] = Tools::displayError('An error occurred when sending order ' . $id_order);
            } else {
                $this->redirect_after = $this->fromPSOrder ? $this->context->link->getAdminLink('AdminOrders') . '&vieworder&id_order=' . $id_order : self::$currentIndex.'&token='.$this->token;
            }
        } else {
            $this->errors[] = Tools::displayError('You must select an order to send.');
        }

        return $result;
    }

    public function processSimulOrder()
    {
        $result = true;
        
        if ($this->simulation_available) {
            $id_order = Tools::getValue('id_order');
            if (is_numeric($id_order)) {
                $order = new Order((int)$id_order);
                $class = 'Ec' . $this->fournisseur;
                $ec_four = new $class($order->id_shop);
                $order_infos = $ec_four->sendOrder(['id_order' => $id_order], true);
                $this->displayInformation('<b>' . $this->l('Order') . ' ' . (int)$id_order . '</b><pre>' . htmlspecialchars(var_export($order_infos, true)) . '</pre>');
            } else {
                $this->errors[] = Tools::displayError('You must select an order to simulate.');
            }
        }

        return $result;
    }

    protected function processBulkSendSelection()
    {
        $result = true;
        $order_errors = array();
        if (is_array($this->boxes) && !empty($this->boxes)) {
            //send orders directly
            foreach ($this->boxes as $id_order) {
                $res_json = Catalog::synchroManuelOrder($id_order, 1, $this->fournisseur);
                if (!is_null($res = json_decode($res_json, true)) && is_array($res) && in_array($this->fournisseur, $res)) {
                    $result &= true;
                } else {
                    $order_errors[] = $id_order;
                    $result = false;
                }
            }

            if (!$result) {
                $this->errors[] = Tools::displayError('An error occurred for orders ' . implode(', ', $order_errors) . ' see logs.');
            } else {
                $this->redirect_after = self::$currentIndex.'&token='.$this->token;
            }
        } else {
            $this->errors[] = Tools::displayError('You must select at least one order to send.');
        }

        return $result;
    }
}
