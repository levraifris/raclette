<?php
/**
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to a commercial license from SARL Ether Creation
 * Use, copy, modification or distribution of this source file without written
 * license agreement from the SARL Ether Creation is strictly forbidden.
 * In order to obtain a license, please contact us: contact@ethercreation.com
 * ...........................................................................
 * INFORMATION SUR LA LICENCE D'UTILISATION
 *
 * L'utilisation de ce fichier source est soumise a une licence commerciale
 * concedee par la societe Ether Creation
 * Toute utilisation, reproduction, modification ou distribution du present
 * fichier source sans contrat de licence ecrit de la part de la SARL Ether Creation est
 * expressement interdite.
 * Pour obtenir une licence, veuillez contacter la SARL Ether Creation a l'adresse: contact@ethercreation.com
 * ...........................................................................
 *
 * @author    Ether Creation SARL <contact@ethercreation.com>
 * @copyright 2008-2021 Ether Creation SARL
 * @license   Commercial license
 * International Registered Trademark & Property of Ether Creation SARL
 */

require_once dirname(__FILE__) . '/../../class/catalog.class.php';
use ecicdiscountpro\Catalog;

class AdminEciCdiscountproTaxeController extends ModuleAdminController
{
    public function __construct()
    {
        parent::__construct();
        $this->fournisseur = "cdiscountpro";
        $this->bootstrap = true;
        $this->context = Context::getContext();
        $this->id_lang = $this->context->language->id;
        $this->id_shop = $this->context->shop->id;
        $this->default_form_language = $this->context->language->id;
    }

    public function initContent()
    {
        parent::initContent();

        $this->context->smarty->assign(
            array(
                'content' => $this->renderForm().$this->content,
            )
        );
    }

    public function renderForm()
    {
        $this->fields_form = array(
            'form' => array(
                'legend' => array('title' => $this->l('Taxes') . ' ' . $this->fournisseur),
                'input' => array(
                ),
                'submit' => array('title' => $this->l('Save'),)
            ),
        );

        $ps_taxes = array_merge(
            array(
                array(
                    'id_tax_rules_group' => 0,
                    'name' => $this->l('Tax not matched'),
                )
            ),
            TaxRulesGroup::getTaxRulesGroups()
        );
        $this->catalog = new Catalog($this->context);
        $eci_taxes_four = $this->catalog->getTaxMatch();
        $values = array();

        if ($eci_taxes_four) {
            foreach ($eci_taxes_four as $eci_tax_four) {
                $this->fields_form['form']['input'][] = array(
                    'type' => 'select',
                    'name' => 'id_tax_eco' . $eci_tax_four['id_tax_eco'],
                    'label'=> $this->l('VAT rate') . ' : ' . $eci_tax_four['rate'] . '%',
                    'options' => array(
                        'query' => $ps_taxes,
                        'id' => 'id_tax_rules_group',
                        'name' => 'name',
                    ),
                    'col' => 2,
                );
                $values['id_tax_eco' . $eci_tax_four['id_tax_eco']] = $eci_tax_four['id_tax_rules_group'];
            }
        }

        $helper = new HelperForm();
        $helper->show_toolbar = false;
        $helper->default_form_language = $this->id_lang;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
        $helper->submit_action = 'update_eci_taxes';
        $helper->tpl_vars = array(
            'fields_value' => $values,
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->id_lang
        );

        return $helper->generateForm(array($this->fields_form));
    }

    public function postProcess()
    {
        $all = Tools::getAllValues();
        if (!empty($all['update_eci_taxes'])) {
            $this->catalog = new Catalog($this->context);
            $all_good = true;
            foreach ($all as $k => $v) {
                if (!preg_match('/^id_tax_eco[0-9]+$/', $k)) {
                    continue;
                }
                $id_tax_eco = str_replace('id_tax_eco', '', $k);
                $all_good &= $this->catalog->saveTaxMatching($id_tax_eco, (int) $v);
            }

            if ($all_good) {
                $this->confirmations[] = $this->l('Parameters updated successfully');
                return true;
            } else {
                $this->errors[] = $this->l('Parameters did not update');
                return false;
            }
        }
    }
}
