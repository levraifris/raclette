<?php
/**
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to a commercial license from SARL Ether Creation
 * Use, copy, modification or distribution of this source file without written
 * license agreement from the SARL Ether Creation is strictly forbidden.
 * In order to obtain a license, please contact us: contact@ethercreation.com
 * ...........................................................................
 * INFORMATION SUR LA LICENCE D'UTILISATION
 *
 * L'utilisation de ce fichier source est soumise a une licence commerciale
 * concedee par la societe Ether Creation
 * Toute utilisation, reproduction, modification ou distribution du present
 * fichier source sans contrat de licence ecrit de la part de la SARL Ether Creation est
 * expressement interdite.
 * Pour obtenir une licence, veuillez contacter la SARL Ether Creation a l'adresse: contact@ethercreation.com
 * ...........................................................................
 *
 * @author    Ether Creation SARL <contact@ethercreation.com>
 * @copyright 2008-2021 Ether Creation SARL
 * @license   Commercial license
 * International Registered Trademark & Property of Ether Creation SARL
 */

require_once dirname(__FILE__) . '/../../class/catalog.class.php';
use ecicdiscountpro\Catalog;

class AdminEciCdiscountproLogController extends ModuleAdminController
{
    public function __construct()
    {
        $this->fournisseur  = 'cdiscountpro';
        $this->bootstrap = true;
        $this->table = 'log';
        $this->identifier   = 'id_log';
        $this->className = 'PrestaShopLogger';
        $this->lang = false;
        $this->allow_export = true;
        $this->list_no_link = true;

        parent::__construct();
        $this->_select .= 'CONCAT(LEFT(e.firstname, 1), \'. \', e.lastname) employee';
        $this->_join .= ' LEFT JOIN '._DB_PREFIX_.'employee e ON (a.id_employee = e.id_employee)';
        $this->_where = ' AND a.message REGEXP \'^eci' . pSQL($this->fournisseur) . ' \'';

        $this->fields_list = array(
            'id_log' => array(
                'title' => $this->l('ID'),
                'align' => 'text-center',
                'class' => 'fixed-width-xs'
            ),
            'employee' => array(
                'title' => $this->l('Employee'),
                'havingFilter' => true,
                'callback' => 'displayEmployee',
                'callback_object' => $this
            ),
            'severity' => array(
                'title' => $this->l('Severity (1-4)'),
                'align' => 'text-center',
                'class' => 'fixed-width-xs'
            ),
            'message' => array(
                'title' => $this->l('Message'),
                'class' => 'wordwrap'
            ),
            'object_type' => array(
                'title' => $this->l('Object type'),
                'class' => 'fixed-width-sm'
            ),
            'object_id' => array(
                'title' => $this->l('Object ID'),
                'align' => 'center',
                'class' => 'fixed-width-xs'
            ),
            'error_code' => array(
                'title' => $this->l('Error code'),
                'align' => 'center',
                'prefix' => '0x',
                'class' => 'fixed-width-xs'
            ),
            'date_add' => array(
                'title' => $this->l('Date'),
                'align' => 'right',
                'type' => 'datetime'
            )
        );

        $this->actions = array('delete');

        $this->bulk_actions = array(
            'deleteSelection' => array(
                'text' => $this->l('Delete selection'),
                'icon' => 'icon-remove text-danger'
            ),
        );

        $this->fields_options = array(
            'general' => array(
                'title' =>    $this->l('Logs by email'),
                'icon' => 'icon-envelope',
                'fields' =>    array(
                    'min_severity' => array(
                        'title' => $this->l('Minimum severity level'),
                        'no_multishop_checkbox' => true,
                        'hint' => $this->l('Enter "5" if you do not want to receive any emails.'),
                        'cast' => 'intval',
                        'updateOption' => 'min_severity',
                        'auto_value' => false,
                        'value' => (int)Catalog::getECIConfigValue('min_severity', 0, $this->fournisseur),
                        'type' => 'text'
                    ),
                    'logs_email' => array(
                        'title' => $this->l('Logs will be sent to'),
                        'no_multishop_checkbox' => true,
                        'updateOption' => 'logs_email',
                        'auto_value' => false,
                        'value' => Catalog::getECIConfigValue('logs_email', 0, $this->fournisseur),
                        'type' => 'text'
                    )
                ),
                'submit' => array('title' => $this->l('Save'))
            )
        );
    }

    public function initContent()
    {
        parent::initContent();
    }

    public function initToolbar()
    {
        parent::initToolbar();
        $this->toolbar_btn['delete'] = array(
            'short' => 'Erase',
            'desc' => $this->l('Erase all'),
            'js' => 'if (confirm(\''.$this->l('Are you sure?').'\')) document.location = \''.Tools::safeOutput($this->context->link->getAdminLink('AdminEci'.Tools::ucfirst($this->fournisseur).'Log')).'&amp;token='.$this->token.'&amp;deleteall=1\';'
        );
        unset($this->toolbar_btn['new']);
    }

    public function displayEmployee($value, $tr)
    {
        $employee = new Employee((int)$tr['id_employee']);
        return '<span class="employee_avatar_small"><img class="imgm img-thumbnail" alt="" src="'.$employee->getImage().'" width="32" height="32" /></span> '.$value;
    }

    public function updateOptionMinSeverity($value = 5)
    {
        $this->fields_options['general']['fields']['min_severity']['value'] = $value;
        if (is_numeric($value)) {
            Catalog::setECIConfigValue('min_severity', $value, 0, $this->fournisseur);
        } else {
            $this->errors[] = $this->l('Non numeric value');
        }
    }

    public function updateOptionLogsEmail($value = '')
    {
        $this->fields_options['general']['fields']['logs_email']['value'] = $value;
        if (empty($value) || Validate::isEmail($value)) {
            Catalog::setECIConfigValue('logs_email', $value, 0, $this->fournisseur);
        } else {
            $this->errors[] = $this->l('The email is invalid');
        }
    }

    public function initProcess()
    {
        parent::initProcess();

        if (Tools::isSubmit('deleteall')) {
            //if ($this->tabAccess['edit'] === '1') {
                $this->action = 'delete_all';
            //} else {
            //    $this->errors[] = Tools::displayError('You do not have permission to edit this.');
            //}
        } elseif (Tools::isSubmit('delete')) {
            //if ($this->tabAccess['edit'] === '1') {
                $this->action = 'delete';
            //} else {
            //    $this->errors[] = Tools::displayError('You do not have permission to edit this.');
            //}
        } elseif (Tools::isSubmit('updatelog')) {
            $this->display = '';
            $this->action = '';
        }
    }

    public function processDelete()
    {
        $class = $this->className;
        $log = new $class($this->id_object);
        if (!Validate::isLoadedObject($log)) {
            $this->errors[] = Tools::displayError('An error occurred while deleting this log.');
        }
        if (!$log->delete()) {
            $this->errors[] = Tools::displayError('An error occurred while deleting this log.');
        }
        Tools::redirectAdmin(self::$currentIndex.'&token='.$this->token);
    }

    public function processDeleteAll()
    {
        if (Db::getInstance()->execute('DELETE FROM ' . _DB_PREFIX_ . 'log WHERE SUBSTR(message, 1, ' . (int) (3 + Tools::strlen($this->fournisseur)) . ') LIKE "eci' . pSQL($this->fournisseur) . '"')) {
            Tools::redirectAdmin(self::$currentIndex.'&token='.$this->token);
        }
    }

    protected function processBulkDeleteSelection()
    {
        $result = true;
        if (is_array($this->boxes) && !empty($this->boxes)) {
            $result &= Db::getInstance()->execute('DELETE FROM ' . _DB_PREFIX_ . 'log WHERE id_log IN ("' . implode('","', array_map('intval', $this->boxes)) . '")');

            if (!$result) {
                $this->errors[] = Tools::displayError('An error occurred while deleting this selection.');
            }
        } else {
            $this->errors[] = Tools::displayError('You must select at least one element to delete.');
        }

        if (!count($this->errors)) {
            Tools::redirectAdmin(self::$currentIndex.'&token='.$this->token);
        }

        return $result;
    }

    public function setMedia($isNewTheme = false)
    {
        parent::setMedia();

        $this->addCss(_PS_MODULE_DIR_ . 'eci' . $this->fournisseur . '/views/css/log.css', 'all');
    }
}
