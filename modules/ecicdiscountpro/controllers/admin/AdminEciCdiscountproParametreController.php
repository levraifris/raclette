<?php
/**
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to a commercial license from SARL Ether Creation
 * Use, copy, modification or distribution of this source file without written
 * license agreement from the SARL Ether Creation is strictly forbidden.
 * In order to obtain a license, please contact us: contact@ethercreation.com
 * ...........................................................................
 * INFORMATION SUR LA LICENCE D'UTILISATION
 *
 * L'utilisation de ce fichier source est soumise a une licence commerciale
 * concedee par la societe Ether Creation
 * Toute utilisation, reproduction, modification ou distribution du present
 * fichier source sans contrat de licence ecrit de la part de la SARL Ether Creation est
 * expressement interdite.
 * Pour obtenir une licence, veuillez contacter la SARL Ether Creation a l'adresse: contact@ethercreation.com
 * ...........................................................................
 *
 * @author    Ether Creation SARL <contact@ethercreation.com>
 * @copyright 2008-2021 Ether Creation SARL
 * @license   Commercial license
 * International Registered Trademark & Property of Ether Creation SARL
 */

require_once dirname(__FILE__) . '/../../class/catalog.class.php';
require_once dirname(__FILE__) . '/../../class/ecitranslations.class.php';
use ecicdiscountpro\Catalog;
use ecicdiscountpro\EciTranslations;

class AdminEciCdiscountproParametreController extends ModuleAdminController
{
    public $fournisseur = "cdiscountpro";
    public $context;
    public $id_shop;
    public $id_lang;
    public $iso_code;
    public $catalog;
    public $ec_four;
    public $tabs_list;
    public $tabs_state;
    
    public function __construct()
    {
        parent::__construct();
        $this->bootstrap = true;
        $this->context = Context::getContext();
        $this->id_lang = $this->context->language->id;
        $this->iso_code = $this->context->language->iso_code;
        $this->id_shop = $this->context->shop->id;
        $this->default_form_language = $this->id_lang;
        $this->_defaultOrderBy = 'id';
        $this->orderBy = 'id';
        $this->_default_pagination = 50;
        $this->_pagination = array(5, 10, 20, 50, 100, 300, 1000);
        
        if (Tools::getValue('token') && ('yes' === Tools::getValue('getMyIP'))) {
            exit(Tools::getRemoteAddr());
        }
    }

    public function initContent()
    {
        if (is_null(Shop::getContextShopID())) {
            $this->errors[] = $this->l('You must select a store to use this tab.');
            return;
        }

        $this->catalog = new Catalog($this->context);
        $this->ec_four = $this->catalog->getGenClass($this->fournisseur);

        parent::initContent();

        if (method_exists($this->ec_four, 'renderForm')) {
            $content = $this->renderForm();
        } else {
            $this->context->smarty->assign(
                array(
                    'ec_token' => Catalog::getInfoEco('ECO_TOKEN'),
                    'eci_baseDir' => __PS_BASE_URI__,
                    'eci_ajaxlink' => $this->context->link->getModuleLink('eci' . $this->fournisseur, 'ajax'),
                    'id_shop' => $this->id_shop,
                    'fournisseur' => $this->fournisseur,
                    'connecteur' => $this->fournisseur,
                    'orderStates' => OrderState::getOrderStates($this->id_lang)
                )
            );

            $this->context->smarty->assign($this->ec_four->persoFournisseur());

            $content = $this->context->smarty->fetch(_PS_MODULE_DIR_ . 'eci' . $this->fournisseur . '/views/templates/admin/displaymessage.tpl');
            $content .= $this->context->smarty->fetch(_PS_MODULE_DIR_ . 'eci' . $this->fournisseur . '/views/templates/admin/EcParametresSpeConnecteur.tpl');
            $content .= $this->context->smarty->fetch(_PS_MODULE_DIR_ . 'eci' . $this->fournisseur . '/views/templates/admin/persoFournisseur.tpl');
        }

        $this->context->smarty->assign(array('content' => $content.$this->content));
    }

    public function buildFieldsForm()
    {
        $orderStates = OrderState::getOrderStates($this->id_lang);
        
        if (method_exists('Store', 'getStores')) {
            $stores = Store::getStores($this->id_lang);
        } else {
            $stores = Db::getInstance()->executeS('SELECT * FROM '._DB_PREFIX_.'store where active = 1');
        }

        $fields_form_gen = $this->ec_four->renderForm();
        foreach ($fields_form_gen as &$field) {
            $field['label'] = EciTranslations::getInstance()->translate($field['label'], 'ec'.$this->fournisseur.'mom', $this->iso_code);
            if (isset($field['desc'])) {
                $field['desc'] = EciTranslations::getInstance()->translate($field['desc'], 'ec'.$this->fournisseur.'mom', $this->iso_code);
            }
        }
        
        $hours = array();
        for ($h = 0; $h < 24; $h++) {
            $hour = Tools::substr('0' . $h, -2);
            $hours[] = array('hour' => $hour, 'hour_name' => $hour . 'h');
        }

        $build = Catalog::getClassConstant($this->ec_four, 'BUILD');

        $this->fields_form = array(
            'form' => array(
                'legend' => array('title' => $this->l('Parameters') . ' ' . $this->fournisseur . ($build ? '<i style="opacity:.2;float:right;">Build ' . $build . '</i>' : '') . '<i style="opacity:.2;float:right;">v ' . $this->module->version . '</i>', 'icon' => 'icon-cogs'),
                'input' => array(
                    array(
                        'type' => 'switch',
                        'name' => 'ACTIVE',
                        'label'=> $this->l('Activate connector'),
                        'is_bool' => true,
                        'values' => array(
                            array(
                                'id' => 'ACTIVE_on',
                                'value' => 1,
                            ),
                            array(
                                'id' => 'ACTIVE_off',
                                'value' => 0,
                            ),
                        ),
                    ),
                    array(
                        'type' => 'switch',
                        'name' => 'ACTION_PRODUCT_DEREF',
                        'label'=> $this->l('Act on dereferenced products'),
                        'is_bool' => true,
                        'values' => array(
                            array(
                                'id' => 'ACTION_PRODUCT_DEREF_on',
                                'value' => 1,
                            ),
                            array(
                                'id' => 'ACTION_PRODUCT_DEREF_off',
                                'value' => 0,
                            ),
                        ),
                    ),
                    array(
                        'type' => 'switch',
                        'name' => 'UPDATE_PRODUCT_DEREF',
                        'label'=> $this->l('Delete dereferenced products'),
                        'desc'=> $this->l('If set to NO, dereferenced products will only be disabled.'),
                        'is_bool' => true,
                        'values' => array(
                            array(
                                'id' => 'UPDATE_PRODUCT_DEREF_on',
                                'value' => 1,
                            ),
                            array(
                                'id' => 'UPDATE_PRODUCT_DEREF_off',
                                'value' => 0,
                            ),
                        ),
                    ),
                    array(
                        'type' => 'switch',
                        'name' => 'PARAM_NEWPRODUCT',
                        'label'=> $this->l('New products are active'),
                        'is_bool' => true,
                        'values' => array(
                            array(
                                'id' => 'PARAM_NEWPRODUCT_on',
                                'value' => 1,
                            ),
                            array(
                                'id' => 'PARAM_NEWPRODUCT_off',
                                'value' => 0,
                            ),
                        ),
                    ),
                    array(
                        'type' => 'switch',
                        'name' => 'ONLY_LAST_CATEGORY',
                        'label'=> $this->l('Associate product only to its last level category'),
                        'desc'=> $this->l('If set to NO, product will be associated to all the branch.'),
                        'is_bool' => true,
                        'values' => array(
                            array(
                                'id' => 'ONLY_LAST_CATEGORY_on',
                                'value' => 1,
                            ),
                            array(
                                'id' => 'ONLY_LAST_CATEGORY_off',
                                'value' => 0,
                            ),
                        ),
                    ),
                    array(
                        'type' => 'switch',
                        'name' => 'FORCE_PACK_COMP',
                        'label'=> $this->l('Importation of a pack will import all components'),
                        'is_bool' => true,
                        'values' => array(
                            array(
                                'id' => 'FORCE_PACK_COMP_on',
                                'value' => 1,
                            ),
                            array(
                                'id' => 'FORCE_PACK_COMP_off',
                                'value' => 0,
                            ),
                        ),
                    ),
                    array(
                        'type' => 'switch',
                        'name' => 'ASSOC_CARRIERS',
                        'label'=> $this->l('Associate all supplier carriers to its products'),
                        'is_bool' => true,
                        'condition' => true,
                        'values' => array(
                            array(
                                'id' => 'ASSOC_CARRIERS_on',
                                'value' => 1,
                            ),
                            array(
                                'id' => 'ASSOC_CARRIERS_off',
                                'value' => 0,
                            ),
                        ),
                    ),
                    array(
                        'type' => 'switch',
                        'name' => 'STOCK_ENABLE',
                        'label'=> $this->l('Disable products out of stock'),
                        'is_bool' => true,
                        'values' => array(
                            array(
                                'id' => 'STOCK_ENABLE_on',
                                'value' => 1,
                            ),
                            array(
                                'id' => 'STOCK_ENABLE_off',
                                'value' => 0,
                            ),
                        ),
                    ),
                    array(
                        'type' => 'switch',
                        'name' => 'REACTIVATE',
                        'label'=> $this->l('Activate products in stock'),
                        'is_bool' => true,
                        'values' => array(
                            array(
                                'id' => 'REACTIVATE_on',
                                'value' => 1,
                            ),
                            array(
                                'id' => 'REACTIVATE_off',
                                'value' => 0,
                            ),
                        ),
                    ),
                    array(
                        'type' => 'switch',
                        'name' => 'PRICEWSTOCK',
                        'label'=> $this->l('Update wholesale prices with stock'),
                        'is_bool' => true,
                        'values' => array(
                            array(
                                'id' => 'PRICEWSTOCK_on',
                                'value' => 1,
                            ),
                            array(
                                'id' => 'PRICEWSTOCK_off',
                                'value' => 0,
                            ),
                        ),
                    ),
                    array(
                        'type' => 'switch',
                        'name' => 'PMVCWSTOCK',
                        'label'=> $this->l('Update prices with stock'),
                        'is_bool' => true,
                        'values' => array(
                            array(
                                'id' => 'PMVCWSTOCK_on',
                                'value' => 1,
                            ),
                            array(
                                'id' => 'PMVCWSTOCK_off',
                                'value' => 0,
                            ),
                        ),
                    ),
                    array(
                        'type' => 'switch',
                        'name' => 'DAILY_PRICE_UPDATE',
                        'label'=> $this->l('Update prices only once a day'),
                        'is_bool' => true,
                        'values' => array(
                            array(
                                'id' => 'DAILY_PRICE_UPDATE_on',
                                'value' => 1,
                            ),
                            array(
                                'id' => 'DAILY_PRICE_UPDATE_off',
                                'value' => 0,
                            ),
                        ),
                    ),
                    array(
                        'type' => 'select',
                        'name' => 'DAILY_PRICE_UPDATE_TIME',
                        'label'=> $this->l('Update prices only once a day after this time'),
                        'condition' => true,
                        'options' => array(
                            'query' => $hours,
                            'id' => 'hour',
                            'name' => 'hour_name',
                        ),
                        'col' => 2,
                    ),
                    array(
                        'type' => 'switch',
                        'name' => 'IMPORT_AUTO',
                        'label'=> $this->l('Automatic order transmission'),
                        'is_bool' => true,
                        'condition' => true,
                        'values' => array(
                            array(
                                'id' => 'IMPORT_AUTO_on',
                                'value' => 1,
                            ),
                            array(
                                'id' => 'IMPORT_AUTO_off',
                                'value' => 0,
                            ),
                        ),
                    ),
                    array(
                        'type' => 'select',
                        'multiple' => true,
                        'name' => 'IMPORT_OST[]',
                        'label'=> $this->l('Automatically send orders when in these states'),
                        'condition' => true,
                        'options' => array(
                            'query' => $orderStates,
                            'id' => 'id_order_state',
                            'name' => 'name',
                        ),
                        'col' => 2,
                    ),
                    array(
                        'type' => 'select',
                        'multiple' => true,
                        'name' => 'FINAL_OST[]',
                        'label'=> $this->l('Never update orders when in these final states'),
                        'condition' => true,
                        'options' => array(
                            'query' => $orderStates,
                            'id' => 'id_order_state',
                            'name' => 'name',
                        ),
                        'col' => 2,
                    ),
                    array(
                        'type' => 'switch',
                        'name' => 'IMPORT_ADDRESS',
                        'label'=> $this->l('Use shop address for delivery'),
                        'desc'=> $this->l('If set to NO, customer address will be used'),
                        'is_bool' => true,
                        'condition' => true,
                        'values' => array(
                            array(
                                'id' => 'IMPORT_ADDRESS_on',
                                'value' => 1,
                            ),
                            array(
                                'id' => 'IMPORT_ADDRESS_off',
                                'value' => 0,
                            ),
                        ),
                    ),
                    array(
                        'type' => 'select',
                        'name' => 'STORE_ADDRESS',
                        'label'=> $this->l('Store for delivery'),
                        'condition' => true,
                        'options' => array(
                            'query' => $stores,
                            'id' => 'id_store',
                            'name' => 'name',
                        ),
                        'col' => 2,
                    ),
                    array(
                        'type' => 'switch',
                        'name' => 'NOTVA',
                        'label'=> $this->l('Are you VAT exempt ?'),
                        'desc'=> $this->l('If set to YES, wholesale prices are calculated including VAT'),
                        'is_bool' => true,
                        'values' => array(
                            array(
                                'id' => 'NOTVA_on',
                                'value' => 1,
                            ),
                            array(
                                'id' => 'NOTVA_off',
                                'value' => 0,
                            ),
                        ),
                    ),
                    array(
                        'type' => 'text',
                        'name' => 'MARGE',
                        'label'=> $this->l('Fix markup'),
                        'desc'=> $this->l('Only applies where no price rule applies. If set to 0 → use recommended selling price'),
                        'required' => true,
                        'suffix'=> '%',
                        'col' => '2',
                    ),
                    array(
                        'type' => 'html',
                        'name' => 'title01',
                        'label' => '',
                        'html_content' => '<h3>' . $this->l('Module catalog parameters') . '</h3>',
                        'col' => '12'
                    ),
                    array(
                        'type' => 'switch',
                        'name' => 'SHOW_MANUFACTURER',
                        'label'=> $this->l('Show manufacturers in module catalog'),
                        'is_bool' => true,
                        'condition' => true,
                        'values' => array(
                            array(
                                'id' => 'SHOW_MANUFACTURER_on',
                                'value' => 1,
                            ),
                            array(
                                'id' => 'SHOW_MANUFACTURER_off',
                                'value' => 0,
                            ),
                        ),
                    ),
                    array(
                        'type' => 'switch',
                        'name' => 'SHOW_REFERENCE',
                        'label'=> $this->l('Show public references in module catalog'),
                        'is_bool' => true,
                        'condition' => true,
                        'values' => array(
                            array(
                                'id' => 'SHOW_REFERENCE_on',
                                'value' => 1,
                            ),
                            array(
                                'id' => 'SHOW_REFERENCE_off',
                                'value' => 0,
                            ),
                        ),
                    ),
                ),
                'submit' => array('title' => $this->l('Save'),)
            ),
        );

        $this->fields_form['form']['input'] = array_merge($this->fields_form['form']['input'], $fields_form_gen);
        
        $this->fields_form_supplier = array(
            'form' => array(
                'legend' => array('title' => $this->l('Reset Supplier'), 'icon' => 'icon-cogs'),
                'input' => array(
                    array(
                        'type' => 'html',
                        'name' => 'text_reset_supplier',
                        'label' => '',
                        'html_content' => $this->l('Verify or reinstall module and connector suppliers'),
                        'col' => '12',
                    ),
                ),
                'submit' => array('title' => $this->l('Reset'),),
            ),
        );
        
        $this->fields_form_tab = array(
            'form' => array(
                'legend' => array('title' => $this->l('Reset Tabs'), 'icon' => 'icon-cogs'),
                'input' => array(
                    array(
                        'type' => 'html',
                        'name' => 'text_reset_tabs',
                        'label' => '',
                        'html_content' => $this->l('Reset Tabs according to connector configuration'),
                        'col' => '12',
                    ),
                ),
                'submit' => array('title' => $this->l('Reset'),),
            ),
        );
        
        $this->tabs_list = Db::getInstance()->executeS(
            'SELECT id_tab, class_name, active
            FROM ' . _DB_PREFIX_ . 'tab
            WHERE module = "eci'.pSQL($this->fournisseur).'"
            AND id_parent = (SELECT MAX(id_parent) FROM ' . _DB_PREFIX_ . 'tab WHERE module = "eci'.pSQL($this->fournisseur).'")'
        );
        $this->fields_form_tab_choice = array(
            'form' => array(
                'legend' => array('title' => $this->l('Choose active tabs'), 'icon' => 'icon-cogs'),
                'input' => array(
                    array(
                        'type' => 'html',
                        'name' => 'text_choose_tabs',
                        'label' => '',
                        'html_content' => $this->l('Activate thoose tabs'),
                        'col' => '12',
                    ),
                    array(
                        'type' => 'select',
                        'multiple' => true,
                        'name' => 'chosen_tabs[]',
                        'options' => [
                            'query' => $this->tabs_list,
                            'id' => 'id_tab',
                            'name' => 'class_name',
                        ],
                    ),
                ),
                'submit' => array('title' => $this->l('Save'),),
            ),
        );
        
        $this->fields_form_hook = array(
            'form' => array(
                'legend' => array('title' => $this->l('Reset hooks'), 'icon' => 'icon-cogs'),
                'input' => array(
                    array(
                        'type' => 'html',
                        'name' => 'text_reset_hooks',
                        'label' => '',
                        'html_content' => $this->l('Reset hooks according to connector configuration'),
                        'col' => '12',
                    ),
                ),
                'submit' => array('title' => $this->l('Reset'),),
            ),
        );
        
        $this->fields_form_token = array(
            'form' => array(
                'legend' => array('title' => $this->l('Reset token'), 'icon' => 'icon-cogs'),
                'input' => array(
                    array(
                        'type' => 'html',
                        'name' => 'text_reset_token',
                        'label' => '',
                        'html_content' => $this->l('Reset module token : will change cron links'),
                        'col' => '12',
                    ),
                ),
                'submit' => array('title' => $this->l('Reset'),),
            ),
        );
        
        $this->fields_form_fname = array(
            'form' => array(
                'legend' => array('title' => $this->l('Reset friendly name'), 'icon' => 'icon-cogs'),
                'input' => array(
                    array(
                        'type' => 'html',
                        'name' => 'text_reset_fname',
                        'label' => '',
                        'html_content' => $this->l('Reset supplier Friendly Name'),
                        'col' => '12',
                    ),
                ),
                'submit' => array('title' => $this->l('Reset'),),
            ),
        );
        
        $this->fields_form_maint = array(
            'form' => array(
                'legend' => array('title' => $this->l('Maintenance'), 'icon' => 'icon-cogs'),
                'input' => array(
                    array(
                        'type' => 'html',
                        'name' => 'text_add_ip_maint',
                        'label' => '',
                        'html_content' => $this->l('Add missing IPs to maintenance'),
                        'col' => '12',
                    ),
                ),
                'submit' => array('title' => $this->l('Add'),),
            ),
        );
        
        $this->fields_form_actr = array(
            'form' => array(
                'legend' => array('title' => $this->l('Reset Activation Reason'), 'icon' => 'icon-cogs'),
                'input' => array(
                    array(
                        'type' => 'html',
                        'name' => 'text_reset_actreason',
                        'label' => '',
                        'html_content' => $this->l('Reset activation reasons for all products'),
                        'col' => '12',
                    ),
                ),
                'submit' => array('title' => $this->l('Reset'),),
            ),
        );
    }

    public function getLabel($name)
    {
        if (empty($this->fields_form)) {
            $this->buildFieldsForm();
        }

        if (empty($this->fields_form['form']['input'])) {
            return '';
        }

        foreach ($this->fields_form['form']['input'] as $input) {
            if (isset($input['name']) && preg_replace('/\[.*$/', '', $input['name']) == $name) {
                return isset($input['label']) ? $input['label'] : '';
            }
        }

        return '';
    }

    public function filterHiddenFields()
    {
        $ec_class = 'ec' . $this->fournisseur;
        if (defined($ec_class.'::HIDE_PARMS')
            && !is_null($hide_parms = Tools::jsonDecode($ec_class::HIDE_PARMS))
            && is_array($hide_parms)) {
            foreach ($this->fields_form['form']['input'] as $key => $input) {
                if (in_array(preg_replace('/\[.*$/', '', $input['name']), $hide_parms)) {
                    $this->fields_form['form']['input'][$key]['condition'] = false;
                }
            }
        }
    }

    public function setFormNamesToConfig()
    {
        foreach ($this->fields_form['form']['input'] as $input) {
            if (!empty($input['lang'])) {
                if (isset($this->ec_four->config[$input['name']]) && !is_array($this->ec_four->config[$input['name']])) {
                    $this->ec_four->config[$input['name']] = Catalog::getMultiLangField($this->ec_four->config[$input['name']]);
                }
            }
            if (empty($input['name']) || !strstr($input['name'], '[')) {
                continue;
            }
            $name = $input['name'];
            $matches = array();
            $depth = preg_match_all('/\[.*?\]/', $name, $matches);
            $var_name = preg_replace('/\[.*$/', '', $name);
            $index1 = str_replace(array('[', ']'), '', $matches[0][0]);
            $this->ec_four->config[$name] = '';
            if (Tools::strlen($index1)) {
                if (isset($this->ec_four->config[$var_name])) {
                    if (isset($this->ec_four->config[$var_name][$index1])) {
                        $this->ec_four->config[$name] = $this->ec_four->config[$var_name][$index1];
                    }
                }
            } elseif (isset($this->ec_four->config[$var_name])) {
                $this->ec_four->config[$name] = $this->ec_four->config[$var_name];
            }
        }
        
        if (!empty($this->tabs_list) && is_array($this->tabs_list)) {
            foreach ($this->tabs_list as $tab_infos) {
                if ($tab_infos['active']) {
                    $this->tabs_state['chosen_tabs[]'][] = $tab_infos['id_tab'];
                }
            }
        }
    }

    public function renderForm()
    {
        $this->buildFieldsForm();

        $this->filterHiddenFields();

        $this->setFormNamesToConfig();

        $helper = new HelperForm();
        $helper->show_toolbar = false;
        $helper->default_form_language = $this->id_lang;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
        $helper->submit_action = 'update_eciconfiguration';
        $helper->tpl_vars = array(
            'fields_value' => $this->ec_four->config,
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->id_lang
        );
        
        $helper_supplier = new HelperForm();
        $helper_supplier->show_toolbar = false;
        $helper_supplier->default_form_language = $this->id_lang;
        $helper_supplier->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
        $helper_supplier->submit_action = 'reset_supplier';
        $helper_supplier->tpl_vars = array(
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->id_lang
        );
        
        $helper_tab = new HelperForm();
        $helper_tab->show_toolbar = false;
        $helper_tab->default_form_language = $this->id_lang;
        $helper_tab->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
        $helper_tab->submit_action = 'reset_tabs';
        $helper_tab->tpl_vars = array(
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->id_lang
        );
        
        
        $helper_tab_choice = new HelperForm();
        $helper_tab_choice->show_toolbar = false;
        $helper_tab_choice->default_form_language = $this->id_lang;
        $helper_tab_choice->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
        $helper_tab_choice->submit_action = 'choose_tabs';
        $helper_tab_choice->tpl_vars = array(
            'fields_value' => $this->tabs_state,
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->id_lang
        );
        
        $helper_hook = new HelperForm();
        $helper_hook->show_toolbar = false;
        $helper_hook->default_form_language = $this->id_lang;
        $helper_hook->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
        $helper_hook->submit_action = 'reset_hooks';
        $helper_hook->tpl_vars = array(
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->id_lang
        );

        $helper_token = new HelperForm();
        $helper_token->show_toolbar = false;
        $helper_token->default_form_language = $this->id_lang;
        $helper_token->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
        $helper_token->submit_action = 'reset_token';
        $helper_token->tpl_vars = array(
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->id_lang
        );

        $helper_fname = new HelperForm();
        $helper_fname->show_toolbar = false;
        $helper_fname->default_form_language = $this->id_lang;
        $helper_fname->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
        $helper_fname->submit_action = 'reset_fname';
        $helper_fname->tpl_vars = array(
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->id_lang
        );

        $helper_maint = new HelperForm();
        $helper_maint->show_toolbar = false;
        $helper_maint->default_form_language = $this->id_lang;
        $helper_maint->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
        $helper_maint->submit_action = 'add_ip_maint';
        $helper_maint->tpl_vars = array(
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->id_lang
        );

        $helper_actr = new HelperForm();
        $helper_actr->show_toolbar = false;
        $helper_actr->default_form_language = $this->id_lang;
        $helper_actr->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
        $helper_actr->submit_action = 'reset_actreason';
        $helper_actr->tpl_vars = array(
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->id_lang
        );

        return $helper->generateForm(array($this->fields_form))
            . $helper_supplier->generateForm(array($this->fields_form_supplier))
            . $helper_tab->generateForm(array($this->fields_form_tab))
            . $helper_tab_choice->generateForm(array($this->fields_form_tab_choice))
            . $helper_hook->generateForm(array($this->fields_form_hook))
            . $helper_token->generateForm(array($this->fields_form_token))
            . $helper_fname->generateForm(array($this->fields_form_fname))
            . $helper_maint->generateForm(array($this->fields_form_maint))
            . $helper_actr->generateForm(array($this->fields_form_actr));
    }

    public function postProcess()
    {
        $ec_class = 'ec' . $this->fournisseur;
        if (!method_exists($ec_class, 'renderForm')) {
            return true;
        }

        $this->ec_four = Catalog::getGenClassStatic($this->fournisseur, $this->id_shop);

        $all = Tools::getAllValues();
        if (!empty($all['update_eciconfiguration'])) {
            foreach (array_merge(Catalog::$tabParamsNumeric, $this->ec_four->numeric_conf_keys) as $num_key) {
                if (isset($all[$num_key]) && !is_numeric($all[$num_key])) {
                    $this->errors[] = $this->l('At least one numeric value is not numeric') . ' : "' . $this->getLabel($num_key) . '"';
                    return false;
                }
            }

            Catalog::updateParamFournisseur(
                http_build_query($all),
                $this->fournisseur,
                $this->id_shop,
                array_merge(Catalog::$tabParamsValid, $this->ec_four->valid_conf_keys),
                array_merge(Catalog::$tabParamsNumeric, $this->ec_four->numeric_conf_keys),
                array_merge(Catalog::$tabParamsMultiple, $this->ec_four->multiple_conf_keys),
                array_merge(Catalog::$tabParamsArray, $this->ec_four->array_conf_keys),
                array_merge(Catalog::$tabParamsAllShops, $this->ec_four->allshop_keys),
                array_merge(Catalog::$tabParamsFiles, $this->ec_four->uploaded_files ?? array()),
                array_merge(Catalog::$tabParamsLang, $this->ec_four->lang_keys ?? array())
            );

            if (!empty($this->ec_four->uploaded_files) && is_array($this->ec_four->uploaded_files)) {
                $dir = dirname(__FILE__) . '/../../files/import/';
                foreach ($this->ec_four->uploaded_files as $file_name => $rename) {
                    if (is_null($file_infos = Tools::fileAttachment($file_name)) || empty($file_infos['name']) || empty($file_infos['content'])) {
                        continue;
                    }

                    file_put_contents($dir . ($rename ? $rename : $file_infos['name']), $file_infos['content']);
                }
            }

            $this->ec_four = Catalog::getGenClassStatic($this->fournisseur, $this->id_shop);

            $this->confirmations[] = $this->l('Parameters updated successfully');
            return true;
        } elseif (!empty($all['reset_supplier'])) {
            $this->context->employee = new Employee(Catalog::getEciEmployeeId());
            $eciClass = 'Eci' . Tools::ucfirst($this->fournisseur);
            $mod = $eciClass;
            $mod::installECISupplier();
            $mod::installGenSupplier();
            
            $this->warnings[] = $this->l('Please be sure to rebuild product_supplier data for this supplier !');
            
            return true;
        } elseif (!empty($all['reset_tabs'])) {
            $this->context->employee = new Employee(Catalog::getEciEmployeeId());
            $eciClass = 'Eci' . Tools::ucfirst($this->fournisseur);
            $mod = new $eciClass($this->context);
            $mod->uninstallTabs();
            $mod->installTabs();
            $this->redirect_after = $this->context->link->getAdminLink('Admin'.$eciClass.'Parametre', false);
            
            return true;
        } elseif (!empty($all['choose_tabs'])) {
            if (empty($all['chosen_tabs']) || !is_array($all['chosen_tabs'])) {
                return true;
            }
            Db::getInstance()->update(
                'tab',
                ['active' => (int) 0,],
                'module = "eci' . pSQL($this->fournisseur) . '" AND icon != "power" AND class_name NOT LIKE"%Parametre"'
            );
            Db::getInstance()->update(
                'tab',
                ['active' => (int) 1,],
                'module = "eci' . pSQL($this->fournisseur) . '" AND icon != "power" AND id_tab IN ("' . implode('","', $all['chosen_tabs']) . '")'
            );
            $eciClass = 'Eci' . Tools::ucfirst($this->fournisseur);
            $this->redirect_after = $this->context->link->getAdminLink('Admin'.$eciClass.'Parametre', true);
            
            return true;
        } elseif (!empty($all['reset_hooks'])) {
            $this->context->employee = new Employee(Catalog::getEciEmployeeId());
            $eciClass = 'Eci' . $this->fournisseur;
            $mod = new $eciClass($this->context);
            $mod->uninstallHooks();
            $mod->installHooks();
            
            return true;
        } elseif (!empty($all['reset_token'])) {
            Catalog::updateInfoEco('ECO_TOKEN', md5(time() . _COOKIE_KEY_));
            
            return true;
        } elseif (!empty($all['reset_fname'])) {
            $ec_four = Catalog::getGenClassStatic($this->fournisseur, $this->id_shop);
            $id_supplier = Catalog::getEciFournId($this->fournisseur);
            $gen_conf = Catalog::getClassConstant($ec_four, 'GEN_CONF', true);
            $friendly_name = $gen_conf['friendly_name'] ?? Tools::ucwords($this->fournisseur);
            $res = Db::getInstance()->update(
                'eci_fournisseur',
                array(
                    'friendly_name' => pSQL($friendly_name)
                ),
                'id_manufacturer = ' . (int) $id_supplier
            );
            
            return $res;
        } elseif (!empty($all['add_ip_maint'])) {
            // get all IPs including the user IP
            $ips_raw = array();
            $ips_raw[] = Tools::getRemoteAddr() ?: null; //remote address
            $ips_raw[] = filter_input(INPUT_SERVER, 'SERVER_ADDR', FILTER_SANITIZE_STRING) ?: null; //server address
            $ips_raw[] = Tools::file_get_contents($this->context->link->getAdminLink('AdminEci'.Tools::ucfirst($this->fournisseur).'Parametre', true, null, array('getMyIP' => 'yes')));
            $ips_raw[] = Tools::file_get_contents('http://79.137.79.204/getMyIP.php?motdepasse='.md5(gmdate('YmdH')));
            $ips = array_unique(array_filter($ips_raw));
            sort($ips);

            // get all configured maintenance IPs
            $ps_maintenance_ip_multishop = Configuration::getMultiShopValues('PS_MAINTENANCE_IP');
            // complete configuration for each shop
            foreach ($ps_maintenance_ip_multishop as $id_shop => $ps_maintenance_ip) {
                if (true && $this->id_shop !== $id_shop) {
                    continue;
                }
                $id_shop_group = Shop::getGroupFromShop($id_shop);
                $maintenance_ips_raw = $ps_maintenance_ip ? explode(',', $ps_maintenance_ip) : array();
                $maintenance_ips = array_unique(array_map('trim', $maintenance_ips_raw));
                foreach ($ips as $ip) {
                    if (!in_array($ip, $maintenance_ips)) {
                        $maintenance_ips[] = $ip;
                    }
                }
                $ps_maintenance_ip_new = implode(',', $maintenance_ips);
                if ($ps_maintenance_ip_new != $ps_maintenance_ip) {
                    Configuration::updateValue('PS_MAINTENANCE_IP', $ps_maintenance_ip_new, false, $id_shop_group, $id_shop);
                }
            }

            return true;
        } elseif (!empty($all['reset_actreason'])) {
            Catalog::resetProductShopActReasons($this->fournisseur, $this->id_shop);
            
            return true;
        }
    }

    public function setMedia($isNewTheme = false)
    {
        parent::setMedia();

        $ec_class = 'ec' . $this->fournisseur;
        if (method_exists($ec_class, 'renderForm')) {
            $this->addJs(_PS_MODULE_DIR_ . 'eci' . $this->fournisseur . '/views/js/newparametre.js', 'all');
        } else {
            $this->addJs(_PS_MODULE_DIR_ . 'eci' . $this->fournisseur . '/views/js/parametre.js', 'all');
            $this->addCss(_PS_MODULE_DIR_ . 'eci' . $this->fournisseur . '/views/css/param.css', 'all');
        }
    }
}
