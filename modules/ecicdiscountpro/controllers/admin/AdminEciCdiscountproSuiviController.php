<?php
/**
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to a commercial license from SARL Ether Creation
 * Use, copy, modification or distribution of this source file without written
 * license agreement from the SARL Ether Creation is strictly forbidden.
 * In order to obtain a license, please contact us: contact@ethercreation.com
 * ...........................................................................
 * INFORMATION SUR LA LICENCE D'UTILISATION
 *
 * L'utilisation de ce fichier source est soumise a une licence commerciale
 * concedee par la societe Ether Creation
 * Toute utilisation, reproduction, modification ou distribution du present
 * fichier source sans contrat de licence ecrit de la part de la SARL Ether Creation est
 * expressement interdite.
 * Pour obtenir une licence, veuillez contacter la SARL Ether Creation a l'adresse: contact@ethercreation.com
 * ...........................................................................
 *
 * @author    Ether Creation SARL <contact@ethercreation.com>
 * @copyright 2008-2021 Ether Creation SARL
 * @license   Commercial license
 * International Registered Trademark & Property of Ether Creation SARL
 */

require_once dirname(__FILE__) . '/../../class/catalog.class.php';
require_once dirname(__FILE__) . '/../../gen/cdiscountpro/class/eccdiscountpro.php';
use ecicdiscountpro\Catalog;

class AdminEciCdiscountproSuiviController extends ModuleAdminController
{
    public function __construct()
    {
        parent::__construct();
        $this->fournisseur = "cdiscountpro";
        $this->bootstrap = true;
        $this->table = 'eci_tracking';
        $this->identifier = 'id_order';
        $this->context = Context::getContext();
        $this->id_shop = $this->context->shop->id;
        $this->id_lang = $this->context->language->id;
        $this->default_form_language = $this->context->language->id;
        $this->_defaultOrderBy = 'id';
        $this->orderBy = 'id';
        $this->_default_pagination = 50;
        $this->_pagination = array(5, 10, 20, 50, 100, 300, 1000);
        $this->_use_found_rows = true;
        $this->allow_export = true;
        $this->list_no_link = true;

        $this->_where = ' AND a.fournisseur = "' . pSQL($this->fournisseur) . '"';

        $this->fromPSOrder = 'AdminOrders' == Tools::getValue('controller');
        if ($this->fromPSOrder) {
            $this->token = Tools::getAdminTokenLite('AdminOrders');
        }

        $id_order = Tools::getValue('id_order', null);
        if (isset($id_order)) {
            $this->_where .= ' AND a.id_order = ' . (int) $id_order;
        }

        $this->toolbar_title = $this->l('Order tracking') . ' ' . Catalog::getFriendlyName();

        $this->fields_list = array();
        $this->fields_list['id_order'] = array(
            'title' => $this->l('ID Order'),
            'type' => 'int',
            'search' => !$this->fromPSOrder,
            'orderby' => !$this->fromPSOrder,
            'align' => 'center',
            'class' => 'fixed-width-xs',
            'callback' => 'getOrderLink',
        );
        $this->fields_list['order_state_name'] = array(
            'title' => $this->l('Order state'),
            'type' => 'text',
            'search' => !$this->fromPSOrder,
            'orderby' => !$this->fromPSOrder,
        );
        $this->fields_list['date_modif_state'] = array(
            'title' => $this->l("Date state"),
            'type' => 'date',
            'search' => !$this->fromPSOrder,
            'orderby' => !$this->fromPSOrder,
        );
        $this->fields_list['date_exp'] = array(
            'title' => $this->l("Date exp"),
            'type' => 'text',
            'search' => !$this->fromPSOrder,
            'orderby' => !$this->fromPSOrder,
        );
        $this->fields_list['numero'] = array(
            'title' => $this->l('Tracking number'),
            'type' => 'text',
            'search' => !$this->fromPSOrder,
            'orderby' => !$this->fromPSOrder,
        );
        $this->fields_list['transport'] = array(
            'title' => $this->l('Shipping mode'),
            'type' => 'text',
            'search' => !$this->fromPSOrder,
            'orderby' => !$this->fromPSOrder,
        );
        $this->fields_list['url_exp'] = array(
            'title' => $this->l('Tracking Url'),
            'type' => 'text',
            'search' => !$this->fromPSOrder,
            'orderby' => !$this->fromPSOrder,
        );
        $this->fields_list['products'] = array(
            'title' => $this->l('Products'),
            'type' => 'text',
            'search' => !$this->fromPSOrder,
            'orderby' => !$this->fromPSOrder,
        );
    }

    public function getOrderLink($id_order)
    {
        if (empty($id_order)) {
            return;
        }

        if ($this->fromPSOrder) {
            return $id_order;
        }

        return '<a href="' . $this->context->link->getAdminLink('AdminOrders') . '&vieworder&id_order=' . $id_order . '" target="_blank">' . $id_order . '</a>';
    }

    public function initContent()
    {
        if (is_null(Shop::getContextShopID())) {
            $this->errors[] = $this->l('You must select a store to use this tab.');
            return;
        }

        return parent::initContent();
    }

    public function initToolbar()
    {
        parent::initToolbar();
        unset($this->toolbar_btn['new']);
    }

    public function renderList()
    {
        $hide_tabs = Catalog::getClassConstant('ec'.$this->fournisseur, 'HIDE_TABS', true);
        if (is_array($hide_tabs) && in_array('Orders process', $hide_tabs)) {
            return;
        }
        
        $this->getList($this->id_lang);
        
        if (empty($this->_list)) {
            return;
        }

        return parent::renderList();
    }
}
