<?php
/**
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to a commercial license from SARL Ether Creation
 * Use, copy, modification or distribution of this source file without written
 * license agreement from the SARL Ether Creation is strictly forbidden.
 * In order to obtain a license, please contact us: contact@ethercreation.com
 * ...........................................................................
 * INFORMATION SUR LA LICENCE D'UTILISATION
 *
 * L'utilisation de ce fichier source est soumise a une licence commerciale
 * concedee par la societe Ether Creation
 * Toute utilisation, reproduction, modification ou distribution du present
 * fichier source sans contrat de licence ecrit de la part de la SARL Ether Creation est
 * expressement interdite.
 * Pour obtenir une licence, veuillez contacter la SARL Ether Creation a l'adresse: contact@ethercreation.com
 * ...........................................................................
 *
 * @author    Ether Creation SARL <contact@ethercreation.com>
 * @copyright 2008-2021 Ether Creation SARL
 * @license   Commercial license
 * International Registered Trademark & Property of Ether Creation SARL
 */

require_once dirname(__FILE__).'/../../class/catalog.class.php';
require_once dirname(__FILE__).'/../../class/reference.class.php';
require_once dirname(__FILE__) . '/../../class/ecitranslations.class.php';
use ecicdiscountpro\Catalog;
use ecicdiscountpro\ImporterReference;
use ecicdiscountpro\EciTranslations;

require_once(dirname(__FILE__).'/../../ecicdiscountpro.php');

class AdminEciCdiscountproCatalogueController extends ModuleAdminController
{
    public function __construct()
    {

        parent::__construct();
        $this->fournisseur = "cdiscountpro";
        $this->bootstrap = true;
        $this->table = 'eci_catalog';
        $this->identifier = 'product_reference';
        $this->context = Context::getContext();
        $this->id_lang = $this->context->language->id;
        $this->iso_code = $this->context->language->iso_code;
        $this->id_shop = $this->context->shop->id;
        $this->default_form_language = $this->id_lang;
        $this->_defaultOrderBy = 'category';
        $this->orderBy = 'category';
        $this->_default_pagination = 20;
        $this->_pagination = array(5, 10, 20, 50, 100, 300, 1000);
        $this->show_stock = Catalog::existsEciStock($this->fournisseur);
        $this->_select = ' pi.imported AS act, eps.id_product AS id_product, IF(eps.id_product!=0,1,0) AS etat, IF(b.blacklist IS NULL,0,b.blacklist) AS blacklist';
        $this->_select .= $this->show_stock ? ', SUM(cs.stock) AS stock' : '';
        $this->_join .= ' LEFT JOIN (SELECT product_reference AS reference, id_shop, IF(b2.blacklist IS NULL,0,b2.blacklist) AS blacklist, c2.fournisseur
                                    FROM ' . _DB_PREFIX_ . 'eci_catalog c2
                                    LEFT JOIN ' . _DB_PREFIX_ . 'eci_product_blacklist b2
                                    ON (c2.product_reference = b2.reference AND c2.fournisseur = b2.fournisseur)
                            ) b ON (a.product_reference = b.reference AND a.fournisseur = b.fournisseur)';
        $this->_join .= ' LEFT JOIN ' . _DB_PREFIX_ . 'eci_product_imported pi ON (a.product_reference = pi.reference AND a.fournisseur = pi.fournisseur AND pi.id_shop = ' . (int)$this->id_shop . ')';
        $this->_join .= ' LEFT JOIN ' . _DB_PREFIX_ . 'eci_product_shop eps ON (a.product_reference = eps.reference AND a.fournisseur = eps.fournisseur AND eps.id_shop = ' . (int)$this->id_shop . ')';
        $this->_join .= $this->show_stock ? ' LEFT JOIN ' . _DB_PREFIX_ . 'eci_catalog_stock cs ON (a.product_reference = cs.product_reference AND a.fournisseur = cs.fournisseur)' : '';
        $this->_where = 'AND a.fournisseur = "'.pSQL($this->fournisseur).'" ';
        $this->_where .= 'AND (b.id_shop = ' . (int)$this->id_shop . ' OR b.id_shop IS NULL) ';
        $this->_where .= 'AND ((a.keep NOT REGEXP \'' . '"id_shop":[0-9]+' . '\') OR (a.keep REGEXP \'' . '"id_shop":' . (int) $this->id_shop . '[^0-9]' . '\')) ';
        $this->_where .= $this->show_stock ? 'AND (cs.id_shop = ' . (int)$this->id_shop . ' OR cs.id_shop = 0 OR cs.id_shop IS NULL) ' : '';
        $this->_group = 'GROUP BY a.product_reference';
        if (Tools::getIsset($this->table . 'Filter_eps!id_product') && Tools::getValue($this->table . 'Filter_eps!id_product')!="all") {
            $this->_filterHaving = 'etat = '.(int)Tools::getValue($this->table . 'Filter_eps!id_product');
        }

        if (Tools::getIsset('filter_by_category')) {
            $this->filter_by_category = Tools::getValue('filter_by_category', 0);
        } else {
            $this->filter_by_category = isset($this->context->cookie->filter_by_category) ? $this->context->cookie->filter_by_category : 0;
        }
        $this->context->cookie->filter_by_category = $this->filter_by_category;

        if ($this->filter_by_category && Tools::getIsset('selectedcategory')) {
            $this->selectedcategory = Tools::getValue('selectedcategory', 0);
        } else {
            $this->selectedcategory = isset($this->context->cookie->selectedcategory) ? $this->context->cookie->selectedcategory : 0;
        }
        $this->context->cookie->selectedcategory = $this->selectedcategory;

        if (Tools::isSubmit('submitReset'.$this->table)) {
            $this->selectedcategory = false;
            $this->context->cookie->selectedcategory = false;
            $this->filter_by_category = 0;
            $this->context->cookie->filter_by_category = 0;
        }

        if ($this->filter_by_category && $this->selectedcategory) {
//            $this->_where .= ' AND a.category RLIKE "' . pSQL(':://::' . $this->selectedcategory) . '(::>>::.*)?"'; //not accurate in multibranch case
            $t_path = explode('::>>::', $this->selectedcategory);
            $t_pat = array();
            foreach ($t_path as $cat) {
                $t_pat[] = str_replace('"', '\"', trim(json_encode(array($this->iso_code => $cat)), '{}'));
            }
            $pat = '\\\[\{[^}]*' . implode('[^}]*\},\{[^}]*', $t_pat);
            //unicode chars cannot be used in all regexp versions of mysql
            while (($upos = strpos($pat, '\u'))) {
                $pat = substr($pat, 0, $upos) . '.{1,8}' . substr($pat, $upos + 6);
            }
            $this->_where .= " AND a.category_group REGEXP '" . $pat . "'";
        }

        $this->fields_list = array();
        $this->fields_list['id_product'] = array(
            'title' => $this->l('ID'),
            'type' => 'int',
            'callback' => 'getProductLink',
            'search' => true,
            'orderby' => true,
            'align' => 'center',
            'class' => 'fixed-width-xs',
        );
        $this->fields_list['pictures'] = array(
            'title' => $this->l('Pictures'),
            'type' => 'text',
            'callback' => 'getThumbnail',
            'search' => false,
            'orderby' => false,
            'class' => 'showpcard',
        );
        $this->fields_list['name'] = array(
            'title' => $this->l('Name'),
            'type' => 'text',
            'search' => true,
            'orderby' => false,
            'class' => 'showpcard',
        );
        $this->fields_list['product_reference'] = array(
            'title' => $this->l('Supplier Reference'),
            'type' => 'text',
            'search' => true,
            'orderby' => true,
            'class' => 'showpcard',
        );
        if (Catalog::getECIConfigValue('SHOW_REFERENCE', $this->id_shop, $this->fournisseur)) {
            $this->fields_list['reference'] = array(
                'title' => $this->l('Reference'),
                'type' => 'text',
                'search' => true,
                'filter_key' => 'a!reference',
                'orderby' => true,
                'class' => 'showpcard',
            );
        }
        $this->fields_list['category'] = array(
            'title' => $this->l('Category'),
            'type' => 'text',
            'search' => true,
            'orderby' => true,
            'class' => 'showpcard',
        );
        if (Catalog::getECIConfigValue('SHOW_MANUFACTURER', $this->id_shop, $this->fournisseur)) {
            $this->fields_list['manufacturer'] = array(
                'title' => $this->l('Manufacturer'),
                'type' => 'text',
                'search' => true,
                'orderby' => true,
                'class' => 'showpcard',
            );
        }
        $this->fields_list['price'] = array(
            'title' => $this->l('Buy price (TE)'),
            'type' => 'price',
            'search' => true,
            'orderby' => true,
            'class' => 'showpcard fixed-width-xs',
            'align' => 'center',
        );
        $this->fields_list['pmvc'] = array(
            'title' => $this->l('Sell price (TE)'),
            'type' => 'price',
            'search' => true,
            'orderby' => true,
            'class' => 'showpcard fixed-width-xs',
            'align' => 'center',
        );
        if ($this->show_stock) {
            $this->fields_list['stock'] = array(
                'title' => $this->l('Stock'),
                'type' => 'int',
                'search' => true,
                'orderby' => true,
                'class' => 'showpcard fixed-width-xs',
                'align' => 'center',
            );
        }
        $this->fields_list['etat'] = array(
            'title' => $this->l('In Shop'),
            'type' => 'bool',
            'callback' => 'printEtat',
            'search' => true,
            'orderby' => true,
            'class' => 'showpcard fixed-width-xs',
            'align' => 'center',
            //'filter_key' => 'eps!id_product',
            'filter_key' => 'etat',
            'filter_type' => 'int',
            'havingFilter' => true,
        );
        $this->fields_list['act'] = array(
            'title' => $this->l('Action'),
            'type' => 'select',
            'list' => [1 => $this->l('Import'), 2 => $this->l('Delete')],
            'callback' => 'printAct',
            'search' => true,
            'orderby' => true,
            'class' => 'fixed-width-xs',
            'align' => 'center',
            'filter_key' =>'pi!imported',
            'filter_type' => 'int',
            //'filter_key' =>'act',
        );
        $this->fields_list['blacklist'] = array(
            'title' => $this->l('Blacklist'),
            'type' => 'bool',
            'callback' => 'printBlacklist',
            'search' => true,
            'orderby' => true,
            'align' => 'text-center',
            'class' => 'fixed-width-xs',
        );

        $this->bulk_actions = array(
            'enableSelection' => array(
                'text' => $this->l('Enable selection'),
                'icon' => 'icon-power-off text-success'
            ),
            'disableSelection' => array(
                'text' => $this->l('Disable selection'),
                'icon' => 'icon-power-off text-danger'
            ),
            'divider' => array(
                'text' => 'divider'
            ),
            'markImportSel' => array(
                'text'    => $this->l('Mark selection for import'),
                'icon'    => 'icon-share',
            ),
            'markDeleteSel' => array(
                'text'    => $this->l('Mark selection for delete'),
                'icon'    => 'icon-remove',
            ),
            'unmark' => array(
                'text'    => $this->l('Unmark all actions'),
                'icon'    => 'icon-question',
                'confirm' => $this->l('Unmark all actions ?'),
            ),
            'markImportCat' => array(
                'text'    => $this->l('Mark category for import'),
                'icon'    => 'icon-share',
            ),
            'markDeleteCat' => array(
                'text'    => $this->l('Mark category for delete'),
                'icon'    => 'icon-remove',
                'confirm' => $this->l('Mark all category for delete ?'),
            ),
            'markImportCag' => array(
                'text'    => $this->l('Mark catalog for import'),
                'icon'    => 'icon-share',
            ),
            'markDeleteCag' => array(
                'text'    => $this->l('Mark catalog for delete'),
                'icon'    => 'icon-remove',
                'confirm' => $this->l('Mark all catalog for delete ?'),
            ),
            'blackListSel' => array(
                'text'    => $this->l('Blacklist selection'),
                'icon'    => 'icon-ban',
            ),
            'whiteListSel' => array(
                'text'    => $this->l('Whitelist selection'),
                'icon'    => 'icon-check',
            ),
            'blackListCat' => array(
                'text'    => $this->l('Blacklist category'),
                'icon'    => 'icon-ban',
            ),
            'whiteListCat' => array(
                'text'    => $this->l('Whitelist category'),
                'icon'    => 'icon-check',
            ),
        );
    }

    public function verifMaintenance()
    {
        return;
        
//        // Attention : context
//        $ps_maintenance_ip = Configuration::get('PS_MAINTENANCE_IP');
//        $maintenance_ips_raw = $ps_maintenance_ip ? explode(',', $ps_maintenance_ip) : array();
//        $maintenance_ips = array_unique(array_map('trim', $maintenance_ips_raw));
//        $remoteAddr = Tools::getRemoteAddr();
//        $serverAddr = filter_input(INPUT_SERVER, 'SERVER_ADDR', FILTER_SANITIZE_STRING);
//        if (!in_array($remoteAddr, $maintenance_ips)) {
//            $maintenance_ips[] = $remoteAddr;
//        }
//        if (!in_array($serverAddr, $maintenance_ips)) {
//            $maintenance_ips[] = $serverAddr;
//        }
//        $ps_maintenance_ip_new = implode(',', $maintenance_ips);
//        if ($ps_maintenance_ip_new != $ps_maintenance_ip) {
//            Configuration::updateValue('PS_MAINTENANCE_IP', $ps_maintenance_ip_new);
//        }
    }

    public function getThumbnail($row)
    {
        if (empty($row)) {
            return;
        }

        return '<img class="imgm img-thumbnail" style="height:55px" src="' . $row . '" alt="" title="" />';
    }

    public function getProductLink($id_product)
    {
        if (empty($id_product)) {
            return;
        }

        return '<a href="' . $this->context->link->getAdminLink('adminproducts') . '&updateproduct&id_product=' . $id_product.'" target="_blank">' . $id_product . '</a>';
    }

    public function printEtat($value)
    {
        return ($value ? '<i style="color:#72c279;" class="icon-check"></i>' : '<i style="color:#e08f95;" class="icon-remove"></i>');
    }

    public function printAct($value, $product)
    {
        return '<a class="'.(1===$value?'list-action-enable action-enabled':(2===$value?'list-action-enable action-disabled':'')).'" href="index.php?'.htmlspecialchars('tab=AdminEciCdiscountproCatalogue&product_reference='
            .urlencode($product['product_reference']).'&oldactvalue='.$value.'&changeActVal&token='.Tools::getAdminTokenLite('AdminEciCdiscountproCatalogue')).'">
            '.(1 == $value ? '<i class="icon-check"></i>' : (2 == $value ? '<i class="icon-remove"></i>' : '<i class="icon-question"></i>')).
            '</a>';
    }

    public function printBlacklist($value, $product)
    {
        return '<a class="list-action-enable '.($value ? 'action-enabled' : 'action-disabled')
            .'" href="index.php?'.htmlspecialchars('tab=AdminEciCdiscountproCatalogue&product_reference='
            .urlencode($product['product_reference']).'&oldblacklistvalue='.$value
            .'&changeBlacklistVal&token='.Tools::getAdminTokenLite('AdminEciCdiscountproCatalogue')).'">'
            .($value ? '<i class="icon-ban"></i>' : '<i class="icon-remove"></i>').'</a>';
    }

    public function initContent()
    {
        if (is_null(Shop::getContextShopID())) {
            $this->errors[] = $this->l('You must select a store to use this tab.');
            return;
        }

        parent::initContent();

        $this->verifMaintenance();
        $link = new Link();
        $tacheslink = $link->getAdminLink('AdminEciCdiscountproTask');
        $eci_ajaxLink = $this->context->link->getModuleLink('eci' . $this->fournisseur, 'ajax');
        $countimp = Catalog::countSynchroImport($this->fournisseur, $this->id_shop);
        $countdel = Catalog::countSynchroDelete($this->fournisseur, $this->id_shop);
        $totalprod = Catalog::getTotalProduct($this->fournisseur);
        $productinshop = Catalog::getTotalProductInShop($this->fournisseur, $this->id_shop);

        $listTasks = Catalog::getTasks(
            array(
                'ECI_CC' => array('hide' => 1, 'nobuttons' => 1),
                'ECI_SS' => array('hide' => 1, 'nobuttons' => 1, 'max' => 1)
            )
        );
        foreach ($listTasks as &$task) {
            $task['name'] = EciTranslations::getInstance()->translate(
                $task['name'],
                ($task['specific'] ? 'ec' . $this->fournisseur . 'mom' : 'catalogmom'),
                $this->iso_code
            );
        }
        $this->context->smarty->assign(
            [
                'listTasks' => $listTasks,
            ]
        );
        $param = $this->context->smarty->fetch(_PS_MODULE_DIR_ . 'eci' . $this->fournisseur . '/views/templates/admin/EcCpanels.tpl');
        $this->context->smarty->assign(array('param' => $param));

        $this->context->smarty->assign(
            array(
                'fournisseur' => $this->fournisseur,
                'countimp' => $countimp,
                'countdel' => $countdel,
                'totalprod' => $totalprod,
                'productinshop' => $productinshop,
                'tacheslink' => $tacheslink,
                'eci_ajaxlink' => $eci_ajaxLink,
                'id_shop' => (int) $this->id_shop,
                'iso_code' => $this->iso_code,
                'ec_token' => Catalog::getInfoEco('ECO_TOKEN'),
                'eci_baseDir' => __PS_BASE_URI__,
                'filter_by_category' => $this->filter_by_category,
            )
        );
        $categories = new HelperTreeCategories('associated-categories-tree', $this->l('Filter by category'));
        if ($this->filter_by_category && $this->selectedcategory) {
            $categories->setSelectedCategories(array($this->selectedcategory));
            self::$currentIndex .= '&selectedcategory='.urlencode($this->selectedcategory);
        }
        $message = "";
        if (Tools::getValue('updcatalog') == "1") {
            $message = $this->displayConfirmation($this->l('Successful update'));
        }
        $content = $this->context->smarty->fetch(_PS_MODULE_DIR_ . 'eci' . $this->fournisseur . '/views/templates/admin/catalogcat.tpl');

        $iso_code = Language::getIsoById(Configuration::get('PS_LANG_DEFAULT'));
        //$iso_code = $this->context->language->iso_code;
        $list = Db::getInstance()->executeS(
            'SELECT DISTINCT(name) AS category
            FROM '._DB_PREFIX_.'eci_category_shop
            WHERE fournisseur = "'.pSQL($this->fournisseur).'"
            AND id_shop = ' . (int) $this->id_shop . '
            AND blacklist = 0
            AND populated = 1
            AND iso_code = "' . pSQL($iso_code) .'"
            ORDER BY name DESC'
        );
        if (!$list) {
            $list = Db::getInstance()->executeS(
                'SELECT DISTINCT(name) AS category
                FROM '._DB_PREFIX_.'eci_category_shop
                WHERE fournisseur = "'.pSQL($this->fournisseur).'"
                AND id_shop = ' . (int) $this->id_shop . '
                AND blacklist = 0
                AND populated = 1
                ORDER BY name DESC'
            );
        }
        $this->tabbb = array();
        $cpt=1;
        foreach ($list as $key => $value) {
            $category = explode('::>>::', $value['category']);
            $list[$key]['category'] = end($category);

            $test = array();
            $n = count($category) - 1;
            $testarbo = $category[0];
            foreach ($category as $key => $value) {
                if ($key != 0) {
                    $testarbo .= '::>>::'.$value;
                }
                $test[] = $testarbo;
            }
            $arbo = array();
            $stringarbo = $category[$n];
            $arbo['0'] = array(
                'id_category' => $test[$n],
                'name' => $category[$n],
                'link_rewrite' => $category[$n],
                'id_lang' => 1
            );
            for ($i = $n-1; $i >= 0; $i--) {
                $stringarbo = $category[$i]."::>>::".$stringarbo;
                $arbo['0'] = array(
                    'id_category' => $test[$i],
                    'name' => $category[$i],
                    'link_rewrite' => $category[$i],
                    'id_lang' => $cpt,
                    'children' => ($i < 0 ? '' : $arbo)
                );
            }
            self::tabEncode1($arbo);
            if ($cpt == 1) {
                $this->tabbb = $arbo;
            } else {
                $this->tabbb = self::arMergeRecursive($arbo, $this->tabbb);
            }
            $cpt++;
        }
        $this->context->smarty->assign(array(
            'content' => $message.$content.$categories->render($this->tabbb).$this->content,
        ));
    }

    private function arMergeRecursive(array $array1, array $array2 = array())
    {
        $val = $array1;
        $merge = $array2;
        $arrays = func_get_args();
        $merge = array_shift($arrays);

        foreach ($arrays as $array) {
            foreach ($array as $key => $val) {
                # if the value is an array and the key already exists
                # we have to make a recursion
                if (is_array($val) && array_key_exists($key, $merge)) {
                    $val = self::arMergeRecursive((array)$merge[$key], $val);
                }

                # if the key is numeric, the value is pushed. Otherwise, it replaces
                # the value of the _merge_ array.
                if (is_numeric($key)) {
                    $merge[] = $val;
                } else {
                    $merge[$key] = $val;
                }
            }
        }

        return $merge;
    }

    private function tabEncode1(&$tab)
    {
        foreach ($tab as $key => $val) {
            if (is_array($val)) {
                self::tabEncode1($val);
                $tab[$key] = $val;
            }
            if (is_numeric($key)) {
                $tab[md5($val['id_category'])] = $val;
                unset($tab[$key]);
            }
        }
    }

    public function renderList()
    {
        $this->addRowAction('view');
        if (!($this->fields_list && is_array($this->fields_list))) {
            return false;
        }
        //unfilter categories (field 'category') if already filtered in field 'category_group' by the upper filtering select
        if ($this->filter_by_category && $this->selectedcategory) {
            $this->_filter = preg_replace('/AND `category` LIKE \'%.*?%\' /', '', $this->_filter);
            unset($this->context->cookie->{$this->getCookieFilterPrefix() . $this->table . 'Filter_' . 'category'});
        }

        $this->getList($this->context->language->id);

//        dump($this->_listsql);

        if (!$this->filter_by_category) {
            unset($this->bulk_actions['markImportCat']);
            unset($this->bulk_actions['markDeleteCat']);
            unset($this->bulk_actions['blackListCat']);
            unset($this->bulk_actions['whiteListCat']);
        }

        $helper = new HelperList();

        // Empty list is ok
        if (!is_array($this->_list)) {
            $this->displayWarning($this->l('Bad SQL query', 'Helper').'<br />'.htmlspecialchars($this->_list_error));
            return false;
        }

        $this->setHelperDisplay($helper);

        $helper->_default_pagination = $this->_default_pagination;
        $helper->_pagination = $this->_pagination;
        $helper->tpl_vars = $this->getTemplateListVars();
        $helper->tpl_delete_link_vars = $this->tpl_delete_link_vars;

        // For compatibility reasons, we have to check standard actions in class attributes
        foreach ($this->actions_available as $action) {
            if (!in_array($action, $this->actions) && isset($this->$action) && $this->$action) {
                $this->actions[] = $action;
            }
        }

        $helper->is_cms = $this->is_cms;
        $helper->sql = $this->_listsql;
        $this->catarbo = array();
        $this->tabb = array();
        $default_iso_code = Language::getIsoById(Configuration::get('PS_LANG_DEFAULT'));
        $iso_code = $this->context->language->iso_code;

        foreach ($this->_list as $key => $value) {
            $category_lg = Catalog::langExpl($this->_list[$key]['category']);
            $category_arb = isset($category_lg[$default_iso_code]) ? $category_lg[$default_iso_code] : reset($category_lg);
            $category = explode('::>>::', $category_arb);
            $this->_list[$key]['category'] = end($category);
            $list_images = array();
            if (!empty($this->_list[$key]['pictures'])) {
                if (($tab_images = Tools::jsonDecode($this->_list[$key]['pictures'], true)) === null) {
                    $list_images = explode('//:://', $this->_list[$key]['pictures']);
                } else {
                    foreach ($tab_images as $image) {
                        $list_images[] = $image['url'];
                    }
                }
            }
            if ($list_images) {
                $this->_list[$key]['pictures'] = $list_images[0];
            } else {
                $other_pictures = Db::getInstance()->executeS(
                    'SELECT pictures
                    FROM ' . _DB_PREFIX_ . 'eci_catalog_attribute
                    WHERE product_reference = "' . pSQL($this->_list[$key]['product_reference']) . '"
                    AND fournisseur = "' . pSQL($this->fournisseur) . '"'
                );
                foreach ($other_pictures as $pictures) {
                    if (!empty($pictures['pictures'])) {
                        if (($tab_images = Tools::jsonDecode($pictures['pictures'], true)) === null) {
                            array_merge($list_images, explode('//:://', $pictures['pictures']));
                        } else {
                            foreach ($tab_images as $image) {
                                $list_images[] = $image['url'];
                            }
                        }
                    }
                }
                if ($list_images) {
                    $this->_list[$key]['pictures'] = $list_images[0];
                }
            }

            $name = Catalog::langExpl($this->_list[$key]['name']);
            $this->_list[$key]['name'] = isset($name[$iso_code]) ? $name[$iso_code] : $name[$default_iso_code];
            $this->_list[$key]['act'] = empty($this->_list[$key]['act']) ? 0 : (int) $this->_list[$key]['act'];
        }
        unset($helper->toolbar_btn['new']);
        $list = $helper->generateList($this->_list, $this->fields_list);

        //$this->displayWarning($this->_listsql);

        return $list;
    }

    public function initProcess()
    {
        parent::initProcess();

        if (Tools::isSubmit('changeBlacklistVal') && Tools::isSubmit('product_reference') && Tools::isSubmit('oldblacklistvalue')) {
            //if ($this->tabAccess['edit'] === '1') {
                $this->product_reference = Tools::getValue('product_reference');
                $this->oldblacklistvalue = Tools::getValue('oldblacklistvalue');
                $this->action = 'change_blacklist_val';
            //} else {
            //    $this->errors[] = Tools::displayError('You do not have permission to edit this.');
            //}
        } elseif (Tools::isSubmit('changeActVal') && Tools::isSubmit('product_reference') && Tools::isSubmit('oldactvalue')) {
            //if ($this->tabAccess['edit'] === '1') {
                $this->product_reference = Tools::getValue('product_reference');
                $this->oldactvalue = Tools::getValue('oldactvalue');
                $this->action = 'change_act_val';
            //} else {
            //    $this->errors[] = Tools::displayError('You do not have permission to edit this.');
            //}
        }
    }

    public function processChangeBlacklistVal()
    {
        $catalog = new Catalog($this->context);
        if ($catalog->productBlacklist($this->fournisseur, $this->product_reference, (1 - $this->oldblacklistvalue), $this->id_shop)) {
            $catalog->setSelection($this->fournisseur);
        } else {
            $this->errors[] = Tools::displayError('An error occurred while updating product information.');
        }
        //Tools::redirectAdmin(self::$currentIndex.'&token='.$this->token);
    }

    public function processChangeActVal()
    {
        //0->1->2 (2 only if product in shop)
        $id_fournisseur = Catalog::getEciFournId($this->fournisseur);
        $ids = new ImporterReference($this->product_reference, $id_fournisseur, $this->id_shop);
        switch ((int)$this->oldactvalue) {
            case 0:
                if ($ids->id_product) {
                    if (Db::getInstance()->insert(
                        'eci_product_imported',
                        array(
                            'reference' => pSQL($this->product_reference),
                            'id_shop' => (int) $this->id_shop,
                            'imported' => 2,
                            'fournisseur' => pSQL($this->fournisseur)
                        ),
                        false,
                        false,
                        Db::ON_DUPLICATE_KEY
                    )) {
                        $catalog = new Catalog($this->context);
                        $catalog->setSelection($this->fournisseur);
                    } else {
                        $this->errors[] = Tools::displayError('An error occurred while updating product information.');
                    }
                } else {
                    if (Db::getInstance()->insert(
                        'eci_product_imported',
                        array(
                            'reference' => pSQL($this->product_reference),
                            'id_shop' => (int) $this->id_shop,
                            'imported' => 1,
                            'fournisseur' => pSQL($this->fournisseur)
                        ),
                        false,
                        false,
                        Db::ON_DUPLICATE_KEY
                    )) {
                        $catalog = new Catalog($this->context);
                        $catalog->setSelection($this->fournisseur);
                    } else {
                        $this->errors[] = Tools::displayError('An error occurred while updating product information.');
                    }
                }
                break;
            default:
                if (Db::getInstance()->delete(
                    'eci_product_imported',
                    'reference = "' . pSQL($this->product_reference) . '"
                    AND id_shop = ' . (int) $this->id_shop . '
                    AND fournisseur = "' . pSQL($this->fournisseur) .'"'
                )) {
                    $catalog = new Catalog($this->context);
                    $catalog->setSelection($this->fournisseur);
                } else {
                    $this->errors[] = Tools::displayError('An error occurred while updating product information.');
                }
                break;
        }

        Tools::redirectAdmin(self::$currentIndex.'&token='.$this->token);
    }

    protected function processBulkMarkImportSel()
    {
        $result = true;
        if (is_array($this->boxes) && !empty($this->boxes)) {
            //get refs and update imported table
            $insert = array();
            foreach ($this->boxes as $ref) {
                $insert[] = array(
                    'reference' => pSQL($ref),
                    'id_shop' => (int) $this->id_shop,
                    'imported' => (int) 1,
                    'fournisseur' => pSQL($this->fournisseur)
                );
                if (500 < count($insert)) {
                    $result &= Db::getInstance()->insert('eci_product_imported', $insert, false, false, Db::REPLACE);
                    $insert = array();
                }
            }
            if ($insert) {
                $result &= Db::getInstance()->insert('eci_product_imported', $insert, false, false, Db::REPLACE);
            }
            $result &= Db::getInstance()->execute(
                'DELETE i
                FROM ' . _DB_PREFIX_ . 'eci_product_imported i
                LEFT JOIN ' . _DB_PREFIX_ . 'eci_product_shop s
                ON i.reference = s.reference
                AND i.fournisseur = s.fournisseur
                AND i.id_shop = s.id_shop
                WHERE i.id_shop = ' . (int) $this->id_shop . '
                AND i.fournisseur = "' . pSQL($this->fournisseur) . '"
                AND s.id_product IS NOT NULL'
            );

            if ($result) {
                $catalog = new Catalog($this->context);
                $result &= $catalog->setSelection($this->fournisseur);
                //redirect
                //$this->redirect_after = self::$currentIndex.'&token='.$this->token; // not this link !
            } else {
                $this->errors[] = Tools::displayError('An error occurred while marking this selection for import.');
            }
        } else {
            $this->errors[] = Tools::displayError('You must select at least one element to mark.');
        }

        return $result;
    }

    protected function processBulkMarkDeleteSel()
    {
        $result = true;
        if (is_array($this->boxes) && !empty($this->boxes)) {
            //get refs and update imported table
            $insert = array();
            foreach ($this->boxes as $ref) {
                $insert[] = array(
                    'reference' => pSQL($ref),
                    'id_shop' => (int) $this->id_shop,
                    'imported' => (int) 2,
                    'fournisseur' => pSQL($this->fournisseur)
                );
                if (500 < count($insert)) {
                    $result &= Db::getInstance()->insert('eci_product_imported', $insert, false, false, Db::REPLACE);
                    $insert = array();
                }
            }
            if ($insert) {
                $result &= Db::getInstance()->insert('eci_product_imported', $insert, false, false, Db::REPLACE);
            }
            $result &= Db::getInstance()->execute(
                'DELETE i
                FROM ' . _DB_PREFIX_ . 'eci_product_imported i
                LEFT JOIN ' . _DB_PREFIX_ . 'eci_product_shop s
                ON i.reference = s.reference
                AND i.fournisseur = s.fournisseur
                AND i.id_shop = s.id_shop
                WHERE i.id_shop = ' . (int) $this->id_shop . '
                AND i.fournisseur = "' . pSQL($this->fournisseur) . '"
                AND s.id_product IS NULL'
            );

            if ($result) {
                $catalog = new Catalog($this->context);
                $result &= $catalog->setSelection($this->fournisseur);
                //redirect
                //$this->redirect_after = self::$currentIndex.'&token='.$this->token; // not this link !
            } else {
                $this->errors[] = Tools::displayError('An error occurred while marking this selection for delete.');
            }
        } else {
            $this->errors[] = Tools::displayError('You must select at least one element to mark.');
        }

        return $result;
    }

    protected function processBulkUnmark()
    {
        $result = true;
        //delete all infos about this fournisseur in this shop in the imported table
        $result &= Db::getInstance()->delete(
            'eci_product_imported',
            'id_shop = ' . (int) $this->id_shop . ' AND fournisseur = "' . pSQL($this->fournisseur) . '"'
        );

        if ($result) {
            $catalog = new Catalog($this->context);
            $result &= $catalog->setSelection($this->fournisseur);
            //redirect
            //$this->redirect_after = self::$currentIndex.'&token='.$this->token; // not this link !
        } else {
            $this->errors[] = Tools::displayError('An error occurred while unmarking actions.');
        }

        return $result;
    }

    protected function processBulkMarkImportCat()
    {
        $result = true;
        $iso_lang = $this->context->language->iso_code;
        if ($this->selectedcategory) {
            //get all refs in the catalog that match the category and send them to imported
            $result &= Db::getInstance()->execute(
                'REPLACE INTO ' . _DB_PREFIX_ . 'eci_product_imported
                (reference, id_shop, imported, fournisseur)
                SELECT product_reference, "' . (int) $this->id_shop . '", "1", fournisseur
                FROM ' . _DB_PREFIX_ . 'eci_catalog
                WHERE category LIKE "%' . pSQL($iso_lang . ':://::' . $this->selectedcategory) . '%"
                AND fournisseur = "' . pSQL($this->fournisseur) . '"'
            );

            $result &= Db::getInstance()->execute(
                'DELETE i
                FROM ' . _DB_PREFIX_ . 'eci_product_imported i
                LEFT JOIN ' . _DB_PREFIX_ . 'eci_product_shop s
                ON i.reference = s.reference
                AND i.fournisseur = s.fournisseur
                AND i.id_shop = s.id_shop
                WHERE i.id_shop = ' . (int) $this->id_shop . '
                AND i.fournisseur = "' . pSQL($this->fournisseur) . '"
                AND s.id_product IS NOT NULL'
            );

            if ($result) {
                $catalog = new Catalog($this->context);
                $result &= $catalog->setSelection($this->fournisseur);
                //redirect
                //$this->redirect_after = self::$currentIndex.'&token='.$this->token; // not this link !
            } else {
                $this->errors[] = Tools::displayError('An error occurred while marking this category for import.');
            }
        } else {
            $this->errors[] = Tools::displayError('You must select a category to import.');
        }

        return $result;
    }

    protected function processBulkMarkDeleteCat()
    {
        $result = true;
        $iso_lang = $this->context->language->iso_code;
        if ($this->selectedcategory) {
            //get all refs in the catalog that match the category and send them to imported
            $result &= Db::getInstance()->execute(
                'REPLACE INTO ' . _DB_PREFIX_ . 'eci_product_imported
                (reference, id_shop, imported, fournisseur)
                SELECT product_reference, "' . (int) $this->id_shop . '", "2", fournisseur
                FROM ' . _DB_PREFIX_ . 'eci_catalog
                WHERE category LIKE "%' . pSQL($iso_lang . ':://::' . $this->selectedcategory) . '%"
                AND fournisseur = "' . pSQL($this->fournisseur) . '"'
            );

            $result &= Db::getInstance()->execute(
                'DELETE i
                FROM ' . _DB_PREFIX_ . 'eci_product_imported i
                LEFT JOIN ' . _DB_PREFIX_ . 'eci_product_shop s
                ON i.reference = s.reference
                AND i.fournisseur = s.fournisseur
                AND i.id_shop = s.id_shop
                WHERE i.id_shop = ' . (int) $this->id_shop . '
                AND i.fournisseur = "' . pSQL($this->fournisseur) . '"
                AND s.id_product IS NULL'
            );

            if ($result) {
                $catalog = new Catalog($this->context);
                $result &= $catalog->setSelection($this->fournisseur);
                //redirect
                //$this->redirect_after = self::$currentIndex.'&token='.$this->token; // not this link !
            } else {
                $this->errors[] = Tools::displayError('An error occurred while marking this category for delete.');
            }
        } else {
            $this->errors[] = Tools::displayError('You must select a category to delete.');
        }

        return $result;
    }

    protected function processBulkMarkImportCag()
    {
        $result = true;
        //get all refs in the catalog and send them to imported
        $result &= Db::getInstance()->execute(
            'REPLACE INTO ' . _DB_PREFIX_ . 'eci_product_imported
            (reference, id_shop, imported, fournisseur)
            SELECT product_reference, "' . (int) $this->id_shop . '", "1", fournisseur
            FROM ' . _DB_PREFIX_ . 'eci_catalog
            WHERE fournisseur = "' . pSQL($this->fournisseur) . '"'
        );

        $result &= Db::getInstance()->execute(
            'DELETE i
            FROM ' . _DB_PREFIX_ . 'eci_product_imported i
            LEFT JOIN ' . _DB_PREFIX_ . 'eci_product_shop s
            ON i.reference = s.reference
            AND i.fournisseur = s.fournisseur
            AND i.id_shop = s.id_shop
            WHERE i.id_shop = ' . (int) $this->id_shop . '
            AND i.fournisseur = "' . pSQL($this->fournisseur) . '"
            AND s.id_product IS NOT NULL'
        );

        if ($result) {
            $catalog = new Catalog($this->context);
            $result &= $catalog->setSelection($this->fournisseur);
            //redirect
            //$this->redirect_after = self::$currentIndex.'&token='.$this->token; // not this link !
        } else {
            $this->errors[] = Tools::displayError('An error occurred while marking this catalog for import.');
        }

        return $result;
    }

    protected function processBulkMarkDeleteCag()
    {
        $result = true;
        //get all refs in the catalog and send them to delete
        $result &= Db::getInstance()->execute(
            'REPLACE INTO ' . _DB_PREFIX_ . 'eci_product_imported
            (reference, id_shop, imported, fournisseur)
            SELECT product_reference, "' . (int) $this->id_shop . '", "2", fournisseur
            FROM ' . _DB_PREFIX_ . 'eci_catalog
            WHERE fournisseur = "' . pSQL($this->fournisseur) . '"'
        );

        $result &= Db::getInstance()->execute(
            'DELETE i
            FROM ' . _DB_PREFIX_ . 'eci_product_imported i
            LEFT JOIN ' . _DB_PREFIX_ . 'eci_product_shop s
            ON i.reference = s.reference
            AND i.fournisseur = s.fournisseur
            AND i.id_shop = s.id_shop
            WHERE i.id_shop = ' . (int) $this->id_shop . '
            AND i.fournisseur = "' . pSQL($this->fournisseur) . '"
            AND s.id_product IS NULL'
        );

        if ($result) {
            $catalog = new Catalog($this->context);
            $result &= $catalog->setSelection($this->fournisseur);
            //redirect
            //$this->redirect_after = self::$currentIndex.'&token='.$this->token; // not this link !
        } else {
            $this->errors[] = Tools::displayError('An error occurred while marking this catalog for delete.');
        }

        return $result;
    }

    protected function processBulkBlackListSel()
    {
        $result = true;
        if (is_array($this->boxes) && !empty($this->boxes)) {
            $catalog = new Catalog($this->context);
            //get refs and update catalog table
            foreach ($this->boxes as $ref) {
                $result &= $catalog->productBlacklist(
                    $this->fournisseur,
                    $ref,
                    1,
                    $this->id_shop
                );
            }
            if ($result) {
                $result &= $catalog->setSelection($this->fournisseur);
                //redirect
                //$this->redirect_after = self::$currentIndex.'&token='.$this->token; // not this link !
            } else {
                $this->errors[] = Tools::displayError('An error occurred while blacklisting this selection.');
            }
        } else {
            $this->errors[] = Tools::displayError('You must select at least one element to blacklist.');
        }

        return $result;
    }

    protected function processBulkWhiteListSel()
    {
        $result = true;
        if (is_array($this->boxes) && !empty($this->boxes)) {
            $catalog = new Catalog($this->context);
            //get refs and update catalog table
            foreach ($this->boxes as $ref) {
                $result &= $catalog->productBlacklist(
                    $this->fournisseur,
                    $ref,
                    0,
                    $this->id_shop
                );
            }
            if ($result) {
                $result &= $catalog->setSelection($this->fournisseur);
                //redirect
                //$this->redirect_after = self::$currentIndex.'&token='.$this->token; // not this link !
            } else {
                $this->errors[] = Tools::displayError('An error occurred while whitelisting this selection.');
            }
        } else {
            $this->errors[] = Tools::displayError('You must select at least one element to whitelist.');
        }

        return $result;
    }

    protected function processBulkBlackListCat()
    {
        $result = true;
        if ($this->selectedcategory) {
            $iso_lang = $this->context->language->iso_code;
            $catalog = new Catalog($this->context);
            $result &= $catalog->productBlacklistByCategory(
                $this->fournisseur,
                $iso_lang . ':://::' . $this->selectedcategory,
                1
            );
            if ($result) {
                $result &= $catalog->setSelection($this->fournisseur);
                //redirect
                //$this->redirect_after = self::$currentIndex.'&token='.$this->token; // not this link !
            } else {
                $this->errors[] = Tools::displayError('An error occurred while blacklisting this category.');
            }
        } else {
            $this->errors[] = Tools::displayError('You must select a category to blacklist.');
        }

        return $result;
    }

    protected function processBulkWhiteListCat()
    {
        $result = true;
        if ($this->selectedcategory) {
            $iso_lang = $this->context->language->iso_code;
            $catalog = new Catalog($this->context);
            $result &= $catalog->productBlacklistByCategory(
                $this->fournisseur,
                $iso_lang . ':://::' . $this->selectedcategory,
                0
            );
            if ($result) {
                $result &= $catalog->setSelection($this->fournisseur);
                //redirect
                //$this->redirect_after = self::$currentIndex.'&token='.$this->token; // not this link !
            } else {
                $this->errors[] = Tools::displayError('An error occurred while whitelisting this category.');
            }
        } else {
            $this->errors[] = Tools::displayError('You must select a category to whitelist.');
        }

        return $result;
    }

    public function postProcess()
    {
        try {
            if ($this->ajax) {
                // from ajax-tab.php
                $action = Tools::getValue('action');
                // no need to use displayConf() here
                if (!empty($action) && method_exists($this, 'ajaxProcess'.Tools::toCamelCase($action))) {
                    Hook::exec('actionAdmin'.Tools::ucfirst($this->action).'Before', array('controller' => $this));
                    Hook::exec('action'.get_class($this).Tools::ucfirst($this->action).'Before', array('controller' => $this));

                    $return = $this->{'ajaxProcess'.Tools::toCamelCase($action)}();

                    Hook::exec('actionAdmin'.Tools::ucfirst($this->action).'After', array('controller' => $this, 'return' => $return));
                    Hook::exec('action'.get_class($this).Tools::ucfirst($this->action).'After', array('controller' => $this, 'return' => $return));

                    return $return;
                } elseif (!empty($action) && $this->controller_name == 'AdminModules' && Tools::getIsset('configure')) {
                    $module_obj = Module::getInstanceByName(Tools::getValue('configure'));
                    if (Validate::isLoadedObject($module_obj) && method_exists($module_obj, 'ajaxProcess'.$action)) {
                        return $module_obj->{'ajaxProcess'.$action}();
                    }
                } elseif (method_exists($this, 'ajaxProcess')) {
                    return $this->ajaxProcess();
                }
            } else {
                // Process list filtering
                if ($this->filter && $this->action != 'reset_filters') {
                    $this->processFilter();
                }

                // If the method named after the action exists, call "before" hooks, then call action method, then call "after" hooks
                if (!empty($this->action) && method_exists($this, 'process'.Tools::ucfirst(Tools::toCamelCase($this->action)))) {
                    // Hook before action
                    Hook::exec('actionAdmin'.Tools::ucfirst($this->action).'Before', array('controller' => $this));
                    Hook::exec('action'.get_class($this).Tools::ucfirst($this->action).'Before', array('controller' => $this));
                    // Call process
                    $return = $this->{'process'.Tools::toCamelCase($this->action)}();
                    // Hook After Action
                    Hook::exec('actionAdmin'.Tools::ucfirst($this->action).'After', array('controller' => $this, 'return' => $return));
                    Hook::exec('action'.get_class($this).Tools::ucfirst($this->action).'After', array('controller' => $this, 'return' => $return));
                    return $return;
                }
            }
        } catch (PrestaShopException $e) {
            $this->errors[] = $e->getMessage();
        }
        return false;
    }

    public function setMedia($isNewTheme = false)
    {
        parent::setMedia();

        $this->addJs(
            _PS_MODULE_DIR_ .'eci' . $this->fournisseur . '/views/js/catalog.js',
            'all'
        );
        $this->addJs(
            _PS_MODULE_DIR_ .'eci' . $this->fournisseur . '/views/js/tasks.js',
            'all'
        );
        $this->addCss(
            _PS_MODULE_DIR_ .'eci' . $this->fournisseur . '/views/css/catalog.css',
            'all'
        );
    }
}
