<?php
/**
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to a commercial license from SARL Ether Creation
 * Use, copy, modification or distribution of this source file without written
 * license agreement from the SARL Ether Creation is strictly forbidden.
 * In order to obtain a license, please contact us: contact@ethercreation.com
 * ...........................................................................
 * INFORMATION SUR LA LICENCE D'UTILISATION
 *
 * L'utilisation de ce fichier source est soumise a une licence commerciale
 * concedee par la societe Ether Creation
 * Toute utilisation, reproduction, modification ou distribution du present
 * fichier source sans contrat de licence ecrit de la part de la SARL Ether Creation est
 * expressement interdite.
 * Pour obtenir une licence, veuillez contacter la SARL Ether Creation a l'adresse: contact@ethercreation.com
 * ...........................................................................
 *
 * @author    Ether Creation SARL <contact@ethercreation.com>
 * @copyright 2008-2021 Ether Creation SARL
 * @license   Commercial license
 * International Registered Trademark & Property of Ether Creation SARL
 */

require_once dirname(__FILE__) . '/../../class/catalog.class.php';
use ecicdiscountpro\Catalog;

class AdminEciCdiscountproDeclinaisonController extends ModuleAdminController
{
    public function __construct()
    {
        parent::__construct();
        $this->fournisseur = "cdiscountpro";
        $this->bootstrap = true;
        $this->context = Context::getContext();
        $this->id_lang = $this->context->language->id;
        $this->id_shop = $this->context->shop->id;
        $this->default_form_language = $this->id_lang;
        $this->_defaultOrderBy = 'id';
        $this->orderBy = 'id';
        $this->_default_pagination = 50;
        $this->_pagination = array(5, 10, 20, 50, 100, 300, 1000);
        $this->allow_split = false;

        $this->split_context = Catalog::getECIConfigValue('ATTR_SPLIT_CTX', 0, $this->fournisseur);
        if (!$this->split_context) {
            $this->split_context = 'none';
        }

        $this->split_query = array(
            array(
                'id' => 'none',
                'name' => $this->l('No split'),
            ),
            array(
                'id' => 'cat0',
                'name' => $this->l('Category highest level'),
            ),
//            array(
//                'id' => 'catn',
//                'name' => $this->l('Category last level'),
//            ),
            array(
                'id' => 'manu',
                'name' => $this->l('Manufacturer'),
            ),
        );
    }

    public function initContent()
    {
        if (is_null(Shop::getContextShopID())) {
            $this->errors[] = $this->l('You must select a store to use this tab.');
            return;
        }

        parent::initContent();
        $this->catalog = new Catalog($this->context);
        $this->catalog->setAttrSplit();

        $param = $this->catalog->getAttributesParam($this->fournisseur);
        $this->context->smarty->assign(
            array(
                'ec_token' => Catalog::getInfoEco('ECO_TOKEN'),
                'eci_baseDir' => __PS_BASE_URI__,
                'eci_ajaxlink' => $this->context->link->getModuleLink('eci' . $this->fournisseur, 'ajax'),
                'fournisseur' => $this->fournisseur,
                'id_shop' => $this->id_shop
            )
        );
        $messages = $this->context->smarty->fetch(_PS_MODULE_DIR_ . 'eci' . $this->fournisseur . '/views/templates/admin/displaymessage.tpl');
        $formulaire = $this->renderForm();
        $this->context->smarty->assign(array('content' => $messages.$formulaire.$param.$this->content));
    }

    public function renderForm()
    {
        if (!$this->allow_split) {
            return '';
        }

        $this->fields_form = array(
            'form' => array(
                'legend' => array('title' => $this->l('Parameters') . ' ' . $this->fournisseur, 'icon' => 'icon-cogs'),
                'input' => array(
                    array(
                        'type' => 'select',
//                        'multiple' => true,
                        'name' => 'ATTR_SPLIT_CTX',
                        'label'=> $this->l('Context of attributes split if necessary'),
                        'desc' => $this->l('Be carefull as this may be changed only once'),
                        'condition' => true,
                        'options' => array(
                            'query' => $this->split_query,
                            'id' => 'id',
                            'name' => 'name',
                        ),
                        'col' => 2,
                        'disabled' => ($this->split_context && 'none' !== $this->split_context) ? true : false, // prevents the user from changing the split_context
                    ),
                ),
                'submit' => array('title' => $this->l('Save'),)
            ),
        );

        $helper = new HelperForm();
        $helper->show_toolbar = false;
        $helper->default_form_language = $this->id_lang;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
        $helper->submit_action = 'update_attr_split_context';
        $helper->tpl_vars = array(
            'fields_value' => array('ATTR_SPLIT_CTX' => $this->split_context),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->id_lang
        );

        return $helper->generateForm(array($this->fields_form));
    }

    public function renderList()
    {
        return parent::renderList();
    }

    public function postProcess()
    {
        $all = Tools::getAllValues();
        if (!empty($all['update_attr_split_context'])) {
            Catalog::setECIConfigValue('ATTR_SPLIT_CTX', $all['ATTR_SPLIT_CTX'], 0, $this->fournisseur);
            $this->confirmations[] = $this->l('Parameter updated successfully');
            return true;
        }
    }

    public function setMedia($isNewTheme = false)
    {
        parent::setMedia();
        $this->addJs(
            _PS_MODULE_DIR_ .'eci' . $this->fournisseur . '/views/js/declinaisons.js',
            'all'
        );
        $this->addCss(
            _PS_MODULE_DIR_ .'eci' . $this->fournisseur . '/views/css/declinaisons.css',
            'all'
        );
    }
}
