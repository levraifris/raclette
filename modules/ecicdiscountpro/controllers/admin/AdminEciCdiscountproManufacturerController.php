<?php
/**
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to a commercial license from SARL Ether Creation
 * Use, copy, modification or distribution of this source file without written
 * license agreement from the SARL Ether Creation is strictly forbidden.
 * In order to obtain a license, please contact us: contact@ethercreation.com
 * ...........................................................................
 * INFORMATION SUR LA LICENCE D'UTILISATION
 *
 * L'utilisation de ce fichier source est soumise a une licence commerciale
 * concedee par la societe Ether Creation
 * Toute utilisation, reproduction, modification ou distribution du present
 * fichier source sans contrat de licence ecrit de la part de la SARL Ether Creation est
 * expressement interdite.
 * Pour obtenir une licence, veuillez contacter la SARL Ether Creation a l'adresse: contact@ethercreation.com
 * ...........................................................................
 *
 * @author    Ether Creation SARL <contact@ethercreation.com>
 * @copyright 2008-2021 Ether Creation SARL
 * @license   Commercial license
 * International Registered Trademark & Property of Ether Creation SARL
 */

require_once dirname(__FILE__) . '/../../class/catalog.class.php';
use ecicdiscountpro\Catalog;

class AdminEciCdiscountproManufacturerController extends ModuleAdminController
{
    public function __construct()
    {
        parent::__construct();
        $this->fournisseur = "cdiscountpro";
        $this->bootstrap = true;
        $this->context = Context::getContext();
        $this->id_lang = $this->context->language->id;
        $this->id_shop = $this->context->shop->id;
        $this->default_form_language = $this->context->language->id;
        
        Catalog::matchManufacturers();
    }

    public function initContent()
    {
        parent::initContent();

        $this->context->smarty->assign(
            array(
                'content' => $this->renderForm().$this->content,
            )
        );
    }

    public function renderForm()
    {
        $this->fields_form = array(
            'form' => array(
                'legend' => array('title' => $this->l('Manufacturers') . ' ' . $this->fournisseur),
                'input' => array(
                ),
                'submit' => array('title' => $this->l('Save'),)
            ),
        );

        $ps_manufacturers = array_merge(
            [
                [
                    'id_manufacturer' => 0,
                    'name' => $this->l('Create automatically'),
                ]
            ],
            Manufacturer::getManufacturers(false, 0, false)
        );
        $eci_manufacturers_four = Catalog::getManufacturersMatch();
        $values = array();

        if ($eci_manufacturers_four) {
            foreach ($eci_manufacturers_four as $eci_manufacturer_four) {
                $this->fields_form['form']['input'][] = array(
                    'type' => 'select',
                    'name' => 'name' . $eci_manufacturer_four['id_eci_manufacturer'],
                    'label'=> $this->l('Manufacturer') . ' : ' . $eci_manufacturer_four['name'],
                    'options' => array(
                        'query' => $ps_manufacturers,
                        'id' => 'id_manufacturer',
                        'name' => 'name',
                    ),
                    'col' => 2,
                );
                $values['name' . $eci_manufacturer_four['id_eci_manufacturer']] = $eci_manufacturer_four['id_manufacturer'];
            }
        }

        $helper = new HelperForm();
        $helper->show_toolbar = false;
        $helper->default_form_language = $this->id_lang;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
        $helper->submit_action = 'update_eci_manufacturers';
        $helper->tpl_vars = array(
            'fields_value' => $values,
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->id_lang
        );

        return $helper->generateForm(array($this->fields_form));
    }

    public function postProcess()
    {
        $all = Tools::getAllValues();
        if (!empty($all['update_eci_manufacturers'])) {
            $all_good = true;
            foreach ($all as $k => $v) {
                if (!preg_match('/^name[0-9]+$/', $k)) {
                    continue;
                }
                $id = str_replace('name', '', $k);
                $all_good &= Catalog::saveManufacturerMatching($id, (int) $v);
            }

            if ($all_good) {
                $this->confirmations[] = $this->l('Parameters updated successfully');
                return true;
            } else {
                $this->errors[] = $this->l('Parameters did not update');
                return false;
            }
        }
    }
}
