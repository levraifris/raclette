<?php
/**
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to a commercial license from SARL Ether Creation
 * Use, copy, modification or distribution of this source file without written
 * license agreement from the SARL Ether Creation is strictly forbidden.
 * In order to obtain a license, please contact us: contact@ethercreation.com
 * ...........................................................................
 * INFORMATION SUR LA LICENCE D'UTILISATION
 *
 * L'utilisation de ce fichier source est soumise a une licence commerciale
 * concedee par la societe Ether Creation
 * Toute utilisation, reproduction, modification ou distribution du present
 * fichier source sans contrat de licence ecrit de la part de la SARL Ether Creation est
 * expressement interdite.
 * Pour obtenir une licence, veuillez contacter la SARL Ether Creation a l'adresse: contact@ethercreation.com
 * ...........................................................................
 *
 * @author    Ether Creation SARL <contact@ethercreation.com>
 * @copyright 2008-2021 Ether Creation SARL
 * @license   Commercial license
 * International Registered Trademark & Property of Ether Creation SARL
 */

require_once dirname(__FILE__) . '/../../class/ecipricerules.class.php';

class AdminEciCdiscountproPricerulesController extends ModuleAdminController
{
    public $list_papv;
    public $list_query_categ;
    public $list_query_manufacturer;
    public $list_manufacturer;

    public function __construct()
    {
        $this->fournisseur  = 'cdiscountpro';
        $this->bootstrap    = true;
        $this->table        = 'eci_pricerules';
        $this->identifier   = 'id_pricerules';
        $this->className    = 'Pricerules'.$this->fournisseur;

        $this->position_identifier = 'position';
        $this->_defaultOrderBy = 'position';

        parent::__construct();

        $this->context      = Context::getContext();
        $this->id_shop      = $this->context->shop->id;
        //$this->informations[] = $this->l('How to use : The catalog must be loaded after rules adding.');

        $this->list_papv = array(
            1 => $this->l('Wholesale price'),
            0 => $this->l('Retail price')
        );

        $this->list_query_categ = array(array('category' => $this->l('All'), 'val' => ''));
        $lst_categ = Db::getInstance()->executes(
            'SELECT DISTINCT(name)
            FROM '._DB_PREFIX_.'eci_category_shop
            WHERE name != ""
            AND fournisseur = "'.$this->fournisseur.'"
            AND id_shop = '.(int) $this->id_shop.'
            ORDER BY name'
        );
        foreach ($lst_categ as $categ) {
            $categ_clean = str_replace('::>>::', ' / ', $categ['name']);
            $this->list_query_categ[] = array('category' => $categ_clean, 'val' => $categ['name']);
            $this->list_categ[] = $categ_clean;
        }

        $this->list_query_manufacturer = array(array('manufacturer' => $this->l('All'), 'val' => ''));
        $lst_manu = Db::getInstance()->executes(
            'SELECT DISTINCT(manufacturer)
            FROM '._DB_PREFIX_.'eci_catalog
            WHERE manufacturer != ""
            AND fournisseur = "'.$this->fournisseur.'"
            ORDER BY manufacturer'
        );
        foreach ($lst_manu as $manu) {
            $this->list_query_manufacturer[] = array('manufacturer' => $manu['manufacturer'], 'val' => $manu['manufacturer']);
            $this->list_manufacturer[] = $manu['manufacturer'];
        }

        $this->_select .= ' REPLACE(a.category, "::>>::", " / ") as category';
        $this->_where = ' AND fournisseur = "'. $this->fournisseur . '" and id_shop = ' . (int) $this->id_shop;


        //data to the grid of the "view" action
        $this->fields_list = array(
            'id_pricerules'       => array(
                'title' => $this->l('ID'),
                'type'  => 'int',
                'align' => 'center',
                //'class' => 'fixed-width-xs',
            ),
            'name'     => array(
                'title' => $this->l('Name'),
                'type'  => 'text',
            ),
            /*'id_shop' => array(
                'title' => $this->l('Shop name'),
                'type'  => 'text',
            ),*/
            'papv' => array(
                'title' => $this->l('Price rules'),
                'align' => 'center',
                'type' => 'select',
                'filter_key' => 'papv',
                'list' => $this->list_papv,
                'callback' => 'printRuleType',
            ),
            'date_from' => array(
                'title' => $this->l('Beginning'),
                'align' => 'right',
                'type' => 'datetime',
            ),
            'date_to' => array(
                'title' => $this->l('End'),
                'align' => 'right',
                'type' => 'datetime'
            ),
            'price_min'     => array(
                'title' => $this->l('Min price range'),
                'type'  => 'text',
            ),
            'price_max'     => array(
                'title' => $this->l('Max price range'),
                'type'  => 'text',
            ),
            'rate'     => array(
                'title' => $this->l('Price variation (%)'),
                'type'  => 'text',
            ),
            'overhead'     => array(
                'title' => $this->l('Price variation (€)'),
                'type'  => 'text',
            ),
            'category'     => array(
                'title' => $this->l('Category'),
                'type'  => 'text',
            ),
            'manufacturer'     => array(
                'title' => $this->l('Manufacturer'),
                'type'  => 'text',
            ),
            'active'   => array(
                'title'  => $this->l('Status'),
                'type' => 'bool',
                'active' => 'status',
                'align'  => 'text-center',
                //'class'  => 'fixed-width-xs'
            ),
            'position' => array(
                'title' => $this->l('Position'),
                'filter_key' => 'position',
                'position' => 'position',
                'align' => 'center'
            ),
            /*'promo' => array(
                'title' => $this->l('Put the product in promo'),
                'align' => 'center',
                'type' => 'select',
                'filter_key' => 'promo',
                'list' => $this->list_promo,
            )*/
        );

        $this->actions = array('edit', 'copy', 'delete');

        $this->simple_header = false;

        $this->bulk_actions = array(
            'delete' => array(
                'text'    => $this->l('Delete selected'),
                'icon'    => 'icon-trash',
                'confirm' => $this->l('Delete selected items ?'),
            )
        );

        //fields to add/edit form
        $this->fields_form = array(
            'legend' => array(
                'title' => $this->l('Price rules'),
            ),
            'input'  => array(
                'name'   => array(
                    'type'     => 'text',
                    'label'    => $this->l('Name'),
                    'name'     => 'name',
                    'required' => true,
                    'col'      => '4',
                ),
                'active' => array(
                    'type'   => 'switch',
                    'label'  => $this->l('Active'),
                    'name'   => 'active',
                    'is_bool' => true,
                    'required' => true,
                    'values' => array(
                        array(
                            'id'    => 'active_on',
                            'value' => 1,
                            'label' => $this->l('Yes'),
                        ),
                        array(
                            'id'    => 'active_off',
                            'value' => 0,
                            'label' => $this->l('No'),
                        ),
                    ),
                    'default_value' => 1,
                ),
                'fournisseur'   => array(
                    'type'     => 'hidden',
                    'label'    => $this->l('Supplier'),
                    'name'     => 'fournisseur',
                    'default_value'     => $this->fournisseur,
                    'required' => true,
                ),
/*                'id_shop' => array( // test
                    'type' => 'select',
                    'label' => $this->l('Shop'),
                    'name' => 'id_shop',
                    'options' => array(
                        'query' => Shop::getShops(),
                        'id' => 'id_shop',
                        'name' => 'name'
                    ),
                    'condition' => Shop::isFeatureActive(),
                    'default_value' => $this->id_shop
                ),*/
                'id_shop' => array(
                    'type' => 'hidden',
                    'label' => $this->l('Shop'),
                    'name' => 'id_shop',
                    'required' => true,
                    'default_value' => $this->id_shop
                ),
                'papv' => array(
                    'type'   => 'select',
                    'label'  => $this->l('Price calculation from'),
                    'name'   => 'papv',
                    'required' => true,
                    'options' => array(
                        'query' => array(
                            array('papv' => $this->l('Wholesales Price'), 'val' => 1),
                            array('papv' => $this->l('Retail Price'), 'val' => 0),
                        ),
                        'id' => 'val',
                        'name' => 'papv',
                    ),
                    'default_value' => 1,
                ),
                'date_from' => array(
                    'name'   => 'date_from',
                    'label' => $this->l('Beginning'),
                    'type' => 'datetime',
                ),
                'date_to' => array(
                    'name'   => 'date_to',
                    'label' => $this->l('End'),
                    'type' => 'datetime',
                ),
                'price_min' => array(
                    'name' => 'price_min',
                    'label' => $this->l('Minimum price range'),
                    'type' => 'text',
                    'col' => '4',
                ),
                'price_max' => array(
                    'name' => 'price_max',
                    'label' => $this->l('Maximum price range'),
                    'type' => 'text',
                    'col' => '4',
                ),
                'rate' => array(
                    'name' => 'rate',
                    'label' => $this->l('Price variation (%)'),
                    'type' => 'text',
                    'col' => '4',
                ),
                'overhead' => array(
                    'name' => 'overhead',
                    'label' => $this->l('Price variation (€)'),
                    'type' => 'text',
                    'col' => '4',
                ),
                'category' => array(
                    'type'   => 'select',
                    'label'  => $this->l('Supplier category'),
                    'name'   => 'category',
                    'options' => array(
                        'query' => $this->list_query_categ,
                        'id' => 'val',
                        'name' => 'category',
                    ),
                    'default_value' => '',
                ),
                'manufacturer' => array(
                    'type'   => 'select',
                    'label'  => $this->l('Supplier manufacturer'),
                    'name'   => 'manufacturer',
                    'options' => array(
                        'query' => $this->list_query_manufacturer,
                        'id' => 'val',
                        'name' => 'manufacturer',
                    ),
                    'default_value' => '',
                ),
                'position' => array(
                    'type' => 'hidden',  //select
                    'label' => $this->l('Position'),
                    'name' => 'position',
                    'default_value' => Db::getInstance()->getValue(
                        'SELECT IF(MAX(position) IS NULL, 0, 1 + MAX(position)) as position
                        FROM ' . _DB_PREFIX_ . $this->table . '
                        WHERE fournisseur = "' . pSQL($this->fournisseur) . '"
                        AND id_shop = ' . $this->id_shop
                    )
                ),
                'promo'   => array(
                    'type' => 'hidden',  //switch
                    'label' => $this->l('create this rule as a promo'),
                    'name' => 'promo',
                    'values' => array(
                        array(
                            'id' => 'promo_on',
                            'value' => 1,
                        ),
                        array(
                            'id'    => 'promo_off',
                            'value' => 0,
                        ),
                    ),
                    'default_value' => 0,
                ),
            ),

            'submit' => array(
                'title' => $this->l('Save'),
            ),
        );
        //$this->fields_value['fournisseur'] = $this->fournisseur;
    }

    public function ajaxProcessUpdatePositions()
    {
        //$way = (int)Tools::getValue('way');
        //$id = (int)Tools::getValue('id');
        $positions = Tools::getValue('pricerules');
        if (is_array($positions)) {
            foreach ($positions as $position => $value) {
                $pos = explode('_', $value);

                Db::getInstance()->update(
                    'eci_pricerules',
                    array(
                        'position' => (int) $position
                    ),
                    'id_pricerules = '.(int) $pos[2]
                );
            }
            return true;
        }
        return false;
    }

    public function displayCopyLink($token = null, $id = 0)
    {
        $idO = $id;
        $token = $idO;
        $id = $token;
        return '<a href='.self::$currentIndex.'&copy'.$this->table.'&'.$this->identifier.'='.$id.'&token='.$this->token.'  title="Copy" class="copy">
            <i class="icon-copy"></i> '.$this->l('Copy').'
            </a>';
        // onclick="if (confirm('Supprimer cet élément ?\n\nNom test')){return true;}else{event.stopPropagation(); event.preventDefault();};"
    }

    public function initContent()
    {
        if (is_null(Shop::getContextShopID())) {
            $this->errors[] = $this->l('You must select a store to use this tab.');
            return;
        }

        parent::initContent();

        if (Tools::getIsset('copysuccess')) {
            $this->confirmations[] = $this->l('Copy successfully registered.');
        }
    }

    public function initProcess()
    {
        parent::initProcess();
        if (Tools::getIsset('copy'.$this->table)) {
            //if ($this->tabAccess['add'] === '1') {
                $this->action = 'copy';
            //} else {
            //    $this->errors[] = Tools::displayError('You do not have permission to copy this.');
            //}
        }
    }

    public function setMedia($isNewTheme = false)
    {
        parent::setMedia();
        $this->addJqueryUi('ui.widget');
        $this->addJqueryPlugin('tagify');
    }

    public function printRuleType($value)
    {
        return $this->list_papv[$value];
    }

    public function processCopy()
    {
        $tab_dup = Db::getInstance()->getRow(
            'SELECT *
            FROM ' . _DB_PREFIX_ . $this->table . '
            WHERE ' . $this->identifier . ' = ' . (int) $this->id_object
        );

        if ($tab_dup) {
            unset($tab_dup[$this->identifier]);
            $tab_dup['position'] = Db::getInstance()->getValue(
                'SELECT IF(MAX(position) IS NULL, 0, 1 + MAX(position)) as position
                FROM ' . _DB_PREFIX_ . $this->table . '
                WHERE fournisseur = "' . pSQL($this->fournisseur) . '"
                AND id_shop = ' . $this->id_shop
            );

            Db::getInstance()->insert(
                $this->table,
                $tab_dup
            );
        }

        $class = 'Pricerules' . $this->fournisseur;
        $class::matchingRules($this->fournisseur, $this->id_shop);

        Tools::redirectAdmin(self::$currentIndex.'&token='.$this->token.'&copysuccess');
    }

    public function processStatus()
    {
        $object = parent::processStatus();

        $class = 'Pricerules' . $this->fournisseur;
        $class::matchingRules($this->fournisseur, $this->id_shop);

        return $object;
    }
}
