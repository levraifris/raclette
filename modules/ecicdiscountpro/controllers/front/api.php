<?php
/**
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to a commercial license from SARL Ether Creation
 * Use, copy, modification or distribution of this source file without written
 * license agreement from the SARL Ether Creation is strictly forbidden.
 * In order to obtain a license, please contact us: contact@ethercreation.com
 * ...........................................................................
 * INFORMATION SUR LA LICENCE D'UTILISATION
 *
 * L'utilisation de ce fichier source est soumise a une licence commerciale
 * concedee par la societe Ether Creation
 * Toute utilisation, reproduction, modification ou distribution du present
 * fichier source sans contrat de licence ecrit de la part de la SARL Ether Creation est
 * expressement interdite.
 * Pour obtenir une licence, veuillez contacter la SARL Ether Creation a l'adresse: contact@ethercreation.com
 * ...........................................................................
 *
 * @author    Ether Creation SARL <contact@ethercreation.com>
 * @copyright 2008-2021 Ether Creation SARL
 * @license   Commercial license
 * International Registered Trademark & Property of Ether Creation SARL
 */

require_once dirname(__FILE__) . '/../../class/catalog.class.php';
require_once dirname(__FILE__) . '/../../class/apiauth.php';
require_once dirname(__FILE__) . '/../../class/apitools.php';
require_once dirname(__FILE__) . '/../../class/apimethodes.php';

use ecicdiscountpro\Catalog;
use ecicdiscountpro\ApiTools;
use ecicdiscountpro\ApiAuth;
use ecicdiscountpro\ApiMethodes;

class EcicdiscountproApiModuleFrontController extends ModuleFrontController
{
    public $php_self     = 'ecicdiscountproapi';
    protected $namespace = 'ecicdiscountpro';
    public $ajax         = true;
    protected $api;

    public function __construct()
    {
        parent::__construct();

        set_error_handler([get_class(), 'exceptionErrorHandler']);
    }

    public function init()
    {
        //get input
        $this->api = new ApiTools();

        //get out if any problem
        if (!$this->api->inputValidated) {
            $this->api->sendResponse();
        }

        //execute the action
        switch (true) {
            case is_callable('\\' . $this->namespace . '\\' . $this->api->action):
                $this->api->action = '\\' . $this->namespace . '\\' . $this->api->action;
                //if the action should be namespaced, it is now corrected
            case is_callable($this->api->action):
                try {
                    $this->api->setResponse(call_user_func_array($this->api->action, $this->api->parameters));
                } catch (Exception $e) {
                    $this->api->setError($e->getMessage());
                }
                break;
            default:
                $this->api->setError('Command unknown ' . json_encode($this->api->action));
        }

        $this->api->sendResponse();
    }

    protected static function exceptionErrorHandler($severity, $message, $file, $line)
    {
        if (!(error_reporting() & $severity)) {
            // Ce code d'erreur n'est pas inclu dans error_reporting

            return;
        }
        throw new ErrorException($message, 0, $severity, $file, $line);
    }
}
