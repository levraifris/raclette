<?php
/**
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to a commercial license from SARL Ether Creation
 * Use, copy, modification or distribution of this source file without written
 * license agreement from the SARL Ether Creation is strictly forbidden.
 * In order to obtain a license, please contact us: contact@ethercreation.com
 * ...........................................................................
 * INFORMATION SUR LA LICENCE D'UTILISATION
 *
 * L'utilisation de ce fichier source est soumise a une licence commerciale
 * concedee par la societe Ether Creation
 * Toute utilisation, reproduction, modification ou distribution du present
 * fichier source sans contrat de licence ecrit de la part de la SARL Ether Creation est
 * expressement interdite.
 * Pour obtenir une licence, veuillez contacter la SARL Ether Creation a l'adresse: contact@ethercreation.com
 * ...........................................................................
 *
 * @author    Ether Creation SARL <contact@ethercreation.com>
 * @copyright 2008-2021 Ether Creation SARL
 * @license   Commercial license
 * International Registered Trademark & Property of Ether Creation SARL
 */

require_once dirname(__FILE__) . '/../../class/catalog.class.php';
use ecicdiscountpro\Catalog;

class EcicdiscountproAjaxModuleFrontController extends ModuleFrontController
{
    public $ajax;
    protected $majsel;
    protected $ec_token;
    protected $catalog;
    protected $connecteur;
    public $tpl_vars;
    public $template;
    public $content;
    protected $logger;

    private $id_shop;
    protected $id_prg;
    
    public function __construct()
    {
        parent::__construct();
        $this->ajax = true;
        
        $this->logger = Catalog::logStart('ajax');

        set_error_handler([get_class(), 'exceptionErrorHandler']);
    }

    public function init()
    {
        parent::init();
        
        //get input
        $this->ec_token = Tools::getValue('ec_token', null);
        $this->id_shop = Tools::getValue('id_shop', $this->context->shop->id ?? Configuration::get('PS_SHOP_DEFAULT'));
        $this->majsel = Tools::getValue('majsel', null);
        $this->catalog = new Catalog();
        $this->connecteur = Tools::getValue('connecteur', $this->module::GEN);
        $this->id_prg = Tools::getValue('prg', 0);
    }

    public function checkAccess()
    {
        //verify token and other infos
        if ($this->ec_token !== Catalog::getInfoEco('ECO_TOKEN')) {
            Tools::redirect($this->context->link->getPageLink('pagenotfound'));
        }
        
        return true;
    }

    public function initContent()
    {
        if ($this->id_prg) {
            $nextCron = Catalog::getNextCron($this->id_prg);
            if ($nextCron) {
                $this->content = Tools::file_get_contents($nextCron['link'] . '&prg=' . $this->id_prg . '&pos=' . $nextCron['position']);
                return true;
            }
        }
        
        //process input
        switch ((int) $this->majsel) {
            case 1:
                $this->tpl_vars = $this->catalog->getProductInfo(urldecode(Tools::getValue('reference')), $this->connecteur, Tools::getValue('iso_code'));
                $this->template = 'views/templates/admin/prod.tpl';
                break;
            case 13:
                $this->content = $this->catalog->countSynchro();
                break;
            case 26:
                $this->content = Catalog::synchroManuelOrder(Tools::getValue('idc'), Tools::getValue('typ'), $this->connecteur);
                break;
            case 27:
                $this->content = Catalog::generateSynchroOrder(Tools::getValue('idc'));
                break;
            case 29:
                $this->content = $this->catalog->checkFolder(Tools::getValue('fourn'));
                break;
            case 31:
                $name_fournisseur = Tools::getValue('plateforme');
                $dirPath = 'gen/' . $name_fournisseur . '/';
                $this->content = $this->catalog->deleteFolder($dirPath);
                break;
            case 33:
                $this->content = Catalog::getEciFournId($this->connecteur);
                break;
            case 35:
                $this->content = $this->catalog->saveAttributeMatching(Tools::getValue('id_attribute_eco'), Tools::getValue('id_attribute'), Tools::getValue('id_attribute_eco_old'));
                break;
            case 37:
//                $this->content = $this->catalog->setAttributeValue(Tools::getValue('id'), Tools::getValue('page'));
                $this->tpl_vars = $this->catalog->setAttributeValue(Tools::getValue('id'), Tools::getValue('page'));
                $this->template = 'views/templates/admin/EcParametresAttributValue.tpl';
                break;
            case 38:
//                $this->content = $this->catalog->setAddAttrMatch(Tools::getValue('id'));
                $this->tpl_vars = $this->catalog->setAddAttrMatch(Tools::getValue('id'));
                $this->template = 'views/templates/admin/AddAttrMatch.tpl';
                break;
            case 39:
                $this->content = $this->catalog->addAttrMatch(Tools::getValue('b'), Tools::getValue('v'), Tools::getValue('s'));
                break;
            case 40:
                $this->content = $this->catalog->delAttrMatch(Tools::getValue('v'), Tools::getValue('s'));
                break;
            case 45:
                $gen = Catalog::getGenClassStatic($this->connecteur, $this->id_shop);
                if (!$gen) {
                    $this->content = 'no gen';
                }
                $this->content = Catalog::updateParamFournisseur(
                    Tools::getValue('eci_erpdemo_conf'),
                    $this->module::GEN,
                    $this->id_shop,
                    array_merge(Catalog::$tabParamsValid, $gen->valid_conf_keys ?? array()),
                    array_merge(Catalog::$tabParamsNumeric, $gen->numeric_conf_keys ?? array()),
                    array_merge(Catalog::$tabParamsMultiple, $gen->multiple_conf_keys ?? array()),
                    array_merge(Catalog::$tabParamsArray, $gen->array_conf_keys ?? array()),
                    array_merge(Catalog::$tabParamsAllShops, $gen->allshop_keys ?? array())
                );
                break;
            case 55:
                $this->tpl_vars = ['message' => Tools::getValue('message'), 'etat' => Tools::getValue('etat')];
                $this->template = 'views/templates/admin/EcDisplay.tpl';
                break;
            case 75:
                $this->tpl_vars = Catalog::getCpanelData(Tools::getValue('prefix'), Tools::getValue('suffix', false));
                $this->template = 'views/templates/admin/EcCpanelTable.tpl';
                break;
            case 85:
                $this->content = $this->catalog->saveFeatureMatching(Tools::getValue('id_feature_eco'), Tools::getValue('id_feature'), Tools::getValue('id_feature_eco_old'));
                break;
            case 87:
                $this->tpl_vars = $this->catalog->setFeatureValue(Tools::getValue('id'), Tools::getValue('page'));
                $this->template = 'views/templates/admin/EcParametresFeatureValue.tpl';
                break;
            case 88:
                $this->tpl_vars = $this->catalog->setAddFeatMatch(Tools::getValue('id'));
                $this->template = 'views/templates/admin/AddFeatMatch.tpl';
                break;
            case 89:
                $this->content = $this->catalog->addFeatMatch(Tools::getValue('b'), Tools::getValue('v'), Tools::getValue('s'));
                break;
            case 90:
                $this->content = $this->catalog->delFeatMatch(Tools::getValue('v'), Tools::getValue('s'));
                break;
            default:
                break;
        }
        
        return true;
    }
    
    public function displayAjax()
    {
        if ($this->template && $this->tpl_vars) {
            $this->context->smarty->assign($this->tpl_vars);
            $this->template = 'modules/'.$this->module->name.'/'.$this->template;
            $this->smartyOutputContent($this->template);

            return true;
        }
        
        echo $this->content;
        
        return true;
    }
    
    protected static function exceptionErrorHandler($severity, $message, $file, $line)
    {
        if (!(error_reporting() & $severity)) {
            // Ce code d'erreur n'est pas inclu dans error_reporting

            return;
        }
        throw new ErrorException($message, 0, $severity, $file, $line);
    }
}
