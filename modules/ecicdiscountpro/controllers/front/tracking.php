<?php
/**
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to a commercial license from SARL Ether Creation
 * Use, copy, modification or distribution of this source file without written
 * license agreement from the SARL Ether Creation is strictly forbidden.
 * In order to obtain a license, please contact us: contact@ethercreation.com
 * ...........................................................................
 * INFORMATION SUR LA LICENCE D'UTILISATION
 *
 * L'utilisation de ce fichier source est soumise a une licence commerciale
 * concedee par la societe Ether Creation
 * Toute utilisation, reproduction, modification ou distribution du present
 * fichier source sans contrat de licence ecrit de la part de la SARL Ether Creation est
 * expressement interdite.
 * Pour obtenir une licence, veuillez contacter la SARL Ether Creation a l'adresse: contact@ethercreation.com
 * ...........................................................................
 *
 * @author    Ether Creation SARL <contact@ethercreation.com>
 * @copyright 2008-2021 Ether Creation SARL
 * @license   Commercial license
 * International Registered Trademark & Property of Ether Creation SARL
 */

require_once dirname(__FILE__) . '/../../class/catalog.class.php';
use ecicdiscountpro\Catalog;

class EcicdiscountproTrackingModuleFrontController extends ModuleFrontController
{
    public $auth = false;

    public $ajax = true;
    protected $ec_token;
    protected $prefix = 'ECI_UTR_';
    protected $cron = 'tracking';
    protected $catalog;
    protected $connecteur = 'cdiscountpro';
    protected $ec_four;
    protected $logger;
    protected $help;
    protected $stopTime;
    protected $nbCron;
    protected $params;
    protected $chain;
    protected $kill;
    protected $spy;
    protected $spy2;
    protected $who;
    protected $prg;
    protected $pos;

    private $id_shop;

    protected $listStages = [
        'started',
        'getfile',
        'updtracking',
    ];
    
    protected $starting;

    public function __construct()
    {
        parent::__construct();
        $this->ajax = true;
        
        $this->logger = Catalog::logStart('tracking');

        set_error_handler([get_class(), 'exceptionErrorHandler']);
    }

    public function init()
    {
        parent::init();
        
        //get input
        $this->ec_token = Tools::getValue('ec_token', null);
        $this->id_shop = Tools::getValue('id_shop', $this->context->shop->id ?? Configuration::get('PS_SHOP_DEFAULT'));
        $this->connecteur = Tools::getValue('connecteur', $this->module::GEN);
        $this->stopTime = time() + 25;
        $paramHelp = Tools::getValue('help', null);
        $this->help = !is_null($paramHelp);
        
        $paramNbCron = Tools::getValue('nbC', null);
        $this->nbCron = is_null($paramNbCron) ? 0 : (int) $paramNbCron;

        $paramJobID = Tools::getValue('jid', null);

        $paramAct = Tools::getValue('act', null);
        $this->action = (Catalog::jGet($this->prefix . 'ACT_' . $this->connecteur) === 'die') ? 'die' : ((empty($paramAct)) ? 'go' : $paramAct);

        $paramSpy = Tools::getValue('spy', null);
        $this->spy = (empty($paramSpy)) ? false : true;

        $paramSpy2 = Tools::getValue('spytwo', null);
        $this->spy2 = (empty($paramSpy2)) ? false : true;
        $this->who = $this->spy ? ($this->spy2 ? 'spy2' : 'spy') : 'normal';

        $paramKill = Tools::getValue('kill', null);
        $this->kill = is_null($paramKill) ? false : true;

        $paramPrg = Tools::getValue('prg', null);
        $paramPos = Tools::getValue('pos', null);
        if (!is_null($paramPrg)) {
            $this->prg = (int) $paramPrg;
            $this->pos = is_null($paramPos) ? 1 : (int) $paramPos;
            $this->chain = '&prg=' . $this->prg . '&pos=' . $this->pos;
        } else {
            $this->prg = false;
            $this->chain = '';
        }

        $this->ts = preg_replace('/0\.([0-9]{6}).*? ([0-9]+)/', '$2$1', microtime());
        $this->jid = is_null($paramJobID) ? $this->ts : $paramJobID;
        $this->params = '?ec_token=' . $this->ec_token . '&ts=' . $this->ts . '&jid=' . $this->jid . '&connecteur=' . $this->connecteur;
        
        if (Configuration::get('PS_REWRITING_SETTINGS')) {
            $this->base_uri = $this->module->protocol . Tools::getShopDomain() . str_replace('/modules/', '/module/', $this->module->getPathUri()) . $this->cron . $this->params . $this->chain;
        } else {
            $this->base_uri = $this->module->protocol . Tools::getShopDomain() . __PS_BASE_URI__ . 'index.php' . $this->params . $this->chain . '&fc=module&module=' . $this->module->name . '&controller=' . $this->cron;
        }
                
        //init properties
        $this->catalog = new Catalog();
        
        $this->starting = ((bool) $this->ec_token) & is_null($paramSpy) & is_null($paramNbCron) & is_null($paramKill) & is_null($paramAct);
        
//        Catalog::logInfo(
//                $this->logger,
//            $this->cron . ' '
//            . $this->who . ' entered, parameters '
//            . $this->connecteur . ','
//            . (int) $this->nbCron . ','
//            . $this->action . ','
//            . (int) $this->spy . ','
//            . (int) $this->spy2 . ','
//            . (int) $this->kill
//        );
    }
    
    public function checkAccess()
    {
        if ($this->help) {
            Catalog::answer(
                json_encode(
                    array(
                        'ec_token' => array(
                            'fr' => 'Token du module. Obligatoire.',
                            'en' => 'Module\'s token. Required.'
                            ),
                        'connecteur' => array(
                            'fr' => 'Connecteur à traiter. Obligatoire.',
                            'en' => 'Supplier to treat. Required.'
                            ),
                    )
                )
            );
            exit();
        }

        //verify token and other infos
        if ($this->ec_token !== Catalog::getInfoEco('ECO_TOKEN')) {
            Tools::redirect($this->context->link->getPageLink('pagenotfound'));
        }
        
        return true;
    }

    public function postProcess()
    {
        // kill
        if ($this->kill) {
            if (Catalog::jGet($this->prefix . 'STATE_' . $this->connecteur) != 'done') {
                Catalog::jUpdateValue($this->prefix . 'ACT_' . $this->connecteur, 'die');
            }
            exit('kill');
        }

        // verify and import connector
        if (!$this->connecteur) {
            exit('no connector');
        }
        
        $this->ec_four = Catalog::getGenClassStatic($this->connecteur);
        //TODO ajouter sécurité

        // advancement control
        if ($this->spy) {
            Catalog::answer('spy');
            sleep(14);
            $state = Catalog::jGet($this->prefix . 'STATE_' . $this->connecteur);
            $progress = Catalog::jGet($this->prefix . 'PROGRESS_' . $this->connecteur);
            if ($this->nbCron == $progress) {
                if ($this->spy2) {
                    if ($state != 'done') {
                        Catalog::jUpdateValue($this->prefix . 'STATE_' . $this->connecteur, 'still');
                    }
                } else {
                    Catalog::followLink($this->base_uri . '&spy=1&spytwo=1&nbC=' . $progress);
                }
            } else {
                Catalog::followLink($this->base_uri . '&spy=1&nbC=' . $progress);
            }
            exit('bond');
        }

        // init or give up
        $etat = Catalog::jGet($this->prefix . 'STATE_' . $this->connecteur);
        if (!$this->starting && ($this->action === 'die')) {
            // kill -> die
            Catalog::jUpdateValue($this->prefix . 'STATE_' . $this->connecteur, 'done');
            Catalog::jUpdateValue($this->prefix . 'END_TIME_' . $this->connecteur, date('Y-m-d H:i:s'));
            Catalog::jUpdateValue($this->prefix . 'ACT_' . $this->connecteur, 'go');
            exit('dead');
        }
        if ($this->starting && ($etat === 'running')) {
            // avoid double launch
            $progress = Catalog::jGet($this->prefix . 'PROGRESS_' . $this->connecteur);
            // spy should be launched to verify false running state due to server reboot
            Catalog::followLink($this->base_uri . '&spy=1&nbC=' . (int) $progress);
            exit('nodoubleplease');
        }
        if (!$this->starting && ($etat === 'still')) {
            // spy got asleep but process is running
            Catalog::jUpdateValue($this->prefix . 'STATE_' . $this->connecteur, 'running');
            Catalog::followLink($this->base_uri . '&spy=1&nbC=' . $this->nbCron);
        }
        if ($this->starting) {
            // init
            Catalog::jUpdateValue($this->prefix . 'START_TIME_' . $this->connecteur, date('Y-m-d H:i:s'));
            Catalog::jUpdateValue($this->prefix . 'END_TIME_' . $this->connecteur, '');
            Catalog::jUpdateValue($this->prefix . 'STAGE_' . $this->connecteur, reset($this->listStages));
            Catalog::jUpdateValue($this->prefix . 'PROGRESS_' . $this->connecteur, 0);
            Catalog::jUpdateValue($this->prefix . 'PROGRESSMAX_' . $this->connecteur, 0);
            Catalog::jUpdateValue($this->prefix . 'LOOPS_' . $this->connecteur, 0);
            Catalog::jUpdateValue($this->prefix . 'STATE_' . $this->connecteur, 'running');
            Catalog::jUpdateValue($this->prefix . 'ACT_' . $this->connecteur, 'go');
            Catalog::jUpdateValue($this->prefix . 'MESSAGE_' . $this->connecteur, '');
            // start advancement control
            Catalog::followLink($this->base_uri . '&spy=1&nbC=0');
        } else {
            Catalog::jUpdateValue($this->prefix . 'PROGRESS_' . $this->connecteur, $this->nbCron);
        }
        $stage = Catalog::jGet($this->prefix . 'STAGE_' . $this->connecteur);
        
        // treat loops, breaks, end
        if ($this->action === 'next') {
            $this->action = 'go';
            Catalog::jUpdateValue($this->prefix . 'ACT_' . $this->connecteur, 'go');

            $numStage = array_search($stage, $this->listStages, true);
            $keys = array_keys($this->listStages);
            $next = $nextKey = false;
            foreach ($keys as $key) {
                if ($next) {
                    $nextKey = $key;
                    break;
                }
                if ($numStage == $key) {
                    $next = true;
                }
            }

            if ($nextKey) {
                $stage = $this->listStages[$nextKey];
                $this->nbCron = 0;
            } else {
                Catalog::jUpdateValue($this->prefix . 'STATE_' . $this->connecteur, 'done');
                Catalog::jUpdateValue($this->prefix . 'END_TIME_' . $this->connecteur, date('Y-m-d H:i:s'));
                if ($this->prg) {
                    //chaining with other task
                    $nextCron = Catalog::getNextCron($this->prg, $this->pos);
                    if ($nextCron) {
                        Catalog::followLink($nextCron['link'] . '&prg=' . $this->prg . '&pos=' . $nextCron['position']);
                    }
                }

                exit('done');
            }
            Catalog::jUpdateValue($this->prefix . 'STAGE_' . $this->connecteur, $stage);
            Catalog::jUpdateValue($this->prefix . 'LOOPS_' . $this->connecteur, 0);
            Catalog::jUpdateValue($this->prefix . 'PROGRESS_' . $this->connecteur, 0);
            Catalog::jUpdateValue($this->prefix . 'PROGRESSMAX_' . $this->connecteur, 0);
        }
        
//        Catalog::logInfo(
//                $this->logger,
//            $this->cron . ' ' . $this->who . ' do ' . $stage . ',' . $this->action
//        );
        
        //aiguillage
        Catalog::answer($stage);
        if (method_exists(get_class(), $stage)) {
            try {
                $reps = $this->$stage();
            } catch (Exception $e) {
                $reps = 'In "' . $stage . '" : ' . $e->getMessage() . ' in line ' . $e->getLine() . ' of file ' . $e->getFile();
            }
            if ($reps === true) {
                Catalog::jUpdateValue($this->prefix . 'ACT_' . $this->connecteur, 'next');
                Catalog::followLink($this->base_uri . '&nbC=0&act=next');
            } elseif (is_numeric($reps)) {
                Catalog::jUpdateValue($this->prefix . 'LOOPS_' . $this->connecteur, Catalog::jGet($this->prefix . 'LOOPS_' . $this->connecteur) + 1);
                Catalog::followLink($this->base_uri . '&nbC=' . $reps);
            }
        } else {
            exit('done');
        }

        if ($reps !== true && (!is_numeric($reps))) {
            Catalog::jUpdateValue($this->prefix . 'MESSAGE_' . $this->connecteur, var_export($reps, true));
            Catalog::logInfo(
                $this->logger,
                $this->cron . ' ' . $this->who . ', ' . $this->connecteur . ', stage ' . $stage . ', ' . var_export($reps, true)
            );
        }
    }

    private function started()
    {
        echo 'Task successfully started. ';

        return true;
    }
    
    private function getfile()
    {
        $type = 'tracking';
        if (!$this->nbCron) {
            Catalog::jUpdateValue($this->prefix . 'DATA_' . $this->connecteur, '');
            try {
                $retour = $this->ec_four->getFile($type);
            } catch (Exception $ex) {
                return 'Error in getFile : ' . $ex->getMessage();
            }
        } else {
            $json_param = Catalog::jGet($this->prefix . 'DATA_' . $this->connecteur);
            $tab_param = json_decode($json_param, true);
            if (!is_null($tab_param)) {
                try {
                    $retour = $this->ec_four->getFile(
                        $type,
                        isset($tab_param['fichier'])?$tab_param['fichier']:'',
                        isset($tab_param['slice'])?$tab_param['slice']:0,
                        isset($tab_param['extparam'])?$tab_param['extparam']:''
                    );
                } catch (Exception $ex) {
                    return 'Error in getFile : ' . $ex->getMessage();
                }
            } else {
                try {
                    $retour = $this->ec_four->getFile($type);
                } catch (Exception $ex) {
                    return 'Error in getFile : ' . $ex->getMessage();
                }
            }
        }

        if (is_array($retour)) {
            if (!isset($retour['rc'])) {
                return $retour;
            } else {
                switch ((int)$retour['rc']) {
                    case 1:
                        return true;
                    case 2:
                        Catalog::jUpdateValue($this->prefix . 'DATA_' . $this->connecteur, json_encode($retour));
                        return (1 + $this->nbCron);
                    default:
                        if (isset($retour['message'])) {
                            return $retour['message'];
                        } else {
                            return $retour;
                        }
                }
            }
        } else {
            $tab_ret = explode(',', $retour);
            if (2 > count($tab_ret)) {
                return 'Too few infos in return from getFile for flux "' . $type . '" : ' . $retour;
            }
            switch ((int)$tab_ret[0]) {
                case 1:
                    return true;
                case 2:
                    $tab_param = array();
                    parse_str($tab_ret[1], $tab_param);
                    /*Catalog::logInfo($logger, var_export($retour, true));
                    Catalog::logInfo($logger, var_export($tab_ret, true));
                    Catalog::logInfo($logger, var_export($tab_param, true));*/
                    Catalog::jUpdateValue($this->prefix . 'DATA_' . $this->connecteur, json_encode($tab_param));
                    return (1 + $this->nbCron);
                case 0:
                    return $tab_ret[1];
                default:
                    return 'Unknown error in getFile : ' . $retour;
            }
        }
    }
    
    private function updtracking()
    {
        $reps = true;
        if (method_exists($this->ec_four, 'updateTracking')) {
            $reps = $this->ec_four->updateTracking($this->nbCron, $this->stopTime);
        }

        return $reps;
    }
    
    protected static function exceptionErrorHandler($severity, $message, $file, $line)
    {
        if (!(error_reporting() & $severity)) {
            // Ce code d'erreur n'est pas inclu dans error_reporting

            return;
        }
        throw new ErrorException($message, 0, $severity, $file, $line);
    }
}
