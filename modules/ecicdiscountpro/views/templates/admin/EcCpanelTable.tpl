{*
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to a commercial license from SARL Ether Creation
 * Use, copy, modification or distribution of this source file without written
 * license agreement from the SARL Ether Creation is strictly forbidden.
 * In order to obtain a license, please contact us: contact@ethercreation.com
 * ...........................................................................
 * INFORMATION SUR LA LICENCE D'UTILISATION
 *
 * L'utilisation de ce fichier source est soumise a une licence commerciale
 * concedee par la societe Ether Creation
 * Toute utilisation, reproduction, modification ou distribution du present
 * fichier source sans contrat de licence ecrit de la part de la SARL Ether Creation est
 * expressement interdite.
 * Pour obtenir une licence, veuillez contacter la SARL Ether Creation a l'adresse: contact@ethercreation.com
 * ...........................................................................
 *
 * @author    Ether Creation SARL <contact@ethercreation.com>
 * @copyright 2008-2021 Ether Creation SARL
 * @license   Commercial license
 * International Registered Trademark & Property of Ether Creation SARL
 *}
<table class="table" style="">
    <thead>
        <tr>
            {*<th style="text-align:center;">{l s='Connector' mod='ecicdiscountpro'}</th>*}
            <th style="text-align:center;">{l s='Start' mod='ecicdiscountpro'}</th>
            <th style="text-align:center;">{l s='End' mod='ecicdiscountpro'}</th>
            <th style="text-align:center;">{l s='State' mod='ecicdiscountpro'}</th>
            <th style="text-align:center;">{l s='Shop' mod='ecicdiscountpro'}</th>
            <th style="text-align:center;">{l s='Step' mod='ecicdiscountpro'}</th>
            <th style="text-align:center;">{l s='Counter' mod='ecicdiscountpro'}</th>
            <th style="text-align:center;">{l s='Maximum' mod='ecicdiscountpro'}</th>
            <th style="text-align:center;">{l s='Message' mod='ecicdiscountpro'}</th>
        </tr>
    </thead>
    <tbody>
        {foreach from=$cpanelData item=data key=connecteur}
            <tr>
                {*<td style="padding:3px;">{$connecteur|escape:'htmlall':'UTF-8'}</td>*}
                <td style="text-align:center;">{$data.start_time|escape:'htmlall':'UTF-8'}</td>
                <td style="text-align:center;">{$data.end_time|escape:'htmlall':'UTF-8'}</td>
            {if $data.state != 'done'}
                <td style="text-align:center;">{$data.state|escape:'htmlall':'UTF-8'}</td>
                <td style="text-align:center;">{$data.shop|escape:'htmlall':'UTF-8'}</td>
                <td style="text-align:center;">{$data.stage|escape:'htmlall':'UTF-8'}</td>
                <td style="text-align:center;" class="eci_task_progress">{$data.progress|escape:'htmlall':'UTF-8'}</td>
                <td style="text-align:center;">{$data.progressmax|escape:'htmlall':'UTF-8'}</td>
            {else}
                <td style="text-align:center;"> </td>
                <td style="text-align:center;"> </td>
                <td style="text-align:center;"> </td>
                <td style="text-align:center;"> </td>
                <td style="text-align:center;"> </td>
            {/if}
                <td style="text-align:center;"><abbr title="{$data.message|escape:'htmlall':'UTF-8'}"><i class='icon-question'></i></abbr></td>
            </tr>
        {/foreach}
    </tbody>
</table>
