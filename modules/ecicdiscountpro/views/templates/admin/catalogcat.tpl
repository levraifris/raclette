{*
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to a commercial license from SARL Ether Creation
 * Use, copy, modification or distribution of this source file without written
 * license agreement from the SARL Ether Creation is strictly forbidden.
 * In order to obtain a license, please contact us: contact@ethercreation.com
 * ...........................................................................
 * INFORMATION SUR LA LICENCE D'UTILISATION
 *
 * L'utilisation de ce fichier source est soumise a une licence commerciale
 * concedee par la societe Ether Creation
 * Toute utilisation, reproduction, modification ou distribution du present
 * fichier source sans contrat de licence ecrit de la part de la SARL Ether Creation est
 * expressement interdite.
 * Pour obtenir une licence, veuillez contacter la SARL Ether Creation a l'adresse: contact@ethercreation.com
 * ...........................................................................
 *
 * @author    Ether Creation SARL <contact@ethercreation.com>
 * @copyright 2008-2021 Ether Creation SARL
 * @license   Commercial license
 * International Registered Trademark & Property of Ether Creation SARL
 *}
<input type="hidden" id='tradactif' value="{l s='Yes' mod='ecicdiscountpro'}"/>
<input type="hidden" id='tradinactif' value="{l s='No' mod='ecicdiscountpro'}"/>
<input type="hidden" id='tradtous' value="{l s='All' mod='ecicdiscountpro'}"/>
<input type="hidden" id='tradimport' value="{l s='Import' mod='ecicdiscountpro'}"/>
<input type="hidden" id='traddelete' value="{l s='Delete' mod='ecicdiscountpro'}"/>
<input type="hidden" id='fournisseur' value="{$fournisseur|escape:'htmlall':'UTF-8'}"/>
<input type="hidden" id='ec_token' value="{$ec_token|escape:'htmlall':'UTF-8'}"/>
<input type="hidden" id='id_shop' value="{$id_shop|escape:'htmlall':'UTF-8'}"/>
<input type="hidden" id='iso_code' value="{$iso_code|escape:'htmlall':'UTF-8'}"/>
<input type="hidden" id='eci_baseDir' value="{$eci_baseDir|escape:'htmlall':'UTF-8'}"/>
<input type="hidden" id='eci_ajaxlink' value="{$eci_ajaxlink|escape:'htmlall':'UTF-8'}"/>
<input type="hidden" id='eci_filter_by_category' value="{$filter_by_category|escape:'htmlall':'UTF-8'}"/>
<input type="hidden" id='tradnoselect' value="{l s='No product selected' mod='ecicdiscountpro'}"/>
<a href="#tloading" id="loaddiv"></a>
<span id="tloading"></span>
<div id="display_message"></div>
<div id="catinfo" class="panel">
    <div class="panel-heading">{l s='Catalog informations' mod='ecicdiscountpro'}</div>
    <div class="panel-body">
        <p class="border">{l s='Total products' mod='ecicdiscountpro'} : {$totalprod|escape:'htmlall':'UTF-8'}</p>
        <p class="border">{l s='Products to be imported' mod='ecicdiscountpro'} : {$countimp|escape:'htmlall':'UTF-8'}</p>
        <p class="border">{l s='Products to be deleted' mod='ecicdiscountpro'} : {$countdel|escape:'htmlall':'UTF-8'}</p>
        <p class="border">{l s='Products already imported' mod='ecicdiscountpro'} : {$productinshop|escape:'htmlall':'UTF-8'}</p>
        <p class="butright">{*{l s='Synchronize the catalog' mod='ecicdiscountpro'} :*} <button id="synccat" onclick="javascript:$.post('../modules/ecicdiscountpro/syncAuto.php?connecteur=cdiscountpro&ec_token={$ec_token|escape:'htmlall':'UTF-8'}');displayInfoRetour('12', 'success');return false;" type="button" class="btn btn-default"><i class="icon-download" style="margin-right:5px"></i>{l s='Import' mod='ecicdiscountpro'} ({$countimp+$countdel})</button></p>
        <p class="butright">{*{l s='Refresh the catalog' mod='ecicdiscountpro'} :*} <button id="refreshcat"  onclick="javascript:$.post('../modules/ecicdiscountpro/catAuto.php?connecteur=cdiscountpro&ec_token={$ec_token|escape:'htmlall':'UTF-8'}');displayInfoRetour('12', 'success');return false;" type="button" class="btn btn-default"><i class="icon-refresh" style="margin-right:5px"></i>{l s='Refresh' mod='ecicdiscountpro'}</button></p>
    </div>
</div>
{html_entity_decode($param|escape:'htmlall':'UTF-8')}

<div id="eci_prod"></div>