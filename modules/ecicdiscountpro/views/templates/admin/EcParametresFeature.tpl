{*
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to a commercial license from SARL Ether Creation
 * Use, copy, modification or distribution of this source file without written
 * license agreement from the SARL Ether Creation is strictly forbidden.
 * In order to obtain a license, please contact us: contact@ethercreation.com
 * ...........................................................................
 * INFORMATION SUR LA LICENCE D'UTILISATION
 *
 * L'utilisation de ce fichier source est soumise a une licence commerciale
 * concedee par la societe Ether Creation
 * Toute utilisation, reproduction, modification ou distribution du present
 * fichier source sans contrat de licence ecrit de la part de la SARL Ether Creation est
 * expressement interdite.
 * Pour obtenir une licence, veuillez contacter la SARL Ether Creation a l'adresse: contact@ethercreation.com
 * ...........................................................................
 *
 * @author    Ether Creation SARL <contact@ethercreation.com>
 * @copyright 2008-2021 Ether Creation SARL
 * @license   Commercial license
 * International Registered Trademark & Property of Ether Creation SARL
 *}
<input type="hidden" id='eci_ajaxlink' value="{$eci_ajaxlink|escape:'htmlall':'UTF-8'}"/>
{foreach from=$eciF item=f}
    <div class="panel form-group">
        <div class="col-lg-3">
            <label class="control-label">{l s='Name :' mod='ecicdiscountpro'} {$f['value']|escape:'htmlall':'UTF-8'}</label>
        </div>
        <div class="margin-form col-lg-9">
            <select class="save_feature" name="feature_ps[]">
                <option value="{$f['id_feature_eco']|escape:'htmlall':'UTF-8'}_0">{l s='Create automatically' mod='ecicdiscountpro'}</option>
                {foreach from=$Feature_PS item=featurePS}
                    <option value="{$f['id_feature_eco']|escape:'htmlall':'UTF-8'}_{$featurePS['id_feature']|escape:'htmlall':'UTF-8'}" {if $f['id_feature'] == $featurePS['id_feature']}selected="selected"{/if}>{$featurePS['name']|escape:'htmlall':'UTF-8'}</option>
                {/foreach}
            </select>
        </div>
        <br>

        <div class="col-lg-12" {if $catalog->setAddFeatMatch($f['id_feature_eco']|escape:'htmlall':'UTF-8') == false} style="display:none;{/if}">
            <div class="col-lg-3">
                <button type="button" class="btn btn-default button detFeat" data-idfeco="{$f['id_feature_eco']|escape:'htmlall':'UTF-8'}">{l s='Detail' mod='ecicdiscountpro'} </button>
            </div>
            <div class="col-lg-9 receDet receDet{$f['id_feature_eco']|escape:'htmlall':'UTF-8'}" style="display:none; margin-top: 20px;">
            </div>
        </div>

    </div>
{/foreach}
<style>
.panel {
    float: left;
    width: 100%;
}
</style>
