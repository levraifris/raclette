{*
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to a commercial license from SARL Ether Creation
 * Use, copy, modification or distribution of this source file without written
 * license agreement from the SARL Ether Creation is strictly forbidden.
 * In order to obtain a license, please contact us: contact@ethercreation.com
 * ...........................................................................
 * INFORMATION SUR LA LICENCE D'UTILISATION
 *
 * L'utilisation de ce fichier source est soumise a une licence commerciale
 * concedee par la societe Ether Creation
 * Toute utilisation, reproduction, modification ou distribution du present
 * fichier source sans contrat de licence ecrit de la part de la SARL Ether Creation est
 * expressement interdite.
 * Pour obtenir une licence, veuillez contacter la SARL Ether Creation a l'adresse: contact@ethercreation.com
 * ...........................................................................
 *
 * @author    Ether Creation SARL <contact@ethercreation.com>
 * @copyright 2008-2021 Ether Creation SARL
 * @license   Commercial license
 * International Registered Trademark & Property of Ether Creation SARL
 *}
<input type="hidden" id='ec_token' value="{$ec_token|escape:'htmlall':'UTF-8'}"/>
<a href="#categoryselector" id="showmatchcat"></a>
<div id="categoryselector" style="display:none;" class="bootstrap">
    <div id="content" style="margin: 0;">
        <form action="" method="POST" class="categoryselector">
            <span id="cs_category_name_rel"></span>
            <input name="fournisseur" id="cs_fournisseur_name" value="{$fournisseur|escape:'htmlall':'UTF-8'}" type="hidden">
            <input name="id_cateco" id="id_cateco" value="" type="hidden">
            <br/>
            <br/>
            <select name="id_catps" id="id_catps">
                <option value="0">{l s='Create automatically' mod='ecicdiscountpro'}</option>
                {foreach from=$category_list item=cat}
                    <option value="{$cat['id_category']|escape:'htmlall':'UTF-8'}">{'&nbsp;'|str_repeat:($cat['level_depth']*4)} {$cat['name']|escape:'htmlall':'UTF-8'}</option>
                {/foreach}
            </select>
            <div class="panel-footer">
                <button type="submit" value="1" id="apply_cat_match" name="submitEciCdiscountproMatchCat" class="btn btn-default pull-right" style="">
                    <i class="process-icon-save"></i> {l s='Save' mod='ecicdiscountpro'}
                </button>
            </div>
        </form>
    </div>
</div>
