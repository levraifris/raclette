{*
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to a commercial license from SARL Ether Creation
 * Use, copy, modification or distribution of this source file without written
 * license agreement from the SARL Ether Creation is strictly forbidden.
 * In order to obtain a license, please contact us: contact@ethercreation.com
 * ...........................................................................
 * INFORMATION SUR LA LICENCE D'UTILISATION
 *
 * L'utilisation de ce fichier source est soumise a une licence commerciale
 * concedee par la societe Ether Creation
 * Toute utilisation, reproduction, modification ou distribution du present
 * fichier source sans contrat de licence ecrit de la part de la SARL Ether Creation est
 * expressement interdite.
 * Pour obtenir une licence, veuillez contacter la SARL Ether Creation a l'adresse: contact@ethercreation.com
 * ...........................................................................
 *
 * @author    Ether Creation SARL <contact@ethercreation.com>
 * @copyright 2008-2021 Ether Creation SARL
 * @license   Commercial license
 * International Registered Trademark & Property of Ether Creation SARL
 *}
{if !empty($nbPage) && 1 != $nbPage}
<div class="ajtMat col-lg-12">
    <ul class="pagination pagination-sm">
        {for $i=1 to $nbPage}
            <li {if $i == $page}class="active" {/if}><a href="#" class="pageDetAtt" data-idgeco="{$idA|escape:'htmlall':'UTF-8'}" data-page="{$i|escape:'htmlall':'UTF-8'}">{$i|escape:'htmlall':'UTF-8'}</a></li>
        {/for}
    </ul>
</div>
{/if}
<div class="ajtMat col-lg-12">
    {foreach from=$att_matched item=attribut}
        <div class="col-lg-12" style="margin-bottom: 20px;">
            <div class="col-lg-4">
                <input class="col-lg-12" value="{$attribut['value']|escape:'htmlall':'UTF-8'}" disabled>
            </div>
            <div class="col-lg-2 eccentered" style="text-align: center;"><i class="icon-forward"></i></div>
            <div class="col-lg-4">
                <select class="matched_att_val" name="attvaleco_{$attribut['id_attribute_eco_value']|escape:'htmlall':'UTF-8'}">
                {foreach from=$Attribut_PS item=attributPS}
                    <option value="{$attributPS['id_attribute']|escape:'htmlall':'UTF-8'}" {if $attribut['id_attribute'] == $attributPS['id_attribute']}selected="selected"{/if}>{$attributPS['name']|escape:'htmlall':'UTF-8'}</option>
                {/foreach}
                </select>
            </div>
            <div class="updateMatch col-lg-1 eccentered" data-idS="{$idshop|escape:'htmlall':'UTF-8'}" data-idE="{$idA|escape:'htmlall':'UTF-8'}" data-idV="{$attribut['id_attribute_eco_value']|escape:'htmlall':'UTF-8'}" style="cursor:pointer; text-align: center;"><i class="icon-check-sign"></i></div>
            <div class="delMatch col-lg-1 eccentered" data-idS="{$idshop|escape:'htmlall':'UTF-8'}" data-idE="{$idA|escape:'htmlall':'UTF-8'}" data-idV="{$attribut['id_attribute_eco_value']|escape:'htmlall':'UTF-8'}" style="cursor:pointer; text-align: center;"><i class="icon-minus"></i></div>
        </div>
    {/foreach}
    <div class="ajtMatAtt col-lg-12" data-id="{$idA|escape:'htmlall':'UTF-8'}" style="cursor:pointer"><button class="btn btn-default">{l s='Add' mod='ecicdiscountpro'}<i class="icon-plus" style="margin-left: 5px;"></i></button></div>
</div>
<style>
.panel{
    float: left;
    width: 100%;
}
</style>
