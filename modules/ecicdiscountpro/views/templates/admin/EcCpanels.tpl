{*
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to a commercial license from SARL Ether Creation
 * Use, copy, modification or distribution of this source file without written
 * license agreement from the SARL Ether Creation is strictly forbidden.
 * In order to obtain a license, please contact us: contact@ethercreation.com
 * ...........................................................................
 * INFORMATION SUR LA LICENCE D'UTILISATION
 *
 * L'utilisation de ce fichier source est soumise a une licence commerciale
 * concedee par la societe Ether Creation
 * Toute utilisation, reproduction, modification ou distribution du present
 * fichier source sans contrat de licence ecrit de la part de la SARL Ether Creation est
 * expressement interdite.
 * Pour obtenir une licence, veuillez contacter la SARL Ether Creation a l'adresse: contact@ethercreation.com
 * ...........................................................................
 *
 * @author    Ether Creation SARL <contact@ethercreation.com>
 * @copyright 2008-2021 Ether Creation SARL
 * @license   Commercial license
 * International Registered Trademark & Property of Ether Creation SARL
 *}
{if !empty($eci_ajaxlink)}
<input type="hidden" id='eci_ajaxlink' value="{$eci_ajaxlink|escape:'htmlall':'UTF-8'}"/>
{/if}
{foreach $listTasks as $task}
    <div class="panel col-lg-12 ecicpanel" data-id_cpanel="{$task.id_cpanel}" data-prefix="{$task.prefix}" data-suffix="{$task.suffix}" data-position="{$task.position}" style="{if !empty($task.hide)}display:none;{/if}">
        <h3>
            <div class="col-lg-12">
                <label class="control-label col-lg-3"><b>{l s='Task' mod='ecicdiscountpro'} "{$task.name|escape:'htmlall':'UTF-8'}"</b></label>
                {*<label class="control-label col-lg-3"><b>{l s='Task' mod='ecicdiscountpro'} "{l s=$task.name mod='ecicdiscountpro'}"</b></label>*}
                {if empty($task.nobuttons)}
                <div class="col-lg-1">
                    <button class="btn btn-success col-lg-{if $task.prefix eq 'nofollow'}12{else}6{/if}" onclick="javascript:$.post('{$task.link}');displayInfoRetour('12', 'success');return false;" id="eci_task_link_{$task.id_cpanel}" data-link="{$task.link}">
                        {l s='Start' mod='ecicdiscountpro'}
                    </button>
                    {if $task.prefix neq 'nofollow'}
                    <button class="btn btn-danger col-lg-6" onclick="javascript:$.post('{$task.link|cat:'&kill'}');displayInfoRetour('13', 'success');return false;">
                        {l s='Stop' mod='ecicdiscountpro'}
                    </button>
                    {/if}
                </div>
                <div class="margin-form col-lg-8">
                    <input type="text" value="{$task.link}" readonly style="cursor:text;"/>
                </div>
                {/if}
            </div>
        </h3>
        {if $task.prefix neq 'nofollow'}
        <i>{l s='Keep the cursor inside this frame to update it' mod='ecicdiscountpro'}</i>
        <div class="ecivpanel" id="ecivpanel{$task.id_cpanel}"></div>
        {/if}
    </div>
{/foreach}




