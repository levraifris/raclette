{*
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to a commercial license from SARL Ether Creation
 * Use, copy, modification or distribution of this source file without written
 * license agreement from the SARL Ether Creation is strictly forbidden.
 * In order to obtain a license, please contact us: contact@ethercreation.com
 * ...........................................................................
 * INFORMATION SUR LA LICENCE D'UTILISATION
 *
 * L'utilisation de ce fichier source est soumise a une licence commerciale
 * concedee par la societe Ether Creation
 * Toute utilisation, reproduction, modification ou distribution du present
 * fichier source sans contrat de licence ecrit de la part de la SARL Ether Creation est
 * expressement interdite.
 * Pour obtenir une licence, veuillez contacter la SARL Ether Creation a l'adresse: contact@ethercreation.com
 * ...........................................................................
 *
 * @author    Ether Creation SARL <contact@ethercreation.com>
 * @copyright 2008-2021 Ether Creation SARL
 * @license   Commercial license
 * International Registered Trademark & Property of Ether Creation SARL
 *}
{if $etat|escape:'htmlall':'UTF-8' == 'message'}
    <!-- pour affichage des informations liées à la synchro produit -->
    {if $message|escape:'htmlall':'UTF-8' == '1'}
        {l s='Importing catalog...' mod='ecicdiscountpro'}
    {/if}

{else}

    <!-- Pour affichage dans le bandeau en haut -->
    <div class="alert {if $etat|escape:'htmlall':'UTF-8' == 'success'}alert-success{else}alert-danger{/if}">
        <button class="close" data-dismiss="alert" type="button">×</button>
        {if $message|escape:'htmlall':'UTF-8' == '1'}
            {l s='Default parameters applied.' mod='ecicdiscountpro'}
        {elseif $message|escape:'htmlall':'UTF-8' == '2'}
            {l s='Tax matched successfully' mod='ecicdiscountpro'}
        {elseif $message|escape:'htmlall':'UTF-8' == '3'}
            {l s='An error occurred while trying to match this tax.' mod='ecicdiscountpro'}
        {elseif $message|escape:'htmlall':'UTF-8' == '4'}
            {l s='Attribute name matched successfully' mod='ecicdiscountpro'}
        {elseif $message|escape:'htmlall':'UTF-8' == '5'}
            {l s='An error occurred while trying to match this attribute name.' mod='ecicdiscountpro'}
        {elseif $message|escape:'htmlall':'UTF-8' == '6'}
            {l s='Parameters successfully saved' mod='ecicdiscountpro'}
        {elseif $message|escape:'htmlall':'UTF-8' == '7'}
            {l s='An error occurred while trying to save parameters.' mod='ecicdiscountpro'}
        {elseif $message|escape:'htmlall':'UTF-8' == '8'}
            {l s='Category successfully matched' mod='ecicdiscountpro'}
        {elseif $message|escape:'htmlall':'UTF-8' == '9'}
            {l s='An error occurred while trying to match this category.' mod='ecicdiscountpro'}
        {elseif $message|escape:'htmlall':'UTF-8' == '10'}
            {l s='Importation ended successfully' mod='ecicdiscountpro'}
        {elseif $message|escape:'htmlall':'UTF-8' == '11'}
            {l s='Non numeric margin' mod='ecicdiscountpro'}
        {elseif $message|escape:'htmlall':'UTF-8' == '12'}
            {l s='Task launched. You can follow it from this control panel' mod='ecicdiscountpro'}
        {elseif $message|escape:'htmlall':'UTF-8' == '13'}
            {l s='Task stopped' mod='ecicdiscountpro'}
        {elseif $message|escape:'htmlall':'UTF-8' == '14'}
            {l s='Task launched' mod='ecicdiscountpro'}
        {elseif $message|escape:'htmlall':'UTF-8' == '15'}
            {l s='An error occurred while trying to get the file !' mod='ecicdiscountpro'}
        {elseif $message|escape:'htmlall':'UTF-8' == '16'}
            {l s='Catalog up to date' mod='ecicdiscountpro'}
        {elseif $message|escape:'htmlall':'UTF-8' == '17'}
            {l s='Non numeric tax' mod='ecicdiscountpro'}
        {elseif $message|escape:'htmlall':'UTF-8' == '18'}
            {l s='Successful update' mod='ecicdiscountpro'}
        {elseif $message|escape:'htmlall':'UTF-8' == '19'}
            {l s='Error' mod='ecicdiscountpro'}
        {elseif $message|escape:'htmlall':'UTF-8' == '20'}
            {l s='Parameters saved successfully' mod='ecicdiscountpro'}
        {elseif $message|escape:'htmlall':'UTF-8' == '21'}
            {l s='An error occurred while saving settings.' mod='ecicdiscountpro'}
        {elseif $message|escape:'htmlall':'UTF-8' == '22'}
            {l s='Fix margin and local TVA must be a number' mod='ecicdiscountpro'}
        {/if}
    </div>
{/if}
