{*
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to a commercial license from SARL Ether Creation
 * Use, copy, modification or distribution of this source file without written
 * license agreement from the SARL Ether Creation is strictly forbidden.
 * In order to obtain a license, please contact us: contact@ethercreation.com
 * ...........................................................................
 * INFORMATION SUR LA LICENCE D'UTILISATION
 *
 * L'utilisation de ce fichier source est soumise a une licence commerciale
 * concedee par la societe Ether Creation
 * Toute utilisation, reproduction, modification ou distribution du present
 * fichier source sans contrat de licence ecrit de la part de la SARL Ether Creation est
 * expressement interdite.
 * Pour obtenir une licence, veuillez contacter la SARL Ether Creation a l'adresse: contact@ethercreation.com
 * ...........................................................................
 *
 * @author    Ether Creation SARL <contact@ethercreation.com>
 * @copyright 2008-2021 Ether Creation SARL
 * @license   Commercial license
 * International Registered Trademark & Property of Ether Creation SARL
 *}
<div id="eciprod_product">
    <div id="eciprod_images">
        <div id="eciprod_bigpic_block">
            <img id="eciprod_bigpic_image" itemprop="image" src="{$product.images.0|escape:'htmlall':'UTF-8'}" title="{$product.name|escape:'html':'UTF-8'}" alt="{$product.name|escape:'html':'UTF-8'}"/>
        </div>
        <div id="eciprod_thumbs_block" {if isset($product.images) && count($product.images) < 2}style="display:none;"{/if}>
            <div class="eciprod_slide_buttons_block">
            {if isset($product.images) && count($product.images) > 3}
                <span class="eciprod_slide_button" onclick="plusDivs(0);">◀</span>
            {/if}
            </div>
            <ul id="eciprod_thumbs_list">
                {if isset($product.images)}
                    {foreach from=$product.images key = k item=image name=thumbnails}
                        <li id="thumbnail_{$k|escape:'htmlall':'UTF-8'}" {if $smarty.foreach.thumbnails.last}class="last eciprod_thumb_image_item"{else}class="eciprod_thumb_image_item"{/if}>
                            <a href="{$image|escape:'html':'UTF-8'}"	data-fancybox-group="other-views" class="fancybox" target="_blank" title="">
                                <img class="eciprod_thumb_image" id="thumb_{$k|escape:'htmlall':'UTF-8'}" src="{$image|escape:'htmlall':'UTF-8'}" alt=""  title="" itemprop="image" onmouseover="document.getElementById('eciprod_bigpic_image').src=this.src;" />
                            </a>
                        </li>
                    {/foreach}
                {/if}
            </ul>
            <div class="eciprod_slide_buttons_block">
            {if isset($product.images) && count($product.images) > 3}
                <span class="eciprod_slide_button" onclick="plusDivs(1);">▶</span>
            {/if}
            </div>
        </div>
    </div>

    <div id="eciprod_infos_block">
    	<h1 itemprop="name">{$product.name|escape:'html':'UTF-8'}</h1>
    	<p id="eciprod_product_reference"{if empty($product.reference) || !$product.reference} style="display: none;"{/if}>
            <label>{l s='Reference :' mod='ecicdiscountpro'} </label><span class="editable" itemprop="sku"{if !empty($product.reference) && $product.reference} content="{$product.reference|escape:'htmlall':'UTF-8'}"{/if}>{$product.reference|escape:'html':'UTF-8'}</span>
    	</p>
        <p id="eciprod_product_manufacturer"{if empty($product.manufacturer) || !$product.manufacturer} style="display: none;"{/if}>
            <label>{l s='Manufacturer :' mod='ecicdiscountpro'} </label><span class="editable" itemprop="sku"{if !empty($product.manufacturer) && $product.manufacturer} content="{$product.manufacturer|escape:'htmlall':'UTF-8'}"{/if}>{$product.manufacturer|escape:'html':'UTF-8'}</span>
    	</p>
    	{if !empty($product.description)}
        <div id="eciprod_short_description_block">
            {if $product.description}
                <div id="eciprod_short_description_content" class="rte align_justify" itemprop="description">{$product.description nofilter}</div>{* We don't want HTML to be escaped !*}
            {/if}
        </div>
    	{/if}
        {if !(empty($product.price) && empty($product.pmvc))}
        <div id="eciprod_price">
            <label>{l s='Price' mod='ecicdiscountpro'}</label>
            <p>{l s='Buy price (tax exclude) :' mod='ecicdiscountpro'} {$product.price|escape:'htmlall':'UTF-8'}</p>
            <p>{l s='Sell price (tax exclude) :' mod='ecicdiscountpro'} {$product.pmvc|escape:'htmlall':'UTF-8'}</p>
            <p>{l s='VAT application :' mod='ecicdiscountpro'} {$product.rate|escape:'htmlall':'UTF-8'}%</p>
        </div>
    	{/if}
        {if isset($product.category)}
            <div class="eciprod_categories">
                <label>{l s='Categories :' mod='ecicdiscountpro'}</label>
                {foreach from=$product.category item=path}
                    <br>{$path|escape:'htmlall':'UTF-8'}
                {/foreach}
            </div>
    	{/if}
        {if isset($product.stock)}
        <div id="eciprod_stock">
            <p><label>{l s='Available quantity :' mod='ecicdiscountpro'}</label> {$product.stock|escape:'htmlall':'UTF-8'}</p>
        </div>
    	{/if}
    </div>
</div>

{if isset($product.attribute)}
<div id="eciprod_variations_table">
    <h3 id="eciprod_variations_heading">{l s='Combinations' mod='ecicdiscountpro'}</h3>
    <table class="eciprod_variations_table">
        <tr class="eciprod_variations_table_row eciprod_variations_table_header">
            <th class="eciprod_variations_table_header_cell">{l s='Supplier Reference' mod='ecicdiscountpro'}</th>
            {if isset($product.attribute.0.reference)}<th class="eciprod_variations_table_header_cell">{l s='Reference' mod='ecicdiscountpro'}</th>{/if}
            <th class="eciprod_variations_table_header_cell">{l s='Attributes' mod='ecicdiscountpro'}</th>
            <th class="eciprod_variations_table_header_cell">{l s='Wholesale price (TE)' mod='ecicdiscountpro'}</th>
            <th class="eciprod_variations_table_header_cell">{l s='Retail price (TE)' mod='ecicdiscountpro'}</th>
            {if isset($product.attribute.0.stock)}<th class="eciprod_variations_table_header_cell">{l s='Stock' mod='ecicdiscountpro'}</th>{/if}
        </tr>
        {foreach from=$product.attribute item=list_attribute}
        <tr class="eciprod_variations_table_row eciprod_variations_table_data">
            <td class="eciprod_variations_table_data_cell">{$list_attribute['reference_attribute']|escape:'htmlall':'UTF-8'}</td>
            {if isset($product.attribute.0.reference)}<td class="eciprod_variations_table_data_cell">{$list_attribute['reference']|escape:'htmlall':'UTF-8'}</td>{/if}
            <td class="eciprod_variations_table_data_cell">{$list_attribute['attribute']|escape:'htmlall':'UTF-8'}</td>
            <td class="eciprod_variations_table_data_cell">{$list_attribute['price']|escape:'htmlall':'UTF-8'}</td>
            <td class="eciprod_variations_table_data_cell">{$list_attribute['pmvc']|escape:'htmlall':'UTF-8'}</td>
            {if isset($product.attribute.0.stock)}<td class="eciprod_variations_table_data_cell">{$list_attribute['stock']|escape:'htmlall':'UTF-8'}</td>{/if}
        </tr>
        {/foreach}
    </table>
</div>
{/if}

{if isset($product.features) && $product.features}
<section id="eciprod_datasheet_block">
    <h3 id="eciprod_datasheet_heading">{l s='Data sheet' mod='ecicdiscountpro'}</h3>
    <table class="eciprod_datasheet_table">
        {foreach from=$product.features key=k item=feature}
            {if isset($feature.0)}
            <tr class="eciprod_datasheet_table_row eciprod_datasheet_table_data {cycle values="odd,even"}">
                <td class="eciprod_datasheet_table_data_cell">{$k|escape:'html':'UTF-8'}</td>
                <td class="eciprod_datasheet_table_data_cell">{$feature|escape:'html':'UTF-8'}</td>
            </tr>
            {/if}
        {/foreach}
    </table>
</section>
{/if}

{if isset($product.pack) && $product.pack}
<section id="eciprod_components_block">
    <h3 id="eciprod_components_heading">{l s='Components list' mod='ecicdiscountpro'}</h3>
    <table class="eciprod_components_table">
        <tr class="eciprod_components_table_row eciprod_components_table_header">
            <th class="eciprod_components_table_header_cell">{l s='Reference' mod='ecicdiscountpro'}</th>
            <th class="eciprod_components_table_header_cell">{l s='Quantity' mod='ecicdiscountpro'}</th>
        </tr>
        {foreach from=$product.pack item=component}
        <tr class="eciprod_components_table_row eciprod_components_table_data">
            <td class="eciprod_components_table_data_cell">{$component['reference']|escape:'htmlall':'UTF-8'}</td>
            <td class="eciprod_components_table_data_cell">{$component['quantity']|escape:'htmlall':'UTF-8'}</td>
        </tr>
        {/foreach}
    </table>
</section>
{/if}
