{*
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to a commercial license from SARL Ether Creation
 * Use, copy, modification or distribution of this source file without written
 * license agreement from the SARL Ether Creation is strictly forbidden.
 * In order to obtain a license, please contact us: contact@ethercreation.com
 * ...........................................................................
 * INFORMATION SUR LA LICENCE D'UTILISATION
 *
 * L'utilisation de ce fichier source est soumise a une licence commerciale
 * concedee par la societe Ether Creation
 * Toute utilisation, reproduction, modification ou distribution du present
 * fichier source sans contrat de licence ecrit de la part de la SARL Ether Creation est
 * expressement interdite.
 * Pour obtenir une licence, veuillez contacter la SARL Ether Creation a l'adresse: contact@ethercreation.com
 * ...........................................................................
 *
 * @author    Ether Creation SARL <contact@ethercreation.com>
 * @copyright 2008-2021 Ether Creation SARL
 * @license   Commercial license
 * International Registered Trademark & Property of Ether Creation SARL
 *}
<div id="ec_paramaters">
    <div class="bootstrap">
        <div class="alert alert-danger" id="parameter_erreur" style="display:none">
            <button class="close" data-dismiss="alert" type="button">x</button>
            <h4>{l s='There is only 1 error.' mod='ecicdiscountpro'}</h4>
            <ul class="list-unstyled">
                <li>
                    {l s='An error occurred while trying to save.' mod='ecicdiscountpro'}
                </li>
            </ul>
        </div>

        <div class="alert alert-success" id="parameter_succes" style="display:none">
            <button class="close" data-dismiss="alert" type="button">x</button>
            <h4>{l s='Congratulations.' mod='ecicdiscountpro'}}</h4>
            <ul class="list-unstyled">
                <li>
                    {l s='Parameters successfully saved.' mod='ecicdiscountpro'}
                </li>
            </ul>
        </div>
    </div>

    <div style="">
        <span id="parameter_connecteur"></span>
    </div>

    <div style="">
        <span id="parameter_connecteur_specifique"></span>
    </div>
</div>