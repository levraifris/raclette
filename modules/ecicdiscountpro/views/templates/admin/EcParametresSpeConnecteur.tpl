{*
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to a commercial license from SARL Ether Creation
 * Use, copy, modification or distribution of this source file without written
 * license agreement from the SARL Ether Creation is strictly forbidden.
 * In order to obtain a license, please contact us: contact@ethercreation.com
 * ...........................................................................
 * INFORMATION SUR LA LICENCE D'UTILISATION
 *
 * L'utilisation de ce fichier source est soumise a une licence commerciale
 * concedee par la societe Ether Creation
 * Toute utilisation, reproduction, modification ou distribution du present
 * fichier source sans contrat de licence ecrit de la part de la SARL Ether Creation est
 * expressement interdite.
 * Pour obtenir une licence, veuillez contacter la SARL Ether Creation a l'adresse: contact@ethercreation.com
 * ...........................................................................
 *
 * @author    Ether Creation SARL <contact@ethercreation.com>
 * @copyright 2008-2021 Ether Creation SARL
 * @license   Commercial license
 * International Registered Trademark & Property of Ether Creation SARL
 *}
<input type="hidden" id='eci_ajaxlink' value="{$eci_ajaxlink|escape:'htmlall':'UTF-8'}"/>
<div class="panel">
    <div class="panel-heading">{l s='Parameters' mod='ecicdiscountpro'} {$connecteur|escape:'htmlall':'UTF-8'}</div>

    <div class="form-group">
        <div class="col-lg-1"><span class="pull-right"></span></div>
        <label class="control-label col-lg-2">{l s='Activate connector :' mod='ecicdiscountpro'}</label>
        <div class="col-lg-9">
            <span class="switch prestashop-switch fixed-width-lg">
                <input type="radio" {if (int)$ACTIVE|escape:'htmlall':'UTF-8' == '1'}checked="checked"{/if} value="1" id="active_on" name="ACTIVE" class="eci_cdiscountpro_conf">
                <label class="radioCheck" for="active_on">
                       {l s='Yes' mod='ecicdiscountpro'}
                </label>
                <input type="radio" value="0" {if (int)$ACTIVE|escape:'htmlall':'UTF-8' == '0'}checked="checked"{/if} id="active_off" name="ACTIVE" class="eci_cdiscountpro_conf">
                <label class="radioCheck" for="active_off">
                        {l s='No' mod='ecicdiscountpro'}
                </label>
                <a class="slide-button btn"></a>
            </span>
        </div>
    </div>

    <div class="form-group">
        <div class="col-lg-1"><span class="pull-right"></span></div>
        <label class="control-label col-lg-2">{l s='Act on dereferenced products :' mod='ecicdiscountpro'}</label>
        <div class="col-lg-9">
            <span class="switch prestashop-switch fixed-width-lg">
                <input type="radio" id="action_on" name="ACTION_PRODUCT_DEREF" value="1" {if (int)$ACTION_PRODUCT_DEREF|escape:'htmlall':'UTF-8' == '1'}checked="checked"{/if} class="eci_cdiscountpro_conf">
                <label class="radioCheck" for="action_on">
                    {l s='Yes' mod='ecicdiscountpro'}
                </label>
                <input type="radio" id="action_off" name="ACTION_PRODUCT_DEREF" value="0" {if (int)$ACTION_PRODUCT_DEREF|escape:'htmlall':'UTF-8' == '0'}checked="checked"{/if} class="eci_cdiscountpro_conf">
                <label class="radioCheck" for="action_off">
                    {l s='No' mod='ecicdiscountpro'}
                </label>
                <a class="slide-button btn"></a>
            </span>
        </div>
    </div>

    <div class="form-group">
        <div class="col-lg-1"><span class="pull-right"></span></div>
        <label class="control-label col-lg-2">{l s='Action on dereferenced products :' mod='ecicdiscountpro'}</label>
        <div class="col-lg-9">
            <span class="switch prestashop-switch fixed-width-lg">
                <input type="radio" id="update_on" name="UPDATE_PRODUCT_DEREF" value="1" {if (int)$UPDATE_PRODUCT_DEREF|escape:'htmlall':'UTF-8' == '1'}checked="checked"{/if} class="eci_cdiscountpro_conf">
                <label class="radioCheck" for="update_on">
                    {l s='Delete' mod='ecicdiscountpro'}
                </label>
                <input type="radio" id="update_off" name="UPDATE_PRODUCT_DEREF" value="0" {if (int)$UPDATE_PRODUCT_DEREF|escape:'htmlall':'UTF-8' == '0'}checked="checked"{/if} class="eci_cdiscountpro_conf">
                <label class="radioCheck" for="update_off">
                    {l s='Disable' mod='ecicdiscountpro'}
                </label>
                <a class="slide-button btn"></a>
            </span>
        </div>
    </div>

    <div class="form-group">
        <div class="col-lg-1"><span class="pull-right"></span></div>
        <label class="control-label col-lg-2">{l s='Status of newly imported products :' mod='ecicdiscountpro'}</label>
        <div class="col-lg-9">
            <span class="switch prestashop-switch fixed-width-lg">
                <input type="radio" id="new_on" name="PARAM_NEWPRODUCT" value="1" {if (int)$PARAM_NEWPRODUCT|escape:'htmlall':'UTF-8' == '1'}checked="checked"{/if} class="eci_cdiscountpro_conf">
                <label class="radioCheck" for="new_on">
                    {l s='Enable' mod='ecicdiscountpro'}
                </label>
                <input type="radio" id="new_off" name="PARAM_NEWPRODUCT" value="0" {if (int)$PARAM_NEWPRODUCT|escape:'htmlall':'UTF-8' == '0'}checked="checked"{/if} class="eci_cdiscountpro_conf">
                <label class="radioCheck" for="new_off">
                    {l s='Disable' mod='ecicdiscountpro'}
                </label>
                <a class="slide-button btn"></a>
            </span>
        </div>
    </div>

    <div class="form-group">
        <div class="col-lg-1"><span class="pull-right"></span></div>
        <label class="control-label col-lg-2">{l s='Associate all supplier carriers to its products' mod='ecicdiscountpro'} :</label>
        <div class="col-lg-9">
            <span class="switch prestashop-switch fixed-width-lg">
                <input type="radio" id="assoccar_on" name="ASSOC_CARRIERS" value="1" {if (int)$ASSOC_CARRIERS|escape:'htmlall':'UTF-8' == '1'}checked="checked"{/if} class="eci_cdiscountpro_conf">
                <label class="radioCheck" for="assoccar_on">
                    {l s='YES' mod='ecicdiscountpro'}
                </label>
                <input type="radio" id="assoccar_off" name="ASSOC_CARRIERS" value="0" {if (int)$ASSOC_CARRIERS|escape:'htmlall':'UTF-8' == '0'}checked="checked"{/if} class="eci_cdiscountpro_conf">
                <label class="radioCheck" for="assoccar_off">
                    {l s='NO' mod='ecicdiscountpro'}
                </label>
                <a class="slide-button btn"></a>
            </span>
        </div>
    </div>

    <div class="form-group">
        <div class="col-lg-1"><span class="pull-right"></span></div>
        <label class="control-label col-lg-2">{l s='Desactivate products out of stock :' mod='ecicdiscountpro'}</label>
        <div class="col-lg-9">
            <span class="switch prestashop-switch fixed-width-lg">
                <input type="radio" id="stock_on" name="STOCK_ENABLE" value="1" {if (int)$STOCK_ENABLE|escape:'htmlall':'UTF-8' == '1'}checked="checked"{/if} class="eci_cdiscountpro_conf">
                <label class="radioCheck" for="stock_on">
                    {l s='Yes' mod='ecicdiscountpro'}
                </label>
                <input type="radio" id="stock_off" name="STOCK_ENABLE" value="0" {if (int)$STOCK_ENABLE|escape:'htmlall':'UTF-8' == '0'}checked="checked"{/if} class="eci_cdiscountpro_conf">
                <label class="radioCheck" for="stock_off">
                    {l s='No' mod='ecicdiscountpro'}
                </label>
                <a class="slide-button btn"></a>
            </span>
        </div>
    </div>

    <div class="form-group">
        <div class="col-lg-1"><span class="pull-right"></span></div>
        <label class="control-label col-lg-2">{l s='Activate products in stock :' mod='ecicdiscountpro'}</label>
        <div class="col-lg-9">
            <span class="switch prestashop-switch fixed-width-lg">
                <input type="radio" id="react_on" name="REACTIVATE" value="1" {if (int)$REACTIVATE|escape:'htmlall':'UTF-8' == '1'}checked="checked"{/if} class="eci_cdiscountpro_conf">
                <label class="radioCheck" for="react_on">
                    {l s='Yes' mod='ecicdiscountpro'}
                </label>
                <input type="radio" id="react_off" name="REACTIVATE" value="0" {if (int)$REACTIVATE|escape:'htmlall':'UTF-8' == '0'}checked="checked"{/if} class="eci_cdiscountpro_conf">
                <label class="radioCheck" for="react_off">
                    {l s='No' mod='ecicdiscountpro'}
                </label>
                <a class="slide-button btn"></a>
            </span>
        </div>
    </div>

    <div class="form-group">
        <div class="col-lg-1"><span class="pull-right"></span></div>
        <label class="control-label col-lg-2">{l s='Update wholesale prices with stock :' mod='ecicdiscountpro'}</label>
        <div class="col-lg-9">
            <span class="switch prestashop-switch fixed-width-lg">
                <input type="radio" id="pws_on" name="PRICEWSTOCK" value="1" {if (int)$PRICEWSTOCK|escape:'htmlall':'UTF-8' == '1'}checked="checked"{/if} class="eci_cdiscountpro_conf">
                <label class="radioCheck" for="pws_on">
                    {l s='Yes' mod='ecicdiscountpro'}
                </label>
                <input type="radio" id="pws_off" name="PRICEWSTOCK" value="0" {if (int)$PRICEWSTOCK|escape:'htmlall':'UTF-8' == '0'}checked="checked"{/if} class="eci_cdiscountpro_conf">
                <label class="radioCheck" for="pws_off">
                    {l s='No' mod='ecicdiscountpro'}
                </label>
                <a class="slide-button btn"></a>
            </span>
        </div>
    </div>

    <div class="form-group">
        <div class="col-lg-1"><span class="pull-right"></span></div>
        <label class="control-label col-lg-2">{l s='Update prices with stock :' mod='ecicdiscountpro'}</label>
        <div class="col-lg-9">
            <span class="switch prestashop-switch fixed-width-lg">
                <input type="radio" id="pvws_on" name="PMVCWSTOCK" value="1" {if (int)$PMVCWSTOCK|escape:'htmlall':'UTF-8' == '1'}checked="checked"{/if} class="eci_cdiscountpro_conf">
                <label class="radioCheck" for="pvws_on">
                    {l s='Yes' mod='ecicdiscountpro'}
                </label>
                <input type="radio" id="pvws_off" name="PMVCWSTOCK" value="0" {if (int)$PMVCWSTOCK|escape:'htmlall':'UTF-8' == '0'}checked="checked"{/if} class="eci_cdiscountpro_conf">
                <label class="radioCheck" for="pvws_off">
                    {l s='No' mod='ecicdiscountpro'}
                </label>
                <a class="slide-button btn"></a>
            </span>
        </div>
    </div>

    <div class="form-group">
        <div class="col-lg-1"><span class="pull-right"></span></div>
        <label class="control-label col-lg-2">{l s='Send orders :' mod='ecicdiscountpro'}</label>
        <div class="col-lg-9">
            <span class="switch prestashop-switch fixed-width-lg">
                <input type="radio" id="imp_on" name="IMPORT_AUTO" value="1" {if (int)$IMPORT_AUTO|escape:'htmlall':'UTF-8' == '1'}checked="checked"{/if} class="eci_cdiscountpro_conf">
                <label class="radioCheck" for="imp_on">
                    {l s='AUTO' mod='ecicdiscountpro'}
                </label>
                <input type="radio" id="imp_off" name="IMPORT_AUTO" value="0" {if (int)$IMPORT_AUTO|escape:'htmlall':'UTF-8' == '0'}checked="checked"{/if} class="eci_cdiscountpro_conf">
                <label class="radioCheck" for="imp_off">
                    {l s='MANUAL' mod='ecicdiscountpro'}
                </label>
                <a class="slide-button btn"></a>
            </span>
        </div>
    </div>

    <div class="form-group">
        <div class="col-lg-1"><span class="pull-right"></span></div>
        <label class="control-label col-lg-2">{l s='Automatically send orders when in states' mod='ecicdiscountpro'} :</label>
        <div class="input-group col-lg-2">
            <select multiple name="IMPORT_OST" class="eci_cdiscountpro_conf">
                {foreach from=$orderStates item=orderState}
                    <option value="{$orderState.id_order_state|escape:'htmlall':'UTF-8'}" {if isset($IMPORT_OST)}{if $orderState.id_order_state|escape:'htmlall':'UTF-8'|in_array:$IMPORT_OST}selected="selected"{/if}{/if}>{$orderState.name|escape:'htmlall':'UTF-8'}</option>
                {/foreach}
            </select>
        </div>
    </div>

    <div class="form-group">
        <div class="col-lg-1"><span class="pull-right"></span></div>
        <label class="control-label col-lg-2">{l s='Delivery address :' mod='ecicdiscountpro'}</label>
        <div class="col-lg-9">
            <span class="switch prestashop-switch fixed-width-lg">
                <input type="radio" id="add_on" name="IMPORT_ADDRESS" value="1" {if (int)$IMPORT_ADDRESS|escape:'htmlall':'UTF-8' == '1'}checked="checked"{/if} class="eci_cdiscountpro_conf">
                <label class="radioCheck" for="add_on">
                    {l s='STORE' mod='ecicdiscountpro'}
                </label>
                <input type="radio" id="add_off" name="IMPORT_ADDRESS" value="0" {if (int)$IMPORT_ADDRESS|escape:'htmlall':'UTF-8' == '0'}checked="checked"{/if} class="eci_cdiscountpro_conf">
                <label class="radioCheck" for="add_off">
                    {l s='CLIENT' mod='ecicdiscountpro'}
                </label>
                <a class="slide-button btn"></a>
            </span>
        </div>
    </div>

    <div class="form-group">
        <div class="col-lg-1"><span class="pull-right"></span></div>
        <label class="control-label col-lg-2">{l s='Are you subject to VAT ?' mod='ecicdiscountpro'}</label>
        <div class="col-lg-9">
            <span class="switch prestashop-switch fixed-width-lg">
                <input type="radio" id="notva_on" name="NOTVA" value="0" {if (int)$NOTVA|escape:'htmlall':'UTF-8' === 0}checked="checked"{/if} class="eci_cdiscountpro_conf">
                <label class="radioCheck" for="notva_on">
                    {l s='Yes' mod='ecicdiscountpro'}
                </label>
                <input type="radio" id="notva_off" name="NOTVA" value="1" {if (int)$NOTVA|escape:'htmlall':'UTF-8' === 1}checked="checked"{/if} class="eci_cdiscountpro_conf">
                <label class="radioCheck" for="notva_off">
                    {l s='No' mod='ecicdiscountpro'}
                </label>
                <a class="slide-button btn"></a>
            </span>
        </div>
    </div>

    <div class="form-group">
        <div class="col-lg-1"><span class="pull-right"></span></div>
        <label class="control-label col-lg-2">{l s='Fix markup (0 → use recommended selling price) :' mod='ecicdiscountpro'}</label>
        <div class="col-lg-2">
            <div class="input-group">
                <span class="input-group-addon">%</span>
                <input type="text" value="{$MARGE|escape:'htmlall':'UTF-8'}" name="MARGE" id="MARGE" maxlength="27" size="11" class="eci_cdiscountpro_conf">
            </div>
        </div>
    </div>

{*
    <div>
        <a class="default_parameters btn btn-default" id="{$connecteur|escape:'htmlall':'UTF-8'}" href="#">
            <i class="icon-repeat"></i> {l s='Default' mod='ecicdiscountpro'}
        </a>
    </div>
*}
    <div class="btnCon">
        <a class="" href="javascript:save_parameter_connecteur();">
            <button type="submit" value="1" id="configuration_form_submit_btn" name="submit" class="btn btn-default pull-right">
                <i class="process-icon-save"></i> {l s='Save' mod='ecicdiscountpro'}
            </button>
        </a>
    </div>

</div>
