/**
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to a commercial license from SARL Ether Creation
 * Use, copy, modification or distribution of this source file without written
 * license agreement from the SARL Ether Creation is strictly forbidden.
 * In order to obtain a license, please contact us: contact@ethercreation.com
 * ...........................................................................
 * INFORMATION SUR LA LICENCE D'UTILISATION
 *
 * L'utilisation de ce fichier source est soumise a une licence commerciale
 * concedee par la societe Ether Creation
 * Toute utilisation, reproduction, modification ou distribution du present
 * fichier source sans contrat de licence ecrit de la part de la SARL Ether Creation est
 * expressement interdite.
 * Pour obtenir une licence, veuillez contacter la SARL Ether Creation a l'adresse: contact@ethercreation.com
 * ...........................................................................
 *
 * @author    Ether Creation SARL <contact@ethercreation.com>
 * @copyright 2008-2021 Ether Creation SARL
 * @license   Commercial license
 * International Registered Trademark & Property of Ether Creation SARL
 */
var tmpDisplayInfoRetour;
var eciSlidePos;
var eci_ajaxlink;
function displayInfoRetour(message, etat) {
    var ec_token = $("#ec_token").val();
    clearTimeout(tmpDisplayInfoRetour);

    if (etat === 'message') {
        $.ajax({
//            url: "../index.php?fc=module&module=ecicdiscountpro&controller=ajax",
//            url: "../module/ecicdiscountpro/ajax",
            url: eci_ajaxlink,
            type: "POST",
            async : false, //mode synchrone
            data: {
                majsel: 55,
                ec_token: ec_token,
                etat: etat,
                message: message
            },
            dataType: 'html'
        }).done(function( data ) {
            return data;
        });
    } else {
        $.ajax({
//            url: "../index.php?fc=module&module=ecicdiscountpro&controller=ajax",
//            url: "../module/ecicdiscountpro/ajax",
            url: eci_ajaxlink,
            type: "POST",
            data: {
                majsel: 55,
                ec_token: ec_token,
                etat: etat,
                message: message
            },
            dataType: 'html'
        }).done(function( data ) {
            $('#display_message').addClass('message_over').show().html(data).css("opacity", "1");
            tmpDisplayInfoRetour = setTimeout(function(){
                $('#display_message').animate({opacity: 0}, 500, function(){$(this).hide(function(){$(this).html('', function(){$(this).removeClass('message_over');});});});
            }, 10000);
        });
    }
}

$(document).ready(function () {
    eci_ajaxlink = $("#eci_ajaxlink").val();
    $('th').removeClass('showpcard');
    function pleaseWait(selector) {
        $(selector).html('<div style="background: rgba(255, 255, 255, 1);background: rgba(255, 255, 255, .5); height: 100%; width: 100%; white-space: nowrap; text-align: center;"><span style="display: inline-block; height: 100%; vertical-align: middle;"></span><img style="vertical-align:middle;" src="../modules/ecicdiscountpro/views/img/loading.gif"></div>');
    }
    function go(url){document.location.href=url;}
    var old_location = window.location.href;

    //category filter
    var filter_by_category = parseInt($('#eci_filter_by_category').val());
    $('div .tree-panel-heading-controls').prepend('<input id="filter_by_category" name="filter_by_category" type="checkbox" value="'+filter_by_category+'" '+(0<filter_by_category ? 'checked>' : '>'));
    if (0<filter_by_category) {
        $('.cattree').children().show();
    } else {
        $('.cattree').children().hide();
    }
    $(document).on('change', '#filter_by_category', function(){
        var old_fbc =  parseInt($('#filter_by_category').val());
        var new_fbc =  1 - old_fbc;
        $('#filter_by_category').val(new_fbc);
        if (1 === new_fbc) {
            $('#filter_by_category').attr('checked', 'checked');
            $('.tree-selected').removeClass('tree-selected');
            $('input[name=categoryBox]').removeAttr('checked');
            $('.cattree').children().show();
        } else {
            $('#filter_by_category').removeAttr('checked');
            $('.cattree').children().hide();
            $('.tree-selected').removeClass('tree-selected');
            $('input[name=categoryBox]').removeAttr('checked');
            var new_location = old_location.replace(/&selectedcategory=[^&]+/g, '').replace(/&filter_by_category=[01]/g, '&filter_by_category=0').replace(/#eci_catalog/g, '')+'#eci_catalog';
            go(new_location);
        }
    });

    $(document).on('change', "#associated-categories-tree li input", function() {
        var new_location = old_location.replace(/&filter_by_category=[01]/g, '');
        new_location = new_location.replace(/&selectedcategory=(.+?)(&|#)/g, '$2');
        new_location = new_location.replace(/#eci_catalog/g, '');
        new_location = new_location+'&filter_by_category=1&selectedcategory='+$(this).val()+'#eci_catalog';
        window.history.pushState("", "", new_location);
        location.reload();
        $( "input[name*='eci_catalogFilter_category']" ).css('font-size','0');
        $( "input[name*='eci_catalogFilter_category']" ).val( $(this).val() );
        var old_action = $('#form-eci_catalog').attr('action');
        var new_action = old_action.replace(/#eci_catalog/g, '');
        $('#form-eci_catalog').attr('action', new_action+'&filter_by_category=1&selectedcategory='+$(this).val()+'#eci_catalog');
        $('#submitFilterButtoneci_catalog').trigger('click');
    });

    //
    if ($('#form-eci_catalog').length >0) {
        var catval = $( "input[name*='eci_catalogFilter_category']" ).val();
        if (typeof catval !== "undefined") {
            var valtab = $('#form-eci_catalog .panel-heading').html();
            valtab = valtab.split(':');
            valtab = valtab[0];
            catval = catval.split('::>>::');
            $("input[name*='eci_catalogFilter_category']").val(catval[catval.length-1]);
            $('#form-eci_catalog .panel-heading').html(valtab+' : '+catval[catval.length-1]);
            if ($('#form-eci_catalog').length >0) {
                var pagetitle = $('.page-head .page-title').html();
                pagetitle = pagetitle.split(':');
                pagetitle = pagetitle[0];
                $('.page-head .page-title').html(pagetitle+' : '+catval[catval.length-1]);
            }
        }

        $("a.fancybox").fancybox({
            'width': '1000px',
            helpers: {
                overlay: {
                    locked: false
                }
            }
        });
        $("span.fancybox").fancybox({
            'type': 'iframe',
            'width': '1000px'
        });
        $( ".table.eci_catalog td .btn" ).each(function( index ) {
            var test2 = $(this).attr('href').split('&product_reference=');
            test2 = test2[1].split('&');
            $(this).addClass('fancybox');
            $(this).attr('href', '#eci_prod');
            $(this).attr('data-ref', test2[0]);
        });
    }

    //lines
    $(".table.eci_catalog td").attr('onclick','').addClass('fancybox');

    $('body').on('click', 'a.fancybox', function() {
        var ec_token = $("#ec_token").val();
        var iso_code = $("#iso_code").val();
        var connecteur = $("#fournisseur").val();
        var reference = $(this).attr('data-ref');
        $.ajax({
//            url: "../index.php?fc=module&module=ecicdiscountpro&controller=ajax",
//            url: "../module/ecicdiscountpro/ajax",
            url: eci_ajaxlink,
            type: "POST",
            data: {
                majsel: 1,
                ec_token: ec_token,
                reference: reference,
                connecteur: connecteur,
                iso_code: iso_code
            },
            dataType: 'html'
        }).done(function( data ) {
            $("#eci_prod").html(data);
            eciSlidePos = 0;
            showDivs();
        });
    });
    $(document).on('click', ".showpcard", function(e) {
        e.preventDefault();
        var p = $(this).parent();
        $('td:last-child a',$(p)).click();
    });

    //task followup
    $(document).on('click', "#refreshcat", function() {
        $('.ecicpanel[data-prefix=ECI_CC]').show();
    });
    $(document).on('click', "#synccat", function() {
        $('.ecicpanel[data-prefix=ECI_SS]').show();
    });

});
function plusDivs(n) {
    var parent = document.getElementById("eciprod_thumbs_list");
    var x = document.getElementsByClassName("eciprod_thumb_image_item");
    var t = x.length;
    if (t > 3) {
        if ((n === 1) && (eciSlidePos < (t - 3))) {
            parent.appendChild(parent.firstChild);
            parent.appendChild(parent.firstChild);
            eciSlidePos += 1;
        } else if ((n === 0) && (eciSlidePos > 0)) {
            parent.insertBefore(parent.lastChild,parent.firstChild);
            parent.insertBefore(parent.lastChild,parent.firstChild);
            eciSlidePos -= 1;
        }
    }
    showDivs();
}
function showDivs() {
    var i;
    var x = document.getElementsByClassName("eciprod_thumb_image_item");
    var t = x.length;
    for (i = 0; i < t; i++) {
        if (i < 3) {
            x[i].style.display = "block";
        } else {
            x[i].style.display = "none";
        }
    }
}
