/**
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to a commercial license from SARL Ether Creation
 * Use, copy, modification or distribution of this source file without written
 * license agreement from the SARL Ether Creation is strictly forbidden.
 * In order to obtain a license, please contact us: contact@ethercreation.com
 * ...........................................................................
 * INFORMATION SUR LA LICENCE D'UTILISATION
 *
 * L'utilisation de ce fichier source est soumise a une licence commerciale
 * concedee par la societe Ether Creation
 * Toute utilisation, reproduction, modification ou distribution du present
 * fichier source sans contrat de licence ecrit de la part de la SARL Ether Creation est
 * expressement interdite.
 * Pour obtenir une licence, veuillez contacter la SARL Ether Creation a l'adresse: contact@ethercreation.com
 * ...........................................................................
 *
 * @author    Ether Creation SARL <contact@ethercreation.com>
 * @copyright 2008-2021 Ether Creation SARL
 * @license   Commercial license
 * International Registered Trademark & Property of Ether Creation SARL
 */

$(document).ready(function() {
    $('select').removeClass('fixed-width-xl');
    $('input[name=reset_supplier]').parent().addClass('hidden');
    $('input[name=reset_tabs]').parent().addClass('hidden');
    $('input[name=choose_tabs]').parent().addClass('hidden');
    $('input[name=reset_hooks]').parent().addClass('hidden');
    $('input[name=reset_token]').parent().addClass('hidden');
    $('input[name=reset_fname]').parent().addClass('hidden');
    $('input[name=add_ip_maint]').parent().addClass('hidden');
    $('input[name=reset_actreason]').parent().addClass('hidden');
    $(document).on('click', '.icon-cogs', function(){
        $('input[name=reset_supplier]').parent().toggleClass('hidden');
        $('input[name=reset_tabs]').parent().toggleClass('hidden');
        $('input[name=choose_tabs]').parent().toggleClass('hidden');
        $('input[name=reset_hooks]').parent().toggleClass('hidden');
        $('input[name=reset_token]').parent().toggleClass('hidden');
        $('input[name=reset_fname]').parent().toggleClass('hidden');
        $('input[name=add_ip_maint]').parent().toggleClass('hidden');
        $('input[name=reset_actreason]').parent().toggleClass('hidden');
    });
});
