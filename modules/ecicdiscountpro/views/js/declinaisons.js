/**
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to a commercial license from SARL Ether Creation
 * Use, copy, modification or distribution of this source file without written
 * license agreement from the SARL Ether Creation is strictly forbidden.
 * In order to obtain a license, please contact us: contact@ethercreation.com
 * ...........................................................................
 * INFORMATION SUR LA LICENCE D'UTILISATION
 *
 * L'utilisation de ce fichier source est soumise a une licence commerciale
 * concedee par la societe Ether Creation
 * Toute utilisation, reproduction, modification ou distribution du present
 * fichier source sans contrat de licence ecrit de la part de la SARL Ether Creation est
 * expressement interdite.
 * Pour obtenir une licence, veuillez contacter la SARL Ether Creation a l'adresse: contact@ethercreation.com
 * ...........................................................................
 *
 * @author    Ether Creation SARL <contact@ethercreation.com>
 * @copyright 2008-2021 Ether Creation SARL
 * @license   Commercial license
 * International Registered Trademark & Property of Ether Creation SARL
 */
var tmpDisplayInfoRetour;
var eci_ajaxlink;
$(document).ready(function () {
    eci_ajaxlink = $("#eci_ajaxlink").val();
});
function displayInfoRetour(message, etat) {
    var ec_token = $("#ec_token").val();
    clearTimeout(tmpDisplayInfoRetour);
    if (etat === 'message') {
        $.ajax({
//            url: "../modules/ecicdiscountpro/ajax.php",
//            url: "../index.php?fc=module&module=ecicdiscountpro&controller=ajax",
//            url: "../module/ecicdiscountpro/ajax",
            url: eci_ajaxlink,
            type: "POST",
            async : false, //mode synchrone
            data: {
                majsel: 55,
                ec_token: ec_token,
                etat: etat,
                message: message
            },
            dataType: 'html'
        }).done(function( data ) {
            return data;
        });
    } else {
        $.ajax({
//            url: "../modules/ecicdiscountpro/ajax.php",
//            url: "../index.php?fc=module&module=ecicdiscountpro&controller=ajax",
//            url: "../module/ecicdiscountpro/ajax",
            url: eci_ajaxlink,
            type: "POST",
            data: {
                majsel: 55,
                ec_token: ec_token,
                etat: etat,
                message: message
            },
            dataType: 'html'
        }).done(function( data ) {
            $('#display_message').addClass('message_over').show().html(data).css("opacity", "1");
            tmpDisplayInfoRetour = setTimeout(function(){
                $('#display_message').animate({opacity: 0}, 500, function(){$(this).hide(function(){$(this).html('', function(){$(this).removeClass('message_over');});});});
            }, 10000);
        });
    }
}

function getAttributeValueMatching(id, page = 1) {
    var ec_token = $('#ec_token').val();
    var id_shop = $('#id_shop').val();
    $.ajax({
//        url: "../modules/ecicdiscountpro/ajax.php",
//        url: "../index.php?fc=module&module=ecicdiscountpro&controller=ajax",
//        url: "../module/ecicdiscountpro/ajax",
        url: eci_ajaxlink,
        type: "POST",
        data: ({
            majsel: 37,
            ec_token: ec_token,
            id_shop: id_shop,
            id: id,
            page: page
        }),
        dataType: "html"
    })
    .done(function (data) {
        $('.receDet'+id).html(data).show(250);
    });
}
function save_attribute(str, str2) {
    var ec_token = $('#ec_token').val();
    var id_shop = $('#id_shop').val();
    var res = str.split("_");
    var res2 = str2.split("_");
    $.ajax({
//        url: "../modules/ecicdiscountpro/ajax.php",
//        url: "../index.php?fc=module&module=ecicdiscountpro&controller=ajax",
//        url: "../module/ecicdiscountpro/ajax",
        url: eci_ajaxlink,
        type: "POST",
        data: ({
            majsel: 35,
            ec_token: ec_token,
            id_shop: id_shop,
            id_attribute_eco: res[0],
            id_attribute: res[1],
            id_attribute_eco_old: res2[0]
        }),
        dataType: "json"
    })
    .done(function (data) {
        displayInfoRetour('18','success');
        //alert('Liaison déclinaison enregistrée avec succès');
    })
    .fail(function (data) {
        displayInfoRetour('19',',nosuccess');
        //alert('Une erreur est survenue pendant l\'enregistrement de la liaison déclinaison.');
    });
	return res[1];
}

$(document).on('change', '.save_attribute', function () {
    var newattributeid = save_attribute($(this).val(), $('option:selected', $(this)).val());
    if ('0' !== newattributeid) {
        $(this).parent().next().next().show();
    } else {
        $(this).parent().next().next().hide();
    }
});
$(document).on('click', '.detAtt', function () {
    var id = $(this).attr('data-idgeco');
    if ($('.detAtt[data-idgeco="'+id+'"]').hasClass('active')) {
        $('.detAtt').removeClass('active');
        $('.receDet').empty().hide();
    } else {
        $('.detAtt').removeClass('active');
        $('.detAtt[data-idgeco="'+id+'"]').addClass('active');
        $('.receDet').empty().hide();
        getAttributeValueMatching(id);
    }
});
$(document).on('click', '.pageDetAtt', function (e) {
    e.preventDefault();
    var id = $(this).attr('data-idgeco');
    var page = $(this).attr('data-page');
    $('.receDet').empty().hide();
    getAttributeValueMatching(id, page);
});
$(document).on('click', '.ajtMatAtt', function () {
    var ec_token = $('#ec_token').val();
//    var id_shop = $('#id_shop').val();
    var id = $(this).attr('data-id');
    $.ajax({
//        url: "../modules/ecicdiscountpro/ajax.php",
//        url: "../index.php?fc=module&module=ecicdiscountpro&controller=ajax",
//        url: "../module/ecicdiscountpro/ajax",
        url: eci_ajaxlink,
        type: "POST",
        data: ({
            majsel: 38,
            ec_token: ec_token,
            id: id
        }),
        dataType: "html"
    })
    .done(function (data) {
        $('.ajtMatAtt').hide();
        $('.ajtMat').append(data);
    });
});
$(document).on('click', '.validMatch', function () {
    var ec_token = $('#ec_token').val();
//    var id_shop = $('#id_shop').val();
    var s = $(this).attr('data-idS');
    var e = $(this).attr('data-idE');
    var v = $('.match_att_val_eco[name=attvaleco]').val();
    var b = $('.match_att_val_ps[name=attvalps]').val();
    $.ajax({
//        url: "../modules/ecicdiscountpro/ajax.php",
//        url: "../index.php?fc=module&module=ecicdiscountpro&controller=ajax",
//        url: "../module/ecicdiscountpro/ajax",
        url: eci_ajaxlink,
        type: "POST",
        data: ({
            majsel: 39,
            ec_token: ec_token,
            b: b,
            v: v,
            s: s,
            e: e
        }),
        dataType: "html"})
    .done(function () {
        getAttributeValueMatching(e);
        displayInfoRetour('18','success');
    });
});
$(document).on('click', '.updateMatch', function () {
    var ec_token = $('#ec_token').val();
//    var id_shop = $('#id_shop').val();
    var s = $(this).attr('data-idS');
    var e = $(this).attr('data-idE');
    var v = $(this).attr('data-idV');
    var b = $('.matched_att_val[name=attvaleco_'+v+']').val();
    $.ajax({
//        url: "../modules/ecicdiscountpro/ajax.php",
//        url: "../index.php?fc=module&module=ecicdiscountpro&controller=ajax",
//        url: "../module/ecicdiscountpro/ajax",
        url: eci_ajaxlink,
        type: "POST",
        data: ({
            majsel: 39,
            ec_token: ec_token,
            b: b,
            v: v,
            s: s
        }),
        dataType: "html"})
    .done(function () {
        getAttributeValueMatching(e);
        displayInfoRetour('18','success');
    })
    .fail(function (data) {
        displayInfoRetour('19','nosuccess');
    });
});
$(document).on('click', '.delMatch', function () {
    var ec_token = $('#ec_token').val();
//    var id_shop = $('#id_shop').val();
    var s = $(this).attr('data-idS');
    var e = $(this).attr('data-idE');
    var v = $(this).attr('data-idV');
    $.ajax({
//        url: "../modules/ecicdiscountpro/ajax.php",
//        url: "../index.php?fc=module&module=ecicdiscountpro&controller=ajax",
//        url: "../module/ecicdiscountpro/ajax",
        url: eci_ajaxlink,
        type: "POST",
        async: true,
        data: ({
            majsel: 40,
            ec_token: ec_token,
            v: v,
            s: s
        }),
        dataType: "html"})
    .done(function () {
        getAttributeValueMatching(e);
    })
    .fail(function (data) {
        displayInfoRetour('19','nosuccess');
    });
});
$(document).on('click', '.delNewMatch', function () {
    var e = $(this).attr('data-idE');
    getAttributeValueMatching(e);
});
