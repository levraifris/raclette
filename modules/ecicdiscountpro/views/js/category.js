/**
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to a commercial license from SARL Ether Creation
 * Use, copy, modification or distribution of this source file without written
 * license agreement from the SARL Ether Creation is strictly forbidden.
 * In order to obtain a license, please contact us: contact@ethercreation.com
 * ...........................................................................
 * INFORMATION SUR LA LICENCE D'UTILISATION
 *
 * L'utilisation de ce fichier source est soumise a une licence commerciale
 * concedee par la societe Ether Creation
 * Toute utilisation, reproduction, modification ou distribution du present
 * fichier source sans contrat de licence ecrit de la part de la SARL Ether Creation est
 * expressement interdite.
 * Pour obtenir une licence, veuillez contacter la SARL Ether Creation a l'adresse: contact@ethercreation.com
 * ...........................................................................
 *
 * @author    Ether Creation SARL <contact@ethercreation.com>
 * @copyright 2008-2021 Ether Creation SARL
 * @license   Commercial license
 * International Registered Trademark & Property of Ether Creation SARL
 */
$("a#showmatchcat").fancybox();
$(document).ready(function () {
    $('th').removeClass('showmatchform');
    //local link
    var href = window.location.href;
    //stop onclick on cells
    $( ".table.eci_category_shop td" ).attr('onclick','');
    //$( ".id_catpsold" ).hide();
    //hide search field for id_category
    $('thead tr:nth-child(2) th:nth-child(4)').addClass('hidden');
    //add class to monitor click
    $( ".table.eci_category_shop td a.btn" ).each(function( index ) {
        $(this).attr('href', '#');
        $(this).addClass('showmatchform');
    });
    //show match form
    $( ".showmatchform" ).click(function(e) {
        e.preventDefault();
        var cateco = $(this).closest('tr').children('.name_cateco').html();
        var id_cateco = $(this).closest('tr').children('.id_cateco').html();
        var id_catpsold = $(this).closest('tr').children('.id_catpsold').html().trim();
        $('#cs_category_name_rel').html(cateco);
        $('#id_cateco').val(id_cateco);
        $('#id_catps').val(id_catpsold);
        $('#showmatchcat').click();
    });
    //hide match form
    $(document).on('click', '#cacher', function (e) {
        e.preventDefault();
        $('.fancybox-close').click();
    });
});
