/**
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to a commercial license from SARL Ether Creation
 * Use, copy, modification or distribution of this source file without written
 * license agreement from the SARL Ether Creation is strictly forbidden.
 * In order to obtain a license, please contact us: contact@ethercreation.com
 * ...........................................................................
 * INFORMATION SUR LA LICENCE D'UTILISATION
 *
 * L'utilisation de ce fichier source est soumise a une licence commerciale
 * concedee par la societe Ether Creation
 * Toute utilisation, reproduction, modification ou distribution du present
 * fichier source sans contrat de licence ecrit de la part de la SARL Ether Creation est
 * expressement interdite.
 * Pour obtenir une licence, veuillez contacter la SARL Ether Creation a l'adresse: contact@ethercreation.com
 * ...........................................................................
 *
 * @author    Ether Creation SARL <contact@ethercreation.com>
 * @copyright 2008-2021 Ether Creation SARL
 * @license   Commercial license
 * International Registered Trademark & Property of Ether Creation SARL
 */
var tmpDisplayInfoRetour;
var eci_ajaxlink;
$(document).ready(function () {
    eci_ajaxlink = $("#eci_ajaxlink").val();
});
function displayInfoRetour(message, etat) {
    var ec_token = $("#ec_token").val();
    clearTimeout(tmpDisplayInfoRetour);

    if (etat === 'message') {
        $.ajax({
//            url: "../modules/ecicdiscountpro/ajax.php",
//            url: "../index.php?fc=module&module=ecicdiscountpro&controller=ajax",
//            url: "../module/ecicdiscountpro/ajax",
            url: eci_ajaxlink,
            type: "POST",
            async : false, //mode synchrone
            data: {
                majsel: 55,
                ec_token: ec_token,
                etat: etat,
                message: message
            },
            dataType: 'html'
        }).done(function( data ) {
            return data;
        });

    } else {
        $.ajax({
//            url: "../modules/ecicdiscountpro/ajax.php",
//            url: "../index.php?fc=module&module=ecicdiscountpro&controller=ajax",
//            url: "../module/ecicdiscountpro/ajax",
            url: eci_ajaxlink,
            type: "POST",
            data: {
                majsel: 55,
                ec_token: ec_token,
                etat: etat,
                message: message
            },
            dataType: 'html'
        }).done(function( data ) {
            $('#display_message').addClass('message_over').show().html(data).css("opacity", "1");
            tmpDisplayInfoRetour = setTimeout(function(){
                $('#display_message').animate({opacity: 0}, 500, function(){$(this).hide(function(){$(this).html('', function(){$(this).removeClass('message_over');});});});
            }, 10000);
        });
    }
}

function getFeatureValueMatching(id, page = 1) {
    var ec_token = $('#ec_token').val();
    var id_shop = $('#id_shop').val();
    $.ajax({
//        url: "../modules/ecicdiscountpro/ajax.php",
//        url: "../index.php?fc=module&module=ecicdiscountpro&controller=ajax",
//        url: "../module/ecicdiscountpro/ajax",
        url: eci_ajaxlink,
        type: "POST",
        data: ({
            majsel: 87,
            ec_token: ec_token,
            id_shop: id_shop,
            id: id,
            page: page
        }),
        dataType: "html"
    })
    .done(function (data) {
        $('.receDet'+id).html(data).show();
    });
}
function save_feature(str, str2) {
    var ec_token = $('#ec_token').val();
    var id_shop = $('#id_shop').val();
    var res = str.split("_");
    var res2 = str2.split("_");
    $.ajax({
//        url: "../modules/ecicdiscountpro/ajax.php",
//        url: "../index.php?fc=module&module=ecicdiscountpro&controller=ajax",
//        url: "../module/ecicdiscountpro/ajax",
        url: eci_ajaxlink,
        type: "POST",
        data: ({
            majsel: 85,
            ec_token: ec_token,
            id_shop: id_shop,
            id_feature_eco: res[0],
            id_feature: res[1],
            id_feature_eco_old: res2[0]
        }),
        dataType: "json"
    })
    .done(function (data) {
        displayInfoRetour('18','success');
        //alert('Liaison déclinaison enregistrée avec succès');
    })
    .fail(function (data) {
        displayInfoRetour('19',',nosuccess');
        //alert('Une erreur est survenue pendant l\'enregistrement de la liaison déclinaison.');
    });
    return res[1];
}

$(document).on('change', '.save_feature', function () {
    var newfeatureid = save_feature($(this).val(), $('option:selected', $(this)).val());
    if ('0' !== newfeatureid) {
        $(this).parent().next().next().show();
    } else {
        $(this).parent().next().next().hide();
    }
});
$(document).on('click', '.detFeat', function () {
    var id = $(this).attr('data-idfeco');
    if ($('.detFeat[data-idfeco="'+id+'"]').hasClass('active')) {
        $('.detFeat').removeClass('active');
        $('.receDet').empty().hide();
    } else {
        $('.detFeat').removeClass('active');
        $('.detFeat[data-idfeco="'+id+'"]').addClass('active');
        $('.receDet').empty().hide();
        getFeatureValueMatching(id);
    }
});
$(document).on('click', '.pageDetFeat', function (e) {
    e.preventDefault();
    var id = $(this).attr('data-idgeco');
    var page = $(this).attr('data-page');
    $('.receDet').empty().hide();
    getFeatureValueMatching(id, page);
});
$(document).on('click', '.ajtMatFeat', function () {
    var ec_token = $('#ec_token').val();
    var id_shop = $('#id_shop').val();
    var id = $(this).attr('data-id');
    $.ajax({
//        url: "../modules/ecicdiscountpro/ajax.php",
//        url: "../index.php?fc=module&module=ecicdiscountpro&controller=ajax",
//        url: "../module/ecicdiscountpro/ajax",
        url: eci_ajaxlink,
        type: "POST",
        data: ({
            majsel: 88,
            ec_token: ec_token,
            id_shop: id_shop,
            id: id
        }),
        dataType: "html"
    })
    .done(function (data) {
        $('.ajtMatFeat').hide();
        $('.ajtMat').append(data);
    });
});
$(document).on('click', '.validMatch', function () {
    var ec_token = $('#ec_token').val();
    var id_shop = $('#id_shop').val();
    var s = $(this).attr('data-idS');
    var e = $(this).attr('data-idE');
    var v = $('.match_feat_val_eco[name=featvaleco]').val();
    var b = $('.match_feat_val_ps[name=featvalps]').val();
    $.ajax({
//        url: "../modules/ecicdiscountpro/ajax.php",
//        url: "../index.php?fc=module&module=ecicdiscountpro&controller=ajax",
//        url: "../module/ecicdiscountpro/ajax",
        url: eci_ajaxlink,
        type: "POST",
        data: ({
            majsel: 89,
            ec_token: ec_token,
            id_shop: id_shop,
            b: b,
            v: v,
            s: s,
            e: e
        }),
        dataType: "html"})
    .done(function () {
        getFeatureValueMatching(e);
        displayInfoRetour('18','success');
    });
});
$(document).on('click', '.updateMatch', function () {
    var ec_token = $('#ec_token').val();
    var id_shop = $('#id_shop').val();
    var s = $(this).attr('data-idS');
    var e = $(this).attr('data-idE');
    var v = $(this).attr('data-idV');
    var b = $('.matched_feat_val[name=featvaleco_'+v+']').val();
    $.ajax({
//        url: "../modules/ecicdiscountpro/ajax.php",
//        url: "../index.php?fc=module&module=ecicdiscountpro&controller=ajax",
//        url: "../module/ecicdiscountpro/ajax",
        url: eci_ajaxlink,
        type: "POST",
        data: ({
            majsel: 89,
            ec_token: ec_token,
            id_shop: id_shop,
            b: b,
            v: v,
            s: s
        }),
        dataType: "html"})
    .done(function () {
        getFeatureValueMatching(e);
        displayInfoRetour('18','success');
    })
    .fail(function (data) {
        displayInfoRetour('19','nosuccess');
    });
});
$(document).on('click', '.delMatch', function () {
    var ec_token = $('#ec_token').val();
    var id_shop = $('#id_shop').val();
    var s = $(this).attr('data-idS');
    var e = $(this).attr('data-idE');
    var v = $(this).attr('data-idV');
    $.ajax({
//        url: "../modules/ecicdiscountpro/ajax.php",
//        url: "../index.php?fc=module&module=ecicdiscountpro&controller=ajax",
//        url: "../module/ecicdiscountpro/ajax",
        url: eci_ajaxlink,
        type: "POST",
        async: true,
        data: ({
            majsel: 90,
            ec_token: ec_token,
            id_shop: id_shop,
            v: v,
            s: s
        }),
        dataType: "html"})
    .done(function () {
        getFeatureValueMatching(e);
    })
    .fail(function (data) {
        displayInfoRetour('19','nosuccess');
    });
});
$(document).on('click', '.delNewMatch', function () {
    var e = $(this).attr('data-idE');
    getFeatureValueMatching(e);
});
