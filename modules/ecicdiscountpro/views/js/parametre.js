/**
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to a commercial license from SARL Ether Creation
 * Use, copy, modification or distribution of this source file without written
 * license agreement from the SARL Ether Creation is strictly forbidden.
 * In order to obtain a license, please contact us: contact@ethercreation.com
 * ...........................................................................
 * INFORMATION SUR LA LICENCE D'UTILISATION
 *
 * L'utilisation de ce fichier source est soumise a une licence commerciale
 * concedee par la societe Ether Creation
 * Toute utilisation, reproduction, modification ou distribution du present
 * fichier source sans contrat de licence ecrit de la part de la SARL Ether Creation est
 * expressement interdite.
 * Pour obtenir une licence, veuillez contacter la SARL Ether Creation a l'adresse: contact@ethercreation.com
 * ...........................................................................
 *
 * @author    Ether Creation SARL <contact@ethercreation.com>
 * @copyright 2008-2021 Ether Creation SARL
 * @license   Commercial license
 * International Registered Trademark & Property of Ether Creation SARL
 */
var tmpDisplayInfoRetour;
function isNumber (n) {
  return !isNaN(parseFloat(n)) && isFinite(n);
}
var eci_ajaxlink;
$(document).ready(function () {
    eci_ajaxlink = $("#eci_ajaxlink").val();
});
function displayInfoRetour (message, etat) {
    var ec_token = $("#ec_token").val();
    var eci_baseDir = $('#eci_baseDir').val();
    clearTimeout(tmpDisplayInfoRetour);
    if (etat === 'message') {
        $.ajax({
//            url: eci_baseDir+"modules/ecicdiscountpro/ajax.php",
//            url: "../index.php?fc=module&module=ecicdiscountpro&controller=ajax",
//            url: "../module/ecicdiscountpro/ajax",
            url: eci_ajaxlink,
            type: "POST",
            async : false, //mode synchrone
            data: {
                majsel: 55,
                ec_token: ec_token,
                etat: etat,
                message: message
            },
            dataType: 'html'
        }).done(function( data ) {
            return data;
        });

    } else {
        $.ajax({
//            url: eci_baseDir+"modules/ecicdiscountpro/ajax.php",
//            url: "../index.php?fc=module&module=ecicdiscountpro&controller=ajax",
//            url: "../module/ecicdiscountpro/ajax",
            url: eci_ajaxlink,
            type: "POST",
            data: {
                majsel: 55,
                ec_token: ec_token,
                etat: etat,
                message: message
            },
            dataType: 'html'
        }).done(function( data ) {
            $('#display_message').addClass('message_over').show().html(data).css("opacity", "1");
            tmpDisplayInfoRetour = setTimeout(function(){
                $('#display_message').animate({opacity: 0}, 500, function(){$(this).hide(function(){$(this).html('', function(){$(this).removeClass('message_over');});});});
            }, 10000);
        });
    }
}
function save_parameter_connecteur () {
    var ec_token = $("#ec_token").val();
    var eci_cdiscountpro_conf = $(".eci_cdiscountpro_conf").serialize();
    var eci_baseDir = $('#eci_baseDir').val();
    var id_shop = $('#id_shop').val();
    $.ajax({
//        url: eci_baseDir+"modules/ecicdiscountpro/ajax.php",
//        url: "../index.php?fc=module&module=ecicdiscountpro&controller=ajax",
//        url: "../module/ecicdiscountpro/ajax",
        url: eci_ajaxlink,
        type: "POST",
        data: ({
            majsel: 45,
            ec_token: ec_token,
            id_shop: id_shop,
            eci_cdiscountpro_conf: eci_cdiscountpro_conf
        }),
        dataType: "json"
    })
    .done(function (data) {
        displayInfoRetour(data.mc, data.suxs);
    })
    .fail(function (data) {
        displayInfoRetour('21','nosuccess');
    });
}
