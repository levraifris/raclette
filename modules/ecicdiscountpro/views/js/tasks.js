/**
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to a commercial license from SARL Ether Creation
 * Use, copy, modification or distribution of this source file without written
 * license agreement from the SARL Ether Creation is strictly forbidden.
 * In order to obtain a license, please contact us: contact@ethercreation.com
 * ...........................................................................
 * INFORMATION SUR LA LICENCE D'UTILISATION
 *
 * L'utilisation de ce fichier source est soumise a une licence commerciale
 * concedee par la societe Ether Creation
 * Toute utilisation, reproduction, modification ou distribution du present
 * fichier source sans contrat de licence ecrit de la part de la SARL Ether Creation est
 * expressement interdite.
 * Pour obtenir une licence, veuillez contacter la SARL Ether Creation a l'adresse: contact@ethercreation.com
 * ...........................................................................
 *
 * @author    Ether Creation SARL <contact@ethercreation.com>
 * @copyright 2008-2021 Ether Creation SARL
 * @license   Commercial license
 * International Registered Trademark & Property of Ether Creation SARL
 */
var eci_baseDir = '';
var tmpDisplayInfoRetour;
var eci_ajaxlink;
$(document).ready(function () {
    eci_ajaxlink = $("#eci_ajaxlink").val();
    $('#eci_jobs_history_form').addClass('hidden');
    $(document).on('click', '.badge', function(){
        $('#eci_jobs_history_form').toggleClass('hidden');
    });
    eci_baseDir = $('#eci_baseDir').val();
    var eciCpanelInterval = 0;
    $(document).on('mouseenter', '.ecicpanel', function () {
        var id_cpanel = $(this).attr('data-id_cpanel');
        var prefix = $(this).attr('data-prefix');
        var suffix = $(this).attr('data-suffix');
        if (eciCpanelInterval === 0 && 'nofollow' !== prefix) {
            eciCpanelInterval = setInterval(function(){updateCpanelDisplay(prefix, suffix, id_cpanel);}, 2000);
        }
    });
    $(document).on('mouseleave', '.ecicpanel', function () {
        if (eciCpanelInterval !== 0) {
            clearInterval(eciCpanelInterval);
            eciCpanelInterval = 0;
        }
    });
    $('.nofollow').attr('onclick', '');
    $(document).on('click', '.eci_task_progress', function () {
        var id_cpanel = $(this).closest('.ecicpanel').attr('data-id_cpanel');
        var link = $('#eci_task_link_' + id_cpanel).attr('data-link');
        var val = $(this).text();
//       	console.log(link + '&nbC=' + val);
        if (val && confirm('Force continue ?')){
            $.ajax({
                url: link + '&nbC=' + val,
                type: "GET"
            });
        }
    });
});
function updateCpanelDisplay(prefix, suffix, id_cpanel) {
    var ec_token = $('#ec_token').val();
    $.ajax({
//        url: eci_baseDir+"modules/ecicdiscountpro/ajax.php",
//        url: "../index.php?fc=module&module=ecicdiscountpro&controller=ajax",
//        url: "../module/ecicdiscountpro/ajax",
        url: eci_ajaxlink,
        type: "POST",
        data: ({
            majsel: 75,
            ec_token: ec_token,
            prefix: prefix,
            suffix: suffix
        }),
        dataType: "html"
    })
    .done(function (data) {
        $('#ecivpanel'+id_cpanel).html(data);
    });
}
function displayInfoRetour(message, etat) {
    var ec_token = $("#ec_token").val();
    clearTimeout(tmpDisplayInfoRetour);

    if (etat === 'message') {
        $.ajax({
//            url: eci_baseDir+"modules/ecicdiscountpro/ajax.php",
//            url: "../index.php?fc=module&module=ecicdiscountpro&controller=ajax",
//            url: "../module/ecicdiscountpro/ajax",
            url: eci_ajaxlink,
            type: "POST",
            async : false, //mode synchrone
            data: {
                majsel: 55,
                ec_token: ec_token,
                etat: etat,
                message: message
            },
            dataType: 'html'
        }).done(function( data ) {
            return data;
        });

    } else {
        $.ajax({
//            url: eci_baseDir+"modules/ecicdiscountpro/ajax.php",
//            url: "../index.php?fc=module&module=ecicdiscountpro&controller=ajax",
//            url: "../module/ecicdiscountpro/ajax",
            url: eci_ajaxlink,
            type: "POST",
            data: {
                majsel: 55,
                ec_token: ec_token,
                etat: etat,
                message: message
            },
            dataType: 'html'
        }).done(function( data ) {
            $('#display_message').addClass('message_over').show().html(data).css("opacity", "1");
            tmpDisplayInfoRetour = setTimeout(function(){
                $('#display_message').animate({opacity: 0}, 500, function(){$(this).hide(function(){$(this).html('', function(){$(this).removeClass('message_over');});});});
            }, 10000);
        });
    }
}
