<?php
require_once dirname(__FILE__) . '/../../../config/config.inc.php';

if (isset($_GET['test2'])) {
    exit($_SERVER['REMOTE_ADDR']);
}


$url = 'http://mon-ip.com';

$header = array();
$header[0] = "Accept: text/xml,application/xml,application/xhtml+xml,";
$header[0] .= "text/html;q=0.9,text/plain;q=0.8,image/png,image/jpeg,*/*;q=0.5";
$header[] = "Cache-Control: max-age=0";
$header[] = "Connection: keep-alive";
$header[] = "Keep-Alive: 300";
$header[] = "Accept-Charset: utf-8,ISO-8859-1;q=0.8,*;q=0.7";
$header[] = "Accept-Language: fr,en-us,en;q=0.5";
$header[] = "Pragma: ";

$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:42.0) Gecko/20100101 Firefox/42.0');
curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
curl_setopt($ch, CURLOPT_REFERER, 'http://www.google.com');
curl_setopt($ch, CURLOPT_AUTOREFERER, true);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($ch, CURLOPT_ENCODING, 'gzip,deflate');
curl_setopt($ch, CURLOPT_COOKIEJAR, dirname(__FILE__).'/cookie.txt');
curl_setopt($ch, CURLOPT_COOKIEFILE, dirname(__FILE__).'/cookie.txt');
curl_setopt($ch, CURLOPT_TIMEOUT, 10);
curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
curl_setopt($ch, CURLINFO_HEADER_OUT, true);
$result = curl_exec($ch);
$err = curl_error($ch);
$errno = curl_errno($ch);
$infos = curl_getinfo($ch);

echo 'errno : ' .$errno;
echo "<br>\n";
echo 'error : ' . $err;
echo "<br>\n";
var_export($infos);
echo "<br>\n";

curl_close($ch);

$lst_IP = array();

$ip_monIP = preg_replace('/.*?([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}).*/s', '$1', $result);
echo 'IP mon-ip.com : ' . $ip_monIP . "<br>\n";
$lst_IP[] = $ip_monIP;

echo 'IP de serveur (php) : ' . $_SERVER['SERVER_ADDR'] . "<br>\n";
$lst_IP[] = $_SERVER['SERVER_ADDR'];

$base_uri = (((Configuration::get('PS_SSL_ENABLED') == 1) && (Configuration::get('PS_SSL_ENABLED_EVERYWHERE') == 1)) ? 'https://' : 'http://' )
    . Tools::getShopDomain()
    . __PS_BASE_URI__
    . str_replace(_PS_ROOT_DIR_ . '/', '', __FILE__);
$script = $base_uri . '?test2=1';
$ip_cachee = file_get_contents($script);
echo 'IP utilisée pour me joindre : ' .$ip_cachee . "<br>\n";
$lst_IP[] = $ip_cachee;

echo "<br>\n" . 'Liste des IP à ajouter : '.implode(',', array_unique($lst_IP)) . "<br>\n";
