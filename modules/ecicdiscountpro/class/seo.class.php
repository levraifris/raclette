<?php
/**
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to a commercial license from SARL Ether Creation
 * Use, copy, modification or distribution of this source file without written
 * license agreement from the SARL Ether Creation is strictly forbidden.
 * In order to obtain a license, please contact us: contact@ethercreation.com
 * ...........................................................................
 * INFORMATION SUR LA LICENCE D'UTILISATION
 *
 * L'utilisation de ce fichier source est soumise a une licence commerciale
 * concedee par la societe Ether Creation
 * Toute utilisation, reproduction, modification ou distribution du present
 * fichier source sans contrat de licence ecrit de la part de la SARL Ether Creation est
 * expressement interdite.
 * Pour obtenir une licence, veuillez contacter la SARL Ether Creation a l'adresse: contact@ethercreation.com
 * ...........................................................................
 *
 * @author    Ether Creation SARL <contact@ethercreation.com>
 * @copyright 2008-2021 Ether Creation SARL
 * @license   Commercial license
 * International Registered Trademark & Property of Ether Creation SARL
 */

namespace ecicdiscountpro;

require_once dirname(__FILE__) . '/ecicustomobjectmodel.php';
use Db;
use Shop;
use Language;

class Seo extends EciCustomObjectModel
{
    public $tabName = 'Seo';
    public $controllerExt = 'Seo';
    public static $definition = array(
        'table' => 'eci_seo',
        'primary' => 'id_seo',
        'multilang' => true,
        'multilang_shop' => true,
        'fields' => array(
            'seo_name' => array('type' => self::TYPE_STRING, 'db_type' => 'varchar(255)', 'lang' => true, 'validate' => 'isString', 'size' => 255, 'required' => true),
            'seo_description' => array('type' => self::TYPE_STRING, 'db_type' => 'varchar(255)', 'lang' => true, 'validate' => 'isString', 'size' => 255, 'required' => true),
            'seo_description_short' => array('type' => self::TYPE_STRING, 'db_type' => 'varchar(255)', 'lang' => true, 'validate' => 'isString', 'size' => 255, 'required' => true),
            'seo_meta_title' => array('type' => self::TYPE_STRING, 'db_type' => 'varchar(255)', 'lang' => true, 'validate' => 'isString', 'size' => 255, 'required' => true),
            'seo_meta_description' => array('type' => self::TYPE_STRING, 'db_type' => 'varchar(255)', 'lang' => true, 'validate' => 'isString', 'size' => 255, 'required' => true),
            'fournisseur' => array('type' => self::TYPE_STRING, 'db_type' => 'varchar(50)', 'validate' => 'isGenericName', 'size' => 50, 'required' => true),
            'active' => array('type' => self::TYPE_BOOL, 'shop' => true, 'db_type' => 'int', 'validate' => 'isBool', 'required' => true),
        ),
    );
    public $seo_name;
    public $seo_description;
    public $seo_description_short;
    public $seo_meta_title;
    public $seo_meta_description;
    public $active;
    public $fournisseur;

    public function install()
    {
        $reqs = array(
            _DB_PREFIX_ . 'eci_seo' => array(
                'fournisseur' => '`fournisseur`',
                'active' => '`active`'
            ),
            _DB_PREFIX_ . 'eci_seo_lang' => array(
                'id_seo' => '`id_seo`',
                'id_lang' => '`id_lang`',
                'id_shop' => '`id_shop`',
            ),
            _DB_PREFIX_ . 'eci_seo_shop' => array(
                'id_seo' => '`id_seo`',
                'id_shop' => '`id_shop`',
                'active' => '`active`',
            )
        );
        $this->addIndexesIfNotExist($reqs);
    }

    public function __construct($id = null, $id_lang = null, $id_shop = null)
    {
        Shop::addTableAssociation(self::$definition['table'], array('type' => 'shop'));
        Shop::addTableAssociation(self::$definition['table'] . '_lang', array('type' => 'fk_shop'));

        parent::__construct($id, $id_lang, $id_shop);
    }

    public static function getValuesStatic($fournisseur, $id_shop)
    {
        $id_seo_active = Db::getInstance()->getValue(
            'SELECT a.id_seo
            FROM ' . _DB_PREFIX_ . 'eci_seo a
            JOIN ' . _DB_PREFIX_ . 'eci_seo_shop sa
            ON (a.id_seo = sa.id_seo AND sa.id_shop = ' . (int) $id_shop . ')
            WHERE a.fournisseur = "' . pSQL($fournisseur) . '"
            AND sa.active = 1'
        );

        if (!$id_seo_active) {
            return self::getDefaultRules();
        }

        $allValues = Db::getInstance()->executeS(
            'SELECT *
            FROM ' . _DB_PREFIX_ . 'eci_seo_lang
            WHERE id_seo = ' . (int) $id_seo_active . '
            AND id_shop = ' . (int) $id_shop
        );

        $values = array();
        foreach ($allValues as $value) {
            $values['seo_name'][$value['id_lang']] = $value['seo_name'];
            $values['seo_description'][$value['id_lang']] = $value['seo_description'];
            $values['seo_description_short'][$value['id_lang']] = $value['seo_description_short'];
            $values['seo_meta_title'][$value['id_lang']] = $value['seo_meta_title'];
            $values['seo_meta_description'][$value['id_lang']] = $value['seo_meta_description'];
        }

        return $values;
    }

    public static function getDefaultRules()
    {
        $values = array();
        foreach (Language::getIDs(false) as $id_lang) {
            $values['seo_name'][$id_lang] = '{name}';
            $values['seo_description'][$id_lang] = '{description}';
            $values['seo_description_short'][$id_lang] = '{description_short}';
            $values['seo_meta_title'][$id_lang] = '{meta_title}';
            $values['seo_meta_description'][$id_lang] = '{meta_description}';
        }

        return $values;
    }
}
