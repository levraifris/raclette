<?php
/**
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to a commercial license from SARL Ether Creation
 * Use, copy, modification or distribution of this source file without written
 * license agreement from the SARL Ether Creation is strictly forbidden.
 * In order to obtain a license, please contact us: contact@ethercreation.com
 * ...........................................................................
 * INFORMATION SUR LA LICENCE D'UTILISATION
 *
 * L'utilisation de ce fichier source est soumise a une licence commerciale
 * concedee par la societe Ether Creation
 * Toute utilisation, reproduction, modification ou distribution du present
 * fichier source sans contrat de licence ecrit de la part de la SARL Ether Creation est
 * expressement interdite.
 * Pour obtenir une licence, veuillez contacter la SARL Ether Creation a l'adresse: contact@ethercreation.com
 * ...........................................................................
 *
 * @author    Ether Creation SARL <contact@ethercreation.com>
 * @copyright 2008-2021 Ether Creation SARL
 * @license   Commercial license
 * International Registered Trademark & Property of Ether Creation SARL
 */

namespace ecicdiscountpro;

require_once dirname(__FILE__) . '/ecicustomobjectmodel.php';

use Db;
use Validate;

class ApiAuth extends EciCustomObjectModel
{

    public $tabName = 'ApiAuthorization';
    public $controllerExt = 'Apiauth';
    public static $definition = array(
        'table' => 'eci_apiauth',
        'primary' => 'id_apiauth',
        'fields' => array(
            'user' => array('type' => self::TYPE_STRING, 'db_type' => 'varchar(64)', 'validate' => 'isString', 'size' => 64, 'required' => true),
            'authtoken' => array('type' => self::TYPE_STRING, 'db_type' => 'varchar(64)', 'validate' => 'isString', 'size' => 64, 'required' => true),
            'authorizations' => array('type' => self::TYPE_STRING, 'db_type' => 'text', 'validate' => 'isAnything', 'required' => true),
            'fournisseur' => array('type' => self::TYPE_STRING, 'db_type' => 'varchar(50)', 'validate' => 'isGenericName', 'size' => 50, 'required' => true),
            'active' => array('type' => self::TYPE_BOOL, 'db_type' => 'int', 'validate' => 'isBool', 'required' => true)
        )
    );
    public $user;
    public $authtoken;
    public $authorizations;
    public $fournisseur;
    public $active;

    public function install()
    {
        $reqs = array(
            _DB_PREFIX_ . 'eci_apiauth' => array(
                'user' => '`user`',
                'authtoken' => '`authtoken`',
                'fournisseur' => '`fournisseur`',
                'active' => '`active`',
            ),
        );
        $this->addIndexesIfNotExist($reqs);
    }

    public function __construct($id = null, $id_lang = null, $id_shop = null)
    {
        parent::__construct($id, $id_lang, $id_shop);

        if (!empty($this->authorizations) && Validate::isJson($this->authorizations)) {
            $this->authorizations = json_decode($this->authorizations, true);
        }
    }

    public function add($auto_date = true, $null_values = false)
    {
        if (is_array($this->authorizations)) {
            $this->authorizations = json_encode($this->authorizations);
        }

        return parent::add($auto_date, $null_values);
    }

    public function update($null_values = false)
    {
        if (is_array($this->authorizations)) {
            $this->authorizations = json_encode($this->authorizations);
        }

        return parent::update($null_values);
    }

    public function save($null_values = false, $auto_date = true)
    {
        if (is_array($this->authorizations)) {
            $this->authorizations = json_encode($this->authorizations);
        }

        return (int) $this->id > 0 ? $this->update($null_values) : $this->add($auto_date, $null_values);
    }

    public static function getAccess($user, $authtoken)
    {
        return (bool) Db::getInstance()->getValue(
            'SELECT COUNT(*)
            FROM ' . _DB_PREFIX_ . 'eci_apiauth
            WHERE user = "' . pSQL($user) . '"
            AND authtoken = "' . pSQL($authtoken) . '"'
        );
    }

    public static function getAuthorizations($user, $authtoken)
    {
        return json_decode(
            Db::getInstance()->getValue(
                'SELECT authorizations
                FROM ' . _DB_PREFIX_ . 'eci_apiauth
                WHERE user = "' . pSQL($user) . '"
                AND authtoken = "' . pSQL($authtoken) . '"
                AND active = 1'
            ),
            true
        );
    }
}
