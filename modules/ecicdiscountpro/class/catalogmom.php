<?php
/**
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to a commercial license from SARL Ether Creation
 * Use, copy, modification or distribution of this source file without written
 * license agreement from the SARL Ether Creation is strictly forbidden.
 * In order to obtain a license, please contact us: contact@ethercreation.com
 * ...........................................................................
 * INFORMATION SUR LA LICENCE D'UTILISATION
 *
 * L'utilisation de ce fichier source est soumise a une licence commerciale
 * concedee par la societe Ether Creation
 * Toute utilisation, reproduction, modification ou distribution du present
 * fichier source sans contrat de licence ecrit de la part de la SARL Ether Creation est
 * expressement interdite.
 * Pour obtenir une licence, veuillez contacter la SARL Ether Creation a l'adresse: contact@ethercreation.com
 * ...........................................................................
 *
 * @author    Ether Creation SARL <contact@ethercreation.com>
 * @copyright 2008-2021 Ether Creation SARL
 * @license   Commercial license
 * International Registered Trademark & Property of Ether Creation SARL
 */

namespace ecicdiscountpro;

require_once dirname(__FILE__) . '/../' . __NAMESPACE__ . '.php';
require_once dirname(__FILE__) . '/info.class.php';
require_once dirname(__FILE__) . '/reference.class.php';
require_once dirname(__FILE__) . '/eciseo.class.php';
require_once dirname(__FILE__) . '/ecitranslations.class.php';
require_once dirname(__FILE__) . '/ecisuppcarrier.class.php';
require_once dirname(__FILE__) . '/ecipricerules.class.php';
use Address;
use Attachment;
use Attribute;
use AttributeGroup;
use Cache;
use Category;
use Carrier;
use Combination;
use Configuration;
use Context;
use Country;
use Currency;
use Customer;
use CustomerMessage;
use CustomerThread;
use CustomizationField;
use Db;
use DbQuery;
use Delivery;
use DOMDocument;
use Employee;
use Exception;
use Feature;
use FeatureValue;
use Group;
use Hook;
use Image;
use ImageManager;
use ImageType;
use Language;
use Link;
use Manufacturer;
use Message;
use Order;
use OrderCarrier;
use Product;
use RangePrice;
use RangeWeight;
use ReflectionClass;
use Search;
use Shop;
use ShopGroup;
use SimplexmlElement;
use StockAvailable;
use Store;
use Supplier;
use Tag;
use Tax;
use TaxRulesGroup;
use Tools;
use Validate;
use WarehouseProductLocation;
use Zone;

if (!defined('_PS_VERSION_')) {
    exit();
}

class CatalogMom
{
    /**
     * paths
     */
    const FILES_PATH = _PS_MODULE_DIR_ . __NAMESPACE__ . '/files/';
    const IMPORT_PATH = _PS_MODULE_DIR_ . __NAMESPACE__ . '/files/import/';
    const INVOICE_PATH = _PS_MODULE_DIR_ . __NAMESPACE__ . '/files/import/factures/';
    const EXPORT_PATH = _PS_MODULE_DIR_ . __NAMESPACE__ . '/files/export/';
    const CLASS_PATH = _PS_MODULE_DIR_ . __NAMESPACE__ . '/class/';
    const MAILS_PATH = _PS_MODULE_DIR_ . __NAMESPACE__ . '/mails/';

    /**
     * Logging or not
     */
    const ALLOW_LOG = true;

    /**
     * Logging of product creation time (hook excepted)
     */
    const LOG_PRODUCT_CREATION_TIME = false;

    /**
     * followLink default parameters
     */
    const FOLLOWLINK_TIMEOUT = 4;
    const FOLLOWLINK_LOG = false;
    const FOLLOWLINK_RETRIES = false;

    /**
     * cache persistance in seconds
     */
    const CACHE_PERSISTANCE = 300;

    /**
     * max work time for autocall phases
     */
    public $limitMax = 5;
    const JOB_TIME_LIMIT = 5;

    /**
     * max number of lines for multiple inserts
     */
    public $limitRowMax = 1000;
    const SQL_RAWS_LIMIT = 1000;

    /**
     * if true all category paths will be set to populated=1
     * but catalog category filtering has to be enhanced
     */
    const ALL_PATHS_ARE_POPULATED = true;

    /**
     * force send order to gen only
     * usefull in ERP cases
     */
    const SEND_ORDER_TO_GEN_ONLY = false;

    /**
     * table of fields in the eci_catalog table
     */
    public $tabInsert = array();

    /**
     * table of fields in the eci_catalog_attribute table
     */
    public $tabInsert_attribute = array();

    public $tabAttributes;
    public $tabAttributes_value;
    public $tabFeatures;
    public $tabFeatures_value;
    public $tabTVA;

    /**
     * tables of supplier generic parameters
     */
    public static $tabParamsValid = array( //valid
        'ACTIVE',
        'ACTION_PRODUCT_DEREF',
        'UPDATE_PRODUCT_DEREF',
        'PARAM_NEWPRODUCT',
        'ONLY_LAST_CATEGORY',
        'FORCE_PACK_COMP',
        'ASSOC_CARRIERS',
        'STOCK_ENABLE',
        'REACTIVATE',
        'PRICEWSTOCK',
        'PMVCWSTOCK',
        'DAILY_PRICE_UPDATE',
        'DAILY_PRICE_UPDATE_TIME',
        'IMPORT_AUTO',
        'IMPORT_OST',
        'FINAL_OST',
        'IMPORT_ADDRESS',
        'STORE_ADDRESS',
        'MARGE',
        'NOTVA',
        'SHOW_MANUFACTURER',
        'SHOW_REFERENCE',
    );
    public static $tabParamsNumeric = array( //numeric
        'MARGE',
        'min_severity',
    );
    public static $tabParamsMultiple = array( //multiple
    );
    public static $tabParamsArray = array( //array
        'IMPORT_OST',
        'FINAL_OST',
    );
    public static $tabParamsAllShops = array( //allShop
    );
    public static $tabParamsFiles = array( //files
    );
    public static $tabParamsLang = array( //lang fields
    );

    public $id_lang;
    public $id_shop;
    public $id_employee;

    public function __construct(Context $context = null)
    {
        $this->tabAttributes = array();
        $this->tabAttributes_value = array();
        $this->tabFeatures = array();
        $this->tabFeatures_value = array();
        $this->tabTVA = array();

        $this->id_shop = $context->shop->id ?? Context::getContext()->shop->id ?? Configuration::get('PS_SHOP_DEFAULT');
        $this->id_lang = $context->language->id ?? Context::getContext()->language->id ?? Configuration::get('PS_LANG_DEFAULT');
        $this->id_employee = $context->employee->id ?? Context::getContext()->employee->id ?? (self::getEciEmployeeId() ?: 1);
        Shop::setContext(Shop::CONTEXT_SHOP, (int) $this->id_shop);
        Context::getContext()->language = new Language((int) $this->id_lang);
        Context::getContext()->shop = new Shop((int) $this->id_shop, (int) $this->id_lang, (int) $this->id_shop);
        Context::getContext()->employee = new Employee($this->id_employee, $this->id_lang);

        $this->tabInsert = self::getCatFields();
        $this->tabInsert_attribute = self::getCatAttFields();
    }

    public static function getCatFields()
    {
        $key = __NAMESPACE__ . '_' . __FUNCTION__;
        if (!self::cacheIsStored($key)) {
            self::cacheStore(
                $key,
                array_flip(array_map('reset', Db::getInstance()->executeS('DESC '._DB_PREFIX_.'eci_catalog')))
            );
        }

        return self::cacheRetrieve($key);
    }

    public static function getCatAttFields()
    {
        $key = __NAMESPACE__ . '_' . __FUNCTION__;
        if (!self::cacheIsStored($key)) {
            self::cacheStore(
                $key,
                array_flip(array_map('reset', Db::getInstance()->executeS('DESC '._DB_PREFIX_.'eci_catalog_attribute')))
            );
        }

        return self::cacheRetrieve($key);
    }

    public static function getPackFields()
    {
        $key = __NAMESPACE__ . '_' . __FUNCTION__;
        if (!self::cacheIsStored($key)) {
            self::cacheStore(
                $key,
                array_flip(array_map('reset', Db::getInstance()->executeS('DESC '._DB_PREFIX_.'eci_catalog_pack')))
            );
        }

        return self::cacheRetrieve($key);
    }

    public function matchTax($forced_id_shop = null)
    {
        $id_shop = $forced_id_shop ?? $this->id_shop ?? Configuration::get('PS_SHOP_DEFAULT');
        $connecteur = self::getConnecteurName();

        foreach ($this->tabTVA as $tax) {
            $id_tax_eco = Db::getInstance()->getValue(
                'SELECT id_tax_eco
                FROM ' . _DB_PREFIX_ . 'eci_tax
                WHERE rate = ' . (float) $tax . '
                AND fournisseur = "' . pSQL($connecteur) .'"'
            );

            if (!$id_tax_eco) {
                Db::getInstance()->insert(
                    'eci_tax',
                    array(
                        'rate' => (float) $tax,
                        'fournisseur' => pSQL($connecteur)
                    )
                );
                $id_tax_eco = Db::getInstance()->Insert_ID();
            }

            $idTaxMatched = Db::getInstance()->getValue(
                'SELECT id_tax_rules_group
                FROM ' . _DB_PREFIX_ . 'eci_tax_shop
                WHERE id_tax_eco = ' . (int) $id_tax_eco . '
                AND id_shop = ' . (int) $id_shop
            );

            if (!$idTaxMatched) {
                $idTaxR = Db::getInstance()->getValue(
                    'SELECT tr.id_tax_rules_group
                    FROM ' . _DB_PREFIX_ . 'tax_rule tr
                    LEFT JOIN ' . _DB_PREFIX_ . 'tax t on t.id_tax = tr.id_tax
                    LEFT JOIN ' . _DB_PREFIX_ . 'tax_rules_group tg on tg.id_tax_rules_group = tr.id_tax_rules_group
                    LEFT JOIN ' . _DB_PREFIX_ . 'tax_rules_group_shop ts on ts.id_tax_rules_group = tr.id_tax_rules_group
                    WHERE t.rate = ' . (float) round($tax, 3) . '
                    AND tr.id_country = ' . (int) Configuration::get('PS_COUNTRY_DEFAULT') . '
                    AND ts.id_shop = ' . (int) $id_shop . '
                    AND t.deleted = 0
                    AND tg.deleted = 0'
                );

                $id_tax_rules_group = empty($idTaxR) ? 0 : $idTaxR;
                Db::getInstance()->insert(
                    'eci_tax_shop',
                    array(
                        'id_tax_eco' => (int) $id_tax_eco,
                        'id_tax_rules_group' => (int) $id_tax_rules_group,
                        'id_shop' => (int) $id_shop
                    ),
                    false,
                    false,
                    Db::INSERT_IGNORE
                );
            }
        }
    }

    public function getTaxMatch($connecteur = null, $forced_id_shop = null)
    {
        if (is_null($connecteur)) {
            $connecteur = self::getConnecteurName();
        }
        $id_shop = $forced_id_shop ?? $this->id_shop ?? Configuration::get('PS_SHOP_DEFAULT');

        $rates = Db::getInstance()->executeS(
            'SELECT DISTINCT(rate)
            FROM ' . _DB_PREFIX_ . 'eci_tax
            WHERE fournisseur = "' . pSQL($connecteur) . '"
            ORDER by rate'
        );
        if (!$rates) {
            return false;
        }

        $eci_taxes_match = Db::getInstance()->executeS(
            'SELECT t.*, s.id_tax_rules_group, s.id_shop
            FROM ' . _DB_PREFIX_ . 'eci_tax t
            LEFT JOIN ' . _DB_PREFIX_ . 'eci_tax_shop s
            ON t.id_tax_eco = s.id_tax_eco
            WHERE fournisseur = "' . pSQL($connecteur) . '"
            AND id_shop = ' . (int) $id_shop . '
            ORDER by rate'
        );
        if (count($eci_taxes_match) == count($rates)) {
            return $eci_taxes_match;
        }

        foreach ($rates as $rate) {
            $this->tabTVA = $rate;
            $this->matchTax($id_shop);
        }

        return $this->getTaxMatch($connecteur, $forced_id_shop);
    }

    public function getTaxMatched($rate)
    {
        $eci_taxes_matched = $this->getTaxMatch();

        if (!$eci_taxes_matched) {
            return false;
        }

        foreach ($eci_taxes_matched as $eci_tax_matched) {
            if ($rate == $eci_tax_matched['rate']) {
                return $eci_tax_matched['id_tax_rules_group'];
            }
        }

        return false;
    }

    public function saveTaxMatching($id_tax_eco, $id_tax, $connecteur = null, $forced_id_shop = null)
    {
        if (is_null($connecteur)) {
            $connecteur = self::getConnecteurName();
        }

        $id_shop = $forced_id_shop ?? $this->id_shop ?? Configuration::get('PS_SHOP_DEFAULT');

        return Db::getInstance()->insert(
            'eci_tax_shop',
            array(
                'id_tax_eco' => (int) $id_tax_eco,
                'id_tax_rules_group' => (int) $id_tax,
                'id_shop' => (int) $id_shop
            ),
            false,
            false,
            Db::ON_DUPLICATE_KEY
        );
    }

    public static function matchManufacturers()
    {
        $connecteur = self::getConnecteurName();
        
        //add new manufacturers from the catalog to the matching table
        Db::getInstance()->execute(
            'INSERT IGNORE INTO ' . _DB_PREFIX_ . 'eci_manufacturer
            (name, fournisseur)
            SELECT DISTINCT manufacturer, fournisseur
            FROM ' . _DB_PREFIX_ . 'eci_catalog
            WHERE fournisseur = "' . pSQL($connecteur) . '"'
        );
        
        //try to match recognized manufacturers
        $tab_manufacturers = Db::getInstance()->executeS(
            'SELECT id_eci_manufacturer, name
            FROM ' . _DB_PREFIX_ . 'eci_manufacturer
            WHERE fournisseur = "' . pSQL($connecteur) . '"
            AND id_manufacturer = 0'
        );
        if (!$tab_manufacturers) {
            return true;
        }
        foreach ($tab_manufacturers as $manufacturer) {
            $id_manufacturer = Manufacturer::getIdByName($manufacturer['name']);
            if ($id_manufacturer) {
                Db::getInstance()->update(
                    'eci_manufacturer',
                    [
                        'id_manufacturer' => (int) $id_manufacturer,
                    ],
                    'id_eci_manufacturer = ' . (int) $manufacturer['id_eci_manufacturer']
                );
            }
        }
        
        return true;
    }

    public static function getManufacturersMatch($connecteur = null)
    {
        if (is_null($connecteur)) {
            $connecteur = self::getConnecteurName();
        }

        $manufacturers_cat = Db::getInstance()->getValue(
            'SELECT COUNT(DISTINCT manufacturer)
            FROM ' . _DB_PREFIX_ . 'eci_catalog
            WHERE fournisseur = "' . pSQL($connecteur) . '"'
        );

        $eci_manufacturers_match = Db::getInstance()->executeS(
            'SELECT *
            FROM ' . _DB_PREFIX_ . 'eci_manufacturer
            WHERE fournisseur = "' . pSQL($connecteur) . '"
            ORDER by name'
        );
        if (count($eci_manufacturers_match) >= $manufacturers_cat) {
            return $eci_manufacturers_match;
        }
        
        self::matchManufacturers();

        return self::getManufacturersMatch($connecteur);
    }

    
    public static function getManufacturerMatched($name)
    {
        $eci_manufacturers_matched = self::getManufacturersMatch();

        if (!$eci_manufacturers_matched) {
            return false;
        }

        foreach ($eci_manufacturers_matched as $eci_manufacturer_matched) {
            if (Tools::strtolower($name) == Tools::strtolower($eci_manufacturer_matched['name'])) {
                return $eci_manufacturer_matched['id_manufacturer'];
            }
        }

        return false;
    }

    public static function saveManufacturerMatching($id_eci_manufacturer, $id_manufacturer, $connecteur = null)
    {
        if (is_null($connecteur)) {
            $connecteur = self::getConnecteurName();
        }

        return Db::getInstance()->update(
            'eci_manufacturer',
            [
                'id_manufacturer' => (int) $id_manufacturer,
            ],
            'id_eci_manufacturer = ' . (int) $id_eci_manufacturer
        );
    }

    public function deleteData($fourn, $continue = false, $timeLimit = false)
    {
        $stopTime = time() + 5;
        $parm_mult = self::getECIConfigValue('limitrow', 0, $fourn);
        $mult = $parm_mult ? (int) $parm_mult : 8;
        $db = Db::getInstance();
        $do_not_delete_old = (bool) self::jGet('ECI_CC_DNDO_' . $fourn);

        if (!$continue && !$do_not_delete_old) {
            $db->delete('eci_catalog_old', 'fournisseur = "' . pSQL($fourn) . '"');
            $db->delete('eci_catalog_attribute_old', 'fournisseur = "' . pSQL($fourn) . '"');
        }

        while (true) {
            if ($timeLimit && (time() > $stopTime)) {
                return false;
            }
            try {
                $maxref = $db->getValue(
                    'SELECT MAX(cl.product_reference)
                    FROM (
                        SELECT product_reference FROM ' . _DB_PREFIX_ . 'eci_catalog
                        WHERE fournisseur = "' . pSQL($fourn) . '"
                        ORDER BY product_reference
                        LIMIT 0,' . (int) ($mult * $this->limitRowMax) . ') cl'
                );
                if (!$maxref) {
                    break;
                }
                if (!$do_not_delete_old) {
                    $db->execute(
                        'INSERT INTO ' . _DB_PREFIX_ . 'eci_catalog_old
                        SELECT * FROM ' . _DB_PREFIX_ . 'eci_catalog
                        WHERE fournisseur = "' . pSQL($fourn) . '"
                        AND product_reference <= "' . pSQL($maxref) . '"'
                    );
                }
            } catch (Exception $e) {
                $mult = ceil($mult / 2);
                self::setECIConfigValue('limitrow', $mult, 0, $fourn);
            }
            $db->delete(
                'eci_catalog',
                'fournisseur = "' . pSQL($fourn) . '" AND product_reference <= "' . pSQL($maxref) . '"'
            );
        }

        while (true) {
            if ($timeLimit && (time() > $stopTime)) {
                return false;
            }
            $maxref = $db->getValue(
                'SELECT MAX(cl.reference_attribute)
                FROM (
                    SELECT reference_attribute FROM ' . _DB_PREFIX_ . 'eci_catalog_attribute
                    WHERE fournisseur = "' . pSQL($fourn) . '"
                    ORDER BY reference_attribute
                    LIMIT 0,' . (int) ($mult * $this->limitRowMax) . ') cl'
            );
            if (!$maxref) {
                break;
            }
            if (!$do_not_delete_old) {
                $db->execute(
                    'INSERT INTO ' . _DB_PREFIX_ . 'eci_catalog_attribute_old
                    SELECT * FROM ' . _DB_PREFIX_ . 'eci_catalog_attribute
                    WHERE fournisseur = "' . pSQL($fourn) . '"
                    AND reference_attribute <= "' . pSQL($maxref) . '"'
                );
            }
            $db->delete(
                'eci_catalog_attribute',
                'fournisseur = "' . pSQL($fourn) . '" AND reference_attribute <= "' . pSQL($maxref) . '"'
            );
        }

        return true;
    }

    public static function setCatalogRecurrentValues($connecteur)
    {
        return Db::getInstance()->execute(
            'UPDATE ' . _DB_PREFIX_ . 'eci_catalog new
            JOIN ' . _DB_PREFIX_ . 'eci_catalog_old old
            ON new.product_reference = old.product_reference
            AND new.fournisseur = old.fournisseur
            SET new.blacklist = old.blacklist
            WHERE new.fournisseur = "' . pSQL($connecteur) .'"'
        );
    }

    public static function getNestedCategories($id_shop = 1, $id_lang = 0)
    {
        if (empty($id_lang)) {
            $id_lang = Configuration::get('PS_LANG_DEFAULT');
        }
        $result = Db::getInstance()->executeS(
            'SELECT  cs.id_category,
                    cl.name,
                    c.level_depth,
                    c.id_parent,
                    cs.position,
                    c.is_root_category
            FROM ' . _DB_PREFIX_ . 'category_shop cs
            LEFT JOIN ' . _DB_PREFIX_ . 'category c
                ON c.id_category=cs.id_category
            LEFT JOIN ' . _DB_PREFIX_ . 'category_lang cl
                ON cl.id_category=cs.id_category
                AND cl.id_shop=cs.id_shop
                AND cl.id_lang=' . (int) $id_lang . '
            WHERE cs.id_shop=' . (int) $id_shop . '
            ORDER BY id_parent,position'
        );
        $list = array();
        $max_depth = 0;
        foreach ($result as $item) {
            $list[$item['id_category']] = $item;
            if ($item['is_root_category']) {
                $id_root_cat = $item['id_category'];
                $depth_root_cat = $item['level_depth'];
            }
            $max_depth = max($max_depth, $item['level_depth']);
        }
        for ($depth = $max_depth; $depth > $depth_root_cat; $depth--) {
            foreach ($list as $id_category => $cat) {
                if (isset($cat['level_depth']) && $cat['level_depth'] == $depth) {
                    $list[$cat['id_parent']]['children'][$id_category] = $cat;
                }
            }
        }

        return array($id_root_cat => $list[$id_root_cat]);
    }

    public static function catNestToList($tree, &$liste)
    {
        foreach ($tree as $branch) {
            $liste[] = array(
                'id_category' => $branch['id_category'],
                'name' => $branch['name'],
                'level_depth' => $branch['level_depth']
            );
            if (!empty($branch['children'])) {
                self::catNestToList($branch['children'], $liste);
            }
        }
    }

    public static function cleanCategoryShop()
    {
        return Db::getInstance()->execute(
            'UPDATE ' . _DB_PREFIX_ . 'eci_category_shop cs
            LEFT JOIN ' . _DB_PREFIX_ . 'category c
            ON cs.id_category = c.id_category
            SET cs.id_category = 0
            WHERE c.id_category IS NULL'
        );
    }

    public static function updateInfoEco($name, $value)
    {
        Cache::store(__NAMESPACE__ . '_getInfoEco_' . $name, $value);

        return Db::getInstance()->insert(
            'eci_info',
            array(
                'name' => pSQL($name),
                'value' => pSQL($value)
            ),
            false,
            false,
            Db::REPLACE
        );
    }

    public static function getInfoEco($name)
    {
        $key = __NAMESPACE__ . '_' . __FUNCTION__ . '_' . $name;
        if (!Cache::isStored($key)) {
            if ($name == 'ID_LANG') {
                $val = Configuration::get('PS_LANG_DEFAULT');
            } else {
                $val = Db::getInstance()->getValue(
                    'SELECT value
                    FROM ' . _DB_PREFIX_ . 'eci_info
                    WHERE name = "' . pSQL($name) . '"'
                );
            }
            Cache::store($key, $val);
        }

        return Cache::retrieve($key);
    }

    public static function getEciEmployeeId()
    {
        $key = __NAMESPACE__ . '_' . __FUNCTION__;
        if (!Cache::isStored($key)) {
            $id_employee = self::getInfoEco('ID_EMPLOYEE');
            if (!$id_employee) {
                $employees = Employee::getEmployeesByProfile(1, true);
                $first_super_admin = reset($employees);
                $id_employee = $first_super_admin['id_employee'];
            }
            Cache::store($key, $id_employee);
        }

        return Cache::retrieve($key);
    }

    public function checkFolder($fourn)
    {
        return Tools::jsonEncode(file_exists(dirname(__FILE__) . '/../gen/' . ((string) $fourn) . '/') ? '1' : '0');
    }

    public function deleteFolder($dirPath)
    {
        if (!is_dir($dirPath)) {
            return $dirPath . ' must be a directory';
        }
        if (Tools::substr($dirPath, -1) != '/') {
            $dirPath .= '/';
        }
        $files = glob($dirPath . '*');
        foreach ($files as $file) {
            if (is_dir($file)) {
                self::deleteFolder($file);
            } else {
                unlink($file);
            }
        }

        return (rmdir($dirPath) == true ? '1' : '0');
    }

    public function updateFluctu($reference, $id_product = null, $fournisseur = null)
    {
        $connecteur = $fournisseur ?? self::getConnecteurName();
        if (is_null($id_product)) {
            return true;
        }
        $id_supplier = self::getEciFournId($connecteur);
        if (empty($id_supplier)) {
            return false;
        }
        $logger = self::logStart(__FUNCTION__);

        $product = self::getProduct($reference, $connecteur);
        $cat_decl = array_flip(array_column($product['declinaisons'], 'reference_attribute'));
        $derefs = array();
        $news = array();
        //for each shop, find the difference between the catalog and the shop
        foreach ($product['prestashop']['declinaisons'] as $id_shop => $psds) {
            $ps_decl = array_column($psds, 'id_product_attribute', 'reference');
            $derefs = array_merge($derefs, array_diff_key($ps_decl, $cat_decl));
            $news_shop = array_diff_key($cat_decl, $ps_decl);
            if ($news_shop) {
                $news[$id_shop] = array_merge($news[$id_shop] ?? array(), array_flip($news_shop));
            }
        }

        //add any missing combination in the shop where it is missing (setAttribute)
        foreach ($news as $id_shop => $news_shop) {
            Shop::setContext(Shop::CONTEXT_SHOP, $id_shop);
            foreach ($news_shop as $new) {
                $att = self::getSelectInArray($product['declinaisons'], 'reference_attribute', $new, true);
                try {
                    $prod = new Product($id_product, false, null, $id_shop);
                    $this->setAttribute($prod, $att, $id_shop);
                    $prod->checkDefaultAttributes();
                } catch (Exception $e) {
                    self::logInfo(
                        $logger,
                        'trouvé déclinaison "' . $new . '" absente du shop ' .
                        'appartient au produit "' . $att['product_reference'] . '" ' .
                        'qui a pour id "' . $id_product . '" a planté pour la raison :' . $e->getMessage()
                    );
                }
            }
        }

        //delete any deref combination from any shop (combinationDelete with Shop::setContext(Shop::CONTEXT_ALL))
        Shop::setContext(Shop::CONTEXT_ALL);
        foreach ($derefs as $reference => $id_product_attribute) {
            self::combinationDelete($id_product, $id_product_attribute);
        }

        return true;
    }

    public function setCatalogAllSelectedConShopForced($connecteur, $id_shop)
    {
        if (empty($connecteur) || empty($id_shop)) {
            return false;
        }

        // liste des produits du catalogue
        $lines_cat = Db::getInstance()->executeS('
            SELECT `product_reference` as `reference`, ' . (int) $id_shop . ' as `id_shop`, 1 as `imported`, `fournisseur`
            FROM `' . _DB_PREFIX_ . 'eci_catalog`
            WHERE `fournisseur` = "' . pSQL($connecteur) . '"');

        // mise à jour des produits restants dans la table imported
        $result = Db::getInstance()->update(
            'eci_product_imported',
            array(
                'imported' => (int) 1
            ),
            'fournisseur = "' . pSQL($connecteur) . '"'
        );

        // insertion des produits dans la table imported
        $result &= Db::getInstance()->insert(
            'eci_product_imported',
            $lines_cat,
            false,
            false,
            Db::INSERT_IGNORE
        );

        return $result;
    }

    public function updateProductToShop($tab, $force = false)
    {
        $db = Db::getInstance();

        $reference = $tab['reference'];
        $id_shop = $tab['id_shop'];
        $fournisseur = $tab['fournisseur'];
        $id_supplier = self::getEciFournId($fournisseur);

        // verify this product in this shop
        $ids = new ImporterReference($reference, $id_supplier);
        // if not -> setProductToShop
        if (empty($ids->id_product)) {
            return $this->setProductToShop($tab);
        }

        Shop::setContext(Shop::CONTEXT_SHOP, $id_shop);
        Context::getContext()->shop->id = (int) $id_shop;
        $thisIdLang = Configuration::get('PS_LANG_DEFAULT');
        $thisIsoLg = Language::getIsoById($thisIdLang);

        $product_eci = $db->getRow(
            'SELECT *
            FROM `' . _DB_PREFIX_ . 'eci_catalog`
            WHERE `product_reference`="' . pSQL($reference) . '"
            AND `fournisseur`="' . pSQL($fournisseur) . '"'
        );

        if (!isset($product_eci)) {
            $db->delete(
                'eci_product_imported',
                'reference="' . pSQL($reference)
                . '" AND id_shop=' . (int) $id_shop
                . ' AND fournisseur="' . pSQL($fournisseur) . '"'
            );
            return false;
        }

        // find the date_update if available
        if (is_null($tab_spe = Tools::jsonDecode($product_eci['special'], true))) {
            return false;
        }
        if (!is_array($tab_spe) || !isset($tab_spe['date_update'])) {
            return false;
        }

        $product = new Product((int) $ids->id_product, false, null, $id_shop, null);

        // update only if product is older than file infos
        try {
            $dt_product = new DateTime($product->date_upd);
            $dt_file = new DateTime($tab_spe['date_update']);
        } catch (Exception $e) {
            return false;
        }
        $interval = $dt_product->diff($dt_file);

        if (($interval->format('%R%s') <= 0) && !$force) {
            $db->delete(
                'eci_product_imported',
                'reference="' . pSQL($reference)
                . '" AND id_shop=' . (int) $id_shop
                . ' AND fournisseur="' . pSQL($fournisseur) . '"'
            );
            return;
        }

        $product->id_manufacturer = self::getManufacturer($product_eci['manufacturer']);
        $product->ean13 = trim($product_eci['ean13']);
        $product->upc = trim($product_eci['upc']);
        $product->reference = $product_eci['reference'];
        $product->supplier_reference = $product_eci['product_reference'];
        if (!is_null($dims = Tools::jsonDecode($product_eci['dimensions'], true))) {
            $product->width = !empty($dims['Lo']) ? (float) $dims['Lo'] : 0;
            $product->height = !empty($dims['Hi']) ? (float) $dims['Hi'] : 0;
            $product->depth = !empty($dims['La']) ? (float) $dims['La'] : 0;
        } else {
            $product->width = 0;
            $product->height = 0;
            $product->depth = 0;
        }
        $product->weight = (float) $product_eci['weight'];

        $product->deleteCategories(true);
        $categoryLg = self::langExpl($product_eci['category']);
        if (empty($categoryLg[$thisIsoLg])) {
            self::updateInfoEco('ID_LANG', Language::getIdByIso('fr'));
            $theCategory = '';
            $product->id_category_default = (int) Category::getTopCategory($thisIdLang);
        } else {
            $theCategory = $categoryLg[$thisIsoLg];
            // not multilang
            //$product->id_category_default = self::chercheOuCreeCategorie($theCategory, $fournisseur, 0, $id_shop, $thisIdLang);
            // multilang
            $product->id_category_default = self::chercheOuCreeCategorie($product_eci['category'], $theCategory, $fournisseur, 0, $id_shop, $thisIdLang);
        }

        $tax_rate = $product_eci['rate'];
        $id_tax_eco = $db->getValue(
            '
            SELECT `id_tax_eco`
            FROM `' . _DB_PREFIX_ . 'eci_tax`
            WHERE `rate`=' . (float) $tax_rate
        );
        $id_tax_rules_group = $db->getValue(
            '
            SELECT `id_tax_rules_group`
            FROM `' . _DB_PREFIX_ . 'eci_tax_shop`
            WHERE `id_tax_eco`=' . (int) $id_tax_eco . '
            AND `id_shop`=' . (int) $id_shop
        );
        if (!empty($id_tax_rules_group)) {
            $product->id_tax_rules_group = $id_tax_rules_group;
            /*
              } elseif (isset($id_tax_rules_group) && $id_tax_rules_group==0) {
              $tax = new Tax();
              $tax->rate = $tax_rate;
              $tax->active = 1;
              $tax->delete = 0;
              $tax->add();
             */
        } else {
            $product->id_tax_rules_group = 1;
        }

        $product->ecotax = $product_eci['ecotax'];
        $product->minimal_quantity = 1;
        $product->price = (float) $product_eci['pmvc'];
        $product->wholesale_price = (float) $product_eci['price'];
        $adship = (float) $product_eci['ship'];
        $product->additional_shipping_cost = (float) $adship;
        $product->active = ($this->tabConfig['PARAM_NEWPRODUCT'] == 1) ? 1 : 0;
        $product->redirect_type = '404';
        $product->available_for_order = 1;
        $product->show_price = 1;

        $productLg = self::langExpl($product_eci['name']);
        if (empty($productLg[$thisIsoLg])) {
            self::updateInfoEco('ID_LANG', Language::getIdByIso('fr'));
            $theProduct = '';
        } else {
            $theProduct = $productLg[$thisIsoLg];
        }
        $descriptionLg = self::langExpl($product_eci['description']);
        $thedescription = (empty($descriptionLg[$thisIsoLg]) ? '' : $descriptionLg[$thisIsoLg]);
        if (empty($thedescription)) {
            self::updateInfoEco('ID_LANG', Language::getIdByIso('fr'));
        }
        $shortdescriptionLg = self::langExpl($product_eci['short_description']);
        $theshortdescription = (empty($shortdescriptionLg[$thisIsoLg]) ? '' : $shortdescriptionLg[$thisIsoLg]);
        if (empty($theshortdescription)) {
            self::updateInfoEco('ID_LANG', Language::getIdByIso('fr'));
        }

        $languages = Language::getLanguages(false);
        foreach ($languages as $language) {
            $isoL = $language['iso_code'];
            $idL = $language['id_lang'];
            $cleanName = self::nettoyeNom(isset($productLg[$isoL]) ? $productLg[$isoL] : $theProduct, 128);
            $cleanUrl = self::str2urlParanoid($cleanName);
            $product->meta_description[(int) $idL] = $cleanName;
            $product->meta_keywords[(int) $idL] = $cleanName;
            $product->meta_title[(int) $idL] = $cleanName;
            $product->name[(int) $idL] = $cleanName;
            $product->description[(int) $idL] = isset($descriptionLg[$isoL]) ? $descriptionLg[$isoL] : $thedescription;
            $product->description_short[(int) $idL] = isset($shortdescriptionLg[$isoL]) ? $shortdescriptionLg[$isoL] : $theshortdescription;
            $product->link_rewrite[(int) $idL] = $cleanUrl;
        }

        if (!is_null($tab_spe = Tools::jsonDecode($product_eci['special'], true))) {
            if (is_array($tab_spe)) {
                foreach ($tab_spe as $prop => $val) {
                    if (property_exists($product, $prop)) {
                        if (is_array($val)) {
                            foreach ($val as $iso => $txt) {
                                if (Validate::isLangIsoCode($iso)) {
                                    $idLg = Language::getIdByIso($iso);
                                    if ($idLg) {
                                        $product->$prop[(int) $idLg] = $txt;
                                    }
                                }
                            }
                        } else {
                            $product->$prop = $val;
                        }
                    }
                }
            }
        }

        try {
            $product->update();
        } catch (Exception $e) {
            return $e->getMessage();
        }



        $listeCategoriesTo = array((int) $product->id_category_default);
        $objCategoryDefault = new Category($product->id_category_default, null, $id_shop);
        foreach ($objCategoryDefault->getParentsCategories() as $tabcat) {
            $listeCategoriesTo[] = (int) $tabcat['id_category'];
        }
        $listeCategoriesTo_unique = array_unique($listeCategoriesTo);
        $product->addToCategories($listeCategoriesTo_unique);

        if (isset($product_eci['carriers'])) {
            // vérifier la structure des données
            if (!is_null($carriers = Tools::jsonDecode($product_eci['carriers'], true))) {
                if (is_array($carriers) && isset($carriers['replace']) && isset($carriers['list'])) {
                    // récupérer les carriers actuels
                    $list_carriers = array();
                    if (!$carriers['replace']) {
                        $list_used_carriers = $product->getCarriers();
                        foreach ($list_used_carriers as $used_carrier) {
                            $list_carriers[] = $used_carrier['id_reference'];
                        }
                    }
                    // ajouter les carriers demandés s'ils existent
                    if (is_array($carriers['list'])) {
                        foreach ($carriers['list'] as $carrier_name) {
                            $carrier_id_reference = $db->getValue(
                                '
                                SELECT `id_reference`
                                FROM `' . _DB_PREFIX_ . 'carrier`
                                WHERE `name` = "' . pSQL($carrier_name) . '"'
                            );
                            if ($carrier_id_reference && !in_array($carrier_id_reference, $list_carriers)) {
                                $list_carriers[] = $carrier_id_reference;
                            }
                        }
                        // associer les carriers
                        $product->setCarriers($list_carriers);
                    }
                }
            }
        }

        $product->deleteAttachments(true);
        if (isset($product_eci['documents'])) {
            // vérifier la structure des données
            if (!is_null($documents = Tools::jsonDecode($product_eci['documents'], true))) {
                if (is_array($documents)) {
                    $idsA = array();
                    foreach ($documents as $document) {
                        if (!is_array($document)) {
                            continue;
                        }

                        if (empty($document['url']) || empty($document['name'])) {
                            continue;
                        }

                        $file = md5(uniqid());
                        $content = Tools::file_get_contents($document['url']);
                        file_put_contents(_PS_ROOT_DIR_ . '/download/' . $file, $content);
                        $finfo = finfo_open(FILEINFO_MIME_TYPE);
                        $mime = finfo_file($finfo, _PS_ROOT_DIR_ . '/download/' . $file);
                        finfo_close($finfo);
                        $file_name = preg_replace('#.*/(.*?)#', '$1', $document['url']);

                        $attach = new Attachment();
                        $attach->file = $file;
                        $attach->file_name = $file_name;
                        $attach->mime = $mime;

                        $name = array();
                        if (is_array($document['name'])) {
                            foreach ($document['name'] as $isoLg => $nameLg) {
                                if (Validate::isLanguageIsoCode($isoLg)) {
                                    $name[Language::getIdByIso($isoLg)] = self::getStrCutBytes($nameLg, 32);
                                }
                            }
                        } else {
                            $name[$thisIdLang] = self::getStrCutBytes($document['name'], 32);
                        }
                        $attach->name = $name;

                        $description = array();
                        if (is_array($document['description'])) {
                            foreach ($document['description'] as $isoLg => $descriptionLg) {
                                if (Validate::isLanguageIsoCode($isoLg)) {
                                    $description[Language::getIdByIso($isoLg)] = self::getStrCutBytes($descriptionLg, 32);
                                }
                            }
                        } else {
                            $description[$thisIdLang] = self::getStrCutBytes($document['description'], 32);
                        }
                        $attach->description = $description;

                        try {
                            if ($attach->add()) {
                                $idsA[] = $attach->id;
                            }
                        } catch (Exception $e) {
                            continue;
                        }
                    }
                    if ($idsA) {
                        Attachment::attachToProduct($product->id, $idsA);
                    }
                }
            }
        }

        // images from json rich infos or old style list of urls
        if (($tab_images = Tools::jsonDecode($product_eci['pictures'], true)) === null) {
            $tab_images = explode('//:://', $product_eci['pictures']);
        }
        if (!empty($tab_images[0])) {
            self::setImages($product, $tab_images);
        }

        $product->deleteFromSupplier();
        $product->addSupplierReference(
            (int) $product->id_supplier,
            0,
            $product->supplier_reference,
            (float) $product->wholesale_price,
            Configuration::get('PS_CURRENCY_DEFAULT')
        );

        $product->deleteDefaultAttributes();
        $product->deleteProductAttributes();
        self::setAttributes($product, $fournisseur, $product->id_shop_default);
        $product->deleteFeatures();
        self::setFeatures($product, $product_eci['feature'], $fournisseur, $product->id_shop_default);

        // attention : version <1.6.1 : pas de champs id_product dans la table product_attribute_shop -> croiser avec table non shop
        $flag_PID = Tools::version_compare(_PS_VERSION_, '1.6.1', '>=');

        // copie des attributs dans la table product_attribute_shop
        $sql = '
            SELECT ' . ($flag_PID ? 'pas.`id_product`,' : '') . '
                pas.`id_product_attribute`,' .
                (int) $id_shop . ' as id_shop,
                pas.`wholesale_price`,
                pas.`price`,
                pas.`ecotax`,
                pas.`weight`,
                pas.`unit_price_impact`,
                pas.`default_on`,
                pas.`minimal_quantity`,
                pas.`available_date`
            FROM `' . _DB_PREFIX_ . 'product_attribute_shop` pas,
            `' . _DB_PREFIX_ . 'product_attribute` pa
            WHERE pas.`id_product_attribute` = pa.`id_product_attribute`
            AND pa.`id_product` = ' . (int) $product->id;
        $listPAS_other = $db->executeS($sql . ' AND pas.`id_shop` != ' . (int) $id_shop);
        if ($listPAS_other) {
            $listPAS_shop = $db->executeS($sql . ' AND pas.`id_shop` = ' . (int) $id_shop);
            $aPAS_shop = array();
            foreach ($listPAS_shop as $lignePAS_shop) {
                $aPAS_shop[] = $lignePAS_shop['id_product_attribute'];
            }
            $aPAS_todo = array();
            foreach ($listPAS_other as $lignePAS_other) {
                $idPAS = $lignePAS_other['id_product_attribute'];
                if (!in_array($idPAS, $aPAS_todo) && !in_array($idPAS, $aPAS_shop)) {
                    $aPAS_todo[] = $idPAS;
                }
            }
            if ($aPAS_todo) {
                $aPASdone = array();
                foreach ($aPAS_todo as $idPAS) {
                    foreach ($listPAS_other as $lignePAS_other) {
                        $idnow = $lignePAS_other['id_product_attribute'];
                        if (($idPAS == $idnow) && !in_array($idnow, $aPASdone)) {
                            if ($lignePAS_other['default_on'] == 0) {
                                $tmp = array();
                                foreach ($lignePAS_other as $key => $val) {
                                    if ($key != 'default_on') {
                                        $tmp[$key] = $val;
                                    }
                                }
                                $lignePAS_other = $tmp;
                            }
                            $db->insert('product_attribute_shop', $lignePAS_other);
                            $aPASdone[] = $idnow;
                        }
                    }
                }
            }
        }

        // copie des attributs dans la table attribute_shop
        $db->execute(
            '
            INSERT INTO `' . _DB_PREFIX_ . 'attribute_shop`
            (`id_attribute`, `id_shop`)
            SELECT DISTINCT `id_attribute`, ' . (int) $id_shop . '
            FROM `' . _DB_PREFIX_ . 'product_attribute_combination` pac
            INNER JOIN `' . _DB_PREFIX_ . 'product_attribute` pa
            ON pa.`id_product_attribute`=pac.`id_product_attribute`
            WHERE pa.`id_product`=' . (int) $product->id . '
            AND `id_attribute` NOT IN
                (SELECT `id_attribute`
                FROM `' . _DB_PREFIX_ . 'attribute_shop`
                WHERE `id_shop`=' . (int) $id_shop . ')'
        );

        // copie des groupes d'attributs dans la table attribute_group_shop
        $db->execute(
            '
            INSERT INTO `' . _DB_PREFIX_ . 'attribute_group_shop`
            (`id_attribute_group`, `id_shop`)
            SELECT DISTINCT `id_attribute_group`, ' . (int) $id_shop . '
            FROM `' . _DB_PREFIX_ . 'attribute` a
            JOIN `' . _DB_PREFIX_ . 'product_attribute_combination` pac
            ON pac.`id_attribute`=a.`id_attribute`
            JOIN `' . _DB_PREFIX_ . 'product_attribute` pa
            ON pac.`id_product_attribute`=pa.`id_product_attribute`
            WHERE pa.`id_product`=' . (int) $product->id . '
            AND `id_attribute_group` NOT IN
                (SELECT `id_attribute_group`
                FROM `' . _DB_PREFIX_ . 'attribute_group_shop`
                WHERE `id_shop`=' . (int) $id_shop . ')'
        );

        $assocShops = $product->getAssociatedShops();
        $tasdimages = Image::getImages($thisIdLang, $product->id, 0);
        foreach ($tasdimages as $image) {
            $objImage = new Image($image['id_image']);
            $objImage->associateTo($assocShops);
            $insert = array(
                'id_product' => (int) $product->id
            );
            if (!empty($image['cover'])) {
                $insert['cover'] = (int) 1;
            }
            try {
                $db->update(
                    'image_shop',
                    $insert,
                    'id_image = ' . (int) $image['id_image']
                );
            } catch (Exception $e) {
            }
        }

        Hook::exec('actionProductUpdate', array('id_product' => (int) $product->id, 'product' => $product));
        $db->insert(
            'eci_product_shop',
            array('reference' => pSQL($product->supplier_reference),
                'id_product' => (int) $product->id,
                'id_shop' => (int) $id_shop,
                'imported' => 0,
                'fournisseur' => pSQL($fournisseur)
            ),
            false,
            false,
            DB::INSERT_IGNORE
        );
        $db->delete(
            'eci_product_imported',
            '`reference`=\'' . pSQL($reference)
            . '\' AND `id_shop`=' . (int) $id_shop
            . ' AND `fournisseur`=\'' . pSQL($fournisseur) . '\''
        );
        $product->deleteSearchIndexes();
        Search::indexation(false, $product->id);

        return true;
    }

    public static function cleanImported()
    {
        $db = Db::getInstance();

        $db->delete('eci_product_imported', 'imported = 0');

        $liste_four = $db->executeS(
            'SELECT name
            FROM ' . _DB_PREFIX_ . 'eci_fournisseur
            WHERE 1'
        );
        foreach ($liste_four as $four) {
            self::cleanImportedFour($four['name']);
        }
    }

    public static function cleanImportedFour($four)
    {
        $db = Db::getInstance();
        $id_four = self::getEciFournId($four);

        // delete if product is dereferenced
        $db->execute(
            'DELETE i
            FROM ' . _DB_PREFIX_ . 'eci_product_imported i
            LEFT JOIN ' . _DB_PREFIX_ . 'eci_catalog c
            ON i.reference = c.product_reference
            AND c.fournisseur = "' . pSQL($four) . '"
            WHERE i.fournisseur = "' . pSQL($four) . '"
            AND c.product_reference IS NULL'
        );

        // delete if product is sync
        $list_imported = $db->executeS(
            'SELECT reference, id_shop
            FROM ' . _DB_PREFIX_ . 'eci_product_imported
            WHERE fournisseur = "' . pSQL($four) . '"
            AND imported = 1'
        );

        $multishop = Configuration::get('PS_MULTISHOP_FEATURE_ACTIVE') ? true : false;

        foreach ($list_imported as $prod2i) {
            $product = new ImporterReference($prod2i['reference'], $id_four);
            if (!empty($product->id_product)) {
                $in_shop = true;
                if ($multishop) {
                    $in_shop = (bool) $db->getValue(
                        'SELECT COUNT(*)
                        FROM ' . _DB_PREFIX_ . 'product_shop
                        WHERE id_product = ' . (int) $product->id_product . '
                        AND id_shop = ' . (int) $prod2i['id_shop']
                    );
                }
                if ($in_shop) {
                    $db->delete(
                        'eci_product_imported',
                        'reference = "' . pSQL($prod2i['reference'])
                        . '" AND id_shop = ' . (int) $prod2i['id_shop']
                        . ' AND fournisseur = "' . pSQL($four) . '"'
                    );
                }
            }
        }
    }

    public function clearImported()
    {
        Db::getInstance()->delete('eci_product_imported', '1');
    }

    public static function cleanEciProdShop()
    {
        $db = Db::getInstance();

        $db->delete('eci_product_shop', 'imported != 0 OR reference = ""');

        $liste_four = $db->executeS(
            'SELECT name
            FROM ' . _DB_PREFIX_ . 'eci_fournisseur
            WHERE perso = 1'
        );
        foreach ($liste_four as $four) {
            self::cleanEciProdShopFour($four['name']);
        }
    }

    public static function cleanEciProdShopFour($four)
    {
        //delete unknown products from eci_product_shop
        Db::getInstance()->execute(
            'DELETE e
            FROM '._DB_PREFIX_.'eci_product_shop e
            LEFT JOIN '._DB_PREFIX_.'product p
            ON p.id_product = e.id_product
            WHERE e.fournisseur = "' . pSQL($four) . '"
            AND p.id_product IS NULL'
        );

        //delete unknown products attributes from eci_product_shop
        Db::getInstance()->execute(
            'DELETE e
            FROM '._DB_PREFIX_.'eci_product_shop e
            LEFT JOIN '._DB_PREFIX_.'product_attribute a
            ON a.id_product_attribute = e.id_product_attribute
            WHERE e.fournisseur = "' . pSQL($four) . '"
            AND e.id_product_attribute != 0
            AND a.id_product_attribute IS NULL'
        );

        if (!Configuration::get('PS_MULTISHOP_FEATURE_ACTIVE')) {
            return true;
        }

        //delete unknown products from eci_product_shop in this shop
        Db::getInstance()->execute(
            'DELETE e
            FROM '._DB_PREFIX_.'eci_product_shop e
            LEFT JOIN '._DB_PREFIX_.'product_shop s
            ON s.id_product = e.id_product
            AND s.id_shop = e.id_shop
            WHERE e.fournisseur = "' . pSQL($four) . '"
            AND s.id_product IS NULL'
        );

        //delete unknown products attributes from eci_product_shop in this shop
        Db::getInstance()->execute(
            'DELETE e
            FROM '._DB_PREFIX_.'eci_product_shop e
            LEFT JOIN '._DB_PREFIX_.'product_attribute_shop s
            ON s.id_product_attribute = e.id_product_attribute
            AND s.id_shop = e.id_shop
            WHERE e.fournisseur = "' . pSQL($four) . '"
            AND e.id_product_attribute != 0
            AND s.id_product_attribute IS NULL'
        );

        //duplicate if product is multishop
        Db::getInstance()->execute(
            'INSERT IGNORE INTO '._DB_PREFIX_.'eci_product_shop (reference, id_product, id_shop, keep, is_pack, fournisseur)
            SELECT DISTINCT p.supplier_reference, e.id_product, ps.id_shop, e.keep, e.is_pack, "' . pSQL($four) . '"
            FROM '._DB_PREFIX_.'eci_product_shop e
            LEFT JOIN '._DB_PREFIX_.'product_shop ps ON e.id_product = ps.id_product
            LEFT JOIN '._DB_PREFIX_.'product p ON p.id_product = ps.id_product
            WHERE e.fournisseur = "' . pSQL($four) . '"
            AND ps.id_product IS NOT NULL
            AND p.supplier_reference IS NOT NULL AND p.supplier_reference != ""'
        );
        
        //duplicate if combination is multishop
        Db::getInstance()->execute(
            'INSERT IGNORE INTO '._DB_PREFIX_.'eci_product_shop (reference, id_product, id_product_attribute, id_shop, keep, fournisseur)
            SELECT DISTINCT pa.supplier_reference, e.id_product, pas.id_product_attribute, pas.id_shop, e.keep, "' . pSQL($four) . '"
            FROM '._DB_PREFIX_.'eci_product_shop e
            LEFT JOIN '._DB_PREFIX_.'product_attribute_shop pas ON e.id_product = pas.id_product
            LEFT JOIN '._DB_PREFIX_.'product_attribute pa ON pa.id_product_attribute = pas.id_product_attribute
            WHERE e.fournisseur = "' . pSQL($four) . '"
            AND pas.id_product_attribute IS NOT NULL
            AND pa.supplier_reference IS NOT NULL AND pa.supplier_reference != ""'
        );

        return true;
    }

/*    public static function cleanEciProdShopFour_old($four, $id_shop = null)
    {
        if ($id_shop == null) {
            $db = null;
        }
        $db = Db::getInstance();
        $id_four = self::getEciFournId($four);

        $multishop = Configuration::get('PS_MULTISHOP_FEATURE_ACTIVE') ? true : false;

        // repair broken id_product <-> supplier_reference associations, hope user keeps catalog reference
        $list_not_ok0 = $db->executeS(
            'SELECT DISTINCT ec.id_product, p.supplier_reference, ec.reference
            FROM ' . _DB_PREFIX_ . 'eci_product_shop ec
            LEFT JOIN ' . _DB_PREFIX_ . 'product p
            ON p.id_product = ec.id_product
            WHERE p.supplier_reference != ec.reference
            AND p.id_supplier = ' . (int) $id_four
        );
        if ($list_not_ok0) {
            foreach ($list_not_ok0 as $ligne) {
                try {
                    $db->update(
                        'eci_product_shop',
                        array(
                            'reference' => pSQL($ligne['supplier_reference']),
                        ),
                        'id_product = ' . (int) $ligne['id_product']
                    );
                } catch (Exception $e) {
                    $db->delete(
                        'eci_product_shop',
                        'id_product = ' . (int) $ligne['id_product'] . ' AND reference = "' . pSQL($ligne['reference']) . '"'
                    );
                }
            }
        }
        unset($list_not_ok0);

        // repair broken product <-> default supplier associations
        $list_not_ok00 = $db->executeS(
            'SELECT DISTINCT ec.id_product
            FROM ' . _DB_PREFIX_ . 'eci_product_shop ec
            LEFT JOIN ' . _DB_PREFIX_ . 'product p
            ON p.id_product = ec.id_product
            WHERE p.id_supplier = ' . (int) $id_four . '
            AND ec.fournisseur != "' . pSQL($four) . '"'
        );
        if ($list_not_ok00) {
            foreach ($list_not_ok00 as $ligne) {
                try {
                    $db->update(
                        'eci_product_shop',
                        array(
                            'fournisseur' => pSQL($four),
                        ),
                        'id_product = ' . (int) $ligne['id_product']
                    );
                } catch (Exception $e) {
                    $db->delete(
                        'eci_product_shop',
                        'id_product = ' . (int) $ligne['id_product'] . ' AND fournisseur != "' . pSQL($four) . '"'
                    );
                }
            }
        }
        unset($list_not_ok00);

        // delete if product is not synced
        $list_not_ok1 = $db->executeS(
            'SELECT ec.id_product
            FROM ' . _DB_PREFIX_ . 'eci_product_shop ec
            LEFT JOIN ' . _DB_PREFIX_ . 'product p
            ON p.id_product = ec.id_product
            WHERE p.id_product IS NULL
            AND ec.fournisseur = "' . pSQL($four) . '"'
        );
        if ($list_not_ok1) {
            foreach ($list_not_ok1 as $ligne) {
                $db->delete(
                    'eci_product_shop',
                    'id_product = ' . (int) $ligne['id_product']
                );
            }
        }
        unset($list_not_ok1);

        // add if product synced
        $list_not_ok2 = $db->executeS(
            'SELECT p.id_product, p.supplier_reference, p.id_shop_default
            FROM ' . _DB_PREFIX_ . 'product p
            LEFT JOIN ' . _DB_PREFIX_ . 'eci_product_shop ec
            ON p.id_product = ec.id_product
            WHERE p.id_supplier = ' . (int) $id_four . '
            AND ec.id_product IS NULL'
        );
        if ($list_not_ok2) {
            foreach ($list_not_ok2 as $ligne) {
                $db->insert(
                    'eci_product_shop',
                    array(
                        'reference' => pSQL($ligne['supplier_reference']),
                        'id_product' => (int) $ligne['id_product'],
                        'id_shop' => (int) $ligne['id_shop_default'],
                        'imported' => (int) 0,
                        'fournisseur' => pSQL($four),
                    ),
                    false,
                    false,
                    Db::INSERT_IGNORE
                );
            }
        }
        unset($list_not_ok2);

        // delete if product not in shop (multishop)
        if ($multishop) {
            $list_not_ok3 = $db->executeS(
                'SELECT ec.id_product, ec.id_shop
                FROM ' . _DB_PREFIX_ . 'eci_product_shop ec
                LEFT JOIN ' . _DB_PREFIX_ . 'product_shop ps
                ON ec.id_product = ps.id_product
                AND ec.id_shop = ps.id_shop
                WHERE ps.id_product IS NULL
                AND ec.fournisseur = "' . pSQL($four) . '"'
            );
            if ($list_not_ok3) {
                foreach ($list_not_ok3 as $ligne) {
                    $db->delete(
                        'eci_product_shop',
                        'id_product = ' . (int) $ligne['id_product'] . ' AND id_shop = ' . (int) $ligne['id_shop']
                    );
                }
            }
        }
        unset($list_not_ok3);

        // add if product in shop (multishop)
        if ($multishop) {
            $list_not_ok4 = $db->executeS(
                'SELECT p.id_product, p.supplier_reference, ps.id_shop
                FROM ' . _DB_PREFIX_ . 'product p
                LEFT JOIN ' . _DB_PREFIX_ . 'product_shop ps
                ON ps.id_product = p.id_product
                LEFT JOIN ' . _DB_PREFIX_ . 'eci_product_shop ec
                ON ec.id_product = ps.id_product
                AND ec.id_shop = ps.id_shop
                WHERE p.id_supplier = ' . (int) $id_four . '
                AND (ec.id_product IS NULL
                OR ps.id_product IS NULL)'
            );
            if ($list_not_ok4) {
                foreach ($list_not_ok4 as $ligne) {
                    $db->insert(
                        'eci_product_shop',
                        array(
                            'reference' => pSQL($ligne['supplier_reference']),
                            'id_product' => (int) $ligne['id_product'],
                            'id_shop' => (int) $ligne['id_shop'],
                            'imported' => (int) 0,
                            'fournisseur' => pSQL($four),
                        ),
                        false,
                        false,
                        Db::ON_DUPLICATE_KEY
                    );
                }
            }
        }
        unset($list_not_ok4);

        $db->delete('eci_product_shop', 'imported != 0 OR reference = ""');
    }*/

    public static function clearEciProdShop()
    {
        Db::getInstance()->delete('eci_product_shop', '1');
    }
    
    public static function cleanFeatureMatching($connecteur = null, $id_feature = null)
    {
        // id_feature = 0 in eci_feature_shop where no corresponding feature in PS
        $req = 'UPDATE ' . _DB_PREFIX_ . 'eci_feature_shop eci
            LEFT JOIN ' . _DB_PREFIX_ . 'feature ps
            ON eci.id_feature = ps.id_feature
            SET eci.id_feature = 0
            WHERE ps.id_feature IS NULL
            AND eci.id_feature != 0' .
            (is_null($connecteur) ? '' : ' AND eci.fournisseur = "' . pSQL($connecteur) . '"') .
            (is_null($id_feature) ? '' : ' AND eci.id_feature = ' . (int) $id_feature);

        return Db::getInstance()->execute($req);
    }
    
    public static function cleanFeatureValueMatching($connecteur = null, $id_feature_value = null)
    {
        // id_feature = 0 in eci_feature_value_shop where no corresponding feature_value in PS
        $req1 = 'UPDATE ' . _DB_PREFIX_ . 'eci_feature_value_shop eci
            LEFT JOIN ' . _DB_PREFIX_ . 'feature_value ps
            ON eci.id_feature = ps.id_feature_value
            SET eci.id_feature = 0
            WHERE ps.id_feature_value IS NULL
            AND eci.id_feature != 0' .
            (is_null($connecteur) ? '' : ' AND eci.fournisseur = "' . pSQL($connecteur) . '"') .
            (is_null($id_feature_value) ? '' : ' AND eci.id_feature = ' . (int) $id_feature_value);
        
        Db::getInstance()->execute($req1);

        // id_feature = 0 in eci_feature_value_shop where id_feature null or 0 in eci_feature_shop (dematch child if parent is not matched)
        $req2 = 'UPDATE ' . _DB_PREFIX_ . 'eci_feature_value_shop fvs
            LEFT JOIN ' . _DB_PREFIX_ . 'eci_feature_value fv
            ON fvs.id_feature_eco_value = fv.id_feature_eco_value
            LEFT JOIN ' . _DB_PREFIX_ . 'eci_feature_shop fs
            ON fv.id_feature_eco = fs.id_feature_eco AND fs.id_shop = fvs.id_shop
            SET fvs.id_feature = 0
            WHERE (fs.id_feature IS NULL OR fs.id_feature = 0)
            AND fvs.id_feature != 0' .
            (is_null($connecteur) ? '' : ' AND fv.fournisseur = "' . pSQL($connecteur) . '"') .
            (is_null($id_feature_value) ? '' : ' AND fvs.id_feature = ' . (int) $id_feature_value);

        Db::getInstance()->execute($req2);
    }

    public function setCatalogAllSelectedConShop($connecteur, $id_shop)
    {
        if (empty($connecteur) || empty($id_shop)) {
            return false;
        }

        Db::getInstance()->delete('eci_product_imported', 'id_shop = ' . (int) $id_shop . ' AND imported != 1 AND fournisseur = "' . pSQL($connecteur) . '"');

        // liste des produits de la table shop
        $lines_shop = Db::getInstance()->executeS(
            'SELECT reference
            FROM ' . _DB_PREFIX_ . 'eci_product_shop
            WHERE id_shop = ' . (int) $id_shop . '
            AND id_product_attribute = 0
            AND fournisseur = "' . pSQL($connecteur) . '"'
        );
        $list_s = array();
        foreach ($lines_shop as $line) {
            $list_s[] = $line['reference'];
        }
        $list_shop = array_flip($list_s);
        unset($lines_shop, $list_s);

        // liste des produits de la table imported
        $lines_imp = Db::getInstance()->executeS(
            'SELECT reference
            FROM ' . _DB_PREFIX_ . 'eci_product_imported
            WHERE id_shop = ' . (int) $id_shop . '
            AND fournisseur = "' . pSQL($connecteur) . '"'
        );
        $list_i = array();
        foreach ($lines_imp as $line) {
            $list_i[] = $line['reference'];
        }
        $list_imp = array_flip($list_i);
        unset($lines_imp, $list_i);

        // liste des produits du catalogue
        $lines_cat = Db::getInstance()->executeS(
            'SELECT product_reference as reference, ' . (int) $id_shop . ' as id_shop, 1 as imported, fournisseur
            FROM ' . _DB_PREFIX_ . 'eci_catalog
            WHERE fournisseur = "' . pSQL($connecteur) . '"'
        );

        // exclusion des produits déjà en shop ou en imported
        $list_to_select = array();
        foreach ($lines_cat as $line) {
            if (!array_key_exists($line['reference'], $list_shop) && !array_key_exists($line['reference'], $list_imp)) {
                $list_to_select[] = $line;
            }
        }
        unset($lines_cat, $list_shop, $list_imp);

        // insertion des produits restants dans la table imported
        $inserts = array_chunk($list_to_select, 1000);
        unset($list_to_select);
        $result = true;
        foreach ($inserts as $insert) {
            $result &= Db::getInstance()->insert(
                'eci_product_imported',
                $insert
            );
        }
        
        return $result;
    }

    public function setCatalogAllDeselectedConShop($connecteur)
    {
        if (empty($connecteur)) {
            return false;
        }

        Db::getInstance()->delete('eci_product_imported', 'id_shop = ' . (int) $this->id_shop . ' AND imported != 2 AND fournisseur = "' . pSQL($connecteur) . '"');

        // liste des produits de la table shop
        $lines_shop = Db::getInstance()->executeS(
            'SELECT reference
            FROM ' . _DB_PREFIX_ . 'eci_product_shop
            WHERE id_shop = ' . (int) $this->id_shop . '
            AND id_product_attribute = 0
            AND fournisseur = "' . pSQL($connecteur) . '"'
        );
        $list_s = array();
        foreach ($lines_shop as $line) {
            $list_s[] = $line['reference'];
        }
        $list_shop = array_flip($list_s);
        unset($lines_shop, $list_s);

        // liste des produits de la table imported
        $lines_imp = Db::getInstance()->executeS(
            'SELECT reference
            FROM ' . _DB_PREFIX_ . 'eci_product_imported
            WHERE id_shop = ' . (int) $this->id_shop . '
            AND fournisseur = "' . pSQL($connecteur) . '"'
        );
        $list_i = array();
        foreach ($lines_imp as $line) {
            $list_i[] = $line['reference'];
        }
        $list_imp = array_flip($list_i);
        unset($lines_imp, $list_i);

        // liste des produits du catalogue
        $lines_cat = Db::getInstance()->executeS(
            'SELECT product_reference as reference, ' . (int) $this->id_shop . ' as id_shop, 2 as imported, fournisseur
            FROM ' . _DB_PREFIX_ . 'eci_catalog
            WHERE fournisseur = "' . pSQL($connecteur) . '"'
        );

        // conservation des produits en shop mais pas en imported
        $list_to_select = array();
        foreach ($lines_cat as $line) {
            if (array_key_exists($line['reference'], $list_shop) && !array_key_exists($line['reference'], $list_imp)) {
                $list_to_select[] = $line;
            }
        }
        unset($lines_cat, $list_shop, $list_imp);

        // insertion des produits restants dans la table imported
        $inserts = array_chunk($list_to_select, 1000);
        unset($list_to_select);
        $result = true;
        foreach ($inserts as $insert) {
            $result &= Db::getInstance()->insert(
                'eci_product_imported',
                $insert
            );
        }

        return $result;
    }

    public function undoSelection($connecteur)
    {
        Db::getInstance()->delete('eci_product_imported', 'id_shop = ' . (int) $this->id_shop . ' AND fournisseur = "' . pSQL($connecteur) . '"');
    }

    public function undoSelectionConShop($connecteur)
    {
        Db::getInstance()->delete('eci_product_imported', 'id_shop = ' . (int) $this->id_shop . ' AND fournisseur = "' . pSQL($connecteur) . '"');
    }

    public static function getTotalProduct($connecteur)
    {
        return (int) Db::getInstance()->getValue(
            'SELECT COUNT(*)
            FROM ' . _DB_PREFIX_ . 'eci_catalog
            WHERE fournisseur LIKE "' . pSQL($connecteur) . '"'
        );
    }

    public static function getTotalProductInShop($connecteur, $id_shop)
    {
        return (int) Db::getInstance()->getValue(
            'SELECT COUNT(*)
            FROM ' . _DB_PREFIX_ . 'eci_product_shop
            WHERE fournisseur LIKE "' . pSQL($connecteur) . '"
            AND id_product_attribute = 0
            AND id_shop = ' . (int)$id_shop
        );
    }

    public function productBlacklist($connecteur, $reference, $blacklist, $id_shop = 0)
    {
        if (!$id_shop) {
            $id_shop = $this->id_shop;
        }

        return Db::getInstance()->insert(
            'eci_product_blacklist',
            array(
                'reference' => pSQL($reference),
                'fournisseur' => pSQL($connecteur),
                'id_shop' => (int) $id_shop,
                'blacklist' => (int) $blacklist
            ),
            false,
            false,
            Db::ON_DUPLICATE_KEY
        );
    }

    public function productBlacklistByCategory($connecteur, $category, $blacklist, $id_shop = 0)
    {
        if (!$id_shop) {
            $id_shop = $this->id_shop;
        }

        return (bool) Db::getInstance()->execute(
            'INSERT INTO ' . _DB_PREFIX_ . 'eci_product_blacklist
            (reference, fournisseur, id_shop, blacklist)
            SELECT product_reference, fournisseur, "' . (int) $id_shop . '", "' . (int) $blacklist . '"
            FROM ' . _DB_PREFIX_ . 'eci_catalog
            WHERE category RLIKE "([a-z]{2}:://::)?' . pSQL($category) . '"
            AND fournisseur LIKE "' . pSQL($connecteur) . '"
            ON DUPLICATE KEY UPDATE blacklist = "' . (int) $blacklist . '"'
        );
    }

    public function productBlacklistByCategoryTree($connecteur, $category, $blacklist, $id_shop = 0)
    {
        if (!$id_shop) {
            $id_shop = $this->id_shop;
        }

        return Db::getInstance()->execute(
            'INSERT INTO ' . _DB_PREFIX_ . 'eci_product_blacklist
            (reference, fournisseur, id_shop, blacklist)
            SELECT product_reference, fournisseur, "' . (int) $id_shop . '", "' . (int) $blacklist . '"
            FROM ' . _DB_PREFIX_ . 'eci_catalog
            WHERE category RLIKE "([a-z]{2}:://::)?' . pSQL($category) . '(::>>::.*)?"
            AND fournisseur LIKE "' . pSQL($connecteur) . '"
            ON DUPLICATE KEY UPDATE blacklist = "' . (int) $blacklist . '"'
        );
    }

    public static function cleanProductBlacklist($connecteur = null, $id_shop = null)
    {
        return Db::getInstance()->execute(
            'DELETE b FROM ' . _DB_PREFIX_ . 'eci_product_blacklist b
            LEFT JOIN ' . _DB_PREFIX_ . 'eci_catalog c
            ON b.reference = c.product_reference
            AND b.fournisseur = c.fournisseur
            WHERE b.fournisseur = "' . pSQL($connecteur) . '"
            AND c.product_reference IS NULL'
            . (is_null($id_shop) ? '' : ' AND id_shop = ' . (int) $id_shop)
        );
    }

    public static function clearProductBlacklist($connecteur = null, $id_shop = null)
    {
        return Db::getInstance()->delete(
            'eci_product_blacklist',
            '1'
            . (is_null($connecteur) ? '' : ' AND fournisseur = "' . pSQL($connecteur) . '"')
            . (is_null($id_shop) ? '' : ' AND id_shop = ' . (int) $id_shop)
        );
    }

    public function categoryBlacklist($connecteur, $id, $blacklist, $id_shop = 0)
    {
        if (!$id_shop) {
            $id_shop = $this->id_shop;
        }

        $ret = true;
        if (($path = self::categoryGetPathById($id))) {
            $ret &= (bool) Db::getInstance()->update(
                'eci_category_shop',
                array('blacklist' => (int) $blacklist),
                'id = ' . (int) $id
            );
            $ret &= $this->productBlacklistByCategory($connecteur, $path, $blacklist, $id_shop);
        }

        return $ret;
    }

    public function categoryBlacklistTree($connecteur, $id, $blacklist, $id_shop = 0)
    {
        if (!$id_shop) {
            $id_shop = $this->id_shop;
        }

        $ret = true;
        if (($path = self::categoryGetPathById($id))) {
            $ret &= (bool) Db::getInstance()->update(
                'eci_category_shop',
                array('blacklist' => (int) $blacklist),
                'name RLIKE "' . pSQL($path) . '(::>>::.*)?" AND id_shop = ' . (int) $id_shop
            );
            $ret &= $this->productBlacklistByCategoryTree($connecteur, $path, $blacklist, $id_shop);
        }

        return $ret;
    }

    public static function clearCategoryBlacklist($connecteur = null, $id_shop = null)
    {
        return Db::getInstance()->update(
            'eci_category_shop',
            array('blacklist' => (int) 0),
            '1'
            . (is_null($connecteur) ? '' : ' AND fournisseur = "' . pSQL($connecteur) . '"')
            . (is_null($id_shop) ? '' : ' AND id_shop = ' . (int) $id_shop)
        );
    }

    public static function categoryBranchesToPaths($category_group)
    {
        $branches = is_array($category_group) ? $category_group : Tools::jsonDecode($category_group, true);

        if (is_null($branches)) {
            return array();
        }

        $paths = array();
        foreach ($branches as $id_branch => $branch) {
            if (!is_array($branch)) {
                return array();
            }
            foreach ($branch as $level) {
                if (!is_array($level)) {
                    return array();
                }
                foreach ($level as $iso_lang => $name) {
                    $paths[$id_branch][$iso_lang][] = $name;
                }
            }
        }

        foreach ($paths as &$path) {
            $pathL = array();
            foreach ($path as $iso_lang => $name) {
                $pathL[] = $iso_lang . ':://::' . implode('::>>::', $name);
            }
            $path = implode('//:://', $pathL);
        }

        return $paths;
    }

    public static function categoryPathsToBranches($paths)
    {
        if (!is_array($paths)) {
            $paths = array($paths);
        }

        $branches = array();
        foreach ($paths as $id_path => $path) {
            $pathL = self::langExpl($path);
            foreach ($pathL as $iso_lang => $chemin) {
                $tab_chemin = explode('::>>::', $chemin);
                foreach ($tab_chemin as $level => $name) {
                    $branches[$id_path][$level][$iso_lang] = $name;
                }
            }
        }

        return Tools::jsonEncode($branches);
    }

    public static function categoryPathToPaths($path)
    {
        $cat_sep = '::>>::';
        $pieces = explode($cat_sep, $path);
        $paths = array();
        $part_path = '';
        foreach ($pieces as $piece) {
            $part_path .= $piece;
            $paths[] = $part_path;
            $part_path .= $cat_sep;
        }

        return $paths;
    }

    public static function categoryGetPathById($id)
    {
        return Db::getInstance()->getValue(
            'SELECT name
            FROM ' . _DB_PREFIX_ . 'eci_category_shop
            WHERE id = ' . (int) $id
        );
    }

    public function categoryGetIdByPath($path, $id_shop = 0, $global = false)
    {
        if (!$id_shop) {
            $id_shop = $this->id_shop;
        }

        return Db::getInstance()->getValue(
            'SELECT id
            FROM ' . _DB_PREFIX_ . 'eci_category_shop
            WHERE name LIKE "' . pSQL($path) . '"
            AND id_shop = ' . (int) $id_shop
            . ($global ? '' : ' AND fournisseur = "' . pSQL(self::getConnecteurName()) . '"')
        );
    }

    public function categoryGetIdCategoryByPath($path, $id_shop = 0, $global = false)
    {
        if (!$id_shop) {
            $id_shop = $this->id_shop;
        }

        return Db::getInstance()->getValue(
            'SELECT id_category
            FROM ' . _DB_PREFIX_ . 'eci_category_shop
            WHERE name LIKE "' . pSQL($path) . '"
            AND id_shop = ' . (int) $id_shop
            . ($global ? '' : ' AND fournisseur = "' . pSQL(self::getConnecteurName()) . '"')
        );
    }

    public function categoryGetAriane($path, $iso_lang = null)
    {
        if (is_null($iso_lang)) {
            $iso_lang = Language::getIsoById($this->id_lang);
        }

        $pathLg = self::langExpl($path);

        return str_replace('::>>::', ' > ', ($pathLg[$iso_lang] ?? reset($pathLg)));
    }

    public function chercheOuCreeCategorie($categMultiLg, $categ, $fournisseur, $categParent = 0, $id_shop = 0, $id_lang = 0)
    {
        if (empty($id_shop)) {
            $id_shop = $this->id_shop;
        }
        if ($id_shop != Context::getContext()->shop->id) {
            Shop::setContext(Shop::CONTEXT_SHOP, (int) $id_shop);
            Context::getContext()->shop->id = (int) $id_shop;
        }
        if (empty($id_lang)) {
            $id_lang = Configuration::get('PS_LANG_DEFAULT');
        }
        Context::getContext()->language->id = $id_lang;

        if (!Cache::isStored('eci_shops_ids')) {
            Cache::store('eci_shops_ids', Shop::getCompleteListOfShopsID());
        }
        $eci_shops_ids = Cache::retrieve('eci_shops_ids');
        $multishop = (count($eci_shops_ids) > 1);

        $tabCategorie = explode('::>>::', $categ);
        $totDepth = $curDepth = count($tabCategorie);
        $stopDepth = 0;
        $deepCategory = false;
        for ($i = $totDepth; $i > 0; $i--) {
            $chemin = implode('::>>::', array_slice($tabCategorie, 0, $i));
            $idCategory = Db::getInstance()->getValue(
                'SELECT id_category
                FROM ' . _DB_PREFIX_ . 'eci_category_shop
                WHERE name LIKE "' . pSQL($chemin) . '"
                AND fournisseur LIKE "' . pSQL($fournisseur) . '"
                AND id_shop = ' . (int) $id_shop
            );
            /*
            if (empty($idCategory)) {
                $idCategory = Db::getInstance()->getValue('
                    SELECT id_category
                    FROM ' . _DB_PREFIX_ . 'eci_category_shop
                    WHERE name LIKE "' . pSQL($chemin) . '"
                    AND fournisseur LIKE "' . pSQL($fournisseur) . '"');
            }
            */
            if ($idCategory) {
                if ($stopDepth == 0) {
                    $stopDepth = $curDepth;
                    $deepCategory = $idCategory;
                }
                if ($multishop) {
                    //$cat = new Category($idCategory, $id_lang, $id_shop);
                    $cat = new Category($idCategory);
                    $cat->associateTo($id_shop);
                    Category::cleanPositions($cat->id_parent);
                    unset($cat);
                } else {
                    break;
                }
            }
            $curDepth--;
        }
        if ($stopDepth != $totDepth) {
            $idCategory = $deepCategory ? $deepCategory : 0;
            $curDepth = $stopDepth;
            while ($curDepth < $totDepth) {
                $idCategory = $this->creeCategorie($categMultiLg, $curDepth, $tabCategorie[$curDepth], $fournisseur, $idCategory, $id_shop, $id_lang);
                $this->setCategorieConToCategorieLoc($fournisseur, implode('::>>::', array_slice($tabCategorie, 0, $curDepth + 1)), $idCategory, $id_shop, $id_lang);
                $curDepth++;
            }
            //Category::regenerateEntireNtree();
        } else {
            $idCategory = $deepCategory;
        }
        return $idCategory;
    }

    public function creeCategorie($categMultiLg, $depth, $categorie, $fournisseur, $categParent = 0, $id_shop = 0, $id_lang = 0)
    {
        if (empty($id_shop)) {
            $id_shop = $this->id_shop;
        }
        if ($id_shop != Context::getContext()->shop->id) {
            Shop::setContext(Shop::CONTEXT_SHOP, (int) $id_shop);
            Context::getContext()->shop->id = (int) $id_shop;
        }
        if (empty($id_lang)) {
            $id_lang = Configuration::get('PS_LANG_DEFAULT');
        }
        Context::getContext()->language->id = $id_lang;

        $tabArboMultiLg = self::langExpl($categMultiLg);
        $tabCategMultiLg = array();
        foreach ($tabArboMultiLg as $isoLg => $arbo) {
            $tabArbo = explode('::>>::', $arbo);
            $tabCategMultiLg[$isoLg] = $tabArbo[$depth] ?? $categorie;
        }

        //$objCategorie = new Category(null, $id_lang, $id_shop);
        $objCategorie = new Category();
        $objCategorie->id_parent = $categParent ? (int) $categParent : Category::getRootCategory()->id_category;
        $objCategorie->id_shop_default = $id_shop;
        $objCategorie->name = array();
        $objCategorie->link_rewrite = array();
        //$link = Tools::link_rewrite($categorie);
        $languages = Language::getLanguages(false);
        foreach ($languages as $lang) {
            $name = $tabCategMultiLg[$lang['iso_code']] ?? $categorie;
            $objCategorie->name[$lang['id_lang']] = $name;
            $objCategorie->link_rewrite[$lang['id_lang']] = self::str2urlParanoid($name);
        }
        $objCategorie->save();
        if (!Cache::isStored('eci_shops_ids')) {
            Cache::store('eci_shops_ids', Shop::getCompleteListOfShopsID());
        }
        $eci_shops_ids = Cache::retrieve('eci_shops_ids');
        foreach ($eci_shops_ids as $idS) {
            if ((int) $idS != (int) $id_shop) {
                $objCategorie->deleteFromShop((int) $idS);
            }
        }
        //Category::cleanPositions($objCategorie->id_parent);
        //Category::regenerateEntireNtree();
        return $objCategorie->id;
    }

    public function setCategories(&$product, $product_ec_catalogue, $id_lang = 0, $iso_lang = 0)
    {
        if (!is_object($product)) {
            return;
        }
        if (!$id_lang) {
            $id_lang = Configuration::get('PS_LANG_DEFAULT');
        }
        if (!$iso_lang) {
            $iso_lang = Language::getIsoById($id_lang);
        }
        $p_data = self::arJsonDecodeRecur($product_ec_catalogue);
        $sw_associate_to_home = (bool) ($p_data['special']['associateToHome'] ?? true);
        $connecteur = self::getConnecteurName();

        if (!Cache::isStored('eci_root_id_for_shop_'.$this->id_shop)) {
            $root = Category::getRootCategory($id_lang);
            Cache::store('eci_root_id_for_shop_'.$this->id_shop, $root->id);
        }
        $root_id = Cache::retrieve('eci_root_id_for_shop_'.$this->id_shop);
        if (!Cache::isStored('ONLY_LAST_CATEGORY_' . $connecteur . '_' . $this->id_shop)) {
            Cache::store('ONLY_LAST_CATEGORY_' . $connecteur . '_' . $this->id_shop, self::getECIConfigValue('ONLY_LAST_CATEGORY', $this->id_shop, $connecteur));
        }
        $only_last_category = Cache::retrieve('ONLY_LAST_CATEGORY_' . $connecteur . '_' . $this->id_shop);

        //récupérer les chemins : si le category_group n'est pas utilisable, utiliser le category
        $ml_paths = self::categoryBranchesToPaths($product_ec_catalogue['category_group']);
        if (!$ml_paths) {
            $ml_paths = array($product_ec_catalogue['category']);
        }
        $listeCategoriesTo = array();
        foreach ($ml_paths as $id_path => $ml_path) {
            $pathLg = self::langExpl($ml_path);
            if (empty($pathLg[$iso_lang])) {
                $id_category = $root_id;
            } else {
                $id_category = $this->chercheOuCreeCategorie(
                    $ml_path,
                    $pathLg[$iso_lang],
                    $product_ec_catalogue['fournisseur'],
                    0,
                    $this->id_shop,
                    $id_lang
                );
            }

            $listeCategoriesTo[] = $id_category;

            if (0 == $id_path) {
                $product->id_category_default = $id_category;
                if ($only_last_category) {
                    break;
                }
            }

            $objCategoryDefault = new Category($id_category, null, $this->id_shop);
            foreach ($objCategoryDefault->getParentsCategories() as $tabcat) {
                if (!$sw_associate_to_home && $tabcat['id_category'] == $root_id) {
                    break;
                }
                $listeCategoriesTo[] = (int) $tabcat['id_category'];
            }
        }

        return array_unique($listeCategoriesTo);
    }

    public function setProductToShop($tab)
    {
        $db = Db::getInstance();
        $reference = $tab['reference'];
        $id_shop = $tab['id_shop'];
        $fournisseur = $tab['fournisseur'];
        $logger = self::logStart(__FUNCTION__);
        $startTime = microtime(true);

        $thisIdLang = Configuration::get('PS_LANG_DEFAULT');
        $thisIsoLg = Language::getIsoById($thisIdLang);
        if ($id_shop != $this->id_shop) {
            Shop::setContext(Shop::CONTEXT_SHOP, $id_shop);
            Context::getContext()->shop = new Shop((int) $id_shop, (int) $thisIdLang);
        }
        $id_supplier = self::getEciFournId($fournisseur);

        if (empty($id_supplier)) {
            $id_suppl = self::getNewSupplierId($fournisseur);
            $id_supplier = !empty($id_suppl) ? $id_suppl : 0;
        }
        $product_ec_catalogue = $db->getRow(
            'SELECT *
            FROM ' . _DB_PREFIX_ . 'eci_catalog
            WHERE product_reference = "' . pSQL($reference) . '"
            AND fournisseur="' . pSQL($fournisseur) . '"'
        );
        if (!$product_ec_catalogue) {
            return false;
        }

        $id_produit = $db->getValue(
            'SELECT id_product
            FROM ' . _DB_PREFIX_ . 'eci_product_shop
            WHERE reference = "' . pSQL($product_ec_catalogue['product_reference']) . '"
            AND fournisseur = "' . pSQL($fournisseur) .'"'
        );
        if (empty($id_produit)) {
            /* Nouveau produit */
            $product = new Product();
            $product->id_supplier = $id_supplier;
            $product->id_manufacturer = self::getManufacturer($product_ec_catalogue['manufacturer']);
            $product->id_shop_default = $id_shop;
            $product->ean13 = trim($product_ec_catalogue['ean13']);
            $product->upc = trim($product_ec_catalogue['upc']);
            $product->reference = $product_ec_catalogue['reference'];
            $product->supplier_reference = $product_ec_catalogue['product_reference'];
            if (!is_null($dims = Tools::jsonDecode($product_ec_catalogue['dimensions'], true))) {
                $product->width = !empty($dims['Lo']) ? (float) $dims['Lo'] : 0;
                $product->height = !empty($dims['Hi']) ? (float) $dims['Hi'] : 0;
                $product->depth = !empty($dims['La']) ? (float) $dims['La'] : 0;
            } else {
                $product->width = 0;
                $product->height = 0;
                $product->depth = 0;
            }
            $product->weight = (float) $product_ec_catalogue['weight'];
            $product->date_add = date('Y-m-d H:i:s');
        } else {
            /* Produit déjà présent */
            /* si déjà dans ce shop : nettoyage table imported + sortie propre */
            $in_shop = true;
            if (Configuration::get('PS_MULTISHOP_FEATURE_ACTIVE')) {
                $in_shop = (bool) $db->getValue(
                    'SELECT COUNT(*)
                    FROM ' . _DB_PREFIX_ . 'product_shop
                    WHERE id_product = ' . (int) $id_produit . '
                    AND id_shop = ' . (int) $id_shop
                );
            }
            if ($in_shop) {
                $db->delete(
                    'eci_product_imported',
                    'reference="' . pSQL($reference) . '" AND id_shop=' . (int) $id_shop . ' AND fournisseur="' . pSQL($fournisseur) . '"'
                );
                return true;
            } else {
                $product = new Product((int) $id_produit, false, null, $id_shop, null);
                $product->associateTo($id_shop);
            }
        }

        $id_tax_rules_group = $this->getTaxMatched($product_ec_catalogue['rate']);
        if ($id_tax_rules_group) {
            $product->id_tax_rules_group = (int) $id_tax_rules_group;
        } else {
            $product->id_tax_rules_group = 1;
        }

        $product->ecotax = (float) $product_ec_catalogue['ecotax'];
        $product->minimal_quantity = 1;
        $pricerulesClass = 'Pricerules' . $fournisseur;
        $calculatedPmvc = $pricerulesClass::calculatePriceRules(
            $reference,
            $fournisseur,
            $id_shop,
            (float) $product_ec_catalogue['price'],
            (float) $product_ec_catalogue['pmvc']
        );
        $product->price = (float) $calculatedPmvc;
        $product->wholesale_price = (float) $product_ec_catalogue['price'];
        $product->additional_shipping_cost = (float) $product_ec_catalogue['ship'];
        $active = (int) self::getECIConfigValue('PARAM_NEWPRODUCT', $id_shop, $fournisseur);
        $product->active = $active;
        $product->redirect_type = '404';
        $product->available_for_order = 1;
        $product->show_price = 1;
        if (($PS_ADVANCED_STOCK_MANAGEMENT = Configuration::get('PS_ADVANCED_STOCK_MANAGEMENT'))) {
            $product->advanced_stock_management = 1;
            $product->depends_on_stock = 0;
        }

        $list_categories = $this->setCategories($product, $product_ec_catalogue, $thisIdLang, $thisIsoLg);

        $product->date_upd = date('Y-m-d H:i:s');

        //default values to prevent errors in PS1.6
        $product->name[(int) $thisIdLang] = '1';
        $product->link_rewrite[(int) $thisIdLang] = '1';
        //will be overwritten by SEO

        try {
            $beforeSaveTime = microtime(true);
            $product->save();
            $afterSaveTime = microtime(true);
        } catch (Exception $e) {
            self::logException($logger, $e, __FUNCTION__ . ' error when product->save for ref "' . $reference .'"', Info::ERROR, true);
        }
        if (empty($product->id)) {
            return false;
        }
        
        $is_pack = self::isPack($reference, $fournisseur);
        self::setEciProductShop(
            array(
                'reference' => $product->supplier_reference,
                'is_pack' => (bool) $is_pack,
                'keep' => $product_ec_catalogue['keep'],
            ),
            array(
                'id_product' => $product->id,
            ),
            $id_shop,
            $fournisseur
        );

        if (!$active) {
            self::updateProductShopActReasons($product->id, $fournisseur, $id_shop, 'user', false);
        }

        if ($list_categories) {
            $product->addToCategories($list_categories);
        }

        if ($PS_ADVANCED_STOCK_MANAGEMENT) {
            $PS_DEFAULT_WAREHOUSE_NEW_PRODUCT = Configuration::get('PS_DEFAULT_WAREHOUSE_NEW_PRODUCT');
            if (!empty($PS_DEFAULT_WAREHOUSE_NEW_PRODUCT)) {
                try {
                    $warehouse_location_entity = new WarehouseProductLocation();
                    $warehouse_location_entity->id_product = $product->id;
                    $warehouse_location_entity->id_product_attribute = 0;
                    $warehouse_location_entity->id_warehouse = $PS_DEFAULT_WAREHOUSE_NEW_PRODUCT;
                    $warehouse_location_entity->location = pSQL('');
                    $warehouse_location_entity->save();
                } catch (Exception $e) {
                    self::logInfo(
                        $logger,
                        'pid ' . $product->id
                        . ' ne peut être mis dans le warehouse ' . $PS_DEFAULT_WAREHOUSE_NEW_PRODUCT
                        . ' : ' . $e->getMessage()
                    );
                }
            }
        }

        if (!empty($product_ec_catalogue['special']) && !is_null($special = json_decode($product_ec_catalogue['special'], true)) && is_array($special)) {
            self::setProductSpecial($product, $special);
        }
        
        if (isset($product_ec_catalogue['carriers'])) {
            // vérifier la structure des données
            if (!is_null($carriers = Tools::jsonDecode($product_ec_catalogue['carriers'], true))) {
                if (is_array($carriers) && isset($carriers['replace']) && isset($carriers['list'])) {
                    // récupérer les carriers actuels
                    $list_carriers = array();
                    if (!$carriers['replace']) {
                        $list_used_carriers = $product->getCarriers();
                        foreach ($list_used_carriers as $used_carrier) {
                            $list_carriers[] = $used_carrier['id_reference'];
                        }
                    }
                    // ajouter les carriers demandés s'ils existent
                    if (is_array($carriers['list'])) {
                        foreach ($carriers['list'] as $carrier_name) {
                            $carrier_id_reference = $db->getValue(
                                'SELECT id_reference
                                FROM ' . _DB_PREFIX_ . 'carrier
                                WHERE name = "' . pSQL($carrier_name) . '"
                                AND deleted = 0'
                            );
                            if ($carrier_id_reference && !in_array($carrier_id_reference, $list_carriers)) {
                                $list_carriers[] = $carrier_id_reference;
                            }
                        }
                        // associer les carriers
                        $product->setCarriers($list_carriers);
                    }
                }
            }
        }

        //supplier's carriers if selected
        if (self::getECIConfigValue('ASSOC_CARRIERS', $id_shop, $fournisseur)) {
            $scClass = 'Suppcarrier' . $fournisseur;
            $list_carriers = array();
            $list_suppcarriers = $scClass::getSuppcarriers($fournisseur);
            foreach ($list_suppcarriers as $suppcarrier) {
                $carrier_id_reference = (int) $db->getValue(
                    'SELECT id_reference
                    FROM ' . _DB_PREFIX_ . 'carrier
                    WHERE id_carrier = ' . (int) $suppcarrier['id_carrier']
                );
                if ($carrier_id_reference && !in_array($carrier_id_reference, $list_carriers)) {
                    $list_carriers[] = $carrier_id_reference;
                }
            }
            if ($list_carriers) {
                $list_used_carriers = $product->getCarriers();
                foreach ($list_used_carriers as $used_carrier) {
                    $list_carriers[] = $used_carrier['id_reference'];
                }
                $product->setCarriers($list_carriers);
            }
        }

        if (!empty($product_ec_catalogue['documents']) && is_array($documents = json_decode($product_ec_catalogue['documents'], true))) {
            self::setDocuments($documents, $product->id);
        }

        if (empty($id_produit)) {
            // images from json rich infos or old style list of urls
            if (($tab_images = Tools::jsonDecode($product_ec_catalogue['pictures'], true)) === null) {
                $tab_images = explode('//:://', $product_ec_catalogue['pictures']);
            }
            if (!empty($tab_images[0])) {
                Cache::clean('*');
                self::setImages($product, $tab_images);
            }
        } else {
            //asociate linked objects to this shop if they are shopped
            $this->associateImagesToShops($product->id, $id_shop);
        }
        
        $beforeAttributeBlockTime = microtime(true);
        $this->setAttributes($product, $fournisseur, $id_shop);
        $this->setFeatures($product, $product_ec_catalogue['feature'], $fournisseur, $id_shop);
        $this->updateProductSEO($product);
        $afterAttributeBlockTime = microtime(true);

        if (empty($id_produit)) {
            //create linked objects if they are new or not shopped
            // register reference of product in product_supplier table
            $ps_currecy_default = Configuration::get('PS_CURRENCY_DEFAULT');
            $product->addSupplierReference(
                (int) $product->id_supplier,
                0,
                $product->supplier_reference,
                (float) $product->wholesale_price,
                $ps_currecy_default
            );
            // if multisupplier : register references for every supplier in product_supplier table
            if (!empty($special['multifournisseur'])) {
                foreach ($special['multifournisseur'] as $supplier_name => $data_mf) {
                    $id_supplier_mf = self::getEciFournId($supplier_name);
                    if ($id_supplier_mf && is_array($data_mf) && array_key_exists('parent', $data_mf)) {
                        if (is_array($data_mf['parent'])) {
                            foreach ($data_mf['parent'] as $ref_parent => $wprice) {
                                $product->addSupplierReference(
                                    (int) $id_supplier_mf,
                                    0,
                                    $ref_parent,
                                    (float) $wprice,
                                    $ps_currecy_default
                                );
                                break;
                            }
                        }
                    }
                }
            }
        }

        $beforeHookTime = microtime(true);
        try {
            Hook::exec('actionProductSave', array('id_product' => (int) $product->id, 'product' => $product));
        } catch (Exception $e) {
            self::logException($logger, $e, __FUNCTION__ . ' error when hook actionProductSave for pid' . $product->id, Info::ERROR, true);
        }
        $afterHookTime = microtime(true);
        Search::indexation(false, $product->id);
        
        if (self::LOG_PRODUCT_CREATION_TIME) {
            self::logInfo(
                $logger,
                'Product "'.$reference.'" of supplier '.$fournisseur
                . ' successfully set to shop '.$id_shop.'.'
                . '<br> Times :'
                . '<br> beforesave : '.($beforeSaveTime - $startTime).'s'
                . '<br> save : '.($afterSaveTime - $beforeSaveTime).'s'
                . '<br> attribute block : '.($afterAttributeBlockTime - $beforeAttributeBlockTime).'s'
                . '<br> save to hook : '.($beforeHookTime - $afterSaveTime).'s'
                . '<br> hook : '.($afterHookTime - $beforeHookTime).'s'
                . '<br> total : '.($afterHookTime - $startTime).'s'
            );
        }

        return true;
    }

    public function updateProductSEO($prod)
    {
        if (!is_object($prod)) {
            return false;
        }

        $connecteur = self::getConnecteurName();
        $thisIdLang = Configuration::get('PS_LANG_DEFAULT');
        $thisIsoLg = Language::getIsoById($thisIdLang);
        $languages = Language::getLanguages(false);
        $data = self::getProduct($prod->supplier_reference);
        $categ = new Category($prod->id_category_default);
        $manu = new Manufacturer($prod->id_manufacturer);

        $class = 'Seo' . $connecteur;
        $seo = $class::getValuesStatic($connecteur, $this->id_shop);

        $sizeLimit = array(
            'name' => Product::$definition['fields']['name']['size'],
            //'description' => $short_desc_limit,
            'description_short' => Configuration::get('PS_PRODUCT_SHORT_DESC_LIMIT'),
            'meta_title' => Product::$definition['fields']['meta_title']['size'],
            'meta_description' => Product::$definition['fields']['meta_description']['size'],
            'link_rewrite' => Product::$definition['fields']['link_rewrite']['size'],
            'meta_keywords' => Product::$definition['fields']['meta_keywords']['size'],
        );
        $eciName = self::langExpl($data['produit']['name']);
        $eciDesc = self::langExpl($data['produit']['description']);
        $eciShortDesc = self::langExpl($data['produit']['short_description']);

        $fields_to_update = [];
        foreach ($languages as $language) {
            $isoL = $language['iso_code'];
            $idL = $language['id_lang'];

            foreach ($seo as $field => $tabLangSeo) {
                $cleanName = self::nettoyeNom((isset($eciName[$isoL]) ? $eciName[$isoL] : (isset($eciName[$thisIsoLg]) ? $eciName[$thisIsoLg] : '')));
                $cleanUrl = self::str2urlParanoid($cleanName);
                $field = str_replace('seo_', '', $field);

                $seoData = str_replace(
                    array(
                        '{name}',
                        '{description}',
                        '{description_short}',
                        '{meta_title}',
                        '{meta_description}',
                        '{ref}',
                        '{manufacturer}',
                        '{categ}',
                        '{categPS}',
                        '{manufacturerPS}',
                    ),
                    array(
                        $cleanName,
                        $eciDesc[$isoL] ?? $eciDesc[$thisIsoLg] ?? '',
                        $eciShortDesc[$isoL] ?? $eciShortDesc[$thisIsoLg] ?? '',
                        $cleanName,
                        $eciName[$isoL] ?? $eciName[$thisIsoLg] ?? '',
                        $data['produit']['reference'],
                        $data['produit']['manufacturer'],
                        $data['produit']['category_group'][0][0][$isoL] ?? $data['produit']['category_group'][0][0][$thisIsoLg] ?? '',
                        $categ->name[$idL],
                        $manu->name,
                    ),
                    $tabLangSeo[$idL]
                );

                $matches = array();
                if (!empty($prod->cache_default_attribute) && preg_match_all('/{attrPS: (.*?)}/', $seoData, $matches)) {
                    $combi = new Combination($prod->cache_default_attribute);
                    $dataCombi = $combi->getAttributesName($idL);
                    foreach ($dataCombi as $lineCombi) {
                        $attr = new Attribute($lineCombi['id_attribute']);
                        $attrGroup = new AttributeGroup($attr->id_attribute_group);
                        foreach ($matches[1] as $i => $match) {
                            if (array_search($match, $attrGroup->name)) {
                                $seoData = str_replace($matches[0][$i], $attr->name[$idL], $seoData);
                            }
                        }
                    }
                }
                if (preg_match_all('/{attrPS: (.*?)}/', $seoData, $matches)) {
                    foreach ($matches[0] as $match) {
                        $seoData = str_replace($match, '', $seoData);
                    }
                }

                if (preg_match_all('/{caracPS: (.*?)}/', $seoData, $matches)) {
                    $tabFeat = $prod->getFeatures();
                    foreach ($tabFeat as $dataFeat) {
                        $feat = new Feature($dataFeat['id_feature']);
                        foreach ($matches[1] as $i => $match) {
                            if (array_search($match, $feat->name)) {
                                $featValue = new FeatureValue($dataFeat['id_feature_value']);
                                $seoData = str_replace($matches[0][$i], $featValue->value[$idL], $seoData);
                            }
                        }
                    }
                }
                if (preg_match_all('/{caracPS: (.*?)}/', $seoData, $matches)) {
                    foreach ($matches[0] as $match) {
                        $seoData = str_replace($match, '', $seoData);
                    }
                }

                $seoData_trimmed = trim($seoData);
                if (empty($sizeLimit[$field])) {
                    $prod->$field[$idL] = $seoData_trimmed;
                } else {
                    $prod->$field[$idL] = self::getStrCutBytes($seoData_trimmed, $sizeLimit[$field]);
                }
                $fields_to_update[$field][$idL] = true;
            }
            $prod->meta_keywords[$idL] = $cleanName;
            $fields_to_update['meta_keywords'][$idL] = true;
            $prod->link_rewrite[$idL] = $cleanUrl;
            $fields_to_update['link_rewrite'][$idL] = true;
        }

        try {
            $prod->setFieldsToUpdate($fields_to_update);
            $prod->update();
        } catch (Exception $e) {
            self::logInfo(self::logStart(__FUNCTION__), 'Error update pid' . $prod->id . ' : in file ' . $e->getFile() . ' line ' . $e->getLine() . ' : ' . $e->getMessage());
        }

        return true;
    }
    
    public static function setProductSpecial($product, $special, $update = false)
    {
        if (!is_object($product)) {
            return false;
        }
        $product->updated = false;
        if (!is_array($special)) {
            return false;
        }
        
        $fields_to_update = [];
        foreach ($special as $prop => $val) {
            if ('tags' == $prop) {
                continue;
            } elseif ('out_of_stock' == $prop) {
                StockAvailable::setProductOutOfStock($product->id, (int) $val);
            } elseif (property_exists($product, $prop)) {
                if (is_array($val)) {
                    $tab_prop = array();
                    foreach ($val as $iso => $txt) {
                        if (Validate::isLangIsoCode($iso)) {
                            $idLg = Language::getIdByIso($iso);
                            if ($idLg) {
                                $tab_prop[(int) $idLg] = $txt;
                                $fields_to_update[$prop][(int) $idLg] = true;
                            }
                        }
                    }
                    $product->$prop = $tab_prop;
                    $product->updated |= true;
                } else {
                    $product->$prop = $val;
                    $fields_to_update[$prop] = true; //attention pourrait planter pour les valeurs languées
                    $product->updated |= true;
                }
            }
        }
        
        if (isset($special['tags']) && is_array($special['tags'])) {
            Tag::deleteTagsForProduct($product->id);
            $sep = ',';
            foreach ($special['tags'] as $iso => $tags) {
                if (Validate::isLangIsoCode($iso)) {
                    $idLg = Language::getIdByIso($iso);
                    if ($idLg) {
                        if (is_array($tags)) {
                            $tab_tags = $tags;
                        } else {
                            $tab_tags = array($tags);
                        }
                        $old_tags = Tag::getProductTags($product->id);
                        foreach ($tab_tags as $id => $tag) {
                            if (isset($old_tags[$idLg]) && in_array($tag, $old_tags[$idLg])) {
                                unset($tab_tags[$id]);
                            }
                        }
                        $lst_tags = implode($sep, $tab_tags);
                        if ($lst_tags) {
                            Tag::addTags($idLg, $product->id, $lst_tags, $sep);
                        }
                    }
                }
            }
        }

        if (isset($special['accessory'])) {
            $accessories = $special['accessory'];
            if (is_array($accessories)) {
                $id_accessories = array();
                $id_supplier = self::getEciFournId(self::getConnecteurName());
                foreach ($accessories as $ref_accessory) {
                    $ids = new ImporterReference($ref_accessory, $id_supplier);
                    if ($ids->id_product && ($ids->id_product != $product->id)) {
                        $id_accessories[] = $ids->id_product;
                    }
                }
                if ($id_accessories) {
                    Product::changeAccessoriesForProduct($id_accessories, $product->id);
                    self::accessoryDedup($product->id);
                }
            }
        }
        
        if (isset($special['customization'])) {
            $cust_fields = $special['customization'];
            $old_customizable = $product->customizable;
            if (is_array($cust_fields)) {
                $product->deleteCustomization();
                foreach ($cust_fields as $cust_field) {
                    if (isset($cust_field['name'])) {
                        $new_customization_field = new CustomizationField();
                        $new_customization_field->id_product = $product->id;
                        $new_customization_field->type = $cust_field['type'] ?? Product::CUSTOMIZE_TEXTFIELD;
                        $new_customization_field->required = $cust_field['required'] ?? 0;
                        $new_customization_field->name = self::getMultiLangField($cust_field['name']);
                        $new_customization_field->add();
                        $product->customizable = 1;
                    }
                }
            }
            if ($old_customizable != $product->customizable) {
                $product->updated = true;
                $fields_to_update['customizable'] = true;
            }
        } else {
            $old_customizable = $product->customizable;
            $product->customizable = 0;
            $product->deleteCustomization();
            if ($old_customizable != $product->customizable) {
                $product->updated = true;
                $fields_to_update['customizable'] = true;
            }
        }
        
        if ($fields_to_update) {
            $product->setFieldsToUpdate($fields_to_update);
        }
        
        if ($update && $product->updated) {
            try {
                $product->update();
            } catch (Exception $e) {
                self::logInfo(self::logStart(__FUNCTION__), 'Product ' . $product->id . ' could not be updated : ' . $e->getMessage());
                return false;
            }
        }
        
        return true;
    }
    
    public static function setAttributeSpecial($combination, $special, $update = false)
    {
        if (!is_object($combination)) {
            return false;
        }
        $combination->updated = false;
        if (!is_array($special)) {
            return false;
        }
        
        $fields_to_update = [];
        foreach ($special as $prop => $val) {
            if (property_exists($combination, $prop)) {
                if ($combination->$prop != $val) {
                    $combination->$prop = $val;
                    $combination->updated = true;
                    $fields_to_update[$prop] = true;
                }
            }
        }
        
        if ($fields_to_update) {
            $combination->setFieldsToUpdate($fields_to_update);
        }
        
        if ($update && $combination->updated) {
            try {
                $combination->update();
            } catch (Exception $e) {
                self::logInfo(self::logStart(__FUNCTION__), 'Combination ' . $combination->id . ' could not be updated : ' . $e->getMessage());
                return false;
            }
        }
        
        return true;
    }

    public static function accessoryDedup($id_product)
    {
        $list_accessories = Db::getInstance()->executeS(
            'SELECT *
            FROM ' . _DB_PREFIX_ . 'accessory
            WHERE id_product_1 = ' . (int) $id_product
        );
        if (!$list_accessories) {
            return true;
        }

        Db::getInstance()->delete('accessory', 'id_product_1 = ' . (int) $id_product);

        $new_list = self::uniqueMultidimArray($list_accessories, 'id_product_2');

        Db::getInstance()->insert('accessory', $new_list);

        return true;
    }

    public static function accessoryDelete($id_product)
    {
        return Db::getInstance()->delete('accessory', 'id_product_1 = ' . (int) $id_product);
    }

    public static function accessoryDeleteFrom($id_product)
    {
        return Db::getInstance()->delete('accessory', 'id_product_2 = ' . (int) $id_product);
    }

    public static function getCurrConvFactor($fournisseur = null)
    {
        if (is_null($fournisseur)) {
            $fournisseur = self::getConnecteurName();
        }

        $key = __FUNCTION__ . '_' . $fournisseur;
        if (!Cache::isStored($key)) {
            if (!Currency::isMultiCurrencyActivated()) {
                $convFactor = 1;
            } else {
                $four_curr_iso = self::getECIConfigValue('CURRENCY', 0, $fournisseur);
                if (!$four_curr_iso) {
                    $four_curr_iso = 'EUR';
                }
                $four_curr_id = Currency::getIdByIsoCode($four_curr_iso);
                if (!$four_curr_id) {
                    $convFactor = 1;
                } else {
                    $four_curr = new Currency($four_curr_id);
                    $convFactor = round((1 / $four_curr->conversion_rate), 6);
                }
            }

            Cache::store($key, $convFactor);
        }

        return Cache::retrieve($key);
    }

    public static function getManufacturer($manufacturer_name)
    {
        if (!$manufacturer_name) {
            return false;
        }
        
        $id_manufacturer = self::getManufacturerMatched($manufacturer_name);
        if (!$id_manufacturer) {
            $id_manufacturer = Manufacturer::getIdByName($manufacturer_name);
            if (!$id_manufacturer) {
                $manufacturer = new Manufacturer();
                $manufacturer->name = $manufacturer_name;
                $manufacturer->active = true;
                $manufacturer->save();
                $id_manufacturer = $manufacturer->id;
            }
        }
        
        return $id_manufacturer;
    }

    public static function setDocuments($documents, $id_product = null)
    {
        $logger = self::logStart(__FUNCTION__);

        if (!is_array($documents)) {
            return false;
        }

        $thisIdLang = Configuration::get('PS_LANG_DEFAULT');

        if (isset($documents['url'])) {
            $documents = array($documents);
        }

        $idsA = array();

        foreach ($documents as $document) {
            if (!is_array($document)) {
                continue;
            }
            if (empty($document['url']) || empty($document['name'])) {
                continue;
            }

            $content = Tools::file_get_contents($document['url']);
            if (false == $content) {
                continue;
            }
            $file = md5(uniqid());
            file_put_contents(_PS_ROOT_DIR_ . '/download/' . $file, $content);

            if (!empty($document['mime'])) {
                $mime = $document['mime'];
            } elseif (function_exists('finfo_open')) {
                $finfo = finfo_open(FILEINFO_MIME_TYPE);
                $mime = finfo_file($finfo, _PS_ROOT_DIR_ . '/download/' . $file);
                finfo_close($finfo);
            } else {
                $mime = 'application/octet-stream';
            }

            if (!empty($document['file_name'])) {
                $file_name = $document['file_name'];
            } else {
                $file_name = preg_replace('#.*/(.*?)#', '$1', self::decodeStr($document['url']));
            }

            $attach = new Attachment();
            $attach->file = $file;
            $attach->file_name = $file_name;
            $attach->mime = $mime;

            $name = array();
            if (is_array($document['name'])) {
                foreach ($document['name'] as $isoLg => $nameLg) {
                    if (Validate::isLanguageIsoCode($isoLg)) {
                        $name[Language::getIdByIso($isoLg)] = self::getStrCutBytes($nameLg, 32);
                    } elseif (is_numeric($isoLg) && $isoLg) {
                        $name[(int) $isoLg] = self::getStrCutBytes($nameLg, 32);
                    }
                }
            } else {
                $name[$thisIdLang] = self::getStrCutBytes($document['name'], 32);
            }
            $attach->name = $name;

            $description = array();
            if (is_array($document['description'])) {
                foreach ($document['description'] as $isoLg => $descriptionLg) {
                    if (Validate::isLanguageIsoCode($isoLg)) {
                        $description[Language::getIdByIso($isoLg)] = self::getStrCutBytes($descriptionLg, 32);
                    } elseif (is_numeric($isoLg) && $isoLg) {
                        $description[(int) $isoLg] = self::getStrCutBytes($descriptionLg, 32);
                    }
                }
            } else {
                $description[$thisIdLang] = self::getStrCutBytes($document['description'], 32);
            }
            $attach->description = $description;

            try {
                if ($attach->add()) {
                    $idsA[] = $attach->id;
                }
            } catch (Exception $e) {
                self::logError($logger, $e->getMessage());
                continue;
            }
        }

        if ($idsA && is_numeric($id_product) && $id_product) {
            Attachment::attachToProduct((int) $id_product, $idsA);
            return true;
        }

        return $idsA;
    }

    public static function getNewSupplierId($SupplierName)
    {
        if (empty($SupplierName)) {
            return false;
        }

        $id_supplier = self::getEciFournId($SupplierName);

        if (!empty($id_supplier)) {
            return $id_supplier;
        }

        if (self::getInfoFourn('name', $SupplierName)) {
            $supplier = new Supplier();
            $supplier->name = $SupplierName;
            $supplier->active = true;
            $supplier->save();
            $supplier->associateTo(array_values(Shop::getShops(true, null, true)));
            Db::getInstance()->update('eci_fournisseur', array('id_manufacturer' => (int) $supplier->id), 'name = "' . pSQL($SupplierName) . '"');
        } else {
            return false;
        }

        return $supplier->id;
    }
    
    public function setFeatures($product, $featureList, $fournisseur = '', $id_shop = 1)
    {
        if (!$featureList) {
            return;
        }

        Shop::setContext(Shop::CONTEXT_SHOP, $id_shop);
        Context::getContext()->shop->id = $id_shop;
        $id_lang = Configuration::get('PS_LANG_DEFAULT');
        $iso_lang = Language::getIsoById($id_lang);
        $tab_lnv = self::parseAttAndFeat($featureList);

        foreach ($tab_lnv as $lnv) {
            $ids_feature = self::getFeatureMatched($lnv, $id_lang, $iso_lang, $id_shop, $fournisseur, true);
            if ($ids_feature && is_array($ids_feature) && !empty(reset($ids_feature))) {
                //$product->addFeaturesToDB($id_feature, $id_feature_value);
                $id_feature_value = reset($ids_feature);
                $id_feature = key($ids_feature);
                Db::getInstance()->insert(
                    'feature_product',
                    array(
                        'id_feature' => (int) $id_feature,
                        'id_product' => (int) $product->id,
                        'id_feature_value' => (int) $id_feature_value
                    ),
                    false,
                    false,
                    Db::INSERT_IGNORE
                );
            }
        }
    }

    public static function getFeatureMatched($lnv, $id_lang, $iso_lang, $id_shop, $fournisseur, $force_match = true, $no_value = false)
    {
        $nv = isset($lnv[$iso_lang]) ? $lnv[$iso_lang] : reset($lnv);
        $name = implode('', array_keys($nv));
        $value = implode('', $nv);

        $feature_match = Db::getInstance()->getRow(
            'SELECT s.id_feature_eco, s.id_feature
            FROM ' . _DB_PREFIX_ . 'eci_feature f
            LEFT JOIN ' . _DB_PREFIX_ . 'eci_feature_shop s
            ON f.id_feature_eco = s.id_feature_eco
            WHERE value = "' . pSQL($name) . '"
            AND fournisseur = "' . pSQL($fournisseur) . '"
            AND id_shop = ' . (int) $id_shop . '
            AND id_lang = ' . (int) $id_lang
        );
        $id_feature_eco = empty($feature_match['id_feature_eco']) ? 0 : $feature_match['id_feature_eco'];
        $id_feature = empty($feature_match['id_feature']) ? 0 : $feature_match['id_feature'];
        if (!$id_feature) {
            if (!$id_feature_eco) {
                $id_feature_eco = Db::getInstance()->getValue(
                    'SELECT id_feature_eco
                    FROM ' . _DB_PREFIX_ . 'eci_feature
                    WHERE value = "' . pSQL($name) . '"
                    AND id_lang = ' . (int) $id_lang . '
                    AND fournisseur = "' . pSQL($fournisseur) . '"'
                );
                if (!$id_feature_eco) {
                    Db::getInstance()->insert(
                        'eci_feature',
                        array(
                            'value' => pSQL($name),
                            'id_lang' => (int) $id_lang,
                            'fournisseur' => pSQL($fournisseur)
                        )
                    );
                    $id_feature_eco = Db::getInstance()->Insert_ID();
                }
            }
            $id_feature = Db::getInstance()->getValue(
                'SELECT id_feature
                FROM ' . _DB_PREFIX_ . 'feature_lang
                WHERE id_lang = ' . (int) $id_lang . '
                AND name = "' . pSQL($name) . '"'
            );
            if ($force_match) {
                if (!$id_feature) {
                    $newFeature = new Feature();
                    $ml_name = self::createMultiLangField(array_map('implode', array_map('array_keys', $lnv)));
                    $newFeature->name = $ml_name;
                    $newFeature->add();
                    $newFeature->associateTo(Shop::getCompleteListOfShopsID());
                    $id_feature = $newFeature->id;
                }
                Db::getInstance()->insert(
                    'eci_feature_shop',
                    array(
                        'id_feature_eco' => (int) $id_feature_eco,
                        'id_feature' => (int) $id_feature,
                        'id_shop' => (int) $id_shop
                    ),
                    false,
                    false,
                    Db::ON_DUPLICATE_KEY
                );
            }
        }

        $feature_value_match = Db::getInstance()->getRow(
            'SELECT s.id_feature_eco_value, s.id_feature
            FROM ' . _DB_PREFIX_ . 'eci_feature_value f
            LEFT JOIN ' . _DB_PREFIX_ . 'eci_feature_value_shop s
            ON f.id_feature_eco_value = s.id_feature_eco_value
            WHERE value = "' . pSQL($value) . '"
            AND f.id_feature_eco = ' . (int) $id_feature_eco . '
            AND s.id_shop = ' . (int) $id_shop . '
            AND id_lang = ' . (int) $id_lang
        );
        $id_feature_eco_value = empty($feature_value_match['id_feature_eco_value']) ? 0 : $feature_value_match['id_feature_eco_value'];
        $id_feature_value = empty($feature_value_match['id_feature']) ? 0 : $feature_value_match['id_feature'];
        if ($no_value) {
            return array($id_feature => $id_feature_value);
        }
        if (!$id_feature_value) {
            if (!$id_feature_eco_value) {
                $id_feature_eco_value = Db::getInstance()->getValue(
                    'SELECT id_feature_eco_value
                    FROM ' . _DB_PREFIX_ . 'eci_feature_value
                    WHERE value = "' . pSQL($value) . '"
                    AND id_feature_eco = ' . (int) $id_feature_eco . '
                    AND id_lang = ' . (int) $id_lang . '
                    AND fournisseur = "' . pSQL($fournisseur) . '"'
                );
                if (!$id_feature_eco_value) {
                    Db::getInstance()->insert(
                        'eci_feature_value',
                        array(
                            'value' => pSQL($value),
                            'id_feature_eco' => (int) $id_feature_eco,
                            'id_lang' => (int) $id_lang,
                            'fournisseur' => pSQL($fournisseur)
                        )
                    );
                    $id_feature_eco_value = Db::getInstance()->Insert_ID();
                }
            }
            $id_feature_value = Db::getInstance()->getValue(
                'SELECT l.id_feature_value
                FROM ' . _DB_PREFIX_ . 'feature_value_lang l
                LEFT JOIN ' . _DB_PREFIX_ . 'feature_value v
                ON l.id_feature_value = v.id_feature_value
                WHERE value = "' . pSQL($value) . '"
                AND id_feature = ' . (int) $id_feature . '
                AND id_lang = ' . (int) $id_lang
            );
            if ($force_match) {
                if (!$id_feature_value) {
                    $newFeatureValue = new FeatureValue();
                    $ml_value = self::createMultiLangField(array_map('implode', $lnv));
                    $newFeatureValue->value = $ml_value;
                    $newFeatureValue->id_feature = (int) $id_feature;
                    $newFeatureValue->add();
                    $id_feature_value = $newFeatureValue->id;
                }
                Db::getInstance()->insert(
                    'eci_feature_value_shop',
                    array(
                        'id_feature_eco_value' => (int) $id_feature_eco_value,
                        'id_feature' => (int) $id_feature_value,
                        'id_shop' => (int) $id_shop
                    ),
                    false,
                    false,
                    Db::ON_DUPLICATE_KEY
                );
            }
        }

        return array($id_feature => $id_feature_value);
    }

    public static function getAllLanguages()
    {
        $key = __NAMESPACE__ . '_' . __FUNCTION__;
        if (!Cache::isStored($key)) {
            $languages = Language::getLanguages(false);
            $tabLanguages = array();
            foreach ($languages as $language) {
                $tabLanguages[$language['iso_code']] = $language['id_lang'];
            }
            Cache::store($key, $tabLanguages);
        }

        return Cache::retrieve($key);
    }

    public static function createMultiLangField($field, $with_isos = false)
    {
        return self::getMultiLangField($field, $with_isos);
    }

    public static function getMultiLangField($field, $with_isos = false)
    {
        $languages = Language::getLanguages(false);
        $res = array();
        if (!is_array($field)) {
            foreach ($languages as $lang) {
                if ($with_isos) {
                    $res[$lang['iso_code']] = $field;
                } else {
                    $res[$lang['id_lang']] = $field;
                }
            }
        } else {
            $id_lang_default = Configuration::get('PS_LANG_DEFAULT');
            $iso_lang_default = Language::getIsoById($id_lang_default);
            $values = array_filter($field, array('self', 'isNotVoidString'));
            $default = $values[$iso_lang_default] ?? $values[$id_lang_default] ?? reset($values);
            foreach ($languages as $lang) {
                if (isset($field[$lang['iso_code']])) {
                    if ($with_isos) {
                        $res[$lang['iso_code']] = self::isNotVoidString($field[$lang['iso_code']]) ? $field[$lang['iso_code']] : $default;
                    } else {
                        $res[$lang['id_lang']] = self::isNotVoidString($field[$lang['iso_code']]) ? $field[$lang['iso_code']] : $default;
                    }
                } elseif (isset($field[$lang['id_lang']])) {
                    if ($with_isos) {
                        $res[$lang['iso_code']] = self::isNotVoidString($field[$lang['id_lang']]) ? $field[$lang['id_lang']] : $default;
                    } else {
                        $res[$lang['id_lang']] = self::isNotVoidString($field[$lang['id_lang']]) ? $field[$lang['id_lang']] : $default;
                    }
                } else {
                    if ($with_isos) {
                        $res[$lang['iso_code']] = $default;
                    } else {
                        $res[$lang['id_lang']] = $default;
                    }
                }
            }
        }

        return $res;
    }

    public static function contExpl($string)
    {
        // item sep = |:|:|         k => v sep = ::||::
        if (false === strstr($string, '::||::')) {
            if (is_null($res = Tools::jsonDecode($string, true))) {
                $res = array();
            }
        } else {
            $res = array();
            $string_exp = explode('|:|:|', str_replace('::||::|:|:|', '::||:: |:|:|', $string));
            foreach ($string_exp as $cont) {
                $tab = explode('::||::', $cont);
                if (isset($tab[0]) && isset($tab[1]) && $tab[0] != '' && $tab[1] != '') {
                    $res[$tab[0]] = $tab[1];
                }
            }
        }

        return $res;
    }

    public static function langExpl($string)
    {
        // item sep = //:://        k => v sep =:://::
        if (false === strstr($string, ':://::')) {
            if (is_null($res = Tools::jsonDecode($string, true))) {
                $res = array();
            }
        } else {
            $res = array();
            $string_exp = explode('//:://', str_replace(':://:://:://', ':://:: //:://', $string));
            if (count($string_exp) > 0) {
                foreach ($string_exp as $cont) {
                    $tab = explode(':://::', $cont);
                    if (!empty($tab[0]) && !empty($tab[1])) {
                        $res[$tab[0]] = $tab[1];
                    }
                }
            }
        }

        return $res;
    }

    public static function parseAttAndFeat($field)
    {
        if (is_null($names_values_multilang = Tools::jsonDecode($field, true))) {
            if (preg_match_all('/::\/\/::/', $field) === preg_match_all('/::\|\|::/', $field)) {
                $names_values_multilang = array_map(array('self', 'langExpl'), explode('|:|:|', $field));
                foreach ($names_values_multilang as &$lnv) {
                    foreach ($lnv as $iso => &$nv) {
                        $tnv = explode('::||::', $nv);
                        $nv = array($tnv[0] => $tnv[1]);
                    }
                }
            } else {
                $tlnv = array_map(array('self', 'contExpl'), self::langExpl($field));
                $names_values_multilang = array();
                foreach ($tlnv as $iso => $tnv) {
                    $idf = 0;
                    foreach ($tnv as $name => $value) {
                        $names_values_multilang[$idf][$iso] = array($name => $value);
                        $idf++;
                    }
                }
            }
        }

        return $names_values_multilang;
    }

    public function setAttributes($product, $fournisseur, $id_shop)
    {
        $attributes = Db::getInstance()->executeS(
            'SELECT *
            FROM ' . _DB_PREFIX_ . 'eci_catalog_attribute
            WHERE product_reference = "' . pSQL($product->supplier_reference) . '"
            AND fournisseur = "' . pSQL($fournisseur) . '"
            ORDER BY attribute'
        );
        $list_ids = array();
        foreach ($attributes as $attribute) {
            if (($idPa = $this->setAttribute($product, $attribute, $id_shop, empty($list_ids)))) {
                $list_ids[] = $idPa;
            }
        }
        $product->checkDefaultAttributes();

        return $list_ids;
    }

    public function setAttribute($product, $attribute, $id_shop, $default = false)
    {
        $logger = self::logStart(__FUNCTION__);
        $fournisseur = $attribute['fournisseur'];
        $id_lang = Configuration::get('PS_LANG_DEFAULT');
        $iso_lang = Language::getIsoById($id_lang);
        $tab_lnv = is_array($attribute['attribute']) ? $attribute['attribute'] : self::parseAttAndFeat($attribute['attribute']);
        $attribute['attribute'] = json_encode($tab_lnv);
        $attribute['keep'] = is_array($attribute['keep']) ? json_encode($attribute['keep']) : $attribute['keep'];
        $attribute['special'] = is_array($attribute['special']) ? json_encode($attribute['special']) : $attribute['special'];

        $matched_attributes = self::getAttributesMatched($tab_lnv, $id_lang, $iso_lang, $id_shop, $fournisseur, $attribute['product_reference']);

        $combinationAttributes = array_map('implode', $matched_attributes);
        if (empty($combinationAttributes)) {
            return false;
        }

        $supplier_reference = $attribute['reference_attribute'];
        $reference = empty($attribute['reference']) ? $supplier_reference : $attribute['reference'];

        $id_product_attribute = $product->productAttributeExists($combinationAttributes, false, null, true, true);
        //combination exists in another shop
        if ($id_product_attribute) {
            //exit if this combination of atributes is known with another reference
            $combination = new Combination((int) $id_product_attribute, null, $id_shop);
            if (!empty($combination->supplier_reference) && $combination->supplier_reference != $supplier_reference) {
                return false;
            }
            //associate combination, attributes and values to this shop
            $this->associateAttributesToShops($matched_attributes, $id_shop);
            $combination->associateTo($id_shop);
            $combination->id_product = $product->id;
            $combination->reference = $reference;
            $combination->ean13 = $attribute['ean13'];
            $combination->upc = $attribute['upc'];
            $combination->wholesale_price = $attribute['price'];
            $combination->price = $attribute['pmvc'];
            $combination->unit_price_impact = 0;
            $combination->ecotax = $attribute['ecotax'];
            $combination->minimal_quantity = 1;
            $combination->weight = $attribute['weight'];
            $combination->default_on = (bool) $default;
            $combination->low_stock_alert = 0;
        } else {
            //new combination
            $id_product_attribute = $product->addAttribute(
                (float) $attribute['pmvc'],             //price
                (float) $attribute['weight'],           //weight
                (float) 0,                              //unit price impact
                (float) $attribute['ecotax'],           //ecotax
                array(),                                //image ids
                $reference,                             //reference
                $attribute['ean13'],                    //ean13
                (bool) $default,                        //default attribute
                null,                                   //location
                null,                                   //upc
                1,                                      //minimal_quantity
                array(),                                //id_shop_list
                null,                                   //available_date
                0,                                      //quantity
                '',                                     //isbn
                null,                                   //low_stock_threshold
                false                                   //low_stock_alert
            );
            if (!$id_product_attribute) {
                self::logError(
                    $logger,
                    'Could not create combination ref ' . $supplier_reference
                );
                return false; //loggable
            }
            // product_supplier
            $ps_currecy_default = Configuration::get('PS_CURRENCY_DEFAULT');
            $product->addSupplierReference(
                (int) $product->id_supplier,
                (int) $id_product_attribute,
                $supplier_reference,
                (0 == $attribute['price'] ? 0 : (float) ((float) $product->price + (float) $attribute['price'])),
                $ps_currecy_default
            );
            // if multisupplier : register references for every supplier in product_supplier table
            if (!is_null($tab_spe = Tools::jsonDecode($attribute['special'], true))) {
                if (is_array($tab_spe) && array_key_exists('multifournisseur', $tab_spe)) {
                    foreach ($tab_spe['multifournisseur'] as $supplier_name => $data_mf) {
                        $id_supplier_mf = self::getEciFournId($supplier_name);
                        if ($id_supplier_mf && is_array($data_mf) && array_key_exists('combination', $data_mf)) {
                            if (is_array($data_mf['combination'])) {
                                foreach ($data_mf['combination'] as $ref_comb => $deltaPrice) {
                                    $product->addSupplierReference(
                                        (int) $id_supplier_mf,
                                        (int) $id_product_attribute,
                                        $ref_comb,
                                        (float) $deltaPrice,
                                        $ps_currecy_default
                                    );
                                    break;
                                }
                            }
                        }
                    }
                }
            }
            // combination
            $combination = new Combination((int) $id_product_attribute, null, $id_shop);
            $combination->associateTo($id_shop);
            $combination->setAttributes($combinationAttributes);
            //add images to combination
            if (!empty($attribute['pictures'])) {
                //parser
                if (($tab_images = Tools::jsonDecode($attribute['pictures'], true)) === null) {
                    $tab_images = explode('//:://', $attribute['pictures']);
                }
                if (!empty($tab_images[0])) {
                    $a_ids_image = self::setImages($product, $tab_images, false);
                    $combination->setImages($a_ids_image);
                }
            }
        }

        //add to eci_product_shop
        self::setEciProductShop(
            array(
                'reference' => $attribute['reference_attribute'],
                'keep' => $attribute['keep'],
            ),
            array(
                'id_product' => $product->id,
                'id_product_attribute' => $id_product_attribute,
            ),
            $id_shop,
            $fournisseur
        );

        $combination->supplier_reference = $supplier_reference;

        //ajout de valeurs particulières depuis le champ special
        if ($attribute['special'] && !is_null($special = json_decode($attribute['special'], true)) && is_array($special)) {
            self::setAttributeSpecial($combination, $special);
        }

        if (!$combination->save()) {
            self::logError(
                $logger,
                'Could not save combination ref ' . $supplier_reference
            );
            return false;
        }

        return $id_product_attribute;
    }

    public static function setEciProductShop($data, $ids, $id_shop, $connecteur = null)
    {
        $fournisseur = $connecteur ?? self::getConnecteurName();
        if (empty($data['reference']) || empty($ids['id_product'])) {
            return false;
        }

        return Db::getInstance()->insert(
            'eci_product_shop',
            array(
                'reference' => pSQL($data['reference']),
                'id_product' => (int) $ids['id_product'],
                'id_product_attribute' => (int) ($ids['id_product_attribute'] ?? 0),
                'id_shop' => (int) $id_shop,
                'is_pack' => (bool) ($data['is_pack'] ?? 0),
                'imported' => (bool) ($data['imported'] ?? 0),
                'keep' => pSQL($data['keep'] ?? ''),
                'act_reasons' => pSQL($data['act_reasons'] ?? ''),
                'deref' => (bool) ($data['deref'] ?? 0),
                'fournisseur' => pSQL($fournisseur)
            ),
            false,
            false,
            Db::INSERT_IGNORE
        );
    }

    public function associateImagesToShops($id_product, $id_shops)
    {
        if (!is_array($id_shops)) {
            $id_shops = array($id_shops);
        }

        $images = Image::getImages(Configuration::get('PS_LANG_DEFAULT'), $id_product, 0);
        $res = true;
        foreach ($images as $image) {
            $objImage = new Image($image['id_image'], null, $this->id_shop);
            if (!Validate::isLoadedObject($objImage)) {
                continue;
            }
            $objImage->associateTo($id_shops);
            $objImage->id_product = $id_product;
            if (!empty($image['cover'])) {
                $objImage->cover = 1;
            }
            $res &= $objImage->save();
        }

        return $res;
    }

    public function associateAttributesToShops($atColl, $id_shops)
    {
        if (!is_array($id_shops)) {
            $id_shops = array($id_shops);
        }

        foreach ($atColl as $l) {
            foreach ($l as $id_attribute_group => $id_attribute) {
                $attribute_group = new AttributeGroup($id_attribute_group, null, $this->id_shop);
                $attribute_group->associateTo($id_shops);
                $attribute_value = new Attribute($id_attribute, null, $this->id_shop);
                $attribute_value->associateTo($id_shops);
            }
        }

        return true;
    }

    public static function getProdutAttributeCombinations($id_product, $with_shop = false)
    {
        return Db::getInstance()->executeS('SELECT p.id_product_attribute, a.id_attribute_group, c.id_attribute' . ($with_shop ? ', p.id_shop' : '') . '
            FROM ' . _DB_PREFIX_ . 'product_attribute' . ($with_shop ? '_shop' : '') . ' p
            LEFT JOIN ' . _DB_PREFIX_ . 'product_attribute_combination c
            ON c.id_product_attribute = p.id_product_attribute
            LEFT JOIN ' . _DB_PREFIX_ . 'attribute a
            ON a.id_attribute = c.id_attribute
            WHERE p.id_product = ' . (int) $id_product);
    }

    public static function getAttributesMatched($tab_lnv, $id_lang, $iso_lang, $id_shop, $fournisseur, $product_reference, $force_match = true)
    {
        $combinationAttributes = array();

        // get context of splitted matching
        $split_context = self::getAttrSplitContext($fournisseur);
        if ('none' !== $split_context) {
            $p_data = self::getProduct($product_reference);
            switch ($split_context) {
                case 'cat0':
                    $p_l = self::langExpl($p_data['produit']['category']);
                    $p = isset($p_l[$iso_lang]) ? $p_l[$iso_lang] : reset($p_l);
                    $ps = self::categoryPathToPaths($p);
                    $split_value = reset($ps);
                    break;
                case 'manu':
                    $split_value = $p_data['produit']['manufacturer'];
                    break;
                default:
                    $split_context = false;
            }
        } else {
            $split_context = false;
        }

        foreach ($tab_lnv as $lnv) {
            $nv = isset($lnv[$iso_lang]) ? $lnv[$iso_lang] : reset($lnv);
            $name_ = implode('', array_keys($nv));
            $value = implode('', $nv);

            for ($tries = 1; $tries >= 0; $tries--) {
                $attribute_group_match = Db::getInstance()->getRow(
                    'SELECT s.id_attribute_eco, s.id_attribute
                    FROM ' . _DB_PREFIX_ . 'eci_attribute a
                    LEFT JOIN ' . _DB_PREFIX_ . 'eci_attribute_shop s
                    ON a.id_attribute_eco = s.id_attribute_eco
                    WHERE value = "' . pSQL($name_) . '"
                    AND fournisseur = "' . pSQL($fournisseur) . '"
                    AND id_shop = ' . (int) $id_shop . '
                    AND id_lang = ' . (int) $id_lang .
                    (($split_context && $tries) ? ' AND split_context = "' . pSQL($split_context) . '" AND split_value = "' . pSQL($split_value) . '"' : '')
                );
                $id_attribute_group_eco = empty($attribute_group_match['id_attribute_eco']) ? 0 : $attribute_group_match['id_attribute_eco'];
                $id_attribute_group = empty($attribute_group_match['id_attribute']) ? 0 : $attribute_group_match['id_attribute'];
                if ($id_attribute_group || !$split_context) {
                    break;
                }
            }
            if (!$id_attribute_group) {
                if (!$id_attribute_group_eco) {
                    $id_attribute_group_eco = Db::getInstance()->getValue(
                        'SELECT id_attribute_eco
                        FROM ' . _DB_PREFIX_ . 'eci_attribute
                        WHERE value = "' . pSQL($name_) . '"
                        AND id_lang = ' . (int) $id_lang . '
                        AND fournisseur = "' . pSQL($fournisseur) . '"'
                    );
                    if (!$id_attribute_group_eco) {
                        Db::getInstance()->insert(
                            'eci_attribute',
                            array(
                                'value' => pSQL($name_),
                                'id_lang' => (int) $id_lang,
                                'fournisseur' => pSQL($fournisseur)
                            )
                        );
                        $id_attribute_group_eco = Db::getInstance()->Insert_ID();
                    }
                }
                $id_attribute_group = Db::getInstance()->getValue(
                    'SELECT id_attribute_group
                    FROM ' . _DB_PREFIX_ . 'attribute_group_lang
                    WHERE id_lang = ' . (int) $id_lang . '
                    AND name = "' . pSQL($name_) . '"'
                );
                if ($force_match) {
                    if (!$id_attribute_group) {
                        //C du groupe d'attribute PS
                        $newGroup = new AttributeGroup();
                        $ml_name = self::createMultiLangField(array_map('implode', array_map('array_keys', $lnv)));
                        $newGroup->name = $ml_name;
                        $newGroup->public_name = $ml_name;
                        $newGroup->group_type = 'select';
                        $newGroup->add();
                        $newGroup->associateTo($id_shop);
                        $id_attribute_group = $newGroup->id;
                    }
                    Db::getInstance()->insert(
                        'eci_attribute_shop',
                        array(
                            'id_attribute_eco' => (int) $id_attribute_group_eco,
                            'id_attribute' => (int) $id_attribute_group,
                            'id_shop' => (int) $id_shop
                        ),
                        false,
                        false,
                        Db::ON_DUPLICATE_KEY
                    );
                }
            }

            $attribute_match = Db::getInstance()->getRow(
                'SELECT s.id_attribute_eco_value, s.id_attribute
                FROM ' . _DB_PREFIX_ . 'eci_attribute_value a
                LEFT JOIN ' . _DB_PREFIX_ . 'eci_attribute_value_shop s
                ON a.id_attribute_eco_value = s.id_attribute_eco_value
                WHERE value = "' . pSQL($value) . '"
                AND a.id_attribute_eco = ' . (int) $id_attribute_group_eco . '
                AND s.id_shop = ' . (int) $id_shop . '
                AND id_lang = ' . (int) $id_lang
            );
            $id_attribute_eco = empty($attribute_match['id_attribute_eco_value']) ? 0 : $attribute_match['id_attribute_eco_value'];
            $id_attribute = empty($attribute_match['id_attribute']) ? 0 : $attribute_match['id_attribute'];
            if (!$id_attribute) {
                if (!$id_attribute_eco) {
                    $id_attribute_eco = Db::getInstance()->getValue(
                        'SELECT id_attribute_eco_value
                        FROM ' . _DB_PREFIX_ . 'eci_attribute_value
                        WHERE value = "' . pSQL($value) . '"
                        AND id_attribute_eco = ' . (int) $id_attribute_group_eco . '
                        AND id_lang = ' . (int) $id_lang . '
                        AND fournisseur = "' . pSQL($fournisseur) . '"'
                    );
                    if (!$id_attribute_eco) {
                        Db::getInstance()->insert(
                            'eci_attribute_value',
                            array(
                                'value' => pSQL($value),
                                'id_attribute_eco' => (int) $id_attribute_group_eco,
                                'id_lang' => (int) $id_lang,
                                'fournisseur' => pSQL($fournisseur)
                            )
                        );
                        $id_attribute_eco = Db::getInstance()->Insert_ID();
                    }
                }
                $id_attribute = Db::getInstance()->getValue(
                    'SELECT l.id_attribute
                    FROM ' . _DB_PREFIX_ . 'attribute_lang l
                    LEFT JOIN ' . _DB_PREFIX_ . 'attribute a
                    ON l.id_attribute = a.id_attribute
                    WHERE name = "' . pSQL($value) . '"
                    AND id_attribute_group = ' . (int) $id_attribute_group . '
                    AND id_lang = ' . (int) $id_lang
                );
                if ($force_match) {
                    if (!$id_attribute) {
                        $newAttribute = new Attribute();
                        $ml_value = self::createMultiLangField(array_map('implode', $lnv));
                        $newAttribute->name = $ml_value;
                        $newAttribute->id_attribute_group = $id_attribute_group;
                        $newAttribute->add();
                        $newAttribute->associateTo($id_shop);
                        $id_attribute = $newAttribute->id;
                    }
                    Db::getInstance()->insert(
                        'eci_attribute_value_shop',
                        array(
                            'id_attribute_eco_value' => (int) $id_attribute_eco,
                            'id_attribute' => (int) $id_attribute,
                            'id_shop' => (int) $id_shop
                        ),
                        false,
                        false,
                        Db::ON_DUPLICATE_KEY
                    );
                }
            }

            $combinationAttributes[] = array($id_attribute_group => $id_attribute);
        }

        return $combinationAttributes;
    }

    public static function combinationDelete($id_product, $id_product_attribute)
    {
        $where = 'id_product_attribute=' . (int) $id_product_attribute;
        Db::getInstance()->delete('product_attribute', $where);
        Db::getInstance()->delete('product_attribute_combination', $where);
        Db::getInstance()->delete('product_attribute_image', $where);
        Db::getInstance()->delete('product_attribute_shop', $where);
        Db::getInstance()->delete('product_supplier', $where);
        Db::getInstance()->delete('stock_available', $where);
        Db::getInstance()->delete('specific_price', $where);
        try {
            Hook::exec('actionProductAttributeDelete', array( 'id_product_attribute' => (int) $id_product_attribute, 'id_product' => (int) $id_product, 'deleteAllAttributes' => false, 'ec_import' => true));
            $product = new Product($id_product);
            $product->checkDefaultAttributes();
        } catch (Exception $e) {
            return $e->getMessage();
        }

        return true;
    }

    public static function setImages($product, $tabImages, $raz = true, $id_shop = 0, $id_lang = 0)
    {
        if ($raz) {
            $product->deleteImages();
        }

        if (!$id_shop) {
            $id_shop = $product->id_shop_default;
        }
        Shop::setContext(Shop::CONTEXT_SHOP, $id_shop);
        Context::getContext()->shop->id = $id_shop;
        Context::getContext()->employee = new Employee(self::getEciEmployeeId());
        $tab_shops = $product->getAssociatedShops();
        if (!$id_lang) {
            $id_lang = Configuration::get('PS_LANG_DEFAULT');
        }

        $a_ids_images = array();
        $firstImage = false;
        foreach ($tabImages as $imageN) {
            $legend = array();
            $fields_to_update = [];
            if (is_array($imageN)) {
                $url = $imageN['url'];
                $position = isset($imageN['position']) ? (int) $imageN['position'] : 0;
                $cover = isset($imageN['cover']) ? (bool) $imageN['cover'] : false;
                if (isset($imageN['legend'])) {
                    if (is_array($imageN['legend'])) {
                        foreach ($imageN['legend'] as $iso => $txt) {
                            if (Validate::isLangIsoCode($iso)) {
                                $idLg = Language::getIdByIso($iso);
                                if ($idLg) {
                                    $legend[(int) $idLg] = $txt;
                                    $fields_to_update['legend'][(int) $idLg] = true;
                                }
                            }
                        }
                    } else {
                        $legend[$id_lang] = $imageN['legend'];
                        $fields_to_update['legend'][$id_lang] = true;
                    }
                }
            } else {
                $url = $imageN;
            }
            $url = str_replace(' ', '%20', $url);

            $cache_key = 'idp'.$product->id.'ids'.$id_shop.'url'.$url;
            if (Cache::isStored($cache_key)) {
                $a_ids_images[] = (int) Cache::retrieve($cache_key);
                continue;
            }

            $image = new Image();
            $image->id_product = $product->id;
            $image->position = empty($position) ? Image::getHighestPosition($product->id) + 1 : (int) $position;
            $image->cover = empty($cover) ? null : 1;

            if (!$image->add()) {
                continue;
            }

            $imageID = $image->id;
            if (!self::copyImg($product->id, $url, $imageID)) {
                $image->delete();
                continue;
            }

            Cache::store($cache_key, (int) $imageID);

            $a_ids_images[] = $imageID;
            $firstImage = $firstImage ? $firstImage : $imageID;
            if (!empty($legend)) {
                try {
                    $image->legend = $legend;
                    $image->setFieldsToUpdate($fields_to_update);
                    $image->update();
                } catch (Exception $e) {
                }
            }

            $image->associateTo($tab_shops);
        }

        if ($firstImage && (!Image::getCover((int) $product->id))) {
            try {
                $image = new Image($firstImage);
                $image->cover = 1;
                $image->setFieldsToUpdate(['cover' => true]);
                $image->update();
                Db::getInstance()->update(
                    'image_shop',
                    array(
                        'id_product' => (int) $product->id,
                        'cover' => (int) 1
                    ),
                    'id_image = ' . (int) $firstImage
                );
            } catch (Exception $e) {
            }
        }

        return array_values($a_ids_images);
    }

    public static function copyImg($id_entity, $url, $id_image = null, $entity = 'products')
    {
        $tmpfile = tempnam(_PS_TMP_IMG_DIR_, 'ps_import');
        $watermark_types = explode(',', Configuration::get('WATERMARK_TYPES'));
        $cleanUrl = trim($url);

        switch ($entity) {
            case 'products':
                $image_obj = new Image($id_image);
                $path = $image_obj->getPathForCreation();
                break;
            case 'categories':
                $path = _PS_CAT_IMG_DIR_ . (int) $id_entity;
                break;
            case 'manufacturers':
                $path = _PS_MANU_IMG_DIR_ . (int) $id_entity;
                break;
            default:
                break;
        }

        /*
        //get infos from image, not allways safe (see doc)
        $infos = @getimagesize($cleanUrl);
        if (!is_array($infos) || !isset($infos['bits'])) {
            return false;
        }
        */
        /*
        //drop images > fullHD (they will most probably cause problems when trying to resize)
        if ($infos[0] > 1920 || $infos[1] > 1920) {
            return false;
        }
        */
        if (!ImageManager::checkImageMemoryLimit($cleanUrl)) {
            return false;
        }
        if (@copy($cleanUrl, $tmpfile)) {
            ImageManager::resize($tmpfile, $path . '.jpg');
            $images_types = ImageType::getImagesTypes($entity);
            foreach ($images_types as $image_type) {
                ImageManager::resize($tmpfile, $path . '-' . Tools::stripslashes($image_type['name']) . '.jpg', $image_type['width'], $image_type['height']);
            }
            if (in_array($image_type['id_image_type'], $watermark_types)) {
                Hook::exec('actionWatermark', array('id_image' => $id_image, 'id_product' => $id_entity));
            }
        } elseif (($content = Tools::file_get_contents($cleanUrl)) !== false) {
            file_put_contents($tmpfile, $content);
            ImageManager::resize($tmpfile, $path . '.jpg');
            $images_types = ImageType::getImagesTypes($entity);
            foreach ($images_types as $image_type) {
                ImageManager::resize($tmpfile, $path . '-' . Tools::stripslashes($image_type['name']) . '.jpg', $image_type['width'], $image_type['height']);
            }
            if (in_array($image_type['id_image_type'], $watermark_types)) {
                Hook::exec('actionWatermark', array('id_image' => $id_image, 'id_product' => $id_entity));
            }
        } else {
            unlink($tmpfile);
            return false;
        }

        unlink($tmpfile);
        return true;
    }

    public function delProductToShop($tab)
    {
        $db = Db::getInstance();
        $reference = $tab['reference'];
        $id_shop = $tab['id_shop'];
        $fournisseur = $tab['fournisseur'];
        Shop::setContext(Shop::CONTEXT_SHOP, $id_shop);
        Context::getContext()->shop->id = $id_shop;
        Context::getContext()->employee = new Employee(self::getInfoEco('ID_EMPLOYEE'));
        $id_fournisseur = self::getEciFournId($fournisseur);

        $productId = new ImporterReference($reference, $id_fournisseur, $id_shop);
        $id_product = $productId->id_product;

        if (!empty($id_product)) {
            try {
                $product = new Product($id_product, false, null, $id_shop, null);
                $product->delete();
            } catch (Exception $e) {
                return $e->getMessage();
            }
            $fail = $db->getValue(
                'SELECT COUNT(*)
                FROM ' . _DB_PREFIX_ . 'product_shop
                WHERE id_product = ' . (int) $id_product . '
                AND id_shop = ' . (int) $id_shop
            );

            if ($fail) {
                return false;
            } else {
                $productUsed = $db->getValue(
                    'SELECT COUNT(*)
                    FROM ' . _DB_PREFIX_ . 'product_shop
                    WHERE id_product = ' . (int) $id_product
                );
                if (!$productUsed) {
                    $db->delete('product_supplier', 'id_product=' . (int) $id_product);
                }
            }
        }
        $db->delete(
            'eci_product_shop',
            'reference="' . pSQL($reference) . '" AND id_shop=' . (int) $id_shop . ' AND fournisseur="' . pSQL($fournisseur) . '"'
        );
        $db->delete(
            'eci_product_imported',
            'reference="' . pSQL($reference) . '" AND id_shop=' . (int) $id_shop . ' AND fournisseur="' . pSQL($fournisseur) . '"'
        );

        return true;
    }

    /**
     * near obsolete since we will no longer put refs in the product table but only in eci_product_shop and product_supplier
     *
     * @param type $reference
     * @param type $id_supplier
     * @return int
     */
    public function getIdsBySupplierRef($reference, $id_supplier = 0)
    {
        if (empty($reference)) {
            return 0;
        }

        if (!Validate::isReference($reference)) {
            return 0;
        }

        $query = new DbQuery();
        $query->select('p.id_product');
        $query->from('product', 'p');
        $query->where('p.id_shop_default = \'' . (int) $this->id_shop . '\' AND p.supplier_reference = \'' . pSQL($reference) . '\'');
        if ($id_supplier) {
            $query->where('p.id_supplier = ' . (int) $id_supplier);
        }

        return array_map('implode', Db::getInstance()->executeS($query));
    }
    
    public static function getIdsFromReference($reference, $id_shop, $is_regex = false)
    {
        $ids = array();

        $ids_p = Db::getInstance()->executeS(
            'SELECT p.id_product, 0 AS id_product_attribute, reference
            FROM ' . _DB_PREFIX_ . 'product p
            LEFT JOIN ' . _DB_PREFIX_ . 'product_shop s
            ON p.id_product = s.id_product
            WHERE p.reference '.($is_regex ? 'RLIKE "' . $reference . '" /* it is a regexp */' : 'LIKE "' . pSQL($reference) . '"').
            ' AND s.id_shop = ' . (int)$id_shop . ';'
        );
        if ($ids_p) {
            $ids = array_merge($ids, $ids_p);
        }

        $ids_a = Db::getInstance()->executeS(
            'SELECT pa.id_product, pa.id_product_attribute, reference
            FROM ' . _DB_PREFIX_ . 'product_attribute pa
            LEFT JOIN ' . _DB_PREFIX_ . 'product_attribute_shop s
            ON pa.id_product_attribute = s.id_product_attribute
            WHERE pa.reference '.($is_regex ? 'RLIKE "' . $reference . '" /* it is a regexp */' : 'LIKE "' . pSQL($reference) . '"').
            ' AND s.id_shop = ' . (int)$id_shop . ';'
        );
        if ($ids_a) {
            $ids = array_merge($ids, $ids_a);
        }

        return $ids;
    }

    public static function nettoyeNom($nom, $limit = 128)
    {
        return self::getStrCutBytes(
            str_replace(array('<', '>', ';', '=', '#', '{', '}', '  '), ' ', html_entity_decode($nom)),
            (int) $limit
        );
    }

    public static function nettoyeNomHard($nom, $limit = 128)
    {
        return self::getStrCutBytes(
            preg_replace(
                //array('/\"/', '/\&/', '/\'/', '/\=/', '/\</', '/\>/', '/\;/', '/\#/', '/\{/', '/\}/', '/\^/', '/\(/', '/\)/', ),
                //array('in', '+', '', '', '', '', '', '', '', '', '', '', '', ),
                array('/\&/', '/\'/', '/\=/', '/\</', '/\>/', '/\;/', '/\#/', '/\{/', '/\}/', '/\^/', '/\(/', '/\)/', ),
                array('+', '', '', '', '', '', '', '', '', '', '', '', ),
                iconv('ASCII', 'UTF-8//TRANSLIT', iconv('UTF-8', 'ASCII//TRANSLIT', $nom))
            ),
            (int) $limit
        );
    }

    public static function nettoyeNomSoft($nom, $limit = 128)
    {
        return self::getStrCutBytes(
            iconv('UTF-8', 'UTF-8//TRANSLIT', $nom),
            (int) $limit
        );
    }

    public static function nettoyeRef($ref, $limit = 32)
    {
        return self::getStrCutBytes(
            preg_replace(array('/[^0-9a-zA-Z\.\-\#\\]/'), '', $ref),
            (int) $limit
        );
    }

    public static function getStrCutBytes($str, $bytes)
    {
        $str_trimmed = trim($str);
        $size_limit = $bytes;
        while (strlen($str_trimmed) > $bytes) { //we need bytes not chars
            $str_trimmed = Tools::substr($str_trimmed, 0, $size_limit);
            $size_limit--;
        }

        return $str_trimmed;
    }

    public static function str2urlParanoid($str)
    {
        return preg_replace('/[^a-z0-9-]/', '', Tools::str2url(Tools::replaceAccentedChars($str)));
    }

    public static function getCommonWords($array, $get_diffs = false, $glue_units = null)
    {
        if (!is_array($array)) {
            return $array;
        }
        if (1 === count($array)) {
            return reset($array);
        }

        $phrases = array();
        foreach ($array as $item) {
            if (!is_null($glue_units)) {
                $item = preg_replace('/([0-9]+)\s('.$glue_units.')/', '$1$2', $item);
            }
            $phrase = preg_split('/\s+/', $item);
            $phrases[] = $phrase;
        }
        $sizes = array_map('count', $phrases);
        $min = min($sizes);
        $pos = array_search($min, $sizes);
        $reference = $phrases[$pos];

        $common = array_fill(0, $min, 1);
        $wordsok = 0;
        $common_phrase = array();
        foreach ($reference as $i => $word) {
            foreach ($phrases as $n => $phrase) {
                if ($n === $pos) {
                    continue;
                }
                if (!in_array($word, array_slice($phrase, $wordsok))) {
                    $common[$i] = 0;
                }
            }
            if ($common[$i]) {
                $wordsok++;
                $common_phrase[] = $word;
            }
        }

        if (!$get_diffs) {
            return implode(' ', $common_phrase);
        }

        $diffs = array();
        foreach ($phrases as $phrase) {
            foreach ($phrase as $i => $word) {
                if (in_array($word, $common_phrase)) {
                    unset($phrase[$i]);
                }
            }
            $diffs[] = implode(' ', $phrase);
        }

        return array_merge(array(implode(' ', $common_phrase)), array($diffs));
    }

    public static function getInfoFourn($name, $fourn = null)
    {
        $connecteur = $fourn ?? self::getConnecteurName();
        
        $key = __NAMESPACE__ . '_' . __FUNCTION__ . '_' . $name . '_' . $connecteur;
        if (!Cache::isStored($key)) {
            $val = Db::getInstance()->getValue(
                'SELECT ' . pSQL($name) . '
                FROM ' . _DB_PREFIX_ . 'eci_fournisseur
                WHERE name = "' . pSQL($connecteur) . '"'
            );
            Cache::store($key, $val);
        }

        return Cache::retrieve($key);
    }

    public static function getEciFournId($fourn = null)
    {
        $connecteur = $fourn ?? self::getConnecteurName();
        
        $key = __NAMESPACE__ . '_' . __FUNCTION__ . '_' . $connecteur;
        if (!Cache::isStored($key)) {
            $id_supplier = self::getInfoFourn('id_manufacturer', $connecteur);
            Cache::store($key, $id_supplier);
        }

        return Cache::retrieve($key);
    }

    public static function getFriendlyName($fourn = null)
    {
        $connecteur = $fourn ?? self::getConnecteurName();

        $key = __NAMESPACE__ . '_' . __FUNCTION__ . '_' . $connecteur;
        if (!Cache::isStored($key)) {
            $firendly_name = self::getInfoFourn('friendly_name', $connecteur);
            Cache::store($key, $firendly_name);
        }

        return Cache::retrieve($key);
    }

    public function countSynchro()
    {
        return (int) Db::getInstance()->getValue(
            'SELECT COUNT(*)
            FROM ' . _DB_PREFIX_ . 'eci_product_imported
            WHERE id_shop=' . (int) $this->id_shop
        );
    }

    public static function countSynchroImport($connecteur, $id_shop)
    {
        return (int) Db::getInstance()->getValue(
            'SELECT COUNT(*)
            FROM ' . _DB_PREFIX_ . 'eci_product_imported
            WHERE imported = 1 AND fournisseur = "'.pSQL($connecteur).'" AND id_shop=' . (int) $id_shop
        );
    }

    public static function countSynchroDelete($connecteur, $id_shop)
    {
        return (int) Db::getInstance()->getValue(
            'SELECT COUNT(*)
            FROM ' . _DB_PREFIX_ . 'eci_product_imported
            WHERE imported = 2 AND fournisseur = "'.pSQL($connecteur).'" AND id_shop=' . (int) $id_shop
        );
    }

    public function insertFeatures($fourn = 'PS')
    {
        $lang_default = Configuration::get('PS_LANG_DEFAULT');
        $tab_insert_features = array();
        foreach ($this->tabFeatures as $info) {
            list($lang, $value) = explode('|', $info);
            if ($lang == $lang_default) {
                $tab_insert_features[] = array(
                    'value' => pSQL($value),
                    'id_lang' => (int) $lang,
                    'fournisseur' => pSQL($fourn)
                );
            }
        }
        if ($tab_insert_features) {
            Db::getInstance()->insert(
                'eci_feature',
                $tab_insert_features,
                false,
                false,
                Db::INSERT_IGNORE
            );
        }

        $tab_insert_feature_values = array();
        foreach ($this->tabFeatures_value as $info) {
            list($lang, $key, $value) = explode('|', $info);
            if ($lang == $lang_default) {
                $idF = Db::getInstance()->getValue(
                    'SELECT id_feature_eco
                    FROM ' . _DB_PREFIX_ . 'eci_feature
                    WHERE value = "' . pSQL($key) . '"
                    AND id_lang = ' . (int) $lang . '
                    AND fournisseur = "' . pSQL($fourn) .'"'
                );
                $tab_insert_feature_values[] = array(
                    'id_feature_eco' =>  (int) $idF,
                    'value' => pSQL($value),
                    'id_lang' => (int) $lang,
                    'fournisseur' => ('PS' !== $fourn ? pSQL($fourn) : '')
                );
            }
        }
        if ($tab_insert_feature_values) {
            Db::getInstance()->insert(
                'eci_feature_value',
                $tab_insert_feature_values,
                false,
                false,
                Db::INSERT_IGNORE
            );
        }
    }

    public function matchFeatures()
    {
        //initialize unmatched feature names
        $all_features = Db::getInstance()->executeS(
            'SELECT f.id_feature_eco, s.id_shop
            FROM ' . _DB_PREFIX_ . 'eci_feature f
            JOIN ' . _DB_PREFIX_ . 'shop s
            LEFT JOIN ' . _DB_PREFIX_ . 'eci_feature_shop b
            ON f.id_feature_eco = b.id_feature_eco
            AND s.id_shop = b.id_shop
            WHERE b.id_feature_eco IS NULL'
        );

        $tab_init_feature = array();
        foreach ($all_features as $feature) {
            $tab_init_feature[] = array(
                'id_feature_eco' => (int) $feature['id_feature_eco'],
                'id_feature' => 0,
                'id_shop' => (int) $feature['id_shop']
            );
        }
        if ($tab_init_feature) {
            Db::getInstance()->insert(
                'eci_feature_shop',
                $tab_init_feature,
                false,
                false,
                Db::INSERT_IGNORE
            );
        }

        //initialize unmatched feature values
        $all_feat_vals = Db::getInstance()->executeS(
            'SELECT f.id_feature_eco_value, s.id_shop
            FROM ' . _DB_PREFIX_ . 'eci_feature_value f
            JOIN ' . _DB_PREFIX_ . 'shop s
            LEFT JOIN ' . _DB_PREFIX_ . 'eci_feature_value_shop b
            ON f.id_feature_eco_value = b.id_feature_eco_value
            AND s.id_shop = b.id_shop
            WHERE b.id_feature_eco_value IS NULL'
        );

        $tab_init_feat_val = array();
        foreach ($all_feat_vals as $value) {
            $tab_init_feat_val[] = array(
                'id_feature_eco_value' => (int) $value['id_feature_eco_value'],
                'id_feature' => 0,
                'id_shop' => (int) $value['id_shop']
            );
        }
        if ($tab_init_feat_val) {
            Db::getInstance()->insert(
                'eci_feature_value_shop',
                $tab_init_feat_val,
                false,
                false,
                Db::INSERT_IGNORE
            );
        }
    }

    public function getFeaturesSpe($id_lang, $idF)
    {
        if (empty($id_lang)) {
            $id_lang = Configuration::get('PS_LANG_DEFAULT');
        }

        return Db::getInstance()->executeS(
            'SELECT DISTINCT f.*, fl.*, fv.id_feature_value, fvl.value, fl.name AS feature
            FROM ' . _DB_PREFIX_ . 'feature f
            LEFT JOIN ' . _DB_PREFIX_ . 'feature_lang fl
            ON (f.id_feature = fl.id_feature AND fl.id_lang = ' . (int) $id_lang . ')
            LEFT JOIN ' . _DB_PREFIX_ . 'feature_value fv
            ON fv.id_feature = f.id_feature
            LEFT JOIN ' . _DB_PREFIX_ . 'feature_value_lang fvl
            ON (fv.id_feature_value = fvl.id_feature_value AND fvl.id_lang = ' . (int) $id_lang . ')
            ' . Shop::addSqlAssociation('feature', 'f') . '
            ' . Shop::addSqlAssociation('feature_value', 'fv') . '
            WHERE fv.id_feature = ' . (int) $idF . '
            ORDER BY fvl.value'
        );
    }

    public function getFeaturesParam($fourn = false)
    {
        $class = __NAMESPACE__;
        $mod = new $class();
        $eciF = Db::getInstance()->executeS(
            'SELECT f.id_feature_eco, f.value, s.id_feature
            FROM  ' . _DB_PREFIX_ . 'eci_feature f
            LEFT JOIN ' . _DB_PREFIX_ . 'eci_feature_shop s
            ON f.id_feature_eco = s.id_feature_eco
            WHERE f.id_lang = ' . (int) $this->id_lang .'
            AND s.id_shop = ' . (int) $this->id_shop .
            ($fourn ? ' AND fournisseur = "' . pSQL($fourn) . '"' : '') . '
            ORDER BY f.value'
        );
        $Feature_PS = Feature::getFeatures($this->id_lang);
        $mod->smartyAssign('this');//sends this occurence
        $mod->smartyAssign(array(
            'eciF' => $eciF,
            'Feature_PS' => $Feature_PS,
            'fourn' => $fourn,
            'eci_ajaxlink' => Context::getContext()->link->getModuleLink('eci' . $fourn, 'ajax'),
        ));

        return $mod->display('/modules/' . __NAMESPACE__ . '/', 'views/templates/admin/EcParametresFeature.tpl');
    }

    public function saveFeatureMatching($id_feature_eco, $id_feature, $id_feature_eco_old = 0)
    {
        $db = Db::getInstance();
        $db->update(
            'eci_feature_value_shop',
            array(
                'id_feature' => (int) 0
            ),
            'id_shop = ' . (int) $this->id_shop
            . ' AND id_feature_eco_value IN ('
            . 'SELECT id_feature_eco_value FROM ' . _DB_PREFIX_ . 'eci_feature_value'
            . ' WHERE id_feature_eco = ' . (int) $id_feature_eco
            . ' AND id_lang = ' . (int) $this->id_lang . ')'
        );
        $db->update(
            'eci_feature_shop',
            array(
                'id_feature' => (int) $id_feature
            ),
            'id_feature_eco = ' . (int) $id_feature_eco
            . ' AND id_shop=' . (int) $this->id_shop
        );
        $this->matchFeatures();

        return true;
    }

    public function setFeatureValue($idF, $page = 1, $nbPerPage = 10)
    {
        //return nothing if feature is not matched
        $idFPS = Db::getInstance()->getValue(
            'SELECT id_feature
            FROM ' . _DB_PREFIX_ . 'eci_feature_shop
            WHERE id_feature_eco = ' . (int) $idF . '
            AND id_shop = ' . (int) $this->id_shop
        );
        if (!$idFPS) {
            return;
        }

        // build the list of all matched values for this feature
        $feat_matched = Db::getInstance()->executeS(
            'SELECT fv.id_feature_eco, fv.id_feature_eco_value, fv.value, s.id_feature
            FROM ' . _DB_PREFIX_ . 'eci_feature_value_shop s
            LEFT JOIN ' . _DB_PREFIX_ . 'eci_feature_value fv
            ON fv.id_feature_eco_value = s.id_feature_eco_value
            WHERE fv.id_feature_eco = ' . (int) $idF . '
            AND s.id_feature != 0
            AND fv.id_lang = ' . (int) $this->id_lang . '
            AND s.id_shop = ' . (int) $this->id_shop . '
            ORDER BY fv.value
            LIMIT ' . (int)($nbPerPage * ($page - 1)) . ',' . (int) $nbPerPage
        );
        $tot_matched = Db::getInstance()->getValue(
            'SELECT COUNT(*)
            FROM ' . _DB_PREFIX_ . 'eci_feature_value_shop s
            LEFT JOIN ' . _DB_PREFIX_ . 'eci_feature_value fv
            ON fv.id_feature_eco_value = s.id_feature_eco_value
            WHERE fv.id_feature_eco = ' . (int) $idF . '
            AND s.id_feature != 0
            AND fv.id_lang = ' . (int) $this->id_lang . '
            AND s.id_shop = ' . (int) $this->id_shop
        );
        $nbPage = (int)($tot_matched / $nbPerPage) + (int) (bool) ($tot_matched % $nbPerPage);

        $Feature_PS = $this->getFeaturesSpe($this->id_lang, $idFPS);
        
        return array(
            'idshop' => $this->id_shop,
            'idF' => $idF,
            'feat_matched' => $feat_matched,
            'Feature_PS' => $Feature_PS,
            'nbPage' => $nbPage,
            'page' => $page
        );
    }

    public function setAddFeatMatch($idF)
    {
        //return nothing if feature is not matched
        $idFPS = Db::getInstance()->getValue(
            'SELECT id_feature
            FROM ' . _DB_PREFIX_ . 'eci_feature_shop
            WHERE id_feature_eco = ' . (int) $idF . '
            AND id_shop = ' . (int) $this->id_shop
        );
        if (!$idFPS) {
            return false;
        }

        //build the list of feature values not matched
        $feat_to_match = Db::getInstance()->executeS(
            'SELECT fv.value, fv.id_feature_eco, fv.id_feature_eco_value, s.id_feature
            FROM ' . _DB_PREFIX_ . 'eci_feature_value fv
            LEFT JOIN ' . _DB_PREFIX_ . 'eci_feature_value_shop s
            ON fv.id_feature_eco_value = s.id_feature_eco_value
            WHERE fv.id_feature_eco = ' . (int) $idF . '
            AND (s.id_feature = 0 OR s.id_feature IS NULL)
            AND fv.id_lang = ' . (int) $this->id_lang . '
            AND (s.id_shop = ' . (int) $this->id_shop . ' OR s.id_shop IS NULL)'
        );
        if (!isset($feat_to_match)) {
            return false;
        }
        $Feature_PS = $this->getFeaturesSpe($this->id_lang, $idFPS);
        
        return array(
            'idshop' => $this->id_shop,
            'idF' => $idF,
            'feat_to_match' => $feat_to_match,
            'Feature_PS' => $Feature_PS
        );
    }

    public function addFeatMatch($b, $v, $s)
    {
        if (empty($b) || empty($v) || empty($s)) {
            return;
        }
        
        return Db::getInstance()->execute(
            'INSERT INTO ' . _DB_PREFIX_ . 'eci_feature_value_shop
            VALUES (' . (int) $v . ', ' . (int) $b . ', ' . (int) $s . ')
            ON DUPLICATE KEY UPDATE
            id_feature_eco_value = ' . (int) $v . ', id_feature = ' . (int) $b . ', id_shop = ' . (int) $s
        );
    }

    public function delFeatMatch($v, $s)
    {
        return Db::getInstance()->update(
            'eci_feature_value_shop',
            array(
                'id_feature' => 0
            ),
            'id_feature_eco_value = ' . (int) $v . ' AND id_shop = ' . (int) $s
        );
    }

    public function insertAttributes($fourn = 'PS')
    {
        $lang_default = Configuration::get('PS_LANG_DEFAULT');
        $tab_insert_attributes = array();
        foreach ($this->tabAttributes as $info) {
            list($lang, $value) = explode('|', $info);
            if ($lang == $lang_default) {
                $tab_insert_attributes[] = array(
                    'value' => pSQL($value),
                    'id_lang' => (int) $lang,
                    'fournisseur' => pSQL($fourn)
                );
            }
        }
        if ($tab_insert_attributes) {
            Db::getInstance()->insert(
                'eci_attribute',
                $tab_insert_attributes,
                false,
                false,
                Db::INSERT_IGNORE
            );
        }

        $tab_insert_attribute_values = array();
        foreach ($this->tabAttributes_value as $info) {
            list($lang, $key, $value) = explode('|', $info);
            if ($lang == $lang_default) {
                $idA = Db::getInstance()->getValue(
                    'SELECT id_attribute_eco
                    FROM ' . _DB_PREFIX_ . 'eci_attribute
                    WHERE value = "' . pSQL($key) . '"
                    AND id_lang = ' . (int) $lang . '
                    AND fournisseur = "' . pSQL($fourn) . '"'
                );
                $tab_insert_attribute_values[] = array(
                    'id_attribute_eco' =>  (int) $idA,
                    'value' => pSQL($value),
                    'id_lang' => (int) $lang,
                    'fournisseur' => pSQL($fourn)
                );
            }
        }
        if ($tab_insert_attribute_values) {
            Db::getInstance()->insert(
                'eci_attribute_value',
                $tab_insert_attribute_values,
                false,
                false,
                Db::INSERT_IGNORE
            );
        }
    }

    public function matchAttributes()
    {
        //initialize unmatched attribute groups
        $all_attributs = Db::getInstance()->executeS(
            'SELECT a.id_attribute_eco, s.id_shop
            FROM ' . _DB_PREFIX_ . 'eci_attribute a
            JOIN ' . _DB_PREFIX_ . 'shop s
            LEFT JOIN ' . _DB_PREFIX_ . 'eci_attribute_shop b
            ON a.id_attribute_eco = b.id_attribute_eco
            AND s.id_shop = b.id_shop
            WHERE b.id_attribute_eco IS NULL'
        );

        $tab_init_attribute = array();
        foreach ($all_attributs as $attribut) {
            $tab_init_attribute[] = array(
                'id_attribute_eco' => (int) $attribut['id_attribute_eco'],
                'id_attribute' => 0,
                'id_shop' => (int) $attribut['id_shop']
            );
        }
        if ($tab_init_attribute) {
            Db::getInstance()->insert(
                'eci_attribute_shop',
                $tab_init_attribute,
                false,
                false,
                Db::INSERT_IGNORE
            );
        }

        //initialize unmatched attribute values
        $all_att_vals = Db::getInstance()->executeS(
            'SELECT a.id_attribute_eco_value, s.id_shop
            FROM ' . _DB_PREFIX_ . 'eci_attribute_value a
            JOIN ' . _DB_PREFIX_ . 'shop s
            LEFT JOIN ' . _DB_PREFIX_ . 'eci_attribute_value_shop b
            ON a.id_attribute_eco_value = b.id_attribute_eco_value
            AND s.id_shop = b.id_shop
            WHERE b.id_attribute_eco_value IS NULL'
        );

        $tab_init_att_val = array();
        foreach ($all_att_vals as $value) {
            $tab_init_att_val[] = array(
                'id_attribute_eco_value' => (int) $value['id_attribute_eco_value'],
                'id_attribute' => 0,
                'id_shop' => (int) $value['id_shop']
            );
        }
        if ($tab_init_att_val) {
            Db::getInstance()->insert(
                'eci_attribute_value_shop',
                $tab_init_att_val,
                false,
                false,
                Db::INSERT_IGNORE
            );
        }
    }

    public function getAttributesSpe($id_lang, $idG)
    {
        if (empty($id_lang)) {
            $id_lang = Configuration::get('PS_LANG_DEFAULT');
        }

        return Db::getInstance()->executeS(
            'SELECT DISTINCT ag.*, agl.*, a.id_attribute, al.name, agl.name AS attribute_group
            FROM ' . _DB_PREFIX_ . 'attribute_group ag
            LEFT JOIN ' . _DB_PREFIX_ . 'attribute_group_lang agl
            ON (ag.id_attribute_group = agl.id_attribute_group AND agl.id_lang = ' . (int) $id_lang . ')
            LEFT JOIN ' . _DB_PREFIX_ . 'attribute a
            ON a.id_attribute_group = ag.id_attribute_group
            LEFT JOIN ' . _DB_PREFIX_ . 'attribute_lang al
            ON (a.id_attribute = al.id_attribute AND al.id_lang = ' . (int) $id_lang . ')
            ' . Shop::addSqlAssociation('attribute_group', 'ag') . '
            ' . Shop::addSqlAssociation('attribute', 'a') . '
            WHERE a.id_attribute_group = ' . (int) $idG . '
            ORDER BY al.name, a.position'
        );
    }

    public function getAttributesParam($fourn = false)
    {
        if (!$fourn) {
            $fourn = self::getConnecteurName();
        }

        $class = __NAMESPACE__;
        $mod = new $class();
        $split_context = self::getAttrSplitContext($fourn);

        $eciAG_ns = Db::getInstance()->executeS(
            'SELECT a.id_attribute_eco, a.value, s.id_attribute, a.split_value
            FROM  ' . _DB_PREFIX_ . 'eci_attribute a
            LEFT JOIN ' . _DB_PREFIX_ . 'eci_attribute_shop s
            ON a.id_attribute_eco = s.id_attribute_eco
            WHERE a.id_lang = ' . (int) $this->id_lang .'
            AND s.id_shop = ' . (int) $this->id_shop
            . ($fourn ? ' AND fournisseur = "' . pSQL($fourn) . '"' : '')
            . ' AND split_context = "none"'
            . ' ORDER BY a.value'
        );
        if ('none' !== $split_context) {
            $eciAG_s = Db::getInstance()->executeS(
                'SELECT a.id_attribute_eco, a.value, s.id_attribute, a.split_value
                FROM  ' . _DB_PREFIX_ . 'eci_attribute a
                LEFT JOIN ' . _DB_PREFIX_ . 'eci_attribute_shop s
                ON a.id_attribute_eco = s.id_attribute_eco
                WHERE a.id_lang = ' . (int) $this->id_lang .'
                AND s.id_shop = ' . (int) $this->id_shop
                . ($fourn ? ' AND fournisseur = "' . pSQL($fourn) . '"' : '')
                . ' AND split_context = "' . pSQL($split_context) . '"'
                . ' ORDER BY a.value, split_value'
            );
        } else {
            $eciAG_s = array();
        }

        $Attribut_PS = AttributeGroup::getAttributesGroups($this->id_lang);
        $mod->smartyAssign('this');
        $mod->smartyAssign(array(
            'eciAG' => array_merge($eciAG_ns, $eciAG_s),
            'Attribut_PS' => $Attribut_PS,
            'fourn' => $fourn,
            'eci_ajaxlink' => Context::getContext()->link->getModuleLink('eci' . $fourn, 'ajax'),
        ));

        return $mod->display('/modules/' . __NAMESPACE__ . '/', 'views/templates/admin/EcParametresAttribut.tpl');
    }

    public function getAttributeInformationProduit($reference, $connecteur, $iso_lang)
    {
        $list_decls = Db::getInstance()->executeS(
            'SELECT *
            FROM ' . _DB_PREFIX_ . 'eci_catalog_attribute
            WHERE product_reference = "' . pSQL($reference) . '"
            AND fournisseur = "' . pSQL($connecteur) . '"'
        );

        if (!$list_decls) {
            return null;
        }

        $getStocks = self::existsEciStock($connecteur);
        $refK = '';
        foreach ($list_decls as &$list_decl) {
            $tabLaAt = self::parseAttAndFeat($list_decl['attribute']);
            $list_decl['attribute'] = self::getAttributesDescription($tabLaAt, $iso_lang);
            if ($getStocks) {
                $list_decl['stock'] = self::getEciStock($list_decl['reference_attribute'], $connecteur, $this->id_shop);
            }
            $refK .= $list_decl['reference'];
        }
        if (!$refK) {
            foreach ($list_decls as &$list_decl) {
                unset($list_decl['reference']);
            }
        }

        return $list_decls;
    }

    public static function getAttributesDescription($tabLaAt, $iso_lang = null)
    {
        $tAt = array();

        foreach ($tabLaAt as $at) {
            foreach ($at as $iso => $tnv) {
                $desc = implode('', array_keys($tnv)) . ' : ' . implode('', $tnv);
                $tAt[$iso] = isset($tAt[$iso]) ? $tAt[$iso] . ' ; ' . $desc : $desc;
            }
        }

        return is_null($iso_lang) ? $tAt : (isset($tAt[$iso_lang]) ? $tAt[$iso_lang] : reset($tAt));
    }

    public function saveAttributeMatching($id_attribute_eco, $id_attribute, $id_attribute_eco_old)
    {
        $id_attribute_eco_old .= '';//not used
        $db = Db::getInstance();
        //unmatch values in this group because this group changed matching
        $db->update(
            'eci_attribute_value_shop',
            array(
                'id_attribute' => (int) 0
            ),
            'id_shop = ' . (int) $this->id_shop
            . ' AND id_attribute_eco_value IN ('
            . 'SELECT id_attribute_eco_value FROM ' . _DB_PREFIX_ . 'eci_attribute_value'
            . ' WHERE id_attribute_eco = ' . (int) $id_attribute_eco
            . ' AND id_lang = ' . (int) $this->id_lang . ')'
        );
        //update group matching
        $db->update(
            'eci_attribute_shop',
            array(
                'id_attribute' => (int) $id_attribute
            ),
            'id_attribute_eco = ' . (int) $id_attribute_eco
            . ' AND id_shop=' . (int) $this->id_shop
        );
        //if matching is splitted and this group is split_context = none => force matching for other contexts that are not matched
        $this->setAttributeMatchingForSplitted($id_attribute_eco);

        $this->matchAttributes();

        return true;
    }

    public function setAttributeMatchingForSplitted($id_attribute_eco)
    {
        //get eci attribute group infos
        $eci_att_grp = Db::getInstance()->getRow(
            'SELECT *
            FROM ' . _DB_PREFIX_ . 'eci_attribute a
            LEFT JOIN ' . _DB_PREFIX_ . 'eci_attribute_shop s
            ON s.id_attribute_eco = a.id_attribute_eco
            WHERE a.id_attribute_eco = ' . (int) $id_attribute_eco . '
            AND s.id_shop = ' . (int) $this->id_shop
        );
        if (empty($eci_att_grp) || '' !== $eci_att_grp['split_value']) {
            return true;
        }

        //for each splitted attribute group
        $splitted_att_grps = Db::getInstance()->executeS(
            'SELECT *
            FROM ' . _DB_PREFIX_ . 'eci_attribute a
            LEFT JOIN ' . _DB_PREFIX_ . 'eci_attribute_shop s
            ON s.id_attribute_eco = a.id_attribute_eco
            WHERE a.value = "' . pSQL($eci_att_grp['value']) . '"
            AND a.id_lang = ' . (int) $eci_att_grp['id_lang'] . '
            AND s.id_shop = ' . (int) $eci_att_grp['id_shop'] . '
            AND a.fournisseur = "' . pSQL($eci_att_grp['fournisseur'])  . '"
            AND a.split_context != "none"'
        );
        if (!$splitted_att_grps) {
            return;
        }
        foreach ($splitted_att_grps as $splitted_att_grp) {
            // only if not matched
            if ($splitted_att_grp['id_attribute']) {
                continue;
            }
            // match to same PS group
            Db::getInstance()->update(
                'eci_attribute_shop',
                array(
                    'id_attribute' => (int) $eci_att_grp['id_attribute']
                ),
                'id_attribute_eco = ' . (int) $splitted_att_grp['id_attribute_eco']
                . ' AND id_shop=' . (int) $this->id_shop
            );
            // clean values matching
            Db::getInstance()->update(
                'eci_attribute_value_shop',
                array(
                    'id_attribute' => (int) 0
                ),
                'id_shop = ' . (int) $this->id_shop
                . ' AND id_attribute_eco_value IN ('
                . 'SELECT id_attribute_eco_value FROM ' . _DB_PREFIX_ . 'eci_attribute_value'
                . ' WHERE id_attribute_eco = ' . (int) $splitted_att_grp['id_attribute_eco']
                . ' AND id_lang = ' . (int) $this->id_lang . ')'
            );
        }

        return true;
    }

    public function setAttributeValue($idA, $page = 1, $nbPerPage = 10)
    {
        //return nothing if attribute is not matched
        $idG = Db::getInstance()->getValue(
            'SELECT id_attribute
            FROM ' . _DB_PREFIX_ . 'eci_attribute_shop
            WHERE id_attribute_eco = ' . (int) $idA . '
            AND id_shop = ' . (int) $this->id_shop
        );
        if (!$idG) {
            return;
        }

        // build the list of all matched values for this attribute
        $att_matched = Db::getInstance()->executeS(
            'SELECT a.id_attribute_eco, a.id_attribute_eco_value, a.value, s.id_attribute
            FROM ' . _DB_PREFIX_ . 'eci_attribute_value_shop s
            LEFT JOIN ' . _DB_PREFIX_ . 'eci_attribute_value a
            ON a.id_attribute_eco_value = s.id_attribute_eco_value
            WHERE a.id_attribute_eco = ' . (int) $idA . '
            AND s.id_attribute != 0
            AND a.id_lang = ' . (int) $this->id_lang . '
            AND s.id_shop = ' . (int) $this->id_shop . '
            ORDER BY a.value
            LIMIT ' . (int)($nbPerPage * ($page - 1)) . ',' . (int) $nbPerPage
        );
        $tot_matched = Db::getInstance()->getValue(
            'SELECT COUNT(*)
            FROM ' . _DB_PREFIX_ . 'eci_attribute_value_shop s
            LEFT JOIN ' . _DB_PREFIX_ . 'eci_attribute_value a
            ON a.id_attribute_eco_value = s.id_attribute_eco_value
            WHERE a.id_attribute_eco = ' . (int) $idA . '
            AND s.id_attribute != 0
            AND a.id_lang = ' . (int) $this->id_lang . '
            AND s.id_shop = ' . (int) $this->id_shop
        );
        $nbPage = (int)($tot_matched / $nbPerPage) + (int) (bool) ($tot_matched % $nbPerPage);

        $Attribut_PS = self::getAttributesSpe($this->id_lang, $idG);
        
        return array(
            'idshop' => $this->id_shop,
            'idA' => $idA,
            'att_matched' => $att_matched,
            'Attribut_PS' => $Attribut_PS,
            'nbPage' => $nbPage,
            'page' => $page
        );
    }

    public function setAddAttrMatch($idA)
    {
        $class = __NAMESPACE__;
        $mod = new $class();
        //return nothing if attribute is not matched
        $idG = Db::getInstance()->getValue(
            'SELECT id_attribute
            FROM ' . _DB_PREFIX_ . 'eci_attribute_shop
            WHERE id_attribute_eco = ' . (int) $idA . '
            AND id_shop = ' . (int) $this->id_shop
        );
        if (!$idG) {
            return false;
        }

        //build the list of attribute values not matched
        $att_to_match = Db::getInstance()->executeS(
            'SELECT a.value, a.id_attribute_eco, a.id_attribute_eco_value, s.id_attribute
            FROM ' . _DB_PREFIX_ . 'eci_attribute_value a
            LEFT JOIN ' . _DB_PREFIX_ . 'eci_attribute_value_shop s
            ON a.id_attribute_eco_value = s.id_attribute_eco_value
            WHERE a.id_attribute_eco = ' . (int) $idA . '
            AND (s.id_attribute = 0 OR s.id_attribute IS NULL)
            AND a.id_lang = ' . (int) $this->id_lang . '
            AND (s.id_shop = ' . (int) $this->id_shop . ' OR s.id_shop IS NULL)
            ORDER BY a.value'
        );
        //var_export($att_to_match);
        if (!isset($att_to_match)) {
            return false;
        }
        $Attribut_PS = self::getAttributesSpe($this->id_lang, $idG);

        return array(
            'idshop' => $this->id_shop,
            'idA' => $idA,
            'att_to_match' => $att_to_match,
            'Attribut_PS' => $Attribut_PS
        );
    }

    public static function addAttrMatch($b, $v, $s)
    {
        if (empty($b) || empty($v) || empty($s)) {
            return;
        }
        return Db::getInstance()->execute(
            'INSERT INTO ' . _DB_PREFIX_ . 'eci_attribute_value_shop
            VALUES (' . (int) $v . ', ' . (int) $b . ', ' . (int) $s . ')
            ON DUPLICATE KEY UPDATE
            id_attribute_eco_value = ' . (int) $v . ', id_attribute = ' . (int) $b . ', id_shop = ' . (int) $s
        );
    }

    public static function delAttrMatch($v, $s)
    {
        return Db::getInstance()->update(
            'eci_attribute_value_shop',
            array(
                'id_attribute' => 0
            ),
            'id_attribute_eco_value = ' . (int) $v . ' AND id_shop = ' . (int) $s
        );
    }

    public static function getAttrSplitContext($connecteur = null)
    {
        if (is_null($connecteur)) {
            $connecteur = self::getConnecteurName();
        }

        $ret = self::getECIConfigValue('ATTR_SPLIT_CTX', 0, $connecteur);

        return $ret ? $ret : 'none';
    }

    /**
     * adds the values for the relevant attributes names and split contexts according to the catalogue
     * @return boolean
     */
    public function setAttrSplit()
    {
        $connecteur = self::getConnecteurName();
        $id_lang = Configuration::get('PS_LANG_DEFAULT');
        $iso_lang = Language::getIsoById($id_lang);
        //initialise void split contexts
        Db::getInstance()->update('eci_attribute', array('split_context' => 'none'), 'split_context = ""');

        $split_context = self::getAttrSplitContext($connecteur);
        if ('none' === $split_context) {
            return true;
        }

        //get attribute names
        $names_raw = Db::getInstance()->executeS('SELECT value FROM '._DB_PREFIX_.'eci_attribute WHERE fournisseur = "'.pSQL($connecteur).'" AND split_context = "none"');
        if (!$names_raw) {
            return true;
        }
        $names = array_map('implode', $names_raw);

        //get split values for this context
        switch ($split_context) {
            case 'cat0':
                $split_values_raw = Db::getInstance()->executeS('SELECT DISTINCT name FROM '._DB_PREFIX_.'eci_category_shop WHERE fournisseur = "'.pSQL($connecteur).'" AND name NOT LIKE "%::>>::%"');
                break;
            case 'manu':
                $split_values_raw = Db::getInstance()->executeS('SELECT DISTINCT manufacturer FROM '._DB_PREFIX_.'eci_catalog WHERE fournisseur = "'.pSQL($connecteur).'"');
                break;
            default:
                return;
        }
        if (empty($split_values_raw)) {
            return true;
        }
        $split_values = array_map('implode', $split_values_raw);

        //insert split attribute names
        $insert_names = array();
        foreach ($split_values as $split_value) {
            foreach ($names as $name) {
                $insert_names[] = array(
                    'value' => pSQL($name),
                    'split_context' => pSQL($split_context),
                    'split_value' => pSQL($split_value),
                    'id_lang' => (int) $id_lang,
                    'fournisseur' => pSQL($connecteur)
                );
            }
        }
        Db::getInstance()->insert(
            'eci_attribute',
            $insert_names,
            false,
            false,
            Db::INSERT_IGNORE
        );
        unset($insert_names);

        //split attribute values
        $insert_values = array();
        $id_names_context_none = array();
        foreach ($split_values as $split_value) {
            //get distinct "attribute" for this split value
            switch ($split_context) {
                case 'cat0':
                    $attributes_sql = 'SELECT DISTINCT attribute FROM ' . _DB_PREFIX_ . 'eci_catalog_attribute a LEFT JOIN ' . _DB_PREFIX_ . 'eci_catalog c ON c.product_reference = a.product_reference and c.fournisseur = a.fournisseur WHERE a.fournisseur = "' . pSQL($connecteur) . '" AND c.category RLIKE "//::' . pSQL($split_value) . '(:.*)?$"';
                    break;
                case 'manu':
                    $attributes_sql = 'SELECT DISTINCT attribute FROM ' . _DB_PREFIX_ . 'eci_catalog_attribute a LEFT JOIN ' . _DB_PREFIX_ . 'eci_catalog c ON c.product_reference = a.product_reference and c.fournisseur = a.fournisseur WHERE a.fournisseur = "' . pSQL($connecteur) . '" AND c.manufacturer = "' . pSQL($split_value) . '"';
                    break;
                default:
                    return;
            }
            $attributes_raw = Db::getInstance()->executeS($attributes_sql);
            if (empty($attributes_raw)) {
                continue;
            }
            $attributes = array_map('implode', $attributes_raw);

            //create list of attributes and values for this context
            $attributes_n_v = array();
            foreach ($attributes as $attribute) {
                $atts = self::parseAttAndFeat($attribute);
                foreach ($atts as $att_v_l) {
                    if (isset($att_v_l[$iso_lang])) {
                        $values = $att_v_l[$iso_lang];
                        $names = array_keys($values);
                        $n = reset($names);
                        $attributes_n_v[$n][] = reset($values);
                    }
                }
            }

            //prepare insertion of attribute values with each corresponding attribute name id
            foreach ($attributes_n_v as $att_n => $values) {
                if (isset($id_names_context_none[$att_n])) {
                    $id_attribute_eco_context_none = $id_names_context_none[$att_n];
                } else {
                    $id_attribute_eco_context_none = Db::getInstance()->getValue(
                        'SELECT id_attribute_eco
                        FROM ' . _DB_PREFIX_ . 'eci_attribute
                        WHERE value = "' . pSQL($att_n) . '"
                        AND split_context = "none"
                        AND fournisseur = "' . pSQL($connecteur) . '"'
                    );
                    if (!$id_attribute_eco_context_none) {
                        continue;
                    }
                    $id_names_context_none[$att_n] = $id_attribute_eco_context_none;
                }
                //get the id of the attribute name
                $id_attribute_eco = Db::getInstance()->getValue(
                    'SELECT id_attribute_eco
                    FROM ' . _DB_PREFIX_ . 'eci_attribute
                    WHERE value = "' . pSQL($att_n) . '"
                    AND split_context = "' . pSQL($split_context) . '"
                    AND split_value = "' . pSQL($split_value) . '"
                    AND fournisseur = "' . pSQL($connecteur) . '"'
                );
                if (!$id_attribute_eco) {
                    continue;
                }

                //add the list of values for this name id
                foreach ($values as $value) {
                    $insert_values[] = array(
                        'id_attribute_eco' => (int) $id_attribute_eco,
                        'value' => pSQL($value),
                        'id_lang' => (int) $id_lang,
                        'fournisseur' => pSQL($connecteur),
                    );
                    $insert_values[] = array(
                        'id_attribute_eco' => (int) $id_attribute_eco_context_none,
                        'value' => pSQL($value),
                        'id_lang' => (int) $id_lang,
                        'fournisseur' => pSQL($connecteur),
                    );
                }
            }
        }

        //insert values massively
        if ($insert_values) {
            Db::getInstance()->insert(
                'eci_attribute_value',
                $insert_values,
                false,
                false,
                Db::INSERT_IGNORE
            );
        }
        unset($insert_values);

        //clean void matching
        Db::getInstance()->execute(
            'DELETE t1
            FROM ' . _DB_PREFIX_ . 'eci_attribute_value_shop t1
            LEFT JOIN ' . _DB_PREFIX_ . 'eci_attribute_value t2
            ON t2.id_attribute_eco_value = t1.id_attribute_eco_value
            WHERE t2.id_attribute_eco_value IS NULL'
        );

        $this->matchAttributes();

        return true;
    }

    public function getProductInfo($reference, $connecteur, $iso_code)
    {
        $context = Context::getContext();
        $id_lang = Language::getIdByIso($iso_code);
        $context->language = new Language($id_lang);
        $class = __NAMESPACE__;
        $mod = new $class($context);
        $getStocks = self::existsEciStock($connecteur);

        $product = array();
        $produit = self::getProduct($reference, $connecteur);

        $list_images = array();
        $listK = '';
        if (!empty($produit['produit']['pictures'])) {
            if (is_array($produit['produit']['pictures'])) {
                foreach ($produit['produit']['pictures'] as $image) {
                    $list_images[] = $image['url'];
                }
            } else {
                $list_images = explode('//:://', $produit['produit']['pictures']);
            }
        }
        if (!empty($produit['declinaisons'])) {
            foreach ($produit['declinaisons'] as $declinaison) {
                $listK .= $declinaison['reference'];
                if (!empty($declinaison['pictures'])) {
                    if (is_array($declinaison['pictures'])) {
                        foreach ($declinaison['pictures'] as $image) {
                            $list_images[] = $image['url'];
                        }
                    } else {
                        array_merge($list_images, explode('//:://', $declinaison['pictures']));
                    }
                }
            }
        }
        $list_images = $list_images ?: array(0 => '');
        $product['images'] = array_unique($list_images);
        $nameLg = self::langExpl($produit['produit']['name']);
        $product['name'] = isset($nameLg[$iso_code]) ? $nameLg[$iso_code] : '';
        $descriptionLg = self::langExpl($produit['produit']['description']);
        $product['description'] = isset($descriptionLg[$iso_code]) ? $descriptionLg[$iso_code] : '';
        $product['reference'] = $reference;
        $product['manufacturer'] = $produit['produit']['manufacturer'];
        $product['price'] = $produit['produit']['price'];
        $product['pmvc'] = $produit['produit']['pmvc'];
        $product['rate'] = $produit['produit']['rate'];
        if (!empty($produit['declinaisons'])) {
            $product['attribute'] = array();
            foreach ($produit['declinaisons'] as $declinaison) {
                $declinaison['attribute'] = self::getAttributesDescription($declinaison['attribute'], $iso_code);
                if ($getStocks) {
                    $declinaison['stock'] = self::getEciStock($declinaison['reference_attribute'], $connecteur, $this->id_shop);
                }
                if (!$listK) {
                    unset($declinaison['reference']);
                }
                $product['attribute'][] = $declinaison;
            }
        } else {
            $product['attribute'] = null;
        }
        $product['features'] = array();
        if (!empty($produit['produit']['feature'])) {
            foreach ($produit['produit']['feature'] as $feat) {
                foreach ($feat as $iso => $tnv) {
                    if ($iso === $iso_code) {
                        $product['features'] = array_merge($product['features'], $tnv);
                    }
                }
            }
        }
        $categories = self::categoryBranchesToPaths($produit['produit']['category_group']);
        $product['category'] = array_map(array(get_class(), 'categoryGetAriane'), $categories);
        $product['pack'] = self::getPackComponents($reference, $connecteur);
        if ($getStocks) {
            $product['stock'] = self::getEciStock($reference, $connecteur, $this->id_shop);
        }

        return ['product' => $product];
    }

    public function setSelection($connecteur = null, $id_shop = 0, $from_controller = true)
    {
        if (is_null($connecteur)) {
            $connecteur = self::getConnecteurName();
        }
        if (!$id_shop) {
            $id_shop = $this->id_shop;
        }
        $force_import_pack_components = self::getECIConfigValue('FORCE_PACK_COMP', $id_shop, $connecteur);

        try {
            //add complete categories but not blacklisted categories and products
            $ret = Db::getInstance()->execute(
                'REPLACE INTO ' . _DB_PREFIX_ . 'eci_product_imported
                (reference, id_shop, imported, fournisseur)
                SELECT c.product_reference, "' . (int) $id_shop . '", "1", "' . pSQL($connecteur) . '"
                FROM ' . _DB_PREFIX_ . 'eci_catalog c
                LEFT JOIN ' . _DB_PREFIX_ . 'eci_category_shop s
                ON c.category LIKE CONCAT("%:://::", s.name, "%")
                AND s.fournisseur = c.fournisseur
                LEFT JOIN ' . _DB_PREFIX_ . 'eci_product_blacklist b
                ON b.reference = c.product_reference
                AND b.fournisseur = c.fournisseur
                WHERE c.fournisseur = "' . pSQL($connecteur) . '"
                AND (b.blacklist = 0 OR b.blacklist IS NULL)
                AND (b.id_shop = ' . (int) $id_shop . ' OR b.id_shop IS NULL)
                AND s.id_shop = ' . (int) $id_shop . '
                AND s.import = 1
                AND s.blacklist = 0'
            );

            //add non blacklisted pack components of non blacklisted packs if switch is set to force import of pack components
            if ($force_import_pack_components) {
                $ret &= Db::getInstance()->execute(
                    'REPLACE INTO ' . _DB_PREFIX_ . 'eci_product_imported
                    (reference, id_shop, imported, fournisseur)
                    SELECT p.product_reference, ip.id_shop, "1", p.fournisseur
                    FROM ' . _DB_PREFIX_ . 'eci_catalog_pack p
                    LEFT JOIN ' . _DB_PREFIX_ . 'eci_product_imported ip
                    ON p.pack_reference = ip.reference AND p.fournisseur = ip.fournisseur
                    LEFT JOIN ' . _DB_PREFIX_ . 'eci_product_blacklist bp
                    ON p.pack_reference = bp.reference AND p.fournisseur = bp.fournisseur AND bp.id_shop = ip.id_shop
                    LEFT JOIN ' . _DB_PREFIX_ . 'eci_product_blacklist bc
                    ON p.product_reference = bc.reference AND p.fournisseur = bc.fournisseur AND bc.id_shop = ip.id_shop
                    WHERE p.fournisseur = "' . pSQL($connecteur) . '"
                    AND ip.id_shop = ' . (int) $id_shop . '
                    AND (bp.blacklist is null OR bp.blacklist = 0)
                    AND (bc.blacklist is null OR bc.blacklist = 0)'
                );
            }

            //remove import of blacklisted categories (from the matching)
            $ret &= Db::getInstance()->execute(
                'DELETE i
                FROM ' . _DB_PREFIX_ . 'eci_product_imported i
                LEFT JOIN ' . _DB_PREFIX_ . 'eci_catalog c
                ON c.product_reference = i.reference
                AND c.fournisseur = i.fournisseur
                LEFT JOIN ' . _DB_PREFIX_ . 'eci_category_shop s
                ON c.category LIKE CONCAT("%:://::", s.name, "%")
                AND s.fournisseur = c.fournisseur
                AND s.id_shop = i.id_shop
                WHERE i.fournisseur = "' . pSQL($connecteur) . '"
                AND i.id_shop = ' . (int)$id_shop . '
                AND i.imported = 1
                AND s.blacklist = 1'
            );

            //remove import of blacklisted products
            $ret &= Db::getInstance()->execute(
                'DELETE i
                FROM ' . _DB_PREFIX_ . 'eci_product_imported i
                LEFT JOIN ' . _DB_PREFIX_ . 'eci_product_blacklist b
                ON b.reference = i.reference
                AND b.fournisseur = i.fournisseur
                AND b.id_shop = i.id_shop
                WHERE i.fournisseur = "' . pSQL($connecteur) . '"
                AND i.id_shop = ' . (int)$id_shop . '
                AND i.imported = 1
                AND b.blacklist = 1'
            );

            //remove import of already imported products
            $ret &= Db::getInstance()->execute(
                'DELETE i
                FROM ' . _DB_PREFIX_ . 'eci_product_imported i
                LEFT JOIN ' . _DB_PREFIX_ . 'eci_product_shop s
                ON s.reference = i.reference
                AND s.fournisseur = i.fournisseur
                AND s.id_shop = i.id_shop
                WHERE i.fournisseur = "' . pSQL($connecteur) . '"
                AND s.id_product_attribute = 0
                AND i.id_shop = ' . (int)$id_shop . '
                AND i.imported = 1
                AND s.imported = 0'
            );

            //remove import of packs if all components are not marked or already imported
            if (!$from_controller) {
                $ret &= Db::getInstance()->execute(
                    'DELETE i
                    FROM ' . _DB_PREFIX_ . 'eci_product_imported i
                    LEFT JOIN (
                        SELECT DISTINCT p.pack_reference as reference, ip.id_shop, p.fournisseur
                        FROM ' . _DB_PREFIX_ . 'eci_catalog_pack p
                        LEFT JOIN ' . _DB_PREFIX_ . 'eci_product_imported ip
                        ON p.pack_reference = ip.reference AND p.fournisseur = ip.fournisseur
                        LEFT JOIN ' . _DB_PREFIX_ . 'eci_product_imported ic
                        ON p.product_reference = ic.reference AND p.fournisseur = ic.fournisseur AND ic.id_shop = ip.id_shop
                        LEFT JOIN ' . _DB_PREFIX_ . 'eci_product_shop s
                        ON p.product_reference = s.reference AND p.fournisseur = s.fournisseur AND s.id_shop = ip.id_shop
                        WHERE ip.imported = 1
                        AND s.id_product_attribute = 0
                        AND ic.imported is null
                        AND s.imported is null
                    ) d
                    ON i.reference = d.reference AND i.id_shop = d.id_shop AND i.fournisseur = d.fournisseur
                    WHERE i.id_shop = ' . (int)$id_shop . '
                    AND i.fournisseur = "' . pSQL($connecteur) . '"
                    AND d.reference IS NOT NULL'
                );
            }
        } catch (Exception $e) {
            return $e->getMessage();
        }

        return (bool) $ret;
    }

    public static function deployCategories($connecteur)
    {
        $iso_lang = Language::getIsoById(Configuration::get('PS_LANG_DEFAULT'));
        $list_shops = Shop::getCompleteListOfShopsID();
        $categories = Db::getInstance()->executeS(
            'SELECT DISTINCT category_group
            FROM ' . _DB_PREFIX_ . 'eci_catalog
            WHERE fournisseur = "' . pSQL($connecteur) . '"
            ORDER BY category'
        );

        Db::getInstance()->update(
            'eci_category_shop',
            array('populated' => 0),
            'fournisseur = "' . pSQL($connecteur) . '"'
        );

        $tab_insert_cat = array();
        foreach ($categories as $category) {
            $ml_paths = self::categoryBranchesToPaths($category['category_group']);
            foreach ($ml_paths as $id_path => $ml_path) {
                $tab_cat_lg = self::langExpl($ml_path);
                if (isset($tab_cat_lg[$iso_lang])) {
                    $cat_arbo = $tab_cat_lg[$iso_lang];
                } else {
                    $cat_arbo = reset($tab_cat_lg);
                }

                $val_insert_pop = array();
                $val_insert_sec = array();
                $tab_cat_arbo_part = self::categoryPathToPaths($cat_arbo);
                foreach ($tab_cat_arbo_part as $cat_arbo_part) {
                    foreach ($list_shops as $id_shop) {
                        if ((self::ALL_PATHS_ARE_POPULATED || 0 == $id_path) && empty($tab_insert_cat[$connecteur.'--'.$cat_arbo_part.'--'.$id_shop])) {
                            $tab_insert_cat[$connecteur.'--'.$cat_arbo_part.'--'.$id_shop] = 1;
                            $val_insert_pop[] = '("' . pSQL($cat_arbo_part) . '", '
                                . '"' . pSQL($connecteur) . '", '
                                . '"' . (int) $id_shop . '", '
                                . '"1", '
                                . '"' . pSQL($iso_lang) . '")';
                        } elseif (!isset($tab_insert_cat[$connecteur.'--'.$cat_arbo_part.'--'.$id_shop])) {
                            $tab_insert_cat[$connecteur.'--'.$cat_arbo_part.'--'.$id_shop] = 0;
                            $val_insert_sec[] = '("' . pSQL($cat_arbo_part) . '", '
                                . '"' . pSQL($connecteur) . '", '
                                . '"' . (int) $id_shop . '", '
                                . '"0", '
                                . '"' . pSQL($iso_lang) . '")';
                        }
                    }
                }
                if ($val_insert_pop) {
                    Db::getInstance()->execute(
                        'INSERT INTO ' . _DB_PREFIX_ . 'eci_category_shop '
                        . '(name, fournisseur, id_shop, populated, iso_code) '
                        . 'VALUES ' . implode(', ', $val_insert_pop)
                        . ' ON DUPLICATE KEY UPDATE populated=VALUES(populated), iso_code=VALUES(iso_code)'
                    );
                }
                if ($val_insert_sec) {
                    Db::getInstance()->execute(
                        'INSERT INTO ' . _DB_PREFIX_ . 'eci_category_shop '
                        . '(name, fournisseur, id_shop, populated, iso_code) '
                        . 'VALUES ' . implode(', ', $val_insert_sec)
                        . ' ON DUPLICATE KEY UPDATE iso_code=VALUES(iso_code)'
                    );
                }
            }
        }
        Db::getInstance()->update(
            'eci_category_shop',
            array('iso_code' => pSQL($iso_lang)),
            'iso_code = ""'
        );

        return true;
    }

    public function setCategorieConToCategorieLoc($connecteur, $categorieConnecteur, $categorieLocale, $id_shop = 0, $id_lang = 0)
    {
        if (!$id_shop) {
            $id_shop = $this->id_shop;
        }
        if (!$id_lang) {
            $id_lang = Configuration::get('PS_LANG_DEFAULT');
        }
        $iso_lang = Language::getIsoById($id_lang);
        //extract category in default language for matching
        $categLg = self::langExpl($categorieConnecteur);
        if (!isset($categLg[$iso_lang])) {
            $id_lang = Configuration::get('PS_LANG_DEFAULT');
            $iso_lang = Language::getIsoById($iso_lang);
            self::updateInfoEco('ID_LANG', $id_lang);
        }
        $name = (isset($categLg[$iso_lang])) ? $categLg[$iso_lang] : $categorieConnecteur;

        $db = Db::getInstance();
        $id_category = $db->getValue(
            'SELECT id_category
            FROM ' . _DB_PREFIX_ . 'eci_category_shop
            WHERE name=\'' . pSQL($name) . '\'
            AND fournisseur=\'' . pSQL($connecteur) . '\'
            AND id_shop=' . (int) $id_shop
        );
        if ((false === $id_category) && $categorieLocale) {
            return $db->insert(
                'eci_category_shop',
                array(
                    'name' => pSQL($name),
                    'fournisseur' => pSQL($connecteur),
                    'id_category' => (int) $categorieLocale,
                    'id_shop' => (int) $id_shop
                )
            );
        } elseif ($id_category && (!$categorieLocale)) {
            return $db->delete(
                'eci_category_shop',
                'name=\'' . pSQL($name) . '\' AND fournisseur=\'' . pSQL($connecteur) . '\' AND id_shop=' . (int) $id_shop
            );
        } elseif ($categorieLocale != $id_category) {
            return $db->update(
                'eci_category_shop',
                array('id_category' => (int) $categorieLocale),
                'name=\'' . pSQL($name) . '\' AND fournisseur=\'' . pSQL($connecteur) . '\' AND id_shop=' . (int) $id_shop
            );
        }

        return true;
    }

    public function getConnecteurParameter($ec_four, $forced_id_shop = null)
    {
        $id_shop = $forced_id_shop ?? $this->id_shop;
        if (0 !== $id_shop && !in_array($id_shop, Shop::getCompleteListOfShopsID())) {
            Db::getInstance()->execute(
                'DELETE c
                FROM ' . _DB_PREFIX_ . 'eci_config c
                LEFT JOIN ' . _DB_PREFIX_ . 'shop s
                ON c.id_shop = s.id_shop
                WHERE c.id_shop != 0
                AND (s.id_shop IS NULL OR s.deleted = 1)'
            );
        }
        $config = self::getECIConfigFournisseur($id_shop, $ec_four->fournisseur_glob);
        return self::getConfigCompleted(
            $config,
            array_merge(self::$tabParamsValid, (isset($ec_four->valid_conf_keys) ? $ec_four->valid_conf_keys : array())),
            array_merge(self::$tabParamsNumeric, (isset($ec_four->numeric_conf_keys) ? $ec_four->numeric_conf_keys : array())),
            array_merge(self::$tabParamsMultiple, (isset($ec_four->multiple_conf_keys) ? $ec_four->multiple_conf_keys : array())),
            array_merge(self::$tabParamsArray, (isset($ec_four->array_conf_keys) ? $ec_four->array_conf_keys : array())),
            array_merge(self::$tabParamsFiles, $ec_four->uploaded_files ?? array()),
            array_merge(self::$tabParamsLang, $ec_four->lang_keys ?? array())
        );
    }

    public static function getConfigCompleted($config0, $valid_conf_keys = array(), $numeric_conf_keys = array(), $multiple_conf_keys = array(), $array_conf_keys = array(), $uploaded_files = array(), $lang_keys = array())
    {
        $config = array();
        foreach ($valid_conf_keys as $param) {
            if (!isset($config0[$param])) {
                if (in_array($param, $array_conf_keys) || in_array($param, $multiple_conf_keys)) {
                    $config[$param] = array();
                } elseif (in_array($param, $numeric_conf_keys)) {
                    $config[$param] = 0;
                } else {
                    $config[$param] = '';
                }
            } else {
                if (in_array($param, $array_conf_keys) || in_array($param, $multiple_conf_keys)) {
                    $config[$param] = Tools::jsonDecode($config0[$param], true);
                    if (is_null($config[$param])) {
                        $config[$param] = array();
                    }
                } elseif (in_array($param, $lang_keys)) {
                    $config[$param] = Tools::jsonDecode($config0[$param], true);
                    if (is_null($config[$param])) {
                        $config[$param] = self::getMultiLangField($config0[$param]);
                    }
                } else {
                    $config[$param] = $config0[$param];
                }
            }
        }

        return $config;
    }

    public static function updateParamFournisseur($config, $fournisseur, $id_shop, $valid_conf_keys = array(), $numeric_conf_keys = array(), $multiple_conf_keys = array(), $array_conf_keys = array(), $allshop_keys = array(), $uploaded_files = array(), $lang_keys = array())
    {
        $allShops = Shop::getCompleteListOfShopsID();
        $allShops[] = 0;
        $ok = true;

        if (!empty($multiple_conf_keys)) {
            foreach ($multiple_conf_keys as $key) {
                $liste = array();
                $matches = array();
                preg_match_all('/(\?|\&)' . $key . '\=([^\&]+)/', $config, $matches);
                if (empty($matches[2])) {
                    continue;
                }
                foreach ($matches[2] as $val) {
                    $liste[] = self::encodeStr(urldecode($val));
                }
                if (empty($liste)) {
                    continue;
                }
                if (in_array($key, $allshop_keys)) {
                    foreach ($allShops as $shop) {
                        self::setECIConfigValue($key, Tools::jsonEncode($liste), $shop, $fournisseur);
                    }
                } else {
                    self::setECIConfigValue($key, Tools::jsonEncode($liste), $id_shop, $fournisseur);
                }
            }
        }

        $configuration = array();
        parse_str($config, $configuration);
        foreach ($configuration as $key => $value) {
            if (in_array($key, $multiple_conf_keys) ||
                in_array($key, $array_conf_keys) ||
                !in_array($key, $valid_conf_keys) ||
                'protected' === $value) {
                continue;
            }

            if (in_array($key, $numeric_conf_keys)) {
                $value_corr = str_replace(',', '.', $value);
                if (is_numeric($value_corr)) {
                    $value = $value_corr;
                } else {
                    $ok = false;
                    $value = 0;
                }
            }
            if (in_array($key, $allshop_keys) && (!in_array($key, $uploaded_files) || (in_array($key, $uploaded_files) && !empty($value)))) {
                foreach ($allShops as $shop) {
                    self::setECIConfigValue($key, $value, $shop, $fournisseur);
                }
            } elseif (!in_array($key, $uploaded_files) || (in_array($key, $uploaded_files) && !empty($value))) {
                self::setECIConfigValue($key, $value, $id_shop, $fournisseur);
            }
        }

        if (!empty($array_conf_keys)) {
            foreach ($array_conf_keys as $key) {
                if (!isset($configuration[$key])) {
                    continue;
                }

                $value = is_array($configuration[$key]) ? $configuration[$key] : array();
                if (in_array($key, $allshop_keys) && (!in_array($key, $uploaded_files) || (in_array($key, $uploaded_files) && !empty($value)))) {
                    foreach ($allShops as $shop) {
                        self::setECIConfigValue($key, Tools::jsonEncode($value), $shop, $fournisseur);
                    }
                } elseif (!in_array($key, $uploaded_files) || (in_array($key, $uploaded_files) && !empty($value))) {
                    self::setECIConfigValue($key, Tools::jsonEncode($value), $id_shop, $fournisseur);
                }
            }
        }

        if (!empty($lang_keys)) {
            $lang_ids = Language::getIDs(true);
            foreach ($lang_keys as $key) {
                $value = array();
                foreach ($lang_ids as $id_lang) {
                    if (!isset($configuration[$key . '_' . $id_lang])) {
                        continue;
                    }
                    $value[$id_lang] = $configuration[$key . '_' . $id_lang];
                }

                if (in_array($key, $allshop_keys) && (!in_array($key, $uploaded_files) || (in_array($key, $uploaded_files) && !empty($value)))) {
                    foreach ($allShops as $shop) {
                        self::setECIConfigValue($key, Tools::jsonEncode($value), $shop, $fournisseur);
                    }
                } elseif (!in_array($key, $uploaded_files) || (in_array($key, $uploaded_files) && !empty($value))) {
                    self::setECIConfigValue($key, Tools::jsonEncode($value), $id_shop, $fournisseur);
                }
            }
        }

        if (!$ok) {
            return Tools::jsonEncode(array('mc' => '22', 'suxs' => 'nosuccess'));
        }

        return Tools::jsonEncode(array('mc' => '20', 'suxs' => 'success'));
    }

    public static function sendOrderState($id_order, $id_order_state)
    {
        $db = Db::getInstance();
        $order = array(
            'id_order' => $id_order,
            'newOrderStatus' => $id_order_state
        );

        // find id_shop
        $id_shop = $db->getValue(
            '
            SELECT id_shop
            FROM  '._DB_PREFIX_.'orders
            WHERE id_order = ' . (int) $id_order
        );

        // retrieve details and build the list of implied suppliers
        $l_order_suppliers = self::getOrderSuppliersList($id_order);

        $order_suppliers = array();
        foreach ($l_order_suppliers as $id_supplier) {
            $order_suppliers[$id_supplier] = 1;
        }

        // build the list of configured and active suppliers
        $l_ec_suppliers = $db->executeS(
            'SELECT id_manufacturer, name, perso
            FROM '._DB_PREFIX_.'eci_fournisseur
            WHERE 1;'
        );

        $o_ec_suppliers = array();
        foreach ($l_ec_suppliers as $line) {
            if (array_key_exists($line['id_manufacturer'], $order_suppliers)) {
                $o_ec_suppliers[$line['name']] = ($line['perso'] == 1 ? true : false);
            }
        }

        // for each implied supplier :
        // if supplier is active and set for automatic orders export : send order state
        foreach ($o_ec_suppliers as $fournisseur => $active) {
            // confirm activity and automatic sending
            $supplier_active = (bool) self::getECIConfigValue('ACTIVE', $id_shop, $fournisseur);
            $supplier_auto = (bool) self::getECIConfigValue('IMPORT_AUTO', $id_shop, $fournisseur);
            $order_to_send = $active && $supplier_active && $supplier_auto;
            if ($order_to_send) {
                if (($classF = self::getGenClassStatic($fournisseur, $id_shop))) {
                    if (method_exists($classF, 'sendOrderState')) {
                        $classF->sendOrderState($order);
                    }
                }
            }
        }
    }

    public static function synchroManuelOrder($id_order, $typ, $connecteur)
    {
        self::setOrderVisibleCon($id_order, $connecteur, $typ);
        if ($typ == 1) {
            return self::generateSynchroOrder($id_order, true);
        }
    }

    public static function generateSynchroOrder($id_order, $manual = false)
    {
        $db = Db::getInstance();
        $order = array('id_order' => $id_order);

        // find id_shop
        $id_shop = $db->getValue(
            'SELECT id_shop
            FROM  '._DB_PREFIX_.'orders
            WHERE id_order = ' . (int) $id_order
        );

        // retrieve details and build the list of implied suppliers
        $l_order_suppliers = self::getOrderSuppliersList($id_order);

        $order_suppliers = array();
        foreach ($l_order_suppliers as $id_supplier) {
            $order_suppliers[$id_supplier] = 1;
        }

        // build the list of configured and active suppliers
        $l_ec_suppliers = $db->executeS(
            'SELECT id_manufacturer, name, perso
            FROM '._DB_PREFIX_.'eci_fournisseur
            WHERE 1;'
        );

        $o_ec_suppliers = array();
        foreach ($l_ec_suppliers as $line) {
            if (array_key_exists($line['id_manufacturer'], $order_suppliers)) {
                $o_ec_suppliers[$line['name']] = ($line['perso'] == 1 ? true : false);
            }
        }

        // for each implied supplier :
        // if supplier is active and set for automatic orders export : send order and set invisible order in history table
        // else just set visible order in history table
        $ok = array();
        foreach ($o_ec_suppliers as $fournisseur => $active) {
            // confirm activity and automatic sending
            $supplier_active = (bool) self::getECIConfigValue('ACTIVE', $id_shop, $fournisseur);
            $supplier_auto = (bool) self::getECIConfigValue('IMPORT_AUTO', $id_shop, $fournisseur);
            $order_to_send = $active && $supplier_active && ($supplier_auto || $manual);
            $visible = true;
            if ($order_to_send) {
                if (($classF = self::getGenClassStatic($fournisseur, $id_shop))) {
                    if (method_exists($classF, 'sendOrder')) {
                        $visible = !$classF->sendOrder($order);
                        $ok[] = $fournisseur;
                    }
                }
            }
            self::setOrderVisibleCon($id_order, $fournisseur, $visible);
        }

        return Tools::jsonEncode($ok);
    }

    public function generateSynchroSlip($order_reference)
    {
        $orders = Order::getByReference($order_reference);
        foreach ($orders as $order) {
            $id_order = $order->id;
            break;
        }

        $db = Db::getInstance();
        $order = array('id_order' => $id_order);

        // find id_shop
        $id_shop = $db->getValue(
            'SELECT id_shop
            FROM  '._DB_PREFIX_.'orders
            WHERE id_order = ' . (int) $id_order
        );

        // retrieve details and build the list of implied suppliers
        $l_order_suppliers = $db->executeS(
            'SELECT DISTINCT p.id_supplier
            FROM '._DB_PREFIX_.'order_detail od
            LEFT JOIN '._DB_PREFIX_.'product p
            ON od.product_id = p.id_product
            WHERE od.id_order = ' . (int) $id_order
        );
        $order_suppliers = array();
        foreach ($l_order_suppliers as $line) {
            $order_suppliers[$line['id_supplier']] = 1;
        }

        // build the list of configured and active suppliers
        $l_ec_suppliers = $db->executeS(
            'SELECT id_manufacturer, name, perso
            FROM '._DB_PREFIX_.'eci_fournisseur
            WHERE 1;'
        );
        $o_ec_suppliers = array();
        foreach ($l_ec_suppliers as $line) {
            if (isset($order_suppliers[$line['id_manufacturer']])) {
                $o_ec_suppliers[$line['name']] = ($line['perso'] == 1 ? true : false);
            }
        }

        // for each implied supplier :
        // if supplier is active and set for automatic orders export : send order and set invisible order in history table
        // else just set visible order in history table
        foreach ($o_ec_suppliers as $fournisseur => $active) {
            // confirm activity and automatic sending
            $supplier_active = (bool) self::getECIConfigValue('ACTIVE', $id_shop, $fournisseur);
            $supplier_auto = (bool) self::getECIConfigValue('IMPORT_AUTO', $id_shop, $fournisseur);
            $order_to_send = $active && $supplier_active && $supplier_auto;
            if ($order_to_send) {
                if (($classF = self::getGenClassStatic($fournisseur, $id_shop))) {
                    if (method_exists($classF, 'sendOrderSlip')) {
                        $classF->sendOrderSlip($order);
                    }
                }
            }
        }
    }

    public function getCustomerConnected($id_customer, $id_shop = 0)
    {
        if (!$id_shop) {
            $id_shop = $this->id_shop;
        }

        $l_ec_suppliers = Db::getInstance()->executeS(
            'SELECT name
            FROM '._DB_PREFIX_.'eci_fournisseur
            WHERE perso = 1;'
        );
        foreach ($l_ec_suppliers as $lf) {
            $fournisseur = $lf['name'];
            // confirm activity
            if (!self::getECIConfigValue('ACTIVE', $id_shop, $fournisseur)) {
                continue;
            }
            if (($classF = $this->getGenClass($fournisseur, $id_shop))) {
                if (method_exists($classF, 'getCustomerConnected')) {
                    $classF->getCustomerConnected($id_customer, $id_shop);
                }
            }
        }
    }

    public function executeSQLFile($file, $fourn)
    {
        $path = dirname(__FILE__) . '/../gen/' . $fourn . '/sql/';
        if (!file_exists($path . $file)) {
            return false;
        } elseif (!$sql = Tools::file_get_contents($path . $file)) {
            return false;
        }
        $sql = preg_split("/;\s*[\r\n]+/", str_replace('MYSQL_ENGINE', 'InnoDB', str_replace('PREFIX_', _DB_PREFIX_, $sql)));
        foreach ($sql as $query) {
            $query = trim($query);
            if ($query) {
                if (!Db::getInstance()->execute($query)) {
                    $this->_postErrors[] = Db::getInstance()->getMsgError() . ' ' . $query;
                    return false;
                }
            }
        }
        return true;
    }

    public static function encodeStr($data)
    {
        return preg_replace_callback(
            '/[^A-Za-z0-9]/',
            function ($matches) {
                return 'SpChN' . ord($matches[0]) . 'E';
            },
            $data
        );
    }

    public static function decodeStr($data)
    {
        return preg_replace_callback(
            '/SpChN([0-9]+)E/',
            function ($matches) {
                return chr($matches[1]);
            },
            $data
        );
    }

    private static function getStrCon($d, $f, $t)
    {
        $tab = unserialize(self::$d($t));
        self::putInArray($tab, array(0, 2), json_encode(array('mod' => self::getConnecteurName(), 'dom' => Tools::getShopDomain())));

        return self::$d($f)($tab);
    }

    private static function d4($a)
    {
        return implode(array_map('chr', array_map(array(get_class(), 'toInt'), array_map('hexdec', str_split($a, 2)))));
    }

    public static function toInt($a)
    {
        return (int) ltrim($a, '0');
    }

    public static function isNotVoidString($data)
    {
        return (is_string($data) && !Tools::strlen($data)) ? false : true;
    }

    public static function uniqueMultidimArray($array, $key)
    {
        $temp_array = array();
        $i = 0;
        $key_array = array();

        foreach ($array as $val) {
            if (!in_array($val[$key], $key_array)) {
                $key_array[$i] = $val[$key];
                $temp_array[$i] = $val;
            }
            $i++;
        }
        return $temp_array;
    }

    /**
     * filter array by value in a column
     *
     * @param array $array multidimensional array like from a select
     * @param string $key
     * @param string $value
     * @param bool $only_first
     * @return array
     */
    public static function getSelectInArray($array, $key, $value, $only_first = false)
    {
        if (!is_array($array)) {
            return false;
        }

        $keys = array_keys($array);
        $values = array_column($array, $key);
        if (count($keys) == count($values)) {
            $good_extract = array_intersect(array_combine($keys, $values), array($value));
            if ($only_first) {
                $good_keys = array_keys($good_extract);
                return $array[reset($good_keys)];
            }
            return array_intersect_key($array, $good_extract);
        }

        $result = array();
        foreach ($array as $k => $line) {
            if (is_array($line) && isset($line[$key]) && $value == $line[$key]) {
                if ($only_first) {
                    return $line;
                }
                $result[$k] = $line;
            }
        }

        return $result;
    }

    /**
     * puts sth in an array, following a numeric path
     * @param array $a
     * @param array $tpos array of successive positions
     * @param any $o objet to put
     */
    public static function putInArray(&$a, $tpos, $o)
    {
        if (!is_array($a) || !is_array($tpos)) {
            return false;
        }

        $pos = array_shift($tpos);
        if (is_null($pos)) {
            return;
        }
        $last = empty($tpos);

        $i = 0;
        foreach ($a as &$v) {
            if ($i < $pos) {
                $i++;
                continue;
            }
            if ($last) {
                $v = $o;
                return;
            }

            self::putInArray($v, $tpos, $o);
        }
    }

    public static function getInArray($a, $tpos)
    {
        if (!is_array($a) || !is_array($tpos)) {
            return false;
        }

        $pos = array_shift($tpos);
        if (is_null($pos)) {
            return false;
        }
        $last = empty($tpos);

        $i = 0;
        foreach ($a as $v) {
            if ($i < $pos) {
                $i++;
                continue;
            }
            if ($last) {
                return $v;
            }

            return self::getInArray($v, $tpos);
        }
    }
    
    public function convertBase($input, $fromBase, $intoBase)
    {
        if (!is_array($fromBase)) {
            $fromBase = str_split((string) $fromBase, 1);
        }
        if (!is_array($intoBase)) {
            $intoBase = str_split((string) $intoBase, 1);
        }
        $number = str_split((string) $input, 1);

        foreach ($number as $digit) {
            if (!in_array($digit, $fromBase)) {
                return false;
            }
        }

        if ($fromBase == $intoBase) {
            return $input;
        }

        $decimalBase = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'];
        $decimalInput = ($decimalBase == $fromBase);
        $decimalOutput = ($decimalBase == $intoBase);

        $fromLen = count($fromBase);
        $toLen = count($intoBase);
        $numberLen = strlen($input);
        $retval = '';

        //special case of decimal target base
        if ($decimalOutput) {
            $retval = 0;
            for ($i = 1; $i <= $numberLen; $i++) {
                $retval = bcadd($retval, bcmul(array_search($number[$i - 1], $fromBase), bcpow($fromLen, $numberLen - $i)));
            }

            return $retval;
        }

        if (!$decimalInput) {
            $base10 = self::convertBase($input, $fromBase, $decimalBase);
        } else {
            $base10 = $input;
        }
        if ($base10 < $toLen) {
            return $intoBase[$base10];
        }
        while ($base10 != '0') {
            $retval = $intoBase[bcmod($base10, $toLen)].$retval;
            $base10 = bcdiv($base10, $toLen, 0);
        }

        return $retval;
    }

    public static function setECIConfigValue($name, $value, $id_shop = 0, $fournisseur = 'default')
    {
        self::calcECIConfig($id_shop, $fournisseur);
        Db::getInstance()->insert(
            'eci_config',
            array(
                'name' => pSQL($name),
                'value' => pSQL($value),
                'id_shop' => (int) $id_shop,
                'fournisseur' => pSQL($fournisseur)),
            false,
            false,
            Db::ON_DUPLICATE_KEY
        );
    }

    public static function setECIConfigFourDefaultShop($id_shop = 0, $fournisseur = 'default')
    {
        self::calcECIConfig($id_shop, $fournisseur);
        $config = Db::getInstance()->executeS(
            'SELECT name,value
            FROM ' . _DB_PREFIX_ . 'eci_config
            WHERE id_shop=' . (int) $id_shop . '
            AND fournisseur = "default"'
        );
        foreach ($config as $config_line) {
            Db::getInstance()->update(
                'eci_config',
                array(
                    'value' => $config_line['value'],
                    ),
                'id_shop = ' . (int) $id_shop . '
                AND fournisseur = "' . pSQL($fournisseur) . '"
                AND name = "' . pSQL($config_line['name']) . '"'
            );
        }
    }

    public static function getECIConfigValue($name, $id_shop = 0, $fournisseur = 'default')
    {
        $val = Db::getInstance()->getValue(
            'SELECT value FROM ' . _DB_PREFIX_ . 'eci_config
            WHERE name="' . pSQL($name) . '"
            AND id_shop=' . (int) $id_shop . '
            AND fournisseur="' . pSQL($fournisseur) . '"'
        );
        if ($val === null) {
            self::calcECIConfig($id_shop, $fournisseur);
            return self::getECIConfigValue($name, $id_shop, $fournisseur);
        }
        return $val;
    }

    public static function getECIConfigValueMultiShop($name, $fournisseur = 'default')
    {
        $tab_vals = array();
        $shops = Shop::getCompleteListOfShopsID();
        foreach ($shops as $id_shop) {
            $tab_vals[$id_shop] = '';
        }
        $vals = Db::getInstance()->executeS(
            'SELECT id_shop, value FROM ' . _DB_PREFIX_ . 'eci_config
            WHERE name="' . pSQL($name) . '"
            AND fournisseur="' . pSQL($fournisseur) . '"'
        );
        foreach ($vals as $val) {
            $tab_vals[$val['id_shop']] = $val['value'];
        }

        return $tab_vals;
    }

    public static function getECIConfigValueAllShops($name, $fournisseur = 'default')
    {
        $values = array();
        $values_shopped = self::getECIConfigValueMultiShop($name, $fournisseur);
        foreach ($values_shopped as $val_json) {
            if (!is_null($val = self::arJsonDecodeRecur($val_json))) {
                $values = array_unique(array_merge($values, (is_array($val)) ? $val : array($val)));
            }
        }

        return $values;
    }

    public static function getECIConfigFournisseur($id_shop = 0, $fournisseur = 'default')
    {
        self::calcECIConfig($id_shop, $fournisseur);
        $config = array();
        $conf1 = Db::getInstance()->executeS(
            'SELECT name,value FROM ' . _DB_PREFIX_ . 'eci_config
            WHERE id_shop=' . (int) $id_shop . '
            AND fournisseur="default"'
        );
        foreach ($conf1 as $ligne) {
            $config[$ligne['name']] = $ligne['value'];
        }
        $conf2 = Db::getInstance()->executeS(
            'SELECT name,value FROM ' . _DB_PREFIX_ . 'eci_config
            WHERE id_shop=' . (int) $id_shop . '
            AND fournisseur="' . pSQL($fournisseur) . '"'
        );
        foreach ($conf2 as $ligne) {
            $config[$ligne['name']] = $ligne['value'];
        }
        return $config;
    }

    public static function getECIConfigLike($name, $id_shop = 0, $fournisseur = 'default')
    {
        self::calcECIConfig($id_shop, $fournisseur);
        $config = array();
        $conf1 = Db::getInstance()->executeS(
            'SELECT name,value FROM ' . _DB_PREFIX_ . 'eci_config
            WHERE name LIKE "' . pSQL($name). '"
            AND id_shop=' . (int) $id_shop . '
            AND fournisseur="default"'
        );
        foreach ($conf1 as $ligne) {
            $config[$ligne['name']] = $ligne['value'];
        }
        $conf2 = Db::getInstance()->executeS(
            'SELECT name,value FROM ' . _DB_PREFIX_ . 'eci_config
            WHERE name LIKE "' . pSQL($name). '"
            AND id_shop=' . (int) $id_shop . '
            AND fournisseur="' . pSQL($fournisseur) . '"'
        );
        foreach ($conf2 as $ligne) {
            $config[$ligne['name']] = $ligne['value'];
        }

        return $config;
    }

    public static function calcECIConfig($id_shop = 0, $fournisseur = 'default')
    {
        if ($id_shop != 0) {
            $conf0d = Db::getInstance()->executeS(
                'SELECT name,value,' . (int) $id_shop . ' as id_shop,fournisseur
                FROM ' . _DB_PREFIX_ . 'eci_config
                WHERE id_shop = 0
                AND fournisseur = "default"'
            );
            Db::getInstance()->insert('eci_config', $conf0d, false, false, Db::INSERT_IGNORE);
        }
        if ($fournisseur != 'default') {
            $confsd = Db::getInstance()->executeS(
                'SELECT name,value,' . (int) $id_shop . ' as id_shop,"' . pSQL($fournisseur) . '" as fournisseur
                FROM ' . _DB_PREFIX_ . 'eci_config
                WHERE id_shop = ' . (int) $id_shop . '
                AND fournisseur = "default"'
            );
            Db::getInstance()->insert('eci_config', $confsd, false, false, Db::INSERT_IGNORE);
        }
        if (($id_shop != 0) && ($fournisseur != 'default')) {
            $confsf = Db::getInstance()->executeS(
                'SELECT name,value,' . (int) $id_shop . ' as id_shop,fournisseur
                FROM ' . _DB_PREFIX_ . 'eci_config
                WHERE id_shop = 0
                AND fournisseur = "' . pSQL($fournisseur) . '"'
            );
            Db::getInstance()->insert('eci_config', $confsf, false, false, Db::INSERT_IGNORE);
        }
    }

    public static function jGet($name)
    {
        return Db::getInstance()->getValue(
            'SELECT value
            FROM ' . _DB_PREFIX_ . 'eci_jobs
            WHERE name = "' . pSQL($name) . '"'
        );
    }

    public static function jGetNLike($name)
    {
        return Db::getInstance()->getValue(
            'SELECT COUINT(*)
            FROM ' . _DB_PREFIX_ . 'eci_jobs
            WHERE name LIKE "' . pSQL($name) . '"'
        );
    }

    public static function jGetLike($name)
    {
        $result = Db::getInstance()->executeS(
            'SELECT name, value
            FROM ' . _DB_PREFIX_ . 'eci_jobs
            WHERE name LIKE "' . pSQL($name) . '"'
        );
        if ($result) {
            $ret = array();
            foreach ($result as $line) {
                $ret[$line['name']] = $line['value'];
            }

            return $ret;
        }

        return false;
    }

    public static function jUpdateValue($name, $value)
    {
        Db::getInstance()->insert(
            'eci_jobs',
            array(
                'name' => pSQL($name),
                'value' => pSQL($value)
            ),
            false,
            false,
            Db::ON_DUPLICATE_KEY
        );

        if (empty($value)) {
            return;
        }

        $matches = array();
        preg_match('/^ECI_([^_]+)_(.*)_(.*)$/', $name, $matches);
        if (empty($matches[0]) || count($matches) < 4) {
            return;
        }

        switch ($matches[2]) {
            case 'START_TIME':
                Db::getInstance()->insert(
                    'eci_jobs_history',
                    array(
                        'task' => pSQL($matches[1]),
                        'state' => pSQL('START'),
                        'fournisseur' => pSQL($matches[3])
                    )
                );
                $id = Db::getInstance()->Insert_ID();
                Db::getInstance()->update(
                    'eci_jobs_history',
                    array(
                        'jid' => (int) $id
                    ),
                    'id_eci_jobs_history = ' . (int) $id
                );
                return;
            case 'END_TIME':
            case 'MESSAGE':
                $jid = Db::getInstance()->getValue(
                    'SELECT h.jid
                    FROM '._DB_PREFIX_.'eci_jobs_history h
                    JOIN '._DB_PREFIX_.'eci_jobs j
                    WHERE h.ts = j.value
                    AND j.name LIKE "ECI_' . pSQL($matches[1]) . '_START_TIME_' . pSQL($matches[3]) . '"'
                );
                $insert = array(
                    'task' => pSQL($matches[1]),
                    'jid' => (int) $jid,
                    'fournisseur' => pSQL($matches[3])
                );
                break;
            default:
                return;
        }
        switch ($matches[2]) {
            case 'MESSAGE':
                $insert['state'] = pSQL('MESSAGE');
                $insert['msg'] = pSQL($value);
                break;
            case 'END_TIME':
                $insert['state'] = pSQL('END');
                break;
            default:
                return;
        }
        Db::getInstance()->insert(
            'eci_jobs_history',
            $insert
        );
    }

    public static function getOneActiveShopByShare($connecteur)
    {
        $listShops = array();
        if (Shop::isFeatureActive()) {
            $shop_groups = ShopGroup::getShopGroups(true);
            foreach ($shop_groups as $shop_group) {
                $shops = ShopGroup::getShopsFromGroup($shop_group->id);
                foreach ($shops as $tab_shop) {
                    $shop = Shop::getShop($tab_shop['id_shop']);
                    if ($shop['active'] && self::getECIConfigValue('ACTIVE', $shop['id_shop'], $connecteur)) {
                        $listShops[] = $shop['id_shop'];
                        if ($shop_group->share_stock) {
                            break;
                        }
                    }
                }
            }
            sort($listShops);
        } else {
            $listShops = array(Configuration::get('PS_SHOP_DEFAULT'));
        }

        return $listShops;
    }

    public static function getFirstActiveShop($connecteur = null)
    {
        if (is_null($connecteur)) {
            $connecteur = self::getConnecteurName();
        }

        $actives = self::getECIConfigValueMultiShop('ACTIVE', $connecteur);
        unset($actives[0]);
        ksort($actives);

        foreach ($actives as $first_active => $active) {
            if ($active) {
                return $first_active;
            }
        }
    }
    
    public static function setNullStock($id_product = null, $id_shop = null, $list_idpa = false)
    {
        if (empty($id_product) && empty($id_shop)) {
            return false;
        }

        if (!is_null($id_shop)) {
            $id_shop = (int) $id_shop;
        }

        if (!$list_idpa) {
            $list_idpa = Product::getProductAttributesIds($id_product);
        } elseif (!is_array($list_idpa)) {
            $list_idpa = array(array('id_product_attribute' => (int) $list_idpa));
        }

        if ($list_idpa) {
            foreach ($list_idpa as $line) {
                StockAvailable::setQuantity((int) $id_product, (int) $line['id_product_attribute'], 0, $id_shop, false);
            }
        } else {
            StockAvailable::setQuantity((int) $id_product, 0, 0, $id_shop, false);
        }

        return true;
    }

    public static function resetStock($connecteur = null, $id_shop = 0)
    {
        if (is_null($connecteur)) {
            $connecteur = self::getConnecteurName();
        }

        return Db::getInstance()->update(
            'eci_stock',
            array(
                'upd_flag' => (int)0
            ),
            'fournisseur = "' . pSQL($connecteur) . '"' . ($id_shop ? ' AND id_shop = ' . (int)$id_shop : '')
        );
    }

    public static function zeroStockPrxFlag($connecteur = null, $id_shop = 0)
    {
        if (is_null($connecteur)) {
            $connecteur = self::getConnecteurName();
        }

        return Db::getInstance()->update(
            'eci_stock',
            array(
                'prx_flag' => (int)0
            ),
            'fournisseur = "' . pSQL($connecteur) . '"' . ($id_shop ? ' AND id_shop = ' . (int)$id_shop : '')
        );
    }

    public static function resetStockPrxFlag($connecteur = null, $id_shop = 0)
    {
        if (is_null($connecteur)) {
            $connecteur = self::getConnecteurName();
        }

        return Db::getInstance()->update(
            'eci_stock',
            array(
                'prx_flag' => (int)0
            ),
            'prx_flag = 1 AND fournisseur = "' . pSQL($connecteur) . '"' . ($id_shop ? ' AND id_shop = ' . (int)$id_shop : '')
        );
    }

    public static function setStockPrxFlag($id_product, $id_pa = null, $id_shop = null, $connecteur = null, $location = null)
    {
        if (is_null($connecteur)) {
            $connecteur = self::getConnecteurName();
        }

        Db::getInstance()->update(
            'eci_stock',
            array(
                'prx_flag' => (int)1
            ),
            'id_product = ' . (int) $id_product . ' AND fournisseur = "' . pSQL($connecteur) . '"'
            . (is_null($id_pa)?'':' AND id_pa = ' . (int) $id_pa)
            . (is_null($id_shop)?'':' AND id_shop = ' . (int) $id_shop)
            . (is_null($location)?'':' AND location = "' . pSQL($location) . '"')
        );
    }

    public static function setStockPrxFlagProtected($id_product, $id_pa = null, $id_shop = null, $connecteur = null, $location = null)
    {
        if (is_null($connecteur)) {
            $connecteur = self::getConnecteurName();
        }

        Db::getInstance()->update(
            'eci_stock',
            array(
                'prx_flag' => (int)2
            ),
            'id_product = ' . (int) $id_product . ' AND fournisseur = "' . pSQL($connecteur) . '"'
            . (is_null($id_pa)?'':' AND id_pa = ' . (int) $id_pa)
            . (is_null($id_shop)?'':' AND id_shop = ' . (int) $id_shop)
            . (is_null($location)?'':' AND location = "' . pSQL($location) . '"')
        );
    }

    public static function zeroStock($connecteur = null, $id_shop = false, $all = false, $keep_negative = false)
    {
        if (is_null($connecteur)) {
            $connecteur = self::getConnecteurName();
        }
        
        $where = 'fournisseur = "' . pSQL($connecteur) . '"' . ($id_shop ? ' AND id_shop = ' . (int)$id_shop : '');
        if (!$all) {
            $where .= $keep_negative ? ' AND upd_flag = 0': ' AND (qt < 0 OR upd_flag = 0)';
        }

        return Db::getInstance()->update(
            'eci_stock',
            array(
                'qt' => (int)0
            ),
            $where
        );
    }

    public static function copyStock($connecteur, $id_shop1, $id_shop2)
    {
        Db::getInstance()->execute(
            'INSERT INTO ' . _DB_PREFIX_ . 'eci_stock (id_product, id_pa, reference, fournisseur, id_shop, qt, location, price, pmvc, upd_flag)
            SELECT id_product, id_pa, reference, fournisseur, ' . (int) $id_shop2 . ', qt, location, price, pmvc, upd_flag
            FROM ' . _DB_PREFIX_ . 'eci_stock
            WHERE fournisseur = "' . pSQL($connecteur) . '" AND id_shop = ' . (int) $id_shop1 . '
            ON DUPLICATE KEY UPDATE qt=VALUES(qt), price=VALUES(price), pmvc=VALUES(pmvc), upd_flag=VALUES(upd_flag)'
        );
    }

    public static function cleanStock($connecteur, $id_shop = 0)
    {
        if (is_null($connecteur)) {
            $connecteur = self::getConnecteurName();
        }

        return Db::getInstance()->delete(
            'eci_stock',
            'upd_flag = 0 AND fournisseur = "' . pSQL($connecteur) . '"' . ($id_shop ? ' AND id_shop = ' . (int)$id_shop : '')
        );
    }

    public static function clearStock($connecteur, $id_shop = 0)
    {
        if (is_null($connecteur)) {
            $connecteur = self::getConnecteurName();
        }

        return Db::getInstance()->delete(
            'eci_stock',
            'fournisseur = "' . pSQL($connecteur) . '"' . ($id_shop ? ' AND id_shop = ' . (int)$id_shop : '')
        );
    }

    public static function setStock($id_product, $reference, $id_shop, $connecteur = null, $id_pa = 0, $qt = 0, $location = null, $price = null, $pmvc = null, $prx_flag = null)
    {
        if (is_null($connecteur)) {
            $connecteur = self::getConnecteurName();
        }

        $insert = array(
            'id_product' => (int) $id_product,
            'id_pa' => (int) $id_pa,
            'reference' => pSQL($reference),
            'fournisseur' => pSQL($connecteur),
            'id_shop' => (int) $id_shop,
            'qt' => (int) $qt
        );
        if (!is_null($price)) {
            $insert['price'] = (float)$price;
        }
        if (!is_null($pmvc)) {
            $insert['pmvc'] = (float)$pmvc;
        }
        if (!is_null($location)) {
            $insert['location'] = pSQL($location);
        }
        if (!is_null($prx_flag)) {
            $insert['prx_flag'] = (int)$prx_flag;
        }
        Db::getInstance()->insert(
            'eci_stock',
            $insert,
            false,
            false,
            Db::REPLACE
        );
    }

    public static function setStockPrices($id_product, $price = 0, $pmvc = 0, $connecteur = null, $id_pa = null, $id_shop = null, $location = null)
    {
        if (is_null($connecteur)) {
            $connecteur = self::getConnecteurName();
        }

        Db::getInstance()->update(
            'eci_stock',
            array(
                'price' => (float) $price,
                'pmvc' => (float) $pmvc,
            ),
            'id_product = ' . (int) $id_product . ' AND fournisseur = "' . pSQL($connecteur) . '"'
            . (is_null($id_pa)?'':' AND id_pa = ' . (int) $id_pa)
            . (is_null($id_shop)?'':' AND id_shop = ' . (int) $id_shop)
            . (is_null($location)?'':' AND location = "' . pSQL($location) . '"')
        );
    }

    public static function getStocks($id_product, $connecteur = null, $id_shop = null, $id_pa = null, $location = null, $all_stocks = false)
    {
        return Db::getInstance()->executeS(
            'SELECT *
            FROM ' . _DB_PREFIX_ . 'eci_stock
            WHERE id_product = ' . (int) $id_product
            . ($all_stocks?'':' AND upd_flag = 1')
            . (is_null($id_pa)?'':' AND id_pa = ' . (int) $id_pa)
            . (is_null($id_shop)?'':' AND id_shop = ' . (int) $id_shop)
            . (is_null($location)?'':' AND location = "' . pSQL($location) . '"')
            . (is_null($connecteur)?'':' AND fournisseur = "' . pSQL($connecteur) . '"')
        );
    }

    public static function getStock($id_product, $connecteur = null, $id_shop = null, $id_pa = null, $location = null, $all_stocks = false)
    {
        return (int) Db::getInstance()->getValue(
            'SELECT SUM(qt)
            FROM ' . _DB_PREFIX_ . 'eci_stock
            WHERE id_product = ' . (int) $id_product
            . ($all_stocks?'':' AND upd_flag = 1')
            . (is_null($id_pa)?'':' AND id_pa = ' . (int) $id_pa)
            . (is_null($id_shop)?'':' AND id_shop = ' . (int) $id_shop)
            . (is_null($location)?'':' AND location = "' . pSQL($location) . '"')
            . (is_null($connecteur)?'':' AND fournisseur = "' . pSQL($connecteur) . '"')
        );
    }

    public static function getStockDiff($id_product, $connecteur = null, $id_shop = null, $id_pa = null, $location = null, $all_stocks = false)
    {
        //calculer les lignes de stocks différents entre stock_available et eci_stock
        //introduire la notion de fournisseur
        //vérifier les id_shop_group pour les groupes qui partagent le stock
        //retourner la liste des produits à mettre à jour
        //permettre de préciser si on veut une liste partielle avec LIMIT offset,number
        
        //créer une autre fonction qui donne le count : getStockDiffCount
        
        'SELECT ps.id_product, ps.id_product_attribute, ps.quantity, es.qt, es.reference
        FROM ' . _DB_PREFIX_ . 'stock_available ps
        LEFT JOIN (
            SELECT SUM(qt) as qt, id_product, id_pa, reference, upd_flag 
            FROM ' . _DB_PREFIX_ . 'eci_stock 
            WHERE upd_flag = 1
            GROUP BY id_product, id_pa
        ) es ON ps.id_product = es.id_product AND ps.id_product_attribute = es.id_pa
        WHERE ps.quantity != es.qt;';
    }

    public static function getCleanStockForInsert($tab_stock, $fournisseur = null)
    {
        $connecteur = $fournisseur ?? self::getConnecteurName();
        $stock_insert = [
            'id_product' => 'int',
            'id_pa' => 'int',
            'reference' => 'varchar',
            'fournisseur' => 'varchar',
            'id_shop' => 'int',
            'qt' => 'varchar',
            'location' => 'varchar',
            'price' => 'varchar',
            'pmvc' => 'varchar',
            'upd_flag' => 'int',
            'prx_flag' => 'int',
        ];
        
        $tab_stock['upd_flag'] = isset($tab_stock['upd_flag']) ? (int)$tab_stock['upd_flag'] : 1;
        $tab_stock['prx_flag'] = isset($tab_stock['prx_flag']) ? (int)$tab_stock['prx_flag'] : 0;
        $tab_insert = array();
        foreach ($stock_insert as $key => $type) {
            $tab_insert[$key] = isset($tab_stock[$key]) ? (('int' === $type) ? (int)$tab_stock[$key] : pSQL($tab_stock[$key])) : null;
        }
        $tab_insert['fournisseur'] = $tab_insert['fournisseur'] ?: pSQL($connecteur);
        
        return $tab_insert;
    }
    
    public static function insertStocks($insert_stocks, $logger = null)
    {
        $divider = 10;
        $logger = $logger ?? self::logStart(__FUNCTION__);
        if (!is_array($insert_stocks)) {
            self::logInfo($logger, 'No array of products to insert in Db');
            return false;
        }
        if (!$insert_stocks) {
            return true;
        }

        try {
            Db::getInstance()->insert('eci_stock', $insert_stocks, false, false, Db::REPLACE);
        } catch (Exception $e) {
            self::logInfo($logger, 'Error in the insertion of stocks in Db : '.$e->getMessage());
            $total = count($insert_stocks);
            $length = (int) ($total / $divider);
            if (!$length) {
                return false;
            }
            $i = 0;
            while (($slice = array_slice($insert_stocks, $i * $length, $length))) {
                $i++;
                if (!self::insertStocks($slice, $logger)) {
                    return false;
                }
            }
        }
        
        return true;
    }

    public static function updateCatalogPrices($parent, $connecteur, $price, $pmvc, $child = null)
    {
        if (is_null($child)) {
            Db::getInstance()->update(
                'eci_catalog',
                array(
                    'price' => (float) $price,
                    'pmvc' => (float) $pmvc
                ),
                'product_reference = "' . pSQL($parent) . '" AND fournisseur = "' . pSQL($connecteur) . '"'
            );
        } else {
            Db::getInstance()->update(
                'eci_catalog_attribute',
                array(
                    'price' => (float) $price,
                    'pmvc' => (float) $pmvc
                ),
                'reference_attribute = "' . pSQL($child) . '" AND product_reference = "' . pSQL($parent) . '" AND fournisseur = "' . pSQL($connecteur) . '"'
            );
        }
    }

    public static function getOrderExists($id_order)
    {
        return (bool) Db::getInstance()->getValue(
            'SELECT COUNT(*)
            FROM ' . _DB_PREFIX_ . 'orders
            WHERE id_order = ' . (int) $id_order
        );
    }

    public static function getOrderExistsCon($id_order, $connecteur)
    {
        $id_supplier = self::getEciFournId($connecteur);
        if (self::getOrderExists($id_order)) {
            return (bool) in_array($id_supplier, self::getOrderSuppliersList($id_order));
        }

        return false;
    }

    public static function getOrderSuppliersList($id_order)
    {
        if (self::SEND_ORDER_TO_GEN_ONLY) {
            return Db::getInstance()->getRow(
                'SELECT id_manufacturer
                FROM ' . _DB_PREFIX_ . 'eci_fournisseur
                WHERE name = "' . pSQL(self::getConnecteurName()) . '"'
            );
        }

        $list_fournisseurs = Db::getInstance()->executeS(
            'SELECT DISTINCT id_supplier
            FROM ' . _DB_PREFIX_ . 'order_detail od
            INNER JOIN ' . _DB_PREFIX_ . 'product p
            ON p.id_product = od.product_id
            WHERE od.id_order = ' . (int) $id_order
        );

        return array_map('implode', $list_fournisseurs);
    }

    public static function getOrderVisibilityCon($id_order, $connecteur)
    {
        return Db::getInstance()->getValue(
            'SELECT visible
            FROM ' . _DB_PREFIX_ . 'eci_export_com
            WHERE id_order=' . (int) $id_order . '
            AND fournisseur = "' . pSQL($connecteur) . '"'
        );
    }

    public static function setOrderVisibleCon($id_order, $connecteur, $type = true)
    {
        $visible = $type ? 1 : 0;
        if (self::getOrderExistsCon($id_order, $connecteur)) {
            $visibility = self::getOrderVisibilityCon($id_order, $connecteur);
            if (is_numeric($visibility)) {
                if ($visibility != $visible) {
                    Db::getInstance()->update(
                        'eci_export_com',
                        array('visible' => (int) $visible),
                        'id_order = ' . (int) $id_order .
                        ' AND fournisseur = "' . pSQL($connecteur) . '"'
                    );
                }
            } else {
                Db::getInstance()->insert(
                    'eci_export_com',
                    array(
                        'id_order' => (int) $id_order,
                        'fournisseur' => pSQL($connecteur),
                        'visible' => (int) $visible
                    )
                );
            }
        }
    }

    public static function setOrderInfo($id_order, $info, $connecteur = null)
    {
        if (is_null($connecteur)) {
            $connecteur = self::getConnecteurName();
        }

        Db::getInstance()->insert(
            'eci_export_com',
            array(
                'id_order' => (int)$id_order,
                'info' => pSQL($info),
                'fournisseur' => pSQL($connecteur)
            ),
            false,
            false,
            Db::ON_DUPLICATE_KEY
        );
    }

    public static function setOrderInfos($id_order, $info, $extid = null, $connecteur = null)
    {
        if (is_null($connecteur)) {
            $connecteur = self::getConnecteurName();
        }

        $insert = array(
            'id_order' => (int)$id_order,
            'info' => pSQL($info),
            'fournisseur' => pSQL($connecteur)
        );
        if (!is_null($extid)) {
            $insert['extid'] = pSQL($extid);
        }

        Db::getInstance()->insert(
            'eci_export_com',
            $insert,
            false,
            false,
            Db::ON_DUPLICATE_KEY
        );
    }

    public static function getOrderInfos($id_order, $connecteur = null)
    {
        if (is_null($connecteur)) {
            $connecteur = self::getConnecteurName();
        }

        return Db::getInstance()->getRow(
            'SELECT *
            FROM '._DB_PREFIX_.'eci_export_com
            WHERE id_order = ' . (int) $id_order . '
            AND fournisseur = "' . pSQL($connecteur) .'"'
        );
    }

    public static function getOrderInfosByInfo($info, $connecteur = null)
    {
        if (is_null($connecteur)) {
            $connecteur = self::getConnecteurName();
        }

        return Db::getInstance()->getRow(
            'SELECT *
            FROM '._DB_PREFIX_.'eci_export_com
            WHERE info = "' . pSQL($info)  . '"
            AND fournisseur = "' . pSQL($connecteur) .'"'
        );
    }

    public static function getOrderInfosByExtid($extid, $connecteur = null)
    {
        if (is_null($connecteur)) {
            $connecteur = self::getConnecteurName();
        }

        return Db::getInstance()->getRow(
            'SELECT *
            FROM '._DB_PREFIX_.'eci_export_com
            WHERE extid = "' . pSQL($extid)  . '"
            AND fournisseur = "' . pSQL($connecteur) .'"'
        );
    }

    public static function setOrderFinal($id_order, $final = true)
    {
        $connecteur = self::getConnecteurName();
        Db::getInstance()->update(
            'eci_export_com',
            array('final' => (int) $final),
            'id_order = ' . (int) $id_order . ' AND fournisseur = "' . pSQL($connecteur) . '"'
        );
    }

    /**
     * sorts the order details according to the table provided
     * keys are ranks and values are id_order_detail to sort
     * no verification if od are from the same order
     * updates the order_detail_tax table too
     *
     * @param array $tab_od_sorted table of sorted order details ids
     * @return boolean
     */
    public static function sortOrder($tab_od_sorted)
    {
        if (!is_array($tab_od_sorted)) {
            return false;
        }

        ksort($tab_od_sorted);
        $tab_od_sorted_clean = array_values(array_unique($tab_od_sorted));
        $tab_od_ids = $tab_od_sorted_clean;
        sort($tab_od_ids);
        $od_transform = array_combine($tab_od_sorted_clean, $tab_od_ids);

        foreach ($od_transform as $old => $new) {
            Db::getInstance()->update(
                'order_detail',
                array('id_order_detail' => (int) ($new + 1000)),
                'id_order_detail = ' . (int) $old
            );
            Db::getInstance()->update(
                'order_detail_tax',
                array('id_order_detail' => (int) ($new + 1000)),
                'id_order_detail = ' . (int) $old
            );
        }
        foreach ($od_transform as $old => $new) {
            Db::getInstance()->update(
                'order_detail',
                array('id_order_detail' => (int) $new),
                'id_order_detail = ' . (int) ($new + 1000)
            );
            Db::getInstance()->update(
                'order_detail_tax',
                array('id_order_detail' => (int) $new),
                'id_order_detail = ' . (int) ($new + 1000)
            );
        }

        return true;
    }
    
    /**
     * TODO
     * @param Order $order
     * @param type $break_packs
     * @return boolean
     */
    public static function getOrderDetailsWithPrices($order, $break_packs = false)
    {
        if (!($order instanceof Order)) {
            return false;
        }
        
        $details = $order->getOrderDetailList();
        $taxes = $order->getOrderDetailTaxes();
        
        return;
    }
    
    public static function setOrderMessage($id_order, $message, $private = 1, $status = 'open')
    {
        if (Validate::isLoadedObject($id_order)) {
            $order = $id_order;
        } else {
            $order = new Order($id_order);
        }

        
        if (Tools::version_compare(_PS_VERSION_, '1.7.4', '<')) {
            $message_size = Message::$definition['fields']['message']['size'];
            $oMsg = new Message();
            $oMsg->id_order = $order->id;
            $oMsg->message = self::getStrCutBytes($message, $message_size);
            $oMsg->private = (int) $private;

            return $oMsg->save();
        }

        $customer = new Customer((int) $order->id_customer);

        $id_customer_thread = CustomerThread::getIdCustomerThreadByEmailAndIdOrder($customer->email, $order->id);
        if (!$id_customer_thread) {
            $customer_thread = new CustomerThread();
            $customer_thread->id_contact = 0;
            $customer_thread->id_customer = (int) $order->id_customer;
            $customer_thread->id_shop = (int) $order->id_shop;
            $customer_thread->id_order = (int) $order->id;
            $customer_thread->id_lang = (int) $order->id_lang;
            $customer_thread->email = $customer->email;
            $customer_thread->status = (in_array($status, array('open', 'closed', 'pending1', 'pending2')) ? $status : 'open');
            $customer_thread->token = Tools::passwdGen(12);
            $customer_thread->add();
        } else {
            $customer_thread = new CustomerThread((int) $id_customer_thread);
        }

        $message_size = CustomerMessage::$definition['fields']['message']['size'];
        $customer_message = new CustomerMessage();
        $customer_message->id_customer_thread = $customer_thread->id;
        $customer_message->id_employee = self::getInfoEco('ID_EMPLOYEE');
        $customer_message->message = self::getStrCutBytes($message, $message_size);
        $customer_message->private = $private;

        return $customer_message->add();
    }

    public static function setOrderState($id_order, $id_os, $message = null, $logger = null)
    {
        $id_employee = self::getInfoEco('ID_EMPLOYEE');
        Context::getContext()->employee = new Employee($id_employee);

        if (Validate::isLoadedObject($id_order)) {
            $order = $id_order;
        } else {
            $order = new Order($id_order);
        }
        $final_ost_raw = self::getECIConfigValue('FINAL_OST', $order->id_shop, self::getConnecteurName());
        $final_ost = Tools::jsonDecode($final_ost_raw, true);
        if (is_array($final_ost) && in_array($order->current_state, $final_ost)) {
            return true;
        }
        if ($id_os != $order->current_state) {
            $order->setCurrentState($id_os, $id_employee);
        }
        if (is_array($final_ost) && in_array($id_os, $final_ost)) {
            self::setOrderFinal($id_order);
        }

        if (is_null($message)) {
            return true;
        }

        $return2 = (bool) self::setOrderMessage($order, $message);
        if ($return2) {
            return true;
        }

        $erMsg = 'Message not associated';
        if (!is_null($logger)) {
            self::logError($logger, $erMsg);
            return false;
        }

        return $erMsg;
    }

    public static function addDynamicOrderState($ec_four, $newOrderStateName, $dynamicOstTableName = 'id_ost')
    {
        if (isset($ec_four->config[$dynamicOstTableName]) && is_array($ec_four->config[$dynamicOstTableName]) && !array_key_exists($newOrderStateName, $ec_four->config[$dynamicOstTableName])) {
            $ec_four->config[$dynamicOstTableName][$newOrderStateName] = 0;
            $encoded = http_build_query(array($dynamicOstTableName => $ec_four->config[$dynamicOstTableName]));
            self::updateParamFournisseur($encoded, $ec_four->fournisseur_glob, $ec_four->id_shop, $ec_four->valid_conf_keys, $ec_four->numeric_conf_keys, $ec_four->multiple_conf_keys, $ec_four->array_conf_keys, $ec_four->allshop_keys);

            return true;
        }

        return false;
    }

    public static function delDynamicOrderState($ec_four, $newOrderStateName, $dynamicOstTableName = 'id_ost')
    {
        if (isset($ec_four->config[$dynamicOstTableName]) && is_array($ec_four->config[$dynamicOstTableName]) && array_key_exists($newOrderStateName, $ec_four->config[$dynamicOstTableName])) {
            unset($ec_four->config[$dynamicOstTableName][$newOrderStateName]);
            $encoded = http_build_query(array($dynamicOstTableName => $ec_four->config[$dynamicOstTableName]));
            self::updateParamFournisseur($encoded, $ec_four->fournisseur_glob, $ec_four->id_shop, $ec_four->valid_conf_keys, $ec_four->numeric_conf_keys, $ec_four->multiple_conf_keys, $ec_four->array_conf_keys, $ec_four->allshop_keys);

            return true;
        }

        return false;
    }

    public static function setTracking($id_order, $id_order_state, $order_state_name = '', $transport = '', $numero = '', $date_modif_state = '', $date_exp = '', $pdts_infos = '', $url_exp = '', $fournisseur = null)
    {
        if (is_null($fournisseur)) {
            $fournisseur = self::getConnecteurName();
        }

        Db::getInstance()->insert(
            'eci_tracking',
            array(
                'id_order' => (int) $id_order,
                'fournisseur' => pSQL($fournisseur),
                'transport' => pSQL($transport),
                'numero' => pSQL($numero),
                'date_exp' => pSQL($date_exp),
                'url_exp' => pSQL($url_exp),
                'id_order_state' => (int) $id_order_state,
                'order_state_name' => pSQL($order_state_name),
                'date_modif_state' => $date_modif_state ? $date_modif_state : date('Y-m-d H:i:s'),
                'pdts_infos' => pSQL($pdts_infos),
            ),
            false,
            false,
            Db::INSERT_IGNORE
        );
    }

    public static function getLastTracking($id_order)
    {
        return Db::getInstance()->getRow('SELECT * FROM '._DB_PREFIX_.'eci_tracking WHERE id_order = '.(int)$id_order.' ORDER BY date_modif_state DESC');
    }

    public static function getTrackings($id_order)
    {
        return Db::getInstance()->executeS('SELECT * FROM '._DB_PREFIX_.'eci_tracking WHERE id_order = '.(int)$id_order.' ORDER BY date_modif_state');
    }

    public static function getTrackingStates($id_order)
    {
        return Db::getInstance()->executeS('SELECT id_order_state, order_state_name, date_modif_state FROM '._DB_PREFIX_.'eci_tracking WHERE id_order = '.(int)$id_order.' ORDER BY date_modif_state');
    }

    public static function setTrackingNumber($order, $number, $logger = null, $id_carrier = null)
    {
        try {
            $id_order_carrier = Db::getInstance()->getValue(
                'SELECT id_order_carrier
                FROM ' . _DB_PREFIX_ . 'order_carrier
                WHERE id_order = ' . (int) $order->id .
                (is_null($id_carrier) ? '' : (' AND id_carrier = ' . (int) $id_carrier))
            );
            if ($id_order_carrier) {
                $order_carrier = new OrderCarrier($id_order_carrier);
                $order_carrier->tracking_number = $number;
                $order_carrier->setFieldsToUpdate(['tracking_number' => true]);
                $order_carrier->update();
            } else {
                if ($order->shipping_number != $number) {
                    $order->shipping_number = $number;
                    $order->setFieldsToUpdate(['shipping_number' => true]);
                    $order->update();
                }
            }

            return true;
        } catch (Exception $e) {
            $msg = __FUNCTION__ . ' error while trying to update order ' . $order->id . ' : ' . $e->getMessage();
            if (is_null($logger)) {
                return $msg;
            }
            self::logInfo($logger, $msg);
        }

        return false;
    }

    public static function validateEAN13($code)
    {
        $key = self::getEAN13Key($code, true);
        if (false === $key) {
            return false;
        }

        return ($key == Tools::substr($code, -1));
    }

    public static function getEAN13WithKey($code, $haskey = false)
    {
        $key = self::getEAN13Key($code, $haskey);
        if (false === $key) {
            return false;
        }
        if ($haskey) {
            $code = Tools::substr($code, 0, Tools::strlen($code) - 1);
        }

        return $code . $key;
    }

    public static function getEAN13Key($code, $haskey = false)
    {
        if (!is_numeric($code) || $code == 0 || Tools::strlen($code) > 13) {
            return false;
        }
        if ($haskey) {
            $code = Tools::substr($code, 0, Tools::strlen($code) - 1);
        }
        $code = $code . '0';

        $aCode = str_split(Tools::substr('00000000000000' . $code, -14));
        $sum = 0;
        foreach ($aCode as $i => $n) {
            $sum += (($i % 2 == 0) ? 3 : 1) * $n;
        }

        return (10 - ($sum % 10)) % 10;
    }

    /**
     * sometimes specific quantity prices are given with their max cart/order quantity
     * this funcion takes an array of max_qt => price and returns the array ready for PS specific prices
     * recalculating the corresponding 'from_quantity'
     *
     * @param array $tabPricesByMaxQt : array(qt0 => price0, qt1 => price1,...)
     * @return boolean|array array(array('price' => x.x, 'from_quantity' => y), ...)
     */
    public static function getSpePricesByMinQt($tabPricesByMaxQt)
    {
        if (!is_array($tabPricesByMaxQt)) {
            return false;
        }

        $sorted_by_qt = array();
        foreach ($tabPricesByMaxQt as $max_qt => $qtp) {
            $key = round(str_replace(',', '.', $max_qt));
            $sorted_by_qt[$key] = $qtp;
        }
        ksort($sorted_by_qt);
        $old = 0;
        $sprices = array();
        foreach ($sorted_by_qt as $from_quantity => $price) {
            $sprices[] = array(
                'price' => $price,
                'from_quantity' => $old
            );
            $old = $from_quantity;
        }

        return $sprices;
    }

    /**
     * get reserved id_specific_price_rule
     * reserve it  if not already done
     *
     * @param type $ec_four
     * @return int
     */
    public static function getReservedSpePriceRuleId($ec_four, $configuration_key = 'reserved_pricerule_id')
    {
        if (!empty($ec_four->config[$configuration_key])) {
            return $ec_four->config[$configuration_key];
        }

        $id_reserved_rule = self::getECIConfigValue($configuration_key, 0, $ec_four->fournisseur_glob);
        if ($id_reserved_rule) {
            return $id_reserved_rule;
        }

        $rule_name = 'Eci' . $ec_four->fournisseur_glob . ' Tarifs règle réservée';
        $id_rule = Db::getInstance()->getValue(
            'SELECT id_specific_price_rule
            FROM '._DB_PREFIX_.'specific_price_rule
            WHERE name LIKE "'.pSQL($rule_name).'"'
        );
        if (!$id_rule) {
            //condition éventuellement fictives car la règle sera supprimée
            $id_shop = Configuration::get('PS_SHOP_DEFAULT');
            $id_group = Configuration::get('PS_CUSTOMER_GROUP');
            $remise = (float) 50;
            $date_from = date('Y-m-d H:i:s');
            $date_to = date('Y-m-d H:i:s', strtotime($date_from . ' +1 days'));

            Db::getInstance()->insert(
                'specific_price_rule',
                array(
                    'name'              =>  pSQL($rule_name),
                    'id_shop'           =>  (int) $id_shop,
                    'id_country'        =>  (int) 0,
                    'id_currency'       =>  (int) 0,
                    'id_group'          =>  (int) $id_group,
                    'from_quantity'     =>  (int) 1,
                    'price'             =>  (float) -1,
                    'reduction'         =>  (float) $remise,
                    'reduction_tax'     =>  (int) 0,
                    'reduction_type'    =>  pSQL('percentage'),
                    'from'              =>  $date_from,
                    'to'                =>  $date_to
                )
            );
            $id_rule = Db::getInstance()->Insert_ID();
            Db::getInstance()->delete(
                'specific_price_rule',
                'id_specific_price_rule = ' . (int) $id_rule
            );
        } else {
            $rule = new SpecificPriceRule($id_rule);
            $rule->delete();
        }

        self::setECIConfigValue($configuration_key, $id_rule, 0, $ec_four->fournisseur_glob);

        return $id_rule;
    }

    public static function arrayToCsv($array, $fs = ';', $ls = "\n")
    {
        return implode($ls, array_map('implode', array_fill(0, count($array), $fs), $array));
    }

    public static function arrayToXML(array $arr, SimpleXMLElement $xml)
    {
        foreach ($arr as $k => $v) {
            $attrArr = array();
            $kArray = explode(' ', $k);
            $tag = array_shift($kArray);

            if (count($kArray) > 0) {
                foreach ($kArray as $attrValue) {
                    $attrArr[] = explode('=', $attrValue);
                }
            }

            if (is_array($v)) {
                if (is_numeric($k)) {
                    self::arrayToXML($v, $xml);
                } else {
                    $child = $xml->addChild($tag);
                    if (isset($attrArr)) {
                        foreach ($attrArr as $attrArrV) {
                            $child->addAttribute($attrArrV[0], $attrArrV[1]);
                        }
                    }
                    self::arrayToXML($v, $child);
                }
            } else {
                if ($v == htmlentities($v)) {
                    $child = $xml->addChild($tag, $v);
                } else {
                    $child = $xml->addChild($tag);
                    $node = dom_import_simplexml($child);
                    $no   = $node->ownerDocument;
                    $node->appendChild($no->createCDATASection($v));
                }
                if (isset($attrArr)) {
                    foreach ($attrArr as $attrArrV) {
                        $child->addAttribute($attrArrV[0], $attrArrV[1]);
                    }
                }
            }
        }

        return $xml;
    }

    public static function prettyXML($xml_text)
    {
        $doc = new DOMDocument();
        $doc->preserveWhiteSpace = false;
        $doc->formatOutput = true;
        $doc->loadXML($xml_text);
        return $doc->saveXML();
    }

    public static function objectToCsv($obj, $key_prefix = null, $split_children = true, $filter = null)
    {
        $ar = json_decode(json_encode($obj), true);
        $sep = ';';
        $nl = "\n";

        if (!is_null($filter) && is_array($filter)) {
            reset($filter);
            $seq = is_integer(key($filter));
            if ($seq) {
                $keys = $filter;
            } else {
                $keys = array_keys($filter);
            }
        } else {
            $filter = false;
        }

        $str = '';
        foreach ($ar as $k => $v) {
            if (is_array($v) && $split_children) {
                $v = self::objectToCsv($v, $key_prefix, $split_children, $filter);
            } else {
                if ($filter) {
                    if (!in_array($k, $keys)) {
                        continue;
                    }
                    if (!$seq) {
                        $k = $filter[$k];
                    }
                }
                if (is_array($v)) {
                    $str .= ($key_prefix ?? '') . $k . $sep . json_encode($v) . $nl;
                } else {
                    $str .= ($key_prefix ?? '') . $k . $sep . (is_null($v) ? '' : var_export($v, true)) . $nl;
                }
            }
        }

        return $str;
    }

    public static function logStart($family, $truncate = false, $limit = 0)
    {
        $logger = new Info($family, $truncate, $limit);
        $logger->dir = _PS_MODULE_DIR_ . '/' . __NAMESPACE__ . '/log/';
        $logger->onlyDB = true; // true if logs must go to DB

        return $logger;
    }

    public static function logDebug($logger, $text)
    {
        if (self::ALLOW_LOG) {
            return $logger->logDebug($text);
        }

        return false;
    }

    public static function logInfo($logger, $text)
    {
        if (self::ALLOW_LOG) {
            return $logger->logInfo($text);
        }

        return false;
    }

    public static function logWarning($logger, $text)
    {
        if (self::ALLOW_LOG) {
            return $logger->logWarning($text);
        }

        return false;
    }

    public static function logError($logger, $text)
    {
        if (self::ALLOW_LOG) {
            return $logger->logError($text);
        }

        return false;
    }

    public static function logTruncate($logger)
    {
        $logger->logTruncate();
    }

    public static function getNextCron($id_prg, $position = 0)
    {
        $cron = Db::getInstance()->getRow(
            'SELECT *
            FROM ' . _DB_PREFIX_ . 'eci_prg_cron
            WHERE id_prg = ' . (int) $id_prg . '
            AND position > ' . (int) $position . '
            ORDER BY position'
        );

        if ($cron && is_array($cron)) {
            if (is_numeric($cron['link']) &&
                $shortlink = Db::getInstance()->getValue(
                    'SELECT link
                    FROM ' . _DB_PREFIX_ . 'eci_cpanel
                    WHERE id_cpanel = ' . (int) $cron['link']
                )) {
                $cron['link'] = self::getCronLink($shortlink);
            }

            return $cron;
        }

        return false;
    }

    public static function getNewPrgId()
    {
        return 1 + (int) Db::getInstance()->getValue(
            'SELECT MAX(id_prg)
            FROM ' . _DB_PREFIX_ . 'eci_prg_names'
        );
    }

    public static function getTasks($filterPrefix = array())
    {
        $class = __NAMESPACE__;
        $mod = new $class();
        $prot = $mod->protocol;
        $dom = Tools::getShopDomain() . __PS_BASE_URI__;
        $tok = Tools::safeOutput(self::getInfoEco('ECO_TOKEN'));
        $mod_path = __PS_BASE_URI__ . 'modules/' . $mod->name . '/';
        $module_uri = $prot . $dom . 'modules/' . $mod->name . '/';
        if (!is_array($filterPrefix)) {
            $filterPrefix = array($filterPrefix);
        }
        $filter = array();
        if ($filterPrefix) {
            foreach ($filterPrefix as $key => $item) {
                if (is_array($item)) {
                    $filter[$key] = $item;
                    $filter[$key]['nb'] = 0;
                    if (!isset($filter[$key]['max'])) {
                        $filter[$key]['max'] = 0;
                    }
                } else {
                    $filter[$item]['nb'] = 0;
                    $filter[$item]['max'] = 0;
                }
            }
        }

        $list_tasks = Db::getInstance()->executeS(
            'SELECT c.*
            FROM ' . _DB_PREFIX_ . 'eci_cpanel c
            LEFT JOIN ' . _DB_PREFIX_ . 'eci_fournisseur f
            ON c.suffix = f.name
            WHERE f.name = "' . pSQL($class::GEN) . '"
            AND c.active = 1
            AND f.perso = 1
            ORDER BY c.position ASC'
        );

        $selected_tasks = array();
        foreach ($list_tasks as $task) {
            if ($filter && !isset($filter[$task['prefix']])) {
                continue;
            }
            if (!empty($filter[$task['prefix']]['max']) &&
                $filter[$task['prefix']]['nb'] >= $filter[$task['prefix']]['max']) {
                continue;
            }
            if ($filter) {
                $filter[$task['prefix']]['nb'] += 1;
            }
            $task['link'] = self::getCronLink($task['link'], $module_uri, $mod_path, $tok);
            if (!empty($filter[$task['prefix']]['nobuttons'])) {
                $task['nobuttons'] = 1;
            }
            if (!empty($filter[$task['prefix']]['hide'])) {
                $task['hide'] = 1;
            }
            $selected_tasks[] = $task;
        }

        return $selected_tasks;
    }

    public static function getCronLink($shortLink, $module_uri = null, $mod_path = null, $token = null)
    {
        if (is_null($module_uri) || is_null($mod_path)) {
            $class = __NAMESPACE__;
            $mod = new $class();
            $prot = $mod->protocol;
            $dom = Tools::getShopDomain() . __PS_BASE_URI__;
            $mod_path = __PS_BASE_URI__ . 'modules/' . $mod->name . '/';
            $module_uri = $prot . $dom . 'modules/' . $mod->name . '/';
        }

        $tok = Tools::safeOutput((is_null($token)?self::getInfoEco('ECO_TOKEN'):$token));

        $link = trim($shortLink);
        if (strstr($link, $mod_path)) {
            $a_link = explode($mod_path, $link);
            $link = isset($a_link[1]) ? $a_link[1] : $link;
        }
        if (strstr($link, '?ec_token=')) {
            $link = preg_replace('/ec_token=[a-z0-9]+\&/i', '', $link);
        } elseif (strstr($link, 'ec_token=')) {
            $link = preg_replace('/\&ec_token=[a-z0-9]+/i', '', $link);
        }

        return $module_uri . $link . (strstr($link, '?')?'&':'?') .'ec_token=' . $tok;
    }

    public static function getCpanelData($prefix, $paramConnecteur = false)
    {
        $prefix = trim($prefix, '_') . '_';

        if (!$paramConnecteur) {
            $connecteurs = Db::getInstance()->executeS(
                'SELECT name
                FROM ' . _DB_PREFIX_ . 'eci_fournisseur
                WHERE perso = 1
                ORDER BY name'
            );
            $aConnecteurs = array();
            foreach ($connecteurs as $connecteur) {
                $aConnecteurs[] = $connecteur['name'];
            }
        } else {
            $aConnecteurs = array($paramConnecteur);
        }

        $status = array();
        $jobs_state = self::jGetLike($prefix . '%');
        if (!$jobs_state) {
            return;
        }

        foreach ($aConnecteurs as $connecteur) {
            $suffix = $connecteur == 'none' ? '' : '_' . $connecteur;
            $status[$connecteur] = array(
                'start_time'  => $jobs_state[$prefix . 'START_TIME' . $suffix] ?? '',
                'end_time'    => $jobs_state[$prefix . 'END_TIME' . $suffix] ?? '',
                'state'       => $jobs_state[$prefix . 'STATE' . $suffix] ?? '',
                'act'         => $jobs_state[$prefix . 'ACT' . $suffix] ?? '',
                'shops_todo'  => $jobs_state[$prefix . 'SHOPS_TODO' . $suffix] ?? '',
                'shop'        => $jobs_state[$prefix . 'SHOP' . $suffix] ?? '',
                'stage'       => $jobs_state[$prefix . 'STAGE' . $suffix] ?? '',
                'loops'       => $jobs_state[$prefix . 'LOOPS' . $suffix] ?? '',
                'progress'    => $jobs_state[$prefix . 'PROGRESS' . $suffix] ?? '',
                'progressmax' => $jobs_state[$prefix . 'PROGRESSMAX' . $suffix] ?? '',
                'data'        => $jobs_state[$prefix . 'DATA' . $suffix] ?? '',
                'message'     => $jobs_state[$prefix . 'MESSAGE' . $suffix] ?? ''
            );
        }
        
        return ['cpanelData' => $status];
    }

    public function setTasks()
    {
        $list_active = Db::getInstance()->executeS(
            'SELECT name
            FROM ' . _DB_PREFIX_ . 'eci_fournisseur
            WHERE perso = 1'
        );

        $result = true;
        foreach ($list_active as $connecteur) {
            $result &= $this->setTasksCon($connecteur['name']);
        }

        return $result;
    }

    /**
     * TODO
     * @param type $connecteur
     * @return type
     */
    public static function clearTasksCon($connecteur)
    {
        // delete cron tasks already present for this connector but not the programmes
        // send the values so we can rebuild the programmes with the good ids
        $crons = Db::getInstance()->executeS('SELECT * FROM '._DB_PREFIX_.'eci_cpanel where suffix = "' . pSQL($connecteur) . '"');
        
        Db::getInstance()->delete('eci_cpanel', 'suffix = "' . pSQL($connecteur) . '"');
        
        return $crons;
    }

    public function setTasksCon($connecteur)
    {
        $modName = 'Eci' . $connecteur;
        $mod = new $modName();
        $logger = self::logStart('tasks');
        // install cron tasks (not forbidden classic plus additionnal) for this connector
        $tab_insert = array();
        $next_position = 1 + (int) self::cleanTaskPositions();

        // get forbidden classic tastks from info2 in the fournisseur table :
        // c : refresh catalog
        // f : import full catalog
        // i : import selection
        // s : update stock
        // t : tracking
        $genClass = 'Ec' . $connecteur;
        $genHide = self::getClassConstant($genClass, 'HIDE_TASKS');
        $list_forbidden = (!is_null($genHide) && $genHide) ? $genHide : (string) Db::getInstance()->getValue(
            'SELECT info2
            FROM ' . _DB_PREFIX_ . 'eci_fournisseur
            WHERE name = "' . pSQL($connecteur) . '"'
        );
        if (false === strpos($list_forbidden, 'c')) {
            $tab_insert[] = array(
                'name' => $mod->l('Refresh module catalog'),
                'link' => pSQL('catAuto.php?connecteur=' . $connecteur),
                'prefix' => pSQL('ECI_CC'),
                'suffix' => pSQL($connecteur),
                'position' => (int) $next_position,
                'specific' => (int) 0
            );
            $next_position++;
        }
        if (false === strpos($list_forbidden, 'i')) {
            $tab_insert[] = array(
                'name' => $mod->l('Import selection'),
                'link' => pSQL('syncAuto.php?connecteur=' . $connecteur),
                'prefix' => pSQL('ECI_SS'),
                'suffix' => pSQL($connecteur),
                'position' => (int) $next_position,
                'specific' => (int) 0
            );
            $next_position++;
        }
        if (false === strpos($list_forbidden, 'f')) {
            $tab_insert[] = array(
                'name' => $mod->l('Import all catalog'),
                'link' => pSQL('syncAuto.php?all&connecteur=' . $connecteur),
                'prefix' => pSQL('ECI_SA'),
                'suffix' => pSQL($connecteur),
                'position' => (int) $next_position,
                'specific' => (int) 0
            );
            $next_position++;
        }
        if (false === strpos($list_forbidden, 's')) {
            $tab_insert[] = array(
                'name' => $mod->l('Update stock and prices'),
                'link' => pSQL('stock.php?connecteur=' . $connecteur),
                'prefix' => pSQL('ECI_STK'),
                'suffix' => pSQL($connecteur),
                'position' => (int) $next_position,
                'specific' => (int) 0
            );
            $next_position++;
        }
        if (false === strpos($list_forbidden, 't')) {
            $tab_insert[] = array(
                'name' => $mod->l('Update order states'),
                'link' => pSQL('tracking.php?connecteur=' . $connecteur),
                'prefix' => pSQL('ECI_UTR'),
                'suffix' => pSQL($connecteur),
                'position' => (int) $next_position,
                'specific' => (int) 0
            );
            $next_position++;
        }

        $class = 'Ec' . $connecteur;
        if (file_exists(dirname(__FILE__) . '/../gen/' . $connecteur . '/class/ec' . $connecteur . '.php')) {
            include_once dirname(__FILE__) . '/../gen/' . $connecteur . '/class/ec' . $connecteur . '.php';
            if (method_exists($class, 'getAdditionnalCrons')) {
                $obj = new $class();
                if (is_array($additionnalCrons = $obj->getAdditionnalCrons())) {
                    foreach ($additionnalCrons as $titre => $infos) {
                        $tab_insert[] = array(
                            'name' => pSQL($titre),
                            'link' => pSQL($infos['link']),
                            'prefix' => pSQL($infos['prefix']),
                            'suffix' => pSQL($infos['suffix']),
                            'position' => (int) $next_position,
                            'specific' => (int) 1
                        );
                        $next_position++;
                    }
                }
            }
        }

        // inscrire tout ça dans la table des cpanel
        try {
            return Db::getInstance()->insert('eci_cpanel', $tab_insert, false, false, Db::REPLACE);
        } catch (Exception $e) {
            self::logInfo($logger, __FUNCTION__ . ' ' . $connecteur . ' error : ' . $e->getMessage());
        }

        return false;
    }

    public static function cleanTaskPositions()
    {
        Db::getInstance()->execute(
            'UPDATE ' . _DB_PREFIX_ . 'eci_cpanel c1
            JOIN (
                SELECT c.id_cpanel, @i := @i+1 new_position
                FROM ' . _DB_PREFIX_ . 'eci_cpanel c, (select @i:=0) temp
                ORDER BY c.position ASC) c2
            ON c1.id_cpanel = c2.id_cpanel
            SET c1.position = c2.new_position'
        );

        return Db::getInstance()->getValue('SELECT MAX(position) FROM ' . _DB_PREFIX_ . 'eci_cpanel');
    }
    
    /**
     * TODO
     * @param type $old_crons
     */
    public static function rebuildProgs($old_crons)
    {
        //old crons give old ids but also unique combinations of prefix-suffix (except "nofollow")
        //so we can update link values in eci_prg_cron with the new id_cpanel given to the tasks
        
        
        //please put the programmes at the last positions using cleanTaskPositions 2 times (before first programme, after last programme)
    }

    public function setCarriers($connecteur)
    {
        // the gen function should :
        // - delete any suppcarrier not linked to a non deleted PS carrier
        // - delete any suppcarrier not in the up to date list of supplier carriers (after a module update for example) and delete the corresponding ps carrier
        // - install missing carriers
        if (($ec_four = $this->getGenClass($connecteur, $this->id_shop))) {
            if (method_exists($ec_four, 'setCarriers')) {
                return $ec_four->setCarriers();
            }
        }
    }

    public static function registerCarrier($carrier, $connecteur = null, $zones = false, $ranges = false, $groups = false)
    {
        if (is_null($connecteur)) {
            $connecteur = self::getConnecteurName();
        }

        $id_lang = Configuration::get('PS_LANG_DEFAULT');

        // register PS carrier if not known
        $PScarrier = new Carrier();
        $PScarrier->name = self::getStrCutBytes($carrier['name'], 64);
        $PScarrier->active = 1;
        $PScarrier->shipping_method = isset($carrier['shipping_method']) ? (int) $carrier['shipping_method'] : Carrier::SHIPPING_METHOD_DEFAULT; //0 default, 1 weight, 2 price, 3 free
        $PScarrier->is_free = isset($carrier['is_free']) ? (bool)$carrier['is_free'] : 0;
        $PScarrier->delay = $carrier['delay'];
        $PScarrier->is_module = 1;
        $PScarrier->shipping_handling = isset($carrier['shipping_handling']) ? (bool) $carrier['shipping_handling'] : 1;
        $PScarrier->range_behavior = isset($carrier['range_behavior']) ? (bool) $carrier['range_behavior'] : 0;
        $PScarrier->shipping_external = 1;
        $PScarrier->external_module_name = 'eci' . $connecteur;
        $PScarrier->need_range = 1;
        $PScarrier->id_tax_rules_group = 1; // the default one
        $PScarrier->max_width = (int) ($carrier['max_width'] ?? 0);
        $PScarrier->max_height = (int) ($carrier['max_height'] ?? 0);
        $PScarrier->max_depth = (int) ($carrier['max_depth'] ?? 0);
        $PScarrier->max_weight = (float) ($carrier['max_weight'] ?? 0);

        if ($PScarrier->add()) {
            //shops
            $PScarrier->associateTo(Shop::getCompleteListOfShopsID());

            //tax rules
            $taxRulesGroups = TaxRulesGroup::getTaxRulesGroups(false);
            $idTaxRuleGroup = 0;
            foreach ($taxRulesGroups as $taxRulesGroup) {
                if (preg_match('#.*FR.*20\%.*#i', $taxRulesGroup['name'])) {
                    $idTaxRuleGroup = $taxRulesGroup['id_tax_rules_group'];
                    break;
                }
            }
            $PScarrier->setTaxRulesGroup($idTaxRuleGroup, true);

            //groups
            if ($groups) {
                $groups = is_array($groups) ? $groups : array($groups);
                $PScarrier->setGroups($groups, true);
            } else {
                $groups = Group::getGroups($id_lang);
                $list_groups = array();
                foreach ($groups as $group) {
                    $list_groups[] = $group['id_group'];
                }
                $PScarrier->setGroups($list_groups, true);
            }

            //zones
            $list_zones = array();
            if ($zones) {
                if (!is_array($zones)) {
                    $zones = array($zones);
                }
                foreach ($zones as $zone) {
                    if (!is_numeric($zone)) {
                        continue;
                    }
                    $list_zones[] = (int) $zone;
                    $PScarrier->addZone((int) $zone);
                }
            } else {
                $zones = Zone::getZones();
                foreach ($zones as $zone) {
                    $list_zones[] = $zone['id_zone'];
                    $PScarrier->addZone($zone['id_zone']);
                }
            }

            //ranges
            if ($ranges) {
                if (isset($ranges['RangePrice'])) {
                    if (isset($ranges['RangePrice']['delimiter1']) && isset($ranges['RangePrice']['delimiter2'])) {
                        $rangePrice = new RangePrice();
                        $rangePrice->id_carrier = $PScarrier->id;
                        $rangePrice->delimiter1 = $ranges['RangePrice']['delimiter1'];
                        $rangePrice->delimiter2 = $ranges['RangePrice']['delimiter2'];
                        $rangePrice->add();
                    }
                }
                if (isset($ranges['RangeWeight'])) {
                    if (isset($ranges['RangeWeight']['delimiter1']) && isset($ranges['RangeWeight']['delimiter2'])) {
                        $rangeWeight = new RangeWeight();
                        $rangeWeight->id_carrier = $PScarrier->id;
                        $rangeWeight->delimiter1 = $ranges['RangeWeight']['delimiter1'];
                        $rangeWeight->delimiter2 = $ranges['RangeWeight']['delimiter2'];
                        $rangeWeight->add();
                    }
                }
            } else {
                $rangePrice = new RangePrice();
                $rangePrice->id_carrier = $PScarrier->id;
                $rangePrice->delimiter1 = '0';
                $rangePrice->delimiter2 = '1000000000';
                $rangePrice->add();
                $rangeWeight = new RangeWeight();
                $rangeWeight->id_carrier = $PScarrier->id;
                $rangeWeight->delimiter1 = '0';
                $rangeWeight->delimiter2 = '1000000000';
                $rangeWeight->add();
            }

            //delivery
            foreach ($list_zones as $id_zone) {
                $id_delivery1 = Db::getInstance()->getValue(
                    'SELECT id_delivery
                    FROM ' . _DB_PREFIX_ . 'delivery
                    WHERE id_carrier = ' . (int) $PScarrier->id . '
                    AND id_range_price = ' . (int) $rangePrice->id . '
                    AND id_zone = ' . (int) $id_zone
                );
                if ($id_delivery1) {
                    $delivery1 = new Delivery($id_delivery1);
                } else {
                    $delivery1 = new Delivery();
                }
                //il y a un petit souci avec ça il faut la renseigner
                $delivery1->id_range_price = $rangePrice->id;
                $delivery1->id_range_weight = 0;
                $delivery1->price = 0;
                $delivery1->save();
                Db::getInstance()->execute(
                    'UPDATE ' . _DB_PREFIX_ . 'delivery
                    SET id_range_weight = NULL, id_shop = NULL, id_shop_group = NULL
                    WHERE id_delivery = ' . (int) $delivery1->id_delivery
                );

                $id_delivery2 = Db::getInstance()->getValue(
                    'SELECT id_delivery
                    FROM ' . _DB_PREFIX_ . 'delivery
                    WHERE id_carrier = ' . (int) $PScarrier->id . '
                    AND id_range_weight = ' . (int) $rangeWeight->id . '
                    AND id_zone = ' . (int) $id_zone
                );
                if ($id_delivery2) {
                    $delivery2 = new Delivery($id_delivery2);
                } else {
                    $delivery2 = new Delivery();
                }
                //il y a un petit souci avec ça il faut la renseigner
                $delivery2->id_range_price = 0;
                $delivery2->id_range_weight = $rangeWeight->id;
                $delivery2->price = 0;
                $delivery2->save();
                //ensuite il faut la remettre à NULL
                Db::getInstance()->execute(
                    'UPDATE ' . _DB_PREFIX_ . 'delivery
                    SET id_range_price = NULL, id_shop = NULL, id_shop_group = NULL
                    WHERE id_delivery = ' . (int) $delivery2->id_delivery
                );
            }

            // register ecicarrier
            $class = 'Suppcarrier' . $connecteur;
            $classDefinition = self::getClassProperty($class, 'definition');
            $suppcarrier = new $class();
            $suppcarrier->name = self::getStrCutBytes($carrier['name'], 64);
            $suppcarrier->supplier_key = $carrier['supplier_key'];
            $suppcarrier->delay = $carrier['delay'];
            $suppcarrier->id_carrier = (int) $PScarrier->id;
            $suppcarrier->use_real = isset($carrier['use_real']) ? (bool) $carrier['use_real'] : 1;
            $suppcarrier->use_increment = isset($carrier['use_increment']) ? (bool) $carrier['use_increment'] : 0;
            $suppcarrier->increment_value = isset($carrier['increment_value']) ? (float) $carrier['increment_value'] : 0;
            if (isset($carrier['increment_type']) &&
                isset($classDefinition['fields']['increment_type']['values']) &&
                is_array($classDefinition['fields']['increment_type']['values']) &&
                in_array($carrier['increment_type'], $classDefinition['fields']['increment_type']['values'])) {
                $suppcarrier->increment_type = $carrier['increment_type'];
            }
            $suppcarrier->fournisseur = $connecteur;
            $suppcarrier->active = 1;
            $suppcarrier->add();

            return $suppcarrier->id;
        }

        return false;
    }

    public static function followLink($link, $forced_timeout = null)
    {
        $timeout = is_null($forced_timeout) ? static::FOLLOWLINK_TIMEOUT : $forced_timeout;
        $logger = self::logStart('curl');
        $a_ret = self::goCurl($link, $timeout);
        if (preg_match('/SSL/', $a_ret['infos']['err'])) {
            $link = str_replace('https', 'http', $link);
            $a_ret = self::goCurl($link, $timeout);
        }
        $tries = 1;
        //'Operation timed out after x milliseconds' is normal response
        while (preg_match('/Connection\ timed\ out\ after/', $a_ret['infos']['err'])      // 522
            || preg_match('/connect\(\)\ timed\ out\!/', $a_ret['infos']['err'])          //
            || preg_match('/Gateway\ Time\-out/', $a_ret['infos']['err'])                 // 504
            || preg_match('/name\ lookup\ timed\ out/', $a_ret['infos']['err'])           //
            || preg_match('/Resolving\ timed\ out\ after/', $a_ret['infos']['err'])) {    //
            self::logInfo(
                $logger,
                'Error (' . $a_ret['infos']['errno'] . ') "' . $a_ret['infos']['err']. '"' . "\n"
                . 'Infos ' . var_export($a_ret['infos']['infos'], true)
            );
            $tries++;
            if ((static::FOLLOWLINK_RETRIES && static::FOLLOWLINK_RETRIES < $tries) || (50 < $tries)) {
                return false;
            }
            self::logInfo($logger, 'Try (' . $tries . ') relaunching ' . $link);
            $a_ret = self::goCurl($link, $timeout);
        }
/*        while (in_array($a_ret['infos']['errno'], array('6', '7'))) { // 22 ? test http
            self::logInfo(
                $logger,
                'Error (' . $a_ret['infos']['errno'] . ') "' . $a_ret['infos']['err']. '"' . "\n"
                . 'Infos ' . var_export($a_ret['infos']['infos'], true) . "\n"
                . 'Relaunched ' . $link);
            $a_ret = self::goCurl($link, $timeout);
        }*/

        if (static::FOLLOWLINK_LOG) {
            self::logInfo($logger, var_export($a_ret, true));
        }

        return $a_ret['result'];
    }

    public static function goCurl($link, $forced_timeout = null)
    {
        $timeout = is_null($forced_timeout) ? static::FOLLOWLINK_TIMEOUT : $forced_timeout;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $link);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
//        curl_setopt($ch, CURLOPT_USERPWD, 'user:pass');
        curl_setopt($ch, CURLOPT_CAINFO, dirname(__FILE__) . '/../cacert.pem');
        //curl_setopt($ch, CURLOPT_CAINFO, _PS_CACHE_CA_CERT_FILE_);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);
        $result = curl_exec($ch);
        $tab_err = array(
            'err' => curl_error($ch),
            'errno' => curl_errno($ch),
            'infos' => curl_getinfo($ch)
        );
        curl_close($ch);

        return array('infos' => $tab_err, 'result' => $result);
    }

    public static function answer($response)
    {
        if (ob_get_level() && ob_get_length() > 0) {
            ob_clean();
        }
        
        if (is_callable('fastcgi_finish_request')) {
            /*
             * This works in Nginx but the next approach not
             */
            echo $response;
            session_write_close();
            fastcgi_finish_request();
            return;
        }
        
        if (is_callable('litespeed_finish_request')) {
            /*
             * This works in Nginx but the next approach not
             */
            echo $response;
            session_write_close();
            litespeed_finish_request();
            return;
        }

        ob_start();
        $serverProtocole = filter_input(INPUT_SERVER, 'SERVER_PROTOCOL', FILTER_SANITIZE_STRING);
        if (!preg_match('/^HTTP/', $serverProtocole)) {
            $serverProtocole = ($_SERVER['SERVER_PROTOCOL'] ?? 'HTTP/1.0') ?: 'HTTP/1.0';
        }
        header($serverProtocole.' 200 OK');
        echo $response;
        header("Content-Encoding: none");
        header('Content-Length: '.ob_get_length());
        header('Connection: close');
        ob_end_flush();
        @ob_flush();
        flush();
    }
    
    public static function getUrlBrowsed($url, $header = array(), $credentials = array())
    {
        if (!is_array($header)) {
            return false;
        }
        
        array_unshift($header, "Accept: text/xml,application/xml,application/xhtml+xml,text/html;q=0.9,text/plain;q=0.8,image/png,image/jpeg,*/*;q=0.5");
        $header[] = "Cache-Control: max-age=0";
        $header[] = "Connection: keep-alive";
        $header[] = "Keep-Alive: 300";
        $header[] = "Accept-Charset: utf-8,ISO-8859-1;q=0.8,*;q=0.7";
        $header[] = "Accept-Language: fr,en-us,en;q=0.5";
        $header[] = "Pragma: ";

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:42.0) Gecko/20100101 Firefox/42.0');
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        if ($credentials) {
            curl_setopt($ch, CURLOPT_USERPWD, implode(':', $credentials));
        }
        curl_setopt($ch, CURLOPT_REFERER, 'http://www.google.com');
        curl_setopt($ch, CURLOPT_AUTOREFERER, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_ENCODING, 'gzip,deflate');
        curl_setopt($ch, CURLOPT_COOKIEJAR, static::FILES_PATH . 'cookie.txt');
        curl_setopt($ch, CURLOPT_COOKIEFILE, static::FILES_PATH . 'cookie.txt');
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
        curl_setopt($ch, CURLINFO_HEADER_OUT, true);
        $result = curl_exec($ch);
        $err = curl_error($ch);
        $errno = curl_errno($ch);
        $infos = curl_getinfo($ch);

        return array(
            'result' => $result,
            'errno' => $errno,
            'err' => $err,
            'infos' => $infos
        );
    }

    public static function revMod()
    {
        $dcat = self::getInfoEco('ECIPT1');
        $fcat = self::$dcat(self::getInfoEco('ECIPF1'));
        $lcat = self::$dcat(self::getInfoEco('ECIPL1'));
        $reps = json_decode($fcat($lcat, false, self::getStrCon($dcat, self::getInfoEco('ECIPS1'), self::getInfoEco('ECIPC1'))), true) ?? false;
        $return = $reps ? ($reps['return'] ?? false) : false;

        return ($return ? ('OK' === $return) : false);
    }

    public static function killWaitLoop($connecteur, $message = false)
    {
        Db::getInstance()->update(
            'eci_jobs',
            array(
                'value' => 'die'
            ),
            'name = "ECI_WL_ACT_' . pSQL($connecteur) . '"'
        );
        if (!$message) {
            return;
        }
        Db::getInstance()->update(
            'eci_jobs',
            array(
                'value' => pSQL($message)
            ),
            'name = "ECI_WL_MESSAGE_' . pSQL($connecteur).'"'
        );
    }

    public function getGenClass($connecteur, $id_shop = 0)
    {
        if (!$id_shop) {
            $id_shop = $this->id_shop;
        }

        return self::getGenClassStatic($connecteur, $id_shop);
    }

    public static function getGenClassStatic($connecteur, $id_shop = 0)
    {
        if (!$id_shop) {
            $id_shop = Configuration::get('PS_SHOP_DEFAULT');
        }
        $class = 'Ec' . $connecteur;
        $path = dirname(__FILE__) . '/../gen/' . $connecteur . '/class/ec' . $connecteur . '.php';

        if (!file_exists($path)) {
            return false;
        }

        require_once $path;

        return new $class($id_shop);
    }

    public static function cacheStore($key, $value = 1)
    {
        Cache::store($key, $value);
        $val = serialize($value);
        Db::getInstance()->insert(
            'eci_cache',
            array(
                'key' => pSQL($key),
                'value' => pSQL($val)
            ),
            false,
            false,
            Db::REPLACE
        );
    }

    public static function cacheStoreMore($values = array())
    {
        if (!$values) {
            return;
        }

        $inserts = array();
        foreach ($values as $key => $value) {
            Cache::store($key, $value);
            $val = serialize($value);
            $inserts[] = array(
                'key' => pSQL($key),
                'value' => pSQL($val)
            );
        }

        Db::getInstance()->insert(
            'eci_cache',
            $inserts,
            false,
            false,
            Db::REPLACE
        );
    }

    public static function cacheIsStored($key)
    {
        if (Cache::isStored($key)) {
            return true;
        }
        self::cacheClean();
        return (bool) Db::getInstance()->getValue(
            'SELECT COUNT(*)
            FROM ' . _DB_PREFIX_ . 'eci_cache
            WHERE `key` = "' . pSQL($key) . '"'
        );
    }

    public static function cacheRetrieve($key)
    {
        if (Cache::isStored($key)) {
            return Cache::retrieve($key);
        }

        $data = Db::getInstance()->getRow(
            'SELECT *
            FROM ' . _DB_PREFIX_ . 'eci_cache
            WHERE `key` = "' . pSQL($key) . '"'
        );
        if (!$data) {
            return false;
        }

        return unserialize($data['value']);
    }

    public static function cacheDelete($key)
    {
        Cache::clean($key);
        Db::getInstance()->delete('eci_cache', '`key` = "' . pSQL($key) . '"');
    }

    public static function cacheClean($duration = self::CACHE_PERSISTANCE)
    {
        Db::getInstance()->delete(
            'eci_cache',
            'ts < (NOW() - INTERVAL ' . (int) $duration . ' SECOND)'
        );
    }

    public static function cacheClear()
    {
        Db::getInstance()->execute('TRUNCATE TABLE ' . _DB_PREFIX_ . 'eci_cache');
    }

    public static function getConnecteurName()
    {
        return str_replace('eci', '', __NAMESPACE__);
    }

    public static function getReferences($reference, $connecteur = null)
    {
        if (is_null($connecteur)) {
            $connecteur = self::getConnecteurName();
        }

        $variant = Db::getInstance()->getRow(
            'SELECT *
            FROM ' . _DB_PREFIX_ . 'eci_catalog_attribute
            WHERE reference_attribute = "'.pSQL($reference).'"
            AND fournisseur = "'.pSQL($connecteur).'"'
        );
        if (!$variant) {
            $parent = Db::getInstance()->getRow(
                'SELECT *
                FROM ' . _DB_PREFIX_ . 'eci_catalog
                WHERE product_reference = "'.pSQL($reference).'"
                AND fournisseur = "'.pSQL($connecteur).'"'
            );
            if (!$parent) {
                return false;
            }

            return array(
                'product_reference' => $parent['product_reference'],
                'reference_attribute' => ''
            );
        }

        return array(
            'product_reference' => $variant['product_reference'],
            'reference_attribute' => $variant['reference_attribute']
         );
    }

    public static function getProduct($reference, $connecteur = '')
    {
        if (empty($connecteur)) {
            $connecteur = self::getConnecteurName();
        }

        $dataps_tmp = Db::getInstance()->executeS(
            'SELECT *
            FROM ' . _DB_PREFIX_ . 'eci_product_shop
            WHERE reference = "'.pSQL($reference).'"
            AND id_product_attribute = 0
            AND fournisseur = "'.pSQL($connecteur).'"'
        );
        $dataps = array();
        $datapas = array();
        if ($dataps_tmp) {
            foreach ($dataps_tmp as $dataps_shop) {
                $dataps[$dataps_shop['id_shop']] = $dataps_shop;
            }
            $id_products = array_unique(array_column($dataps, 'id_product'));
            $datapas_tmp = Db::getInstance()->executeS(
                'SELECT *
                FROM ' . _DB_PREFIX_ . 'eci_product_shop
                WHERE id_product in ("' . implode('","', $id_products) . '")
                AND id_product_attribute != 0
                AND fournisseur = "'.pSQL($connecteur).'"'
            );
            foreach ($datapas_tmp as $datapas_shop) {
                $datapas[$datapas_shop['id_shop']][] = $datapas_shop;
            }
        }
        $datac = Db::getInstance()->getRow(
            'SELECT *
            FROM ' . _DB_PREFIX_ . 'eci_catalog
            WHERE product_reference = "'.pSQL($reference).'"
            AND fournisseur = "'.pSQL($connecteur).'"'
        );
        $dataca = Db::getInstance()->executeS(
            'SELECT *
            FROM ' . _DB_PREFIX_ . 'eci_catalog_attribute
            WHERE product_reference = "'.pSQL($reference).'"
            AND fournisseur = "'.pSQL($connecteur).'"'
        );

        $data = array();
        $data['produit'] = is_array($datac) ? self::arJsonDecodeRecur($datac) : array();
        $data['declinaisons'] = is_array($dataca) ? self::arJsonDecodeRecur($dataca) : array();
        $data['prestashop']['produit'] = $dataps ? self::arJsonDecodeRecur($dataps) : array();
        $data['prestashop']['declinaisons'] = $datapas ? self::arJsonDecodeRecur($datapas) : array();

        return $data;
    }

    public static function getCombination($reference, $connecteur = '')
    {
        if (empty($connecteur)) {
            $connecteur = self::getConnecteurName();
        }

        $dataps = Db::getInstance()->getRow(
            'SELECT *
            FROM ' . _DB_PREFIX_ . 'eci_product_shop
            WHERE reference = "'.pSQL($reference).'"
            AND id_product_attribute != 0
            AND fournisseur = "'.pSQL($connecteur).'"'
        );
        if ($dataps) {
            $dataps['parent'] = self::getParentReferenceFromId($dataps['id_product']);
        }
        $dataca = Db::getInstance()->getRow(
            'SELECT *
            FROM ' . _DB_PREFIX_ . 'eci_catalog_attribute
            WHERE reference_attribute = "'.pSQL($reference).'"
            AND fournisseur = "'.pSQL($connecteur).'"'
        );

        $data = array();
        $data['declinaison'] = is_array($dataca) ? self::arJsonDecodeRecur($dataca) : array();
        $data['prestashop'] = $dataps ? self::arJsonDecodeRecur($dataps) : array();

        return $data;
    }
    
    public static function getParentReferenceFromId($id_product)
    {
        return Db::getInstance()->getValue(
            'SELECT reference
            FROM '._DB_PREFIX_.'eci_product_shop
            WHERE id_product = '.(int)$id_product.'
            AND id_product_attribute = 0'
        );
    }

    public static function getKeepFromIds($ids)
    {
        if (!is_array($ids)) {
            $ids = [
                'id_product' => (int)$ids,
                'id_product_attribute' => 0,
            ];
        }
        
        $raw_keep = Db::getInstance()->getValue(
            'SELECT keep
            FROM '._DB_PREFIX_.'eci_product_shop
            WHERE id_product = '.(int)($ids['id_product']??0).'
            AND id_product_attribute = '.(int)($ids['id_product_attribute']??0)
        );
        
        return self::arJsonDecodeRecur($raw_keep);
    }

    public function getProductName($pref, $aref = '', $iso_lang = '', $connecteur = null)
    {
        if (!$iso_lang) {
            $iso_lang = Language::getIsoById($this->id_lang);
        }

        return self::getProductNameStatic($pref, $aref, $iso_lang, $connecteur);
    }

    public static function getProductNameStatic($pref, $aref, $iso_lang, $connecteur = null)
    {
        if (is_null($connecteur)) {
            $connecteur = self::getConnecteurName();
        }

        $product_name_lg = Db::getInstance()->getValue(
            'SELECT name
            FROM ' . _DB_PREFIX_ . 'eci_catalog
            WHERE product_reference = "' . pSQL($pref) . '"
            AND fournisseur = "' . pSQL($connecteur) . '"'
        );

        $tab_names = self::langExpl($product_name_lg);

        $product_name = isset($tab_names[$iso_lang]) ? $tab_names[$iso_lang] : reset($tab_names);

        if (!$aref) {
            return $product_name;
        }

        $attributes_lg = Db::getInstance()->getValue(
            'SELECT attribute
            FROM ' . _DB_PREFIX_ . 'eci_catalog_attribute
            WHERE product_reference = "' . pSQL($pref) . '"
            AND reference_attribute = "' . pSQL($aref) . '"
            AND fournisseur = "' . pSQL($connecteur) . '"'
        );

        if (!$attributes_lg) {
            return $product_name;
        }

        $tat = self::parseAttAndFeat($attributes_lg);
        $at_desc = self::getAttributesDescription($tat, $iso_lang);
        $at_desc_clean = str_replace(array(':', ';', '  '), array('', '', ' '), $at_desc);

        return $product_name . ' ' . $at_desc_clean;
    }
    
    public static function getProductSupplierReference($id_product, $id_product_attribute = 0, $connecteur = null)
    {
        if (is_null($connecteur)) {
            $connecteur = self::getConnecteurName();
        }
        
        $supplier_reference = Db::getInstance()->getValue(
            'SELECT reference
            FROM ' . _DB_PREFIX_ . 'eci_product_shop
            WHERE id_product = ' . (int) $id_product . '
            AND id_product_attribute = ' . (int) $id_product_attribute . '
            AND fournisseur = "' . pSQL($connecteur) . '"'
        );
        if (!$supplier_reference) {
            $id_supplier = self::getEciFournId($connecteur);
            $supplier_reference = Db::getInstance()->getValue(
                'SELECT product_supplier_reference
                FROM ' . _DB_PREFIX_ . 'product_supplier
                WHERE id_product = ' . (int) $id_product . '
                AND id_product_attribute = ' . (int) $id_product_attribute . '
                AND id_supplier = ' . (int) $id_supplier
            );
        }
        if (!$supplier_reference) {
            $supplier_reference = Db::getInstance()->getValue(
                'SELECT supplier_reference
                FROM ' . _DB_PREFIX_ . 'product' . ($id_product_attribute ? '_attribute' : '') . '
                WHERE id_product = ' . (int) $id_product .
                ($id_product_attribute ? ' AND id_product_attribute = ' . (int) $id_product_attribute : '')
            );
        }
        
        return $supplier_reference;
    }

    public static function arJsonDecodeRecur($a)
    {
        if (!is_array($a) && is_null($a2 = Tools::jsonDecode($a, true))) {
            return $a;
        } elseif (!is_array($a) && is_array($a2)) {
            return self::arJsonDecodeRecur($a2);
        } elseif (!is_array($a)) {
            return $a2;
        }

        foreach ($a as &$v) {
            $v = is_array($v) ? self::arJsonDecodeRecur($v) : (is_null($w = Tools::jsonDecode($v, true)) ? $v : $w);
        }

        return $a;
    }

    public function getProductShopData($id_product, $id_product_attribute = 0, $connecteur = null, $id_shop = 0)
    {
        if (is_null($connecteur)) {
            $connecteur = self::getConnecteurName();
        }

        if (!$id_shop) {
            $id_shop = $this->id_shop;
        }

        return self::getProductShopDataStatic($id_product, $id_product_attribute, $connecteur, $id_shop);
    }

    public static function getProductShopDataStatic($id_product, $id_product_attribute, $connecteur, $id_shop)
    {
        $data = Db::getInstance()->getRow(
            'SELECT *
            FROM ' . _DB_PREFIX_ . 'eci_product_shop
            WHERE id_product = ' . (int) $id_product . '
            AND id_product_attribute = ' . (int) $id_product_attribute . '
            AND id_shop = ' . (int) $id_shop . '
            AND fournisseur = "' . pSQL($connecteur) . '"'
        );

        return self::arJsonDecodeRecur($data);
    }

    public static function getProductShopActReasons($id_product, $connecteur, $id_shop)
    {
        $data = Db::getInstance()->getRow(
            'SELECT act_reasons
            FROM ' . _DB_PREFIX_ . 'eci_product_shop
            WHERE id_product = ' . (int) $id_product . '
            AND id_product_attribute = 0
            AND id_shop = ' . (int) $id_shop . '
            AND fournisseur = "' . pSQL($connecteur) . '"'
        );

        return self::arJsonDecodeRecur($data['act_reasons']);
    }

    public static function getProductShopActState($id_product, $connecteur, $id_shop)
    {
        $act_reasons = self::getProductShopActReasons($id_product, $connecteur, $id_shop);

        return self::getActState($act_reasons);
    }

    public static function getActStateFromProduct($product, $id_shop)
    {
        if (isset($product['prestashop']['produit'][$id_shop]['act_reasons'])) {
            $act_reasons = $product['prestashop']['produit'][$id_shop]['act_reasons'];
            return self::getActState($act_reasons);
        }

        return true;
    }

    public static function getActStateFromProductShop($product)
    {
        if (isset($product['act_reasons'])) {
            $act_reasons = $product['act_reasons'];
            return self::getActState($act_reasons);
        }

        return true;
    }

    public static function getActState($act_reasons)
    {
        if ("" === $act_reasons) {
            return true;
        } elseif (is_array($act_reasons)) {
            return (count($act_reasons) == count(array_filter($act_reasons)));
        } else {
            return (bool) $act_reasons;
        }
    }

    public static function updateProductShopActReasons($id_product, $connecteur, $id_shop, $key, $val = true)
    {
        $act_reasons = self::getProductShopActReasons($id_product, $connecteur, $id_shop);

        if (is_array($act_reasons)) {
            $act_reasons[$key] = (bool) $val;
        } else {
            $act_reasons = array($key => (bool) $val);
        }

        $data = Tools::jsonEncode($act_reasons);

        Db::getInstance()->update(
            'eci_product_shop',
            array(
                'act_reasons' => pSQL($data)
            ),
            'id_product = ' . (int) $id_product . ' AND id_product_attribute = 0 AND id_shop = ' . (int) $id_shop . ' AND fournisseur = "' . pSQL($connecteur) . '"'
        );
    }

    public static function updateProductShopActReasonsAllShops($id_product, $connecteur, $key, $val = true)
    {
        $shops = Shop::getCompleteListOfShopsID();
        foreach ($shops as $id_shop) {
            $act_reasons = self::getProductShopActReasons($id_product, $connecteur, $id_shop);

            if (is_array($act_reasons)) {
                $act_reasons[$key] = (bool) $val;
            } else {
                $act_reasons = array($key => (bool) $val);
            }

            $data = Tools::jsonEncode($act_reasons);

            Db::getInstance()->update(
                'eci_product_shop',
                array(
                    'act_reasons' => pSQL($data)
                ),
                'id_product = ' . (int) $id_product . ' AND id_product_attribute = 0 AND id_shop = ' . (int) $id_shop . ' AND fournisseur = "' . pSQL($connecteur) . '"'
            );
        }
    }

    public static function resetProductShopActReasons($connecteur = null, $id_shop = null, $id_product = null)
    {
        Db::getInstance()->update(
            'eci_product_shop',
            array(
                'act_reasons' => pSQL("")
            ),
            'id_product_attribute = 0'
            . (is_null($connecteur) ? '' : ' AND fournisseur = "' . pSQL($connecteur) . '"')
            . (is_null($id_shop) ? '' : ' AND id_shop = ' . (int) $id_shop)
            . (is_null($id_product) ? '' : ' AND id_product = ' . (int) $id_product)
        );
    }

    public static function getConfigTempTable($mandatory_keys = array(), $connecteur = null, $permanent = false)
    {
        if (is_null($connecteur)) {
            $connecteur = self::getConnecteurName();
        }

        //drop table if exists
        Db::getInstance()->execute('DROP ' . (!$permanent ? 'TEMPORARY' : '') . ' TABLE IF EXISTS ' . _DB_PREFIX_ . 'eci_conftmp');

        //create temp sql table for config : id_shop is the key
        Db::getInstance()->execute('CREATE ' . (!$permanent ? 'TEMPORARY' : '') . ' TABLE ' . _DB_PREFIX_ . 'eci_conftmp (id_shop INT PRIMARY KEY)');

        //get config for this connector
        $global_config = Db::getInstance()->executeS('SELECT * FROM ' . _DB_PREFIX_ . 'eci_config WHERE fournisseur = "' . pSQL($connecteur) . '"');

        // buils lists : keys and inserts
        $mkeys = $mandatory_keys;
        $configs = array();
        foreach ($global_config as $ck) {
            $mkeys[] = $ck['name'];
            $configs[$ck['id_shop']]['id_shop'] = (int) $ck['id_shop'];
            $configs[$ck['id_shop']][$ck['name']] = pSQL($ck['value']);
        }
        $keys = array_unique($mkeys);

        //alter table with all keys found, build regular inserts
        $alter = array();
        foreach ($keys as $key) {
            $alter[] = 'ADD `' . pSQL($key) . '` TEXT DEFAULT NULL';
            foreach ($configs as $id_shop => $config) {
                if (!isset($config[$key])) {
                    $configs[$id_shop][$key] = '';
                }
            }
        }
        foreach ($configs as &$config) {
            ksort($config);
        }
        if ($alter) {
            Db::getInstance()->execute('ALTER TABLE ' . _DB_PREFIX_ . 'eci_conftmp ' . implode(', ', $alter));
        }

        //insert values of config by key id_shop
        Db::getInstance()->insert(
            'eci_conftmp',
            $configs,
            false,
            false,
            Db::INSERT_IGNORE
        );
    }

    public static function resetEciStock($connecteur = null, $id_shop = false)
    {
        if (is_null($connecteur)) {
            $connecteur = self::getConnecteurName();
        }

        return Db::getInstance()->update(
            'eci_catalog_stock',
            array(
                'upd_flag' => (int)0
            ),
            'fournisseur = "' . pSQL($connecteur) . '"' . ($id_shop ? ' AND id_shop = ' . (int)$id_shop : '')
        );
    }

    public static function zeroEciStock($connecteur = null, $id_shop = false, $all = false, $keep_negative = false)
    {
        if (is_null($connecteur)) {
            $connecteur = self::getConnecteurName();
        }

        $where = 'fournisseur = "' . pSQL($connecteur) . '"' . ($id_shop ? ' AND id_shop = ' . (int)$id_shop : '');
        if (!$all) {
            $where .= $keep_negative ? ' AND upd_flag = 0': ' AND (stock < 0 OR upd_flag = 0)';
        }
        
        return Db::getInstance()->update(
            'eci_catalog_stock',
            array(
                'stock' => (int)0
            ),
            $where
        );
    }

    public static function copyEciStock($connecteur, $id_shop1, $id_shop2)
    {
        Db::getInstance()->execute(
            'INSERT INTO ' . _DB_PREFIX_ . 'eci_catalog_stock (product_reference, reference_attribute, id_shop, stock, fournisseur, upd_flag)
            SELECT product_reference, reference_attribute, ' . (int) $id_shop2 . ', stock, fournisseur, upd_flag
            FROM ' . _DB_PREFIX_ . 'eci_catalog_stock
            WHERE fournisseur = "' . pSQL($connecteur) . '" AND id_shop = ' . (int) $id_shop1 . '
            ON DUPLICATE KEY UPDATE stock=VALUES(stock), upd_flag=VALUES(upd_flag)'
        );
    }

    public static function setEciStock($pref, $stock, $aref = null, $connecteur = null, $id_shop = false)
    {
        if (is_null($connecteur)) {
            $connecteur = self::getConnecteurName();
        }

        $array = array(
            'product_reference' => pSQL($pref),
            'stock' => (int)$stock,
            'id_shop' => (int)$id_shop,
            'fournisseur' => pSQL($connecteur),
            'upd_flag' => (int)1,
        );

        if (!is_null($aref)) {
            $array['reference_attribute'] = pSQL($aref);
        }

        return Db::getInstance()->insert(
            'eci_catalog_stock',
            $array,
            false,
            false,
            Db::REPLACE
        );
    }

    public static function getEciStock($ref, $connecteur = null, $id_shop = 0)
    {
        if (is_null($connecteur)) {
            $connecteur = self::getConnecteurName();
        }

        $stock = Db::getInstance()->getValue(
            'SELECT stock
            FROM ' . _DB_PREFIX_ . 'eci_catalog_stock
            WHERE reference_attribute = "' . pSQL($ref) . '"
            AND fournisseur = "' . pSQL($connecteur) . '"
            AND id_shop = ' . (int) $id_shop
        );

        if (false !== $stock) {
            return (int) $stock;
        }

        return (int) Db::getInstance()->getValue(
            'SELECT SUM(stock) as stock
            FROM ' . _DB_PREFIX_ . 'eci_catalog_stock
            WHERE product_reference = "' . pSQL($ref) . '"
            AND fournisseur = "' . pSQL($connecteur) . '"
            AND id_shop = ' . (int) $id_shop . '
            GROUP BY product_reference'
        );
    }

    public static function existsEciStock($connecteur = null)
    {
        if (is_null($connecteur)) {
            $connecteur = self::getConnecteurName();
        }

        if (! Db::getInstance()->executeS('SHOW TABLES LIKE "' . _DB_PREFIX_ . 'eci_catalog_stock"')) {
            return false;
        }

        return (bool) Db::getInstance()->getValue('SELECT COUNT(*) FROM ' . _DB_PREFIX_ . 'eci_catalog_stock WHERE fournisseur = "' . pSQL($connecteur) . '"');
    }

    public static function cleanEciStock($connecteur = null, $id_shop = false)
    {
        if (is_null($connecteur)) {
            $connecteur = self::getConnecteurName();
        }

        return Db::getInstance()->delete(
            'eci_catalog_stock',
            'fournisseur = "' . pSQL($connecteur) . '" AND upd_flag = 0' . ($id_shop ? ' AND id_shop = ' . (int)$id_shop : '')
        );
    }

    public static function getPackedProducts($reference, $connecteur = null)
    {
        if (is_null($connecteur)) {
            $connecteur = self::getConnecteurName();
        }

        $products =  Db::getInstance()->executeS(
            'SELECT
                p.pack_reference as pack_pack_reference,
                p.product_reference as pack_product_reference,
                p.reference_attribute as pack_reference_attribute,
                p.quantity as pack_quantity,
                p.fournisseur as pack_fournisseur,
                c.category as product_category,
                c.category_group as product_category_group,
                c.name as product_name,
                c.description as product_description,
                c.short_description as product_short_description,
                c.feature as product_feature,
                c.product_reference as product_product_reference,
                c.reference as product_reference,
                c.ean13 as product_ean13,
                c.upc as product_upc,
                c.ecotax as product_ecotax,
                c.weight as product_weight,
                c.dimensions as product_dimensions,
                c.ship as product_ship,
                c.carriers as product_carriers,
                c.manufacturer as product_manufacturer,
                c.price as product_price,
                c.pmvc as product_pmvc,
                c.pictures as product_pictures,
                c.rate as product_rate,
                c.documents as product_documents,
                c.special as product_special,
                c.keep as product_keep,
                c.fournisseur as product_fournisseur,
                a.product_reference as attribute_product_reference,
                a.reference_attribute as attribute_attribute_reference,
                a.price as attribute_price,
                a.pmvc as attribute_pmvc,
                a.ean13 as attribute_ean13,
                a.weight as attribute_weight,
                a.ecotax as attribute_ecotax,
                a.attribute as attribute_attribute,
                a.upc as attribute_upc,
                a.pictures as attribute_pictures,
                a.special as attribute_special,
                a.fournisseur as attribute_fournisseur
            FROM ' . _DB_PREFIX_ . 'eci_catalog_pack p
            LEFT JOIN ' . _DB_PREFIX_ . 'eci_catalog c
            ON p.product_reference = c.product_reference AND p.fournisseur = c.fournisseur
            LEFT JOIN ' . _DB_PREFIX_ . 'eci_catalog_attribute a
            ON p.product_reference = a.product_reference AND p.reference_attribute = a.reference_attribute AND p.fournisseur = a.fournisseur
            WHERE p.pack_reference = "' . pSQL($reference) . '"
            AND p.fournisseur = "' . pSQL($connecteur) . '"'
        );

        if (!$products) {
            return false;
        }

        $return = array();
        foreach ($products as $i => $product) {
            foreach ($product as $k => $v) {
                $ls = explode('_', $k);
                $l = reset($ls);
                $k2 = preg_replace('/^' . preg_quote($l . '_', '/') . '/', '', $k);
                $return[$i][$l][$k2] = $v;
            }
        }

        return $return;
    }

    public static function getPackComponents($reference, $connecteur = null)
    {
        if (is_null($connecteur)) {
            $connecteur = self::getConnecteurName();
        }

        $products = self::getPackedProducts($reference, $connecteur);

        if (!$products) {
            return false;
        }

        $return = array();
        foreach ($products as $product) {
            $return[] = array(
                'parent_reference' => $product['pack']['product_reference'],
                'reference' => !empty($product['pack']['reference_attribute']) ? $product['pack']['reference_attribute'] : $product['pack']['product_reference'],
                'quantity' => $product['pack']['quantity'],
            );
        }

        return $return;
    }

    public static function getPackComponentsFast($reference, $connecteur = null)
    {
        if (is_null($connecteur)) {
            $connecteur = self::getConnecteurName();
        }

        $products = Db::getInstance()->executeS('select * from '._DB_PREFIX_.'eci_catalog_pack where pack_reference = "'.pSQL($reference).'" and fournisseur = "'.pSQL($connecteur).'"');

        if (!$products) {
            return false;
        }

        $return = array();
        foreach ($products as $product) {
            $return[] = array(
                'parent_reference' => $product['product_reference'],
                'reference' => $product['reference_attribute'] ?: $product['product_reference'],
                'quantity' => $product['quantity'],
            );
        }

        return $return;
    }

    public static function isPack($reference, $connecteur = null)
    {
        if (is_null($connecteur)) {
            $connecteur = self::getConnecteurName();
        }

        return (bool) self::getPackedProducts($reference, $connecteur);
    }

    public static function inPacks($refP, $refA = false, $connecteur = null)
    {
        if (is_null($connecteur)) {
            $connecteur = self::getConnecteurName();
        }

        return Db::getInstance()->executeS(
            'SELECT *
            FROM ' . _DB_PREFIX_ . 'eci_catalog_pack
            WHERE product_reference = "' . pSQL($refP) . '"' .
            ($refA ? ' AND reference_attribute = "' . pSQL($refA) . '"' : '') . '
            AND fournisseur = "' . pSQL($connecteur) . '"'
        );
    }

    public static function isPacked($refP, $refA = false, $connecteur = null)
    {
        if (is_null($connecteur)) {
            $connecteur = self::getConnecteurName();
        }

        return (bool) self::inPacks($refP, $refA, $connecteur);
    }

    public static function clearPacks($connecteur = null)
    {
        if (is_null($connecteur)) {
            $connecteur = self::getConnecteurName();
        }

        return Db::getInstance()->delete(
            'eci_catalog_pack',
            'fournisseur = "' . pSQL($connecteur) . '"'
        );
    }

    public static function cleanPacks($connecteur = null)
    {
        if (is_null($connecteur)) {
            $connecteur = self::getConnecteurName();
        }

        $return = Db::getInstance()->delete(
            'eci_catalog_pack',
            'fournisseur = "' . pSQL($connecteur) . '" AND upd_flag = 0'
        );
        
        $return &= Db::getInstance()->execute(
            'DELETE c0
            FROM '._DB_PREFIX_.'eci_catalog c0
            LEFT JOIN (
                SELECT DISTINCT p.pack_reference
                FROM '._DB_PREFIX_.'eci_catalog_pack p
                LEFT JOIN '._DB_PREFIX_.'eci_catalog c
                ON p.product_reference = c.product_reference AND p.fournisseur = c.fournisseur
                LEFT JOIN '._DB_PREFIX_.'eci_catalog_attribute a
                ON p.product_reference = a.product_reference AND p.reference_attribute = a.reference_attribute AND p.fournisseur = a.fournisseur
                WHERE p.fournisseur = "' . pSQL($connecteur) . '"
                AND c.product_reference IS NULL
                OR (p.reference_attribute != "" AND a.reference_attribute IS NULL)
            ) n
            ON c0.product_reference = n.pack_reference
            WHERE c0.fournisseur = "' . pSQL($connecteur) . '"
            AND n.pack_reference IS NOT NULL'
        );

        $return &= Db::getInstance()->execute(
            'DELETE p
            FROM '._DB_PREFIX_.'eci_catalog_pack p
            LEFT JOIN '._DB_PREFIX_.'eci_catalog c
            ON p.product_reference = c.product_reference AND p.fournisseur = c.fournisseur
            WHERE p.fournisseur = "' . pSQL($connecteur) . '"
            AND c.product_reference IS NULL'
        );

        return $return;
    }

    public static function resetPacks($connecteur = null)
    {
        if (is_null($connecteur)) {
            $connecteur = self::getConnecteurName();
        }

        return Db::getInstance()->update(
            'eci_catalog_pack',
            array(
                'upd_flag' => (int)0
            ),
            'fournisseur = "' . pSQL($connecteur) . '"'
        );
    }
    
    public function getCleanProductForInsert($tab_product, $html_fields = null, $fournisseur = null)
    {
        $connecteur = $fournisseur ?? self::getConnecteurName();
        $html_fields = $html_fields ?? array();
        
        $tab_insert = array();
        foreach (array_keys($this->tabInsert) as $key) {
            $tab_insert[$key] = isset($tab_product[$key]) ? pSQL($tab_product[$key], in_array($key, $html_fields)) : '';
        }

        $tab_insert['fournisseur'] = $tab_insert['fournisseur'] ?: pSQL($connecteur);
        
        return $tab_insert;
    }
    
    public static function insertProducts($insert_parents, $logger = null, $replace = false)
    {
        $divider = 10;
        $logger = $logger ?? self::logStart(__FUNCTION__);
        if (!is_array($insert_parents)) {
            self::logInfo($logger, 'No array of products to insert in Db');
            return false;
        }
        if (!$insert_parents) {
            return true;
        }

        try {
            Db::getInstance()->insert('eci_catalog', $insert_parents, false, false, $replace ? Db::REPLACE : Db::INSERT_IGNORE);
        } catch (Exception $e) {
            self::logInfo($logger, 'Error in the insertion of products in Db : '.$e->getMessage());
            $total = count($insert_parents);
            $length = (int) ($total / $divider);
            if (!$length) {
                return false;
            }
            $i = 0;
            while (($slice = array_slice($insert_parents, $i * $length, $length))) {
                $i++;
                if (!self::insertProducts($slice, $logger)) {
                    return false;
                }
            }
        }
        
        return true;
    }

    public function getCleanCombinationForInsert($tab_combination, $fournisseur = null)
    {
        $connecteur = $fournisseur ?? self::getConnecteurName();
        
        $tab_insert = array();
        foreach (array_keys($this->tabInsert_attribute) as $key) {
            $tab_insert[$key] = isset($tab_combination[$key]) ? pSQL($tab_combination[$key]) : '';
        }

        $tab_insert['fournisseur'] = $tab_insert['fournisseur'] ?: pSQL($connecteur);
        
        return $tab_insert;
    }
    
    public static function insertCombinations($insert_combinations, $logger = null, $replace = false)
    {
        $divider = 10;
        $logger = $logger ?? self::logStart(__FUNCTION__);
        if (!is_array($insert_combinations)) {
            self::logInfo($logger, 'No array of combinations to insert in Db');
            return false;
        }
        if (!$insert_combinations) {
            return true;
        }

        try {
            Db::getInstance()->insert('eci_catalog_attribute', $insert_combinations, false, false, $replace ? Db::REPLACE : Db::INSERT_IGNORE);
        } catch (Exception $e) {
            self::logInfo($logger, 'Error in the insertion of combinations in Db : '.$e->getMessage());
            $total = count($insert_combinations);
            $length = (int) ($total / $divider);
            if (!$length) {
                return false;
            }
            $i = 0;
            while (($slice = array_slice($insert_combinations, $i * $length, $length))) {
                $i++;
                if (!self::insertCombinations($slice, $logger)) {
                    return false;
                }
            }
        }
        
        return true;
    }

    public function getCleanPackForInsert($tab_pack, $fournisseur = null)
    {
        $connecteur = $fournisseur ?? self::getConnecteurName();
        $tabInsert_pack = self::getPackFields();
        
        $tab_insert = array();
        foreach (array_keys($tabInsert_pack) as $key) {
            $tab_insert[$key] = isset($tab_pack[$key]) ? pSQL($tab_pack[$key]) : '';
        }

        $tab_insert['fournisseur'] = $tab_insert['fournisseur'] ?: pSQL($connecteur);
        
        return $tab_insert;
    }
    
    public static function insertPacks($insert_packs, $logger = null)
    {
        $divider = 10;
        $logger = $logger ?? self::logStart(__FUNCTION__);
        if (!is_array($insert_packs)) {
            self::logInfo($logger, 'No array of packs to insert in Db');
            return false;
        }
        if (!$insert_packs) {
            return true;
        }

        try {
            Db::getInstance()->insert('eci_catalog_pack', $insert_packs, false, false, Db::REPLACE);
        } catch (Exception $e) {
            self::logInfo($logger, 'Error in the insertion of packs in Db : '.$e->getMessage());
            $total = count($insert_packs);
            $length = (int) ($total / $divider);
            if (!$length) {
                return false;
            }
            $i = 0;
            while (($slice = array_slice($insert_packs, $i * $length, $length))) {
                $i++;
                if (!self::insertPacks($slice, $logger)) {
                    return false;
                }
            }
        }
        
        return true;
    }

    public static function getIncompletePacks($connecteur = null)
    {
        if (is_null($connecteur)) {
            $connecteur = self::getConnecteurName();
        }

        $raw_return = Db::getInstance()->executeS(
            'SELECT c.product_reference
            FROM '._DB_PREFIX_.'eci_catalog c
            LEFT JOIN (
                SELECT DISTINCT p.pack_reference, p.fournisseur
                FROM '._DB_PREFIX_.'eci_catalog_pack p
                LEFT JOIN '._DB_PREFIX_.'eci_catalog pc
                ON p.product_reference = pc.product_reference AND p.fournisseur = pc.fournisseur
                LEFT JOIN '._DB_PREFIX_.'eci_catalog_attribute pa
                ON p.product_reference = pa.product_reference AND p.reference_attribute = pa.reference_attribute AND p.fournisseur = pa.fournisseur
                WHERE pc.product_reference is null
                OR (p.reference_attribute != "" and pa.reference_attribute is null)
            ) d
            ON c.product_reference = d.pack_reference AND c.fournisseur = d.fournisseur
            WHERE c.fournisseur = "' . pSQL($connecteur) . '"
            AND d.pack_reference IS NOT NULL'
        );

        if (!$raw_return) {
            return false;
        }

        return array_map('implode', $raw_return);
    }

    public static function getNbEciPs($connecteur = null, $id_shop = null, $distinct_shops = false)
    {
        return Db::getInstance()->getValue(
            'SELECT COUNT(DISTINCT id_product, reference' . ($distinct_shops ? ', id_shop' : '') . ')
            FROM ' . _DB_PREFIX_ . 'eci_product_shop
            WHERE id_product_attribute = 0'
            . (is_null($connecteur) ? '' : ' AND fournisseur = "' . pSQL($connecteur) . '"')
            . (is_null($id_shop) ? '' : ' AND id_shop = ' . (int) $id_shop)
        );
    }

    public static function getListProdInShop($connecteur = null, $nbc = 0, $nl = 100, $with_shop = false, $id_shop = null)
    {
        $fournisseur = $connecteur ?? self::getConnecteurName();

        return Db::getInstance()->executeS(
            'SELECT DISTINCT id_product, reference' . ($with_shop ? ', id_shop' : '') . '
            FROM ' . _DB_PREFIX_ . 'eci_product_shop
            WHERE fournisseur = "' . pSQL($fournisseur) . '"
            AND id_product_attribute = 0 ' . (is_null($id_shop) ? '' : 'AND id_shop = ' . (int) $id_shop) . '
            ORDER BY ' . ($with_shop ? 'id_shop, ' : '') . 'id_product
            LIMIT ' . (int) $nbc . ', ' . (int) $nl
        );
    }

    public static function getCatInfos($ref, $connecteur = null, $listinfos = null)
    {
        $fournisseur = $connecteur ?? self::getConnecteurName();
        if (!is_null($listinfos) && !is_array($listinfos)) {
            $listinfos = array($listinfos);
        }

        $req = 'SELECT ' . (is_null($listinfos) ? '*' : '`' . implode('`,`', $listinfos) . '`') . '
            FROM ' . _DB_PREFIX_ . 'eci_catalog
            WHERE product_reference LIKE "' . pSQL($ref) .'"
            AND fournisseur LIKE "' . pSQL($fournisseur) .'"';

        return (is_array($listinfos) && 1 == count($listinfos)) ? Db::getInstance()->getValue($req) : Db::getInstance()->getRow($req);
    }

    public static function getCatInfoDiff($ref, $connecteur = null, $listinfos = null)
    {
        $fournisseur = $connecteur ?? self::getConnecteurName();
        if (is_null($listinfos)) {
            return false;
        }
        if (!is_array($listinfos)) {
            $listinfos = array($listinfos);
        }

        $diff_conditions = array();
        foreach ($listinfos as $info) {
            $diff_conditions[] = 'c.`' . pSQL($info) . '` != o.`' . pSQL($info) . '`';
        }

        $req = 'SELECT c.`' . implode('`, c.`', $listinfos) . '`
            FROM ' . _DB_PREFIX_ . 'eci_catalog c
            LEFT JOIN ' . _DB_PREFIX_ . 'eci_catalog_old o
            ON c.product_reference = o.product_reference
            AND c.fournisseur = o.fournisseur
            WHERE c.product_reference LIKE "' . pSQL($ref) .'"
            AND c.fournisseur LIKE "' . pSQL($fournisseur) .'"
            AND (' . implode(' OR ', $diff_conditions) . ')';

        return (1 == count($listinfos)) ? Db::getInstance()->getValue($req) : Db::getInstance()->getRow($req);
    }

    public static function getCatAttInfos($ref, $connecteur = null, $listinfos = null)
    {
        $fournisseur = $connecteur ?? self::getConnecteurName();
        if (!is_null($listinfos) && !is_array($listinfos)) {
            $listinfos = array($listinfos);
        }

        return Db::getInstance()->getRow(
            'SELECT ' . (is_null($listinfos) ? '*' : '`' . implode('`,`', $listinfos) . '`') . '
            FROM ' . _DB_PREFIX_ . 'eci_catalog_attribute
            WHERE reference_attribute LIKE "' . pSQL($ref) .'"
            AND fournisseur LIKE "' . pSQL($fournisseur) .'"'
        );
    }

    public static function getCatAttInfoDiff($ref, $connecteur = null, $listinfos = null)
    {
        $fournisseur = $connecteur ?? self::getConnecteurName();
        if (is_null($listinfos)) {
            return false;
        }
        if (!is_array($listinfos)) {
            $listinfos = array($listinfos);
        }

        $diff_conditions = array();
        foreach ($listinfos as $info) {
            $diff_conditions[] = 'c.`' . pSQL($info) . '` != o.`' . pSQL($info) . '`';
        }

        $req = 'SELECT c.`' . implode('`, c.`', $listinfos) . '`
            FROM ' . _DB_PREFIX_ . 'eci_catalog_attribute c
            LEFT JOIN ' . _DB_PREFIX_ . 'eci_catalog_attribute_old o
            ON c.reference_attribute = o.reference_attribute
            AND c.fournisseur = o.fournisseur
            WHERE c.reference_attribute LIKE "' . pSQL($ref) .'"
            AND c.fournisseur LIKE "' . pSQL($fournisseur) .'"
            AND (' . implode(' OR ', $diff_conditions) . ')';

        return (1 == count($listinfos)) ? Db::getInstance()->getValue($req) : Db::getInstance()->getRow($req);
    }

    public static function getCatAttInfosForProd($ref, $connecteur = null, $listinfos = null)
    {
        $fournisseur = $connecteur ?? self::getConnecteurName();
        if (!is_null($listinfos) && !is_array($listinfos)) {
            $listinfos = array($listinfos);
        }
        if (is_array($listinfos) && !in_array('reference_attribute', $listinfos)) {
            array_unshift($listinfos, 'reference_attribute');
        }

        return Db::getInstance()->executeS(
            'SELECT ' . (is_null($listinfos) ? '*' : '`' . implode('`,`', $listinfos) . '`') . '
            FROM ' . _DB_PREFIX_ . 'eci_catalog_attribute
            WHERE product_reference LIKE "' . pSQL($ref) .'"
            AND fournisseur LIKE "' . pSQL($fournisseur) .'"'
        );
    }

    public static function getCatAttInfoDiffForProd($ref, $connecteur = null, $listinfos = null)
    {
        $fournisseur = $connecteur ?? self::getConnecteurName();
        if (is_null($listinfos)) {
            return false;
        }
        if (!is_array($listinfos)) {
            $listinfos = array($listinfos);
        }

        $diff_conditions = array();
        foreach ($listinfos as $info) {
            $diff_conditions[] = 'c.`' . pSQL($info) . '` != o.`' . pSQL($info) . '`';
        }

        return Db::getInstance()->executeS(
            'SELECT c.reference_attribute, c.`' . implode('`, c.`', $listinfos) . '`
            FROM ' . _DB_PREFIX_ . 'eci_catalog_attribute c
            LEFT JOIN ' . _DB_PREFIX_ . 'eci_catalog_attribute_old o
            ON c.product_reference = o.product_reference
            AND c.reference_attribute = o.reference_attribute
            AND c.fournisseur = o.fournisseur
            WHERE c.product_reference LIKE "' . pSQL($ref) .'"
            AND c.fournisseur LIKE "' . pSQL($fournisseur) .'"
            AND (' . implode(' OR ', $diff_conditions) . ')'
        );
    }

    public static function getClassProperty($className, $property, $decode = false)
    {
        if (!class_exists($className)) {
            return null;
        }
        if (!property_exists($className, $property)) {
            return null;
        }

        $vars = get_class_vars($className);

        return $decode ? self::arJsonDecodeRecur($vars[$property]) : $vars[$property];
    }

    public static function getClassConstant($class, $constant, $decode = false)
    {
        if (!is_object($class) && !class_exists($class)) {
            return null;
        }

        $reflection = new ReflectionClass($class);
        $constants = $reflection->getConstants();

        if (!array_key_exists($constant, $constants)) {
            return null;
        }

        return $decode ? self::arJsonDecodeRecur($constants[$constant]) : $constants[$constant];
    }

    public static function setSpecialDataFromCatAttToCat($key, $mask, $connecteur = null)
    {
        if (is_null($connecteur)) {
            $connecteur = self::getConnecteurName();
        }

        return Db::getInstance()->execute(
            'UPDATE ' . _DB_PREFIX_ . 'eci_catalog c
            LEFT JOIN ' . _DB_PREFIX_ . 'eci_catalog_attribute a
            ON c.product_reference = a.product_reference AND c.fournisseur = a.fournisseur
            SET c.special = REPLACE(c.special, "\"' . pSQL($mask) . '\"", SUBSTRING_INDEX(SUBSTRING_INDEX(SUBSTRING_INDEX(a.special, "'. pSQL($key) . '\":", -1), ",", 1), "}", 1))
            WHERE c.fournisseur = "' . pSQL($connecteur) . '"
            AND a.product_reference IS NOT NULL
            AND a.special LIKE "%' . pSQL($key) . '%"'
        );
    }

    public static function setSpecialDataFromCatToCatAtt($key, $mask, $connecteur = null)
    {
        if (is_null($connecteur)) {
            $connecteur = self::getConnecteurName();
        }

        return Db::getInstance()->execute(
            'UPDATE ' . _DB_PREFIX_ . 'eci_catalog_attribute a
            LEFT JOIN ' . _DB_PREFIX_ . 'eci_catalog c
            ON c.product_reference = a.product_reference AND c.fournisseur = a.fournisseur
            SET a.special = REPLACE(a.special, "\"' . pSQL($mask) . '\"", SUBSTRING_INDEX(SUBSTRING_INDEX(SUBSTRING_INDEX(c.special, "'. pSQL($key) . '\":", -1), ",", 1), "}", 1))
            WHERE a.fournisseur = "' . pSQL($connecteur) . '"
            AND c.product_reference IS NOT NULL
            AND c.special LIKE "%' . pSQL($key) . '%"'
        );
    }

    public static function getPhones($list)
    {
        if (!is_array($list)) {
            $list = array($list);
        }

        $resp = array(
            'phone' => null,
            'mobile' => null
        );

        foreach ($list as $number) {
            // clean
            $number = str_replace(array(' ', '.', '-'), '', $number);
            // format with 10 or +11 digits for French phone numbers, add other REGEXP if necessary
            if (!preg_match('/^\+?[0-9]{10,11}$/', $number)) {
                continue;
            }
            // separate mobile numbers (French case)
            if (is_null($resp['phone']) && in_array(Tools::substr($number, -9, 1), array(1, 2, 3, 4, 5, 8, 9))) {
                $resp['phone'] = $number;
            } elseif (is_null($resp['mobile']) && in_array(Tools::substr($number, -9, 1), array(6, 7))) {
                $resp['mobile'] = $number;
            }
        }

        return $resp;
    }
    
    public static function logException(Info $logger, Exception $e, $text = '', $severity = Info::INFO, $getTrace = false)
    {
        switch ($severity) {
            case Info::DEBUG:
                $function = 'logDebug';
                break;
            case Info::WARNING:
                $function = 'logWarning';
                break;
            case Info::ERROR:
                $function = 'logError';
                break;
            case Info::INFO:
            default:
                $function = 'logInfo';
        }
        
        if (!$text) {
            $debug_backtrace = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS, 2);
            $text = 'Error in ' . $debug_backtrace[1]['function'];
        }

        $message = $text . ' on line ' . $e->getLine() . ' of file ' . $e->getFile() . ' : ' . $e->getMessage();
        
        self::$function($logger, $message);
        
        if ($getTrace) {
            self::$function($logger, $e->getTraceAsString());
        }
        
        return;
    }
    
    public static function getRemiseGlobale($remises)
    {
        if (!is_array($remises)) {
            $remises = [$remises];
        }
        if (count($remises) != count(array_filter($remises, 'is_numeric'))) {
            return false;
        }

        $global = 1;
        foreach ($remises as $remise) {
            $global *= (1 - $remise / 100);
        }

        return self::getRemisePercentFromCoef($global);
    }
    
    public static function getRemisePercentFromCoef($coef)
    {
        return 100 * (1 - $coef);
    }
    
    public static function getAdminFolder()
    {
        $dirs = glob(_PS_ROOT_DIR_.'/*', GLOB_ONLYDIR);
        $adminFolder = false;

        foreach ($dirs as $dir) {
            $files = glob($dir.'/*.php');
            foreach ($files as $file) {
                if ('ajax_products_list' === pathinfo($file, PATHINFO_FILENAME)) {
                    $adminFolder = pathinfo($dir, PATHINFO_FILENAME);
                    break 2;
                }
            }
        }

        return $adminFolder;
    }

    public static function cleanMessage($string)
    {
        return preg_replace(['/;/', '/\v/', '/\t/', '/[ ]+/'], ' ', self::htmlNumericEntitiesDecode($string));
    }

    public static function htmlNumericEntitiesDecode($string)
    {
        while (preg_match('/&[a-z]+;/', $string)) {
            $string = html_entity_decode($string, ENT_QUOTES, 'UTF-8');
        }
        while (preg_match('/&#[0-9]+;/', $string)) {
            $string = self::htmlNumericEntityDecode($string);
        }
        return $string;
    }

    public static function htmlNumericEntityDecode($string)
    {
        return mb_convert_encoding($string, 'UTF-8', 'HTML-ENTITIES');
    }
    
    public static function getStoreAddress($id_store)
    {
        $id_lang = Context::getContext()->language->id ?? Configuration::get('PS_LANG_DEFAULT');
        
        if (!Validate::isLoadedObject($store = new Store((int) $id_store, (int) $id_lang))) {
            return false;
        }
        
        $address = new Address();
        $address->firstname = '';
        $address->lastname = $store->name;
        $address->address1 = $store->address1;
        $address->address2 = $store->address2;
        $address->company = $store->name;
        $address->city = $store->city;
        $address->postcode = $store->postcode;
        $address->id_country = $store->id_country;
        $address->country = Country::getIsoById($store->id_country);
        $address->id_state = $store->id_state;
        $address->phone = $store->phone;
        $address->phone_mobile = '';
        
        return $address;
    }
}
