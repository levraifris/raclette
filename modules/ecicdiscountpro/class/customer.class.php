<?php
/**
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to a commercial license from SARL Ether Creation
 * Use, copy, modification or distribution of this source file without written
 * license agreement from the SARL Ether Creation is strictly forbidden.
 * In order to obtain a license, please contact us: contact@ethercreation.com
 * ...........................................................................
 * INFORMATION SUR LA LICENCE D'UTILISATION
 *
 * L'utilisation de ce fichier source est soumise a une licence commerciale
 * concedee par la societe Ether Creation
 * Toute utilisation, reproduction, modification ou distribution du present
 * fichier source sans contrat de licence ecrit de la part de la SARL Ether Creation est
 * expressement interdite.
 * Pour obtenir une licence, veuillez contacter la SARL Ether Creation a l'adresse: contact@ethercreation.com
 * ...........................................................................
 *
 * @author    Ether Creation SARL <contact@ethercreation.com>
 * @copyright 2008-2021 Ether Creation SARL
 * @license   Commercial license
 * International Registered Trademark & Property of Ether Creation SARL
 */

require_once dirname(__FILE__) . '/catalog.class.php';
use ecicdiscountpro\Catalog;

if (!defined('_PS_VERSION_')) {
    exit();
}

class EcCustomer
{
    const ADDRESSDEFAULTS = [
        'alias' => 'Adresse',
        'firstname' => '-',
        'lastname' => '-',
    ];

    const CUSTOMERDEFAULTS = [
        'firstname' => '-',
        'lastname' => '-',
        'password' => '4faa8698fd3f8c2323e83194d557a32b',
    ];

    const GROUPDEFAULTS = [
        'price_display_method' => Group::PRICE_DISPLAY_METHOD_TAX_INCL,
    ];

    public static function existsGroup($tabGroup)
    {
        if (empty($tabGroup['name'])) {
            return false;
        }
        $id_lang = Configuration::get('PS_LANG_DEFAULT');
        $iso_lang = Language::getIsoById($id_lang);
        if (is_array($tabGroup['name'])) {
            $group_name = $tabGroup['name'][$iso_lang] ?? $tabGroup['name'][$id_lang] ?? reset($tabGroup['name']);
        } else {
            $group_name = $tabGroup['name'];
        }
        
        if (!$group_name) {
            return false;
        }
        
        $id_group = Db::getInstance()->getValue(
            'SELECT id_group
            FROM ' . _DB_PREFIX_ . 'group_lang
            WHERE name = "' . pSQL($group_name) . '"
            AND id_lang = ' . (int) $id_lang
        );

        return $id_group;
    }

    public static function actionGroup($tabGroup, $force = false, $defaults = self::GROUPDEFAULTS)
    {
        $logger = Catalog::logStart('groups_errors');
        $defs = Group::$definition;
        $err_msg = array();

        $id_group = self::existsGroup($tabGroup);
        if (!$id_group) {
            $group = new Group();
        } elseif ($id_group && $force) {
            $group = new Group($id_group);
        } else {
            return $id_group;
        }
        
        $tabGroup['name'] = Catalog::getMultiLangField($tabGroup['name']);
        
        $groupDefaults = is_array($defaults) ? $defaults : self::GROUPDEFAULTS;

        foreach ($tabGroup as $prop => $val) {
            if (!property_exists('Group', $prop) || !isset($defs['fields'][$prop])) {
                continue;
            }
            if (!isset($group->$prop) && '' === $val && isset($groupDefaults[$prop])) {
                $val = $groupDefaults[$prop];
            }
            if ('' !== $val && isset($defs['fields'][$prop]['validate'])) {
                $valfunc = $defs['fields'][$prop]['validate'];
                if (!empty($defs['fields'][$prop]['lang']) && is_array($val)) {
                    $size = $defs['fields'][$prop]['size'] ?? 32;
                    foreach ($val as $id => $v) {
                        if (!Validate::$valfunc($v)) {
                            unset($val[$id]);
                        }
                    }
                } else {
                    if (!Validate::$valfunc($val)) {
                        $err_msg[] = 'Invalid value for property : ' . $prop . ' => "' . $val . '"';
                        continue;
                    }
                }
            }
            $group->$prop = $val;
        }
        foreach ($defs['fields'] as $field => $conf) {
            if (isset($conf['required']) && $conf['required'] && !isset($group->$field)) {
                $err_msg[] = 'Required property missing : ' . $field;
            }
        }
        if ($err_msg) {
            $err_msg[] = 'The group will not be ' . ($id_group ? 'updated' : 'created');
        } else {
            try {
                if ($id_group) {
                    $group->update();
                } else {
                    $group->add();
                }
            } catch (Exception $e) {
                $err_msg[] = 'The group will not be ' . ($id_group ? 'updated' : 'created') . ' : ' . $e->getMessage();
            }
        }
        if ($err_msg) {
            $err_msg[] = var_export($tabGroup, true);
            Catalog::logError($logger, implode("\n<br>", $err_msg));
            return false;
        }

        return $group->id;
    }

    public static function actionGroupShop($idG, $idS)
    {
        if (!is_array($idS)) {
            $idS = array($idS);
        }
        
        $insert = array();
        foreach ($idS as $id_shop) {
            $insert[] = array(
                'id_group' => (int) $idG,
                'id_shop' => (int) $id_shop
            );
        }
        
        return Db::getInstance()->insert(
            'group_shop',
            $insert,
            false,
            false,
            Db::INSERT_IGNORE
        );
    }

    public static function modGroupShop($idG)
    {
        $lstMod = Db::getInstance()->executeS(
            'SELECT *
            FROM ' . _DB_PREFIX_ . 'module_group
            WHERE id_group = 1'
        );
        
        $insert = array();
        foreach ($lstMod as $mod) {
            $insert[] = array(
                'id_group' => (int) $idG,
                'id_shop' => (int) $mod['id_shop'],
                'id_module' => (int) $mod['id_module']
            );
        }
        
        Db::getInstance()->insert(
            'module_group',
            $insert,
            false,
            false,
            Db::INSERT_IGNORE
        );
    }

    public static function actionCustomerGroup($idGroups, $id_customer, $del = false)
    {
        if ($del) {
            Db::getInstance()->delete('customer_group', 'id_customer = ' . (int) $id_customer);
        }
        if (!is_array($idGroups)) {
            $idGroups = array($idGroups);
        }
        
        $insert = array();
        foreach ($idGroups as $id_group) {
            $insert[] = array(
                'id_customer' => (int) $id_customer,
                'id_group' => (int) $id_group
            );
        }

        Db::getInstance()->insert(
            'customer_group',
            $insert,
            false,
            false,
            Db::INSERT_IGNORE
        );
    }

    public static function actionReducCategGroup($id_group, $id_categ, $reduction)
    {
        $inf = Db::getInstance()->getRow(
            'SELECT id_group_reduction, reduction
            FROM ' . _DB_PREFIX . 'group_reduction
            WHERE id_category = ' . (int) $id_categ . '
            AND id_group = ' . (int) $id_group
        );

        if ($inf['id_group_reduction'] > 0 && $inf['reduction'] != $reduction) {
            Db::getInstance()->update('group_reduction', array('reduction' => pSQL($reduction)), 'id_group_reduction = ' . (int) $inf['id_group_reduction']);
        } elseif ($inf['id_group_reduction'] == 0) {
            Db::getInstance()->insert('group_reduction', array('id_group' => (int) $id_group, 'id_category' => (int) $id_categ, 'reduction' => pSQL($reduction)));
        }
        if ($reduction == 0) {
            Db::getInstance()->delete('group_reduction', 'id_group = ' . (int) $id_group . ' AND id_category = ' . (int) $id_categ);
        }
    }
    
    public function getAddressDefaults()
    {
        return $this->addressDefaults;
    }

    public function setAddressDefaults($addressDefaults)
    {
        $this->addressDefaults = $addressDefaults;
    }

    public static function actionAdress($tabAddress, $comp = array(), $force = false, $defaults = self::ADDRESSDEFAULTS)
    {
        $logger = Catalog::logStart('address_errors');
        $defs = Address::$definition;
        $err_msg = array();
        $multi = isset($tabAddress['multi']) ? (bool) $tabAddress['multi'] : false;
        $unicity_key = '';
        if ($multi) {
            $k = '';
            foreach ($comp as $field) {
                $k .= isset($tabAddress[$field]) ? $tabAddress[$field] : '';
            }
            $unicity_key = md5($k);
        }

        //Customer
        $id_address = self::existsAddress($tabAddress, $comp);

        if ($id_address) {
            if (!$force && self::sameAddressHash($tabAddress['id_customer'], $unicity_key, self::getHash($tabAddress))) {
                return $id_address;
            }
            $address = new Address($id_address);
        } else {
            $address = new Address();
        }
        
        //defaults
        $addressDefaults = is_array($defaults) ? $defaults : self::ADDRESSDEFAULTS;

        foreach ($tabAddress as $prop => $val) {
            if (!property_exists('Address', $prop) || !isset($defs['fields'][$prop])) {
                continue;
            }
            if (!isset($address->$prop) && '' === $val && isset($addressDefaults[$prop])) {
                $val = $addressDefaults[$prop];
            }
            if ('' !== $val && isset($defs['fields'][$prop]['validate'])) {
                $valfunc = $defs['fields'][$prop]['validate'];
                if (!Validate::$valfunc($val)) {
                    $err_msg[] = 'Invalid value for property : ' . $prop . ' => "' . $val . '"';
                    continue;
                }
            }
            $address->$prop = $val;
        }
        foreach ($defs['fields'] as $field => $conf) {
            if (isset($conf['required']) && $conf['required'] && !isset($address->$field)) {
                $err_msg[] = 'Required property missing : ' . $field;
            }
        }
        if ($err_msg) {
            $err_msg[] = 'The address will not be ' . ($id_address ? 'updated' : 'created');
        } else {
            try {
                if ($id_address) {
                    $address->update();
                } else {
                    $address->add();
                }
            } catch (Exception $e) {
                $err_msg[] = 'The address will not be ' . ($id_address ? 'updated' : 'created') . ' : ' . $e->getMessage();
            }
        }
        if ($err_msg) {
            $err_msg[] = var_export($tabAddress, true);
            Catalog::logError($logger, implode("\n<br>", $err_msg));
            return false;
        }

        self::setAddressHash($address->id, $unicity_key, self::getHash($tabAddress));

        return $address->id;
    }

    public static function existsAddress($tabAddress, $comp = array())
    {
        $multi = isset($tabAddress['multi']) ? (bool) $tabAddress['multi'] : false;

        $compReq = '';
        if ($multi) {
            foreach ($comp as $inf) {
                $compReq .= isset($tabAddress[$inf]) ? ' AND ' . $inf . ' = "' . pSQL($tabAddress[$inf]) . '"' : '';
            }
        }

        $id_adressComp = (int) Db::getInstance()->getValue(
            'SELECT `id_address`
            FROM `' . _DB_PREFIX_ . 'address`
            WHERE `id_customer` = ' . (int) $tabAddress['id_customer'] . ($multi && $compReq ? $compReq : '')
        );

        return $id_adressComp;
    }

    public static function actionCustomer($tabCustomer, $force = false, $defaults = self::CUSTOMERDEFAULTS)
    {
        $logger = Catalog::logStart('customer_errors');
        $defs = Customer::$definition;
        $err_msg = array();

        //Customer
        $id_cus = self::existsCustomer($tabCustomer);

        if ($id_cus) {
            if (!$force && self::sameCustomerHash($id_cus, self::getHash($tabCustomer))) {
                return $id_cus;
            }
            $customer = new Customer($id_cus);
        } else {
            $customer = new Customer();
        }
        
        $customerDefaults = is_array($defaults) ? $defaults : self::CUSTOMERDEFAULTS;

        foreach ($tabCustomer as $prop => $val) {
            if (!property_exists('Customer', $prop) || !isset($defs['fields'][$prop])) {
                continue;
            }
            if (!isset($customer->$prop) && '' === $val && isset($customerDefaults[$prop])) {
                $val = $customerDefaults[$prop];
            }
            if ('' !== $val && isset($defs['fields'][$prop]['validate'])) {
                $valfunc = $defs['fields'][$prop]['validate'];
                if (!Validate::$valfunc($val)) {
                    $err_msg[] = 'Invalid value for property : ' . $prop . ' => "' . $val . '"';
                    continue;
                }
            }
            if ('passwd' == $prop && version_compare(_PS_VERSION_, '1.7.0', '>=')) {
                $customer->$prop = Tools::hash($val); // only from 1.7.0
            } else {
                $customer->$prop = $val; // from 1.7.0 passwords wont be encrypted
            }
        }
        foreach ($defs['fields'] as $field => $conf) {
            if (isset($conf['required']) && $conf['required'] && !isset($customer->$field)) {
                $err_msg[] = 'Required property missing : ' . $field;
            }
        }
        if ($err_msg) {
            $err_msg[] = 'The customer will not be ' . ($id_cus ? 'updated' : 'created');
        } else {
            try {
                if ($id_cus) {
                    $customer->update();
                } else {
                    $customer->add();
                }
            } catch (Exception $e) {
                $err_msg[] = 'The customer will not be ' . ($id_cus ? 'updated' : 'created') . ' : ' . $e->getMessage();
            }
        }
        if ($err_msg) {
            $err_msg[] = var_export($tabCustomer, true);
            Catalog::logError($logger, implode("\n<br>", $err_msg));
            return false;
        }

        self::setCustomerHash($customer->id, self::getHash($tabCustomer));

        return $customer->id;
    }

    public static function existsCustomer($tabCustomer)
    {
        $id_cus = false;
        if (empty($tabCustomer['pivot_field']) || 'email' === $tabCustomer['pivot_field']) {
            $customers = Customer::getCustomersByEmail($tabCustomer['email']);
            if ($customers) {
                //keep customer that are not guests
                foreach ($customers as $i => $customer) {
                    if ($customer['is_guest']) {
                        unset($customers[$i]);
                    }
                }
            }
        } else {
            if ('id_customer' === $tabCustomer['pivot_field'] && !empty($tabCustomer['id_customer'])) {
                $customers = Db::getInstance()->executeS(
                    'SELECT *
                    FROM ' . _DB_PREFIX_ . 'customer
                    WHERE `id_customer` = ' . (int) $tabCustomer['id_customer']
                );
            } elseif (property_exists('Customer', $tabCustomer['pivot_field']) && array_key_exists($tabCustomer['pivot_field'], $tabCustomer)) {
                $customers = Db::getInstance()->executeS(
                    'SELECT *
                    FROM ' . _DB_PREFIX_ . 'customer
                    WHERE is_guest = 0
                    AND `' . $tabCustomer['pivot_field'] . '` = "' . pSQL($tabCustomer[$tabCustomer['pivot_field']]) . '"'
                );
            } else {
                $customers = array();
            }
        }
        if ((count($customers) >= 1) &&
            isset($tabCustomer['second_field']) &&
            isset($tabCustomer[$tabCustomer['second_field']])) {
            //one or more email we can distinct : choose the one with good 'second_field' value or the first with empty 'second_value'
            $first_empty = false;
            foreach ($customers as $cust) {
                if (!$first_empty && !$cust[$tabCustomer['second_field']]) {
                    $first_empty = $cust['id_customer'];
                }
                if ($cust[$tabCustomer['second_field']] == $tabCustomer[$tabCustomer['second_field']]) {
                    $id_cus = $cust['id_customer'];
                    break;
                }
            }
            $id_cus = $id_cus ? $id_cus : $first_empty;
        } elseif (count($customers) >= 1) {
            //one or more email we can't distinct
            $id_cus = $customers[0]['id_customer'];
        }

        return $id_cus;
    }

    public static function getHash($tab)
    {
        return md5(serialize($tab));
    }

    public static function setCustomerHash($id_customer, $hash)
    {
        Db::getInstance()->insert(
            'eci_customer_hash',
            array(
                'id_customer' => (int) $id_customer,
                'customer_hash' => pSQL($hash)
            ),
            false,
            false,
            Db::ON_DUPLICATE_KEY
        );
    }

    public static function sameCustomerHash($id_customer, $hash)
    {
        return ($hash == Db::getInstance()->getValue(
            'SELECT customer_hash
            FROM ' . _DB_PREFIX_ . 'eci_customer_hash
            WHERE id_customer = ' . (int) $id_customer
        )
        );
    }

    public static function setAddressHash($id_customer, $alias, $hash)
    {
        Db::getInstance()->insert(
            'eci_address_hash',
            array(
                'id_customer' => (int) $id_customer,
                'alias' => pSQL($alias),
                'address_hash' => pSQL($hash)
            ),
            false,
            false,
            Db::ON_DUPLICATE_KEY
        );
    }

    public static function sameAddressHash($id_customer, $alias, $hash)
    {
        return ($hash == Db::getInstance()->getValue(
            'SELECT address_hash
            FROM ' . _DB_PREFIX_ . 'eci_address_hash
            WHERE id_customer = ' . (int) $id_customer .
            ($alias ? ' AND alias = "' . pSQL($alias) . '"' : '')
        )
        );
    }
}
