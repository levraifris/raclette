<?php
/**
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to a commercial license from SARL Ether Creation
 * Use, copy, modification or distribution of this source file without written
 * license agreement from the SARL Ether Creation is strictly forbidden.
 * In order to obtain a license, please contact us: contact@ethercreation.com
 * ...........................................................................
 * INFORMATION SUR LA LICENCE D'UTILISATION
 *
 * L'utilisation de ce fichier source est soumise a une licence commerciale
 * concedee par la societe Ether Creation
 * Toute utilisation, reproduction, modification ou distribution du present
 * fichier source sans contrat de licence ecrit de la part de la SARL Ether Creation est
 * expressement interdite.
 * Pour obtenir une licence, veuillez contacter la SARL Ether Creation a l'adresse: contact@ethercreation.com
 * ...........................................................................
 *
 * @author    Ether Creation SARL <contact@ethercreation.com>
 * @copyright 2008-2021 Ether Creation SARL
 * @license   Commercial license
 * International Registered Trademark & Property of Ether Creation SARL
 */

namespace ecicdiscountpro;

require_once dirname(__FILE__) . '/catalog.class.php';
require_once dirname(__FILE__) . '/info.class.php';
require_once dirname(__FILE__) . '/apiauth.php';
require_once dirname(__FILE__) . '/apimethodes.php';
require_once dirname(__FILE__) . '/../gen/cdiscountpro/class/eccdiscountpro.php';

use Tools;
use ReflectionClass;
use ReflectionMethod;

class ApiTools
{
    private $rawPost = null;
    private $input = null;
    public $user = null;
    public $authtoken = null;
    public $query = null;
    public $action = null;
    public $parameters = null;
    public $inputValidated = false;
    public $accessValidated = false;
    public $authorized = false;
    private $authorizations = [];
    private $apiError = null;
    private $http_code = null;
    private $response = [];
    public $log = true;

    public function __construct()
    {
        return $this->validateInput();
    }
    
    public function __getPost()
    {
        $this->rawPost = Tools::file_get_contents("php://input");
        
        return $this;
    }
    
    private function validateInput()
    {
        $this->inputValidated = false;
        if (!$this->rawPost) {
            $this->__getPost();
        }
        
        if (is_null($this->input = json_decode($this->rawPost, true)) || !is_array($this->input)) {
            $this->setError('invalid input', 400);
        } elseif (is_null($this->user = $this->input['user'] ?? null) || !is_string($this->user) || is_null($this->authtoken = $this->input['authtoken'] ?? null) || !is_string($this->authtoken)) {
            $this->setError('no credentials', 401);
        } elseif (!$this->validateAccess()->accessValidated) {
            $this->setError('invalid credentials', 401);
        } elseif (is_null($this->query = $this->input['query'] ?? null)) {
            $this->setError('no request', 400);
        } elseif (!is_array($this->query) || is_null($this->action = $this->query['action'] ?? null) || !is_string($this->action) || is_null($this->parameters = $this->query['parameters'] ?? null) || !is_array($this->parameters)) {
            $this->setError('invalid request', 418);
        } elseif (!$this->validateAuthorization()->authorized) {
            $this->setError('request not authorized', 405);
        } else {
            $this->inputValidated = true;
        }
        
        $this->log();
        
        return $this;
    }
    
    private function validateAccess()
    {
        $this->accessValidated = false;
        if (ApiAuth::getAccess($this->user, $this->authtoken)) {
            $this->accessValidated = true;
        }
        
        return $this;
    }
    
    private function validateAuthorization()
    {
        $this->authorized = false;
        $this->authorizations = [];
        if (!$this->accessValidated) {
            return $this;
        }
        
        if (in_array($this->action, $this->getAuthorizations()->authorizations)) {
            $this->authorized = true;
        }
        
        return $this;
    }
    
    public function setResponse($param)
    {
        $this->response = $param;
        
        return $this;
    }

    public function setError($text, $http_code = null)
    {
        $this->apiError = $text;
        $this->http_code = $http_code;
        
        return $this;
    }

    public function sendResponse()
    {
        header('Content-Type: application/json', true);
        http_response_code($this->http_code ?? 200);
        $return = [
            'error' => 0,
            'message' => 'OK',
            'response' => $this->response
        ];
        if (!is_null($this->apiError)) {
            $return['error'] = 1;
            $return['message'] = $this->apiError;
            $return['response'] = [];
        }
        
        echo json_encode($return);
        
        exit();
    }
    
    private function log()
    {
        $logger = new Info(str_replace(['\\', __NAMESPACE__], '', get_class()));
        $logger->onlyDB = true;
        $logger->logInfo(
            json_encode($this->user) . ' is accessing API with token ' . json_encode($this->authtoken)
            . ' trying to do ' . json_encode($this->action) . ' with parameters ' . json_encode($this->parameters)
            . ', this access is ' . ($this->accessValidated?'':'not ') . 'validated'
            . ', this action is ' . ($this->authorized?'':'not ') . 'authorized.'
        );
    }
    
    private function getAuthorizations()
    {
        $this->authorizations = [];
        if ($this->accessValidated) {
            $this->authorizations = ApiAuth::getAuthorizations($this->user, $this->authtoken) ?? [];
        }
        
        return $this;
    }
    
    public static function getApiMethodes()
    {
        $classes = Catalog::getClassConstant('Ec'.Catalog::getConnecteurName(), 'API_CLASSES', true);
        $methodes = [];

        if ($classes && is_array($classes)) {
            foreach ($classes as $class) {
                if (!class_exists($class)) {
                    continue;
                }
                $class_no_ns = str_replace('\\' . __NAMESPACE__ . '\\', '', $class);
                $reflexion = new ReflectionClass($class);
                foreach (array_intersect($reflexion->getMethods(ReflectionMethod::IS_PUBLIC), $reflexion->getMethods(ReflectionMethod::IS_STATIC)) as $methode) {
                    $parameters = [];
                    foreach ($methode->getParameters() as $param) {
                        $parameters[] = $param->getName() . ($param->isOptional() ? '' : '*');
                    }
                    $methodes[$class_no_ns . '::' . $methode->name] = ['name' => $class_no_ns . '::' . $methode->name, 'parameters' => json_encode($parameters)];
                }
            }
            sort($methodes);
        }
        
        return $methodes;
    }
}
