<?php
/**
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to a commercial license from SARL Ether Creation
 * Use, copy, modification or distribution of this source file without written
 * license agreement from the SARL Ether Creation is strictly forbidden.
 * In order to obtain a license, please contact us: contact@ethercreation.com
 * ...........................................................................
 * INFORMATION SUR LA LICENCE D'UTILISATION
 *
 * L'utilisation de ce fichier source est soumise a une licence commerciale
 * concedee par la societe Ether Creation
 * Toute utilisation, reproduction, modification ou distribution du present
 * fichier source sans contrat de licence ecrit de la part de la SARL Ether Creation est
 * expressement interdite.
 * Pour obtenir une licence, veuillez contacter la SARL Ether Creation a l'adresse: contact@ethercreation.com
 * ...........................................................................
 *
 * @author    Ether Creation SARL <contact@ethercreation.com>
 * @copyright 2008-2021 Ether Creation SARL
 * @license   Commercial license
 * International Registered Trademark & Property of Ether Creation SARL
 */

namespace ecicdiscountpro;

require_once dirname(__FILE__) . '/ecicustomobjectmodel.php';
use Db;

class Pricerules extends EciCustomObjectModel
{
    public $tabName = 'Pricerules';
    public $controllerExt = 'Pricerules';
    public static $definition = array(
        'table' => 'eci_pricerules',
        'primary' => 'id_pricerules',
        'fields' => array(
            'name' => array('type' => self::TYPE_STRING, 'db_type' => 'varchar(64)', 'validate' => 'isString', 'size' => 64, 'required' => true),
            'fournisseur' => array('type' => self::TYPE_STRING, 'db_type' => 'varchar(50)', 'validate' => 'isGenericName', 'size' => 50, 'required' => true),
            'active' => array('type' => self::TYPE_BOOL, 'db_type' => 'int', 'validate' => 'isBool', 'required' => true),
            'papv' => array('type' => self::TYPE_BOOL, 'db_type' => 'int', 'validate' => 'isBool', 'required' => true),
            'date_from' => array('type' => self::TYPE_DATE, 'db_type' => 'datetime', 'validate' => 'isDateFormat', 'required' => false),
            'date_to' => array('type' => self::TYPE_DATE, 'db_type' => 'datetime', 'validate' => 'isDateFormat', 'required' => false),
            'price_min' => array('type' => self::TYPE_FLOAT, 'db_type' => 'decimal(20,6)', 'validate' => 'isFloat'),
            'price_max' => array('type' => self::TYPE_FLOAT, 'db_type' => 'decimal(20,6)', 'validate' => 'isFloat'),
            'rate' => array('type' => self::TYPE_FLOAT, 'db_type' => 'decimal(20,6)', 'validate' => 'isFloat'),
            'overhead' => array('type' => self::TYPE_FLOAT, 'db_type' => 'decimal(20,6)', 'validate' => 'isFloat'),
            'category' => array('type' => self::TYPE_STRING, 'db_type' => 'text', 'validate' => 'isString'),
            'manufacturer' => array('type' => self::TYPE_STRING, 'db_type' => 'varchar(50)', 'validate' => 'isGenericName', 'size' => 50),
            'position' => array('type' => self::TYPE_INT, 'db_type' => 'int', 'validate' => 'isUnsignedId', 'required' => true),
            'promo' => array('type' => self::TYPE_BOOL, 'db_type' => 'int', 'validate' => 'isBool', 'required' => true),
            'id_shop' => array('type' => self::TYPE_INT, 'db_type' => 'int', 'validate' => 'isUnsignedId', 'required' => true),
        )
    );
    public $name;
    public $fournisseur;
    public $active;
    public $papv;
    public $date_from;
    public $date_to;
    public $price_max;
    public $price_min;
    public $rate;
    public $overhead;
    public $category;
    public $manufacturer;
    public $position;
    public $promo;
    public $id_shop;

    public function install()
    {
        Db::getInstance()->execute(
            'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'eci_pricerules_match` (
            `product_reference` varchar(50) NOT NULL,
            `fournisseur` varchar(50) NOT NULL,
            `id_pricerules` int(11) NOT NULL,
            `flag` int(11) NOT NULL,
            UNIQUE KEY `product_reference_fournisseur_id_pricerules` (`product_reference`, `fournisseur`, `id_pricerules`),
            INDEX `product_reference` (`product_reference`),
            INDEX `fournisseur` (`fournisseur`),
            INDEX `id_pricerules` (`id_pricerules`),
            INDEX `flag` (`flag`)
            ) ENGINE = InnoDB DEFAULT CHARSET=utf8'
        );

        $reqs = array(
            _DB_PREFIX_ . 'eci_pricerules' => array(
                'fournisseur' => '`fournisseur`',
                'active' => '`active`',
                'date_from' => '`date_from`',
                'date_to' => '`date_to`',
                'price_min' => '`price_min`',
                'price_max' => '`price_max`',
                'category' => '`category`(255)',
                'manufacturer' => '`manufacturer`',
                'position' => '`position`',
                'id_shop' => '`id_shop`',
            ),
        );
        $this->addIndexesIfNotExist($reqs);
    }

    public function uninstall()
    {
        Db::getInstance()->execute('DROP TABLE IF EXISTS `' . _DB_PREFIX_ . 'eci_pricerules_match`');
    }

    public function add($auto_date = true, $null_values = false)
    {
        $ret = parent::add($auto_date, $null_values);

        self::matchingRules($this->fournisseur, $this->id_shop);

        return $ret;
    }

    public function delete()
    {
        $ret = parent::delete();

        self::matchingRules($this->fournisseur, $this->id_shop);

        return $ret;
    }

    public static function calculatePriceRules($ref, $fourn, $id_shop, $paht, $pvht)
    {
        $rule = Db::getInstance()->getRow(
            'SELECT papv, rate, overhead
            FROM '._DB_PREFIX_.'eci_pricerules pr
            LEFT JOIN '._DB_PREFIX_.'eci_pricerules_match prm ON pr.id_pricerules = prm.id_pricerules
            WHERE prm.product_reference = "'.pSQL($ref).'"
            AND prm.fournisseur = "'.pSQL($fourn).'"
            AND pr.id_shop = '.(int) $id_shop.'
            AND pr.active = 1
            AND ((pr.date_from = "0000-00-00") OR (pr.date_from <= NOW()))
            AND ((pr.date_to = "0000-00-00") OR (pr.date_to >= NOW()))
            AND ((price_min = "") OR (price_min <= IF(papv = 1, '.(float) $paht.', '.(float) $pvht.')))
            AND ((price_max = "") OR (price_max >= IF(papv = 1, '.(float) $paht.', '.(float) $pvht.')))
            ORDER BY pr.position ASC'
        );

        if (empty($rule)) {
            return $pvht;
        } elseif ((1 == $rule['papv']) && ((0 == $paht) || (0 == $pvht))) {
            return 0;
        } elseif ((0 == $rule['papv']) && (0 == $pvht)) {
            return 0;
        }

        $price = $rule['papv'] ? $paht : $pvht;
        return ($price + ($price * $rule['rate']) / 100) + $rule['overhead'];
    }

    public static function matchingRules($fourn, $id_shop)
    {
        Db::getInstance()->update('eci_pricerules_match', array('flag' => 0), 'fournisseur = "' . pSQL($fourn) . '"');
        $lst_rules = Db::getInstance()->executes(
            'SELECT *
            FROM '._DB_PREFIX_.'eci_pricerules
            WHERE fournisseur LIKE "'.pSQL($fourn).'"
            AND active = 1
            AND id_shop = '.(int) $id_shop
        );

        $now = date('Y-m-d');
        foreach ($lst_rules as $rules) {
            if (('0000-00-00 00:00:00' != $rules['date_to']) && ($rules['date_to'] <= $now)) {
                Db::getInstance()->update(
                    'eci_pricerules',
                    array('active' => 0),
                    'id_pricerules = '.(int) $rules['id_pricerules']
                );
                continue;
            }
            if (!$rules['rate'] && !$rules['overhead']) {
                continue;
            }

            $search_price = $rules['papv'] ? 'price' : 'pmvc';
            $sql = 'SELECT product_reference, fournisseur, '.(int) $rules['id_pricerules'].', 1
                FROM '._DB_PREFIX_.'eci_catalog
                WHERE fournisseur = "'.pSQL($fourn).'"';
            $sql .= $rules['category'] ? ' AND category LIKE "%:://::'.pSQL($rules['category']).'%"' : '';
            $sql .= $rules['manufacturer'] ? ' AND manufacturer LIKE "'.pSQL($rules['manufacturer']).'"' : '';
            $sql .= (0 != $rules['price_min']) ? ' AND '.pSQL($search_price).' >= '.(float) $rules['price_min']: '';
            $sql .= (0 != $rules['price_max']) ? ' AND '.pSQL($search_price).' <= '.(float) $rules['price_max']: '';
            Db::getInstance()->execute('REPLACE INTO '._DB_PREFIX_.'eci_pricerules_match '.$sql);

            $sql_attr = 'SELECT ca.reference_attribute, ca.fournisseur, '.(int) $rules['id_pricerules'].', 1
                FROM '._DB_PREFIX_.'eci_catalog_attribute ca
                LEFT JOIN '._DB_PREFIX_.'eci_catalog c ON ca.product_reference = c.product_reference
                WHERE ca.fournisseur = "'.pSQL($fourn).'"';
            $sql_attr .= $rules['category'] ? ' AND c.category LIKE "%:://::'.pSQL($rules['category']).'%"' : '';
            $sql_attr .= $rules['manufacturer'] ? ' AND c.manufacturer LIKE "'.pSQL($rules['manufacturer']).'"' : '';
            $sql_attr .= (0 != $rules['price_min']) ? ' AND ca.'.pSQL($search_price).' >= '.(float) $rules['price_min']: '';
            $sql_attr .= (0 != $rules['price_max']) ? ' AND ca.'.pSQL($search_price).' <= '.(float) $rules['price_max']: '';
            Db::getInstance()->execute('REPLACE INTO '._DB_PREFIX_.'eci_pricerules_match '.$sql_attr);
        }

        Db::getInstance()->delete('eci_pricerules_match', 'fournisseur = "' . pSQL($fourn) . '" AND flag = 0');

        return true;
    }
}
