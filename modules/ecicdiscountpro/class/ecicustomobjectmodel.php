<?php
/**
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to a commercial license from SARL Ether Creation
 * Use, copy, modification or distribution of this source file without written
 * license agreement from the SARL Ether Creation is strictly forbidden.
 * In order to obtain a license, please contact us: contact@ethercreation.com
 * ...........................................................................
 * INFORMATION SUR LA LICENCE D'UTILISATION
 *
 * L'utilisation de ce fichier source est soumise a une licence commerciale
 * concedee par la societe Ether Creation
 * Toute utilisation, reproduction, modification ou distribution du present
 * fichier source sans contrat de licence ecrit de la part de la SARL Ether Creation est
 * expressement interdite.
 * Pour obtenir une licence, veuillez contacter la SARL Ether Creation a l'adresse: contact@ethercreation.com
 * ...........................................................................
 *
 * @author    Ether Creation SARL <contact@ethercreation.com>
 * @copyright 2008-2021 Ether Creation SARL
 * @license   Commercial license
 * International Registered Trademark & Property of Ether Creation SARL
 */

namespace ecicdiscountpro;

use Db;
use ObjectModel;

class EciCustomObjectModel extends ObjectModel
{

    /**
     *  Create the database table with its columns. Similar to the createColumn() method.
     */
    public function createDatabase()
    {
        $definition = ObjectModel::getDefinition($this);

        $multilang = isset($definition['multilang']) && $definition['multilang'];
        $multishop = (int) array_filter(
            $definition['fields'],
            function ($v) {
                return is_array($v) && isset($v['shop']);
            }
        );

        $sql = 'CREATE TABLE IF NOT EXISTS ' . _DB_PREFIX_ . $definition['table'] . ' (';
        $sql .= $definition['primary'] . ' INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,';

        foreach ($definition['fields'] as $field_name => $field) {
            if ($field_name === $definition['primary']) {
                continue;
            }

            if ($multilang && isset($field['lang']) && $field['lang']) {
                continue;
            }

            $sql .= $field_name . ' ' . $field['db_type'];

            if (isset($field['required']) && $field['required']) {
                $sql .= ' NOT NULL';
            }

            if (isset($field['default'])) {
                $sql .= ' DEFAULT "' . $field['default'] . '"';
            }

            $sql .= ',';
        }

        $sql = trim($sql, ',');
        $sql .= ')';

        Db::getInstance()->execute($sql);

        //create multilang tables
        if ($multilang) {
            $sql = 'CREATE TABLE IF NOT EXISTS ' . _DB_PREFIX_ . $definition['table'] . '_lang (';
            $sql .= $definition['primary'] . ' INTEGER UNSIGNED NOT NULL,';
            $sql .= 'id_lang INTEGER UNSIGNED NOT NULL,';

            if (isset($definition['multilang_shop']) && $definition['multilang_shop']) {
                $sql .= 'id_shop INTEGER UNSIGNED NOT NULL,';
                $sqlidx1 = 'ALTER TABLE ' . _DB_PREFIX_ . $definition['table'] . '_lang ADD PRIMARY KEY (' . $definition['primary'] . ', id_lang, id_shop)';
            } else {
                $sqlidx1 = 'ALTER TABLE ' . _DB_PREFIX_ . $definition['table'] . '_lang ADD PRIMARY KEY (' . $definition['primary'] . ', id_lang)';
            }

            foreach ($definition['fields'] as $field_name => $field) {
                if ($field_name === $definition['primary']) {
                    continue;
                }

                if (!isset($field['lang']) || !$field['lang']) {
                    continue;
                }

                $sql .= $field_name . ' ' . $field['db_type'];

                if (isset($field['required']) && $field['required']) {
                    $sql .= ' NOT NULL';
                }

                if (isset($field['default'])) {
                    $sql .= ' DEFAULT "' . $field['default'] . '"';
                }

                $sql .= ',';
            }

            $sql = trim($sql, ',');
            $sql .= ')';

            Db::getInstance()->execute($sql);

            //key
            $sqlidxQ = 'SELECT COUNT(*)
                FROM information_schema.table_constraints
                WHERE table_schema = "' . _DB_NAME_ . '"
                AND table_name = "' . _DB_PREFIX_ . $definition['table'] . '_lang"
                AND constraint_type = "PRIMARY KEY"';
            $sqlidx0 = 'ALTER TABLE ' . _DB_PREFIX_ . $definition['table'] . '_lang DROP PRIMARY KEY';
            if (Db::getInstance()->getValue($sqlidxQ)) {
                Db::getInstance()->execute($sqlidx0);
            }
            Db::getInstance()->execute($sqlidx1);
        }

        //create multishop tables
        if ($multishop) {
            $sql = 'CREATE TABLE IF NOT EXISTS ' . _DB_PREFIX_ . $definition['table'] . '_shop (';
            $sql .= $definition['primary'] . ' INTEGER UNSIGNED NOT NULL,';
            $sql .= 'id_shop INTEGER UNSIGNED NOT NULL,';

            foreach ($definition['fields'] as $field_name => $field) {
                if ($field_name === $definition['primary']) {
                    continue;
                }

                if (!isset($field['shop']) || !$field['shop']) {
                    continue;
                }

                $sql .= $field_name . ' ' . $field['db_type'];

                if (isset($field['required']) && $field['required']) {
                    $sql .= ' NOT NULL';
                }

                if (isset($field['default'])) {
                    $sql .= ' DEFAULT "' . $field['default'] . '"';
                }

                $sql .= ',';
            }

            $sql = trim($sql, ',');
            $sql .= ')';

            Db::getInstance()->execute($sql);

            //key
            $sqlidxQ = 'SELECT COUNT(*)
                FROM information_schema.table_constraints
                WHERE table_schema = "' . _DB_NAME_ . '"
                AND table_name = "' . _DB_PREFIX_ . $definition['table'] . '_shop"
                AND constraint_type = "PRIMARY KEY"';
            $sqlidx0 = 'ALTER TABLE ' . _DB_PREFIX_ . $definition['table'] . '_shop DROP PRIMARY KEY';
            $sqlidx1 = 'ALTER TABLE ' . _DB_PREFIX_ . $definition['table'] . '_shop ADD PRIMARY KEY (' . $definition['primary'] . ', id_shop)';
            if (Db::getInstance()->getValue($sqlidxQ)) {
                Db::getInstance()->execute($sqlidx0);
            }
            Db::getInstance()->execute($sqlidx1);
        }

        $this->install();
    }

    public function dropDatabase()
    {
        $definition = ObjectModel::getDefinition($this);
        $multilang = isset($definition['multilang']) && $definition['multilang'];
        $multishop = (int) array_filter(
            $definition['fields'],
            function ($v) {
                return is_array($v) && isset($v['shop']);
            }
        );

        $sql = 'DROP TABLE IF EXISTS ' . _DB_PREFIX_ . $definition['table'];
        Db::getInstance()->execute($sql);

        if ($multilang) {
            $sql = 'DROP TABLE IF EXISTS ' . _DB_PREFIX_ . $definition['table'] . '_lang';
            Db::getInstance()->execute($sql);
        }

        if ($multishop) {
            $sql = 'DROP TABLE IF EXISTS ' . _DB_PREFIX_ . $definition['table'] . '_shop';
            Db::getInstance()->execute($sql);
        }

        $this->uninstall();
    }

    public function truncateDatabase()
    {
        $definition = ObjectModel::getDefinition($this);
        $multilang = isset($definition['multilang']) && $definition['multilang'];
        $multishop = (int) array_filter(
            $definition['fields'],
            function ($v) {
                return is_array($v) && isset($v['shop']);
            }
        );

        $sql = 'TRUNCATE TABLE ' . _DB_PREFIX_ . $definition['table'];
        Db::getInstance()->execute($sql);

        if ($multilang) {
            $sql = 'TRUNCATE TABLE ' . _DB_PREFIX_ . $definition['table'] . '_lang';
            Db::getInstance()->execute($sql);
        }

        if ($multishop) {
            $sql = 'TRUNCATE TABLE ' . _DB_PREFIX_ . $definition['table'] . '_shop';
            Db::getInstance()->execute($sql);
        }
    }

    public function install()
    {
    }

    public function uninstall()
    {
    }

    public function addIndexesIfNotExist($indexes)
    {
        foreach ($indexes as $table => $lst_index) {
            $show_index = Db::getInstance()->executes('SHOW INDEX FROM '.pSQL($table));
            foreach ($lst_index as $key => $field) {
                $add = true;
                foreach ($show_index as $tab_show_index) {
                    if ($tab_show_index['Key_name'] == $key) {
                        $add = false;
                        break;
                    }
                }
                if ($add) {
                    Db::getInstance()->execute('ALTER TABLE '.pSQL($table).' ADD INDEX `'.pSQL($key).'` ('.pSQL($field).')');
                }
            }
        }
    }
}
