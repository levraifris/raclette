<?php
/**
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to a commercial license from SARL Ether Creation
 * Use, copy, modification or distribution of this source file without written
 * license agreement from the SARL Ether Creation is strictly forbidden.
 * In order to obtain a license, please contact us: contact@ethercreation.com
 * ...........................................................................
 * INFORMATION SUR LA LICENCE D'UTILISATION
 *
 * L'utilisation de ce fichier source est soumise a une licence commerciale
 * concedee par la societe Ether Creation
 * Toute utilisation, reproduction, modification ou distribution du present
 * fichier source sans contrat de licence ecrit de la part de la SARL Ether Creation est
 * expressement interdite.
 * Pour obtenir une licence, veuillez contacter la SARL Ether Creation a l'adresse: contact@ethercreation.com
 * ...........................................................................
 *
 * @author    Ether Creation SARL <contact@ethercreation.com>
 * @copyright 2008-2021 Ether Creation SARL
 * @license   Commercial license
 * International Registered Trademark & Property of Ether Creation SARL
 */

namespace ecicdiscountpro;

use Tools;

/**
 * singleton that keeps translations across processes
 *
 * @author Alec Page
 */
class EciTranslations
{
    private $_translations;
    private static $_instance = null;

    private function __construct()
    {
        $this->loadTranslations();
    }

    public static function getInstance()
    {
        if (is_null(self::$_instance)) {
            self::$_instance = new EciTranslations();
        }

        return self::$_instance;
    }

    public function translate($string, $class, $iso_code = null)
    {
        if (!is_array($this->_translations)) {
            $this->loadTranslations();
        }

        $translation = array();
        $one_translation = '';
        $key = '<{' . __NAMESPACE__ . '}prestashop>' . Tools::strtolower($class) . '_' . md5($string);
        $languages = \Language::getLanguages(false);
        foreach ($languages as $language) {
            $translation[$language['id_lang']] = isset($this->_translations[$key][$language['id_lang']])
                ? $this->_translations[$key][$language['id_lang']]
                : $string;
            if ($language['iso_code'] === $iso_code) {
                $one_translation = isset($this->_translations[$key][$language['id_lang']])
                    ? $this->_translations[$key][$language['id_lang']]
                    : $string;
            }
        }

        if (!is_null($iso_code)) {
            return $one_translation ? $one_translation : $string;
        }

        return $translation;
    }

    public function loadTranslations()
    {
        $languages = \Language::getLanguages(false);
        $this->_translations = array();
        foreach ($languages as $language) {
            $file_path = dirname(__FILE__) . '/../translations/' . $language['iso_code'] . '.php';
            if (file_exists($file_path)) {
                $_MODULE = array();
                require $file_path;
                foreach ($_MODULE as $key => $value) {
                    $this->_translations[$key][$language['id_lang']] = $value;
                }
            }
        }
    }
}
