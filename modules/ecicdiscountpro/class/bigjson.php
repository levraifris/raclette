<?php
/**
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to a commercial license from SARL Ether Creation
 * Use, copy, modification or distribution of this source file without written
 * license agreement from the SARL Ether Creation is strictly forbidden.
 * In order to obtain a license, please contact us: contact@ethercreation.com
 * ...........................................................................
 * INFORMATION SUR LA LICENCE D'UTILISATION
 *
 * L'utilisation de ce fichier source est soumise a une licence commerciale
 * concedee par la societe Ether Creation
 * Toute utilisation, reproduction, modification ou distribution du present
 * fichier source sans contrat de licence ecrit de la part de la SARL Ether Creation est
 * expressement interdite.
 * Pour obtenir une licence, veuillez contacter la SARL Ether Creation a l'adresse: contact@ethercreation.com
 * ...........................................................................
 *
 * @author    Ether Creation SARL <contact@ethercreation.com>
 * @copyright 2008-2021 Ether Creation SARL
 * @license   Commercial license
 * International Registered Trademark & Property of Ether Creation SARL
 */

namespace ecicdiscountpro;

use Exception;
use Tools;
use XMLReader;

class Bigjson
{
    const EXACT = 0;
    const IN = 1;
    const REGEX = 2;
    const CI = 4;
    const NOT = 8;

    const CHUNK_SIZE = 64;
    const BEGIN_ARRAY = '[';
    const END_ARRAY = ']';
    const BEGIN_OBJ = '{';
    const END_OBJ = '}';
    const OBJ_SEP = ',';
    const ASSOC_SEP = ':';
    const STRING_MARK = '"';

    const DX = '.txt';
    const IX = '.json';

    private $dir;
    private $file;
    private $data;
    private $offsets;

    public $maxLine;
    public $nLines;
    public $currentLine;
    public $autoIndex = true;
    private $tempIndex;
    public $decode = true;
    private $nextLine;
    private $pos;
    private $len;
    public $indexes = array();
    public $keys = array();

    public function __construct($fileName, $dir, $tempIndex = true, $decode = true)
    {
        $dir = rtrim($dir, '/').'/';

        if (!file_exists($dir.$fileName.self::DX)) {
            throw new Exception('Data file is missing for ' . $fileName, 1);
        }
        if (!file_exists($dir.$fileName.self::IX)) {
            throw new Exception('Offsets file is missing for ' . $fileName, 2);
        }

        if (($this->data = @fopen($dir.$fileName.self::DX, 'r')) === false) {
            throw new Exception('Data file cannot be opened for reading in ' . $fileName, 3);
        }
        if (($offsetsJson = Tools::file_get_contents($dir.$fileName.self::IX)) === false) {
            throw new Exception('Offsets file cannot be read in ' . $fileName, 4);
        }
        if (($this->offsets = json_decode($offsetsJson, true)) === null) {
            throw new Exception('Offsets file is not a good json for ' . $fileName, 5);
        }
        if (!is_array($this->offsets)) {
            throw new Exception('Offsets file is not an array in ' . $fileName, 6);
        }
        if (count($this->offsets) < 2) {
            throw new Exception('Offsets file is empty or shorter than 2 values in ' . $fileName, 7);
        }
        if ($this->offsets[0] !== 0) {
            throw new Exception('Offsets file is not a proper file of offsets for ' . $fileName, 8);
        }

        $this->dir = $dir;
        $this->file = $fileName;
        $this->tempIndex = (bool) $tempIndex;
        if (!$tempIndex) {
            $this->loadIndex();
        }
        $this->decode = (bool) $decode;
        $this->pos = 0;
        $this->currentLine = 0;
        $this->nextLine = 0;
        $this->maxLine = count($this->offsets) - 2;
        $this->nLines = $this->maxLine + 1;
        $this->len = $this->offsets[1];
        $this->keys = array_keys($this->get(0));

        return $this;
    }

    public function read($nLine = null)
    {
        if (!is_null($nLine) && !is_int($nLine)) {
            return false;
        }

        if (is_null($nLine) && ($this->nextLine <= $this->maxLine)) {
            // if $nLine is null, read the next line and go forward
            fseek($this->data, $this->pos);
            $json = fread($this->data, $this->len);
            $this->currentLine = $this->nextLine;
            $this->nextLine++;
            if ($this->nextLine <= $this->maxLine) {
                $this->pos += $this->len;
                $this->len = $this->offsets[$this->nextLine + 1];
            }

            return $this->decode ? json_decode($json, true) : $json;
        } elseif (is_null($nLine)) {
            // return false if totalLines is reached
            return false;
        } elseif ($nLine <= $this->maxLine) {
            $this->currentLine = $this->nextLine = $nLine;
            if ($this->go($this->currentLine)) {
                return $this->read();
            }
        }

        $this->currentLine = $this->nextLine = $nLine;

        return false;
    }

    public function go($nLine)
    {
        if ($nLine > $this->maxLine) {
            return false;
        }

        $this->pos = array_sum(array_slice($this->offsets, 0, $nLine + 1));
        $this->len = $this->offsets[$nLine + 1];
        $this->nextLine = $this->currentLine = $nLine;

        return true;
    }

    public function get($nLine)
    {
        if ($nLine > $this->maxLine) {
            return false;
        }

        $pos = array_sum(array_slice($this->offsets, 0, $nLine + 1));
        $len = $this->offsets[$nLine + 1];
        fseek($this->data, $pos);
        $json = fread($this->data, $len);

        return $this->decode ? json_decode($json, true) : $json;
    }

    public function searchOne($key, $value, $compare = self::EXACT)
    {
        if (isset($this->indexes[$key])) {
            foreach ($this->indexes[$key] as $indexedValue => $listIndexes) {
                if (self::_isFound($indexedValue, $value, $compare)) {
                    return $listIndexes[0];
                }
            }
        } elseif ($this->autoIndex) {
            $this->buildIndex($key);
            return $this->searchOne($key, $value, $compare);
        } else {
            $pos = 0;
            for ($i = 0; $i <= $this->maxLine; $i++) {
                $pos += $this->offsets[$i];
                $len = $this->offsets[$i + 1];
                fseek($this->data, $pos);
                $json = fread($this->data, $len);
                $tab = json_decode($json, true);
                if (!isset($tab[$key])) {
                    continue;
                }
                if (self::_isFound($tab[$key], $value, $compare)) {
                    return $i;
                }
            }
        }

        return false;
    }

    public function searchAll($key, $value, $compare = self::EXACT)
    {
        $found = array();
        if (isset($this->indexes[$key])) {
            foreach ($this->indexes[$key] as $indexedValue => $listIndexes) {
                if (self::_isFound($indexedValue, $value, $compare)) {
                    $found = array_merge($found, $listIndexes);
                }
            }
            //$found = array_unique($found);
            $found = array_keys(array_flip($found));//faster than array_unique()
            sort($found);
        } elseif ($this->autoIndex) {
            $this->buildIndex($key);
            $found = $this->searchAll($key, $value, $compare);
        } else {
            $pos = 0;
            for ($i = 0; $i <= $this->maxLine; $i++) {
                $pos += $this->offsets[$i];
                $len = $this->offsets[$i + 1];
                fseek($this->data, $pos);
                $json = fread($this->data, $len);
                $tab = json_decode($json, true);
                if (!isset($tab[$key])) {
                    continue;
                }
                if (self::_isFound($tab[$key], $value, $compare)) {
                    $found[] = $i;
                }
            }
        }

        return $found;
    }

    public function searchLast($key, $value, $compare = self::EXACT)
    {
        $found = $this->searchAll($key, $value, $compare);

        return $found ? end($found) : false;
    }

    private static function _isFound($tabValue, $searchValue, $compare = self::EXACT)
    {
        if (!@($tabValue == (string) $tabValue) || !@($searchValue == (string) $searchValue)) {
            return false;
        }

        switch ($compare) {
            case self::IN:
                if (Tools::strpos($tabValue, $searchValue) !== false) {
                    return true;
                }
                break;
            case self::NOT + self::IN:
                if (Tools::strpos($tabValue, $searchValue) === false) {
                    return true;
                }
                break;
            case self::REGEX:
                if (preg_match($searchValue, $tabValue)) {
                    return true;
                }
                break;
            case self::NOT + self::REGEX:
                if (!preg_match($searchValue, $tabValue)) {
                    return true;
                }
                break;
            case self::EXACT + self::CI:
                if (!strcasecmp($tabValue, $searchValue)) {
                    return true;
                }
                break;
            case self::NOT + self::EXACT + self::CI:
                if (0 !== strcasecmp($tabValue, $searchValue)) {
                    return true;
                }
                break;
            case self::IN + self::CI:
                if (stripos($tabValue, $searchValue) !== false) {
                    return true;
                }
                break;
            case self::NOT + self::IN + self::CI:
                if (stripos($tabValue, $searchValue) === false) {
                    return true;
                }
                break;
            case self::NOT:
                if (0 !== strcmp($tabValue, $searchValue)) {
                    return true;
                }
                break;
            default:
                if (0 === strcmp($tabValue, $searchValue)) {
                    return true;
                }
                break;
        }

        return false;
    }

    public function buildIndex($key)
    {
        $pos = 0;
        $found = array();
        for ($i = 0; $i <= $this->maxLine; $i++) {
            $pos += $this->offsets[$i];
            $len = $this->offsets[$i + 1];
            fseek($this->data, $pos);
            $json = fread($this->data, $len);
            $tab = json_decode($json, true);
            if (!isset($tab[$key]) || !@($tab[$key] == (string) $tab[$key])) {
                continue;
            }
            $found[$tab[$key]][] = $i;
        }

        $this->indexes[$key] = $found;
    }

    public function rebuildIndex()
    {
        foreach (array_keys($this->indexes) as $key) {
            $this->buildIndex($key);
        }
        $this->_writeIndexes();
    }

    public function loadIndex()
    {
        if (file_exists($this->dir.$this->file.'_indexes.json')) {
            $this->indexes = json_decode(Tools::file_get_contents($this->dir.$this->file.'_indexes.json'), true);
        }
    }

    public function deleteIndex($key = false)
    {
        if (!$key) {
            $this->indexes = array();
        } else {
            unset($this->indexes[$key]);
        }
        $this->_writeIndexes();
    }

    private function _writeIndexes()
    {
        file_put_contents($this->dir.$this->file.'_indexes.json', json_encode($this->indexes));
    }

    /**
     * get values instead of indexes
     * $rfields : array of field names that will be returned
     * $wheres : array of arrays of three parameter (searchAll parameters)
     * return array of associative arrays matching all wheres (joined by AND)
     */
    public function select($rfields, $wheres, $start = null, $size = null, $only_count = false)
    {
        if (!is_array($wheres)) {
            return false;
        }
        if (!is_array($rfields)) {
            $rfields = array($rfields);
        }

        $fields = array_flip($rfields);

        $indexes = array();
        $intersect = false;
        foreach ($wheres as $where) {
            if (!is_array($where) || 2 > count($where) || 3 < count($where)) {
                continue;
            }
            $where[2] = isset($where[2]) ? $where[2] : self::EXACT;

            $indexes = $intersect
                ? array_intersect($indexes, $this->searchAll($where[0], $where[1], (int) $where[2]))
                : $this->searchAll($where[0], $where[1], (int) $where[2]);
            $intersect = true;
        }

        if ($only_count) {
            return count($indexes);
        }

        if (!$indexes) {
            return array();
        }

        sort($indexes);

        $old_decode = $this->decode;
        $this->decode = true;
        $return = array();
        if (!is_null($start) && !is_null($size)) {
            for ($i = (int) $start; $i < ((int) $start + (int) $size); $i++) {
                if ($i >= count($indexes)) {
                    break;
                }
                if (($inter = array_intersect_key($this->get($indexes[$i]), $fields))) {
                    $return[$i] = $inter;
                }
            }
        } elseif (!is_null($start) && is_null($size)) {
            for ($i = (int) $start; $i < count($indexes); $i++) {
                if (($inter = array_intersect_key($this->get($indexes[$i]), $fields))) {
                    $return[$i] = $inter;
                }
            }
        } else {
            foreach ($indexes as $i) {
                if (($inter = array_intersect_key($this->get($i), $fields))) {
                    $return[$i] = $inter;
                }
            }
        }
        $this->decode = $old_decode;

        return $return;
    }

    public function __destruct()
    {
        fclose($this->data);
        if (!$this->tempIndex) {
            $this->_writeIndexes();
        }
    }

    /**
     * general csv (products, customers,...) to bigjson transform
     * $path to the original csv
     * $header = 1:firstline, array:this array
     * $field_sep is the field separator character
     * $enclosure is the field enclosure character
     * $escape is the character escape character
     * $line_endings character ending the line
     * $encoding of the original csv
     * $lcallback is the line exploding callback function in case str_getcsv is not sufficent
     * $fcallback is the field modifying callback function if needed
     * $continue old files or from scratch (old files will be overwritten)
     * $icallback is the array modifying callback function if needed
     */
    public static function csv2bjs($path, $header = 1, $field_sep = ';', $enclosure = '"', $escape = '\\', $line_endings = "\n", $encoding = 'UTF-8', $lcallback = null, $fcallback = null, $continue = false, $icallback = null)
    {
        if (1 !== $header && !is_array($header)) {
            return false;
        }
        $path_parts = pathinfo($path);
        $fileName = $path_parts['dirname'] . '/' . $path_parts['filename'];
        $fileExt = self::DX;
        $offsetsExt = self::IX;
        if (!$continue && file_exists($fileName . $fileExt)) {
            @unlink($fileName . $fileExt);
        }
        if ($continue && file_exists($fileName . $offsetsExt)) {
            $offsets = json_decode(Tools::file_get_contents($fileName . $offsetsExt), true);
        } else {
            $offsets = array(0);
        }
        $handle = fopen($path, 'rb');
        if (is_callable($icallback)) {
            $icallback_parm = null;
        } elseif (!is_null($icallback) && is_array($icallback) && 2 == count($icallback) && is_callable($icallback[0]) && is_array($icallback[1])) {
            $icallback_parm = $icallback[1];
            $icallback = $icallback[0];
        } else {
            $icallback = null;
        }

        $n = 0;
        $buffer = '';
        $et = $header === 1 ? array() : $header;
        $added = 0;
        while (($sline = stream_get_line($handle, 8192, $line_endings)) !== false) {
            if ('UTF-8' != Tools::strtoupper($encoding)) {
                $sline = mb_convert_encoding($sline, 'UTF-8', $encoding);
                //$sline = html_entity_decode(htmlentities($sline, ENT_NOQUOTES, $encoding), ENT_NOQUOTES, 'UTF-8');
            }
            if (is_null($lcallback)) {
                $line = str_getcsv($sline, $field_sep, $enclosure, $escape);
            } else {
                $line = call_user_func($lcallback, $sline);
            }
            $n++;
            if (1 === $n && 1 === $header) {
                // first line is supposed to be header
                if (is_null($fcallback)) {
                    $et = $line;
                } else {
                    $et = array_map($fcallback, $line);
                }
                continue;
            } elseif (count($et) !== count($line)) {
                if (count($et) > count($line)) {
                    $line = array_pad($line, count($et), '');
                } else {
                    $line = array_slice($line, 0, count($et));
                }
            }
            if (is_null($fcallback)) {
                $prod = array_combine($et, $line);
            } else {
                $prod = array_combine($et, array_map($fcallback, $line));
            }
            if (!is_null($icallback)) {
                if (is_null($icallback_parm)) {
                    $prod = call_user_func($icallback, $prod);
                } else {
                    $prod = call_user_func_array($icallback, array_merge(array($prod), $icallback_parm));
                }
            }
            if (!$prod) {
                continue;
            }
            $ijson = json_encode($prod);
            if (JSON_ERROR_NONE !== json_last_error()) {
                return 'line ' . $n . ' ' . json_last_error_msg() . "\n" . var_export($prod, true);
                /*continue;*/
            }
            $buffer .= $ijson;
            if (Tools::strlen($buffer, '8bit') > 81920) {
                file_put_contents($fileName . $fileExt, $buffer, FILE_APPEND);
                $buffer = '';
            }
            $offsets[] = strlen($ijson); // we want bytes, not characters
            $added++;
        }
        if (!empty($buffer)) {
            file_put_contents($fileName . $fileExt, $buffer, FILE_APPEND);
        }
        file_put_contents($fileName . $offsetsExt, json_encode($offsets));

        fclose($handle);

        if (1 === count($offsets)) {
            return false;
        }

        return $added;
    }

    public static function csv2bjsMl($path, $header = 1, $field_sep = ';', $enclosure = '"', $escape = '\\', $line_endings = "\n", $encoding = 'UTF-8', $lcallback = null, $fcallback = null, $continue = false, $icallback = null)
    {
        if (1 !== $header && !is_array($header)) {
            return false;
        }
        $path_parts = pathinfo($path);
        $fileName = $path_parts['dirname'] . '/' . $path_parts['filename'];
        $fileExt = self::DX;
        $offsetsExt = self::IX;
        if (!$continue && file_exists($fileName . $fileExt)) {
            @unlink($fileName . $fileExt);
        }
        if ($continue && file_exists($fileName . $offsetsExt)) {
            $offsets = json_decode(Tools::file_get_contents($fileName . $offsetsExt), true);
        } else {
            $offsets = array(0);
        }
        $handle = fopen($path, 'rb');
        if (is_callable($icallback)) {
            $icallback_parm = null;
        } elseif (!is_null($icallback) && is_array($icallback) && 2 == count($icallback) && is_callable($icallback[0]) && is_array($icallback[1])) {
            $icallback_parm = $icallback[1];
            $icallback = $icallback[0];
        } else {
            $icallback = null;
        }

        $n = 0;
        $buffer = '';
        $et = $header === 1 ? array() : $header;
        $added = 0;
        while (($dat = fgetcsv($handle, 0, $field_sep, $enclosure, $escape)) !== false) {
            foreach ($dat as &$item) {
                $item = ($enclosure ?: '') . str_replace($enclosure, $escape . $enclosure, $item) . ($enclosure ?: '');
            }
            $sline = implode($field_sep, $dat);
            if ('UTF-8' != Tools::strtoupper($encoding)) {
                $sline = mb_convert_encoding($sline, 'UTF-8', $encoding);
                //$sline = html_entity_decode(htmlentities($sline, ENT_NOQUOTES, $encoding), ENT_NOQUOTES, 'UTF-8');
            }
            if (is_null($lcallback)) {
                $line = str_getcsv($sline, $field_sep, $enclosure, $escape);
            } else {
                $line = call_user_func($lcallback, $sline);
            }
            $n++;
            if (1 === $n && 1 === $header) {
                // first line is supposed to be header
                if (is_null($fcallback)) {
                    $et = $line;
                } else {
                    $et = array_map($fcallback, $line);
                }
                continue;
            } elseif (count($et) !== count($line)) {
                if (count($et) > count($line)) {
                    $line = array_pad($line, count($et), '');
                } else {
                    $line = array_slice($line, 0, count($et));
                }
            }
            if (is_null($fcallback)) {
                $prod = array_combine($et, $line);
            } else {
                $prod = array_combine($et, array_map($fcallback, $line));
            }
            if (!is_null($icallback)) {
                if (is_null($icallback_parm)) {
                    $prod = call_user_func($icallback, $prod);
                } else {
                    $prod = call_user_func_array($icallback, array_merge(array($prod), $icallback_parm));
                }
            }
            if (!$prod) {
                continue;
            }
            $ijson = json_encode($prod);
            if (JSON_ERROR_NONE !== json_last_error()) {
                return 'line ' . $n . ' ' . json_last_error_msg() . "\n" . var_export($prod, true);
                /*continue;*/
            }
            $buffer .= $ijson;
            if (Tools::strlen($buffer, '8bit') > 81920) {
                file_put_contents($fileName . $fileExt, $buffer, FILE_APPEND);
                $buffer = '';
            }
            $offsets[] = strlen($ijson); // we want bytes, not characters
            $added++;
        }
        if (!empty($buffer)) {
            file_put_contents($fileName . $fileExt, $buffer, FILE_APPEND);
        }
        file_put_contents($fileName . $offsetsExt, json_encode($offsets));

        fclose($handle);

        if (1 === count($offsets)) {
            return false;
        }

        return $added;
    }

    /**
     *  specific json object (products, customers,...) to bigjson transform
     */
    public static function jso2bjs($path, $continue = false, $icallback = null)
    {
        $path_parts = pathinfo($path);
        $fileName = $path_parts['dirname'] . '/' . $path_parts['filename'];
        $fileExt = self::DX;
        $offsetsExt = self::IX;
        if (!$continue && file_exists($fileName . $fileExt)) {
            @unlink($fileName . $fileExt);
        }
        if ($continue && file_exists($fileName . $offsetsExt)) {
            $offsets = json_decode(Tools::file_get_contents($fileName . $offsetsExt), true);
        } else {
            $offsets = array(0);
        }
        $handle = fopen($path, 'rb');
        if (is_callable($icallback)) {
            $icallback_parm = null;
        } elseif (!is_null($icallback) && is_array($icallback) && 2 == count($icallback) && is_callable($icallback[0]) && is_array($icallback[1])) {
            $icallback_parm = $icallback[1];
            $icallback = $icallback[0];
        } else {
            $icallback = null;
        }

        $buffer = '';
        $pos = 0;
        $ob_switch = 0;
        $added = 0;
        $str = self::BEGIN_ARRAY;
        while (false !== ($chunk = fread($handle, 8192))) {
            $pointer = 0;
            while (isset($chunk[$pointer])) {
                $prev_str = $str;
                $char = ord($chunk[$pointer]);
                if ($char < 128) {
                    $str = $chunk[$pointer];
                    $bytes = 1;
                } else {
                    if ($char < 224) {
                        $bytes = 2;
                    } elseif ($char < 240) {
                        $bytes = 3;
                    } elseif ($char < 248) {
                        $bytes = 4;
                    } elseif ($char == 252) {
                        $bytes = 5;
                    } else {
                        $bytes = 6;
                    }
                    $str = substr($chunk, $pointer, $bytes); // we want bytes, not characters
                    if (($pointer + $bytes > 8192) || false === ($str = substr($chunk, $pointer, $bytes))) { // we want bytes, not characters
                        break;
                    }
                }
                $pointer += $bytes;
                if (0 === $pos && 0 === $pointer && self::BEGIN_ARRAY === $str) {
                    continue;
                }
                if (self::BEGIN_OBJ === $str && (self::BEGIN_ARRAY === $prev_str || self::OBJ_SEP === $prev_str)) {
                    $ob_switch++;
                }
                if ($ob_switch >= 1) {
                    $buffer .= $str;
                }
                if (self::END_OBJ === $str) {
                    $ob_switch--;
                    if (0 === $ob_switch) {
                        if (is_null(json_decode($buffer, true))) {
                            $ob_switch++;
                            continue;
                        }
                        if (!is_null($icallback)) {
                            if (is_null($icallback_parm)) {
                                $prod = call_user_func($icallback, json_decode($buffer, true));
                            } else {
                                $prod = call_user_func_array($icallback, array_merge(array(json_decode($buffer, true)), $icallback_parm));
                            }
                            if (!$prod) {
                                $buffer = '';
                                continue;
                            }
                            $buffer = json_encode($prod);
                        }
                        file_put_contents($fileName . $fileExt, $buffer, FILE_APPEND);
                        $offsets[] = strlen($buffer); // we want bytes, not characters
                        $buffer = '';
                        $added++;
                    }
                }
            }
            if (feof($handle)) {
                break;
            }
            $pos += $pointer;
            fseek($handle, $pos);
        }
        fclose($handle);
        file_put_contents($fileName . $offsetsExt, json_encode($offsets));

        if (1 === count($offsets)) {
            return false;
        }

        return $added;
    }

    /**
     *  specific xml object (products, customers,...) to bigjson transform
     */
    public static function xmlo2bjs($path, $tagName, $continue = false, $icallback = null)
    {
        $startPatrn0 = '/^\<(\w+)?:?(?:' . implode('|$)(?:', str_split($tagName)) . '|$)(?:(\ |\>)|$)/';
        $startPatrn1 = '/^\<(\w+:)?' . $tagName . '\ |\>/';
        $endPatrn = '/\<\/(\w+:)?' . $tagName . '\>$/';

        $path_parts = pathinfo($path);
        $fileName = $path_parts['dirname'] . '/' . $path_parts['filename'];
        $fileExt = self::DX;
        $offsetsExt = self::IX;
        if (!$continue && file_exists($fileName . $fileExt)) {
            @unlink($fileName . $fileExt);
        }
        if ($continue && file_exists($fileName . $offsetsExt)) {
            $offsets = json_decode(Tools::file_get_contents($fileName . $offsetsExt), true);
        } else {
            $offsets = array(0);
        }
        $handle = fopen($path, 'rb');
        if (is_callable($icallback)) {
            $icallback_parm = null;
        } elseif (!is_null($icallback) && is_array($icallback) && 2 == count($icallback) && is_callable($icallback[0]) && is_array($icallback[1])) {
            $icallback_parm = $icallback[1];
            $icallback = $icallback[0];
        } else {
            $icallback = null;
        }

        $pos = 0;
        $buffer = $item = '';
        $sw = false;
        $added = 0;
        while ($chunk = fread($handle, 8192)) {
            $pointer = 0;
            while (isset($chunk[$pointer])) {
                $char = ord($chunk[$pointer]);
                if ($char < 128) {
                    $str = $chunk[$pointer];
                    $bytes = 1;
                } else {
                    if ($char < 224) {
                        $bytes = 2;
                    } elseif ($char < 240) {
                        $bytes = 3;
                    } elseif ($char < 248) {
                        $bytes = 4;
                    } elseif ($char == 252) {
                        $bytes = 5;
                    } else {
                        $bytes = 6;
                    }
                    if (($pointer + $bytes > 8192) || false === ($str = substr($chunk, $pointer, $bytes))) { // we want bytes, not characters
                        break;
                    }
                }
                $pointer += $bytes;
                $item .= $str;
                if (!$sw) {
                    if (!preg_match($startPatrn0, $item)) {
                        $item = '';
                    } elseif (preg_match($startPatrn1, $item)) {
                        $sw = true;
                    }
                }
                if ($sw && preg_match($endPatrn, $item)) {
                    $sitem = preg_replace(
                        array('#(</?)\w+:([^>]+>)#', '# \w+:(\w*=".*?")#'),
                        array('$1$2', ' $1'),
                        $item
                    );
                    $xitem = simplexml_load_string($sitem, "SimpleXMLElement", LIBXML_COMPACT | LIBXML_NOCDATA);
                    $jitem = json_encode($xitem);
                    if (!is_null($icallback)) {
                        if (is_null($icallback_parm)) {
                            $prod = call_user_func($icallback, json_decode($jitem, true));
                        } else {
                            $prod = call_user_func_array($icallback, array_merge(array(json_decode($jitem, true)), $icallback_parm));
                        }
                        if (!$prod) {
                            $item = '';
                            $sw = false;
                            continue;
                        }
                        $jitem = json_encode($prod);
                    }
                    $buffer .= $jitem;
                    $offsets[] = strlen($jitem); // we want bytes, not characters
                    if (Tools::strlen($buffer, '8bit') > 81920) {
                        file_put_contents($fileName . $fileExt, $buffer, FILE_APPEND);
                        $buffer = '';
                    }
                    $added++;
                    $item = '';
                    $sw = false;
                }
            }
            $pos += $pointer;
            fseek($handle, $pos);
        }

        if ($buffer) {
            file_put_contents($fileName . $fileExt, $buffer, FILE_APPEND);
        }
        file_put_contents($fileName . $offsetsExt, json_encode($offsets));

        if (1 === count($offsets)) {
            return false;
        }

        return $added;
    }

    /**
     *  specific xml object (products, customers,...) to bigjson transform
     */
    public static function xmlo2bjsFast($path, $tagName, $continue = false, $icallback = null)
    {
        $path_parts = pathinfo($path);
        $fileName = $path_parts['dirname'] . '/' . $path_parts['filename'];
        $fileExt = self::DX;
        $offsetsExt = self::IX;
        if (!$continue && file_exists($fileName . $fileExt)) {
            @unlink($fileName . $fileExt);
        }
        if ($continue && file_exists($fileName . $offsetsExt)) {
            $offsets = json_decode(Tools::file_get_contents($fileName . $offsetsExt), true);
        } else {
            $offsets = array(0);
        }
        if (is_callable($icallback)) {
            $icallback_parm = null;
        } elseif (!is_null($icallback) && is_array($icallback) && 2 == count($icallback) && is_callable($icallback[0]) && is_array($icallback[1])) {
            $icallback_parm = $icallback[1];
            $icallback = $icallback[0];
        } else {
            $icallback = null;
        }

        $offsets = array(0);
        $buffer = '';
        $xml = new XMLReader();
        $xml->open($path);
        self::seekTag($xml, $tagName);
        $added = 0;
        while ($xml->localName === $tagName) {
            $jitem_o = json_encode(
                simplexml_load_string(
                    $xml->readOuterXML(),
                    "SimpleXMLElement",
                    LIBXML_COMPACT | LIBXML_NOCDATA
                )
            );
            $item = json_decode($jitem_o, true);
            //array_walk($item, function (&$v) {$v = is_array($v)?'':$v;});
            if (!is_null($icallback)) {
                if (is_null($icallback_parm)) {
                    $item = call_user_func($icallback, $item);
                } else {
                    $item = call_user_func_array($icallback, array_merge(array($item), $icallback_parm));
                }
            }
            if ($item) {
                $jitem_f = json_encode($item);
                $buffer .= $jitem_f;
                $offsets[] = strlen($jitem_f); // we want bytes, not chars
                if (Tools::strlen($buffer) > 8192) {
                    file_put_contents($fileName . $fileExt, $buffer, FILE_APPEND);
                    $buffer = '';
                }
                $added++;
            }
            self::seekTag($xml, $tagName);
            self::seekTag($xml, $tagName);
        }
        $xml->close();
        if ($buffer) {
            file_put_contents($fileName . $fileExt, $buffer, FILE_APPEND);
        }
        file_put_contents($fileName . $offsetsExt, json_encode($offsets));

        if (1 === count($offsets)) {
            return false;
        }

        return $added;
    }

    public static function seekTag($xml, $tag)
    {
        while ($xml->read() && $tag !== $xml->localName) {
            continue;
        }
    }

    /**
     *  specific array of arrays (e.g. as returned from "select" request) to bigjson format
     */
    public static function ar2bjs($array, $path, $continue = false, $icallback = null)
    {
        $path_parts = pathinfo($path);
        $fileName = $path_parts['dirname'] . '/' . $path_parts['filename'];
        $fileExt = self::DX;
        $offsetsExt = self::IX;
        if (!$continue && file_exists($fileName . $fileExt)) {
            @unlink($fileName . $fileExt);
        }
        if ($continue && file_exists($fileName . $offsetsExt)) {
            $offsets = json_decode(Tools::file_get_contents($fileName . $offsetsExt), true);
        } else {
            $offsets = array(0);
        }
        if (is_callable($icallback)) {
            $icallback_parm = null;
        } elseif (!is_null($icallback) && is_array($icallback) && 2 == count($icallback) && is_callable($icallback[0]) && is_array($icallback[1])) {
            $icallback_parm = $icallback[1];
            $icallback = $icallback[0];
        } else {
            $icallback = null;
        }

        $buffer = '';
        $added = 0;
        foreach ($array as $product) {
            if (!is_null($icallback)) {
                if (is_null($icallback_parm)) {
                    $product = call_user_func($icallback, $product);
                } else {
                    $product = call_user_func_array($icallback, array_merge(array($product), $icallback_parm));
                }
            }
            if (!$product) {
                continue;
            }
            $ijson = json_encode($product);
            if (JSON_ERROR_NONE !== json_last_error()) {
                return json_last_error_msg() . "\n" . var_export($product, true);
                /*continue;*/
            }
            $buffer .= $ijson;
            if (Tools::strlen($buffer) > 81920) {
                file_put_contents($fileName . $fileExt, $buffer, FILE_APPEND);
                $buffer = '';
            }
            $offsets[] = strlen($ijson); // we want bytes, not characters
            $added++;
        }

        if ($buffer) {
            file_put_contents($fileName . $fileExt, $buffer, FILE_APPEND);
        }
        file_put_contents($fileName . $offsetsExt, json_encode($offsets));

        if (1 === count($offsets)) {
            return false;
        }

        return $added;
    }

    public static function mergebjs($first, $second)
    {
        $fileExt = self::DX;
        $offsetsExt = self::IX;

        $path_parts1 = pathinfo($first);
        $firstfileName = $path_parts1['dirname'] . '/' . $path_parts1['filename'];

        $path_parts2 = pathinfo($second);
        $secondfileName = $path_parts2['dirname'] . '/' . $path_parts2['filename'];

        file_put_contents($firstfileName . $fileExt, Tools::file_get_contents($secondfileName . $fileExt), FILE_APPEND);

        $firstOffsets = json_decode(Tools::file_get_contents($firstfileName . $offsetsExt), true);
        $secondOffsets = json_decode(Tools::file_get_contents($secondfileName . $offsetsExt), true);
        array_shift($secondOffsets);
        file_put_contents($firstfileName . $offsetsExt, json_encode(array_merge($firstOffsets, $secondOffsets)));

        return true;
    }
}
