<?php
/**
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to a commercial license from SARL Ether Creation
 * Use, copy, modification or distribution of this source file without written
 * license agreement from the SARL Ether Creation is strictly forbidden.
 * In order to obtain a license, please contact us: contact@ethercreation.com
 * ...........................................................................
 * INFORMATION SUR LA LICENCE D'UTILISATION
 *
 * L'utilisation de ce fichier source est soumise a une licence commerciale
 * concedee par la societe Ether Creation
 * Toute utilisation, reproduction, modification ou distribution du present
 * fichier source sans contrat de licence ecrit de la part de la SARL Ether Creation est
 * expressement interdite.
 * Pour obtenir une licence, veuillez contacter la SARL Ether Creation a l'adresse: contact@ethercreation.com
 * ...........................................................................
 *
 * @author    Ether Creation SARL <contact@ethercreation.com>
 * @copyright 2008-2021 Ether Creation SARL
 * @license   Commercial license
 * International Registered Trademark & Property of Ether Creation SARL
 */

namespace ecicdiscountpro;

use Db;

if (!defined('_PS_VERSION_')) {
    exit();
}

class ImporterReference
{

    public $id_product = 0;
    public $id_product_attribute = 0;
    public $active = 0;

    public function __construct($reference, $id_fournisseur = false, $id_shop = false)
    {
        if (empty($reference)) {
            return;
        }
        $res = self::getProductIdByReference($reference, $id_fournisseur, $id_shop);
        $this->id_product = isset($res['idp']) ? $res['idp'] : 0;
        $this->id_product_attribute = isset($res['idpa']) ? $res['idpa']: 0;
        $this->active = isset($res['act']) ? $res['act']: 0;
    }

    public static function getProductIdByReference($reference, $id_fournisseur = false, $id_shop = false)
    {
        $res1 = Db::getInstance()->getRow(
            'SELECT
                p' . ($id_shop ? 's' : '') . '.id_product as idp,
                pa' . ($id_shop ? 's' : '') . '.id_product_attribute as idpa,
                p' . ($id_shop ? 's' : '') . '.active as act
            FROM ' . _DB_PREFIX_ . 'product_attribute pa
            LEFT JOIN ' . _DB_PREFIX_ . 'product p ON pa.id_product = p.id_product ' .
            ($id_shop ? 'LEFT JOIN ' . _DB_PREFIX_ . 'product_shop ps ON ps.id_product = p.id_product ' : '') .
            ($id_shop ? 'LEFT JOIN ' . _DB_PREFIX_ . 'product_attribute_shop pas ON pas.id_product_attribute = pa.id_product_attribute' : '') . '
            WHERE pa.supplier_reference = "' . pSQL($reference) . '"' .
            ($id_fournisseur ? ' AND p.id_supplier = ' . (int) $id_fournisseur : '') .
            ($id_shop ? ' AND pas.id_shop = ' . (int) $id_shop : '') .
            ($id_shop ? ' AND ps.id_shop = ' . (int) $id_shop : '') . '
            AND pa' . ($id_shop ? 's' : '') . '.id_product_attribute IS NOT NULL'
        );
        if (isset($res1['idpa'])) {
            return $res1;
        }

        $res2 = Db::getInstance()->getRow(
            'SELECT p' . ($id_shop ? 's' : '') . '.id_product as idp, p' . ($id_shop ? 's' : '') . '.active as act
            FROM ' . _DB_PREFIX_ . 'product p ' .
            ($id_shop ? 'LEFT JOIN ' . _DB_PREFIX_ . 'product_shop ps ON ps.id_product = p.id_product ' : '') . '
            WHERE p.supplier_reference = "' . pSQL($reference) . '"' .
            ($id_fournisseur ? ' AND id_supplier = ' . (int) $id_fournisseur : '') .
            ($id_shop ? ' AND ps.id_shop = ' . (int) $id_shop : '') . '
            AND p' . ($id_shop ? 's' : '') . '.id_product IS NOT NULL'
        );
        if (isset($res2['idp'])) {
            return array('idp' => $res2['idp'], 'idpa' => 0, 'act' => $res2['act']);
        }

        return false;
    }

    public static function getIdsBySupplierReference($reference, $id_fournisseur = false)
    {
        $res = Db::getInstance()->getRow(
            'SELECT id_product as idp, id_product_attribute as idpa
            FROM ' . _DB_PREFIX_ . 'product_supplier
            WHERE product_supplier_reference = "' . pSQL($reference) . '"' .
            (!$id_fournisseur ? '' : ' AND id_supplier = ' . (int) $id_fournisseur) .
            ' ORDER BY id_product_attribute DESC'
        );

        return $res ? $res : false;
    }

    public static function getAllProductIdByReference($reference)
    {
        $res = Db::getInstance()->ExecuteS(
            'SELECT `id_product_attribute`
            FROM `' . _DB_PREFIX_ . 'product_attribute`
            WHERE `supplier_reference` = "' . pSQL($reference) . '"'
        );
        if (count($res) > 0) {
            $all = array();
            foreach ($res as $det) {
                $all[] = $det['id_product_attribute'];
            }
            return $all;
        }

        return false;
    }

    public static function getShopProduct($id_shop, $id_product)
    {
        if (version_compare(_PS_VERSION_, '1.5', '<')) {
            return true;
        }
        if (Db::getInstance()->getValue('
            SELECT 1
            FROM `' . _DB_PREFIX_ . 'product_shop`
            WHERE `id_product` = ' . (int) $id_product . '
            AND `id_shop` = ' . (int) $id_shop)) {
            return true;
        }

        return false;
    }
}
