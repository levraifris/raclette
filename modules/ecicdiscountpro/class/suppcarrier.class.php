<?php
/**
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to a commercial license from SARL Ether Creation
 * Use, copy, modification or distribution of this source file without written
 * license agreement from the SARL Ether Creation is strictly forbidden.
 * In order to obtain a license, please contact us: contact@ethercreation.com
 * ...........................................................................
 * INFORMATION SUR LA LICENCE D'UTILISATION
 *
 * L'utilisation de ce fichier source est soumise a une licence commerciale
 * concedee par la societe Ether Creation
 * Toute utilisation, reproduction, modification ou distribution du present
 * fichier source sans contrat de licence ecrit de la part de la SARL Ether Creation est
 * expressement interdite.
 * Pour obtenir une licence, veuillez contacter la SARL Ether Creation a l'adresse: contact@ethercreation.com
 * ...........................................................................
 *
 * @author    Ether Creation SARL <contact@ethercreation.com>
 * @copyright 2008-2021 Ether Creation SARL
 * @license   Commercial license
 * International Registered Trademark & Property of Ether Creation SARL
 */

/*
 * TODO
 * suppression/désactivation PS désactive dans ECI (pas obligatoire mais très pratique)
 * activation dans ECI force l'activation/c dans PS (pour garder le lien)
 */

namespace ecicdiscountpro;

require_once dirname(__FILE__) . '/ecicustomobjectmodel.php';
use Db;
use Shop;
use Tools;

class Suppcarrier extends EciCustomObjectModel
{
    public $tabName = 'Carriers';
    public $controllerExt = 'Suppcarrier';
    public static $definition = array(
        'table' => 'eci_suppcarrier',
        'primary' => 'id_suppcarrier',
        'multilang' => true,
        'fields' => array(
            'name' => array('type' => self::TYPE_STRING, 'db_type' => 'varchar(64)', 'validate' => 'isString', 'size' => 64, 'required' => true),
            'supplier_key' => array('type' => self::TYPE_STRING, 'db_type' => 'varchar(64)', 'validate' => 'isString', 'size' => 64, 'required' => true),
            'delay' => array('type' => self::TYPE_STRING, 'db_type' => 'varchar(512)', 'lang' => true, 'validate' => 'isString', 'size' => 512, 'required' => true),
            'id_carrier' => array('type' => self::TYPE_INT, 'db_type' => 'int', 'validate' => 'isInt', 'required' => true),
            'use_real' => array('type' => self::TYPE_BOOL, 'db_type' => 'int', 'validate' => 'isBool', 'required' => true),
            'use_increment' => array('type' => self::TYPE_BOOL, 'db_type' => 'int', 'validate' => 'isBool', 'required' => true),
            'increment_replaces' => array('type' => self::TYPE_BOOL, 'db_type' => 'int', 'validate' => 'isBool', 'required' => false),
            'increment_value' => array('type' => self::TYPE_FLOAT, 'db_type' => 'decimal(20,6)', 'validate' => 'isFloat'),
            'increment_type' => array('type' => self::TYPE_STRING, 'db_type' => 'enum("percentage", "amount")', 'validate' => 'isGenericName', 'values' => array('percentage', 'amount'), 'default' => 'amount'),
            'fournisseur' => array('type' => self::TYPE_STRING, 'db_type' => 'varchar(50)', 'validate' => 'isGenericName', 'size' => 50, 'required' => true),
            'active' => array('type' => self::TYPE_BOOL, 'db_type' => 'int', 'validate' => 'isBool', 'required' => true)
        )
    );
    public $name;
    public $supplier_key;
    public $delay;
    public $id_carrier;
    public $use_real; // switch : use PS shipping cost (true) or supplier shipping cost (false)
    public $use_increment;
    public $increment_replaces;
    public $increment_value;
    public $increment_type;
    public $fournisseur;
    public $active;

    public function install()
    {
        $reqs = array(
            _DB_PREFIX_ . 'eci_suppcarrier' => array(
                'fournisseur' => '`fournisseur`',
                'id_carrier' => '`id_carrier`',
                'supplier_key' => '`supplier_key`',
                'active' => '`active`',
            ),
            _DB_PREFIX_ . 'eci_suppcarrier_lang' => array(
                'id_suppcarrier' => '`id_suppcarrier`',
                'id_lang' => '`id_lang`'
            )
        );
        $this->addIndexesIfNotExist($reqs);
    }

    public function __construct($id = null, $id_lang = null)
    {
        Shop::addTableAssociation(self::$definition['table'] . '_lang', array('type' => 'lang'));

        parent::__construct($id, $id_lang);
    }

    public static function suppcarrierExists($id_carrier)
    {
        return (bool) Db::getInstance()->getValue(
            'SELECT COUNT(*)
            FROM ' . _DB_PREFIX_ . 'eci_suppcarrier
            WHERE id_carrier = ' . (int) $id_carrier
        );
    }

    public static function getSuppcarrierByIdCarrier($id_carrier)
    {
        return Db::getInstance()->getRow(
            'SELECT e.*, c2.id_carrier as carrier_id
            FROM ' . _DB_PREFIX_ . 'carrier c1
            LEFT JOIN ' . _DB_PREFIX_ . 'carrier c2
            ON c1.id_reference = c2.id_reference
            LEFT JOIN ' . _DB_PREFIX_ . 'carrier c3
            ON c3.id_reference = c1.id_reference
            LEFT JOIN ' . _DB_PREFIX_ . 'eci_suppcarrier e
            ON e.id_carrier = c3.id_carrier
            WHERE c1.id_carrier = ' . (int) $id_carrier . '
            AND c2.deleted = 0
            AND e.id_suppcarrier IS NOT NULL'
        );
    }

    public static function getSuppcarrierByCarrierReference($id_reference, $deleted = false)
    {
        return Db::getInstance()->getRow(
            'SELECT e.*, c2.id_carrier as carrier_id
            FROM ' . _DB_PREFIX_ . 'eci_suppcarrier e
            LEFT JOIN ' . _DB_PREFIX_ . 'carrier c1
            ON c1.id_carrier = e.id_carrier
            LEFT JOIN ' . _DB_PREFIX_ . 'carrier c2
            ON c2.id_reference = c1.id_reference
            WHERE c1.id_reference = ' . (int) $id_reference . '
            AND c2.deleted = ' . (int) $deleted . '
            ORDER BY c2.id_carrier DESC'
        );
    }

    public static function getSuppcarrierBySupplierKey($connecteur, $supplier_key)
    {
        if ($supplier_key && $supplier_key[0] == '/' && $supplier_key[Tools::strlen($supplier_key) - 1] == '/' && false !== preg_match($supplier_key, '')) {
            $supplier_key = trim($supplier_key, '/');
            $suppcarrier = Db::getInstance()->getRow(
                'SELECT *
                FROM ' . _DB_PREFIX_ . 'eci_suppcarrier
                WHERE fournisseur = "' . pSQL($connecteur) . '"
                AND supplier_key RLIKE "' . $supplier_key . '"'
            );
        } else {
            $suppcarrier = Db::getInstance()->getRow(
                'SELECT *
                FROM ' . _DB_PREFIX_ . 'eci_suppcarrier
                WHERE fournisseur = "' . pSQL($connecteur) . '"
                AND supplier_key = "' . pSQL($supplier_key) . '"'
            );
        }

        if (!$suppcarrier) {
            return false;
        }

        return $suppcarrier;
    }

    public static function getSuppcarriers($connecteur = false)
    {
        return Db::getInstance()->executeS(
            'SELECT *
            FROM ' . _DB_PREFIX_ . 'eci_suppcarrier WHERE '
            . ($connecteur ? 'fournisseur = "' . pSQL($connecteur) . '"' : '1')
        );
    }

    public static function updateSuppcarrierIdCarrierByIdCarrier($id_carrier)
    {
        $suppcarrier = self::getSuppcarrierByIdCarrier($id_carrier);
        if (!$suppcarrier) {
            return false;
        }

        Db::getInstance()->update(
            'eci_suppcarrier',
            array(
                'id_carrier' => (int) $suppcarrier['carrier_id']
            ),
            'id_suppcarrier = ' . (int) $suppcarrier['id_suppcarrier']
        );

        return true;
    }

    public static function updateSuppcarrierIdCarrierByCarrierReference($id_reference)
    {
        $suppcarrier = self::getSuppcarrierByCarrierReference($id_reference);
        if (!$suppcarrier) {
            return false;
        }

        Db::getInstance()->update(
            'eci_suppcarrier',
            array(
                'id_carrier' => (int) $suppcarrier['carrier_id']
            ),
            'id_suppcarrier = ' . (int) $suppcarrier['id_suppcarrier']
        );

        return true;
    }
}
