<?php
/**
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to a commercial license from SARL Ether Creation
 * Use, copy, modification or distribution of this source file without written
 * license agreement from the SARL Ether Creation is strictly forbidden.
 * In order to obtain a license, please contact us: contact@ethercreation.com
 * ...........................................................................
 * INFORMATION SUR LA LICENCE D'UTILISATION
 *
 * L'utilisation de ce fichier source est soumise a une licence commerciale
 * concedee par la societe Ether Creation
 * Toute utilisation, reproduction, modification ou distribution du present
 * fichier source sans contrat de licence ecrit de la part de la SARL Ether Creation est
 * expressement interdite.
 * Pour obtenir une licence, veuillez contacter la SARL Ether Creation a l'adresse: contact@ethercreation.com
 * ...........................................................................
 *
 * @author    Ether Creation SARL <contact@ethercreation.com>
 * @copyright 2008-2021 Ether Creation SARL
 * @license   Commercial license
 * International Registered Trademark & Property of Ether Creation SARL
 */

ob_start();

require_once dirname(__FILE__) . '/../../../config/config.inc.php';
require_once dirname(__FILE__) . '/catalog.class.php';
use ecicdiscountpro\Catalog;

$logger = Catalog::logStart(basename(__FILE__, '.php'));

function getUrl($url, $credentials = false)
{
    $header = array();
    $header[0] = "Accept: text/xml,application/xml,application/xhtml+xml,";
    $header[0] .= "text/html;q=0.9,text/plain;q=0.8,image/png,image/jpeg,*/*;q=0.5";
    $header[] = "Cache-Control: max-age=0";
    $header[] = "Connection: keep-alive";
    $header[] = "Keep-Alive: 300";
    $header[] = "Accept-Charset: utf-8,ISO-8859-1;q=0.8,*;q=0.7";
    $header[] = "Accept-Language: fr,en-us,en;q=0.5";
    $header[] = "Pragma: ";

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:49.0) Gecko/20100101 Firefox/49.0');
    curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
    curl_setopt($ch, CURLOPT_REFERER, 'http://www.google.com');
    curl_setopt($ch, CURLOPT_AUTOREFERER, true);
    if ($credentials) {
        curl_setopt($ch, CURLOPT_USERPWD, implode(':', array_values($credentials)));
    }
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_ENCODING, 'gzip,deflate');
    curl_setopt($ch, CURLOPT_COOKIEJAR, dirname(__FILE__).'/../files/cookie.txt');
    curl_setopt($ch, CURLOPT_COOKIEFILE, dirname(__FILE__).'/../files/cookie.txt');
    curl_setopt($ch, CURLOPT_TIMEOUT, 10);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);

    $buffer = curl_exec($ch);
    curl_close($ch);

    return $buffer;
}

function ftpGetUrl($url, $credentials)
{
    $matches = array();
    preg_match('/^ftp\:\/\/(.*?)\/(.*)$/', $url, $matches);
    $ftp_access = explode(':', $matches[1]);
    $ftp_server = reset($ftp_access);
    $ftp_port = $ftp_access[1] ?? 21;
    $remote_file = '/' . $matches[2];

    $conn_id = ftp_connect($ftp_server, (int) $ftp_port);
    ftp_login($conn_id, urldecode($credentials['user']), urldecode($credentials['pswd']));
    ftp_pasv($conn_id, true);
    $tmp = tmpfile();
    $ret = @ftp_fget($conn_id, $tmp, $remote_file, FTP_BINARY, 0);
    ftp_close($conn_id);
    if (!$ret) {
        return false;
    }

    return stream_get_contents($tmp, -1, 0);
}

$imageURL = Catalog::decodeStr(Tools::getValue('url'));
if (empty($imageURL)) {
    exit();
}

//$logger->logInfo($imageURL);

$credentials = false;
$matches = array();
if (preg_match('/(http[s]?|ftp)\:\/\/(.*?)\:(.*)\@(.*)/', $imageURL, $matches)) {
    $protocol = $matches[1];
    $imageURL = $matches[1] . '://' . $matches[4];
    $credentials = array(
        'user' => $matches[2],
        'pswd' => $matches[3]
    );
} elseif (preg_match('/(http[s]?|ftp)\:\/\/(.*)/', $imageURL, $matches)) {
    $protocol = $matches[1];
    $imageURL = $matches[1] . '://' . $matches[2];
}

//$logger->logInfo(var_export($protocol, true));
//$logger->logInfo(var_export($imageURL, true));
//$logger->logInfo(var_export($credentials, true));


$ext = trim(Tools::strtolower(preg_replace('/.*\./', '', $imageURL)));
$type = $ext == 'jpg' ? 'jpeg' : $ext;
$tmpName = 'img'.str_replace('.', '', microtime(true));
$fichierTmp = dirname(__FILE__).'/../files/'.$tmpName.'.'.$type;
$handle = fopen($fichierTmp, 'wb');
switch ($protocol) {
    case 'http':
    case 'https':
        $oldImage = getUrl($imageURL, $credentials);
        break;
    case 'ftp':
        $oldImage = ftpGetUrl($imageURL, $credentials);
        break;
}

fwrite($handle, $oldImage);
fclose($handle);

if (false !== $oldImage) {
    if (strpos($oldImage, '</html>')) {
        $newImage = false;
    } elseif (!Tools::strlen($oldImage)) {
        $newImage = false;
    } else {
        $dims = Db::getInstance()->getRow(
            'SELECT `width`,`height`
            FROM `'._DB_PREFIX_.'image_type`
            WHERE `name`="large_default"'
        );
        $width = $dims['width'];
        $height = $dims['height'];

        list($width_orig, $height_orig) = getimagesize($fichierTmp);
        $ratio_orig = $width_orig/$height_orig;
        if ($width/$height > $ratio_orig) {
            $width = $height * $ratio_orig;
        } else {
            $height = $width / $ratio_orig;
        }
        $newImage = imagecreatetruecolor($width, $height);
        if ('png' == $ext) {
            imagealphablending($newImage, false);
            imagesavealpha($newImage, true);
            $transparent = imagecolorallocatealpha($newImage, 255, 255, 255, 127);
            imagefilledrectangle($newImage, 0, 0, $width, $height, $transparent);
        }
        $imageCreateFromExt = 'imagecreatefrom'.$type;
        $Image_orig = @$imageCreateFromExt($fichierTmp);
        if ($Image_orig) {
            imagecopyresampled($newImage, $Image_orig, 0, 0, 0, 0, $width, $height, $width_orig, $height_orig);
        } else {
            Catalog::logInfo($logger, $imageURL . ' not a ' . $ext . ' image');
            $newImage = false;
        }
    }
}

if (empty($newImage)) {
    if (file_exists(dirname(__FILE__) . '/../views/img/image404.png')) {
        $newImage = imagecreatefrompng(dirname(__FILE__) . '/../views/img/image404.png');
    } else {
        $newImage = imagecreatetruecolor(150, 30);
        $bgc = imagecolorallocate($newImage, 255, 255, 255);
        $tc  = imagecolorallocate($newImage, 0, 0, 0);
        imagefilledrectangle($newImage, 0, 0, 150, 30, $bgc);
        imagestring($newImage, 1, 5, 5, 'No Picture', $tc);
    }
}

@unlink($fichierTmp);

ob_end_clean();

switch ($type) {
    case 'jpg':
        header('Content-Type: image/jpeg');
        break;
    case 'jpeg':
        header('Content-Type: image/jpeg');
        break;
    case 'png':
        header('Content-Type: image/png');
        break;
    default:
        header('Content-Type: image/' . $ext);
}

$imageExt = 'image'.$type;
$imageExt($newImage);
imagedestroy($newImage);
