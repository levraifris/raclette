<?php
/**
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to a commercial license from SARL Ether Creation
 * Use, copy, modification or distribution of this source file without written
 * license agreement from the SARL Ether Creation is strictly forbidden.
 * In order to obtain a license, please contact us: contact@ethercreation.com
 * ...........................................................................
 * INFORMATION SUR LA LICENCE D'UTILISATION
 *
 * L'utilisation de ce fichier source est soumise a une licence commerciale
 * concedee par la societe Ether Creation
 * Toute utilisation, reproduction, modification ou distribution du present
 * fichier source sans contrat de licence ecrit de la part de la SARL Ether Creation est
 * expressement interdite.
 * Pour obtenir une licence, veuillez contacter la SARL Ether Creation a l'adresse: contact@ethercreation.com
 * ...........................................................................
 *
 * @author    Ether Creation SARL <contact@ethercreation.com>
 * @copyright 2008-2021 Ether Creation SARL
 * @license   Commercial license
 * International Registered Trademark & Property of Ether Creation SARL
 */

namespace ecicdiscountpro;

require_once dirname(__FILE__) . '/catalog.class.php';
use ecicdiscountpro\Catalog;
use Configuration;
use Db;
use Mail;
use Tools;
use Validate;

/**
 * Description of info
 *
 * this class is designed to manage logs
 *
 * @author alec
 */
class Info
{

    public $dir = '';
    public $family;
    public $file;
    public $limit = 10000;
    public $truncate;
    public $onlyDB = false;
    public $onlyCSV = false;
    private $email;
    private $min_severity;
    protected $level_value = array(
        0 => 'DEBUG',
        1 => 'INFO',
        2 => 'WARNING',
        3 => 'ERROR',
    );

    const DEBUG = 0;
    const INFO = 1;
    const WARNING = 2;
    const ERROR = 3;

    public function __construct($family, $truncate = false, $limit = 0)
    {
        $this->family = (string) $family;
        $this->file = $family . '.log';
        $this->truncate = (bool) $truncate;
        if ($this->truncate && is_numeric($limit)) {
            $this->limit = (int) $limit;
        } else {
            $this->truncate = false;
        }
        $this->email = Catalog::getECIConfigValue('logs_email', 0, Tools::substr(__NAMESPACE__, 3));
        $this->min_severity = (int) Catalog::getECIConfigValue('min_severity', 0, Tools::substr(__NAMESPACE__, 3)) - 1;
    }

    public function logDebug($text)
    {
        return $this->_writeFile($text, self::DEBUG);
    }

    public function logInfo($text)
    {
        if ($this->onlyDB) {
            return $this->_writeDb($text, self::INFO, 1);
        }

        return $this->_writeFile($text, self::INFO);
    }

    public function logWarning($text)
    {
        if ($this->onlyDB) {
            return $this->_writeDb($text, self::WARNING, 1);
        }

        return $this->_writeFile($text, self::WARNING);
    }

    public function logError($text)
    {
        if ($this->onlyDB) {
            return $this->_writeDb($text, self::ERROR, 1);
        }

        return $this->_writeFile($text, self::ERROR);
    }

    private function _writeDb($text, $severity = self::INFO, $errc = 1)
    {
        $now = date('Y-m-d H:i:s');

        if ($severity >= $this->min_severity) {
            $this->logSendMail(
                '*' . $this->level_value[$severity] . '*' . "\t" . $now . ': '
                . __NAMESPACE__ . ' ' . $this->family . ' : '
                . (is_string($text) ? $text : var_export($text, true)) . "\n"
            );
        }

        return Db::getInstance()->insert(
            'log',
            array(
            'severity' => (int) $severity,
            'error_code' => (int) $errc,
            'message' => pSQL(__NAMESPACE__ . ' ' . (is_string($text) ? $text : var_export($text, true))),
            'object_type' => pSQL($this->family),
            'id_employee' => (int) 0,
            'date_add' => $now,
            'date_upd' => $now
            )
        );
    }

    private function _writeFile($text, $severity = self::INFO)
    {
        $now = date('Y-m-d H:i:s');

        if ($severity >= $this->min_severity) {
            $this->logSendMail(
                '*' . $this->level_value[$severity] . '*' . "\t" . $now . ': '
                . __NAMESPACE__ . ' ' . $this->family . ' : '
                . (is_string($text) ? $text : var_export($text, true)) . "\n"
            );
        }

        if ($this->onlyCSV) {
            return $this->_writeCSV($text, $severity);
        }

        return (bool) file_put_contents(
            $this->dir . $this->file,
            '*' . $this->level_value[$severity] . '*' . "\t" . $now . ': '
            . __NAMESPACE__ . ' '
            . (is_string($text) ? $text : var_export($text, true)) . "\n",
            FILE_APPEND
        );
    }

    private function _writeCSV($text, $severity = self::INFO)
    {
        $now = date('Y-m-d H:i:s');

        return (bool) file_put_contents(
            $this->dir . $this->family . '.csv',
            $this->level_value[$severity] . ';' . $now . ';' . (is_array($text) ? implode(';', $text) : $text) . "\n",
            FILE_APPEND
        );
    }

    public function logTruncate()
    {
        $new_f = fopen($this->dir . $this->file . '.new', 'w');
        $old_f = fopen($this->dir . $this->file, 'r');
        $nb = 0;
        while (fgets($old_f) !== false) {
            $nb++;
        }
        rewind($old_f);
        $keep = $this->limit < $nb ? $this->limit : $nb - 1;
        $i = 0;
        while (($line = fgets($old_f)) !== false) {
            if ($i >= ($nb - $keep)) {
                fwrite($new_f, $line);
            }
            $i++;
        }
        fclose($new_f);
        fclose($old_f);
        rename($this->dir . $this->file . '.new', $this->dir . $this->file);
    }

    public function __destruct()
    {
        if ($this->truncate && is_numeric($this->limit)) {
            $this->logTruncate();
        }
    }

    public function logDelete($DB_before = null)
    {
        if ($this->onlyDB) {
            return Db::getInstance()->delete(
                'log',
                'message LIKE "' . pSQL(__NAMESPACE__) . ' %" AND object_type = "' . pSQL($this->family) . '"'
                . (is_null($DB_before) ? '' : ' AND date_add < "' . pSQL($DB_before) . '"')
            );
        }

        $oldName = $this->dir . ($this->onlyCSV ? $this->family . '.csv' : $this->file);
        if (file_exists($oldName)) {
            return @unlink($oldName);
        }

        return true;
    }

    public function logArchive()
    {
        if ($this->onlyDB) {
            return;
        }

        $oldName = $this->dir . $this->file;
        $newName = preg_replace('/\.log$/', date('_YmdHis') . '.log', $oldName);

        return @rename($oldName, $newName);
    }

    public function logSendMail($text)
    {
        //send mail for this log if the configuration is ok (email and value of severity)
        if (Validate::isEmail($this->email)) {
            Mail::Send(
                (int) Configuration::get('PS_LANG_DEFAULT'),
                'log_alert',
                Mail::l('Log: You have a new alert from your shop', (int) Configuration::get('PS_LANG_DEFAULT')),
                array('{texte}' => $text),
                $this->email
            );
        }
    }
}
