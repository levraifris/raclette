<?php
/**
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to a commercial license from SARL Ether Creation
 * Use, copy, modification or distribution of this source file without written
 * license agreement from the SARL Ether Creation is strictly forbidden.
 * In order to obtain a license, please contact us: contact@ethercreation.com
 * ...........................................................................
 * INFORMATION SUR LA LICENCE D'UTILISATION
 *
 * L'utilisation de ce fichier source est soumise a une licence commerciale
 * concedee par la societe Ether Creation
 * Toute utilisation, reproduction, modification ou distribution du present
 * fichier source sans contrat de licence ecrit de la part de la SARL Ether Creation est
 * expressement interdite.
 * Pour obtenir une licence, veuillez contacter la SARL Ether Creation a l'adresse: contact@ethercreation.com
 * ...........................................................................
 *
 * @author    Ether Creation SARL <contact@ethercreation.com>
 * @copyright 2008-2021 Ether Creation SARL
 * @license   Commercial license
 * International Registered Trademark & Property of Ether Creation SARL
 */

namespace ecicdiscountpro;

require_once dirname(__FILE__) . '/catalog.class.php';
use Configuration;
use Context;
use Db;
use Employee;
use Exception;
use Feature;
use FeatureValue;
use Product;
use Shop;
use StockAvailable;
use Tools;
use Validate;

class ApiMethodes
{
    
    public static function setProductToCatalog($eciProduct, $html_fields = null, $connecteur = null)
    {
        //verif du contenu
        if (!is_array($eciProduct)) {
            throw new Exception('no array provided');
        }
        if (empty($eciProduct['product_reference'])) {
            throw new Exception('not enough infos in the array');
        }
        
        $catalog = new Catalog();
        
        //injection dans la table eci_catalog
        $insert_parents = [$catalog->getCleanProductForInsert($eciProduct, $html_fields, $connecteur)];
        
        return Catalog::insertProducts($insert_parents, null, true);
    }

    public static function setCombinationToCatalog($eciCombination, $connecteur = null)
    {
        //verif du contenu
        if (!is_array($eciCombination)) {
            throw new Exception('no array provided');
        }
        $catalog = new Catalog();
        
        //injection dans la table eci_catalog
        $insert_combinations = [$catalog->getCleanCombinationForInsert($eciCombination, $connecteur)];
        
        return Catalog::insertCombinations($insert_combinations, null, true);
    }

    public static function setPackToCatalog($eciPack, $connecteur = null)
    {
        //verif du contenu
        if (!is_array($eciPack)) {
            throw new Exception('no array provided');
        }
        $catalog = new Catalog();
        
        //injection dans la table eci_catalog
        $insert_packs = [$catalog->getCleanPackForInsert($eciPack, $connecteur)];
        
        return Catalog::insertPacks($insert_packs);
    }

    public static function setCatalogMatchings()
    {
        $catalog = new Catalog();
        $connecteur = Catalog::getConnecteurName();
        $id_lang = Configuration::get('PS_LANG_DEFAULT');
        //matchings (créer dans la classe Catalog les fonctions d'insertion globales pour les attributes, features, taxes à partir du catalogue)
        $n = 1000;
        
        //matching des attributs
        $i = 0;
        while (true) {
            $catalog->tabAttributes = [];
            $catalog->tabAttributes_value = [];
            $attributes = Db::getInstance()->executeS(
                'SELECT DISTINCT attribute FROM '._DB_PREFIX_.'eci_catalog_attribute WHERE attribute != "" ORDER BY attribute LIMIT '.(int)$i.','.(int)$n
            );
            if (!$attributes) {
                break;
            }
            foreach ($attributes as $attribute_json) {
                if (is_null($attribute = json_decode($attribute_json['attribute'], true))) {
                    continue;
                }
                foreach ($attribute as $k => $als) {
                    $ta = $als['fr'] ?? reset($als);
                    foreach ($ta as $an => $av) {
                        $catalog->tabAttributes[] = $id_lang . '|' . $an;
                        $catalog->tabAttributes_value[] = $id_lang . '|' . $an . '|' . $av;
                        break;
                    }
                }
            }
            unset($attributes);
            $catalog->insertAttributes($connecteur);
            $i += $n;
        }
        $catalog->tabAttributes = [];
        $catalog->tabAttributes_value = [];
        $catalog->matchAttributes();
        
        
        //matching des features
        $i = 0;
        while (true) {
            $catalog->tabFeatures = [];
            $catalog->tabFeatures_value = [];
            $features = Db::getInstance()->executeS(
                'SELECT DISTINCT feature FROM '._DB_PREFIX_.'eci_catalog WHERE feature != "" ORDER BY feature LIMIT '.(int)$i.','.(int)$n
            );
            if (!$features) {
                break;
            }
            foreach ($features as $feature_json) {
                if (is_null($feature = json_decode($feature_json['feature'], true))) {
                    continue;
                }
                foreach ($feature as $k => $fls) {
                    $tf = $fls['fr'] ?? reset($fls);
                    foreach ($tf as $fn => $fv) {
                        $catalog->tabFeatures[] = $id_lang . '|' . $fn;
                        $catalog->tabFeatures_value[] = $id_lang . '|' . $fn . '|' . $fv;
                        break;
                    }
                }
            }
            unset($features);
            $catalog->insertFeatures($connecteur);
            $i += $n;
        }
        $catalog->tabFeatures = [];
        $catalog->tabFeatures_value = [];
        $catalog->matchFeatures();
        
        //matching des taxes
        $catalog->tabTVA = [];
        $taxes = Db::getInstance()->executeS(
            'SELECT DISTINCT rate FROM '._DB_PREFIX_.'eci_catalog WHERE rate != "" ORDER BY rate'
        );
        foreach ($taxes as $taxe) {
            $catalog->tabTVA[] = $taxe['rate'];
        }
        unset($taxes);
        $catalog->matchTax();
        $catalog->tabTVA = [];
        
        //deployCategories
        $catalog->deployCategories($connecteur);
        
        return true;
    }

    public static function setProductToShop($eciProductRef)
    {
        //verification
        if (!is_array($eciProductRef)) {
            throw new Exception('no array provided');
        }
        $tab = [];
        $tab['reference'] = $eciProductRef['id'] ?? $eciProductRef['product_reference'] ?? $eciProductRef['reference'] ?? null;
        if (empty($tab['reference'])) {
            throw new Exception('no reference found in input');
        }
        $tab['id_shop'] = empty($eciProductRef['id_shop']) ? Configuration::get('PS_SHOP_DEFAULT') : $eciProductRef['id_shop'];
        $tab['fournisseur'] = empty($eciProductRef['fournisseur']) ? Catalog::getConnecteurName() : $eciProductRef['fournisseur'];
        self::setPSContext($tab['id_shop']);
        $catalog = new Catalog();
        
        //setProductToShop
        if (!$catalog->setProductToShop($tab)) {
            //ici on devrait peut-être supprimer toute trace de ce qui a quand-même été fait car cela peut empêcher de réparer automatiquement
            return false;
        }
        
        //récupérer les ids et setCrossIds pour le produit, les déclinaisons, les catégories, ...
        $eciProductInfos = Catalog::getProduct($tab['reference'], $tab['fournisseur']);
        if (!empty($id_product = $eciProductInfos['prestashop']['produit'][(int)$tab['id_shop']]['id_product'])) {
            self::setCrossIds($id_product, $tab['reference'], 'Product', $tab['id_shop']);
            //rechercher les catégories pour les matcher aussi
        }
        if (!empty($combinations = $eciProductInfos['prestashop']['produit'][(int)$tab['id_shop']])) {
            foreach ($combinations as $combination) {
                if (!empty($combination['reference']) && !empty($combination['id_product_attribute'])) {
                    self::setCrossIds($combination['id_product_attribute'], $combination['reference'], 'Combination', $tab['id_shop']);
                    //rechercher les attributs et les features pour les macher aussi
                }
            }
        }
        
        return true;
    }

    public static function setProduct($extproduct)
    {
        $id_shop = empty($extproduct['id_shop']) ? Configuration::get('PS_SHOP_DEFAULT') : $extproduct['id_shop'];
        self::setPSContext($id_shop);
        
        if (empty($extproduct['id'])) {
            throw new Exception('No external id provided');
        }

        $id_product = self::getPSId($extproduct['id'], 'Product', $id_shop);
        if ($id_product) {
            $product = new Product($id_product);
        } else {
            $product = new Product();
        }
        
        self::setObjectProperties($product, 'Product', $extproduct);
        $ret = $product->save();
        
        if (!$id_product) {
            self::setCrossIds($id_product, $extproduct['id'], 'Product', $id_shop);
        }
        
        return $ret;
    }
    
    public static function deleteProduct($extproduct)
    {
        $id_shop = empty($extproduct['id_shop']) ? Configuration::get('PS_SHOP_DEFAULT') : $extproduct['id_shop'];
        self::setPSContext($id_shop);

        if (empty($extproduct['id'])) {
            throw new Exception('No external id provided');
        }

        $id_product = self::getPSId($extproduct['id'], 'Product', $id_shop);
        if (!$id_product) {
            throw new Exception('No matching product to delete');
        }
            
        $product = new Product($id_product);
        $ret = $product->delete();
        
        self::delCrossIds($id_product, $extproduct['id'], 'Product', $id_shop);
        
        return $ret;
    }
    
    public static function setFeature($extfeature)
    {
        $id_shop = empty($extfeature['id_shop']) ? Configuration::get('PS_SHOP_DEFAULT') : $extfeature['id_shop'];
        self::setPSContext($id_shop);
        
        if (empty($extfeature['id'])) {
            throw new Exception('No external id provided');
        }

        $id_feature = self::getPSId($extfeature['id'], 'Feature', $id_shop);
        if ($id_feature) {
            $feature = new Feature($id_feature);
        } else {
            $feature = new Feature();
        }
        
        self::setObjectProperties($feature, 'Feature', $extfeature);
        $ret = $feature->save();
        
        if (!$id_feature) {
            self::setCrossIds($id_feature, $extfeature['id'], 'Feature', $id_shop);
        }
        
        return $ret;
    }
    
    public static function deleteFeature($extfeature)
    {
        $id_shop = empty($extfeature['id_shop']) ? Configuration::get('PS_SHOP_DEFAULT') : $extfeature['id_shop'];
        self::setPSContext($id_shop);

        if (empty($extfeature['id'])) {
            throw new Exception('No external id provided');
        }

        $id_feature = self::getPSId($extfeature['id'], 'Feature', $id_shop);
        if (!$id_feature) {
            throw new Exception('No matching product to delete');
        }
            
        $feature = new Product($id_feature);
        $ret = $feature->delete();
        
        self::delCrossIds($id_feature, $extfeature['id'], 'Feature', $id_shop);
        
        return $ret;
    }
    
    public static function setFeatureValue($extfeatureValue)
    {
        $id_shop = empty($extfeatureValue['id_shop']) ? Configuration::get('PS_SHOP_DEFAULT') : $extfeatureValue['id_shop'];
        self::setPSContext($id_shop);
        
        if (empty($extfeatureValue['id'])) {
            throw new Exception('No external id provided');
        }
        if (empty($extfeatureValue['id_feature'])) {
            throw new Exception('No external id_feature provided');
        }
        
        $id_feature = self::getPSId($extfeatureValue['id_feature'], 'Feature', $id_shop);
        if (!$id_feature) {
            throw new Exception('No matched parent found');
        }
        
        $id_feature_value = self::getPSId($extfeatureValue['id'], 'FeatureValue', $id_shop);
        if ($id_feature_value) {
            $feature_value = new FeatureValue($id_feature_value);
        } else {
            $feature_value = new FeatureValue();
        }
        
        self::setObjectProperties($feature_value, 'FeatureValue', $extfeatureValue);
        $feature_value->id_feature = $id_feature;
        $ret = $feature_value->save();
        
        if (!$id_feature_value) {
            self::setCrossIds($id_feature_value, $extfeatureValue['id'], 'FeatureValue', $id_shop);
        }
        
        return $ret;
    }
    
    private static function setObjectProperties($psobject, $psclass, $extobject)
    {
        $class = Tools::ucfirst(Tools::strtolower($psclass));
        $defs = $class::$definition;
        foreach ($extobject as $prop => $val) {
            if (!property_exists($class, $prop) || !isset($defs['fields'][$prop])) {
                continue;
            }
            if ('' !== $val && isset($defs['fields'][$prop]['validate'])) {
                $valfunc = $defs['fields'][$prop]['validate'];
                if (!empty($defs['fields'][$prop]['lang']) && is_array($val)) {
                    $val = Catalog::getMultiLangField($val);
                    $size = $defs['fields'][$prop]['size'] ?? 32;
                    foreach ($val as $id => $v) {
                        if (!Validate::$valfunc($v)) {
                            unset($val[$id]);
                        }
                    }
                } else {
                    if (!Validate::$valfunc($val)) {
                        throw new Exception('Invalid value for property : ' . $prop . ' => "' . $val . '"');
                    }
                }
            }
            $psobject->$prop = $val;
        }
        
        return true;
    }
    
    public static function setProductStock($id_ext, $object, $qt = 0, $id_shop = null)
    {
        $id_shop = $id_shop ?? Configuration::get('PS_SHOP_DEFAULT');
        if (!($id_product = self::getPSId($id_ext, $object, $id_shop))) {
            throw new Exception('No matching '.$object.' found');
        }
        self::setPSContext($id_shop);

        if ('Product' === $object) {
            StockAvailable::setQuantity($id_product, 0, (int)$qt, $id_shop, false);
        } elseif ('Combination' === $object) {
            $id_product_attribute = $id_product;
            $id_product = Db::getInstance()->getValue('SELECT id_product FROM ' . _DB_PREFIX_ . 'product_attribute WHERE id_product_attribute = ' . (int) $id_product_attribute);
            StockAvailable::setQuantity($id_product, $id_product_attribute, (int)$qt, $id_shop, false);
        } else {
            throw new Exception('Wrong type of object');
        }
        
        return true;
    }
    
    private static function setPSContext($id_shop)
    {
        Shop::setContext(Shop::CONTEXT_SHOP, (int) $id_shop);
        $context = Context::getContext();
        $context->shop = new Shop($id_shop, null, $id_shop);
        $context->employee = new Employee(Catalog::getEciEmployeeId());
    }
    
    private static function getPSId($id_ext, $object, $id_shop = null, $fournisseur = null)
    {
        return Db::getInstance()->getValue(
            'SELECT id_ps
            FROM '._DB_PREFIX_.'eci_apicrossid
            WHERE id_ext = "' . pSQL($id_ext) . '"
            AND object = "' . pSQL($object) . '"
            AND id_shop = ' . (int) ($id_shop ?? Configuration::get('PS_SHOP_DEFAULT')) . '
            AND fournisseur = "' . pSQL(($fournisseur ?? Catalog::getConnecteurName())) . '"'
        );
    }
    
    private static function setCrossIds($id_ps, $id_ext, $object, $id_shop = null, $fournisseur = null)
    {
        Db::getInstance()->insert(
            'eci_apicrossid',
            [
                'id_ps' => (int) $id_ps,
                'id_ext' => pSQL($id_ext),
                'object' => pSQL($object),
                'id_shop' => (int) ($id_shop ?? Configuration::get('PS_SHOP_DEFAULT')),
                'fournisseur' => pSQL(($fournisseur ?? Catalog::getConnecteurName())),
            ],
            false,
            false,
            Db::INSERT_IGNORE
        );
    }
    
    private static function delCrossIds($id_ps, $id_ext, $object, $id_shop = null, $fournisseur = null)
    {
        Db::getInstance()->delete(
            'eci_apicrossid',
            'id_ps = ' . (int) $id_ps
            . ' AND id_ext = ' . (int) $id_ext
            . ' AND object = "' . pSQL($object)
            . '" AND id_shop = ' . (int) ($id_shop ?? Configuration::get('PS_SHOP_DEFAULT'))
            . ' AND fournisseur = "' . pSQL(($fournisseur ?? Catalog::getConnecteurName())) . '"'
        );
    }
}
