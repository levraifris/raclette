<?php
/**
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to a commercial license from SARL Ether Creation
 * Use, copy, modification or distribution of this source file without written
 * license agreement from the SARL Ether Creation is strictly forbidden.
 * In order to obtain a license, please contact us: contact@ethercreation.com
 * ...........................................................................
 * INFORMATION SUR LA LICENCE D'UTILISATION
 *
 * L'utilisation de ce fichier source est soumise a une licence commerciale
 * concedee par la societe Ether Creation
 * Toute utilisation, reproduction, modification ou distribution du present
 * fichier source sans contrat de licence ecrit de la part de la SARL Ether Creation est
 * expressement interdite.
 * Pour obtenir une licence, veuillez contacter la SARL Ether Creation a l'adresse: contact@ethercreation.com
 * ...........................................................................
 *
 * @author    Ether Creation SARL <contact@ethercreation.com>
 * @copyright 2008-2021 Ether Creation SARL
 * @license   Commercial license
 * International Registered Trademark & Property of Ether Creation SARL
 */

class Jsonreader
{

    const CHUNK_SIZE = 64;
    const BEGIN_ARRAY = '[';
    const END_ARRAY = ']';
    const BEGIN_OBJ = '{';
    const END_OBJ = '}';
    const OBJ_SEP = ',';
    const ASSOC_SEP = ':';
    const STRING_MARK = '"';

    /**
     * path
     * @var string The filename being read
     * @access public
     */
    public $path = '';

    /**
     * assoc
     * @var bool json_decode to object (default = false) or assoc (true)
     * @access public
     */
    public $assoc = false;

    /**
     * depth
     * @var integer depth of json_decode recursion (default = 512)
     * @access public
     */
    public $depth = 512;

    /**
     * options
     * @var integer The options of json_decode
     * @access public
     */
    public $options = 0;

    /**
     * pointer
     * @var integer The current position the file is being read from
     * @access private
     */
    private $pointer = 0;

    /**
     * handle
     * @var resource The fopen() resource
     * @access private
     */
    private $handle = null;

    /**
     * buffer
     * @var string content of the current chunk
     * @access private
     */
    private $buffer = '';

    /**
     * mult
     * @var integer multiplier of chunk size to meet object size
     * @access public
     */
    public $mult = 1;

    /**
     * __construct
     * Builds the Chunk object
     *
     * @param string $path The filename to work with
     * @param bool $assoc whether json will be array or object (json_decode 2nd parameter)
     * @param int $depth max depth of json (json_decode 3rd parameter)
     * @param int $options the options of json_decode (json_decode 4th parameter)
     */
    public function __construct($path, $assoc = false, $depth = 512, $options = 0)
    {
        // verify the path
        if (!file_exists($path)) {
            throw new Exception('File does not exist : ' . $path);
        }
        $this->path = $path;

        // open the file
        $this->handle = fopen($this->path, 'rb');

        // check the file opened successfully
        if (!is_resource($this->handle)) {
            throw new Exception('Error opening file for reading');
        }

        $this->assoc = (bool) $assoc;
        $this->depth = (int) $depth;
        $this->options = (int) $options;
    }

    /**
     * __destruct
     * Cleans up
     *
     * @return void
     */
    public function __destruct()
    {
        // close the file resource
        if (is_resource($this->handle)) {
            fclose($this->handle);
        }
    }

    /**
     * rawRead
     * return the next available string representing array/object
     *
     * @return json strin representing array/object
     */
    public function rawRead()
    {
        if (@feof($this->handle)) {
            return false;
        }

        if (false === $this->__stream()) {
            return false;
        }

        // if $pointer is 0, pass the first occurence of [
        $first_char = Tools::substr($this->buffer, 0, 1);
        if (!$this->pointer && self::BEGIN_ARRAY === $first_char) {
            $this->pointer += strlen(self::BEGIN_ARRAY); //we want bytes, not chars
            if (false === $this->__stream()) {
                return false;
            }
        } elseif (self::OBJ_SEP === $first_char) {
            $this->pointer += strlen(self::OBJ_SEP); //we want bytes, not chars
            if (false === $this->__stream()) {
                return false;
            }
        }
        if ($this->pointer && self::END_ARRAY === $first_char) {
            $this->pointer += strlen(self::END_ARRAY); //we want bytes, not chars
            return false;
        }

        while (is_null($end = $this->__seekEnd())) {
            if (false === $end) {
                return false;
            }
            $this->mult++;
            $this->__stream();
        }
        $json_string = Tools::substr($this->buffer, 0, $end);
        $json_length = strlen($json_string); //we want bytes, not chars
        $this->pointer += $json_length;
        $this->mult = 1 + (int) ($json_length / self::CHUNK_SIZE);

        return $json_string;
    }

    /**
     * read
     * return the next available array/object
     *
     * @return array/object
     */
    public function read()
    {

        if (false === ($json_string = $this->rawRead())) {
            return false;
        }

        $json_object = Tools::jsonDecode($json_string, $this->assoc, $this->depth, $this->options);
        return is_null($json_object) ? false : $json_object;
    }

    /**
     *  return chunk beginning at pointer
     */
    private function __stream()
    {
        @fseek($this->handle, $this->pointer);
        $this->buffer = @fread($this->handle, $this->mult * self::CHUNK_SIZE);
        if (false === $this->buffer || ('' === $this->buffer && @feof($this->handle))) {
            return false;
        }

        return true;
    }

    /**
     *  return the position of the end of the current object if in this chunk
     */
    private function __seekEnd()
    {
        $chars = preg_split('//u', $this->buffer, -1, PREG_SPLIT_NO_EMPTY);
        switch ($chars[0]) {
            case self::BEGIN_ARRAY:
                $plus = self::BEGIN_ARRAY;
                $moins = self::END_ARRAY;
                break;
            case self::BEGIN_OBJ:
                $plus = self::BEGIN_OBJ;
                $moins = self::END_OBJ;
                break;
            case self::STRING_MARK:
                $plus = '';
                $moins = self::STRING_MARK;
                break;
            default:
                if (is_numeric($chars[0]) || '-' === $chars[0]) {
                    $plus = '';
                    $moins = self::OBJ_SEP;
                } else {
                    return false;
                }
        }

        $cnt = 0;
        foreach ($chars as $idx => $char) {
            if (!$idx) {
                $cnt++;
                continue;
            }
            if ($plus === $char) {
                $cnt++;
            } elseif ($moins === $char) {
                $cnt--;
            } elseif (self::OBJ_SEP === $moins && self::END_ARRAY === $char) {
                $cnt--;
            }
            if (!$cnt) {
                return self::OBJ_SEP === $moins ? $idx : ($idx + 1);
            }
        }

        return null;
    }

    public function toZero()
    {
        rewind($this->handle);
        $this->pointer = 0;
        $this->buffer = '';
        $this->mult = 1;
    }
}
