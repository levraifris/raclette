<?php
/**
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to a commercial license from SARL Ether Creation
 * Use, copy, modification or distribution of this source file without written
 * license agreement from the SARL Ether Creation is strictly forbidden.
 * In order to obtain a license, please contact us: contact@ethercreation.com
 * ...........................................................................
 * INFORMATION SUR LA LICENCE D'UTILISATION
 *
 * L'utilisation de ce fichier source est soumise a une licence commerciale
 * concedee par la societe Ether Creation
 * Toute utilisation, reproduction, modification ou distribution du present
 * fichier source sans contrat de licence ecrit de la part de la SARL Ether Creation est
 * expressement interdite.
 * Pour obtenir une licence, veuillez contacter la SARL Ether Creation a l'adresse: contact@ethercreation.com
 * ...........................................................................
 *
 * @author    Ether Creation SARL <contact@ethercreation.com>
 * @copyright 2008-2021 Ether Creation SARL
 * @license   Commercial license
 * International Registered Trademark & Property of Ether Creation SARL
 */

namespace ecisifam;

require_once dirname(__FILE__).'/catalog.class.php';
use Db;
use Exception;
use Tools;
use XMLReader;

class DbFile
{
    const EXACT = 0;
    const IN = 1;
    const REGEX = 2;
    const CI = 4;
    const NOT = 8;

    const CHUNK_SIZE = 64;
    const BEGIN_ARRAY = '[';
    const END_ARRAY = ']';
    const BEGIN_OBJ = '{';
    const END_OBJ = '}';
    const OBJ_SEP = ',';
    const ASSOC_SEP = ':';
    const STRING_MARK = '"';

    const PREFIX = _DB_PREFIX_ . 'eci_file_';

    const INSERT_SIZE = 100;

    private $file;
    private $table;
    private $data;

    public $nLines;
    public $currentLine;
    public $nextLine;
    public $indexes = array();
    public $autoIndex = true;
    public $decode = true;
    public $keys = array();
    public $types = array();

/**
 * à partir de la bigjson
 * le principe est de générer des tables
 * - à partir de flux csv, xml, json
 * - à la volée
 * - avec le minimum d'infos de base
 * - peu de formats de champs utilisés : int, varchar(255), text
 * - éventuellement avec indexes sur int et varchar(255)
 * - on y ajoute un autoincrément ?
 * - les contenus tableaux sont en json
 *
 *
 * Il faut des fonctions
 * - d'insertion avec reprise
 * - d'optimisation de type de données
 * - de recherche
 * - de dédoublonnage
 * - de fusion par ajout de lignes
 * - de fusion par ajout de colonnes sur x critères
 * - de réinitialisation d'autoincrément (SET @num := 0;UPDATE a_test SET id = @num := (@num+1);ALTER TABLE a_test AUTO_INCREMENT =1;)
 *
 *
 */

    public function __construct($fileName, $decode = true)
    {
        $this->decode = (bool) $decode;
        $this->file = trim($fileName);
        if (false !== strpos($this->file, ' ')) {
            throw new Exception('This cannot be the name of a dbfile "' . $this->file . '"', 1);
        }
        $this->table = self::getTableName($this->file);
        try {
            $this->nLines = Db::getInstance()->getValue('SELECT COUNT(*) FROM ' . $this->table);
        } catch (Exception $e) {
            throw new Exception('Data table is missing for ' . $this->file, 2);
        }
        $this->currentLine = 1;
        $this->nextLine = 1;
        $this->getFieldsInfo();
        $this->getIndexes();

        return $this;
    }

    public static function getTableName($fileName)
    {
        if (false !== strpos($fileName, ' ')) {
            return false;
        }

        if (0 !== strpos($fileName, static::PREFIX)) {
            return static::PREFIX . $fileName;
        }

        return $fileName;
    }

    public function read()
    {
        if ($this->nextLine > 0 && $this->nextLine <= $this->nLines) {
            $this->currentLine = $this->nextLine;
            $this->nextLine++;
            return $this->get($this->currentLine);
        }

        return false;
    }

    public function go($nLine)
    {
        if (!is_int($nLine) || $nLine > $this->nLines) {
            return false;
        }

        $this->currentLine = $this->nextLine = max(array(1, (int) $nLine));

        return true;
    }

    public function get($nLine)
    {
        if ($nLine > $this->nLines) {
            return false;
        }

        $json = Db::getInstance()->getRow('SELECT * FROM ' . $this->table . ' WHERE id = ' . (int) $nLine);

        return $this->decode ? self::arJsonDecodeRecur($json) : $json;
    }

    public function resetAI()
    {
        return Db::getInstance()->execute(
            'SET @num := 0;UPDATE '.$this->table.' SET id = @num := (@num+1);ALTER TABLE '.$this->table.' AUTO_INCREMENT = 1;'
        );
    }

    public function dedup($criteres = array(), $resetAI = true)
    {
        if (!$criteres) {
            $criteres = $this->keys;
        }
    }

    public function searchOne($key, $value, $compare = self::EXACT)
    {
        if (!in_array($key, $this->keys)) {
            return false;
        }
        $this->buildIndex($key);

        try {
            $ret = Db::getInstance()->getValue(
                'SELECT id
                FROM ' . $this->table . '
                WHERE ' . self::_compare($key, $value, $compare)
            );
        } catch (Exception $e) {
            return false;
        }

        return $ret;
    }

    public function searchAll($key, $value, $compare = self::EXACT)
    {
        if (!in_array($key, $this->keys)) {
            return false;
        }
        $this->buildIndex($key);

        try {
            $ret = Db::getInstance()->executeS(
                'SELECT id
                FROM ' . $this->table . '
                WHERE ' . self::_compare($key, $value, $compare)
            );
        } catch (Exception $e) {
            return false;
        }

        return is_array($ret) ? array_map('implode', $ret) : $ret;
    }

    public function searchLast($key, $value, $compare = self::EXACT)
    {
        $found = $this->searchAll($key, $value, $compare);

        return $found ? end($found) : false;
    }

    private static function _compare($name, $value, $compare = self::EXACT)
    {
        switch ($compare) {
            case self::IN:
                return '`'.pSQL($name).'` LIKE BINARY "%'.pSQL($value).'%"'; // BINARY ?
            case self::NOT + self::IN:
                return '`'.pSQL($name).'` NOT LIKE BINARY "%'.pSQL($value).'%"'; // BINARY ?
            case self::REGEX:
                return '`'.pSQL($name).'` RLIKE "'.$value.'"';
            case self::NOT + self::REGEX:
                return '`'.pSQL($name).'` NOT RLIKE "'.$value.'"';
            case self::EXACT + self::CI:
                return '`'.pSQL($name).'` LIKE "'.pSQL($value).'"';
            case self::NOT + self::EXACT + self::CI:
                return '`'.pSQL($name).'` NOT LIKE "'.pSQL($value).'"';
            case self::IN + self::CI:
                return '`'.pSQL($name).'` LIKE "%'.pSQL($value).'%"';
            case self::NOT + self::IN + self::CI:
                return '`'.pSQL($name).'` NOT LIKE "%'.pSQL($value).'%"';
            case self::NOT:
                return '`'.pSQL($name).'` NOT LIKE "'.pSQL($value).'"';
            default:
                return '`'.pSQL($name).'` LIKE BINARY "'.$value.'"';
        }

        return false;
    }

    public function getFieldsInfo()
    {
        $desc = Db::getInstance()->executeS('DESC ' . $this->table);

        $this->keys = array_map('reset', $desc);

        $this->types = array();
        foreach ($desc as $infos) {
            $this->types[$infos['Field']] = rtrim($infos['Type'], '(0123456789)');
        }

        return true;
    }

    public function getIndexes()
    {
        $indexes_raw = Db::getInstance()->executeS('SHOW INDEXES FROM '.$this->table);
        $this->indexes = array();
        foreach ($indexes_raw as $index) {
            $this->indexes[] = $index['Column_name'];
        }

        return $this->indexes;
    }

    public function buildIndex($key)
    {
        if (!in_array($key, $this->keys)) {
            return false;
        }
        if (in_array($key, $this->indexes)) {
            return true;
        }

        // il faut créer l'index suivant le type : entier index normal, chaine index(255)
        try {
            $ret = Db::getInstance()->execute(
                'ALTER TABLE `' . $this->table . '` ADD INDEX `'.$key.'` (`'.$key.'`);'
            );
        } catch (Exception $e) {
            return false;
        }

        $this->getIndexes();

        return true;
    }

    public function deleteIndex($key = false)
    {
        if (!$key) {
            foreach ($this->indexes as $index) {
                if ('id' === $index) {
                    continue;
                }
                try {
                    $ret = Db::getInstance()->execute(
                        'ALTER TABLE `' . $this->table . '` DROP KEY `'.$index.'`;'
                    );
                } catch (Exception $e) {
                    continue;
                }
            }
        } else {
            if (!in_array($key, $this->indexes)) {
                return false;
            }
            try {
                $ret = Db::getInstance()->execute(
                    'ALTER TABLE `' . $this->table . '` DROP KEY `'.$key.'`;'
                );
            } catch (Exception $e) {
                return false;
            }
        }

        $this->getIndexes();

        return true;
    }


    /**
     * get values instead of indexes
     * $rfields : array of field names that will be returned
     * $wheres : array of arrays of three parameter (searchAll parameters)
     * return array of associative arrays matching all wheres (joined by AND)
     */
    public function select($rfields, $wheres, $start = null, $size = null, $only_count = false)
    {
        if (!is_array($wheres)) {
            return false;
        }
        if (!is_array($rfields)) {
            $rfields = array($rfields);
        }

        $fields = array_flip($rfields);

        $indexes = array();
        $intersect = false;
        foreach ($wheres as $where) {
            if (!is_array($where) || 2 > count($where) || 3 < count($where)) {
                continue;
            }
            $where[2] = isset($where[2]) ? $where[2] : self::EXACT;

            $indexes = $intersect
                ? array_intersect($indexes, $this->searchAll($where[0], $where[1], (int) $where[2]))
                : $this->searchAll($where[0], $where[1], (int) $where[2]);
            $intersect = true;
        }

        if ($only_count) {
            return count($indexes);
        }

        if (!$indexes) {
            return array();
        }

        sort($indexes);

        $old_decode = $this->decode;
        $this->decode = true;
        $return = array();
        if (!is_null($start) && !is_null($size)) {
            for ($i = (int) $start; $i < ((int) $start + (int) $size); $i++) {
                if ($i >= count($indexes)) {
                    break;
                }
                if (($inter = array_intersect_key($this->get($indexes[$i]), $fields))) {
                    $return[$i] = $inter;
                }
            }
        } elseif (!is_null($start) && is_null($size)) {
            for ($i = (int) $start; $i < count($indexes); $i++) {
                if (($inter = array_intersect_key($this->get($indexes[$i]), $fields))) {
                    $return[$i] = $inter;
                }
            }
        } else {
            foreach ($indexes as $i) {
                if (($inter = array_intersect_key($this->get($i), $fields))) {
                    $return[$i] = $inter;
                }
            }
        }
        $this->decode = $old_decode;

        return $return;
    }

    public static function existsTable($name)
    {
        $table_name = self::getTableName($name);
        if (!$table_name) {
            throw new Exception('This cannot be the name of a dbfile "' . $name . '"', 1);
        }

        if (!Db::getInstance()->executeS('SHOW TABLES LIKE "'.pSQL($table_name).'"')) {
            return (bool) Db::getInstance()->executeS('SHOW TABLES LIKE "'.pSQL(self::getTableName($table_name)).'"');
        }

        return true;
    }

    public static function createTable($name, $fields = array())
    {
        $table_name = self::getTableName($name);
        if (!$table_name) {
            throw new Exception('This cannot be the name of a dbfile "' . $name . '"', 1);
        }

        $sql = 'CREATE TABLE IF NOT EXISTS `' . $table_name . '` (`id` int NOT NULL AUTO_INCREMENT,';
        foreach ($fields as $field) {
            if (is_array($field) && isset($field['name'])) {
                $sql .= '`' . $field['name'] . '` ';
                if (isset($field['type']) && in_array($field['type'], array('int', 'varchar', 'text'))) {
                    $sql .= $field['type'] . ('varchar' == $field['type'] ? '(255) ' : ' ');
                } else {
                    $sql .= 'varchar(255) ';
                }
            } else {
                $sql .= '`' . $field . '` varchar(255) ';
            }
            $sql .= ',';
        }
        $sql .= 'PRIMARY KEY (`id`)) ENGINE = ' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8;';

        try {
            Db::getInstance()->execute($sql);
        } catch (Exception $e) {
            return false;
        }

        return true;
    }

    public static function alterTable($name, $fields = array())
    {
        $table_name = self::getTableName($name);
        if (!$table_name) {
            throw new Exception('This cannot be the name of a dbfile "' . $name . '"', 1);
        }

        //TODO : ajouter des retry pour éventuellement modifier un champ... attention ça va le déplacer
        $sql = '';
        foreach ($fields as $field) {
            $sql .= 'ALTER TABLE `' . $table_name . '` ADD ';
            if (is_array($field) && isset($field['name'])) {
                $sql .= '`' . $field['name'] . '` ';
                if (isset($field['type']) && in_array($field['type'], array('int', 'varchar', 'text'))) {
                    $sql .= $field['type'] . ('varchar' == $field['type'] ? '(255) ' : ' ');
                } else {
                    $sql .= 'varchar(255) ';
                }
            } else {
                $sql .= '`' . $field . '` varchar(255) ';
            }
            $sql .= ';';
        }

        try {
            Db::getInstance()->execute($sql);
        } catch (Exception $e) {
            return false;
        }

        return true;
    }

    public static function dropTable($name)
    {
        $table_name = self::getTableName($name);
        if (!$table_name) {
            throw new Exception('This cannot be the name of a dbfile "' . $name . '"', 1);
        }

        try {
            Db::getInstance()->execute('DROP TABLE IF EXISTS `'.$table_name.'`');
        } catch (Exception $e) {
            return false;
        }

        return true;
    }

    public static function truncateTable($name)
    {
        $table_name = self::getTableName($name);
        if (!$table_name) {
            throw new Exception('This cannot be the name of a dbfile "' . $name . '"', 1);
        }

        if (!self::existsTable($table_name)) {
            return true;
        }

        try {
            Db::getInstance()->execute('TRUNCATE TABLE `'.$table_name.'`');
        } catch (Exception $e) {
            return false;
        }

        return true;
    }

    public static function insertTable($name, $data)
    {
        $table_name = self::getTableName($name);
        if (!$table_name) {
            throw new Exception('This cannot be the name of a dbfile "' . $name . '"', 1);
        }

        try {
            Db::getInstance()->insert(
                $table_name,
                $data,
                false,
                false,
                Db::INSERT,
                false
            );
        } catch (Exception $e) {
            throw new Exception('Insert Error : '.$e->getMessage());
        }

        return true;
    }



    /**
     * general csv (products, customers,...) to dbfile transform
     * $path to the original csv
     * $header = 1:firstline, array:this array
     * $field_sep is the field separator character
     * $enclosure is the field enclosure character
     * $escape is the character escape character
     * $line_endings character ending the line
     * $encoding of the original csv
     * $lcallback is the line exploding callback function in case str_getcsv is not sufficent
     * $fcallback is the field modifying callback function if needed
     * $continue old files or from scratch (old files will be overwritten)
     * $icallback is the array modifying callback function if needed
     */
    public static function csv2dbf($path, $header = 1, $field_sep = ';', $enclosure = '"', $escape = '\\', $line_endings = "\n", $encoding = 'UTF-8', $lcallback = null, $fcallback = null, $continue = false, $icallback = null)
    {
        if (1 !== $header && !is_array($header)) {
            return false;
        }
        $path_parts = pathinfo($path);
        $fileName = $path_parts['filename'];
        if (!$continue && self::existsTable($fileName)) {
            self::dropTable($fileName);
        }
        $just_created = false;
        if (!self::existsTable($fileName)) {
            self::createTable($fileName);
            $just_created = true;
        }
        $handle = fopen($path, 'rb');
        if (is_callable($icallback)) {
            $icallback_parm = null;
        } elseif (!is_null($icallback) && is_array($icallback) && 2 == count($icallback) && is_callable($icallback[0]) && is_array($icallback[1])) {
            $icallback_parm = $icallback[1];
            $icallback = $icallback[0];
        } else {
            $icallback = null;
        }

        $n = 0;
        $t_buffer = array();
        $et = $header === 1 ? array() : $header;
        $added = 0;
        while (($sline = stream_get_line($handle, 8192, $line_endings)) !== false) {
            if ('UTF-8' != Tools::strtoupper($encoding)) {
                $sline = mb_convert_encoding($sline, 'UTF-8', $encoding);
                //$sline = html_entity_decode(htmlentities($sline, ENT_NOQUOTES, $encoding), ENT_NOQUOTES, 'UTF-8');
            }
            if (is_null($lcallback)) {
                $line = str_getcsv($sline, $field_sep, $enclosure, $escape);
            } else {
                $line = call_user_func($lcallback, $sline);
            }
            $n++;
            if (1 === $n && 1 === $header) {
                // first line is supposed to be header
                if (is_null($fcallback)) {
                    $et = $line;
                } else {
                    $et = array_map($fcallback, $line);
                }
                continue;
            } elseif (count($et) !== count($line)) {
                if (count($et) > count($line)) {
                    $line = array_pad($line, count($et), '');
                } else {
                    $line = array_slice($line, 0, count($et));
                }
            }
            if (is_null($fcallback)) {
                $prod = array_combine($et, $line);
            } else {
                $prod = array_combine($et, array_map($fcallback, $line));
            }
            if (!is_null($icallback)) {
                if (is_null($icallback_parm)) {
                    $prod = call_user_func($icallback, $prod);
                } else {
                    $prod = call_user_func_array($icallback, array_merge(array($prod), $icallback_parm));
                }
            }
            if ($just_created && !$added) {
                self::alterTable($fileName, array_keys($prod));
            }
            $t_buffer[] = array_map('pSQL', $prod);
            if (count($t_buffer) >= self::INSERT_SIZE) {
                self::insertTable($fileName, $t_buffer);
                $t_buffer = array();
            }
            $added++;
        }
        fclose($handle);

        if ($t_buffer) {
            self::insertTable($fileName, $t_buffer);
        }

        if (0 === $added) {
            return false;
        }

        return $added;
    }

    /**
     *  specific json object (products, customers,...) to dbfile transform
     */
    public static function jso2dbf($path, $continue = false, $icallback = null)
    {
        $path_parts = pathinfo($path);
        $fileName = $path_parts['filename'];
        if (!$continue && self::existsTable($fileName)) {
            self::dropTable($fileName);
        }
        $just_created = false;
        if (!self::existsTable($fileName)) {
            self::createTable($fileName);
            $just_created = true;
        }
        $handle = fopen($path, 'rb');
        if (is_callable($icallback)) {
            $icallback_parm = null;
        } elseif (!is_null($icallback) && is_array($icallback) && 2 == count($icallback) && is_callable($icallback[0]) && is_array($icallback[1])) {
            $icallback_parm = $icallback[1];
            $icallback = $icallback[0];
        } else {
            $icallback = null;
        }

        $buffer = '';
        $i_buffer = array();
        $t_buffer = array();
        $pos = 0;
        $ob_switch = 0;
        $added = 0;
        while (false !== ($chunk = fread($handle, 8192))) {
            $pointer = 0;
            while (isset($chunk[$pointer])) {
                $char = ord($chunk[$pointer]);
                if ($char < 128) {
                    $str = $chunk[$pointer];
                    $bytes = 1;
                } else {
                    if ($char < 224) {
                        $bytes = 2;
                    } elseif ($char < 240) {
                        $bytes = 3;
                    } elseif ($char < 248) {
                        $bytes = 4;
                    } elseif ($char == 252) {
                        $bytes = 5;
                    } else {
                        $bytes = 6;
                    }
                    $str = substr($chunk, $pointer, $bytes); // we want bytes, not characters
                    if (($pointer + $bytes > 8192) || false === ($str = substr($chunk, $pointer, $bytes))) { // we want bytes, not characters
                        break;
                    }
                }
                $pointer += $bytes;
                if (0 === $pos && 0 === $pointer && self::BEGIN_ARRAY === $str) {
                    continue;
                }
                if (self::BEGIN_OBJ === $str) {
                    $ob_switch++;
                }
                if ($ob_switch >= 1) {
                    $buffer .= $str;
                }
                if (self::END_OBJ === $str) {
                    $ob_switch--;
                    if (0 === $ob_switch) {
                        if (!is_null($icallback)) {
                            if (is_null($icallback_parm)) {
                                $i_buffer = call_user_func($icallback, Tools::jsonDecode($buffer, true));
                            } else {
                                $i_buffer = call_user_func_array($icallback, array_merge(array(Tools::jsonDecode($buffer, true)), $icallback_parm));
                            }
                        } else {
                            $i_buffer = json_decode($buffer, true);
                        }
                        if ($just_created && !$added) {
                            self::alterTable($fileName, array_keys($i_buffer));
                        }
                        $t_buffer[] = array_map('pSQL', $i_buffer);
                        if (count($t_buffer) >= self::INSERT_SIZE) {
                            self::insertTable($fileName, $t_buffer);
                            $t_buffer = array();
                        }
                        $buffer = '';
                        $added++;
                    }
                }
            }
            if (feof($handle)) {
                break;
            }
            $pos += $pointer;
            fseek($handle, $pos);
        }
        fclose($handle);

        if ($t_buffer) {
            self::insertTable($fileName, $t_buffer);
        }

        if (0 === $added) {
            return false;
        }

        return $added;
    }

    /**
     *  specific xml object (products, customers,...) to dbfile transform
     */
    public static function xmlo2dbf($path, $tagName, $continue = false, $icallback = null)
    {
        $startPatrn0 = '/^\<(\w+)?:?(?:' . implode('|$)(?:', str_split($tagName)) . '|$)(?:(\ |\>)|$)/';
        $startPatrn1 = '/^\<(\w+:)?' . $tagName . '\ |\>/';
        $endPatrn = '/\<\/(\w+:)?' . $tagName . '\>$/';

        $path_parts = pathinfo($path);
        $fileName = $path_parts['filename'];
        if (!$continue && self::existsTable($fileName)) {
            self::dropTable($fileName);
        }
        $just_created = false;
        if (!self::existsTable($fileName)) {
            self::createTable($fileName);
            $just_created = true;
        }
        $handle = fopen($path, 'rb');
        if (is_callable($icallback)) {
            $icallback_parm = null;
        } elseif (!is_null($icallback) && is_array($icallback) && 2 == count($icallback) && is_callable($icallback[0]) && is_array($icallback[1])) {
            $icallback_parm = $icallback[1];
            $icallback = $icallback[0];
        } else {
            $icallback = null;
        }

        $pos = 0;
        $item = '';
        $t_buffer = array();
        $sw = false;
        $added = 0;
        while ($chunk = fread($handle, 8192)) {
            $pointer = 0;
            while (isset($chunk[$pointer])) {
                $char = ord($chunk[$pointer]);
                if ($char < 128) {
                    $str = $chunk[$pointer];
                    $bytes = 1;
                } else {
                    if ($char < 224) {
                        $bytes = 2;
                    } elseif ($char < 240) {
                        $bytes = 3;
                    } elseif ($char < 248) {
                        $bytes = 4;
                    } elseif ($char == 252) {
                        $bytes = 5;
                    } else {
                        $bytes = 6;
                    }
                    if (($pointer + $bytes > 8192) || false === ($str = substr($chunk, $pointer, $bytes))) { // we want bytes, not characters
                        break;
                    }
                }
                $pointer += $bytes;
                $item .= $str;
                if (!$sw) {
                    if (!preg_match($startPatrn0, $item)) {
                        $item = '';
                    } elseif (preg_match($startPatrn1, $item)) {
                        $sw = true;
                    }
                }
                if ($sw && preg_match($endPatrn, $item)) {
                    $sitem = preg_replace(
                        array('#(</?)\w+:([^>]+>)#', '# \w+:(\w*=".*?")#'),
                        array('$1$2', ' $1'),
                        $item
                    );
                    $xitem = simplexml_load_string($sitem, "SimpleXMLElement", LIBXML_COMPACT | LIBXML_NOCDATA);
                    $jitem = Tools::jsonEncode($xitem);
                    if (!is_null($icallback)) {
                        if (is_null($icallback_parm)) {
                            $ujitem = call_user_func($icallback, Tools::jsonDecode($jitem, true));
                        } else {
                            $ujitem = call_user_func_array($icallback, array_merge(array(Tools::jsonDecode($jitem, true)), $icallback_parm));
                        }
                    } else {
                        $ujitem = json_decode($jitem, true);
                    }
                    if ($just_created && !$added) {
                        self::alterTable($fileName, array_keys($ujitem));
                    }
                    $t_buffer[] = array_map('pSQL', $ujitem);
                    if (count($t_buffer) >= self::INSERT_SIZE) {
                        self::insertTable($fileName, $t_buffer);
                        $t_buffer = array();
                    }
                    $item = '';
                    $sw = false;
                    $added++;
                }
            }
            $pos += $pointer;
            fseek($handle, $pos);
        }

        if ($t_buffer) {
            self::insertTable($fileName, $t_buffer);
        }

        if (0 === $added) {
            return false;
        }

        return $added;
    }

    /**
     *  specific xml object (products, customers,...) to dbfile transform
     */
    public static function xmlo2dbfFast($path, $tagName, $continue = false, $icallback = null)
    {
        $path_parts = pathinfo($path);
        $fileName = $path_parts['filename'];
        if (!$continue && self::existsTable($fileName)) {
            self::dropTable($fileName);
        }
        $just_created = false;
        if (!self::existsTable($fileName)) {
            self::createTable($fileName);
            $just_created = true;
        }
        if (is_callable($icallback)) {
            $icallback_parm = null;
        } elseif (!is_null($icallback) && is_array($icallback) && 2 == count($icallback) && is_callable($icallback[0]) && is_array($icallback[1])) {
            $icallback_parm = $icallback[1];
            $icallback = $icallback[0];
        } else {
            $icallback = null;
        }

        $t_buffer = array();
        $xml = new XMLReader();
        $xml->open($path);
        self::seekTag($xml, $tagName);
        $added = 0;
        while ($xml->localName === $tagName) {
            $jitem_o = Tools::jsonEncode(
                simplexml_load_string(
                    $xml->readOuterXML(),
                    "SimpleXMLElement",
                    LIBXML_COMPACT | LIBXML_NOCDATA
                )
            );
            $item = Tools::jsonDecode($jitem_o, true);
            //array_walk($item, function (&$v) {$v = is_array($v)?'':$v;});
            if (!is_null($icallback)) {
                if (is_null($icallback_parm)) {
                    $item = call_user_func($icallback, $item);
                } else {
                    $item = call_user_func_array($icallback, array_merge(array($item), $icallback_parm));
                }
            }
            if ($just_created && !$added) {
                self::alterTable($fileName, array_keys($item));
            }
            $t_buffer[] = array_map('pSQL', $item);
            if (count($t_buffer) >= self::INSERT_SIZE) {
                self::insertTable($fileName, $t_buffer);
                $t_buffer = array();
            }
            self::seekTag($xml, $tagName);
            self::seekTag($xml, $tagName);
            $added++;
        }
        $xml->close();

        if ($t_buffer) {
            self::insertTable($fileName, $t_buffer);
        }

        if (0 === $added) {
            return false;
        }

        return $added;
    }

    public static function seekTag($xml, $tag)
    {
        while ($xml->read() && $tag !== $xml->localName) {
            continue;
        }
    }

    /**
     *  specific array of arrays (e.g. as returned from "select" request) to dbfile format
     */
    public static function ar2dbf($array, $path, $continue = false, $icallback = null)
    {
        $path_parts = pathinfo($path);
        $fileName = $path_parts['filename'];
        if (!$continue && self::existsTable($fileName)) {
            self::dropTable($fileName);
        }
        $just_created = false;
        if (!self::existsTable($fileName)) {
            self::createTable($fileName);
            $just_created = true;
        }
        if (is_callable($icallback)) {
            $icallback_parm = null;
        } elseif (!is_null($icallback) && is_array($icallback) && 2 == count($icallback) && is_callable($icallback[0]) && is_array($icallback[1])) {
            $icallback_parm = $icallback[1];
            $icallback = $icallback[0];
        } else {
            $icallback = null;
        }

        $added = 0;
        $t_buffer = array();
        foreach ($array as $product) {
            if (!is_null($icallback)) {
                if (is_null($icallback_parm)) {
                    $product = call_user_func($icallback, $product);
                } else {
                    $product = call_user_func_array($icallback, array_merge(array($product), $icallback_parm));
                }
            }
            if ($just_created && !$added) {
                self::alterTable($fileName, array_keys($product));
            }
            $t_buffer[] = array_map('pSQL', $product);
            if (count($t_buffer) >= self::INSERT_SIZE) {
                self::insertTable($fileName, $t_buffer);
                $t_buffer = array();
            }
            $added++;
        }

        if ($t_buffer) {
            self::insertTable($fileName, $t_buffer);
        }

        if (0 === $added) {
            return false;
        }

        return $added;
    }

    /**
     *
     * Ajouter le contenu de la seconde table à la première
     *
     * @param type $first
     * @param type $second
     * @return boolean
     */
    public static function mergebjs($first, $second)
    {
        $fileExt = self::DX;
        $offsetsExt = self::IX;

        $path_parts1 = pathinfo($first);
        $firstfileName = $path_parts1['dirname'] . '/' . $path_parts1['filename'];

        $path_parts2 = pathinfo($second);
        $secondfileName = $path_parts2['dirname'] . '/' . $path_parts2['filename'];

        file_put_contents($firstfileName . $fileExt, Tools::file_get_contents($secondfileName . $fileExt), FILE_APPEND);

        $firstOffsets = Tools::jsonDecode(Tools::file_get_contents($firstfileName . $offsetsExt), true);
        $secondOffsets = Tools::jsonDecode(Tools::file_get_contents($secondfileName . $offsetsExt), true);
        array_shift($secondOffsets);
        file_put_contents($firstfileName . $offsetsExt, Tools::jsonEncode(array_merge($firstOffsets, $secondOffsets)));

        return true;
    }

    public static function arJsonDecodeRecur($a)
    {
        if (!is_array($a) && is_null($a2 = json_decode($a, true))) {
            return $a;
        } elseif (!is_array($a) && is_array($a2)) {
            return self::arJsonDecodeRecur($a2);
        } elseif (!is_array($a)) {
            return $a2;
        }

        foreach ($a as &$v) {
            $v = is_array($v) ? self::arJsonDecodeRecur($v) : (is_null($w = json_decode($v, true)) ? $v : $w);
        }

        return $a;
    }
}
