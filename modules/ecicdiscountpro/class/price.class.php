<?php
/**
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to a commercial license from SARL Ether Creation
 * Use, copy, modification or distribution of this source file without written
 * license agreement from the SARL Ether Creation is strictly forbidden.
 * In order to obtain a license, please contact us: contact@ethercreation.com
 * ...........................................................................
 * INFORMATION SUR LA LICENCE D'UTILISATION
 *
 * L'utilisation de ce fichier source est soumise a une licence commerciale
 * concedee par la societe Ether Creation
 * Toute utilisation, reproduction, modification ou distribution du present
 * fichier source sans contrat de licence ecrit de la part de la SARL Ether Creation est
 * expressement interdite.
 * Pour obtenir une licence, veuillez contacter la SARL Ether Creation a l'adresse: contact@ethercreation.com
 * ...........................................................................
 *
 * @author    Ether Creation SARL <contact@ethercreation.com>
 * @copyright 2008-2021 Ether Creation SARL
 * @license   Commercial license
 * International Registered Trademark & Property of Ether Creation SARL
 */
 
require_once dirname(__FILE__).'/catalog.class.php';
use ecicdiscountpro\Catalog;

if (!defined('_PS_VERSION_')) {
    exit;
}

class ImporterPrice
{

    public function addPriceProduct($id_product = 0, $id_product_attribute = 0, $reference_parent = '', $reference = '', $supplier_name = false, $idShop = 1)
    {
        $id_supplier = $idShop;
        $id_supplier = Supplier::getIdByName($supplier_name);

        if (!$id_product) {
            $listIds = self::getProductIdsFromRefs($reference_parent, $reference, $supplier_name);
            if (!is_array($listIds)) {
                return false;
            }

            list($id_product, $id_product_attribute, $id_supplier) = array_values($listIds);
        }

        if (!$reference_parent) {
            $reference_parent = Db::getInstance()->getValue('
                SELECT supplier_reference
                FROM ' . _DB_PREFIX_ . 'product
                WHERE id_product = "' . (int) $id_product . '"' .
                ($supplier_name ? ' AND id_supplier = ' . (int) Supplier::getIdByName($supplier_name) : ''));
        }

        $lstReducRaw = Db::getInstance()->executeS('
            SELECT *
            FROM ' . _DB_PREFIX_ . 'eci_price_spe
            WHERE reference = "' . pSQL($reference) . '"
            AND reference_parent = "' . pSQL($reference_parent) . '"');

        // optimize : find the smallest 'price' and try this only one
        $lstReduc = $this->calculateBestPrices($lstReducRaw);

        $this->setPriceRules($lstReduc, false, $id_product, $id_product_attribute);
    }

    private function setPriceRules($lstReduc, $multi = false, $id_product = 0, $id_product_attribute = 0)
    {
        $logger = Catalog::logStart('updat_price');

        $oldRefP = '';
        $oldRefA = '';
        $oldFour = '';

        foreach ($lstReduc as $reduc) {
            if (($multi && !($reduc['reference_parent'] == $oldRefP && $reduc['reference'] == $oldRefA && $reduc['fournisseur'] == $oldFour)) ||
                (!$multi && !$id_product)) {
                $listIds = self::getProductIdsFromRefs($reduc['reference_parent'], $reduc['reference'], $reduc['fournisseur']);
                if (!is_array($listIds)) {
                    continue;
                }
                list($id_product, $id_product_attribute, $id_supplier) = array_values($listIds);
            }
            $oldRefP = $reduc['reference_parent'];
            $oldRefA = $reduc['reference'];
            $oldFour = $reduc['fournisseur'];

            $idRP = SpecificPrice::exists($id_product, $id_product_attribute, $reduc['id_shop'], $reduc['id_group'], $reduc['id_country'], $reduc['id_currency'], $reduc['id_customer'], $reduc['quantity'], $reduc['dat_deb'], $reduc['dat_fin']);
            if ($idRP) {
                $specific_price = new SpecificPrice($idRP);
                //calculate price according to old rule
                $old_price = (float) $specific_price->price;
                if ('-1' == $old_price) {
                    $old_price = (float) self::getProductPrice($id_product, $id_product_attribute, $reduc['id_shop']);
                }
                if ('amount' == $specific_price->reduction_type) {
                    $old_price -= (float) $specific_price->reduction;
                } else {
                    $old_price -= (float) ($old_price * $specific_price->reduction);
                }
                //calculate price according to new rule
                $new_price = (float) $reduc['price'];
                if ('-1' == $new_price) {
                    $new_price = (float) self::getProductPrice($id_product, $id_product_attribute, $reduc['id_shop']);
                }
                if ('amount' == $reduc['typ_reduc']) {
                    $new_price -= (float) $reduc['reduc'];
                } else {
                    $new_price -= (float) ($new_price * $reduc['reduc'] / 100);
                }
                // compare prices between the two candidate rules : if the latter is better -> update
                if ($new_price < $old_price) {
                    $specific_price->price = (float) $reduc['price'];
                    $specific_price->reduction_type = (!empty($reduc['typ_reduc']) ? $reduc['typ_reduc'] : 'amount');
                    $specific_price->reduction = ($reduc['typ_reduc'] == 'percentage' ? (float) $reduc['reduc'] / 100 : (float) $reduc['reduc']);
                    try {
                        $specific_price->update();
                    } catch (Exception $e) {
                        Catalog::logInfo($logger, 'Erreur addPriceProduct/update: ' . $e->getMessage() . '; line ' . $e->getLine());
                    }
                }
                Db::getInstance()->delete(
                    'eci_price_spe',
                    'id = ' . (int) $reduc['id']
                );
            } else {
                $specific_price = new SpecificPrice();
                $specific_price->id_product = (int) $id_product;
                $specific_price->id_product_attribute = (int) $id_product_attribute;
                $specific_price->id_customer = (int) $reduc['id_customer'];
                $specific_price->id_shop = (int) $reduc['id_shop'];
                $specific_price->id_country = (int) $reduc['id_country'];
                $specific_price->id_currency = (int) $reduc['id_currency'];
                $specific_price->id_group = (int) $reduc['id_group'];
                $specific_price->id_shop_group = (int) Shop::getGroupFromShop((int) $reduc['id_shop']);
                $specific_price->price = (float) $reduc['price'];
                $specific_price->reduction_type = (!empty($reduc['typ_reduc']) ? $reduc['typ_reduc'] : 'amount');
                $specific_price->reduction = ($reduc['typ_reduc'] == 'percentage' ? (float) $reduc['reduc'] / 100 : (float) $reduc['reduc']);
                $specific_price->from = $reduc['dat_deb'];
                $specific_price->to = $reduc['dat_fin'];
                $specific_price->from_quantity = (int) $reduc['quantity'];
                try {
                    $specific_price->add();
                    Db::getInstance()->delete(
                        'eci_price_spe',
                        'id = ' . (int) $reduc['id']
                    );
                } catch (Exception $e) {
                    Catalog::logInfo($logger, 'Erreur addPriceProduct/add: ' . $e->getMessage() . '; line ' . $e->getLine());
                }
            }
        }
    }

    public function addCustomerPrices($id_customer, $idShop = 1)
    {
        $lstReduc = Db::getInstance()->executeS('
            SELECT *
            FROM ' . _DB_PREFIX_ . 'eci_price_spe
            WHERE id_customer = ' . (int) $id_customer . '
            AND id_shop = ' . (int) $idShop);

        $this->setPriceRules($lstReduc, true);
    }

    public function addGroupPrices($id_group, $idShop = 1)
    {
        $lstReduc = Db::getInstance()->executeS('
            SELECT *
            FROM ' . _DB_PREFIX_ . 'eci_price_spe
            WHERE id_group = ' . (int) $id_group . '
            AND id_shop = ' . (int) $idShop);

        $this->setPriceRules($lstReduc, true);
    }

    public function addProductGroupPrice($ref, $id_group = 0, $idShop = 1)
    {
        $lstReduc = Db::getInstance()->executeS('
            SELECT *
            FROM ' . _DB_PREFIX_ . 'eci_price_spe
            WHERE reference_parent = "' . pSQL($ref)  . '"
            AND id_group = ' . (int) $id_group . '
            AND id_shop = ' . (int) $idShop);

        $this->setPriceRules($lstReduc, true);
    }

    private function calculateBestPrices($rules)
    {
        return $rules;
    }

    public static function getProductIdsFromRefs($reference_parent, $reference = '', $supplier_name = '')
    {
        if (!$supplier_name || !$reference_parent) {
            return false;
        }

        $id_supplier = Supplier::getIdByName($supplier_name);

        if ($reference) {
            return Db::getInstance()->getRow('
                SELECT p.id_product, pa.id_product_attribute, p.id_supplier
                FROM ' . _DB_PREFIX_ . 'product p
                INNER JOIN ' . _DB_PREFIX_ . 'product_attribute pa
                ON p.id_product = pa.id_product
                WHERE p.supplier_reference LIKE "' . pSQL($reference_parent) . '"
                AND pa.supplier_reference LIKE "' . pSQL($reference) . '"
                AND p.id_supplier = ' . (int) $id_supplier);
        } else {
            return Db::getInstance()->getRow('
                SELECT id_product, 0, id_supplier
                FROM ' . _DB_PREFIX_ . 'product
                WHERE supplier_reference = "' . pSQL($reference_parent) . '"
                AND id_supplier = ' . (int) $id_supplier);
        }
    }

    public static function getProductPrice($id_product, $id_product_attribute = 0, $id_shop = 1)
    {
        $produit = new Product($id_product, false, null, $id_shop, null);
        $price = (float) $produit->price;
        if ($id_product_attribute) {
            $combinaisons = $produit->getAttributeCombinationsById($id_product_attribute, Configuration::get('PS_LANG_DEFAULT'));
            foreach ($combinaisons as $combinaison) {
                $price += (float) $combinaison['price'];
            }
        }

        return $price;
    }
}
