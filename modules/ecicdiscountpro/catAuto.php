<?php
/**
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to a commercial license from SARL Ether Creation
 * Use, copy, modification or distribution of this source file without written
 * license agreement from the SARL Ether Creation is strictly forbidden.
 * In order to obtain a license, please contact us: contact@ethercreation.com
 * ...........................................................................
 * INFORMATION SUR LA LICENCE D'UTILISATION
 *
 * L'utilisation de ce fichier source est soumise a une licence commerciale
 * concedee par la societe Ether Creation
 * Toute utilisation, reproduction, modification ou distribution du present
 * fichier source sans contrat de licence ecrit de la part de la SARL Ether Creation est
 * expressement interdite.
 * Pour obtenir une licence, veuillez contacter la SARL Ether Creation a l'adresse: contact@ethercreation.com
 * ...........................................................................
 *
 * @author    Ether Creation SARL <contact@ethercreation.com>
 * @copyright 2008-2021 Ether Creation SARL
 * @license   Commercial license
 * International Registered Trademark & Property of Ether Creation SARL
 */

require_once dirname(__FILE__) . '/../../config/config.inc.php';
require_once dirname(__FILE__) . '/class/catalog.class.php';
require_once dirname(__FILE__) . '/class/reference.class.php';
require_once dirname(__FILE__) . '/class/pricerules.class.php';
require_once dirname(__FILE__) . '/class/ecisuppcarrier.class.php';
use ecicdiscountpro\Catalog;
use ecicdiscountpro\ImporterReference;
use ecicdiscountpro\Pricerules;

ignore_user_abort(true);
set_error_handler("exception_error_handler");

$paramHelp = Tools::getValue('help', null);
if (!is_null($paramHelp)) {
    $help = array(
        'ec_token' => array(
            'fr' => 'Token du module. Obligatoire.',
            'en' => 'Module\'s token. Required.'
            ),
        'connecteur' => array(
            'fr' => 'Connecteur à traiter. Obligatoire.',
            'en' => 'Module\'s token. Required.'
            ),
        'kill' => array(
            'fr' => 'Arrête l\'opération en cours. Facultatif.',
            'en' => 'Stops the current refresh. Optional.'
            ),
    );
    Catalog::answer(Tools::jsonEncode($help));
    exit();
}

$token = Tools::getValue('ec_token', '1');
if ($token != Catalog::getInfoEco('ECO_TOKEN')) {
    header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
    header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
    header('Cache-Control: no-store, no-cache, must-revalidate');
    header('Cache-Control: post-check=0, pre-check=0', false);
    header('Pragma: no-cache');
    Tools::redirect('Location: ../');
    exit();
}

$logger = Catalog::logStart('catalog');
$prefix = 'ECI_CC_';

// parameters
$paramFourn = Tools::getValue('connecteur');
if (!empty($paramFourn)) {
    $fourn = Db::getInstance()->getRow('
        SELECT *
        FROM `' . _DB_PREFIX_ . 'eci_fournisseur`
        WHERE `perso`=1
        AND `name`=\'' . pSQL($paramFourn) . '\'');
}
if (empty($fourn)) {
    $fourn = array();
    $fourn['name'] = false;
}
$connecteur = $fourn['name'];

$paramNbCron = Tools::getValue('nbC', null);
$nbCron = is_null($paramNbCron) ? 0 : (int) $paramNbCron;

$paramJobID = Tools::getValue('jid', null);

$paramAct = Tools::getValue('act', null);
$action = (Catalog::jGet($prefix . 'ACT_' . $connecteur) === 'die') ? 'die' : (is_null($paramAct) ? 'go' : $paramAct);

$paramSpy = Tools::getValue('spy', null);
$spy = is_null($paramSpy) ? false : true;

$paramSpy2 = Tools::getValue('spytwo', null);
$spy2 = is_null($paramSpy2) ? false : true;
$who = $spy ? ($spy2 ? 'spy2' : 'spy') : 'normal';

$paramKill = Tools::getValue('kill', null);
$kill = is_null($paramKill) ? false : true;

$paramPrg = Tools::getValue('prg', null);
$paramPos = Tools::getValue('pos', null);
if (!is_null($paramPrg)) {
    $prg = (int) $paramPrg;
    $pos = is_null($paramPos) ? 1 : (int) $paramPos;
    $chain = '&prg=' . $prg . '&pos=' . $pos;
} else {
    $prg = false;
    $chain = '';
}

// constants
$listStages = array(
    'started',
    'getfile',
    'deldata',
    'refresh',
    'deplcat',
    'upkeep',
    'findderef',
    'upderef',
    'ecitclean',
    'upfluctuplus',
    'upfluctuminus',
    'upfluctums',
//    'ufpack',
//    'ufean',
//    'ufcarac',
//    'ufpict',
//    'ufbrand',
//    'ufdesc',
//    'ufspecial',
//    'upaccessory',
//    'ufnames',
//    'ufdocs',
//    'ufcarrier',
//    'rstcarrier',
//    'ufcategadd',
//    'ufcategrepl',
//    'ufattrn',
//    'ufattrx',
//    'reassoc',
//    'chkdftat',
//    'ufweight',
//    'forcepict',
//    'bealorder',
//    'ufdims',
//    'uftax',
//    'upseo',
    'prules',
//    'ufprices',
    'suppcarriers',
);
$eci_base_uri = implode('/', explode('\\', (((Configuration::get('PS_SSL_ENABLED') == 1) && (Configuration::get('PS_SSL_ENABLED_EVERYWHERE') == 1)) ? 'https://' : 'http://' ) .
    Tools::getShopDomain() . __PS_BASE_URI__ . str_replace(_PS_ROOT_DIR_ . DIRECTORY_SEPARATOR, '', dirname(__FILE__) . '/')));
$ts = preg_replace('/0\.([0-9]{6}).*? ([0-9]+)/', '$2$1', microtime());
$jid = is_null($paramJobID) ? $ts : $paramJobID;
$params = '?ec_token=' . $token . '&ts=' . $ts . '&jid=' . $jid;
$base_uri = $eci_base_uri . basename(__FILE__) . $params . $chain;
$stopTime = time() + 5;


/*
Catalog::logInfo(
    $logger,
    'catAuto '
    . $who . ' entered, parameters '
    . $connecteur . ','
    . (int) $nbCron . ','
    . $action . ','
    . (int) $spy . ','
    . (int) $spy2 . ','
    . (int) $kill
);
*/


// kill
if ($kill) {
    if ($connecteur) {
        if (Catalog::jGet($prefix . 'STATE_' . $connecteur) != 'done') {
            Catalog::jUpdateValue($prefix . 'ACT_' . $connecteur, 'die');
        }
    } else {
        $listFourns = Db::getInstance()->executeS('
            SELECT `name`
            FROM `' . _DB_PREFIX_ . 'eci_fournisseur`
            WHERE `perso`=1');
        foreach ($listFourns as $fourn) {
            $connecteur = $fourn['name'];
            if (Catalog::jGet($prefix . 'STATE_' . $connecteur) != 'done') {
                Catalog::jUpdateValue($prefix . 'ACT_' . $connecteur, 'die');
            }
        }
    }
    exit('die');
}


// invalid connector
if (!$connecteur) {
    exit('noconnector');
}
if (file_exists(dirname(__FILE__) . '/gen/' . $connecteur . '/class/ec' . $connecteur . '.php')) {
    require_once dirname(__FILE__) . '/gen/' . $connecteur . '/class/ec' . $connecteur . '.php';
} else {
    exit('noclass');
}
$genClass = 'Ec' . $connecteur;
$ec_four = new $genClass();
$genStages = Catalog::getClassConstant($genClass, 'CAT_STAGES');
if (!is_null($genStages) && $genStages) {
    $listStages_r = is_null(Tools::jsonDecode($genStages, true)) ? $listStages : Tools::jsonDecode($genStages, true);
    $listStages = array_diff($listStages_r, array_filter($listStages_r, 'intval'));
}
if ($listStages[0] !== 'started') {
    array_unshift($listStages, 'started');
}


// spy
if ($spy) {
    Catalog::answer('spy');
    sleep(14);
    $state = Catalog::jGet($prefix . 'STATE_' . $connecteur);
    $global_progress = 1000000 * (int) array_search(Catalog::jGet($prefix . 'STAGE_' . $connecteur), $listStages, true)
    + 1000 * Catalog::jGet($prefix . 'LOOPS_' . $connecteur)
    + Catalog::jGet($prefix . 'PROGRESS_' . $connecteur);
    if ($nbCron == $global_progress) {
        if ($spy2) {
            if ($state != 'done') {
                Catalog::jUpdateValue($prefix . 'STATE_' . $connecteur, 'still');
            }
        } else {
            Catalog::followLink($base_uri . '&spy=1&spytwo=1&connecteur=' . $connecteur . '&nbC=' . $global_progress);
        }
    } else {
        Catalog::followLink($base_uri . '&spy=1&connecteur=' . $connecteur . '&nbC=' . $global_progress);
    }
    exit('bond');
}


// init or quit
$etat = Catalog::jGet($prefix . 'STATE_' . $connecteur);
$starting = (bool) $token & (bool) $connecteur & is_null($paramSpy) & is_null($paramNbCron) & is_null($paramKill) & is_null($paramAct);
if (!$starting && $action === 'die') {
    Catalog::jUpdateValue($prefix . 'STATE_' . $connecteur, 'done');
    Catalog::jUpdateValue($prefix . 'END_TIME_' . $connecteur, date('Y-m-d H:i:s'));
    Catalog::jUpdateValue($prefix . 'ACT_' . $connecteur, 'go');
    exit('die');
}
if ($starting && $etat === 'running') {
    $progress = Catalog::jGet($prefix . 'PROGRESS_' . $connecteur);
    Catalog::followLink($base_uri . '&spy=1&connecteur=' . $connecteur . '&nbC=' . (int) $progress);
    exit('no double');
}
if (!$starting && $etat === 'still') {
    Catalog::jUpdateValue($prefix . 'STATE_' . $connecteur, 'running');
    Catalog::followLink($base_uri . '&spy=1&connecteur=' . $connecteur . '&nbC=' . $nbCron);
}
if ($starting) {
    Catalog::jUpdateValue($prefix . 'START_TIME_' . $connecteur, date('Y-m-d H:i:s'));
    Catalog::jUpdateValue($prefix . 'END_TIME_' . $connecteur, '');
    Catalog::jUpdateValue($prefix . 'STAGE_' . $connecteur, reset($listStages));
    Catalog::jUpdateValue($prefix . 'PROGRESS_' . $connecteur, 0);
    Catalog::jUpdateValue($prefix . 'PROGRESSMAX_' . $connecteur, 0);
    Catalog::jUpdateValue($prefix . 'LOOPS_' . $connecteur, 0);
    Catalog::jUpdateValue($prefix . 'STATE_' . $connecteur, 'running');
    Catalog::jUpdateValue($prefix . 'ACT_' . $connecteur, 'go');
    Catalog::jUpdateValue($prefix . 'MESSAGE_' . $connecteur, '');
    Catalog::followLink($base_uri . '&spy=1&connecteur=' . $connecteur . '&nbC=0');
} else {
    Catalog::jUpdateValue($prefix . 'PROGRESS_' . $connecteur, $nbCron);
}
$stage = Catalog::jGet($prefix . 'STAGE_' . $connecteur);

Context::getContext()->employee = new Employee(Catalog::getEciEmployeeId());

// loop, break, end
if ($action === 'next') {
    $action = 'go';
    Catalog::jUpdateValue($prefix . 'ACT_' . $connecteur, 'go');

    $numStage = array_search($stage, $listStages, true);
    $keys = array_keys($listStages);
    $next = $nextKey = false;
    foreach ($keys as $key) {
        if ($next) {
            $nextKey = $key;
            break;
        }
        if ($numStage == $key) {
            $next = true;
        }
    }

    if ($nextKey) {
        $stage = $listStages[$nextKey];
        $nbCron = 0;
    } else {
        Catalog::jUpdateValue($prefix . 'STATE_' . $connecteur, 'done');
        Catalog::jUpdateValue($prefix . 'END_TIME_' . $connecteur, date('Y-m-d H:i:s'));
        if ($prg) {
            //chaining with other task
            $nextCron = Catalog::getNextCron($prg, $pos);
            if ($nextCron) {
                Catalog::followLink($nextCron['link'] . '&prg=' . $prg . '&pos=' . $nextCron['position']);
            }
        }

        exit('done');
    }
    Catalog::jUpdateValue($prefix . 'STAGE_' . $connecteur, $stage);
    Catalog::jUpdateValue($prefix . 'LOOPS_' . $connecteur, 0);
    Catalog::jUpdateValue($prefix . 'PROGRESS_' . $connecteur, 0);
    Catalog::jUpdateValue($prefix . 'PROGRESSMAX_' . $connecteur, 0);
}

/*
Catalog::logInfo(
    $logger,
    'catAuto ' . $who . ' do ' . $stage . ',' . $action
);
*/

// aiguillage
$reps = null;
Catalog::answer($stage);
$can_use_method = in_array($stage, get_class_methods($ec_four));
if ($can_use_method || function_exists($stage)) {
    try {
        $reps = $can_use_method ? $ec_four->$stage($prefix, $nbCron, $stopTime, $logger) : $stage($prefix, $connecteur, $nbCron, $stopTime, $logger, $ec_four);
    } catch (Exception $e) {
        $reps = 'In "' . $stage . '" : ' . $e->getMessage() . ' in line ' . $e->getLine() . ' of file ' . $e->getFile();
    }
    if ($reps === true) {
        Catalog::jUpdateValue($prefix . 'ACT_' . $connecteur, 'next');
        Catalog::followLink($base_uri . '&connecteur=' . $connecteur . '&nbC=0&act=next');
    } elseif (is_numeric($reps)) {
        Catalog::jUpdateValue($prefix . 'LOOPS_' . $connecteur, Catalog::jGet($prefix . 'LOOPS_' . $connecteur) + 1);
        Catalog::followLink($base_uri . '&connecteur=' . $connecteur . '&nbC=' . $reps);
    }
} else {
    $reps = 'Phase "' . $stage . '" does not exist';
}

if ((!is_null($reps)) && (true !== $reps) && (!is_numeric($reps))) {
    Catalog::jUpdateValue($prefix . 'MESSAGE_' . $connecteur, date('Y-m-d H:i:s ') . var_export($reps, true));
    Catalog::logInfo(
        $logger,
        'catAuto ' . $who . ', ' . $connecteur . ', stage ' . $stage . ', ' . var_export($reps, true)
    );
}

exit('Bye');


function started($prefix, $connecteur, $nbCron, $stopTime, $logger, $ec_four)
{
    // check that the supplier exists
    $id_supplier = Catalog::getEciFournId($connecteur);
    if (!Supplier::supplierExists($id_supplier)) {
        Catalog::logError($logger, 'Le fournisseur ' . $connecteur . ' a été supprimé');
        return 'Le fournisseur ' . $connecteur . ' a été supprimé';
    }

//    $logger->logInfo(get_included_files());

    echo 'Task successfully started.';

    return Catalog::revMod();
}

function getfile($prefix, $connecteur, $nbCron, $stopTime, $logger, $ec_four)
{
    $type = 'catalog';
    if (!$nbCron) {
        Catalog::jUpdateValue($prefix . 'DATA_' . $connecteur, '');
        try {
            $retour = $ec_four->getFile($type);
        } catch (Exception $ex) {
            return 'Error in getFile : ' . $ex->getMessage() . ' line ' . $ex->getLine();
        }
    } else {
        $json_param = Catalog::jGet($prefix . 'DATA_' . $connecteur);
        $tab_param = Tools::jsonDecode($json_param, true);
        if (!is_null($tab_param)) {
            try {
                $retour = $ec_four->getFile(
                    $type,
                    isset($tab_param['fichier'])?$tab_param['fichier']:'',
                    isset($tab_param['slice'])?$tab_param['slice']:0,
                    isset($tab_param['extparam'])?$tab_param['extparam']:''
                );
            } catch (Exception $ex) {
                return 'Error in getFile : ' . $ex->getMessage() . ' line ' . $ex->getLine();
            }
        } else {
            try {
                $retour = $ec_four->getFile($type);
            } catch (Exception $ex) {
                return 'Error in getFile : ' . $ex->getMessage() . ' line ' . $ex->getLine();
            }
        }
    }

    if (is_array($retour)) {
        if (!isset($retour['rc'])) {
            return var_export($retour, true);
        } else {
            switch ((int)$retour['rc']) {
                case 1:
                    return true;
                case 2:
                    Catalog::jUpdateValue($prefix . 'DATA_' . $connecteur, Tools::jsonEncode($retour));
                    return (1 + $nbCron);
                default:
                    if (isset($retour['message'])) {
                        return $retour['message'];
                    } else {
                        return var_export($retour, true);
                    }
            }
        }
    } else {
        $tab_ret = explode(',', $retour);
        if (2 > count($tab_ret)) {
            return 'Too few infos in return from getFile for flux "' . $type . '" : ' . $retour;
        }
        switch ((int)$tab_ret[0]) {
            case 1:
                return true;
            case 2:
                $tab_param = array();
                parse_str($tab_ret[1], $tab_param);
                //Catalog::logInfo($logger, var_export($retour, true));
                //Catalog::logInfo($logger, var_export($tab_ret, true));
                //Catalog::logInfo($logger, var_export($tab_param, true));
                Catalog::jUpdateValue($prefix . 'DATA_' . $connecteur, Tools::jsonEncode($tab_param));
                return (1 + $nbCron);
            case 0:
                return is_numeric($tab_ret[1]) ? 'Exit getFile with no message defined ' . $tab_ret[1] : $tab_ret[1];
            default:
                return 'Unknown error in getFile : ' . $retour;
        }
    }
}

function deldata($prefix, $connecteur, $nbCron, $stopTime, $logger, $ec_four)
{
    try {
        $catalog = new Catalog();
        $reps = $catalog->deleteData($connecteur, (bool) $nbCron, true);
    } catch (Exception $ex) {
        return 'Error in deleteData : ' . $ex->getMessage();
    }
    if ($reps) {
        return true;
    } else {
        $nbCron++;
    }

    return $nbCron;
}

function refresh($prefix, $connecteur, $nbCron, $stopTime, $logger, $ec_four)
{
    //signal : 1 begin majsel3
    if (!$nbCron) {
        Catalog::jUpdateValue($prefix . 'DNDO_' . $connecteur, 1);
    }

    $json_param = Catalog::jGet($prefix . 'DATA_' . $connecteur);
    $tab_param = Tools::jsonDecode($json_param, true);
    if (!is_null($tab_param)) {
        try {
            $retour = $ec_four->fillCatalog(
                isset($tab_param['row'])?$tab_param['row']:0,
                isset($tab_param['rowRef'])?$tab_param['rowRef']:'',
                isset($tab_param['tentative'])?$tab_param['tentative']:0,
                isset($tab_param['fichierActu'])?$tab_param['fichierActu']:0,
                isset($tab_param['nbFichier'])?$tab_param['nbFichier']:0,
                isset($tab_param['lignesTot'])?$tab_param['lignesTot']:0,
                isset($tab_param['tot'])?$tab_param['tot']:1,
                isset($tab_param['act'])?$tab_param['act']:1
            );
        } catch (Exception $ex) {
            return 'Error in fillCatalog : ' . $ex->getMessage() . ' line ' . $ex->getLine();
        }
    } else {
        try {
            $retour = $ec_four->fillCatalog(0, '', 0, 0, 0, 0, 1, 1);
        } catch (Exception $ex) {
            return 'Error in fillCatalog : ' . $ex->getMessage() . ' line ' . $ex->getLine();
        }
    }

    if (is_array($retour)) {
        Catalog::jUpdateValue(
            $prefix . 'PROGRESSMAX_' . $connecteur,
            isset($retour['lignesTot'])?(int) $retour['lignesTot']:0
        );
        Catalog::jUpdateValue($prefix . 'DATA_' . $connecteur, Tools::jsonEncode($retour));
        if (is_callable(array($ec_four, 'refreshLoopRule'))) {
            return $ec_four->refreshLoopRule($retour);
        } else {
            return (($retour['row'] >= $retour['lignesTot']) ? true : $nbCron + 1);
        }
    } else {
        return 'Error in fillCatalog : ' . $retour;
    }
}

function rebaseprices($prefix, $connecteur, $nbCron, $stopTime, $logger, $ec_four)
{
    //TODO
    //calculate smallest attribute pmvc and update the parent
    //signal : 0 majsel3 ended OK
    if (!$nbCron) {
        Catalog::jUpdateValue($prefix . 'DNDO_' . $connecteur, 0);
        //update max : the count of distinct product_reference in catalog_attribute for this fournisseur
    }

    //get list of limited products

    //get the attributes of a product

    //calculate the min -> update the parent

    //calculate the deltas -> update the attributes
}

function deplcat($prefix, $connecteur, $nbCron, $stopTime, $logger, $ec_four)
{
    if (!$nbCron) {
        Catalog::jUpdateValue($prefix . 'DNDO_' . $connecteur, 0);
    }
    // deployes any category present in the catalog
    // to do : find a way to clean them if no more present in catalog
    // hint : perhaps use the "populated" bool calculated by this function to hide them in the controller

    Catalog::deployCategories($connecteur);

    return true;
}

function upkeep($prefix, $connecteur, $nbCron, $stopTime, $logger, $ec_four)
{
    //update 'keep' field in the eci_product_shop table
    if (Catalog::jGet($prefix . 'DNDO_' . $connecteur)) {
        return true;
    }

    if (!$nbCron) {
        Db::getInstance()->execute(
            'UPDATE ' . _DB_PREFIX_ . 'eci_product_shop s
            LEFT JOIN ' . _DB_PREFIX_ . 'eci_catalog c
            ON c.product_reference = s.reference
            AND c.fournisseur = s.fournisseur
            SET s.keep = c.keep
            WHERE s.id_product_attribute = 0
            AND s.fournisseur = "' . pSQL($connecteur) . '"
            AND s.keep != c.keep'
        );
        Db::getInstance()->execute(
            'UPDATE ' . _DB_PREFIX_ . 'eci_product_shop s
            LEFT JOIN ' . _DB_PREFIX_ . 'eci_catalog_attribute a
            ON a.reference_attribute = s.reference
            AND a.fournisseur = s.fournisseur
            SET s.keep = a.keep
            WHERE s.id_product_attribute != 0
            AND s.fournisseur = "' . pSQL($connecteur) . '"
            AND s.keep != a.keep'
        );
//        Catalog::jUpdateValue($prefix . 'PROGRESSMAX_' . $connecteur, Catalog::getNbEciPs($connecteur, null, true));
    }

//    $lRefs = Db::getInstance()->executeS(
//        'SELECT id_product, keep, act_reasons, id_shop
//        FROM ' . _DB_PREFIX_ . 'eci_product_shop
//        WHERE id_product_attribute = 0
//        AND fournisseur = "' . pSQL($connecteur) .'"
//        ORDER BY id_product, id_shop
//        LIMIT ' . (int) $nbCron . ', 500'
//    );
//    if (!$lRefs) {
//        return true;
//    }
//
//    $jobLine = $nbCron;
//    foreach ($lRefs as $prod) {
//        if (time() > $stopTime) {
//            break;
//        }
//        $jobLine++;
//
//        $keep = Catalog::arJsonDecodeRecur($prod['keep']);
//        $keep_act_reasons = isset($keep['act_reasons']) && is_array($keep['act_reasons']) ? $keep['act_reasons'] : array();
//        $act_reasons = Catalog::arJsonDecodeRecur($prod['act_reasons']);
//        if (!is_array($act_reasons)) {
//            $act_reasons = array();
//        }
//
//        $new_act_reasons = array_merge($act_reasons, $keep_act_reasons);
//
//        Db::getInstance()->update(
//            'eci_product_shop',
//            array('act_reasons' => pSQL(json_encode($new_act_reasons))),
//            'id_product = ' . (int) $prod['id_product'] . ' AND id_product_attribute = 0 AND fournisseur = "' . pSQL($connecteur) . '" AND id_shop = ' . (int) $prod['id_shop']
//        );
//    }
//
//    return $jobLine;
    return true;
}

function findderef($prefix, $connecteur, $nbCron, $stopTime, $logger, $ec_four)
{
    //reset deref
    Db::getInstance()->update(
        'eci_product_shop',
        array(
            'deref' => 0
        ),
        'fournisseur = "' . pSQL($connecteur) . '"'
    );
    //find dereferenced products and combinations -> update the eci_product_shop table
    Db::getInstance()->execute(
        'UPDATE ' . _DB_PREFIX_ . 'eci_product_shop s
        LEFT JOIN ' . _DB_PREFIX_ . 'eci_catalog c
        ON c.product_reference = s.reference
        AND c.fournisseur = s.fournisseur
        SET s.deref = 1
        WHERE s.id_product_attribute = 0
        AND s.fournisseur = "' . pSQL($connecteur) . '"
        AND c.product_reference IS NULL'
    );
    Db::getInstance()->execute(
        'UPDATE ' . _DB_PREFIX_ . 'eci_product_shop s
        LEFT JOIN ' . _DB_PREFIX_ . 'eci_catalog_attribute a
        ON a.reference_attribute = s.reference
        AND a.fournisseur = s.fournisseur
        SET s.deref = 1
        WHERE s.id_product_attribute != 0
        AND s.fournisseur = "' . pSQL($connecteur) . '"
        AND a.reference_attribute IS NULL'
    );
    //dereference product if they are in shops where they are not wanted
    Db::getInstance()->execute(
        'UPDATE ' . _DB_PREFIX_ . 'eci_product_shop s
        LEFT JOIN ' . _DB_PREFIX_ . 'eci_catalog c
        ON c.product_reference = s.reference
        AND c.fournisseur = s.fournisseur
        SET s.deref = 1
        WHERE 1
        /*AND s.id_product_attribute = 0*/
        AND s.fournisseur = "' . pSQL($connecteur) . '"
        AND c.keep REGEXP "\"id_shop\":[0-9]+"
        AND c.keep NOT REGEXP CONCAT("\"id_shop\":", s.id_shop, "[^0-9]")'
    );

    return true;
}

function upderef($prefix, $connecteur, $nbCron, $stopTime, $logger, $ec_four)
{
    // dereferenced products will be inactivated or deleted
    // be sure to have run the findderef function
    Catalog::getConfigTempTable(array('ACTIVE', 'ACTION_PRODUCT_DEREF', 'UPDATE_PRODUCT_DEREF'), $connecteur);

    if (!$nbCron) {
        $max = (int) Db::getInstance()->getValue(
            'SELECT COUNT(*)
            FROM ' . _DB_PREFIX_ . 'eci_product_shop e
            LEFT JOIN ' . _DB_PREFIX_ . 'product_shop ps
            ON e.id_product = ps.id_product
            AND e.id_shop = ps.id_shop
            LEFT JOIN ' . _DB_PREFIX_ . 'eci_conftmp tmp
            ON ps.id_shop = tmp.id_shop
            WHERE 1
            AND ((0 = UPDATE_PRODUCT_DEREF AND 1 = ps.active) OR (1 = UPDATE_PRODUCT_DEREF))
            AND e.fournisseur = "' . pSQL($connecteur) . '"
            AND e.id_product_attribute = 0
            AND e.deref = 1
            AND tmp.ACTIVE = 1
            AND tmp.ACTION_PRODUCT_DEREF = 1'
        );
        Catalog::jUpdateValue($prefix . 'PROGRESSMAX_' . $connecteur, $max);
    }

    $prods_deref = Db::getInstance()->executeS(
        'SELECT e.reference,
                e.id_product,
                ps.id_shop,
                tmp.UPDATE_PRODUCT_DEREF as del
        FROM ' . _DB_PREFIX_ . 'eci_product_shop e
        LEFT JOIN ' . _DB_PREFIX_ . 'product_shop ps
        ON e.id_product = ps.id_product
        AND e.id_shop = ps.id_shop
        LEFT JOIN ' . _DB_PREFIX_ . 'eci_conftmp tmp
        ON ps.id_shop = tmp.id_shop
        WHERE 1
        AND ((0 = UPDATE_PRODUCT_DEREF AND 1 = ps.active) OR (1 = UPDATE_PRODUCT_DEREF))
        AND e.fournisseur = "' . pSQL($connecteur) . '"
        AND e.id_product_attribute = 0
        AND e.deref = 1
        AND tmp.ACTIVE = 1
        AND tmp.ACTION_PRODUCT_DEREF = 1
        ORDER BY ps.id_shop, ps.id_product
        LIMIT 100'
    );
    if (!$prods_deref) {
        return true;
    }

    $old_shop = $prods_deref[0]['id_shop'];
    Shop::setContext(Shop::CONTEXT_SHOP, $old_shop);
    $in = array();
    $where = 'id_shop = ' . (int) $old_shop . ' AND fournisseur = "' . pSQL($connecteur) . '" AND id_product IN ("';
    foreach ($prods_deref as $prod_deref) {
        $id_shop = $prod_deref['id_shop'];
        if ($id_shop != $old_shop || time() > $stopTime) {
            break;
        }
        $nbCron++;
        $id_product = $prod_deref['id_product'];
        $del = $prod_deref['del'];
        Catalog::setNullStock($id_product, $id_shop);
        try {
            $product = new Product((int) $id_product, false, null, $id_shop, null);
            if ($del) {
                $product->delete();
                $in[] = (int) $id_product;
            } else {
                if ($product->active) {
                    $product->active = 0;
                    $product->update();
                }
            }
        } catch (Exception $e) {
            Db::getInstance()->update(
                'product_shop',
                array('active' => (int)0),
                'id_product = ' . (int) $id_product . ' AND id_shop = ' . (int) $id_shop
            );
            Catalog::logInfo(
                $logger,
                'Error in ' . __FUNCTION__ . ' for idp ' . $id_product . ' ; '
                . $e->getMessage()
                . '. Product ' . $prod_deref['reference'] . ($del ? ' not deleted but disabled' : ' disabled in DB')
            );
        }
        if ($in && ((count($in) % 50) == 0)) {
            Db::getInstance()->delete('eci_product_shop', $where . implode('","', $in) . '")');
            $in = array();
        }
    }
    if ($in) {
        Db::getInstance()->delete('eci_product_shop', $where . implode('","', $in) . '")');
    }

    return $nbCron;
}

function ecitclean($prefix, $connecteur, $nbCron, $stopTime, $logger, $ec_four)
{
    Catalog::cleanEciProdShop();
    Catalog::cleanProductBlacklist($connecteur);
    Catalog::cleanImported();
    Catalog::cleanCategoryShop();
    Catalog::cleanPacks($connecteur);

    return true;
}

function upfluctuplus($prefix, $connecteur, $nbCron, $stopTime, $logger, $ec_four)
{
    // adding new combinations
    $context = Context::getContext();
    $id_lang = Configuration::get('PS_LANG_DEFAULT');
    $context->language = new Language($id_lang);
    $db = Db::getInstance();
    $do = true;
    $log = false;

    if (!$nbCron) {
        $max = (int) $db->getValue(
            'SELECT COUNT(DISTINCT ca.reference_attribute, eps2.id_product, eps2.id_shop)
            FROM ' . _DB_PREFIX_ . 'eci_catalog_attribute ca
            LEFT JOIN ' . _DB_PREFIX_ . 'eci_product_shop eps1
            ON eps1.reference = ca.reference_attribute AND eps1.fournisseur = ca.fournisseur
            LEFT JOIN ' . _DB_PREFIX_ . 'eci_product_shop eps2
            ON eps2.reference = ca.product_reference AND eps2.fournisseur = ca.fournisseur
            WHERE ca.fournisseur = "' . pSQL($connecteur) . '"
            AND (eps1.id_product_attribute = 0 OR eps1.reference IS NULL)
            AND eps2.id_product IS NOT NULL
            AND eps2.id_product_attribute = 0
            AND ca.special != "nocreate"'
        );
        Catalog::jUpdateValue($prefix . 'PROGRESSMAX_' . $connecteur, $max);
    } elseif ($nbCron > Catalog::jGet($prefix . 'PROGRESSMAX_' . $connecteur)) {
        Catalog::logInfo($logger, 'Attention : problème avec ' . __FUNCTION__ . ' à étudier');
        return true;
    }

    $list = $db->executeS(
        'SELECT DISTINCT ca.*, eps2.id_product, eps2.id_shop
        FROM ' . _DB_PREFIX_ . 'eci_catalog_attribute ca
        LEFT JOIN ' . _DB_PREFIX_ . 'eci_product_shop eps1
        ON eps1.reference = ca.reference_attribute AND eps1.fournisseur = ca.fournisseur
        LEFT JOIN ' . _DB_PREFIX_ . 'eci_product_shop eps2
        ON eps2.reference = ca.product_reference AND eps2.fournisseur = ca.fournisseur
        WHERE ca.fournisseur = "' . pSQL($connecteur) . '"
        AND (eps1.id_product_attribute = 0 OR eps1.reference IS NULL)
        AND eps2.id_product IS NOT NULL
        AND eps2.id_product_attribute = 0
        AND ca.special != "nocreate"
        ORDER BY eps2.id_product, ca.product_reference, ca.reference_attribute
        LIMIT 500'
    );
    if (!$list) {
        return true;
    }

    $old_id_shop = 0;
    foreach ($list as $decl) {
        if (time() > $stopTime) {
            break;
        }
        $nbCron++;
        $refa = $decl['reference_attribute'];
        $refp = $decl['product_reference'];
        $id_product = (int) $decl['id_product'];
        $id_shop = $decl['id_shop'];
        if ($old_id_shop != $id_shop) {
            $context->shop = new Shop($id_shop, $id_lang, $id_shop);
            $catalog = new Catalog($context);
            $old_id_shop = $id_shop;
        }
        try {
            if ($log) {
                Catalog::logInfo(
                    $logger,
                    __FUNCTION__ . ' doit ajouter la déclinaison "'.$refa.'" au produit "'.$refp.'" ('.$id_product.')'
                );
            }
            if ($do) {
                if (Shop::isFeatureActive()) {
                    $product = new Product($id_product, false, $id_lang, $id_shop);
                } else {
                    $product = new Product($id_product);
                }
                if (!Validate::isLoadedObject($product)) {
                    //delete product in eci_product_shop
                    $db->delete(
                        'eci_product_shop',
                        'id_product = ' . (int) $id_product
                    );
                }
                if (!$catalog->setAttribute($product, $decl, $product->id_shop_default)) {
                    //mark combination as problematic in eci_catalog_attribute, log, continue
                    $db->update(
                        'eci_catalog_attribute',
                        array(
                            'special' => pSQL('nocreate')
                        ),
                        'product_reference = "'.pSQL($refp).'" AND reference_attribute = "'.pSQL($refa).'" AND fournisseur = "'.pSQL($connecteur).'"'
                    );
                    Catalog::logInfo(
                        $logger,
                        __FUNCTION__ . ' ne peut pas ajouter la déclinaison "'.$refa.'" au produit "'.$refp.'" ('.$id_product.')'
                    );
                    continue;
                }
                $product->checkDefaultAttributes();
            }
        } catch (Exception $e) {
            $db->update(
                'eci_catalog_attribute',
                array(
                    'special' => pSQL('nocreate')
                ),
                'product_reference = "'.pSQL($refp).'" AND reference_attribute = "'.pSQL($refa).'" AND fournisseur = "'.pSQL($connecteur).'"'
            );
            Catalog::logInfo(
                $logger,
                'trouvé déclinaison "' . $refa . '" absente du shop ' .
                'appartient au produit "' . $refp . '" ' .
                'qui a pour id "' . $id_product . '" a planté pour la raison :' . $e->getMessage()
            );
        }
    }

    return $do ? $nbCron : true;
}

function upfluctuminus($prefix, $connecteur, $nbCron, $stopTime, $logger, $ec_four)
{
    // deleting combinations no more in catalog
    $db = Db::getInstance();
    $do = true;
    $log = false;

    if (!$nbCron) {
        $max = (int) $db->getValue(
            'SELECT COUNT( DISTINCT eps1.id_product_attribute)
            FROM '._DB_PREFIX_.'eci_product_shop eps1
            LEFT JOIN '._DB_PREFIX_.'eci_product_shop eps2
            ON eps2.id_product = eps1.id_product AND eps2.fournisseur = eps1.fournisseur
            WHERE eps1.fournisseur = "' . pSQL($connecteur) . '"
            AND eps1.id_product_attribute != 0
            AND eps1.deref = 1
            AND eps2.id_product_attribute = 0'
        );
        Catalog::jUpdateValue($prefix . 'PROGRESSMAX_' . $connecteur, $max);
    }

    $list = $db->executeS(
        'SELECT DISTINCT eps1.id_product, eps1.id_product_attribute, eps2.reference as product_reference, eps1.reference as reference_attribute
        FROM '._DB_PREFIX_.'eci_product_shop eps1
        LEFT JOIN '._DB_PREFIX_.'eci_product_shop eps2
        ON eps2.id_product = eps1.id_product AND eps2.fournisseur = eps1.fournisseur
        WHERE eps1.fournisseur = "' . pSQL($connecteur) . '"
        AND eps1.id_product_attribute != 0
        AND eps1.deref = 1
        AND eps2.id_product_attribute = 0
        ORDER BY eps1.id_product, eps1.reference
        LIMIT 500'
    );
    if (!$list) {
        return true;
    }

    foreach ($list as $decl) {
        if (time() > $stopTime) {
            break;
        }
        $nbCron++;
        $refa = $decl['reference_attribute'];
        $refp = $decl['product_reference'];
        $id_product = (int) $decl['id_product'];
        $id_product_attribute = (int) $decl['id_product_attribute'];
        if ($log) {
            Catalog::logInfo(
                $logger,
                __FUNCTION__ . ' doit supprimer la déclinaison "'.$refa.'" du produit "'.$refp.'" ('.$id_product.'-'.$id_product_attribute.')'
            );
        }
        if (!$do) {
            continue;
        }
        $del = Catalog::combinationDelete($id_product, $id_product_attribute);
        if (true !== $del) {
            Catalog::logInfo(
                $logger,
                'la suppression de la déclinaison "' . $refa . '" appartenant au produit "' . $refp . '" ' .
                'qui a pour id "' . $id_product . '" a planté pour la raison :' . $del
            );
        }
    }

    return $do ? $nbCron : true;
}

function upfluctums($prefix, $connecteur, $nbCron, $stopTime, $logger, $ec_four)
{
    // spreading attribute groups and attributes accross all shops (not done by PS)
    $db = Db::getInstance();

    $lShops = array_values(Shop::getShops(false, null, true));
    $nShops = count($lShops);
    if (1 == $nShops) {
        return true;
    }

    // copy attributes in the table attribute_shop
    $lFlawAS = $db->executeS(
        'SELECT `id_attribute`
        FROM `' . _DB_PREFIX_ . 'attribute_shop`
        WHERE 1
        GROUP BY `id_attribute`
        HAVING COUNT(`id_attribute`) != ' . (int) $nShops// order by id_attribute and limit $nbCron,500
    );
    foreach ($lFlawAS as $line) {
        $enreg = array('id_attribute' => (int) $line['id_attribute']);
        $insert = array();
        foreach ($lShops as $id_shop) {
            $enreg['id_shop'] = (int) $id_shop;
            $insert[] = $enreg;
        }
        $db->insert('attribute_shop', $insert, false, false, Db::INSERT_IGNORE);
    }
    unset($lFlawAS);

    // copy attribute groups in the table attribute_group_shop
    $lFlawAGS = $db->executeS(
        'SELECT `id_attribute_group`
        FROM `' . _DB_PREFIX_ . 'attribute_group_shop`
        WHERE 1
        GROUP BY `id_attribute_group`
        HAVING COUNT(`id_attribute_group`) != ' . (int) $nShops// order by id_attribute_group and limit $nbCron,500
    );
    foreach ($lFlawAGS as $line) {
        $enreg = array('id_attribute_group' => (int) $line['id_attribute_group']);
        $insert = array();
        foreach ($lShops as $id_shop) {
            $enreg['id_shop'] = (int) $id_shop;
            $insert[] = $enreg;
        }
        $db->insert('attribute_group_shop', $insert, false, false, Db::INSERT_IGNORE);
    }
    unset($lFlawAGS);

    return true;
}

function ufean($prefix, $connecteur, $nbCron, $stopTime, $logger, $ec_four)
{
    //resolve fluctuations in EAN13 and UPC
    if (!$nbCron) {
        Catalog::jUpdateValue($prefix . 'PROGRESSMAX_' . $connecteur, Catalog::getNbEciPs($connecteur));
    }

    $lRefs = Catalog::getListProdInShop($connecteur, $nbCron);
    if (!$lRefs) {
        return true;
    }

    $jobLine = $nbCron;
    foreach ($lRefs as $prod) {
        if (time() > $stopTime) {
            break;
        }
        $jobLine++;

        //update ean13 and upc for products
        $ecip = Catalog::getProduct($prod['reference'], $connecteur);
        if (isset($ecip['produit'])) {
            try {
                $product = new Product($prod['id_product']);
                if (!Validate::isLoadedObject($product)) {
                    continue;
                }
                $fields_to_update = [];
                if ($product->ean13 != $ecip['produit']['ean13']) {
                    $product->ean13 = $ecip['produit']['ean13'];
                    $fields_to_update['ean13'] = true;
                }
                if ($product->upc != $ecip['produit']['upc']) {
                    $product->upc = $ecip['produit']['upc'];
                    $fields_to_update['upc'] = true;
                }
                if ($fields_to_update) {
                    $product->setFieldsToUpdate($fields_to_update);
                    $product->update();
                }
            } catch (Exception $e) {
                Catalog::logInfo($logger, 'Error in ' . __FUNCTION__ . ' for pid' . $prod['id_product'] . ' : ' . $e->getMessage());
            }
        }

        //update ean13 and upc for combinations
        if (!$ecip['declinaisons'] || !$ecip['prestashop']['declinaisons']) {
            continue;
        }
        $combinations = array_column(reset($ecip['prestashop']['declinaisons']), 'id_product_attribute', 'reference');
        $eans = array_column($ecip['declinaisons'], 'ean13', 'reference_attribute');
        $upcs = array_column($ecip['declinaisons'], 'upc', 'reference_attribute');
        foreach ($combinations as $reference => $id_pa) {
            try {
                $decl = new Combination($id_pa);
                if (!Validate::isLoadedObject($decl)) {
                    continue;
                }
                $fields_to_update = [];
                if (isset($eans[$reference]) && $decl->ean13 != $eans[$reference]) {
                    $decl->ean13 = $eans[$reference];
                    $fields_to_update['ean13'] = true;
                }
                if (isset($upcs[$reference]) && $decl->upc != $upcs[$reference]) {
                    $decl->upc = $upcs[$reference];
                    $fields_to_update['upc'] = true;
                }
                if ($fields_to_update) {
                    $decl->setFieldsToUpdate($fields_to_update);
                    $decl->update();
                }
            } catch (Exception $e) {
                Catalog::logInfo($logger, 'Error in ' . __FUNCTION__ . ' for pid' . $prod['id_product'] . '-' . $id_pa . ' : ' . $e->getMessage());
            }
        }
    }

    return $jobLine;
}

function ufcarac($prefix, $connecteur, $nbCron, $stopTime, $logger, $ec_four)
{
    //resolve fluctuations in features
    if (!$nbCron) {
        Catalog::jUpdateValue($prefix . 'PROGRESSMAX_' . $connecteur, Catalog::getNbEciPs($connecteur, null, true));
    }

    $lRefs = Catalog::getListProdInShop($connecteur, $nbCron, 50, true);
    if (!$lRefs) {
        return true;
    }

    $context = Context::getContext();

    $old_shop = null;
    $catalog = null;
    $jobLine = $nbCron;
    foreach ($lRefs as $prod) {
        if (time() > $stopTime) {
            break;
        }
        $jobLine++;

        if ($old_shop !== $prod['id_shop']) {
            $context->shop->id = (int) $prod['id_shop'];
            Shop::setContext(Shop::CONTEXT_SHOP, (int) $prod['id_shop']);
            $catalog = new Catalog($context);
            $old_shop = $prod['id_shop'];
        }

        //update features
        $diff_feat = Catalog::getCatInfoDiff($prod['reference'], $connecteur, 'feature');
        if (!$diff_feat) {
            continue;
        }
        try {
            $product = new Product($prod['id_product'], false, null, $prod['id_shop']);
            if (!Validate::isLoadedObject($product)) {
                continue;
            }
            if ($prod['id_shop'] != $product->id_shop_default) {
                continue;
            }
            $product->deleteFeatures();
            $catalog->setFeatures($product, $diff_feat, $connecteur, $prod['id_shop']);
            $product->update();
        } catch (Exception $e) {
            Catalog::logInfo(
                $logger,
                'Error in ' . __FUNCTION__ . ' for pid' . $prod['id_product'] . ' : ' . var_export($diff_feat, true) . "\n" . $e->getMessage()
            );
        }
    }

    return $jobLine;
}

function ufcaracx($prefix, $connecteur, $nbCron, $stopTime, $logger, $ec_four)
{
    //resolve non differential fluctuations in features using matching of group and value :
    // if the supplier changed the features of some products some time ago
    // we delete them and rebuild them
    $context = Context::getContext();
    $id_lang = Configuration::get('PS_LANG_DEFAULT');
    $iso_lang = Language::getIsoById($id_lang);
    $keep_ps_features_not_in_eci = false;

    if (!$nbCron) {
        Catalog::jUpdateValue($prefix . 'PROGRESSMAX_' . $connecteur, Catalog::getNbEciPs($connecteur, null, true));
    }

    $lRefs = Catalog::getListProdInShop($connecteur, $nbCron, 50, true);
    if (!$lRefs) {
        return true;
    }

    $id_supplier = Catalog::getEciFournId($connecteur);

    $old_shop = null;
    $catalog = null;
    $jobLine = $nbCron;
    foreach ($lRefs as $prod) {
        if (time() > $stopTime) {
            break;
        }
        $jobLine++;

        if ($old_shop !== $prod['id_shop']) {
            $context->shop->id = (int) $prod['id_shop'];
            Shop::setContext(Shop::CONTEXT_SHOP, (int) $prod['id_shop']);
            $catalog = new Catalog($context);
            $catalog->matchFeatures();
            $old_shop = $prod['id_shop'];
        }

        $ids = new ImporterReference($prod['reference'], $id_supplier, $prod['id_shop']);
        if ((!$ids->id_product) || ($ids->id_product_attribute)) {
            continue;
        }

        try {
            $product = new Product($prod['id_product'], false, null, $prod['id_shop'], $context);
        } catch (Exception $e) {
            Catalog::logInfo($logger, 'error in ' . __FUNCTION__ . ' while instantiating pid ' . $prod['id_product'] . ' : ' . $e->getMessage());
            continue;
        }
        if ($prod['id_shop'] != $product->id_shop_default) {
            continue;
        }

        $eci_feature = Catalog::getCatInfos($prod['reference'], $connecteur, 'feature');
        if (!$eci_feature) {
            $product->deleteFeatures();
            continue;
        }

        $tab_lnv = Catalog::parseAttAndFeat($eci_feature);
        $ps_feature = Product::getFeaturesStatic($prod['id_product']);

        $prod_ok = true;
        foreach ($tab_lnv as $lnv) {
            if (!count($ps_feature)) {
                $prod_ok = false;
                break;
            }

            $ids_feature = Catalog::getFeatureMatched($lnv, $id_lang, $iso_lang, $prod['id_shop'], $connecteur, false);
            if ($ids_feature && is_array($ids_feature) && !empty(reset($ids_feature))) {
                $id_feature_value = reset($ids_feature);
                $id_feature = key($ids_feature);
            } else {
                $prod_ok = false;
                $reason = 1;
                break;
            }

            //la trouver dans la liste
            $found = false;
            foreach ($ps_feature as $i => $tfv) {
                if ($tfv['id_feature'] == $id_feature && $tfv['id_feature_value'] == $id_feature_value) {
                    $found = true;
                    unset($ps_feature[$i]);
                    break;
                }
            }
            if (!$found) {//feature pas trouvée
                $prod_ok = false;
                $reason = 2;
                break;
            }
        }

        if ($prod_ok && !$keep_ps_features_not_in_eci && count($ps_feature)) {
            $prod_ok = false;
            $reason = 3;
        }

        if ($prod_ok) {
            continue;
        }

        try {
            // s'il manque un nom ou une valeur on réinitialise
            $product->deleteFeatures();
            $catalog->setFeatures($product, $eci_feature, $connecteur, $prod['id_shop']);
        } catch (Exception $e) {
            Catalog::logInfo($logger, 'error in ' . __FUNCTION__ . ' for pid ' . $prod['id_product'] . ' : ' . $e->getMessage());
        }
    }

    return $jobLine;
}

function ufpict($prefix, $connecteur, $nbCron, $stopTime, $logger, $ec_four)
{
    //reload images in product and combinations if one changed
    $context = Context::getContext();
    $catalog = new Catalog($context);

    if (!$nbCron) {
        Catalog::jUpdateValue($prefix . 'PROGRESSMAX_' . $connecteur, Catalog::getNbEciPs($connecteur));
    }

    $lRefs = Catalog::getListProdInShop($connecteur, $nbCron);
    if (!$lRefs) {
        $list_miniatures = glob(_PS_IMG_DIR_ . '/tmp/product_mini_*.jpg');
        foreach ($list_miniatures as $image) {
            try {
                @unlink($image);
            } catch (Exception $e) {
            }
        }
        return true;
    }
    $id_supplier = Catalog::getEciFournId($connecteur);

    $jobLine = $nbCron;
    foreach ($lRefs as $prod) {
        if (time() > $stopTime) {
            break;
        }
        $jobLine++;

        //update pictures
        $diff_pict_prod = Catalog::getCatInfoDiff($prod['reference'], $connecteur, 'pictures');
        $diff_pict_decl = Catalog::getCatAttInfoDiffForProd($prod['reference'], $connecteur, 'pictures');

        if (!$diff_pict_prod && !$diff_pict_decl) {
            continue;
        }

        try {
            $product = new Product($prod['id_product']);
            if (!Validate::isLoadedObject($product)) {
                continue;
            }
            $product->deleteImages();
            // Product images from json rich infos or old style list of urls
            if (($tab_images = Tools::jsonDecode($diff_pict_prod, true)) === null) {
                $tab_images = explode('//:://', $diff_pict_prod);
            }
            if (!empty($tab_images[0])) {
                $catalog->setImages($product, $tab_images);
            }
            // Combinations images from json rich infos or old style list of urls
            if (!$diff_pict_decl) {
                continue;
            }
            foreach ($diff_pict_decl as $decl) {
                if (($tab_images = Tools::jsonDecode($decl['pictures'], true)) === null) {
                    $tab_images = explode('//:://', $decl['pictures']);
                }
                if (!empty($tab_images[0])) {
                    $ids = new ImporterReference($decl['reference_attribute'], $id_supplier);
                    if (!$ids->id_product_attribute) {
                        continue;
                    }
                    $a_ids_image = $catalog->setImages($product, $tab_images, false);
                    //ajouter à la combinaison
                    $combination = new Combination($ids->id_product_attribute);
                    $combination->setImages($a_ids_image);
                }
            }
        } catch (Exception $e) {
            Catalog::logInfo(
                $logger,
                'Error in ' . __FUNCTION__ . ' for pid ' . $prod['id_product'] . ' : ' . $e->getMessage()
            );
        }
    }

    return $jobLine;
}

function ufbrand($prefix, $connecteur, $nbCron, $stopTime, $logger, $ec_four)
{
    //resolve fluctuations in manufacturers
    $force = false;
    if (!$nbCron) {
        Catalog::jUpdateValue($prefix . 'PROGRESSMAX_' . $connecteur, Catalog::getNbEciPs($connecteur));
    }

    $lRefs = Catalog::getListProdInShop($connecteur, $nbCron);
    if (!$lRefs) {
        return true;
    }

    $jobLine = $nbCron;
    foreach ($lRefs as $prod) {
        if (time() > $stopTime) {
            break;
        }
        $jobLine++;

        //update manufacturer if needed
        if ($force) {
            $manufacturer = Catalog::getCatInfos($prod['reference'], $connecteur, 'manufacturer');
        } else {
            $manufacturer = Catalog::getCatInfoDiff($prod['reference'], $connecteur, 'manufacturer');
        }
        if (!$manufacturer) {
            continue;
        }
        try {
            $product = new Product($prod['id_product'], false);
            if (!Validate::isLoadedObject($product)) {
                continue;
            }
            $id_manufacturer = Catalog::getManufacturer($manufacturer);
            if ($product->id_manufacturer != $id_manufacturer) {
                $product->id_manufacturer = $id_manufacturer;
                $product->setFieldsToUpdate(['id_manufacturer' => true]);
                $product->update();
            }
        } catch (Exception $e) {
            Catalog::logInfo($logger, 'Error in ' . __FUNCTION__ . ' for pid ' . $prod['id_product'] . ' : ' . $e->getMessage() . ' in line ' . $e->getLine() . 'of file ' . $e->getFile());
        }
    }

    return $jobLine;
}

function ufdesc($prefix, $connecteur, $nbCron, $stopTime, $logger, $ec_four)
{
    //resolve fluctuations in descriptions
    if (!$nbCron) {
        Catalog::jUpdateValue($prefix . 'PROGRESSMAX_' . $connecteur, Catalog::getNbEciPs($connecteur, null, true));
    }

    $lRefs = Catalog::getListProdInShop($connecteur, $nbCron, 50, true);
    if (!$lRefs) {
        return true;
    }

    $jobLine = $nbCron;
    foreach ($lRefs as $prod) {
        if (time() > $stopTime) {
            break;
        }
        $jobLine++;

        //update description if needed
        $desc = Catalog::getCatInfoDiff($prod['reference'], $connecteur, array('description', 'short_description'));
        if (!$desc) {
            continue;
        }

        try {
            Context::getContext()->shop->id = (int) $prod['id_shop'];
            Shop::setContext(Shop::CONTEXT_SHOP, (int) $prod['id_shop']);
            $product = new Product($prod['id_product'], false, null, (int) $prod['id_shop']);
            if (!Validate::isLoadedObject($product)) {
                continue;
            }
            $fields_to_update = [];
            $tab_desc = Catalog::langExpl($desc['description']);
            foreach ($tab_desc as $iso => $txt) {
                if (Validate::isLangIsoCode($iso)) {
                    $idLg = Language::getIdByIso($iso);
                    if ($idLg) {
                        $product->description[(int) $idLg] = $txt;
                        $fields_to_update['description'][(int) $idLg] = true;
                    }
                }
            }
            $tab_sdesc = Catalog::langExpl($desc['short_description']);
            foreach ($tab_sdesc as $iso => $txt) {
                if (Validate::isLangIsoCode($iso)) {
                    $idLg = Language::getIdByIso($iso);
                    if ($idLg) {
                        $product->description_short[(int) $idLg] = $txt;
                        $fields_to_update['description_short'][(int) $idLg] = true;
                    }
                }
            }

            $product->setFieldsToUpdate($fields_to_update);
            $product->update();
        } catch (Exception $e) {
            Catalog::logInfo($logger, 'catAuto erreur UFDESC product->update() : ' . $e->getMessage());
        }
    }

    return $jobLine;
}

function ufspecial($prefix, $connecteur, $nbCron, $stopTime, $logger, $ec_four)
{
    //resolve fluctuations in "special" data
    $id_supplier = Catalog::getEciFournId($connecteur);
    $sw_force = false;

    if (!$nbCron) {
        Catalog::jUpdateValue($prefix . 'PROGRESSMAX_' . $connecteur, Catalog::getNbEciPs($connecteur, null, true));
    }

    $lRefs = Catalog::getListProdInShop($connecteur, $nbCron, 50, true);
    if (!$lRefs) {
        return true;
    }

    $old_shop = 0;
    $jobLine = $nbCron;
    foreach ($lRefs as $prod) {
        if (time() > $stopTime) {
            break;
        }
        $jobLine++;

        //update special if needed
        if ($sw_force) {
            $special_js = Catalog::getCatInfos($prod['reference'], $connecteur, 'special');
        } else {
            $special_js = Catalog::getCatInfoDiff($prod['reference'], $connecteur, 'special');
        }
        if ($special_js && !is_null($special = json_decode($special_js, true)) && is_array($special)) {
            try {
                if ($prod['id_shop'] != $old_shop) {
                    Context::getContext()->shop = new Shop((int) $prod['id_shop'], null, (int) $prod['id_shop']);
                    Shop::setContext(Shop::CONTEXT_SHOP, (int) $prod['id_shop']);
                    $old_shop = (int) $prod['id_shop'];
                }
                if (Shop::isFeatureActive()) {
                    $product = new Product($prod['id_product'], false, null, (int) $prod['id_shop']);
                } else {
                    $product = new Product($prod['id_product']);
                }
                if (!Validate::isLoadedObject($product)) {
                    continue;
                }

                $retour = Catalog::setProductSpecial($product, $special);
                if ($retour && !empty($product->updated)) {
                    $product->update();
                }
            } catch (Exception $e) {
                Catalog::logInfo($logger, 'Error in ' . __FUNCTION__ . ' product->update() pid' . $prod['id_product'] . ' : ' . $e->getMessage());
            }
        }

        //update attributes special if needed
        if ($sw_force) {
            $special_a = Catalog::getCatAttInfosForProd($prod['reference'], $connecteur, 'special');
        } else {
            $special_a = Catalog::getCatAttInfoDiffForProd($prod['reference'], $connecteur, 'special');
        }
        if (!$special_a) {
            continue;
        }
        foreach ($special_a as $special_line) {
            $reference_attribute = $special_line['reference_attribute'];
            $special_js = $special_line['special'];
            if (!$special_js || is_null($special = json_decode($special_js, true)) || !is_array($special)) {
                continue;
            }
            
            $ids = new ImporterReference($reference_attribute, $id_supplier);
            if (!$ids->id_product_attribute) {
                continue;
            }

            try {
                if (Shop::isFeatureActive()) {
                    $combination = new Combination((int) $ids->id_product_attribute, null, (int) $prod['id_shop']);
                } else {
                    $combination = new Combination((int) $ids->id_product_attribute);
                }
                
                if (!Validate::isLoadedObject($combination)) {
                    continue;
                }

                $retour = Catalog::setAttributeSpecial($combination, $special);
                if ($retour && !empty($combination->updated)) {
                    $combination->update();
                }
            } catch (Exception $e) {
                Catalog::logInfo($logger, 'Error in ' . __FUNCTION__ . ' combination->update() pid' . $ids->id_product . '-' . $ids->id_product_attribute . ' : ' . $e->getMessage());
            }
        }
    }

    return $jobLine;
}

function upaccessory($prefix, $connecteur, $nbCron, $stopTime, $logger, $ec_four)
{
    //update accessory
    $id_supplier = Catalog::getEciFournId($connecteur);
    $sw_remove_old_accessories = false;

    if (!$nbCron) {
        Catalog::jUpdateValue($prefix . 'PROGRESSMAX_' . $connecteur, Catalog::getNbEciPs($connecteur));
    }

    $lRefs = Catalog::getListProdInShop($connecteur, $nbCron);
    if (!$lRefs) {
        return true;
    }

    $jobLine = $nbCron;
    foreach ($lRefs as $prod) {
        if (time() > $stopTime) {
            break;
        }
        $jobLine++;

        if ($sw_remove_old_accessories) {
            Catalog::accessoryDelete($prod['id_product']);
        }

        //get special if containing accessory
        $special = Db::getInstance()->getValue(
            'SELECT special
            FROM ' . _DB_PREFIX_ . 'eci_catalog
            WHERE product_reference = "' . pSQL($prod['reference']) . '"
            AND fournisseur = "' . pSQL($connecteur) . '"
            AND special LIKE "%accessory_:%"'
        );
        if (!$special) {
            continue;
        }

        if (is_null($tab_spe = Tools::jsonDecode($special, true))) {
            continue;
        }
        if ((!is_array($tab_spe)) || empty($tab_spe['accessory']) || (!is_array($tab_spe['accessory']))) {
            continue;
        }

        $accessories = $tab_spe['accessory'];
        $id_accessories = array();
        foreach ($accessories as $ref_accessory) {
            $ids = new ImporterReference($ref_accessory, $id_supplier);
            if ($ids->id_product && ($ids->id_product != $prod['id_product'])) {
                $id_accessories[] = $ids->id_product;
            }
        }
        if ($id_accessories) {
            Product::changeAccessoriesForProduct($id_accessories, $prod['id_product']);
            Catalog::accessoryDedup($prod['id_product']);
        }
    }

    return $jobLine;
}

function ufnames($prefix, $connecteur, $nbCron, $stopTime, $logger, $ec_four)
{
    //resolve fluctuations in names
    $id_lang = Catalog::getInfoEco('ID_LANG');
    $iso_lang = Language::getIsoById($id_lang);
    $all_lang = Language::getLanguages(true);

    if (!$nbCron) {
        Catalog::jUpdateValue($prefix . 'PROGRESSMAX_' . $connecteur, Catalog::getNbEciPs($connecteur, null, true));
    }

    $lRefs = Catalog::getListProdInShop($connecteur, $nbCron, 50, true);
    if (!$lRefs) {
        return true;
    }

    $jobLine = $nbCron;
    foreach ($lRefs as $prod) {
        if (time() > $stopTime) {
            break;
        }
        $jobLine++;

        //update name if needed
        $name = Catalog::getCatInfoDiff($prod['reference'], $connecteur, 'name');
        if (!$name) {
            continue;
        }

        try {
            Context::getContext()->shop->id = (int) $prod['id_shop'];
            Shop::setContext(Shop::CONTEXT_SHOP, (int) $prod['id_shop']);
            $product = new Product($prod['id_product'], false, null, (int) $prod['id_shop']);
            if (!Validate::isLoadedObject($product)) {
                continue;
            }
            $tab_name = Catalog::langExpl($name);
            $default_name = isset($tab_name[$iso_lang]) ? $tab_name[$iso_lang] : reset($tab_name);
            $fields_to_update = [];
            foreach ($all_lang as $language) {
                $product->name[(int) $language['id_lang']] = isset($tab_name[$language['iso_code']]) ? $tab_name[$language['iso_code']] : $default_name;
                $fields_to_update['name'][(int) $language['id_lang']] = true;
            }

            $product->setFieldsToUpdate($fields_to_update);
            $product->update();
        } catch (Exception $e) {
            Catalog::logInfo($logger, 'Error in ' . __FUNCTION__ . ' product->update() for pid ' . $prod['id_product'] . ': ' . $e->getMessage());
        }
    }

    return $jobLine;
}

function ufdocs($prefix, $connecteur, $nbCron, $stopTime, $logger, $ec_four)
{
    //reload documents in product if one changed
    if (!$nbCron) {
        Catalog::jUpdateValue($prefix . 'PROGRESSMAX_' . $connecteur, Catalog::getNbEciPs($connecteur));
    }

    $lRefs = Catalog::getListProdInShop($connecteur, $nbCron);
    if (!$lRefs) {
        $list_att_to_del = Db::getInstance()->executeS(
            'SELECT a.id_attachment
            FROM ' . _DB_PREFIX_ . 'attachment a
            LEFT JOIN ' . _DB_PREFIX_ . 'product_attachment pa
            ON a.id_attachment = pa.id_attachment
            WHERE pa.id_attachment IS NULL'
        );

        foreach ($list_att_to_del as $att) {
            $att = new Attachment((int) $att['id_attachment']);
            $att->delete();
        }

        return true;
    }

    $jobLine = $nbCron;
    foreach ($lRefs as $prod) {
        if (time() > $stopTime) {
            break;
        }
        $jobLine++;

        //update documents
        $docs_prod = Catalog::getCatInfoDiff($prod['reference'], $connecteur, 'documents');
        if (!$docs_prod) {
            continue;
        }

        try {
            Attachment::deleteProductAttachments($prod['id_product']);

            if (is_array($documents = Tools::jsonDecode($docs_prod, true))) {
                Catalog::setDocuments($documents, $prod['id_product']);
            }
        } catch (Exception $e) {
            Catalog::logInfo($logger, 'Error in ' . __FUNCTION__ . ' for pid ' . $prod['id_product'] . ' : ' . $e->getMessage());
        }
    }

    return $jobLine;
}

function ufcarrier($prefix, $connecteur, $nbCron, $stopTime, $logger, $ec_four)
{
    //resolve fluctuations in carriers
    if (!$nbCron) {
        Catalog::jUpdateValue($prefix . 'PROGRESSMAX_' . $connecteur, Catalog::getNbEciPs($connecteur));
    }

    $lRefs = Catalog::getListProdInShop($connecteur, $nbCron);
    if (!$lRefs) {
        return true;
    }

    $jobLine = $nbCron;
    foreach ($lRefs as $prod) {
        if (time() > $stopTime && $jobLine > $nbCron) {
            break;
        }
        $jobLine++;
        //update carriers if needed
        $carriers = Catalog::getCatInfoDiff($prod['reference'], $connecteur, 'carriers');
        if (!$carriers) {
            continue;
        }

        try {
            // verify data structure
            if (!is_null($tab_carriers = Tools::jsonDecode($carriers, true))) {
                if (is_array($tab_carriers) && isset($tab_carriers['replace']) && isset($tab_carriers['list'])) {
                    $shops = Product::getShopsByProduct((int) $prod['id_product']);
                    if (!$shops) {
                        continue;
                    }
                    // catch carriers
                    $list_carriers = array();
                    if (!$tab_carriers['replace']) {
                        $list_used_carriers = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS(
                            'SELECT DISTINCT c.id_reference
                            FROM `' . _DB_PREFIX_ . 'product_carrier` pc
                            INNER JOIN `'._DB_PREFIX_.'carrier` c
                            ON (c.`id_reference` = pc.`id_carrier_reference` AND c.`deleted` = 0)
                            WHERE pc.`id_product` = ' . (int) $prod['id_product']
                        );
                        foreach ($list_used_carriers as $used_carrier) {
                            $list_carriers[] = $used_carrier['id_reference'];
                        }
                    }
                    // add carriers
                    if (is_array($tab_carriers['list'])) {
                        foreach ($tab_carriers['list'] as $carrier_name) {
                            $carrier_id_reference = Db::getInstance()->getValue(
                                'SELECT `id_reference`
                                FROM `' . _DB_PREFIX_ . 'carrier`
                                WHERE `name` = "' . pSQL($carrier_name) . '"
                                AND deleted = 0'
                            );
                            if ($carrier_id_reference && !in_array($carrier_id_reference, $list_carriers)) {
                                $list_carriers[] = $carrier_id_reference;
                            }
                        }
                        $list_carriers = array_unique($list_carriers);
                        // associate carriers to product for all shop where the product is
                        $data = array();
                        foreach ($shops as $shop) {
                            foreach ($list_carriers as $carrier) {
                                $data[] = array(
                                    'id_product' => (int) $prod['id_product'],
                                    'id_carrier_reference' => (int) $carrier,
                                    'id_shop' => (int) $shop['id_shop']
                                );
                            }
                        }
                        Db::getInstance()->delete(
                            'product_carrier',
                            'id_product = ' . (int) $prod['id_product']
                        );
                        if ($data) {
                            Db::getInstance()->insert('product_carrier', $data, false, true, Db::INSERT_IGNORE);
                        }
                    }
                }
            }
        } catch (Exception $e) {
            Catalog::logInfo($logger, 'Error in ' . __FUNCTION__ . ' product->update() for pid ' . $prod['id_product'] . ' : ' . $e->getMessage());
        }
    }

    return $jobLine;
}

function rstcarrier($prefix, $connecteur, $nbCron, $stopTime, $logger, $ec_four)
{
    //reset associations of carriers
    if (!$nbCron) {
        Catalog::jUpdateValue($prefix . 'PROGRESSMAX_' . $connecteur, Catalog::getNbEciPs($connecteur));
    }

    $lRefs = Catalog::getListProdInShop($connecteur, $nbCron);
    if (!$lRefs) {
        return true;
    }

    $jobLine = $nbCron;
    foreach ($lRefs as $prod) {
        if (time() > $stopTime && $jobLine > $nbCron) {
            break;
        }
        $jobLine++;
        //update carriers if needed
        $carriers = Catalog::getCatInfos($prod['reference'], $connecteur, 'carriers');
        if (!$carriers) {
            continue;
        }

        try {
            // verify data structure
            if (!is_null($tab_carriers = Tools::jsonDecode($carriers, true))) {
                if (is_array($tab_carriers) && isset($tab_carriers['replace']) && isset($tab_carriers['list'])) {
                    $shops = Product::getShopsByProduct((int) $prod['id_product']);
                    // catch carriers
                    $list_carriers = array();
                    if (!$tab_carriers['replace']) {
                        $list_used_carriers = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS(
                            'SELECT DISTINCT c.id_reference
                            FROM `' . _DB_PREFIX_ . 'product_carrier` pc
                            INNER JOIN `'._DB_PREFIX_.'carrier` c
                            ON (c.`id_reference` = pc.`id_carrier_reference` AND c.`deleted` = 0)
                            WHERE pc.`id_product` = ' . (int) $prod['id_product']
                        );
                        foreach ($list_used_carriers as $used_carrier) {
                            $list_carriers[] = $used_carrier['id_reference'];
                        }
                    }
                    // add carriers
                    if (is_array($tab_carriers['list'])) {
                        foreach ($tab_carriers['list'] as $carrier_name) {
                            $carrier_id_reference = Db::getInstance()->getValue(
                                'SELECT `id_reference`
                                FROM `' . _DB_PREFIX_ . 'carrier`
                                WHERE `name` = "' . pSQL($carrier_name) . '"
                                AND deleted = 0'
                            );
                            if ($carrier_id_reference && !in_array($carrier_id_reference, $list_carriers)) {
                                $list_carriers[] = $carrier_id_reference;
                            }
                        }
                        $list_carriers = array_unique($list_carriers);
                        // associate carriers to product for all shop where the product is
                        $data = array();
                        foreach ($shops as $shop) {
                            foreach ($list_carriers as $carrier) {
                                $data[] = array(
                                    'id_product' => (int) $prod['id_product'],
                                    'id_carrier_reference' => (int) $carrier,
                                    'id_shop' => (int) $shop['id_shop']
                                );
                            }
                        }
                        Db::getInstance()->delete(
                            'product_carrier',
                            'id_product = ' . (int) $prod['id_product']
                        );
                        if ($data) {
                            Db::getInstance()->insert('product_carrier', $data, false, true, Db::INSERT_IGNORE);
                        }
                    }
                }
            }
        } catch (Exception $e) {
            Catalog::logInfo($logger, 'Error in ' . __FUNCTION__ . ' product->update() for pid ' . $prod['id_product'] . ' : ' . $e->getMessage());
        }
    }

    return $jobLine;
}

function ufcategadd($prefix, $connecteur, $nbCron, $stopTime, $logger, $ec_four)
{
    //resolve fluctuations in categories, adds new associations, does not delete old associations
    $context = Context::getContext();
    $id_lang = Configuration::get('PS_LANG_DEFAULT');
    $iso_lang = Language::getIsoById($id_lang);
    $catalog = new Catalog($context);

    if (!$nbCron) {
        Catalog::jUpdateValue($prefix . 'PROGRESSMAX_' . $connecteur, Catalog::getNbEciPs($connecteur, null, true));
    }

    $lRefs = Catalog::getListProdInShop($connecteur, $nbCron, 100, true);
    if (!$lRefs) {
        return true;
    }

    $jobLine = $nbCron;
    foreach ($lRefs as $prod) {
        if (time() > $stopTime) {
            break;
        }
        $jobLine++;
        //update categories if needed
        $category = Catalog::getCatInfoDiff($prod['reference'], $connecteur, array('category', 'category_group', 'fournisseur'));//needed for the function
        if (!$category) {
            continue;
        }

        try {
            $context->shop->id = (int) $prod['id_shop'];
            $catalog->id_shop = (int) $prod['id_shop'];
            $product = new Product((int) $prod['id_product'], false, (int) $id_lang, (int) $prod['id_shop'], $context);
            if (!Validate::isLoadedObject($product)) {
                continue;
            }
            $old_category_default = $product->id_category_default;
            $list_categories = $catalog->setCategories($product, $category, $id_lang, $iso_lang);
            if ($product->id_category_default != $old_category_default) {
                $product->setFieldsToUpdate(['id_category_default' => true]);
                $product->update();
            }
            if ($list_categories) {
                $product->addToCategories($list_categories); //does not suppress old associations
            }
        } catch (Exception $e) {
            Catalog::logInfo($logger, 'catAuto erreur ' . __FUNCTION__ . ' product->update() id '. $prod['id_product'] .' : ' . $e->getMessage());
        }
    }

    return $jobLine;
}

function ufcategrepl($prefix, $connecteur, $nbCron, $stopTime, $logger, $ec_four)
{
    //resolve fluctuations in categories, replaces old associations
    $context = Context::getContext();
    $id_lang = Configuration::get('PS_LANG_DEFAULT');
    $iso_lang = Language::getIsoById($id_lang);
    $catalog = new Catalog($context);

    if (!$nbCron) {
        Catalog::jUpdateValue($prefix . 'PROGRESSMAX_' . $connecteur, Catalog::getNbEciPs($connecteur, null, true));
    }

    $lRefs = Catalog::getListProdInShop($connecteur, $nbCron, 100, true);
    if (!$lRefs) {
        return true;
    }

    $jobLine = $nbCron;
    foreach ($lRefs as $prod) {
        if (time() > $stopTime) {
            break;
        }
        $jobLine++;
        //update categories if needed
        $category = Catalog::getCatInfoDiff($prod['reference'], $connecteur, array('category', 'category_group', 'fournisseur'));//needed for the function
        if (!$category) {
            continue;
        }

        try {
            $context->shop->id = (int) $prod['id_shop'];
            $catalog->id_shop = (int) $prod['id_shop'];
            $product = new Product((int) $prod['id_product'], false, (int) $id_lang, (int) $prod['id_shop'], $context);
            if (!Validate::isLoadedObject($product)) {
                continue;
            }
            $old_category_default = $product->id_category_default;
            $list_categories = $catalog->setCategories($product, $category, $id_lang, $iso_lang);
            if ($product->id_category_default != $old_category_default) {
                $product->setFieldsToUpdate(['id_category_default' => true]);
                $product->update();
            }
            $product->updateCategories($list_categories);
        } catch (Exception $e) {
            Catalog::logInfo($logger, 'catAuto erreur ' . __FUNCTION__ . ' product->update() id '. $prod['id_product'] .' : ' . $e->getMessage());
        }
    }

    return $jobLine;
}

function ufattrn($prefix, $connecteur, $nbCron, $stopTime, $logger, $ec_four)
{
    //resolve fluctuations in attributes (normal using comparison with the old version):
    // delete cobinations and rebuild the missing ones with updateFluctu
    $context = Context::getContext();

    if (!$nbCron) {
        Catalog::jUpdateValue($prefix . 'PROGRESSMAX_' . $connecteur, Catalog::getNbEciPs($connecteur, null, true));
    }

    $lRefs = Catalog::getListProdInShop($connecteur, $nbCron, 100, true);
    if (!$lRefs) {
        return true;
    }

    $id_supplier = Catalog::getEciFournId($connecteur);
    $catalog = new Catalog();

    $jobLine = $nbCron;
    foreach ($lRefs as $prod) {
        if (time() > $stopTime) {
            break;
        }
        $jobLine++;
        //update attributes if needed
        $lAttributes = Catalog::getCatAttInfoDiffForProd($prod['reference'], $connecteur, 'attribute');
        if (!$lAttributes) {
            continue;
        }

        $context->shop->id = (int) $prod['id_shop'];

        foreach ($lAttributes as $attribute) {
            $ids = new ImporterReference($attribute['reference_attribute'], $id_supplier);
            if ((!$ids->id_product) || (!$ids->id_product_attribute)) {
                continue;
            }

            $del = Catalog::combinationDelete($ids->id_product, $ids->id_product_attribute);
            if (true !== $del) {
                Catalog::logInfo($logger, 'Error in ' . __FUNCTION__ . ' : ' . $del);
            }
        }
        $catalog->updateFluctu($prod['reference'], $prod['id_shop'], $connecteur);
    }

    return $jobLine;
}

function ufattrx($prefix, $connecteur, $nbCron, $stopTime, $logger, $ec_four)
{
    //resolve strange fluctuations in attributes (abnormal, using matching of group and value) :
    // if the supplier changed the attributes of some combinations some time ago
    // we delete them and rebuild the missing ones with updateFluctu
    $context = Context::getContext();
    $id_lang = Configuration::get('PS_LANG_DEFAULT');
    $iso_lang = Language::getIsoById($id_lang);

    if (!$nbCron) {
        Catalog::jUpdateValue($prefix . 'PROGRESSMAX_' . $connecteur, Catalog::getNbEciPs($connecteur, null, true));
    }

    $lRefs = Catalog::getListProdInShop($connecteur, $nbCron, 100, true);
    if (!$lRefs) {
        return true;
    }

    $id_supplier = Catalog::getEciFournId($connecteur);

    $jobLine = $nbCron;
    foreach ($lRefs as $prod) {
        if (time() > $stopTime) {
            break;
        }
        $jobLine++;
        //update attributes if needed
        $lAttributes = Catalog::getCatAttInfosForProd($prod['reference'], $connecteur);
        if (!$lAttributes) {
            continue;
        }

        $context->shop->id = (int) $prod['id_shop'];
        $catalog = new Catalog($context);

        $prod_ok = true;
        foreach ($lAttributes as $attribute) {
            $ids = new ImporterReference($attribute['reference_attribute'], $id_supplier, $prod['id_shop']);
            if ((!$ids->id_product) || (!$ids->id_product_attribute)) {
                continue;
            }
            // get attributes and values ids
            $atvs = Db::getInstance()->executeS(
                'SELECT a.id_attribute_group, pac.id_attribute
                FROM ' . _DB_PREFIX_ . 'product_attribute_combination pac
                LEFT JOIN ' . _DB_PREFIX_ . 'attribute a
                ON a.id_attribute = pac.id_attribute
                WHERE pac.id_product_attribute = ' . (int) $ids->id_product_attribute
            );
            $atvs_tab = array();
            foreach ($atvs as $atv) {
                $atvs_tab[$atv['id_attribute_group']] = $atv['id_attribute'];
            }

            //get attributes matched
            $attributes_matched = Catalog::getAttributesMatched(
                Catalog::parseAttAndFeat($attribute['attribute']),
                $id_lang,
                $iso_lang,
                $prod['id_shop'],
                $connecteur,
                $attribute['product_reference'],
                false
            );

            $all_ok = true;
            foreach ($attributes_matched as $attribute_matched) {
                foreach ($attribute_matched as $id_att_g => $id_att_v) {
                    if (isset($atvs_tab[$id_att_g]) && $id_att_v == $atvs_tab[$id_att_g]) {
                        continue;
                    }
                    $all_ok = $prod_ok = false;
                }
            }
            if ($all_ok && count($atvs_tab) == count($attributes_matched)) {
                continue;
            }

            // s'il manque un groupe ou une valeur pour cet attribut
            $del = Catalog::combinationDelete($ids->id_product, $ids->id_product_attribute);
            if (true !== $del) {
                Catalog::logInfo($logger, 'Error in ' . __FUNCTION__ . ' : ' . $del);
            }
        }
        if ($prod_ok) {
            continue;
        }
        $catalog->updateFluctu($prod['reference'], $prod['id_shop'], $connecteur);
    }

    return $jobLine;
}

function ufattrc($prefix, $connecteur, $nbCron, $stopTime, $logger, $ec_four)
{
    //resolve strange fluctuations in attributes (abnormal, using matching of group and value) :
    // if the supplier changed the attributes of some combinations some time ago
    // we update just the attributes... very dangerous if something else changed !!!
    $context = Context::getContext();
    $id_lang = Configuration::get('PS_LANG_DEFAULT');
    $iso_lang = Language::getIsoById($id_lang);

    if (!$nbCron) {
        Catalog::jUpdateValue($prefix . 'PROGRESSMAX_' . $connecteur, Catalog::getNbEciPs($connecteur, null, true));
    }

    $lRefs = Catalog::getListProdInShop($connecteur, $nbCron, 100, true);
    if (!$lRefs) {
        return true;
    }

    $id_supplier = Catalog::getEciFournId($connecteur);

    $jobLine = $nbCron;
    foreach ($lRefs as $prod) {
        if (time() > $stopTime) {
            break;
        }
        $jobLine++;
        //update attributes if needed
        $lAttributes = Catalog::getCatAttInfosForProd($prod['reference'], $connecteur);
        if (!$lAttributes) {
            continue;
        }

        $context->shop->id = (int) $prod['id_shop'];
        $catalog = new Catalog($context);

        $prod_ok = true;
        foreach ($lAttributes as $attribute) {
            $ids = new ImporterReference($attribute['reference_attribute'], $id_supplier, $prod['id_shop']);
            if ((!$ids->id_product) || (!$ids->id_product_attribute)) {
                continue;
            }
            // get attributes and values ids
            $atvs = Db::getInstance()->executeS(
                'SELECT a.id_attribute_group, pac.id_attribute
                FROM ' . _DB_PREFIX_ . 'product_attribute_combination pac
                LEFT JOIN ' . _DB_PREFIX_ . 'attribute a
                ON a.id_attribute = pac.id_attribute
                WHERE pac.id_product_attribute = ' . (int) $ids->id_product_attribute
            );
            $atvs_tab = array();
            foreach ($atvs as $atv) {
                $atvs_tab[$atv['id_attribute_group']] = $atv['id_attribute'];
            }

            //get attributes matched
            $attributes_matched = Catalog::getAttributesMatched(
                Catalog::parseAttAndFeat($attribute['attribute']),
                $id_lang,
                $iso_lang,
                $prod['id_shop'],
                $connecteur,
                $attribute['product_reference'],
                true // force attribute creation if not known
            );

            $all_ok = true;
            foreach ($attributes_matched as $attribute_matched) {
                foreach ($attribute_matched as $id_att_g => $id_att_v) {
                    if (isset($atvs_tab[$id_att_g]) && $id_att_v == $atvs_tab[$id_att_g]) {
                        continue;
                    }
                    $all_ok = $prod_ok = false;
                }
            }
            if ($all_ok && count($atvs_tab) == count($attributes_matched)) {
                continue;
            }

            // s'il manque un groupe ou une valeur pour cette déclinaison
            //suppprimer les attributs de cette déclinaison
            Db::getInstance()->delete(
                'product_attribute_combination',
                'id_product_attribute = ' . (int) $ids->id_product_attribute
            );
            Db::getInstance()->delete(
                'cart_product',
                'id_product_attribute = ' . (int) $ids->id_product_attribute
            );
            //recréer les attaches attributs-déclinaison
            $idsAttribute = [];
            $insert = [];
            foreach ($attributes_matched as $attribute_matched) {
                foreach ($attribute_matched as $id_att_g => $id_att_v) {
                    $idsAttribute[] = (int) $id_att_v;
                    $insert[] = [
                        'id_product_attribute' => (int) $ids->id_product_attribute,
                        'id_attribute' => (int) $id_att_v
                    ];
                }
            }
            if ($idsAttribute) {
                Db::getInstance()->insert(
                    'product_attribute_combination',
                    $insert,
                    false,
                    false,
                    Db::INSERT_IGNORE
                );
                Hook::exec('actionAttributeCombinationSave', array('id_product_attribute' => (int) $ids->id_product_attribute, 'id_attributes' => $idsAttribute));
            }
        }
    }

    return $jobLine;
}

function reassoc($prefix, $connecteur, $nbCron, $stopTime, $logger, $ec_four)
{
    //reassociate categories as in the matching if one associated category was deleted, replaces categories associations
    $id_lang = Configuration::get('PS_LANG_DEFAULT');
    $iso_lang = Language::getIsoById($id_lang);
    $context = Context::getContext();
    $context->language = new Language($id_lang);

    if (!$nbCron) {
        Catalog::jUpdateValue($prefix . 'PROGRESSMAX_' . $connecteur, Catalog::getNbEciPs($connecteur, null, true));
    }

    $lRefs = Catalog::getListProdInShop($connecteur, $nbCron, 100, true);
    if (!$lRefs) {
        return true;
    }

    $old_shop = 0;
    $jobLine = $nbCron;
    $catalog = null;
    foreach ($lRefs as $prod) {
        if (time() > $stopTime) {
            break;
        }
        $jobLine++;
        //update categories if needed
        $category = Catalog::getCatInfos($prod['reference'], $connecteur, array('category', 'category_group', 'fournisseur'));
        if (!$category) {
            continue;
        }

        try {
            if (($prod['id_shop'] != $old_shop) || empty($catalog)) {
                $context->shop = new Shop((int) $prod['id_shop'], $id_lang, (int) $prod['id_shop']);
                $catalog = new Catalog($context);
                $old_shop = $prod['id_shop'];
            }
            
            if (Shop::isFeatureActive()) {
                $product = new Product((int) $prod['id_product'], false, (int) $id_lang, (int) $prod['id_shop'], $context);
            } else {
                $product = new Product((int) $prod['id_product'], false, (int) $id_lang, null, $context);
            }
            if (!Validate::isLoadedObject($product)) {
                continue;
            }
            $old_default_category = $product->id_category_default;
            $old_categories = $product->getCategories();
            sort($old_categories);
            $list_categories = $catalog->setCategories($product, $category, $id_lang, $iso_lang);
            sort($list_categories);
            if ($product->id_category_default != $old_default_category) {
                $product->update();
            }
            if ($old_categories != $list_categories) {
                $product->updateCategories($list_categories); //replaces categories associations
            }
        } catch (Exception $e) {
            Catalog::logInfo($logger, 'Error in ' . __FUNCTION__ . ' product->update() for pid '. $prod['id_product'] .' : ' . $e->getMessage());
        }
    }

    return $jobLine;
}

function chkdftat($prefix, $connecteur, $nbCron, $stopTime, $logger, $ec_four)
{
    //check default attributes
    $context = Context::getContext();
    $id_supplier = Catalog::getEciFournId($connecteur);

    if (!$nbCron) {
        $max = Db::getInstance()->getValue(
            'SELECT COUNT(*)
            FROM ' . _DB_PREFIX_ . 'product
            WHERE id_supplier = ' . (int) $id_supplier
        );
        Catalog::jUpdateValue($prefix . 'PROGRESSMAX_' . $connecteur, Catalog::getNbEciPs($connecteur, null, true));
    }

    $PIDs = Catalog::getListProdInShop($connecteur, $nbCron, 100, true);
    if (!$PIDs) {
        return true;
    }

    $jobLine = $nbCron;
    foreach ($PIDs as $prod) {
        if (time() > $stopTime) {
            break;
        }
        $jobLine++;
        try {
            $product = new Product((int) $prod['id_product'], false, null, null, $context);
            if (!Validate::isLoadedObject($product)) {
                continue;
            }
            $product->checkDefaultAttributes();
        } catch (Exception $e) {
            Catalog::logInfo($logger, 'Error in ' . __FUNCTION__ . ' new product for pid ' . $prod['id_product'] . ' ' . $e->getMessage());
        }
    }

    return $jobLine;
}

function ufweight($prefix, $connecteur, $nbCron, $stopTime, $logger, $ec_four)
{
    //resolve fluctuations in weights
    $id_supplier = Catalog::getEciFournId($connecteur);

    if (!$nbCron) {
        Catalog::jUpdateValue($prefix . 'PROGRESSMAX_' . $connecteur, Catalog::getNbEciPs($connecteur, null, true));
    }

    $lRefs = Catalog::getListProdInShop($connecteur, $nbCron, 100, true);
    if (!$lRefs) {
        return true;
    }

    $old_shop = 0;
    $jobLine = $nbCron;
    foreach ($lRefs as $prod) {
        if (time() > $stopTime) {
            break;
        }
        $jobLine++;
        //update weight if needed
        $ids = new ImporterReference($prod['reference'], $id_supplier, $prod['id_shop']);
        if (!$ids->id_product) {
            continue;
        }
        
        if ($prod['id_shop'] != $old_shop) {
            Context::getContext()->shop = new Shop((int) $prod['id_shop'], null, (int) $prod['id_shop']);
            Shop::setContext(Shop::CONTEXT_SHOP, (int) $prod['id_shop']);
            $old_shop = (int) $prod['id_shop'];
        }

        $ecip = Catalog::getProduct($prod['reference'], $connecteur);

        if (isset($ecip['produit']['weight'])) {
            try {
                if (Shop::isFeatureActive()) {
                    $product = new Product((int) $ids->id_product, false, null, $prod['id_shop']);
                } else {
                    $product = new Product((int) $ids->id_product);
                }
                if (!Validate::isLoadedObject($product)) {
                    continue;
                }
                if ($product->weight != $ecip['produit']['weight']) {
                    $product->weight = (float) $ecip['produit']['weight'];
                    $product->setFieldsToUpdate(['weight' => true]);
                    $product->update();
                }
            } catch (Exception $e) {
                Catalog::logInfo($logger, 'Error in ' . __FUNCTION__ . ' for pid ' . $ids->id_product . ' : ' . $e->getMessage());
            }
        }

        //update combination weight
        if (!$ecip['declinaisons'] || !$ecip['prestashop']['declinaisons']) {
            continue;
        }
        $combinations = array_column(reset($ecip['prestashop']['declinaisons']), 'id_product_attribute', 'reference');
        foreach ($combinations as $reference => $id_pa) {
            $weights = array_column($ecip['declinaisons'], 'weight', 'reference_attribute');
            try {
                if (Shop::isFeatureActive()) {
                    $decl = new Combination($id_pa, null, $prod['id_shop']);
                } else {
                    $decl = new Combination($id_pa);
                }
                if (isset($weights[$reference]) && $decl->weight != $weights[$reference]) {
                    $decl->weight = (float) $weights[$reference];
                    $decl->setFieldsToUpdate(['weight' => true]);
                    $decl->update();
                }
            } catch (Exception $e) {
                Catalog::logInfo($logger, 'Error in ' . __FUNCTION__ . ' for paid ' . $id_pa . ' : ' . $e->getMessage());
            }
        }
    }

    return $jobLine;
}

function forcepict($prefix, $connecteur, $nbCron, $stopTime, $logger, $ec_four)
{
    // force reload images in product and combinations
    if (!$nbCron) {
        Catalog::jUpdateValue($prefix . 'PROGRESSMAX_' . $connecteur, Catalog::getNbEciPs($connecteur));
    }

    $lRefs = Catalog::getListProdInShop($connecteur, $nbCron);
    if (!$lRefs) {
        $list_miniatures = glob(_PS_IMG_DIR_ . '/tmp/product_mini_*.jpg');
        foreach ($list_miniatures as $image) {
            try {
                @unlink($image);
            } catch (Exception $e) {
            }
        }
        return true;
    }
    $id_supplier = Catalog::getEciFournId($connecteur);

    $jobLine = $nbCron;
    foreach ($lRefs as $prod) {
        if (time() > $stopTime) {
            break;
        }
        $jobLine++;

        $pict_prod = Catalog::getCatInfos($prod['reference'], $connecteur, 'pictures');
        $pict_decl = Catalog::getCatAttInfosForProd($prod['reference'], $connecteur, 'pictures');

        if (!$pict_prod && !$pict_decl) {
            continue;
        }

        try {
            $product = new Product($prod['id_product']);
            if (!Validate::isLoadedObject($product)) {
                continue;
            }
            $product->deleteImages();
            Cache::clean('*');
            // Product images from json rich infos or old style list of urls
            if (($tab_images = Tools::jsonDecode($pict_prod, true)) === null) {
                $tab_images = explode('//:://', $pict_prod);
            }
            if (!empty($tab_images[0])) {
                Catalog::setImages($product, $tab_images, true);
            }
            // Combinations images from json rich infos or old style list of urls
            if (!$pict_decl) {
                continue;
            }
            foreach ($pict_decl as $decl) {
                if (($tab_images = Tools::jsonDecode($decl['pictures'], true)) === null) {
                    $tab_images = explode('//:://', $decl['pictures']);
                }
                if (!empty($tab_images[0])) {
                    $ids = new ImporterReference($decl['reference_attribute'], $id_supplier);
                    if (!$ids->id_product_attribute) {
                        continue;
                    }
                    $a_ids_image = Catalog::setImages($product, $tab_images, false);
                    //ajouter à la combinaison
                    $combination = new Combination($ids->id_product_attribute);
                    $combination->setImages($a_ids_image);
                }
            }
        } catch (Exception $e) {
            Catalog::logInfo($logger, 'Error in ' . __FUNCTION__ . ' product->update() for pid ' . $prod['id_product'] . ' : ' . $e->getMessage());
        }
    }

    return $jobLine;
}

function bealorder($prefix, $connecteur, $nbCron, $stopTime, $logger, $ec_four)
{
    // force order of products according to their alphabetic order in "keep" field for BEAL
    if (!$nbCron) {
        Db::getInstance()->execute(
            'DELETE cp
            FROM ' . _DB_PREFIX_ . 'category_product cp
            LEFT JOIN ' . _DB_PREFIX_ . 'product p
            ON cp.id_product = p.id_product
            WHERE p.id_product IS NULL'
        );
        $max = Db::getInstance()->getValue(
            'SELECT COUNT(*)
            FROM ' . _DB_PREFIX_ . 'category_product'
        );
        Catalog::jUpdateValue($prefix . 'PROGRESSMAX_' . $connecteur, $max);
    }

    while (true) {
        if (time() >= $stopTime) {
            break;
        }

        $id_category = Db::getInstance()->getValue(
            'SELECT MIN(id_category)
            FROM ' . _DB_PREFIX_ . 'category_product
            WHERE id_category > ' . (int) $nbCron
        );
        if (!$id_category) {
            return true;
        }
        $nbCron = $id_category;

        //reorder products in this category by their "keep" value in the eci_product_shop table.
        //!!! not multishop firendly !!!
        try {
            Db::getInstance()->execute(
                'UPDATE ' . _DB_PREFIX_ . 'category_product c1
                JOIN (
                    SELECT cp.id_category, cp.id_product, ps.keep, @i := @i+1 new_position
                    FROM ' . _DB_PREFIX_ . 'category_product cp
                    LEFT JOIN ' . _DB_PREFIX_ . 'eci_product_shop ps
                    ON cp.id_product = ps.id_product
                    JOIN (SELECT @i := -1) temp
                    WHERE id_category = ' . (int) $id_category . '
                    AND ps.id_product_attribute = 0
                    ORDER BY ps.keep, ps.reference ASC
                ) c2
                ON c1.id_category = c2.id_category AND c1.id_product = c2.id_product
                SET c1.position = c2.new_position'
            );
        } catch (Exception $e) {
            Catalog::logInfo($logger, 'Error in ' . __FUNCTION__ . ' for cid ' . $id_category . ' : ' . $e->getMessage());
        }
    }

    return $nbCron;
}

function ufdims($prefix, $connecteur, $nbCron, $stopTime, $logger, $ec_four)
{
    //resolve fluctuations in dimension
    $id_supplier = Catalog::getEciFournId($connecteur);

    if (!$nbCron) {
        Catalog::jUpdateValue($prefix . 'PROGRESSMAX_' . $connecteur, Catalog::getNbEciPs($connecteur));
    }

    $lRefs = Catalog::getListProdInShop($connecteur, $nbCron);
    if (!$lRefs) {
        return true;
    }

    $jobLine = $nbCron;
    foreach ($lRefs as $prod) {
        if (time() > $stopTime) {
            break;
        }
        $jobLine++;

        $ids = new ImporterReference($prod['reference'], $id_supplier);
        if (!$ids->id_product) {
            continue;
        }

        $dimensions = Catalog::getCatInfoDiff($prod['reference'], $connecteur, 'dimensions');
        if (!$dimensions) {
            continue;
        }

        try {
            if (is_null($dims = Tools::jsonDecode($dimensions, true))) {
                continue;
            }
            $product = new Product((int) $ids->id_product);
            if (!Validate::isLoadedObject($product)) {
                continue;
            }
            $width = !empty($dims['Lo']) ? (float) $dims['Lo'] : 0;
            $height = !empty($dims['Hi']) ? (float) $dims['Hi'] : 0;
            $depth = !empty($dims['La']) ? (float) $dims['La'] : 0;

            $fields_to_update = [];
            if ($product->width != $width) {
                $product->width = $width;
                $fields_to_update['width'] = true;
            }
            if ($product->height != $height) {
                $product->height = $height;
                $fields_to_update['height'] = true;
            }
            if ($product->depth != $depth) {
                $product->depth = $depth;
                $fields_to_update['depth'] = true;
            }
            if ($fields_to_update) {
                $product->setFieldsToUpdate($fields_to_update);
                $product->update();
            }
        } catch (Exception $e) {
            Catalog::logInfo($logger, 'Error in ' . __FUNCTION__ . ' for pid ' . $ids->id_product . ' : ' . $e->getMessage());
        }
    }

    return $jobLine;
}

function ufpack($prefix, $connecteur, $nbCron, $stopTime, $logger, $ec_four)
{
    //resolve fluctuations in packs composition
    $id_supplier = Catalog::getEciFournId($connecteur);
    $id_lang = Configuration::get('PS_LANG_DEFAULT');

    if (!$nbCron) {
        $max = Db::getInstance()->getValue(
            'SELECT COUNT(DISTINCT s.id_product)
            FROM '._DB_PREFIX_.'eci_product_shop s
            LEFT JOIN '._DB_PREFIX_.'eci_catalog_pack p
            ON p.pack_reference = s.reference AND p.fournisseur = s.fournisseur
            WHERE s.fournisseur = "' . pSQL($connecteur) . '"
            AND p.pack_reference IS NOT NULL'
        );
        Catalog::jUpdateValue($prefix.'PROGRESSMAX_'.$connecteur, $max);
    }

    $lRefs = Db::getInstance()->executeS(
        'SELECT DISTINCT s.id_product, s.reference
        FROM '._DB_PREFIX_.'eci_product_shop s
        LEFT JOIN '._DB_PREFIX_.'eci_catalog_pack p
        ON p.pack_reference = s.reference AND p.fournisseur = s.fournisseur
        WHERE s.fournisseur = "' . pSQL($connecteur) . '"
        AND p.pack_reference IS NOT NULL
        ORDER BY s.id_product
        LIMIT ' . (int) $nbCron . ',50'
    );
    if (!$lRefs) {
        return true;
    }

    $jobLine = $nbCron;
    foreach ($lRefs as $prod) {
        if (time() > $stopTime) {
            break;
        }
        $jobLine++;

        $ids = new ImporterReference($prod['reference'], $id_supplier);
        if (!$ids->id_product) {
            continue;
        }
        if ($prod['id_product'] != $ids->id_product) {
            Catalog::logWarning($logger, 'catAuto warning ' . __FUNCTION__ . ' pid ' . $ids->id_product . ' is supposed to be pid ' . $prod['id_product'] . ' in eci_product_shop');
        }

        try {
            //vérifier que c'est un PS-pack
            if (!Pack::isPack($ids->id_product)) {
                Catalog::logWarning($logger, 'catAuto warning ' . __FUNCTION__ . ' pid ' . $ids->id_product . ' is supposed to be a pack but is not');
//                continue;
            }

            //récupérer la compo PS du pack
            $items = Pack::getItems($ids->id_product, $id_lang);

            //récupérer la compo ECI du pack
            $components = Catalog::getPackComponents($prod['reference'], $connecteur);

            //comparer et vérifier si tous les composants sont en boutique, stocker les ids
            $same = true;
            $all_here = true;
            $list_c = array();
            if (!$components  || count($items) != count($components)) {
                $same = false;
            } else {
                foreach ($components as $component) {
                    $cmp_ids = new ImporterReference($component['reference'], $id_supplier);
                    if (!$cmp_ids->id_product) {
                        $all_here = false;
                        break;
                    }
                    foreach ($items as $i => $item) {
                        if ($cmp_ids->id_product == $item->id && $cmp_ids->id_product_attribute == $item->id_pack_product_attribute && $component['quantity'] == $item->pack_quantity) {
                            unset($items[$i]);
                            break;
                        }
                    }
                    $list_c[$component['reference']] = array('ids' => $cmp_ids, 'quantity' => $component['quantity']);
                }
                $same = !(bool)$items;
            }
            if ($same) {
                continue;
            }

            //si il manque des composants et qu'on a pas le droit de forcer l'import des composants on s'arrête là
            if (!$all_here && !$ec_four->config['FORCE_PACK_COMP']) {
                $pack = new Product($ids->id_product, false, $id_lang);
                if (!Validate::isLoadedObject($pack)) {
                    continue;
                }
                $pack->active = false;
                $pack->update();
                Catalog::logInfo($logger, 'catAuto info ' . __FUNCTION__ . ' pid ' . $ids->id_product . ' : is an obsolete pack that will not be updated because of the config');
                continue;
            }

            //si pas pareil regénérer le PS-pack depuis l'ECI-pack
            if (count($list_c) != count($components)) {
                $product = new Product($ids->id_product);
                if (!Validate::isLoadedObject($product)) {
                    continue;
                }
                $catalog = new Catalog();
                foreach ($components as $component) {
                    if (empty($list_c[$component['reference']])) {
                        //créer produit manquant
                        $catalog->setProductToShop(
                            array(
                                'reference'=>$component['parent_reference'],
                                'id_shop' => $product->id_shop_default,
                                'fournisseur' => $connecteur
                            )
                        );
                        //récupérer ses ids
                        $cmp_ids = new ImporterReference($component['reference'], $id_supplier);
                        $list_c[$component['reference']] = array('ids' => $cmp_ids, 'quantity' => $component['quantity']);
                    }
                }
            }
            Pack::deleteItems($ids->id_product);
            foreach ($list_c as $c_infos) {
                Pack::addItem(
                    (int) $ids->id_product,
                    (int) $c_infos['ids']->id_product,
                    (int) $c_infos['quantity'],
                    (int) $c_infos['ids']->id_product_attribute
                );
            }
        } catch (Exception $e) {
            Catalog::logError($logger, 'catAuto erreur ' . __FUNCTION__ . ' pid ' . $ids->id_product . ' : ' . $e->getMessage());
        }
    }

    return $jobLine;
}

function uftax($prefix, $connecteur, $nbCron, $stopTime, $logger, $ec_four)
{
    //resolve fluctuations in taxes
    $context = Context::getContext();
    $id_supplier = Catalog::getEciFournId($connecteur);
    $force = false;

    if (!$nbCron) {
        Catalog::jUpdateValue($prefix . 'PROGRESSMAX_' . $connecteur, Catalog::getNbEciPs($connecteur, null, true));
    }

    $lRefs = Catalog::getListProdInShop($connecteur, $nbCron, 100, true);
    if (!$lRefs) {
        return true;
    }

    $jobLine = $nbCron;
    $old_id_shop = 0;
    foreach ($lRefs as $prod) {
        if (time() > $stopTime) {
            break;
        }
        $jobLine++;

        $ids = new ImporterReference($prod['reference'], $id_supplier, $prod['id_shop']);
        if (!$ids->id_product) {
            continue;
        }

        if ($force) {
            $taxes = Catalog::getCatInfos($prod['reference'], $connecteur, 'rate');
        } else {
            $taxes = Catalog::getCatInfoDiff($prod['reference'], $connecteur, 'rate');
        }

        if ($taxes) {
            try {
                if ($old_id_shop != $prod['id_shop']) {
                    $context->shop = new Shop((int) $prod['id_shop']);
                    $catalog = new Catalog($context);
                    $old_id_shop = $prod['id_shop'];
                }
                if (Shop::isFeatureActive()) {
                    $product = new Product((int) $ids->id_product, false, null, $prod['id_shop']);
                } else {
                    $product = new Product((int) $ids->id_product);
                }
                if (!Validate::isLoadedObject($product)) {
                    continue;
                }
                $id_tax_rules_group = $catalog->getTaxMatched($taxes);
                if ($id_tax_rules_group && $id_tax_rules_group != $product->id_tax_rules_group) {
                    $product->id_tax_rules_group = (int) $id_tax_rules_group;
                    $product->setFieldsToUpdate(['id_tax_rules_group' => true]);
                    $product->update();
                }
            } catch (Exception $e) {
                Catalog::logInfo($logger, 'Error in '.__FUNCTION__.' for pid ' . $ids->id_product . ' : ' . $e->getMessage());
            }
        }
    }

    return $jobLine;
}

function ufprices($prefix, $connecteur, $nbCron, $stopTime, $logger, $ec_four)
{
    //resolve fluctuations in prices
    $id_supplier = Catalog::getEciFournId($connecteur);
    $sw_force = false;

    if (!$nbCron) {
        Catalog::jUpdateValue($prefix . 'PROGRESSMAX_' . $connecteur, Catalog::getNbEciPs($connecteur, null, true));
    }

    $lRefs = Catalog::getListProdInShop($connecteur, $nbCron, 100, true);
    if (!$lRefs) {
        return true;
    }

    $jobLine = $nbCron;
    foreach ($lRefs as $prod) {
        if (time() > $stopTime) {
            break;
        }
        $jobLine++;

        $ids = new ImporterReference($prod['reference'], $id_supplier, $prod['id_shop']);
        if (!$ids->id_product) {
            continue;
        }

        if ($sw_force) {
            $p_prices = Catalog::getCatInfos($prod['reference'], $connecteur, array('price', 'pmvc'));
            $a_prices = Catalog::getCatAttInfosForProd($prod['reference'], $connecteur, array('price', 'pmvc'));
        } else {
            $p_prices = Catalog::getCatInfoDiff($prod['reference'], $connecteur, array('price', 'pmvc'));
            $a_prices = Catalog::getCatAttInfoDiffForProd($prod['reference'], $connecteur, array('price', 'pmvc'));
        }

        if ($p_prices) {
            try {
                $product = new Product((int) $ids->id_product, false, null, $prod['id_shop']);
                if (!Validate::isLoadedObject($product)) {
                    continue;
                }
                $fields_to_update = [];
                if ($product->wholesale_price != $p_prices['price']) {
                    $product->wholesale_price = $p_prices['price'];
                    $fields_to_update['wholesale_price'] = true;
                }
                if ($product->price != $p_prices['pmvc']) {
                    $product->price = $p_prices['pmvc'];
                    $fields_to_update['price'] = true;
                }
                if ($fields_to_update) {
                    $product->setFieldsToUpdate($fields_to_update);
                    $product->update();
                }
            } catch (Exception $e) {
                Catalog::logInfo($logger, 'erreur '.__FUNCTION__.' pid ' . $ids->id_product . ' : ' . $e->getMessage());
            }
        }

        if (!$a_prices) {
            continue;
        }

        foreach ($a_prices as $v_prices) {
            $ids = new ImporterReference($v_prices['reference_attribute'], $id_supplier, $prod['id_shop']);
            if (!$ids->id_product_attribute) {
                continue;
            }
            try {
                $combination = new Combination((int) $ids->id_product_attribute, null, $prod['id_shop']);
                if (!Validate::isLoadedObject($combination)) {
                    continue;
                }
                $fields_to_update = [];
                if ($combination->wholesale_price != $v_prices['price']) {
                    $combination->wholesale_price = $v_prices['price'];
                    $fields_to_update['wholesale_price'] = true;
                }
                if ($combination->price != $v_prices['pmvc']) {
                    $combination->price = $v_prices['pmvc'];
                    $fields_to_update['price'] = true;
                }
                if ($fields_to_update) {
                    $combination->setFieldsToUpdate($fields_to_update);
                    $combination->update();
                }
            } catch (Exception $e) {
                Catalog::logInfo($logger, 'Error in ' . __FUNCTION__ . ' for pid ' . $ids->id_product . '-' . $ids->id_product_attribute . ' : ' . $e->getMessage());
            }
        }
    }

    return $jobLine;
}

function prules($prefix, $connecteur, $nbCron, $stopTime, $logger, $ec_four)
{
    $shops = Db::getInstance()->executeS(
        'SELECT DISTINCT ps.id_shop
        FROM ' . _DB_PREFIX_ . 'shop ps
        LEFT JOIN ' . _DB_PREFIX_ . 'eci_config c
        ON ps.id_shop = c.id_shop
        LEFT JOIN ' . _DB_PREFIX_ . 'eci_pricerules r
        ON ps.id_shop = r.id_shop AND c.fournisseur = r.fournisseur
        WHERE c.fournisseur = "' . pSQL($connecteur) . '"
        AND c.name = "ACTIVE" AND c.value = 1
        AND r.active = 1
        ORDER BY ps.id_shop'
    );

    if (!$nbCron) {
        Catalog::jUpdateValue($prefix . 'PROGRESSMAX_' . $connecteur, count($shops));
    }

    if (!isset($shops[$nbCron])) {
        return true;
    }

    Pricerules::matchingRules($connecteur, $shops[$nbCron]['id_shop']);

    return (count($shops) == (1 + $nbCron) ? true : 1 + $nbCron);
}

function suppcarriers($prefix, $connecteur, $nbCron, $stopTime, $logger, $ec_four)
{
    //reassociate supplier carriers if configured
    $context = Context::getContext();
    $id_supplier = Catalog::getEciFournId($connecteur);

    if (!$nbCron) {
        Catalog::jUpdateValue($prefix . 'PROGRESSMAX_' . $connecteur, Catalog::getNbEciPs($connecteur, null, true));
    }

    $lRefs = Catalog::getListProdInShop($connecteur, $nbCron, 100, true);
    if (!$lRefs) {
        return true;
    }

    $scClass = 'Suppcarrier' . $connecteur;
    $list_suppcarriers = $scClass::getSuppcarriers($connecteur);
    foreach ($list_suppcarriers as &$suppcarrier) {
        $carrier_id_reference = (int) Db::getInstance()->getValue(
            'SELECT id_reference
            FROM ' . _DB_PREFIX_ . 'carrier
            WHERE id_carrier = ' . (int) $suppcarrier['id_carrier']
        );
        $suppcarrier['id_reference'] = $carrier_id_reference;
    }
    $config = Catalog::getECIConfigValueMultiShop('ASSOC_CARRIERS', $connecteur);

    $jobLine = $nbCron;
    $old_id_shop = 0;
    foreach ($lRefs as $prod) {
        if (time() > $stopTime) {
            break;
        }
        $jobLine++;

        $ids = new ImporterReference($prod['reference'], $id_supplier, $prod['id_shop']);
        if (!$ids->id_product) {
            continue;
        }

        try {
            if ($config[$prod['id_shop']]) {
                if (empty($context->shop->id) || $context->shop->id != $old_id_shop) {
                    $context->shop = new Shop((int) $prod['id_shop']);
                }
                $old_id_shop = $context->shop->id;

                $product = new Product($ids->id_product, false, null, $prod['id_shop']);
                if (!Validate::isLoadedObject($product)) {
                    continue;
                }
                $list_carriers = array();
                foreach ($list_suppcarriers as $suppcarrier) {
                    if ($suppcarrier['id_reference'] && !in_array($suppcarrier['id_reference'], $list_carriers)) {
                        $list_carriers[] = $suppcarrier['id_reference'];
                    }
                }
                if ($list_carriers) {
                    $list_used_carriers = $product->getCarriers();
                    foreach ($list_used_carriers as $used_carrier) {
                        $list_carriers[] = $used_carrier['id_reference'];
                    }
                    $product->setCarriers($list_carriers);
                }
            }
        } catch (Exception $e) {
            Catalog::logInfo($logger, 'Error in ' . __FUNCTION__ . ' : ' . $e->getMessage());
        }
    }

    return $jobLine;
}

function upseo($prefix, $connecteur, $nbCron, $stopTime, $logger, $ec_four)
{
    //update seo
    if (!$nbCron) {
        Catalog::jUpdateValue($prefix . 'PROGRESSMAX_' . $connecteur, Catalog::getNbEciPs($connecteur, null, true));
    }

    $lRefs = Catalog::getListProdInShop($connecteur, $nbCron, 100, true);
    if (!$lRefs) {
        return true;
    }

    $jobLine = $nbCron;
    $old_id_shop = 0;
    foreach ($lRefs as $prod) {
        if (time() > $stopTime) {
            break;
        }
        $jobLine++;

        $context = Context::getContext();
        if (empty($context->shop->id) || $context->shop->id != $old_id_shop) {
            $context->shop = new Shop((int) $prod['id_shop']);
        }
        $old_id_shop = $context->shop->id;

        $catalog = new Catalog($context);
        $product = new Product($prod['id_product']);

        $catalog->updateProductSEO($product);
    }

    return $jobLine;
}

function reparepicture($prefix, $connecteur, $nbCron, $stopTime, $logger, $ec_four)
{
    // force reload images in product and combinations if one file does not exist
    if (!$nbCron) {
        Catalog::jUpdateValue($prefix . 'PROGRESSMAX_' . $connecteur, Catalog::getNbEciPs($connecteur));
    }

    $lRefs = Catalog::getListProdInShop($connecteur, $nbCron, 100);
    if (!$lRefs) {
        $list_miniatures = glob(_PS_IMG_DIR_ . '/tmp/product_mini_*.jpg');
        foreach ($list_miniatures as $image) {
            try {
                @unlink($image);
            } catch (Exception $e) {
                $logger->logInfo('échec de tentative de suppression de miniature ' . $image . ' ' . $e->getMessage());
            }
        }
        return true;
    }
    $id_supplier = Catalog::getEciFournId($connecteur);
    $id_lang = Configuration::get('PS_LANG_DEFAULT');

    $jobLine = $nbCron;
    foreach ($lRefs as $prod) {
        if (time() > $stopTime) {
            break;
        }
        $jobLine++;

        $pict_prod = Catalog::getCatInfos($prod['reference'], $connecteur, array('pictures'));
        $pict_decl = Catalog::getCatAttInfosForProd($prod['reference'], $connecteur, array('pictures'));

        if (!$pict_prod && !$pict_decl) {
            continue;
        }

        try {
            $product = new Product($prod['id_product']);
            if (!Validate::isLoadedObject($product) || !$product->id) {
                continue;
            }
            $images = $product->getImages($id_lang);
            if (is_array($images) && $images) {
                $all_here = true;
                foreach ($images as $image) {
                    $img = new Image((int)$image['id_image']);
                    $files = glob(_PS_PROD_IMG_DIR_ . $img->getImgPath() . '.*');
                    if (!count($files)) {
                        $all_here = false;
                        break;
                    }
                }
                if ($all_here) {
                    continue;
                }
            }

            $product->deleteImages();
            Cache::clean('*');
            // Product images from json rich infos or old style list of urls
            if ($pict_prod) {
                if (($tab_images = Tools::jsonDecode($pict_prod, true)) === null) {
                    $tab_images = explode('//:://', $pict_prod);
                }
                if ($tab_images && !empty($tab_images[0])) {
                    Catalog::setImages($product, $tab_images, true);
                }
            }
            // Combinations images from json rich infos or old style list of urls
            if (!$pict_decl) {
                continue;
            }
            foreach ($pict_decl as $decl) {
                if (($tab_images = Tools::jsonDecode($decl['pictures'], true)) === null) {
                    $tab_images = explode('//:://', $decl['pictures']);
                }
                if ($tab_images && !empty($tab_images[0])) {
                    $ids = new ImporterReference($decl['reference_attribute'], $id_supplier);
                    if (!$ids->id_product_attribute) {
                        continue;
                    }
                    $a_ids_image = Catalog::setImages($product, $tab_images, false);
                    if (!$a_ids_image || !is_array($a_ids_image)) {
                        continue;
                    }
                    //ajouter à la combinaison
                    $combination = new Combination($ids->id_product_attribute);
                    if (!Validate::isLoadedObject($combination) || !$combination->id) {
                        continue;
                    }
                    $combination->setImages($a_ids_image);
                }
            }
        } catch (Exception $e) {
            Catalog::logInfo($logger, 'catAuto erreur ' . __FUNCTION__ . ' product->update() : ' . $e->getMessage());
        }
    }

    return $jobLine;
}

function ufref($prefix, $connecteur, $nbCron, $stopTime, $logger, $ec_four)
{
    //resolve fluctuations in product "reference"
    if (!$nbCron) {
        Catalog::jUpdateValue($prefix . 'PROGRESSMAX_' . $connecteur, Catalog::getNbEciPs($connecteur));
    }

    $lRefs = Catalog::getListProdInShop($connecteur, $nbCron, 100);
    if (!$lRefs) {
        return true;
    }

    $jobLine = $nbCron;
    foreach ($lRefs as $prod) {
        if (time() > $stopTime) {
            break;
        }
        $jobLine++;

        //update product reference if needed
        $reference = Catalog::getCatInfoDiff($prod['reference'], $connecteur, 'reference');

        if ($reference) {
            try {
                $product = new Product($prod['id_product']);
                $product->reference = $reference;
                $product->setFieldsToUpdate(['reference' => true]);
                $product->update();
            } catch (Exception $e) {
                Catalog::logInfo($logger, 'Error in ' . __FUNCTION__ . ' for pid ' . $prod['id_product'] . ' : ' . $e->getMessage());
            }
        }
    }

    return $jobLine;
}


function exception_error_handler($severity, $message, $file, $line)
{
    if (!(error_reporting() & $severity)) {
        // Ce code d'erreur n'est pas inclu dans error_reporting

        return;
    }
    throw new ErrorException($message, 0, $severity, $file, $line);
}

function splitResponse($data = '')
{
    $matches = array();
    preg_match_all('/([^{}]*)(\{[^}]+\})/', $data, $matches);

    return array(
        'errtxt' => $matches[1][0],
        'json' => $matches[2][0],
    );
}

function splitPhpErrors($data = '')
{
    return array_filter(array_map('trim', explode('<br />', $data)));
}

function isECIerror($errors = '')
{
    if (!is_array($errors)) {
        $errors = array($errors);
    }

    foreach ($errors as $error) {
        if (preg_match('/\/modules\/ecicdiscountpro\//', $error)) {
            return true;
        }
    }
    return false;
}
