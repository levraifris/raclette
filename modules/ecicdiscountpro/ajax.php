<?php
/**
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to a commercial license from SARL Ether Creation
 * Use, copy, modification or distribution of this source file without written
 * license agreement from the SARL Ether Creation is strictly forbidden.
 * In order to obtain a license, please contact us: contact@ethercreation.com
 * ...........................................................................
 * INFORMATION SUR LA LICENCE D'UTILISATION
 *
 * L'utilisation de ce fichier source est soumise a une licence commerciale
 * concedee par la societe Ether Creation
 * Toute utilisation, reproduction, modification ou distribution du present
 * fichier source sans contrat de licence ecrit de la part de la SARL Ether Creation est
 * expressement interdite.
 * Pour obtenir une licence, veuillez contacter la SARL Ether Creation a l'adresse: contact@ethercreation.com
 * ...........................................................................
 *
 * @author    Ether Creation SARL <contact@ethercreation.com>
 * @copyright 2008-2020 Ether Creation SARL
 * @license   Commercial license
 * International Registered Trademark & Property of Ether Creation SARL
 */

require_once dirname(__FILE__) . '/../../config/config.inc.php';
require_once dirname(__FILE__) . '/class/catalog.class.php';
use ecicdiscountpro\Catalog;

set_error_handler("exception_error_handler");

if (Tools::getValue('ec_token') != Catalog::getInfoEco('ECO_TOKEN')) {
    header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
    header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
    header('Cache-Control: no-store, no-cache, must-revalidate');
    header('Cache-Control: post-check=0, pre-check=0', false);
    header('Pragma: no-cache');
    Tools::redirect('Location: ../');
    exit();
}

$id_prg = Tools::getValue('prg', 0);
if ($id_prg) {
    $nextCron = Catalog::getNextCron($id_prg);
    if ($nextCron) {
        $reps = Tools::file_get_contents($nextCron['link'] . '&prg=' . $id_prg . '&pos=' . $nextCron['position']);
        echo $reps;
    }
}

$id_shop = Tools::getValue('id_shop', Configuration::get('PS_SHOP_DEFAULT'));
$context = Context::getContext();
$context->shop->id = $id_shop;
$catalog = new Catalog($context);

$connecteur = Tools::getValue('connecteur', ecicdiscountpro::GEN);

switch ((int) Tools::getValue('majsel')) {
    case 1:
        echo $catalog->getProductInfo(Tools::getValue('reference'), $connecteur, Tools::getValue('iso_code'));
        break;
    case 13:
        echo $catalog->countSynchro();
        break;
    case 26:
        echo Catalog::synchroManuelOrder(Tools::getValue('idc'), Tools::getValue('typ'), $connecteur);
        break;
    case 27:
        echo Catalog::generateSynchroOrder(Tools::getValue('idc'));
        break;
    case 29:
        echo $catalog->checkFolder(Tools::getValue('fourn'));
        break;
    case 30:
        $form_nom = Tools::getValue('nom');
        $form_prenom = Tools::getValue('prenom');
        $form_societe = Tools::getValue('societe');
        $form_tel = Tools::getValue('tel');
        $form_mail = Tools::getValue('mail');
        $form_url = Tools::getValue('url');
        $form_comm = Tools::getValue('comm');
        $form_plateforme = Tools::getValue('plateforme');
        $form_cle2 = Tools::getValue('cle2');
        echo Tools::file_get_contents('http://shippingdrop.fr/lic/ins.php?nom=' . $form_nom . '&prenom=' . $form_prenom . '&societe=' . $form_societe . '&tel=' . $form_tel . '&mail=' . $form_mail . '&url=' . $form_url . '&comm=' . $form_comm . '&pf=' . $form_plateforme . '&cle2=' . $form_cle2);
        break;
    case 31:
        $name_fournisseur = Tools::getValue('plateforme');
        $dirPath = 'gen/' . $name_fournisseur . '/';
        echo $catalog->deleteFolder($dirPath);
        break;
    case 33:
        echo Catalog::getEciFournId($connecteur);
        break;
    case 34:
        echo $catalog->saveFeatureMatching(Tools::getValue('id_feature_eco'), Tools::getValue('id_feature'));
        break;
    case 35:
        echo $catalog->saveAttributeMatching(Tools::getValue('id_attribute_eco'), Tools::getValue('id_attribute'), Tools::getValue('id_attribute_eco_old'));
        break;
    case 37:
        echo $catalog->setAttributeValue(Tools::getValue('id'), Tools::getValue('page'));
        break;
    case 38:
        echo $catalog->setAddAttrMatch(Tools::getValue('id'));
        break;
    case 39:
        echo $catalog->addAttrMatch(Tools::getValue('b'), Tools::getValue('v'), Tools::getValue('s'));
        break;
    case 40:
        echo $catalog->delAttrMatch(Tools::getValue('v'), Tools::getValue('s'));
        break;
    case 45:
        $gen = Catalog::getGenClassStatic($connecteur, $id_shop);
        if (!$gen) {
            exit('no gen');
        }
        echo Catalog::updateParamFournisseur(
            Tools::getValue('eci_cdiscountpro_conf'),
            ecicdiscountpro::GEN,
            $id_shop,
            array_merge(Catalog::$tabParamsValid, isset($gen->valid_conf_keys)?$gen->valid_conf_keys:array()),
            array_merge(Catalog::$tabParamsNumeric, isset($gen->numeric_conf_keys)?$gen->numeric_conf_keys:array()),
            array_merge(Catalog::$tabParamsMultiple, isset($gen->multiple_conf_keys)?$gen->multiple_conf_keys:array()),
            array_merge(Catalog::$tabParamsArray, isset($gen->array_conf_keys)?$gen->array_conf_keys:array()),
            array_merge(Catalog::$tabParamsAllShops, isset($gen->allshop_keys)?$gen->allshop_keys:array())
        );
        break;
    case 55:
        echo Catalog::displayInfoRetour(Tools::getValue('message'), Tools::getValue('etat'));
        break;
    case 75:
        echo Catalog::getCpanelData(Tools::getValue('prefix'), Tools::getValue('suffix', false));
        break;
    case 85:
        echo $catalog->saveFeatureMatching(Tools::getValue('id_feature_eco'), Tools::getValue('id_feature'), Tools::getValue('id_feature_eco_old'));
        break;
    case 87:
        echo $catalog->setFeatureValue(Tools::getValue('id'), Tools::getValue('page'));
        break;
    case 88:
        echo $catalog->setAddFeatMatch(Tools::getValue('id'));
        break;
    case 89:
        echo $catalog->addFeatMatch(Tools::getValue('b'), Tools::getValue('v'), Tools::getValue('s'));
        break;
    case 90:
        echo $catalog->delFeatMatch(Tools::getValue('v'), Tools::getValue('s'));
        break;
    default:
        break;
}

exit();

function exception_error_handler($severity, $message, $file, $line)
{
    if (!(error_reporting() & $severity)) {
        // Ce code d'erreur n'est pas inclu dans error_reporting

        return;
    }
    throw new ErrorException($message, 0, $severity, $file, $line);
}