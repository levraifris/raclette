<?php
/**
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to a commercial license from SARL Ether Creation
 * Use, copy, modification or distribution of this source file without written
 * license agreement from the SARL Ether Creation is strictly forbidden.
 * In order to obtain a license, please contact us: contact@ethercreation.com
 * ...........................................................................
 * INFORMATION SUR LA LICENCE D'UTILISATION
 *
 * L'utilisation de ce fichier source est soumise a une licence commerciale
 * concedee par la societe Ether Creation
 * Toute utilisation, reproduction, modification ou distribution du present
 * fichier source sans contrat de licence ecrit de la part de la SARL Ether Creation est
 * expressement interdite.
 * Pour obtenir une licence, veuillez contacter la SARL Ether Creation a l'adresse: contact@ethercreation.com
 * ...........................................................................
 *
 * @author    Ether Creation SARL <contact@ethercreation.com>
 * @copyright 2008-2021 Ether Creation SARL
 * @license   Commercial license
 * International Registered Trademark & Property of Ether Creation SARL
 */

require_once dirname(__FILE__) . '/class/catalog.class.php';
require_once dirname(__FILE__) . '/class/ecitranslations.class.php';
require_once dirname(__FILE__) . '/class/reference.class.php';
require_once dirname(__FILE__) . '/class/eciseo.class.php';
require_once dirname(__FILE__) . '/class/ecisuppcarrier.class.php';
require_once dirname(__FILE__) . '/class/ecipricerules.class.php';
require_once dirname(__FILE__) . '/class/eciapiauth.class.php';
require_once dirname(__FILE__) . '/gen/cdiscountpro/class/eccdiscountpro.php';
use ecicdiscountpro\Catalog;
use ecicdiscountpro\EciTranslations;
use ecicdiscountpro\ImporterReference;

if (!defined('_CAN_LOAD_FILES_')) {
    exit();
}

class EcicdiscountproFuncs extends CarrierModule
{

    const GEN = 'cdiscountpro';
    public $ucfirst_gen;

    public $id_carrier;

    private $_postErrors = array();

    const INSTALL_SQL_FILE = 'create';
    const UNINSTALL_SQL_FILE = 'drop';

    public $protocol = 'http://';

    /**
     * envoi des commandes passées (ERP) ou payées (normal)
     */
    const ERP_ORDERS = false;

    /**
     * si non nul, le hook sera appelé mais lachera la commande et elle sera déclenchée après DELAY_ORDER secondes
     */
    const DELAY_ORDERS = 0;

    /**
     * action sur connexion de client
     */
    const HOOK_CUSTCONX = false;

    /**
     * demo mode
     */
    const DEMO = false;

    /**
     * models to install
     */
//    public $models = array('seo', 'suppcarrier', 'pricerules'); //normal
    public $models = array('seo', 'suppcarrier', 'pricerules', 'apiauth'); //with apiauth


    public function __construct(Context $context = null)
    {
        $this->ucfirst_gen = Tools::ucfirst(self::GEN);
        parent::__construct();
        if (!$context) {
            $context = Context::getContext();
        }
        $this->protocol = (((Configuration::get('PS_SSL_ENABLED') == 1) &&
                (Configuration::get('PS_SSL_ENABLED_EVERYWHERE') == 1)) ? 'https://' : 'http://' );
    }

    public function getProtocol()
    {
        $this->protocol = (((Configuration::get('PS_SSL_ENABLED') == 1) &&
                (Configuration::get('PS_SSL_ENABLED_EVERYWHERE') == 1)) ? 'https://' : 'http://' );

        return $this->protocol;
    }

    public function install()
    {
        set_time_limit(0);

        $base_exists = (bool) Db::getInstance()->executeS('SHOW TABLES LIKE "' . _DB_PREFIX_ . 'eci_catalog"');

        if (!parent::install()) {
            return 'Error 0 : error during the parent Install';
        }

        if (!$this->executeSQLFile(self::INSTALL_SQL_FILE)) {
            return 'Error 1 : error during the execution of create.sql file';
        }

        if ($base_exists) {
            $requests = array();
            require_once dirname(__FILE__) . '/sql/upgrade.php';
            if (!empty($requests)) {
                foreach ($requests as $query) {
                    if (true !== ($response = self::executeQuery($query))) {
                        return 'Error 1.1 : error during the execution of upgrade.php file : ' . $response;
                    }
                }
            } else {
                return 'Error 1.2 : error :no upgrade.php file';
            }
        }

        if (!$this->installModels()) {
            return 'Error 2 : error during the installation of models for "' . self::GEN . '"';
        }

        if (!$this->installGen()) {
            return 'Error 3 : error during the installation of gen "' . self::GEN . '"';
        }

        if (version_compare(_PS_VERSION_, '1.5', '<')) {
            return "Error 4.1 : Register the hooks would fail (PS<1.5)";
        }
        if (!$this->installHooks()) {
            return "Error 4.2 : Register the hooks failed";
        }

        //install general supplier
        self::installECISupplier();

        // install token if not done by another gen
        if (!self::getInfoEco('ECO_TOKEN')) {
            self::updateInfoEco('ECO_TOKEN', md5(time() . _COOKIE_KEY_));
        }

        // install employee if not done by another gen
        if (!self::getInfoEco('ID_EMPLOYEE')) {
            self::updateInfoEco('ID_EMPLOYEE', Context::getContext()->employee->id ?? 1);
        }

        if (!$this->installTabs() || !self::revMod()) {
            return 'Error 5 : error during the installation of tabs for "' . self::GEN . '"';
        }

        return true;
    }
    
    public static function installECISupplier()
    {
        //install general supplier
        $id_eci_supplier = Supplier::getIdByName('EcImport');
        if ($id_eci_supplier) {
            $eci_supplier = new Supplier($id_eci_supplier);
        } else {
            $id_eci_supplier = Catalog::getInfoEco('ECI_SUPPLIER');
            $eci_supplier = new Supplier();
            $eci_supplier->name = 'EcImport';
            if ($id_eci_supplier) {
                $eci_supplier->id = (int) $id_eci_supplier;
                $eci_supplier->force_id = true;
                Db::getInstance()->delete('supplier_shop', 'id_supplier = ' . (int) $id_eci_supplier);
            }
            $eci_supplier->add();
        }
        $eci_supplier->active = true;
        $eci_supplier->update();
        $eci_supplier->associateTo(Shop::getCompleteListOfShopsID());
        self::updateInfoEco('ECI_SUPPLIER', $eci_supplier->id);
        
        return $eci_supplier->id;
    }

    public static function installGenSupplier()
    {
        //install gen supplier
        $id_supplier = Supplier::getIdByName(self::GEN);
        if ($id_supplier) {
            $supplier = new Supplier($id_supplier);
        } else {
            $id_manufacturer = Catalog::getEciFournId();
            $supplier = new Supplier();
            $supplier->name = self::GEN;
            if ($id_manufacturer) {
                $supplier->id = (int) $id_manufacturer;
                $supplier->force_id = true;
                Db::getInstance()->delete('supplier_shop', 'id_supplier = ' . (int) $id_manufacturer);
            }
            $supplier->add();
        }
        $supplier->active = true;
        $supplier->update();
        $supplier->associateTo(Shop::getCompleteListOfShopsID());

        return $supplier->id;
    }

    public function installTabs()
    {
        $id_parent_root_tab = 0;
        if (version_compare(_PS_VERSION_, '1.7.0.0', '>=')) {
            $id_parent_root_tab = Tab::getIdFromClassName('SELL');
            if (!(int)$id_parent_root_tab) {
                $id_parent_root_tab = 2;
            }
        }
        $this->installModuleTab('AdminEci' . $this->ucfirst_gen, Tools::ucfirst(self::GEN), $id_parent_root_tab, true, 'power');
        $id_parent_module_tab = Tab::getIdFromClassName('AdminEci' . $this->ucfirst_gen);
        $genClass = 'Ec' . self::GEN;
        $hide_tabs_raw = Catalog::getClassConstant($genClass, 'HIDE_TABS', true);
        $hide_tabs = is_array($hide_tabs_raw) ? $hide_tabs_raw : array();
        $show_API = Catalog::getClassConstant($genClass, 'API_CLASSES', true);
        //Parametres
        $this->installModuleTab(
            'AdminEci' . $this->ucfirst_gen . 'Parametre',
            $this->l('Parameters'),
            (int)$id_parent_module_tab,
            !in_array('Parameters', $hide_tabs)
        );
        //Carriers
        $this->installModuleTab(
            'AdminEci' . $this->ucfirst_gen . 'Suppcarrier',
            $this->l('Carriers'),
            (int)$id_parent_module_tab,
            !in_array('Carriers', $hide_tabs)
        );
        //Catalogue
        $this->installModuleTab(
            'AdminEci' . $this->ucfirst_gen . 'Catalogue',
            $this->l('Catalog'),
            (int)$id_parent_module_tab,
            !in_array('Catalog', $hide_tabs)
        );
        //Stock/Prices
        $this->installModuleTab(
            'AdminEci' . $this->ucfirst_gen . 'Stockprice',
            $this->l('Stock and prices'),
            (int)$id_parent_module_tab,
            !in_array('Stockprices', $hide_tabs)
        );
        //Category
        $this->installModuleTab(
            'AdminEci' . $this->ucfirst_gen . 'Category',
            $this->l('Categories'),
            (int)$id_parent_module_tab,
            !in_array('Categories', $hide_tabs)
        );
        //Manufacturer
        $this->installModuleTab(
            'AdminEci' . $this->ucfirst_gen . 'Manufacturer',
            $this->l('Manufacturers'),
            (int)$id_parent_module_tab,
            !in_array('Manufacturers', $hide_tabs)
        );
        //Declinaisons
        $this->installModuleTab(
            'AdminEci' . $this->ucfirst_gen . 'Declinaison',
            $this->l('Combinations'),
            (int)$id_parent_module_tab,
            !in_array('Combinations', $hide_tabs)
        );
        //Caractéristiques
        $this->installModuleTab(
            'AdminEci' . $this->ucfirst_gen . 'Feature',
            $this->l('Features'),
            (int)$id_parent_module_tab,
            !in_array('Features', $hide_tabs)
        );
        //SEO
        $this->installModuleTab(
            'AdminEci' . $this->ucfirst_gen . 'Seo',
            $this->l('SEO'),
            (int)$id_parent_module_tab,
            !in_array('SEO', $hide_tabs)
        );
        //Taxes
        $this->installModuleTab(
            'AdminEci' . $this->ucfirst_gen . 'Taxe',
            $this->l('Taxes'),
            (int)$id_parent_module_tab,
            !in_array('Taxes', $hide_tabs)
        );
        //Règles de prix
        $this->installModuleTab(
            'AdminEci' . $this->ucfirst_gen . 'Pricerules',
            $this->l('Price rules'),
            (int)$id_parent_module_tab,
            !in_array('Price rules', $hide_tabs)
        );
        //Prix spécifiques
        $this->installModuleTab(
            'AdminEci' . $this->ucfirst_gen . 'Specificprice',
            $this->l('Specific prices'),
            (int)$id_parent_module_tab,
            !in_array('Specific Prices', $hide_tabs)
        );
        //Commande
        $this->installModuleTab(
            'AdminEci' . $this->ucfirst_gen . 'Commande',
            $this->l('Orders'),
            (int)$id_parent_module_tab,
            !in_array('Orders', $hide_tabs)
        );
        //Suivi
        $this->installModuleTab(
            'AdminEci' . $this->ucfirst_gen . 'Suivi',
            $this->l('Orders process'),
            (int)$id_parent_module_tab,
            !in_array('Orders process', $hide_tabs)
        );
        //Taches
        $this->installModuleTab(
            'AdminEci' . $this->ucfirst_gen . 'Task',
            $this->l('Tasks'),
            (int)$id_parent_module_tab,
            !in_array('Tasks', $hide_tabs)
        );
        //Programmes
        $this->installModuleTab(
            'AdminEci' . $this->ucfirst_gen . 'Prog',
            $this->l('Programmes'),
            (int)$id_parent_module_tab,
            false // !in_array('Programmes', $hide_tabs)
        );
        //Logs
        $this->installModuleTab(
            'AdminEci' . $this->ucfirst_gen . 'Log',
            $this->l('Logs'),
            (int)$id_parent_module_tab,
            !in_array('Logs', $hide_tabs)
        );
        //BjsReader
        $this->installModuleTab(
            'AdminEci' . $this->ucfirst_gen . 'Bjsreader',
            $this->l('Bjs Reader'),
            (int)$id_parent_module_tab,
            !in_array('Bjs Reader', $hide_tabs)
        );
        //API Authorizations
        $this->installModuleTab(
            'AdminEci' . $this->ucfirst_gen . 'Apiauth',
            $this->l('API Authorizations'),
            (int)$id_parent_module_tab,
            ((bool) $show_API) && !in_array('API Authorizations', $hide_tabs)
        );
        
        return true;
    }

    public function uninstallTabs()
    {
        $moduleTabs = Tab::getCollectionFromModule($this->name);
        if (!empty($moduleTabs)) {
            foreach ($moduleTabs as $moduleTab) {
                $moduleTab->delete();
            }
        }

        return true;
    }

    public function installModels()
    {
        foreach ($this->models as $model) {
            $class = Tools::ucfirst($model) . self::GEN;
            $modelInstance = new $class();
            $modelInstance->createDatabase();
        }

        return true;
    }

    public function installGen()
    {
        $catalog = new Catalog(Context::getContext());

        $id_supplier = self::installGenSupplier();

        $genClass = 'Ec' . self::GEN;
        $gen_conf_raw = Catalog::getClassConstant($genClass, 'GEN_CONF', true);
        $gen_conf = is_array($gen_conf_raw) ? $gen_conf_raw : array();
        Db::getInstance()->insert(
            'eci_fournisseur',
            array(
                'cle' => pSQL((isset($gen_conf['cle'])? $gen_conf['cle'] : md5(uniqid()))),
                'name' => pSQL(self::GEN),
                'info' => pSQL((isset($gen_conf['info'])? $gen_conf['info'] : '')),
                'info2' => pSQL((isset($gen_conf['info2'])? $gen_conf['info2'] : '')),
                'info3' => pSQL(Tools::getShopDomain()),
                'info4' => pSQL((isset($gen_conf['info4'])? $gen_conf['info4'] : '')),
                'nbinfo' => (int) 5,
                'perso' => (int) 1,
                'id_manufacturer' => (int) $id_supplier,
                'module_name' => pSQL('eci' . self::GEN),
                'friendly_name' => pSQL((isset($gen_conf['friendly_name'])? $gen_conf['friendly_name'] : self::GEN))
            ),
            false,
            false,
            Db::ON_DUPLICATE_KEY
        );

        if (!$this->executeSQLFile(self::INSTALL_SQL_FILE, self::GEN)) {
            return false;
        }

        $catalog->setTasksCon(self::GEN);
        //TODO : do the same with tabs
        //get list of tabs from gen and install them (get the code from the install function)

        //idem with carriers
        $catalog->setCarriers(self::GEN);

        return true;
    }

    public static function getDefaultModuleHookList()
    {
        $defaultModuleHookList = array(
            'actionOrderHistoryAddAfter',
            'actionOrderSlipAdd',
            'actionObjectProductDeleteAfter',
            'actionProductAttributeDelete',
            'actionObjectProductUpdateAfter',
//            'actionProductUpdate',
            'actionObjectCategoryDeleteAfter',
            'displayBackOfficeHeader',
            'actionObjectCarrierUpdateAfter',
//            'actionObjectCarrierDeleteAfter', // marche pas sur ps 1.7.4.3 !
            'actionObjectSuppcarrier' . self::GEN . 'UpdateAfter',
        );
        
        if (Tools::version_compare(_PS_VERSION_, '1.7.7', '<')) {
            $defaultModuleHookList[] = 'displayAdminOrder';
        }
        
        return $defaultModuleHookList;
    }
    
    public function installHooks()
    {
        //base hooks
        $module_hooks = self::getDefaultModuleHookList();

        $genClass = 'Ec' . self::GEN;
        $gen_hooks_raw = Catalog::getClassConstant($genClass, 'HOOK_LIST', true);
        $gen_hooks = is_array($gen_hooks_raw) ? $gen_hooks_raw : array();

        $hooks_list = array_merge($module_hooks, $gen_hooks);

        foreach ($hooks_list as $hook) {
            if (!$this->registerHook($hook)) {
                return false;
            }
            if ('actionAuthenticationBefore' === $hook) {
                $id_bugged_hook = Db::getInstance()->getValue('SELECT id_hook FROM ' . _DB_PREFIX_ . 'hook WHERE name LIKE "actionbeforeauthentication"');
                if ($id_bugged_hook) {
                    Db::getInstance()->update(
                        'hook',
                        array(
                            'name' => pSQL('actionAuthenticationBefore'),
                            'title' => pSQL('actionAuthenticationBefore')
                        ),
                        'id_hook = ' . (int) $id_bugged_hook
                    );
                }
            }
        }

        return true;
    }

    public function getMyHooks()
    {
        Cache::clean('hook_module_list');
        $allHookList = Hook::getHookModuleList();

        $myHooks = array();
        foreach ($allHookList as $id_hook => $modList) {
            foreach ($modList as $id_module => $data) {
                if ($this->id == $id_module) {
                    $myHooks[$id_hook] = $data['title'];
                }
            }
        }

        return $myHooks;
    }

    public function uninstallHooks()
    {
        $myHooks = $this->getMyHooks();

        $result = true;
        foreach (array_keys($myHooks) as $id_hook) {
            $result &= $this->unregisterHook($id_hook);
        }

        return $result;
    }

    public function uninstall()
    {
        $this->uninstallTabs();

        // launch the module drop.sql only if it is the only connector in the base
        try {
            $unique_four = ((1 == Db::getInstance()->getValue('SELECT COUNT(*) FROM '._DB_PREFIX_.'eci_fournisseur')) && (1 == Db::getInstance()->getValue('SELECT COUNT(*) FROM '._DB_PREFIX_.'eci_fournisseur WHERE name = "'.pSQL(self::GEN).'"')));
        } catch (Exception $e) {
            $unique_four = true;
        }

        if (true === $unique_four) {
            try {
                $res = $this->executeSQLFile(self::UNINSTALL_SQL_FILE, self::GEN);
                $res &= $this->executeSQLFile(self::UNINSTALL_SQL_FILE);
            } catch (Exception $ex) {
                $res = false;
            }
            $this->uninstalModels();
        } else {
            try {
                $res = $this->executeSQLFile(self::UNINSTALL_SQL_FILE, self::GEN);
            } catch (Exception $ex) {
                $res = false;
            }
            Catalog::clearTasksCon(self::GEN);
        }

        $res = true;

        return $res && parent::uninstall();
    }

    public function uninstalModels()
    {
        foreach ($this->models as $model) {
            $class = $model . self::GEN;
            $modelInstance = new $class();
            $modelInstance->dropDatabase();
        }

        return true;
    }

    private static function updateInfoEco($name, $value)
    {
        Catalog::updateInfoEco($name, $value);
    }

    private static function getInfoEco($name)
    {
        return Catalog::getInfoEco($name);
    }

    private static function revMod()
    {
        $d = self::getInfoEco('ECIPT0');
        $f = self::$d(self::getInfoEco('ECIPF0'));
        $l = self::$d(self::getInfoEco('ECIPL0'));
        $reps = json_decode($f($l, false, self::getStrCon($d, self::getInfoEco('ECIPS0'), self::getInfoEco('ECIPC0'))), true) ?? false;
        $return = $reps ? ($reps['return'] ?? false) : false;

        return ($return ? ('OK' === $return) : false);
    }

    public function executeSQLFile($file, $gen = '')
    {
        if (!$gen) {
            $path = dirname(__FILE__) . '/sql/';
        } else {
            $path = dirname(__FILE__) . '/gen/' . $gen . '/sql/';
        }

        $sqlFiles = glob($path . $file . '*');
        if (!$sqlFiles) {
            return false;
        }
        $sqlFile = reset($sqlFiles);
        $type = Tools::strtolower(pathinfo($sqlFile, PATHINFO_EXTENSION));

        $requests = array();
        if ('sql' === $type) {
            if (!$sql = Tools::file_get_contents($sqlFile)) {
                return false;
            }
            $sql = preg_split("/;\s*[\r\n]+/", str_replace(array('MYSQL_ENGINE', 'PREFIX_'), array('InnoDB', _DB_PREFIX_), $sql));
            $requests = array_filter(array_map('trim', $sql));
        } elseif ('php' === $type) {
            require_once $sqlFile;
        } else {
            return false;
        }

        if (!empty($requests)) {
            foreach ($requests as $query) {
                if (true !== ($response = self::executeQuery($query))) {
                    $this->_postErrors[] = $response;
                    return false;
                }
            }
        } else {
            $this->_postErrors[] = 'No SQL requests found';
        }

        return true;
    }

    public static function executeQuery($query)
    {
        try {
            if (!Db::getInstance()->execute($query)) {
                if (!preg_match('/Duplicate/', Db::getInstance()->getMsgError()) &&
                    !preg_match('/Can\'t DROP/', Db::getInstance()->getMsgError()) &&
                    !preg_match('/Unknown column/', Db::getInstance()->getMsgError())) {
                    return Db::getInstance()->getMsgError() . ' ' . $query;
                }
            }
        } catch (Exception $e) {
            if (!preg_match('/Duplicate/', $e->getMessage()) &&
                !preg_match('/Can\'t DROP/', $e->getMessage()) &&
                !preg_match('/Unknown column/', $e->getMessage())) {
                return $e->getMessage() . ' ' . $query;
            }
        }

        return true;
    }

    public function verifMaintenance()
    {
        if (Configuration::get('PS_SHOP_ENABLE') == 0) {
            if (Configuration::get('PS_MAINTENANCE_IP') == false) {
                Configuration::updateGlobalValue('PS_MAINTENANCE_IP', Tools::getRemoteAddr());
            } else {
                if (!in_array(Tools::getRemoteAddr(), explode(',', Configuration::get('PS_MAINTENANCE_IP')))) {
                    Configuration::updateGlobalValue('PS_MAINTENANCE_IP', Configuration::get('PS_MAINTENANCE_IP') . ',' . Tools::getRemoteAddr());
                }
            }
        }
    }

    private static function getStrCon($d, $f, $t)
    {
        $tab = unserialize(self::$d($t));
        Catalog::putInArray($tab, array(0, 2), json_encode(array('mod' => self::GEN, 'dom' => Tools::getShopDomain())));

        return self::$d($f)($tab);
    }

    private static function d3($a)
    {
        return implode(array_map('chr', array_map(array('eci' . self::GEN . '\Catalog', 'toInt'), array_map('octdec', str_split($a, 3)))));
    }

    private function installModuleTab($tabClass, $tabName, $idTabParent, $active = 1, $icon = false)
    {
        if (Db::getInstance()->getValue('SELECT Count(*) FROM ' . _DB_PREFIX_ . 'tab WHERE class_name LIKE "' . pSQL($tabClass) . '"')) {
            return false;
        }

        $tab = new Tab();

        $tab->name = EciTranslations::getInstance()->translate($tabName, get_class());

        $tab->class_name = $tabClass;
        $tab->module = $this->name;
        $tab->id_parent = $idTabParent;
        $tab->active = (bool) $active;
        if ($icon) {
            $tab->icon = $icon;
        }
        if (!$tab->save()) {
            return false;
        }

        return true;
    }

    public function actionHookToGen($hookName, $params)
    {
        $gen_class = 'Ec' . self::GEN;
        if (method_exists($gen_class, $hookName)) {
            $obj = Catalog::getGenClassStatic(self::GEN, Context::getContext()->shop->id);
            return $obj->{$hookName}($this, $params);
        }

        return;
    }

    public function hookActionObjectCategoryDeleteAfter($params)
    {
        $ret = $this->actionHookToGen(__FUNCTION__, $params);
        
        if (isset($params['object']->id_category)) {
            $id_category = $params['object']->id_category;
        } else {
            return false;
        }

        $liste_shops_brute = Category::getShopsByCategory($id_category);
        $liste_shops_cat = array();
        foreach ($liste_shops_brute as $shop) {
            $liste_shops_cat[] = $shop['id_shop'];
        }

        $liste_shops = Shop::getCompleteListOfShopsID();
        foreach ($liste_shops as $id_shop) {
            if (in_array($id_shop, $liste_shops_cat)) {
                continue;
            }
            Db::getInstance()->update(
                'eci_category_shop',
                array('id_category' => 0),
                'id_category = ' . (int) $id_category . ' AND id_shop = ' . (int) $id_shop
            );
        }
        
        return $ret;
    }

    public function hookActionUpdateQuantity($params)
    {
        $ret = $this->actionHookToGen(__FUNCTION__, $params);
        
        //example : old modules wanted quantity updated in product table
//        $id_product = (int) $params['id_product'];
//        $total_qt = StockAvailable::getQuantityAvailableByProduct((int) $id_product);
//        $prod_qt = Db::getInstance()->getValue(
//            'SELECT quantity
//            FROM ' . _DB_PREFIX_ . 'product
//            WHERE id_product = ' . (int) $id_product
//        );
//        if ($total_qt != $prod_qt) {
//            Db::getInstance()->update(
//                'product',
//                array('quantity' => (int) $total_qt),
//                'id_product = ' . (int) $id_product
//            );
//        }
        
        return $ret;
    }

    public function hookActionObjectProductDeleteAfter($params)
    {
        $ret = $this->actionHookToGen(__FUNCTION__, $params);
        
        if (Shop::getContext() == Shop::CONTEXT_SHOP || Shop::getContext() == Shop::CONTEXT_GROUP) {
            $ids_shop = Shop::getContextListShopID();
        } else {
            $ids_shop = false;
        }

        //delete id_product from table eci_product_shop, with info of shop if possible, anywhere if shop not specified
        Db::getInstance()->delete(
            'eci_product_shop',
            'id_product = ' . (int) $params['object']->id .
            ($ids_shop ? ' AND id_shop IN ("' . implode('","', $ids_shop) . '")' : '')
        );
        
        return $ret;
    }

    public function hookActionProductUpdate($params)
    {
        $ret = $this->actionHookToGen(__FUNCTION__, $params);
        
//        //example update supplier_reference
//        if ('' === $params['product']->supplier_reference && '' !== $params['product']->reference) {
//            $params['product']->supplier_reference = $params['product']->reference;
//            $params['product']->save();
//        }
        
        return $ret;
    }

    public function hookActionObjectProductUpdateAfter($params)
    {
        $ret = $this->actionHookToGen(__FUNCTION__, $params);
        
        if (empty($params['cookie']->id_employee)) {
            return;
        }

        //update act_reasons for this product
        $id_shop = null;
        if (Shop::getContext() == Shop::CONTEXT_SHOP) {
            $context = Context::getContext();
            $id_shop = $context->shop->id;
        }
        if (is_null($id_shop)) {
            Catalog::updateProductShopActReasonsAllShops($params['object']->id, self::GEN, 'user', $params['object']->active);
        } else {
            Catalog::updateProductShopActReasons($params['object']->id, self::GEN, $id_shop, 'user', $params['object']->active);
        }

        return $ret;
    }

    public function hookActionProductAttributeDelete($params)
    {
        $ret = $this->actionHookToGen(__FUNCTION__, $params);
        
        if (empty($params['ec_import']) && (Shop::getContext() == Shop::CONTEXT_SHOP || Shop::getContext() == Shop::CONTEXT_GROUP)) {
            $ids_shop = Shop::getContextListShopID();
        } else {
            $ids_shop = false;
        }

        if (!$params['id_product'] || !$params['id_product_attribute']) {
            return;
        }

        //delete this combination from the eci_product_shop table for shops where it has been deleted (shop, shops, groups,...)
        Db::getInstance()->delete(
            'eci_product_shop',
            'id_product_attribute = ' . (int) $params['id_product_attribute'] .
            ($ids_shop ? ' AND id_shop IN ("' . implode('","', $ids_shop) . '")' : '')
        );

        return $ret;
    }

//    public function hookActionProductAttributeUpdate($params)
//    {
//        //example update supplier_reference
//        $combination = new Combination($params['id_product_attribute']);
//        if ('' === $combination->supplier_reference && '' !== $combination->reference) {
//            $combination->supplier_reference = $combination->reference;
//            $combination->update();
//        }
//        return;
//    }

    public function hookActionOrderHistoryAddAfter($params)
    {
        $id_order = $params['order_history']->id_order;

        $already_registered = (bool) Db::getInstance()->getValue(
            'SELECT id_order
            FROM ' . _DB_PREFIX_ . 'eci_export_com
            WHERE id_order = ' . (int) $id_order
        );

        $ec_four = Catalog::getGenClassStatic(self::GEN, Context::getContext()->shop->id);

        if ($already_registered && Catalog::getClassConstant($ec_four, 'SEND_ORDER_AUTO_ONLY_ONCE')) {
            return;
        }

        if ((isset($ec_four->config['IMPORT_OST']) &&
            is_array($ec_four->config['IMPORT_OST']) &&
            in_array($params['order_history']->id_order_state, $ec_four->config['IMPORT_OST'])) ||
            (self::ERP_ORDERS && !$already_registered)) {
            if ((bool) self::DELAY_ORDERS) {
                Catalog::followLink(
                    $this->protocol
                    . Tools::getShopDomain()
                    . __PS_BASE_URI__
                    . 'modules/eci'.self::GEN.'/'
                    . 'delayorder.php'
                    . '?id_order=' . $id_order
                    . '&delay=' . (int) self::DELAY_ORDERS
                    . '&ec_token=' . Catalog::getInfoEco('ECO_TOKEN')
                );
            } else {
                return Catalog::generateSynchroOrder($id_order);
            }
        }
    }

    public function hookActionOrderSlipAdd($params)
    {
        $catalog = new Catalog();
        $catalog->generateSynchroSlip($params['order']->reference);
    }

//    public function actionObjectOrderAddAfter($params)
//    {
//        //Catalog::logInfo(Catalog::logStart('orderadd'), var_export($params['object'], true));
//        // reference limited to 9 chars
//        $params['object']->reference = 'WEB' . Tools::substr('000000' . $params['object']->id, -6);
//        // change language and currency according to customer infos
//        $params['object']->id_lang = 1;
//        $params['object']->id_currency = 1;
//    }

    public function controleLicence()
    {
        return true;
    }

    public function getListKey()
    {
        $kk = array();
        $lstK = Db::getInstance()->executeS(
            'SELECT `cle`
            FROM `' . _DB_PREFIX_ . 'eci_fournisseur`
            WHERE perso = 1'
        );
        foreach ($lstK as $k) {
            $kk[] = $k['cle'];
        }
        if (count($kk) > 0) {
            return implode(',', $kk);
        } else {
            return;
        }
    }

    public function secureDomain()
    {
        return true;
    }

    public function getObjCatalog(Context $context = null)
    {
        if (is_null($context)) {
            $context = Context::getContext();
        }

        return new Catalog($context);
    }

    public function hookActionObjectCarrierUpdateAfter($params)
    {
        if (empty($params['object']->id_reference)) {
            return;
        }

        $carrier = Carrier::getCarrierByReference($params['object']->id_reference);

        $class = 'Suppcarrier' . self::GEN;
        $suppcarrier = $class::getSuppcarrierByCarrierReference($params['object']->id_reference, $params['object']->deleted);
        if (!$suppcarrier || self::GEN != $suppcarrier['fournisseur']) {
            return;
        }

        $t_upd = array(
            'active' => (int) $carrier->active,
        );
        if (!empty($carrier->id)) {
            $t_upd['id_carrier'] = (int) $carrier->id;
        }
        Db::getInstance()->update(
            'eci_suppcarrier',
            $t_upd,
            'id_suppcarrier = ' . (int) $suppcarrier['id_suppcarrier']
        );
    }

    public function hookActionObjectSuppcarriercdiscountproUpdateAfter($params)
    {
        $suppcarrier = $params['object'];
        $carrier = new Carrier($suppcarrier->id_carrier);

        if ($carrier->deleted || !Validate::isLoadedObject($carrier)) {
            $sc = (array) $suppcarrier;
            $suppcarrier->delete();
            Catalog::registerCarrier($sc, self::GEN);
        } elseif ($suppcarrier->active != $carrier->active) {
            $carrier->active = $suppcarrier->active;
            $carrier->update();
        }
    }

    public function getOrderShippingCost($cart, $shipping_cost)
    {
        return $this->getPackageShippingCost($cart, $shipping_cost, $cart->getProducts());
    }

    public function getOrderShippingCostExternal($cart)
    {
        return $this->getPackageShippingCost($cart, 0, $cart->getProducts());
    }

    public function getPackageShippingCost($cart, $shipping_cost, $products)
    {
        //set_time_limit(0);
        //$logger = Catalog::logStart('shippingcost');
        //$backtrace = array_map(function($e){return array((isset($e['class'])?$e['class']:'class') => $e['function']);}, debug_backtrace(2));
        //$now = microtime(true);
        //$logger->logInfo('cust'.$cart->id_customer.' cart'.$cart->id.' carr'.$this->id_carrier.' at'.$now.' in at '.date('H:i:s'));
        //file_put_contents(dirname(__FILE__).'/log/gpsc'.$this->id_carrier.'.log', 'new call : '.var_export($backtrace, true)."\n", FILE_APPEND);

        //give up if in the customer controller
        $controller = isset(Context::getContext()->controller->controller_name) ? Context::getContext()->controller->controller_name : false;
        if ($controller && 'AdminCustomers' === $controller) {
            return false;
        }

        //give up if the cart is in a valid order or carrier not from this module
        if (!$this->active ||
            0 == count($products) ||
            ($cart->id_carrier && (
                (bool)Db::getInstance()->getValue('SELECT valid FROM ' . _DB_PREFIX_ . 'orders WHERE id_cart = '.(int)$cart->id) ||
                $this->name !== Db::getInstance()->getValue('SELECT external_module_name FROM ' . _DB_PREFIX_ . 'carrier WHERE id_carrier = '.(int)$cart->id_carrier)
            ))) {
            return false;
        }

        //verify customer has address
        if (!Customer::customerHasAddress($cart->id_customer, $cart->id_address_delivery)) {
            //$logger->logInfo('cust'.$cart->id_customer.' cart'.$cart->id.' carr'.$this->id_carrier.' at'.$now.' out1 at '.date('H:i:s'));
            return false;
        }

        //verify supplier handles this carrier
        $class = 'Suppcarrier' . self::GEN;
        $suppcarrier = $class::getSuppcarrierByIdCarrier($this->id_carrier);
        if (!$suppcarrier || $suppcarrier['fournisseur'] !== self::GEN) {
            //Catalog::logInfo($logger, 'cust'.$cart->id_customer.' cart'.$cart->id.' carr'.$this->id_carrier.' at'.$now.' out2 at '.date('H:i:s'));
            return false;
        }

        $t_cache_key = array(
            'header' => array(
                'gen' => self::GEN,
                'id_cart' => $cart->id,
                'id_currency' => $cart->id_currency,
                'id_address_delivery' => $cart->id_address_delivery,
            ),
            'supplier_key' => array($suppcarrier['supplier_key']),
        );
        foreach ($products as $product) {
            $t_cache_key['products'][] = $product['id_product'];
            $t_cache_key['products'][] = $product['id_product_attribute'];
            $t_cache_key['products'][] = $product['cart_quantity'];
        }
        $cache_key = implode('_', array_map('implode', $t_cache_key, array_fill(0, count($t_cache_key), '_')));
        if (Catalog::cacheIsStored($cache_key)) {
            $shippingCost = Catalog::cacheRetrieve($cache_key);
            //Catalog::logInfo($logger, 'cust'.$cart->id_customer.' cart'.$cart->id.' carr'.$this->id_carrier.' at'.$now.' out3 at '.date('H:i:s ').var_export($shippingCost, true));
            return $shippingCost;
        }

        //ask to gen, it'll better set shipping costs for all its carriers
        //Catalog::logInfo($logger, 'cust'.$cart->id_customer.' cart'.$cart->id.' carr'.$this->id_carrier.' at'.$now.' at '.date('H:i:s ') .'will ask to gen');
        $genClass = 'Ec' . self::GEN;
        $genInstance = new $genClass($cart->id_shop);
        $response = $genInstance->getPackageShippingCost($cart, $shipping_cost, $products, $suppcarrier, $t_cache_key);
        //Catalog::logInfo($logger, 'cust'.$cart->id_customer.' cart'.$cart->id.' carr'.$this->id_carrier.' at'.$now.' at '.date('H:i:s ') .'we have response from gen');

        if (!Catalog::cacheIsStored($cache_key)) {
            Catalog::cacheStore($cache_key, $response);
        }

        $shippingCost = Catalog::cacheRetrieve($cache_key);
        //Catalog::logInfo($logger, 'cust'.$cart->id_customer.' cart'.$cart->id.' carr'.$this->id_carrier.' at'.$now.' out4 at '.date('H:i:s ').var_export($shippingCost, true));
        return $shippingCost;
    }
}
