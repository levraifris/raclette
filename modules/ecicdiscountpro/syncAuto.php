<?php
/**
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to a commercial license from SARL Ether Creation
 * Use, copy, modification or distribution of this source file without written
 * license agreement from the SARL Ether Creation is strictly forbidden.
 * In order to obtain a license, please contact us: contact@ethercreation.com
 * ...........................................................................
 * INFORMATION SUR LA LICENCE D'UTILISATION
 *
 * L'utilisation de ce fichier source est soumise a une licence commerciale
 * concedee par la societe Ether Creation
 * Toute utilisation, reproduction, modification ou distribution du present
 * fichier source sans contrat de licence ecrit de la part de la SARL Ether Creation est
 * expressement interdite.
 * Pour obtenir une licence, veuillez contacter la SARL Ether Creation a l'adresse: contact@ethercreation.com
 * ...........................................................................
 *
 * @author    Ether Creation SARL <contact@ethercreation.com>
 * @copyright 2008-2021 Ether Creation SARL
 * @license   Commercial license
 * International Registered Trademark & Property of Ether Creation SARL
 */

require_once dirname(__FILE__) . '/../../config/config.inc.php';
require_once dirname(__FILE__) . '/class/catalog.class.php';
require_once dirname(__FILE__) . '/class/reference.class.php';
use ecicdiscountpro\Catalog;
use ecicdiscountpro\ImporterReference;

ignore_user_abort(true);
set_error_handler("exception_error_handler");

$paramHelp = Tools::getValue('help', null);
if (!is_null($paramHelp)) {
    $help = array(
        'ec_token' => array(
            'fr' => 'Token du module. Obligatoire.',
            'en' => 'Module\'s token. Required.'
            ),
        'connecteur' => array(
            'fr' => 'Connecteur à traiter. Obligatoire.',
            'en' => 'Module\'s token. Required.'
            ),
        'all' => array(
            'fr' => 'Force la synchronisation de tous les produits au catalogue. Facultatif.',
            'en' => 'Force sync of all products in catalog. Optional.'
            ),
        'upd' => array(
            'fr' => 'Écrase Les données obsolètes existantes. Facultatif.',
            'en' => 'Update obsolete products. Optional.'
            ),
        'force' => array(
            'fr' => 'Force l\'écrasement des données existantes. Facultatif.',
            'en' => 'Force update of existing products. Optional.'
            ),
        'onlycat' => array(
            'fr' => 'Réassocie les produits avec les catégories matchées ; sous-entend upd. Facultatif.',
            'en' => 'Force update of existing products. Optional.'
            ),
        'kill' => array(
            'fr' => 'Arrête l\'opération en cours. À utiliser seul.',
            'en' => 'Stops the current sync. Use as single parameter.'
            ),
    );
    Catalog::answer(Tools::jsonEncode($help));
    exit();
}

$token = Tools::getValue('ec_token', 1);
if ($token != Catalog::getInfoEco('ECO_TOKEN')) {
    header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
    header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
    header('Cache-Control: no-store, no-cache, must-revalidate');
    header('Cache-Control: post-check=0, pre-check=0', false);
    header('Pragma: no-cache');
    Tools::redirect('Location: ../');
    exit();
}

$logger = Catalog::logStart('synchro');

// paramètres
$paramFourn = Tools::getValue('connecteur');
if (!empty($paramFourn)) {
    $fourn = Db::getInstance()->getRow(
        '
        SELECT *
        FROM `' . _DB_PREFIX_ . 'eci_fournisseur`
        WHERE `perso`=1
        AND `name`=\'' . pSQL($paramFourn) . '\''
    );
}
if (empty($fourn)) {
    $fourn = array();
    $fourn['name'] = false;
}
$connecteur = $fourn['name'];
// on peut aussi envisager :
//  si pas de connecteur en paramètre -> s'autoappelle avec les mêmes paramètres pour tous les connecteurs actifs

$paramNbCron = Tools::getValue('nbC');
$nbCron = !$connecteur ? 0 : (empty($paramNbCron) ? 0 : (int) $paramNbCron);

$paramJobID = Tools::getValue('jid', null);

$paramAll = Tools::getValue('all', null);
$all = is_null($paramAll) ? false : true;
$prefix = $all ? 'ECI_SA_' : 'ECI_SS_';

$paramUpd = Tools::getValue('upd', null);
$upd = is_null($paramUpd) ? false : true;

$paramForce = Tools::getValue('force', null);
$force = is_null($paramForce) ? false : true;

$paramAct = Tools::getValue('act');
$action = (Catalog::jGet($prefix . 'ACT_' . $connecteur) === 'die') ? 'die' : ((empty($paramAct)) ? 'go' : $paramAct);

$paramSpy = Tools::getValue('spy');
$spy = (empty($paramSpy)) ? false : true;

$paramSpy2 = Tools::getValue('spytwo');
$spy2 = (empty($paramSpy2)) ? false : true;
$who = $spy ? ($spy2 ? 'spy2' : 'spy') : 'normal';

$paramKill = Tools::getValue('kill', null);
$kill = is_null($paramKill) ? false : true;

$paramPrg = Tools::getValue('prg', null);
$paramPos = Tools::getValue('pos', null);
if (!is_null($paramPrg)) {
    $prg = (int) $paramPrg;
    $pos = is_null($paramPos) ? 1 : (int) $paramPos;
    $chain = '&prg=' . $prg . '&pos=' . $pos;
} else {
    $prg = false;
    $chain = '';
}

// constants
$stopTime = time() + 10;
$listStages = array(
    'started',
    'select',
    'listcat',
    'category',
    'product',
    'packs',
);
$eci_base_uri = implode('/', explode('\\', (((Configuration::get('PS_SSL_ENABLED') == 1) && (Configuration::get('PS_SSL_ENABLED_EVERYWHERE') == 1)) ? 'https://' : 'http://' ) .
    Tools::getShopDomain() . __PS_BASE_URI__ . str_replace(_PS_ROOT_DIR_ . DIRECTORY_SEPARATOR, '', dirname(__FILE__) . '/')));
$ts = preg_replace('/0\.([0-9]{6}).*? ([0-9]+)/', '$2$1', microtime());
$jid = is_null($paramJobID) ? $ts : $paramJobID;
$params = '?ec_token=' . $token . '&ts=' . $ts . '&jid=' . $jid . ($all ? '&all' : '') . ($upd ? '&upd' : '') . ($force ? '&force' : '');
$base_uri = $eci_base_uri . basename(__FILE__) . $params . $chain;

/*
Catalog::logInfo(
    $logger,
    'syncAuto '
    . $who . ' entered, parameters '
    . $connecteur . ','
    . (int) $nbCron . ','
    . $action . ','
    . (int) $spy . ','
    . (int) $spy2 . ','
    . (int) $kill
);
*/

// kill
if ($kill) {
    if ($connecteur) {
        if (Catalog::jGet($prefix . 'STATE_' . $connecteur) != 'done') {
            Catalog::jUpdateValue($prefix . 'ACT_' . $connecteur, 'die');
        }
    } else {
        $listFourns = Db::getInstance()->executeS(
            '
            SELECT `name`
            FROM `' . _DB_PREFIX_ . 'eci_fournisseur`
            WHERE `perso` = 1'
        );
        foreach ($listFourns as $fourn) {
            $connecteur = $fourn['name'];
            if (Catalog::jGet($prefix . 'STATE_' . $connecteur) != 'done') {
                Catalog::jUpdateValue($prefix . 'ACT_' . $connecteur, 'die');
            }
        }
    }
    exit('die');
}


// fournisseur invalide
if (!$connecteur) {
    exit('noconnector');
}
if (file_exists(dirname(__FILE__) . '/gen/' . $connecteur . '/class/ec' . $connecteur . '.php')) {
    require_once dirname(__FILE__) . '/gen/' . $connecteur . '/class/ec' . $connecteur . '.php';
} else {
    exit('noclass');
}
$genClass = 'Ec' . $connecteur;
$ec_four = new $genClass();
$genStages = Catalog::getClassConstant($genClass, 'IMP_STAGES');
if (!is_null($genStages) && $genStages) {
    $listStages_r = is_null(Tools::jsonDecode($genStages, true)) ? $listStages : Tools::jsonDecode($genStages, true);
    $listStages = array_diff($listStages_r, array_filter($listStages_r, 'intval'));
}
if ($listStages[0] !== 'started') {
    array_unshift($listStages, 'started');
}


// espion
if ($spy) {
    Catalog::answer('spy');
    sleep(19);
    $state = Catalog::jGet($prefix . 'STATE_' . $connecteur);
    $progress = Catalog::jGet($prefix . 'PROGRESS_' . $connecteur);
    if ($nbCron == $progress) {
        if ($spy2) {
            if ($state != 'done') {
                Catalog::jUpdateValue($prefix . 'STATE_' . $connecteur, 'still');
            }
        } else {
            Catalog::followLink($base_uri . '&spy=1&spytwo=1&connecteur=' . $connecteur . '&nbC=' . $progress);
        }
    } else {
        Catalog::followLink($base_uri . '&spy=1&connecteur=' . $connecteur . '&nbC=' . $progress);
    }
    exit('bond');
}


// abandon ou initialisation
$etat = Catalog::jGet($prefix . 'STATE_' . $connecteur);
$starting = ((bool) $token) & ((bool) $paramFourn) & ($paramSpy === false) & ($paramNbCron === false) & ($paramKill === null) & ($paramAct === false);
if (!$starting && ($action === 'die')) {
    // abandon demandé par un kill
    Catalog::jUpdateValue($prefix . 'STATE_' . $connecteur, 'done');
    Catalog::jUpdateValue($prefix . 'END_TIME_' . $connecteur, date('Y-m-d H:i:s'));
    Catalog::jUpdateValue($prefix . 'ACT_' . $connecteur, 'go');
    exit('dead');
}
if ($starting && ($etat === 'running')) {
    // tentative de double lancement à éviter
    $progress = Catalog::jGet($prefix . 'PROGRESS_' . $connecteur);
    // envoi d'espion pour déjouer un plantage de serveur pendant une mise à jour
    Catalog::followLink($base_uri . '&spy=1&connecteur=' . $connecteur . '&nbC=' . (int) $progress);
    exit('nodoubleplease');
}
if (!$starting && ($etat === 'still')) {
    // un espion a pensé à tort qu'on était planté mais on est là !
    Catalog::jUpdateValue($prefix . 'STATE_' . $connecteur, 'running');
    Catalog::followLink($base_uri . '&spy=1&connecteur=' . $connecteur . '&nbC=' . $nbCron);
}
if ($starting) {
    // initialisation du process
    $listShops = Shop::getShops(true, null, true);
    sort($listShops);
    Catalog::jUpdateValue($prefix . 'START_TIME_' . $connecteur, date('Y-m-d H:i:s'));
    Catalog::jUpdateValue($prefix . 'END_TIME_' . $connecteur, '');
    Catalog::jUpdateValue($prefix . 'SHOPS_TODO_' . $connecteur, Tools::jsonEncode($listShops));
    Catalog::jUpdateValue($prefix . 'SHOP_' . $connecteur, reset($listShops));
    Catalog::jUpdateValue($prefix . 'STAGE_' . $connecteur, reset($listStages));
    Catalog::jUpdateValue($prefix . 'PROGRESS_' . $connecteur, 0);
    Catalog::jUpdateValue($prefix . 'PROGRESSMAX_' . $connecteur, 0);
    Catalog::jUpdateValue($prefix . 'LOOPS_' . $connecteur, 0);
    Catalog::jUpdateValue($prefix . 'STATE_' . $connecteur, 'running');
    Catalog::jUpdateValue($prefix . 'ACT_' . $connecteur, 'go');
    Catalog::jUpdateValue($prefix . 'MESSAGE_' . $connecteur, '');
    // lancement de l'espion
    Catalog::followLink($base_uri . '&spy=1&connecteur=' . $connecteur . '&nbC=0');
} else {
    Catalog::jUpdateValue($prefix . 'PROGRESS_' . $connecteur, $nbCron);
}
$listShopsTodo = Tools::jsonDecode(Catalog::jGet($prefix . 'SHOPS_TODO_' . $connecteur), true);
$id_shop = (int) Catalog::jGet($prefix . 'SHOP_' . $connecteur);
$stage = Catalog::jGet($prefix . 'STAGE_' . $connecteur);

$context = Context::getContext();
$context->shop = new Shop($id_shop, null, $id_shop);
Shop::setContext(Shop::CONTEXT_SHOP, (int) $id_shop);
$context->employee = new Employee(Catalog::getInfoEco('ID_EMPLOYEE'));
$catalog = new Catalog($context);

// gestion des reprises, ruptures, fin
if ($action === 'next') {
    $action = 'go';
    Catalog::jUpdateValue($prefix . 'ACT_' . $connecteur, 'go');

    $numStage = array_search($stage, $listStages, true);
    $keys = array_keys($listStages);
    $next = $nextKey = false;
    foreach ($keys as $key) {
        if ($next) {
            $nextKey = $key;
            break;
        }
        if ($numStage == $key) {
            $next = true;
        }
    }

    if ($nextKey) {
        $stage = $listStages[$nextKey];
        $nbCron = 0;
    } else {
        $ShopJustDone = array_shift($listShopsTodo);
        if (count($listShopsTodo) > 0) {
            $id_shop = reset($listShopsTodo);
            $stage = reset($listStages);
            $nbCron = 0;
        } else {
            Catalog::jUpdateValue($prefix . 'SHOPS_TODO_' . $connecteur, '');
            Catalog::jUpdateValue($prefix . 'STATE_' . $connecteur, 'done');
            Catalog::jUpdateValue($prefix . 'END_TIME_' . $connecteur, date('Y-m-d H:i:s'));
            if ($prg) {
                //chaining with other task
                $nextCron = Catalog::getNextCron($prg, $pos);
                if ($nextCron) {
                    Catalog::followLink($nextCron['link'] . '&prg=' . $prg . '&pos=' . $nextCron['position']);
                }
            }

            exit('alldone');
        }
    }
    Catalog::jUpdateValue($prefix . 'SHOPS_TODO_' . $connecteur, Tools::jsonEncode($listShopsTodo));
    Catalog::jUpdateValue($prefix . 'SHOP_' . $connecteur, $id_shop);
    Catalog::jUpdateValue($prefix . 'STAGE_' . $connecteur, $stage);
    Catalog::jUpdateValue($prefix . 'LOOPS_' . $connecteur, 0);
    Catalog::jUpdateValue($prefix . 'PROGRESS_' . $connecteur, 0);
    Catalog::jUpdateValue($prefix . 'PROGRESSMAX_' . $connecteur, 0);
}

/*
Catalog::logInfo(
    $logger,
    'syncAuto ' . $who . ' do ' . $stage . ',' . $action
);
*/

// aiguillage
$reps = null;
Catalog::answer($stage);
$can_use_method = in_array($stage, get_class_methods($ec_four));
if ($can_use_method || function_exists($stage)) {
    try {
        $reps = $can_use_method ? $ec_four->$stage($stopTime, $id_shop, $nbCron) : $stage($prefix, $connecteur, $nbCron, $stopTime, $logger, $catalog, $id_shop, $upd, $force, $all);
    } catch (Exception $e) {
        $reps = 'In "' . $stage . '" : ' . $e->getMessage() . ' in line ' . $e->getLine() . ' of file ' . $e->getFile();
    }
    if ($reps === true) {
        Catalog::jUpdateValue($prefix . 'ACT_' . $connecteur, 'next');
        Catalog::followLink($base_uri . '&connecteur=' . $connecteur . '&nbC=0&act=next');
    } elseif (is_numeric($reps)) {
        Catalog::jUpdateValue($prefix . 'LOOPS_' . $connecteur, Catalog::jGet($prefix . 'LOOPS_' . $connecteur) + 1);
        Catalog::followLink($base_uri . '&connecteur=' . $connecteur . '&nbC=' . $reps);
    }
} else {
    $reps = 'Phase "' . $stage . '" does not exist';
}

if ((!is_null($reps)) && (true !== $reps) && (!is_numeric($reps))) {
    Catalog::jUpdateValue($prefix . 'MESSAGE_' . $connecteur, date('Y-m-d H:i:s ') . var_export($reps, true));
    Catalog::logInfo(
        $logger,
        'syncAuto ' . $who . ', ' . $connecteur . ', stage ' . $stage . ', ' . var_export($reps, true)
    );
}

exit('bye');


function started($prefix, $connecteur, $nbCron, $stopTime, $logger, $catalog, $id_shop, $upd, $force, $all)
{
    // check that the supplier exists
    $id_supplier = Catalog::getEciFournId($connecteur);
    if (!Supplier::supplierExists($id_supplier)) {
        Catalog::logError($logger, 'Le fournisseur ' . $connecteur . ' a été supprimé');
        return 'Le fournisseur ' . $connecteur . ' a été supprimé';
    }

    echo 'Task successfully started.';

    return true;
}

function select($prefix, $connecteur, $nbCron, $stopTime, $logger, $catalog, $id_shop, $upd, $force, $all)
{
    try {
        set_time_limit(0);
    } catch (Exception $ex) {
    }
    
    if ($all) {
        if ($upd) {
            $catalog->setCatalogAllSelectedConShopForced($connecteur, $id_shop);
        } else {
            $catalog->setCatalogAllSelectedConShop($connecteur, $id_shop);
        }
    }

    if (true !== ($ret = $catalog->setSelection($connecteur, $id_shop, false))) {
        return $ret;
    }

    // multiply data in the eci_product_imported in all shops
/*
    $ashops = Shop::getCompleteListOfShopsID();
    asort($ashops);
    if ($id_shop = $ashops[0]) {
        Db::getInstance()->execute('
            INSERT IGNORE INTO ' . _DB_PREFIX_ . 'eci_product_imported
            SELECT i.reference, s.id_shop, i.imported, i.fournisseur
            FROM ' . _DB_PREFIX_ . 'eci_product_imported i
            JOIN ' . _DB_PREFIX_ . 'shop s
            WHERE 1'
        );
    }
*/

    // delete products from shops where they are not wanted if specified in the keep field
    Db::getInstance()->execute(
        'DELETE i
        FROM ' . _DB_PREFIX_ . 'eci_product_imported i
        LEFT JOIN ' . _DB_PREFIX_ . 'eci_catalog c
        ON c.product_reference = i.reference AND c.fournisseur = i.fournisseur
        WHERE i.id_shop = ' . (int)$id_shop . '
        AND i.imported = 1
        AND c.keep REGEXP \'' . '"id_shop":[0-9]+' . '\'
        AND c.keep NOT REGEXP \'' . '"id_shop":' . (int) $id_shop . '[^0-9]' . '\''
    );

    return true;
}

function listcat($prefix, $connecteur, $nbCron, $stopTime, $logger, $catalog, $id_shop, $upd, $force, $all)
{
    //find or create all categories
    $f_liste_categ = dirname(__FILE__) . '/files/' . $connecteur . '_category_liste.csv';

    //récupérer les catégories de produits à importer dans ce shop
    $list_categ = Db::getInstance()->executeS(
        'SELECT DISTINCT c.category_group
        FROM ' . _DB_PREFIX_ . 'eci_product_imported i
        LEFT JOIN ' . _DB_PREFIX_ . 'eci_catalog c
        ON c.product_reference = i.reference
        AND c.fournisseur = i.fournisseur
        WHERE i.fournisseur = "' . pSQL($connecteur) . '"
        AND i.imported = 1
        AND i.id_shop = ' . (int) $id_shop . '
        ORDER BY c.category_group'
    );
    Catalog::cacheStore($connecteur . '_listcat_size_shop_' . $id_shop, count($list_categ));

    // put the list in a file
    if (($handle = fopen($f_liste_categ, 'wb')) !== false) {
        foreach ($list_categ as $line) {
            if (trim($line['category_group'])) {
                fwrite($handle, $line['category_group'] . PHP_EOL);
            }
        }
        unset($list_categ);
        fclose($handle);
    } else {
        return 'phase ' . __FUNCTION__ . ' fichier temporaire \'' . $f_liste_categ . '\' inouvrable pour écriture';
    }

    return true;
}

function category($prefix, $connecteur, $nbCron, $stopTime, $logger, $catalog, $id_shop, $upd, $force, $all)
{
    try {
        set_time_limit(0);
    } catch (Exception $ex) {
    }
    
    //find or create all categories
    $f_liste_categ = dirname(__FILE__) . '/files/' . $connecteur . '_category_liste.csv';
    $id_lang = Configuration::get('PS_LANG_DEFAULT');
    $iso_lang = Language::getIsoById($id_lang);

    if (!$nbCron) {
        Catalog::jUpdateValue($prefix . 'PROGRESSMAX_' . $connecteur, (int) Catalog::cacheRetrieve($connecteur . '_listcat_size_shop_' . $id_shop));
    }

    if (($handle = fopen($f_liste_categ, 'rb')) === false) {
        return 'phase ' . __FUNCTION__ . ' fichier temporaire \'' . $f_liste_categ . '\' inouvrable pour lecture';
    }

    $jobLine = 0;
    while (($ligne = fgets($handle)) !== false) {
        if (time() > $stopTime && $jobLine > $nbCron) {
            fclose($handle);
            return $jobLine;
        }

        $jobLine++;

        if (($jobLine < $nbCron) || empty($ligne)) {
            continue;
        }

        $cleanLine = trim($ligne);
        $ml_paths = Catalog::categoryBranchesToPaths($cleanLine);
        if (!$ml_paths) {
            $ml_paths = array($cleanLine);
        }
        try {
            foreach ($ml_paths as $ml_path) {
                $pathLg = Catalog::langExpl($ml_path);
                if (empty($pathLg[$iso_lang])) {
                    continue;
                }
                $catalog->chercheOuCreeCategorie(
                    $ml_path,
                    $pathLg[$iso_lang],
                    $connecteur,
                    0,
                    $id_shop,
                    $id_lang
                );
            }
        } catch (Exception $e) {
            //fclose($handle);
            Catalog::logWarning(
                $logger,
                'syncAuto erreur phase ' . __FUNCTION__ . ' catégorie ' . json_encode($ml_paths) . ' : ' . $e->getMessage() . ' line ' . $e->getLine() . ' of file ' . $e->getFile()
            );
        }
        Catalog::jUpdateValue($prefix . 'PROGRESS_' . $connecteur, $jobLine);
    }
    fclose($handle);
    //@unlink($f_liste_categ);

    return true;
}

function product($prefix, $connecteur, $nbCron, $stopTime, $logger, $catalog, $id_shop, $upd, $force, $all)
{
    try {
        set_time_limit(0);
    } catch (Exception $ex) {
    }
    
    $jobLine = $nbCron;

    if (!$nbCron) {
        $max = (int) Db::getInstance()->getValue(
            'SELECT COUNT(*)
            FROM `' . _DB_PREFIX_ . 'eci_product_imported`
            WHERE `fournisseur` = "' . pSQL($connecteur) . '"
            AND (`imported` = 1 OR `imported` = 2)
            AND `id_shop` = ' . (int) $id_shop
        );
        Catalog::jUpdateValue($prefix . 'PROGRESSMAX_' . $connecteur, $max);
    }

    $ref_i = Db::getInstance()->getRow(
        'SELECT reference, imported
        FROM `' . _DB_PREFIX_ . 'eci_product_imported`
        WHERE `fournisseur` = "' . pSQL($connecteur) . '"
        AND (`imported` = 1 OR `imported` = 2)
        AND `id_shop` = ' . (int) $id_shop
    );

    if (!$ref_i) {
        return true;
    }

    $ref = $ref_i['reference'];
    $i = $ref_i['imported'];
    $jobLine++;

    try {
        $tab = array(
            'reference' => $ref,
            'id_shop' => $id_shop,
            'i' => $i,
            'fournisseur' => $connecteur
        );
        if ((1 == $i) && $upd) {
            $reps = $catalog->updateProductToShop($tab, (bool) $force);
        } elseif ((1 == $i) && (!$upd)) {
            $reps = $catalog->setProductToShop($tab);
        } elseif (2 == $i) {
            $reps = $catalog->delProductToShop($tab);
        }
        if (true === $reps) {
            if (Catalog::isPack($ref, $connecteur)) {
                Db::getInstance()->update(
                    'eci_product_imported',
                    array('imported' => (int) 4),
                    'reference = "' . pSQL($ref) . '" AND fournisseur = "' . pSQL($connecteur) . '" AND id_shop = ' . (int) $id_shop
                );
            }
        } else {
            Db::getInstance()->delete(
                'eci_product_imported',
                'reference = "' . pSQL($ref) . '" AND fournisseur = "' . pSQL($connecteur) . '" AND id_shop = ' . (int) $id_shop
            );
        }
    } catch (Exception $e) {
        Catalog::logError(
            $logger,
            'syncAuto exception phase ' . __FUNCTION__ . ' produit ' . $ref . ' : ' . $e->getMessage() . ' line ' . $e->getLine() . ' of file ' . $e->getFile()
        );
        Db::getInstance()->update(
            'eci_product_imported',
            array(
                'imported' => (int) 2,
            ),
            'reference = "' . pSQL($ref) . '" AND fournisseur = "' . pSQL($connecteur) . '" AND id_shop = ' . (int) $id_shop
        );
    }

    Catalog::jUpdateValue($prefix . 'PROGRESS_' . $connecteur, $jobLine);

    return $jobLine;
}

function packs($prefix, $connecteur, $nbCron, $stopTime, $logger, $catalog, $id_shop, $upd, $force, $all)
{
    if (!$nbCron) {
        $max = (int) Db::getInstance()->getValue(
            'SELECT COUNT(*)
            FROM `' . _DB_PREFIX_ . 'eci_product_imported`
            WHERE `fournisseur` = "' . pSQL($connecteur) . '"
            AND `imported` = 4
            AND `id_shop` = ' . (int) $id_shop
        );
        Catalog::jUpdateValue($prefix . 'PROGRESSMAX_' . $connecteur, $max);
    }

    $refs = Db::getInstance()->executeS(
        'SELECT reference
        FROM `' . _DB_PREFIX_ . 'eci_product_imported`
        WHERE `fournisseur` = "' . pSQL($connecteur) . '"
        AND `imported` = 4
        AND `id_shop` = ' . (int) $id_shop . '
        LIMIT 50'
    );

    if (!$refs) {
        return true;
    }

    $id_fournisseur = Catalog::getEciFournId($connecteur);
    $jobLine = $nbCron;
    foreach ($refs as $p) {
        if (time() > $stopTime && $jobLine > $nbCron) {
            return $jobLine;
        }
        $jobLine++;
        $ref = $p['reference'];

        $components = Catalog::getPackComponents($ref, $connecteur);
        if (!$components) {
            continue;
        }

        $ids = new ImporterReference($ref, $id_fournisseur, $id_shop);
        if (!$ids->id_product) {
            continue;
        }

        $list_c = array();
        foreach ($components as $component) {
            $c_ids = new ImporterReference($component['reference'], $id_fournisseur, $id_shop);
            if (!$c_ids->id_product) {
                Catalog::logError(
                    $logger,
                    __FUNCTION__ . ' component ref' . $component['reference'] . ' of pack ref' . $ref . ' is not imported.'
                );
                break;
            }
            $list_c[] = array('ids' => $c_ids, 'quantity' => $component['quantity']);
        }
        if (count($list_c) == count($components)) {
            foreach ($list_c as $c_infos) {
                Pack::addItem(
                    (int) $ids->id_product,
                    (int) $c_infos['ids']->id_product,
                    (int) $c_infos['quantity'],
                    (int) $c_infos['ids']->id_product_attribute
                );
            }
        }

        Db::getInstance()->delete(
            'eci_product_imported',
            'reference = "' . pSQL($ref) . '" AND fournisseur = "' . pSQL($connecteur) . '" AND id_shop = ' . (int) $id_shop
        );
        Db::getInstance()->update(
            'eci_product_shop',
            array('is_pack' => (int) 1),
            'id_product = ' . (int) $ids->id_product
        );
    }

    return $jobLine;
}

function exception_error_handler($severity, $message, $file, $line)
{
    if (!(error_reporting() & $severity)) {
        // Ce code d'erreur n'est pas inclu dans error_reporting

        return;
    }
    throw new ErrorException($message, 0, $severity, $file, $line);
}
