<?php
/**
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to a commercial license from SARL Ether Creation
 * Use, copy, modification or distribution of this source file without written
 * license agreement from the SARL Ether Creation is strictly forbidden.
 * In order to obtain a license, please contact us: contact@ethercreation.com
 * ...........................................................................
 * INFORMATION SUR LA LICENCE D'UTILISATION
 *
 * L'utilisation de ce fichier source est soumise a une licence commerciale
 * concedee par la societe Ether Creation
 * Toute utilisation, reproduction, modification ou distribution du present
 * fichier source sans contrat de licence ecrit de la part de la SARL Ether Creation est
 * expressement interdite.
 * Pour obtenir une licence, veuillez contacter la SARL Ether Creation a l'adresse: contact@ethercreation.com
 * ...........................................................................
 *
 * @author    Ether Creation SARL <contact@ethercreation.com>
 * @copyright 2008-2021 Ether Creation SARL
 * @license   Commercial license
 * International Registered Trademark & Property of Ether Creation SARL
 */

if (!defined('_CAN_LOAD_FILES_')) {
    exit();
}
if (Tools::version_compare(_PS_VERSION_, '1.7.0', '>=') && isset(Context::getContext()->customer)) {
    require_once _PS_ROOT_DIR_ . '/init.php';
}
require_once dirname(__FILE__) . '/ecicdiscountprofuncs.php';

class Ecicdiscountpro extends EcicdiscountproFuncs // EcicdiscountproFuncs extends CarrierModule
{
    public function __construct(Context $context = null)
    {
        $this->name = 'ecicdiscountpro';
        $this->tab = 'shipping_logistics';
        $this->version = '4.5.2';
        $this->bootstrap = true;
        $this->need_instance = 0;
        $this->author = 'Ether Creation';
        $this->displayName = $this->l('EC Import - Dropshipping module Cdiscountpro');
        $this->description = $this->l('Connect your shop to Cdiscountpro, your dropshipping supplier.');
        $this->confirmUninstall = $this->l('Are you sure you want to uninstall the module? ');
        $this->module_key = 'cf1f93fe6c1c9a3639262720c3bf7533';
        parent::__construct($context);
        if (isset($this->context->controller->controller_name) && !$this->active && (0 === stripos($this->context->controller->controller_name, 'admin' . $this->name))) {
            $this->context->controller->warnings[] = $this->l('Module is not active');
        }
    }

    public function smartyAssign($smarty_name, $smarty_value = null)
    {
        if ($smarty_name == 'this') {
            return $this->smarty->assign('catalog', $this->getObjCatalog(Context::getContext()));
        }

        return $this->smarty->assign($smarty_name, $smarty_value);
    }

    public function hookDisplayBackOfficeHeader($params)
    {
        if (version_compare(_PS_VERSION_, '1.7', '<')) {
            $this->context->controller->addCSS($this->_path.'views/css/back.css');
        }

        return $this->displayHookToGen(__FUNCTION__, $params);
    }

    public function hookDisplayAdminOrder($params)
    {
        require_once dirname(__FILE__) . '/controllers/admin/AdminEci' . Tools::ucfirst(self::GEN) . 'CommandeController.php';
        $comnd_ctrl_class = 'AdminEci' . Tools::ucfirst(self::GEN) . 'CommandeController';
        $comnd_ctrl = new $comnd_ctrl_class();
        $comnd = $comnd_ctrl->renderList();
        unset($comnd_ctrl);
        require_once dirname(__FILE__) . '/controllers/admin/AdminEci' . Tools::ucfirst(self::GEN) . 'SuiviController.php';
        $suivi_ctrl_class = 'AdminEci' . Tools::ucfirst(self::GEN) . 'SuiviController';
        $suivi_ctrl = new $suivi_ctrl_class();
        $suivi = $suivi_ctrl->renderList();
        unset($suivi_ctrl);

        return $comnd . $suivi;
    }

    public function displayHookToGen($hookName, $params)
    {
        $cat_class = 'eci' . self::GEN . '\Catalog';
        $gen_class = 'Ec' . self::GEN;
        if (method_exists($gen_class, $hookName)) {
            $obj = $cat_class::getGenClassStatic(self::GEN, Context::getContext()->shop->id);
            $data = $obj->{$hookName}($this, $params);
        }

        if (empty($data)) {
            return;
        }

        if (!empty($data['css'])) {
            if (!is_array($data['css'])) {
                $data['css'] = array($data['css']);
            }
            foreach ($data['css'] as $css) {
                $this->context->controller->addCSS($css);
            }
        }

        if (!empty($data['js'])) {
            if (!is_array($data['js'])) {
                $data['js'] = array($data['js']);
            }
            foreach ($data['js'] as $js) {
                $this->context->controller->addJS($js);
            }
        }

        if (!empty($data['tpl'])) {
            if (!is_array($data['tpl'])) {
                $data['tpl'] = array($data['tpl']);
            }
            $html = '';
            foreach ($data['tpl'] as $tpl) {
                $html .= $tpl;
            }

            return $html;
        }

        return;
    }
    
    /**
     * general hook
     */
    public function __call($hookName, $params)
    {
        $gen_class = 'Ec' . self::GEN;
        if (4 === stripos($hookName, 'display')) {
            // if $hookName is "header" or begins with "display"
            if (method_exists($this, $hookName)) {
                // if $hookName is callable in the module, call the module method
                return $this->{$hookName}($params);
            } elseif (method_exists($gen_class, $hookName)) {
                // if $hookName is callable in the gen, call the displayHookToGen method
                return $this->displayHookToGen($hookName, $params);
            }
            return '';
        } elseif (method_exists($this, $hookName)) {
            // if $hookName is callable in the module, call the module method
            return $this->{$hookName}($params);
        } elseif (method_exists($gen_class, $hookName)) {
            // if $hookName is callable in the gen, call the actionHookToGen method
            return $this->actionHookToGen($hookName, $params);
        }
        
        return;
    }
}
