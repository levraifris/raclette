<?php
/**
 * 2007-2017 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to a commercial license from SARL Ether Creation
 * Use, copy, modification or distribution of this source file without written
 * license agreement from the SARL Ether Creation is strictly forbidden.
 * In order to obtain a license, please contact us: contact@ethercreation.com
 * ...........................................................................
 * INFORMATION SUR LA LICENCE D'UTILISATION
 *
 * L'utilisation de ce fichier source est soumise a une licence commerciale
 * concedee par la societe Ether Creation
 * Toute utilisation, reproduction, modification ou distribution du present
 * fichier source sans contrat de licence ecrit de la part de la SARL Ether Creation est
 * expressement interdite.
 * Pour obtenir une licence, veuillez contacter la SARL Ether Creation a l'adresse: contact@ethercreation.com
 * ...........................................................................
 *
 * @author    Ether Creation SARL <contact@ethercreation.com>
 * @copyright 2008-2018 Ether Creation SARL
 * @license   Commercial license
 * International Registered Trademark & Property of PrestaShop Ether Creation SARL
 */

require_once dirname(__FILE__).'/../../../../config/config.inc.php';
require_once dirname(__FILE__).'/../../class/catalog.class.php';
require_once dirname(__FILE__).'/class/eccdiscountpro.php';
require_once dirname(__FILE__).'/class/PHPExcel.php';
require_once dirname(__FILE__).'/class/PHPExcel/Writer/Excel2007.php';
use ecicdiscountpro\Catalog;

$ec_four = new eccdiscountpro();

if (Tools::getValue('ec_token') != Catalog::getInfoEco('ECO_TOKEN')) {
    header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
    header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
    header('Cache-Control: no-store, no-cache, must-revalidate');
    header('Cache-Control: post-check=0, pre-check=0', false);
    header('Pragma: no-cache');
    Tools::redirect('Location: ../');
    exit();
}

$logger = Catalog::logStart('error');

$l_dir = dirname(__FILE__) . '/../../files/import/';
$d_dir = '/Anomalie/';
$fichier_xls = 'error.xls';
$fichier_csv = 'error.csv';

// se connecter au (s)ftp des commandes
// lister le dossier Anomalie
// laisser les fichiers "done_"
// convertir les fichiers xls restants en csv local et les traiter :
// si toutes les lignes sont en traité : passer le fichier en "done_"
// si une ligne est en anomalie, vérifier le tracking module pour cette commande et si on a pas déjà le message, le rajouter et passer la comande en état d'erreur correspondante

//exit();

try {
    if (!$ec_four->transLogin(true)) {
        Catalog::logError($logger, 'Impossible de se connecter au (S)FTP');
        exit();
    }

    $buff = $ec_four->transGetList($d_dir);
    if (!$buff) {
        exit();
    }

    if (!$ec_four->transChDir($d_dir)) {
        Catalog::logError($logger, 'Impossible de se trouver le dossier ' . $d_dir);
        exit();
    }

    foreach ($buff as $lstErr) {
        if ('done_' === Tools::substr($lstErr, 0, 5)) {
            continue;
        }

        $ec_four->transGetFile($l_dir . $fichier_xls, $lstErr);

        if (true === $ec_four->transGetFile($l_dir . $fichier_xls, $lstErr)) {
            $fileType = PHPExcel_IOFactory::identify($l_dir . $fichier_xls);
            $objReader = PHPExcel_IOFactory::createReader($fileType);
            $objReader->setReadDataOnly(true);
            $objPHPExcel = $objReader->load($l_dir . $fichier_xls);
            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'CSV');
            $objWriter->save($l_dir . $fichier_csv);

            $handle2 = fopen($l_dir . $fichier_csv, 'r');
            if (false === $handle2) {
                Catalog::logError($logger, 'Impossible de lire ' . $fichier_csv);
                continue;
            }
            $kol = 0;

            while (($data = fgetcsv($handle2, 10000, ',')) !== false) {
                if (0 == $kol) {
                    $kol = 1;
                    continue;
                }

                $idOKK = explode('_', $data[23]);
                $idOK = explode('.xls', $idOKK[1]);
                $idO = $idOK[0];

                if ($data[0] == utf8_encode('N° de Cmd')) {
                    continue;
                }

                if (!$idO) {
                    continue;
                }

                $order = new Order((int) $idO);
                if (!Validate::isLoadedObject($order)) {
                    Catalog::logWarning($logger, 'Commande ' . $idO . ' inexistante');
                    continue;
                }

                if (strpos($data[22], utf8_encode('Traitée')) !== false) {
                    if (!empty($ec_four->config['id_ok'])) {
                        Catalog::setOrderVisibleCon($idO, $ec_four->fournisseur_glob, false);
                        //verifier que l'état a déjà été passé à OK pour cette commande sinon le passer
                        Catalog::setOrderState($idO, $ec_four->config['id_ok']);
                    }
                } else {
                    Catalog::setOrderVisibleCon($idO, $ec_four->fournisseur_glob, true);

                    //Rupture de stock
                    if (strpos($data[22], 'Non traitée : Rupture de stock pour le produi') !== false) {
                        if (!empty($ec_four->config['id_rupt'])) {
                            Catalog::setOrderVisibleCon($idO, $ec_four->fournisseur_glob, false);
                            Catalog::setOrderState($idO, $ec_four->config['id_rupt']);
                        }
                    } elseif (strpos($data[22], 'Non traitée : commande déjà existante en doublon') !== false) { //Doublon
                        Catalog::setOrderVisibleCon($idO, $ec_four->fournisseur_glob, false);
                    } else {
                        if (!empty($ec_four->config['id_err'])) {
                            Catalog::setOrderState($idO, $ec_four->config['id_err']);
                        }
                    }
                }
            }
            fclose($handle2);
        }
        $ec_four->transFileDelete($lstErr); // see if the new version is always overwriting the old
//        $ec_four->transFileRename($lstErr, 'done_' . date('Y_m_d__H_i_s_') . $lstErr);
    }

    $ec_four->transLogoff();
} catch (Exception $e) {
    Catalog::logError($logger, $e->getMessage());
}
