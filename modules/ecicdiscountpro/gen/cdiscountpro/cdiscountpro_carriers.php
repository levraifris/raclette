<?php
/**
 * 2007-2017 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to a commercial license from SARL Ether Création
 * Use, copy, modification or distribution of this source file without written
 * license agreement from the SARL Ether Création is strictly forbidden.
 * In order to obtain a license, please contact us: contact@ethercreation.com
 * ...........................................................................
 * INFORMATION SUR LA LICENCE D'UTILISATION
 *
 * L'utilisation de ce fichier source est soumise a une licence commerciale
 * concedee par la societe Ether Création
 * Toute utilisation, reproduction, modification ou distribution du present
 * fichier source sans contrat de licence ecrit de la part de la SARL Ether Création est
 * expressement interdite.
 * Pour obtenir une licence, veuillez contacter la SARL Ether Création a l'adresse: contact@ethercreation.com
 * ...........................................................................
 *
 * @author    Ether Création SARL <contact@ethercreation.com>
 * @copyright 2008-2018 Ether Création SARL
 * @license   Commercial license
 * International Registered Trademark & Property of PrestaShop Ether Création SARL
 */

require_once dirname(__FILE__) . '/../../../../config/config.inc.php';
require_once dirname(__FILE__) . '/../../class/catalog.class.php';
require_once dirname(__FILE__) . '/../../class/ecisuppcarrier.class.php';
require_once dirname(__FILE__) . '/../../class/bigjson.php';
use ecicdiscountpro\Catalog;
use ecicdiscountpro\Bigjson;

ignore_user_abort(true);
set_time_limit(0);

$paramHelp = Tools::getValue('help', null);
if (!is_null($paramHelp)) {
    $help = array(
        'ec_token' => array(
            'fr' => 'Token du module. Obligatoire.',
            'en' => 'Module\'s token. Required.'
            ),
        'connecteur' => array(
            'fr' => 'Connecteur à traiter. Obligatoire.',
            'en' => 'Supplier to treat. Required.'
            ),
    );
    Catalog::answer(Tools::jsonEncode($help));
    exit();
}

$token = Tools::getValue('ec_token');
if ($token != Catalog::getInfoEco('ECO_TOKEN')) {
    header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
    header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
    header('Cache-Control: no-store, no-cache, must-revalidate');
    header('Cache-Control: post-check=0, pre-check=0', false);
    header('Pragma: no-cache');
    Tools::redirect('Location: ../');
    exit();
}

$logger = Catalog::logStart('carriers');
$prefix = 'ECI_CAR_';

$stopTime = time() + 15;
$listStages = array(
    '1' => 'STARTED',
    '2' => 'GETFILE',
    '3' => 'BUILDINDEX',
);

// paramètres
$connecteur = 'cdiscountpro';

$paramNbCron = Tools::getValue('nbC', null);
$nbCron = is_null($paramNbCron) ? 0 : (int) $paramNbCron;

$paramJobID = Tools::getValue('jid', null);

$paramAct = Tools::getValue('act', null);
$action = (Catalog::jGet($prefix . 'ACT_' . $connecteur) === 'die') ? 'die' : ((empty($paramAct)) ? 'go' : $paramAct);

$paramSpy = Tools::getValue('spy', null);
$spy = (empty($paramSpy)) ? false : true;

$paramSpy2 = Tools::getValue('spytwo', null);
$spy2 = (empty($paramSpy2)) ? false : true;
$who = $spy ? ($spy2 ? 'spy2' : 'spy') : 'normal';

$paramKill = Tools::getValue('kill', null);
$kill = is_null($paramKill) ? false : true;

$paramPrg = Tools::getValue('prg', null);
$paramPos = Tools::getValue('pos', null);
if (!is_null($paramPrg)) {
    $prg = (int) $paramPrg;
    $pos = is_null($paramPos) ? 1 : (int) $paramPos;
    $chain = '&prg=' . $prg . '&pos=' . $pos;
} else {
    $prg = false;
    $chain = '';
}

//links
$eci_base_uri = (((Configuration::get('PS_SSL_ENABLED') == 1) && (Configuration::get('PS_SSL_ENABLED_EVERYWHERE') == 1)) ? 'https://' : 'http://' ) .
        Tools::getShopDomain() . __PS_BASE_URI__ . str_replace(_PS_ROOT_DIR_ . '/', '', dirname(__FILE__) . '/');
$ts = preg_replace('/0\.([0-9]{6}).*? ([0-9]+)/', '$2$1', microtime());
$jid = is_null($paramJobID) ? $ts : $paramJobID;
$params = '?ec_token=' . $token . '&ts=' . $ts . '&jid=' . $jid;
$carriers_uri = $eci_base_uri . basename(__FILE__) . $params . $chain;


// kill
if ($kill) {
    if (Catalog::jGet($prefix . 'STATE_' . $connecteur) != 'done') {
        Catalog::jUpdateValue($prefix . 'ACT_' . $connecteur, 'die');
    }
    exit('kill');
}


// espion
if ($spy) {
    Catalog::answer('spy');
    sleep(19);
    $state = Catalog::jGet($prefix . 'STATE_' . $connecteur);
    $progress = Catalog::jGet($prefix . 'PROGRESS_' . $connecteur);
    if ($nbCron == $progress) {
        if ($spy2) {
            if ($state != 'done') {
                Catalog::jUpdateValue($prefix . 'STATE_' . $connecteur, 'still');
            }
        } else {
            Catalog::followLink($carriers_uri . '&spy=1&spytwo=1&nbC=' . $progress);
        }
    } else {
        Catalog::followLink($carriers_uri . '&spy=1&nbC=' . $progress);
    }
    exit('bond');
}

if (false === ($ec_four = Catalog::getGenClassStatic($connecteur))) {
    exit('nogen');
}

// abandon ou initialisation
$etat = Catalog::jGet($prefix . 'STATE_' . $connecteur);
$starting = ((bool) $token) & is_null($paramSpy) & is_null($paramNbCron) & is_null($paramKill) & is_null($paramAct);
if (!$starting && ($action === 'die')) {
    // abandon demandé par un kill
    Catalog::jUpdateValue($prefix . 'STATE_' . $connecteur, 'done');
    Catalog::jUpdateValue($prefix . 'END_TIME_' . $connecteur, date('Y-m-d H:i:s'));
    Catalog::jUpdateValue($prefix . 'ACT_' . $connecteur, 'go');
    exit('dead');
}
if ($starting && ($etat === 'running')) {
    // tentative de double lancement à éviter
    $progress = Catalog::jGet($prefix . 'PROGRESS_' . $connecteur);
    // envoi d'espion pour déjouer un plantage de serveur pendant une mise à jour
    Catalog::followLink($carriers_uri . '&spy=1&nbC=' . (int) $progress);
    exit('nodouble');
}
if (!$starting && ($etat === 'still')) {
    // un espion a pensé à tort qu'on était planté mais on est là !
    Catalog::jUpdateValue($prefix . 'STATE_' . $connecteur, 'running');
    Catalog::followLink($carriers_uri . '&spy=1&nbC=' . $nbCron);
}
if ($starting) {
    // initialisation du process
    Catalog::jUpdateValue($prefix . 'START_TIME_' . $connecteur, date('Y-m-d H:i:s'));
    Catalog::jUpdateValue($prefix . 'END_TIME_' . $connecteur, '');
    Catalog::jUpdateValue($prefix . 'STAGE_' . $connecteur, $listStages['1']);
    Catalog::jUpdateValue($prefix . 'PROGRESS_' . $connecteur, 0);
    Catalog::jUpdateValue($prefix . 'PROGRESSMAX_' . $connecteur, 0);
    Catalog::jUpdateValue($prefix . 'LOOPS_' . $connecteur, 0);
    Catalog::jUpdateValue($prefix . 'STATE_' . $connecteur, 'running');
    Catalog::jUpdateValue($prefix . 'ACT_' . $connecteur, 'go');
    Catalog::jUpdateValue($prefix . 'MESSAGE_' . $connecteur, '');
    // lancement de l'espion
    Catalog::followLink($carriers_uri . '&spy=1&nbC=0');
} else {
    Catalog::jUpdateValue($prefix . 'PROGRESS_' . $connecteur, $nbCron);
}
$stage = Catalog::jGet($prefix . 'STAGE_' . $connecteur);


// gestion des reprises, ruptures, fin
if ($action === 'next') {
    $action = 'go';
    Catalog::jUpdateValue($prefix . 'ACT_' . $connecteur, 'go');

    $numStage = array_search($stage, $listStages, true);
    $keys = array_keys($listStages);
    $next = $nextKey = false;
    foreach ($keys as $key) {
        if ($next) {
            $nextKey = $key;
            break;
        }
        if ($numStage == $key) {
            $next = true;
        }
    }

    if ($nextKey) {
        $stage = $listStages[$nextKey];
        $nbCron = 0;
    } else {
        Catalog::jUpdateValue($prefix . 'STATE_' . $connecteur, 'done');
        Catalog::jUpdateValue($prefix . 'END_TIME_' . $connecteur, date('Y-m-d H:i:s'));
        if ($prg) {
            //chaining with other task
            $nextCron = Catalog::getNextCron($prg, $pos);
            if ($nextCron) {
                Catalog::followLink($nextCron['link'] . '&prg=' . $prg . '&pos=' . $nextCron['position']);
            }
        }

        exit('alldone');
    }
    Catalog::jUpdateValue($prefix . 'STAGE_' . $connecteur, $stage);
    Catalog::jUpdateValue($prefix . 'LOOPS_' . $connecteur, 0);
    Catalog::jUpdateValue($prefix . 'PROGRESS_' . $connecteur, 0);
    Catalog::jUpdateValue($prefix . 'PROGRESSMAX_' . $connecteur, 0);
}


// aiguillage
Catalog::answer($stage);
$function = 'stage'.array_search($stage, $listStages, true);
if (function_exists($function)) {
    $reps = $function($prefix, $connecteur, $nbCron, $stopTime, $logger, $ec_four);
    if ($reps === true) {
        Catalog::jUpdateValue($prefix . 'ACT_' . $connecteur, 'next');
        Catalog::followLink($carriers_uri . '&nbC=0&act=next');
    } elseif (is_numeric($reps)) {
        Catalog::jUpdateValue($prefix . 'LOOPS_' . $connecteur, Catalog::jGet($prefix . 'LOOPS_' . $connecteur) + 1);
        Catalog::followLink($carriers_uri . '&nbC=' . $reps);
    }
} else {
    exit('done');
}

if ($reps !== true && (!is_numeric($reps))) {
    Catalog::jUpdateValue($prefix . 'MESSAGE_' . $connecteur, date('Y-m-d H:i:s ') . var_export($reps, true));
    Catalog::logInfo(
        $logger,
        'carriers ' . $who . ', ' . $connecteur . ', stage ' . $stage . ', ' . var_export($reps, true)
    );
}


exit('Bye');


function stage1()
{
    echo 'Task successfully started.';

    return true;
}

function stage2($prefix, $connecteur, $nbCron, $stopTime, $logger, $ec_four)
{
    $type = 'carriers';
    if (!$nbCron) {
        Catalog::jUpdateValue($prefix . 'DATA_' . $connecteur, '');
        try {
            $retour = $ec_four->getFile($type);
        } catch (Exception $ex) {
            return 'Error in getFile : ' . $ex->getMessage();
        }
    } else {
        $json_param = Catalog::jGet($prefix . 'DATA_' . $connecteur);
        $tab_param = Tools::jsonDecode($json_param, true);
        if (!is_null($tab_param)) {
            try {
                $retour = $ec_four->getFile(
                    $type,
                    isset($tab_param['fichier'])?$tab_param['fichier']:'',
                    isset($tab_param['slice'])?$tab_param['slice']:0,
                    isset($tab_param['extparam'])?$tab_param['extparam']:''
                );
            } catch (Exception $ex) {
                return 'Error in getFile : ' . $ex->getMessage();
            }
        } else {
            try {
                $retour = $ec_four->getFile($type);
            } catch (Exception $ex) {
                return 'Error in getFile : ' . $ex->getMessage();
            }
        }
    }

    if (is_array($retour)) {
        if (!isset($retour['rc'])) {
            return $retour;
        } else {
            switch ((int)$retour['rc']) {
                case 1:
                    return true;
                case 2:
                    Catalog::jUpdateValue($prefix . 'DATA_' . $connecteur, Tools::jsonEncode($retour));
                    return (1 + $nbCron);
                default:
                    if (isset($retour['message'])) {
                        return $retour['message'];
                    } else {
                        return $retour;
                    }
            }
        }
    } else {
        $tab_ret = explode(',', $retour);
        if (2 > count($tab_ret)) {
            return 'Too few infos in return from getFile for flux "' . $type . '" : ' . $retour;
        }
        switch ((int)$tab_ret[0]) {
            case 1:
                return true;
            case 2:
                $tab_param = array();
                parse_str($tab_ret[1], $tab_param);
                /*Catalog::logInfo($logger, var_export($retour, true));
                Catalog::logInfo($logger, var_export($tab_ret, true));
                Catalog::logInfo($logger, var_export($tab_param, true));*/
                Catalog::jUpdateValue($prefix . 'DATA_' . $connecteur, Tools::jsonEncode($tab_param));
                return (1 + $nbCron);
            case 0:
                return $tab_ret[1];
            default:
                return 'Unknown error in getFile : ' . $retour;
        }
    }
}

function stage3($prefix, $connecteur, $nbCron, $stopTime, $logger, $ec_four)
{
    $dir = dirname(__FILE__) . '/../../files/';
    $file = $ec_four->carriers_name;

    try {
        $carriers = new Bigjson($file, $dir, false);
        $carriers->deleteIndex();
        $carriers->buildIndex('ProductId');
    } catch (Exception $e) {
        return 'Problème Bigjson : ' . $e->getMessage();
    }

    return true;
}
