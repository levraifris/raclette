<?php
/**
 * 2007-2017 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to a commercial license from SARL Ether Création
 * Use, copy, modification or distribution of this source file without written
 * license agreement from the SARL Ether Création is strictly forbidden.
 * In order to obtain a license, please contact us: contact@ethercreation.com
 * ...........................................................................
 * INFORMATION SUR LA LICENCE D'UTILISATION
 *
 * L'utilisation de ce fichier source est soumise a une licence commerciale
 * concedee par la societe Ether Création
 * Toute utilisation, reproduction, modification ou distribution du present
 * fichier source sans contrat de licence ecrit de la part de la SARL Ether Création est
 * expressement interdite.
 * Pour obtenir une licence, veuillez contacter la SARL Ether Création a l'adresse: contact@ethercreation.com
 * ...........................................................................
 *
 * @author    Ether Création SARL <contact@ethercreation.com>
 * @copyright 2008-2018 Ether Création SARL
 * @license   Commercial license
 * International Registered Trademark & Property of PrestaShop Ether Création SARL
 */

require_once dirname(__FILE__) . '/../../../class/catalog.class.php';
require_once dirname(__FILE__) . '/../../../class/reference.class.php';
require_once dirname(__FILE__) . '/../../../class/ecisuppcarrier.class.php';
require_once dirname(__FILE__) . '/../../../class/ecipricerules.class.php';
require_once dirname(__FILE__) . '/../../../class/bigjson.php';
use ecicdiscountpro\Catalog;
use ecicdiscountpro\ImporterReference;
use ecicdiscountpro\Bigjson;

class EccdiscountproMom
{
    public $fournisseur_glob = 'cdiscountpro';
    public $current_file = 'eccdiscountpromom';
    const BUILD = '20210212';

    const GEN_CONF = '{"friendly_name":"CDiscount Pro"}'; //see installGen
    const HIDE_TABS = '["Bjs Reader","Specific Prices"]'; //see install
    const HIDE_TASKS = ''; //see setTasksCon
    const HIDE_PARMS = '["paymodule","FORCE_PACK_COMP"]'; //see ParametreController
    const CAT_STAGES = '["started","getfile","deldata","refresh","deplcat","upkeep","findderef","upderef","ecitclean","upfluctuplus","upfluctuminus","upfluctums","ufcarrier","prules","suppcarriers"]'; //normal phases of catAuto pre4.2
    const STK_STAGES = '["started","getfile","updateStock","updatepriceruleprices","updatestockps","updatestockpsprice","updatestockafter","reactivate","stockenable"]';

    public $catalog_name = 'cdiscountpro_catalogue';
    public $stock_name = 'cdiscountpro_stock';
    public $stockprice_name = 'cdiscountpro_stockprice';
    public $carriers_name = 'cdiscountpro_carriers';
    public $ftp_carriers_name = 'Produits_ModLiv.csv';
    public $catalog_et      = 'Sku;Sku parent;Taille;EAN;Id Catégorie 1;Libellé Catégorie 1;Id Catégorie 2;Libellé Catégorie 2;Id Catégorie 3;Libellé Catégorie 3;Id Catégorie 4;Libellé Catégorie 4;Marque;Libellé;Description;Type produit;Indication stock;Prix barré HT;Prix achat HT (avec ecotaxes);Promotion;Remise (%);EcoTaxe HT;Sorecop;Taux de TVA;Image 1;Image 2;Image 3;Image 4;Image 5;Image 6;Longueur colis (cm);Largeur colis (cm);Hauteur colis (cm);Poids (g)'; //ancien format drop
    public $catalog_et_2    = 'Sku;Sku parent;Taille;EAN;Id Catégorie 1;Libellé Catégorie 1;Id Catégorie 2;Libellé Catégorie 2;Id Catégorie 3;Libellé Catégorie 3;Id Catégorie 4;Libellé Catégorie 4;Marque;Libellé;Description;Type produit;Indication stock;Prix barré HT;Prix achat HT (avec ecotaxes);Promotion;Prix Remisé HT;EcoTaxe HT;Sorecop;Taux de TVA;Image 1;Image 2;Image 3;Image 4;Image 5;Image 6;Longueur colis (cm);Largeur colis (cm);Hauteur colis (cm);Poids (g);Mode_liv'; //nouveau format dropshipping
    public $catalog_et_3    = 'Sku;Sku parent;Taille;EAN;Id Catégorie 1;Libellé Catégorie 1;Id Catégorie 2;Libellé Catégorie 2;Id Catégorie 3;Libellé Catégorie 3;Id Catégorie 4;Libellé Catégorie 4;Marque;Libellé;Description;Type produit;Indication stock;Prix barré HT;Prix achat HT (avec ecotaxes);Promotion;Prix Remisé HT;EcoTaxe HT;Sorecop;Taux de TVA;Image 1;Image 2;Image 3;Image 4;Image 5;Image 6;Longueur colis (cm);Largeur colis (cm);Hauteur colis (cm);Poids (g)'; //nouveau format international
    public $poids_unite = 'g';
    public $cdp_languages = array('fr', 'en', 'de', 'es');
    public $slice_size = 100000;
    public $tab_catalogue = array(
        'reference' => 'Sku',
        'product_reference' => 'Sku',
        'sku_parent' => 'Sku parent',
        'taille' => 'Taille',
        'ean13' => 'EAN',
        'category' => '',
        'manufacturer' => 'Marque',
        'name' => 'Libellé',
        'description' => 'Description',
        'short_description' => '',
        'prod_type' => 'Type produit',
        'stock' => 'Indication stock', // do not use this one, use stock file instead
        'pmvc' => 'Prix barré HT',
        'price' => 'Prix achat HT (avec ecotaxes)',
        'promo_flag' => 'Promotion',
        'discount' => 'Remise (%)',
        'discounted' => 'Prix Remisé HT',
        'ecotax' => 'EcoTaxe HT',
        'private_copy' => 'Sorecop',
        'rate' => 'Taux de TVA',
        'pictures' => '',
        'feature' => '',
        'dimensions' => '',
        'weight' => 'Poids (g)',
        'upc' => '',
        'ship' => '',
        'mode_livraison' => 'Mode_liv',
    );
    public $tab_features_cat = array(
        'Longueur colis (cm)' => 'Longueur colis (cm)',
        'Largeur colis (cm)' => 'Largeur colis (cm)',
        'Hauteur colis (cm)' => 'Hauteur colis (cm)',
    );
    public $tab_dims = array(
        'Lo' => 'Longueur colis (cm)',
        'La' => 'Largeur colis (cm)',
        'Hi' => 'Hauteur colis (cm)',
    );
    public $tab_categorie = array('Libellé Catégorie 1', 'Libellé Catégorie 2', 'Libellé Catégorie 3', 'Libellé Catégorie 4');
    public $tab_pictures = array('Image 1', 'Image 2', 'Image 3', 'Image 4', 'Image 5', 'Image 6');
    public $tab_keep = array(
        'type' => 'prod_type',
        'promo' => 'promo_flag',
        'liv' => 'mode_livraison',
    );
    public $html_fields = array();
    public $tab_stock = array(
        'reference' => 'Sku',
        'sku_parent' => 'Sku parent',
        'stock' => 'Indication stock',
        'pmvc' => 'Prix barré HT',
        'price' => 'Prix achat HT (avec ecotaxes)',
        'ecotax' => 'EcoTaxe HT',
        'discount' => 'Remise (%)',
        'discounted' => 'Prix Remisé HT',
        'promo_flag' => 'Promotion',
        'rate' => 'Taux de TVA',
    );
    public $carriers = array(
        'D' => array(
            'Colissimo avec signature' => 'COL',
    //        'Colissimo sans signature' => 'CDS',
            'Express avec signature' => 'TNT',
            //'Point retrait' => 'SO1',
            'Livraison standard' => 'LSP',
            'Livraison ECO' => 'TRP',
            //'Point retrait' => 'EMP',
            'Chronopost International' => 'CHD',
            'Transporteur DPS' => 'DPS',
            'Transporteur DPD' => 'DPD',
            'Transporteur COX' => 'COX',
        ),
        'I' => array(
            'Chronopost International' => 'CHD',
        )
    );
    public $carriers_type = array(
        'COL' => array('M30'),
        'CDS' => array('M30'),
        'DPS' => array('M30'),
        'DPD' => array('M30'),
        'COX' => array('M30'),
        'TNT' => array('M30'),
        'SO1' => array('M30'),
        'CHD' => array('M30', 'P30'),
        'LSP' => array('P30'),
        'TRP' => array('P30'),
        'EMP' => array('P30'),
    );
    public $carriers_by_modliv = array(
        'D' => array(
            'COL' => array(
                'M30' => array('COL')
            ),
            'DPS' => array(
                'M30' => array('DPS')
            ),
            'DPD' => array(
                'M30' => array('DPD')
            ),
            'TNT' => array(
                'M30' => array('TNT')
            ),
            'CHD' => array(
                'M30' => array('CHD')
            ),
            'COX' => array(
                'M30' => array('COX')
            ),
            'LSP' => array(
                'P30' => array('LSP')
            ),
            'TRP' => array(
                'P30' => array('TRP')
            ),
        ),
        'I' => array(
            1 => array(
                'M30' => array('CHD',),
                'P30' => array('CHD',)
            ),
        ),
    );
    public $dflt_carrier = array(
        'D' => array(
            'M30' => 'COL',
            'P30' => 'LSP',
        ),
        'I' => array(
            'M30' => 'CHD',
            'P30' => 'CHD',
        ),
    );
    public $tab_track = array(
        'refCampagne',
        'refCdp',
        'refPS',
        'CusName',
        'CusMail',
        'CusPhone',
        'CusMobile',
        'stCom',
        'stBP',
        'dtExp',
        'sku',
        'qt',
        'numero',
        'trkLink',
        'ordFile',
    );
    public $cdp_states = array(
        'CX' => 'Commande créée',
        'VX' => 'Commande validée',
        'VP' => 'Commande validée + BP en préparation',
        'XP' => 'Commande validée + BP en préparation',
        'XV' => 'Commande validée + BP expédié',
        'VC' => 'Commande validée + BP créé',
        'XC' => 'Commande validée + BP créé',
        'VV' => 'Commande validée + BP expédié',
        'VS' => 'Commande validée + BP expédié',
        'SP' => 'Commande validée + BP expédié',
        'SC' => 'Commande validée + BP expédié',
    );
    public $ost_cdp_to_ps = array(
        'CX' => 'id_ok',
        'VX' => 'id_ok',
        'VP' => 'id_ok',
        'XP' => 'id_ok',
        'XV' => 'id_ok',
        'VC' => 'id_ok',
        'XC' => 'id_ok',
        'VV' => 'id_ok',
        'VS' => 'id_ok',
        'SP' => 'id_ok',
        'SC' => 'id_ok',
    );
    protected $trans_stream = null;

    public $config;
    public $id_shop;
    public $valid_conf_keys = array(// all keys
        'sendorderbymail',
        'orderemail',
        'trans_mode',
        'sftp_use_key',
        'ftp_cat_log',
        'ftp_cat_pas',
        'cat_rsa_file',
        'ftp_com_log',
        'ftp_com_pas',
        'com_rsa_file',
        'cat_format',
        'ftp_cat_name',
        'ftp_stk_name',
        'id_cdiscountpro',
        'min_product_price',
        'max_product_price',
        'long_desc',
        'stock_val',
        'use_typed_carriers',
        'carriers_list',
        'dflt_carrier_M30',
        'dflt_carrier_P30',
        'ship_cost_first',
        'ship_cost_plus',
        'id_state',
        'id_rupt',
        'id_trans',
        'id_notrans',
        'id_ok',
        'id_err',
    );
    public $numeric_conf_keys = array(//only numeric
        'min_product_price',
        'max_product_price',
        'stock_val',
    );
    public $multiple_conf_keys = array(//from select multiple
    );
    public $array_conf_keys = array(//multiple values
        'carriers_list',
        'ship_cost_first',
        'ship_cost_plus',
        'ftp_cat_name',
   );
    public $allshop_keys = array(//keys that must be equal in all shops
        'trans_mode',
        'sftp_use_key',
        'ftp_cat_log',
        'ftp_cat_pas',
        'cat_rsa_file',
        'ftp_com_log',
        'ftp_com_pas',
        'com_rsa_file',
        'cat_format',
        'ftp_cat_name',
        'ftp_stk_name',
        'id_cdiscountpro',
        'min_product_price',
        'max_product_price',
        'long_desc',
        'stock_val',
        'use_typed_carriers',
        'carriers_list',
        'dflt_carrier_M30',
        'dflt_carrier_P30',
        'ship_cost_first',
        'ship_cost_plus',
        'id_state',
        'id_rupt',
        'id_trans',
        'id_notrans',
        'id_ok',
        'id_err',
    );
    public $uploaded_files = array(
        'cat_rsa_file' => 'cat_rsa_key',
        'com_rsa_file' => 'com_rsa_key',
    );

    public function __construct($id_shop = 0)
    {
        if (!$id_shop) {
            $id_shop = empty(Context::getContext()->shop->id)?Configuration::get('PS_SHOP_DEFAULT'):Context::getContext()->shop->id;
        }
        $this->id_shop = (int) $id_shop;
        $catalog = new Catalog();
        $this->config = $catalog->getConnecteurParameter($this, $this->id_shop);
    }

    public function persoFournisseur()
    {
        return array_merge(
            $this->config,
            array(
            )
        );
    }

    public function renderForm()
    {
        $eciClass = 'eci' . $this->fournisseur_glob;
        $mod = new $eciClass();
        $id_lang = Configuration::get('PS_LANG_DEFAULT');
        $thisAbo = $this->getAbo();
        
        $orderStates = array_merge(
            array(
                array(
                    'id_order_state' => 0,
                    'name' => '-- ' . $mod->l('Do nothing', 'ec' . $this->fournisseur_glob.'mom') . ' --'
                )
            ),
            OrderState::getOrderStates($id_lang)
        );
        
        $carriers = $carriers_M30 = $carriers_P30 = array();
        foreach ($this->carriers[$thisAbo] as $name => $key) {
            $carriers[] = array('name' => $name, 'id' => $key);
            if (isset($this->carriers_type[$key]) && in_array('M30', $this->carriers_type[$key])) {
                $carriers_M30[] = array('name' => $name, 'id' => $key);
            }
            if (isset($this->carriers_type[$key]) && in_array('P30', $this->carriers_type[$key])) {
                $carriers_P30[] = array('name' => $name, 'id' => $key);
            }
        }
        
        //get list of files if possible
        $ftp_files_list = array(
            array(
                'id' => 0,
                'name' => $mod->l('no file', 'ec'.$this->fournisseur_glob.'mom'),
            ),
        );
        $ftp_list = $this->transGetList();
        if ($ftp_list) {
            foreach ($ftp_list as $ftp_file) {
                $ftp_files_list[] = array('id' => $ftp_file, 'name' => $ftp_file);
            }
        }

        $fields_form_input = array(
            array(
                'type' => 'html',
                'name' => 'spacer',
                'label' => '',
                'html_content' => '<hr>',
                'col' => '12'
            )
        );
        if (!$ftp_list) {
            $fields_form_input[] = array(
                'type' => 'html',
                'name' => 'warningframe',
                'label' => '',
                //'html_content' => $mod->displayConfirmation($mod->l('OK', 'ec'.$this->fournisseur_glob.'mom')),
                //'html_content' => $mod->displayError($mod->l('Error', 'ec'.$this->fournisseur_glob.'mom')),
                'html_content' => $mod->displayWarning($mod->l('Please configure the FTP access first or retry later', 'ec'.$this->fournisseur_glob.'mom')),
                'col' => '6'
            );
        }
        $fields_form_input[] = array(
            'type' => 'html',
            'name' => 'title1',
            'label' => '',
            'html_content' => '<h3>' . $mod->l('Connection parameters', 'ec'.$this->fournisseur_glob.'mom') . '</h3>',
            'col' => '12'
        );
        $fields_form_input[] = array(
            'type' => 'switch',
            'name' => 'trans_mode',
            'label'=> $mod->l('Use SFTP instead of FTP', 'ec'.$this->fournisseur_glob.'mom'),
            'is_bool' => true,
            'condition' => true,
            'values' => array(
                array(
                    'id' => 'trans_mode_on',
                    'value' => 1,
                ),
                array(
                    'id' => 'trans_mode_off',
                    'value' => 0,
                ),
            ),
            'col' => '2',
        );
        if (!$eciClass::DEMO && !empty($this->config['trans_mode'])) {
            $fields_form_input[] = array(
                'type' => 'switch',
                'name' => 'sftp_use_key',
                'label'=> $mod->l('Use RSA/PPK key instead of password', 'ec'.$this->fournisseur_glob.'mom'),
                'is_bool' => true,
                'condition' => true,
                'values' => array(
                    array(
                        'id' => 'sftp_use_key_on',
                        'value' => 1,
                    ),
                    array(
                        'id' => 'sftp_use_key_off',
                        'value' => 0,
                    ),
                ),
                'col' => '2',
            );
        }
        if (!$eciClass::DEMO) {
            $fields_form_input[] = array(
                'type' => 'text',
                'name' => 'ftp_cat_log',
                'label'=> $mod->l('FTP catalog login', 'ec'.$this->fournisseur_glob.'mom'),
                'required' => true,
                'condition' => true,
                'col' => '2',
            );
        }
        if (!$eciClass::DEMO && empty($this->config['sftp_use_key'])) {
            $fields_form_input[] = array(
                'type' => 'text',
                'name' => 'ftp_cat_pas',
                'label'=> $mod->l('FTP catalog password', 'ec'.$this->fournisseur_glob.'mom'),
                'required' => true,
                'condition' => true,
                'col' => '2',
            );
        }
        if (!$eciClass::DEMO && !empty($this->config['trans_mode']) && !empty($this->config['sftp_use_key'])) {
            $fields_form_input[] = array(
                'type' => 'file',
                'name' => 'cat_rsa_file',
                'label'=> $mod->l('Upload your RSA/PPK key for catalog', 'ec'.$this->fournisseur_glob.'mom'),
                'condition' => true,
                'col' => 6,
            );
        }
        if (!$eciClass::DEMO) {
            $fields_form_input[] = array(
                'type' => 'text',
                'name' => 'ftp_com_log',
                'label'=> $mod->l('FTP orders login', 'ec'.$this->fournisseur_glob.'mom'),
                'required' => true,
                'condition' => true,
                'col' => '2',
            );
        }
        if (!$eciClass::DEMO && empty($this->config['sftp_use_key'])) {
            $fields_form_input[] = array(
                'type' => 'text',
                'name' => 'ftp_com_pas',
                'label'=> $mod->l('FTP orders password', 'ec'.$this->fournisseur_glob.'mom'),
                'required' => true,
                'condition' => true,
                'col' => '2',
            );
        }
        if (!$eciClass::DEMO && !empty($this->config['trans_mode']) && !empty($this->config['sftp_use_key'])) {
            $fields_form_input[] = array(
                'type' => 'file',
                'name' => 'com_rsa_file',
                'label'=> $mod->l('Upload your RSA/PPK key for orders', 'ec'.$this->fournisseur_glob.'mom'),
                'condition' => true,
                'col' => 6,
            );
        }
        if (!$eciClass::DEMO) {
            $fields_form_input[] = array(
                'type' => 'text',
                'name' => 'id_cdiscountpro',
                'label'=> $mod->l('ID CDiscount Pro', 'ec'.$this->fournisseur_glob.'mom'),
                'required' => true,
                'condition' => true,
                'col' => '2',
            );
        }
        $fields_form_input[] = array(
            'type' => 'html',
            'name' => 'title2',
            'label' => '',
            'html_content' => '<h3>' . $mod->l('Files parameters', 'ec'.$this->fournisseur_glob.'mom') . '</h3>',
            'col' => '12'
        );
        $fields_form_input[] = array(
            'type' => 'select',
            'name' => 'cat_format',
            'label'=> $mod->l('Catalog file format', 'ec'.$this->fournisseur_glob.'mom'),
            'required' => true,
            'condition' => true,
            'options' => array(
                'query' => array(
                    array(
                        'id' => '1',
                        'name' => $mod->l('Legacy format', 'ec'.$this->fournisseur_glob.'mom'),
                    ),
                    array(
                        'id' => '2',
                        'name' => $mod->l('Dropshipping format', 'ec'.$this->fournisseur_glob.'mom'),
                    ),
                    array(
                        'id' => '3',
                        'name' => $mod->l('Internatinal format', 'ec'.$this->fournisseur_glob.'mom'),
                    ),
                ),
                'id' => 'id',
                'name' => 'name',
            ),
            'col' => '2',
        );
        foreach (Language::getIsoIds(true) as $lang) {
            $fields_form_input[] = array(
                'type' => 'select',
                'name' => 'ftp_cat_name[' . $lang['iso_code']. ']',
                'label'=> $mod->l('Name of the catalog file on FTP for', 'ec'.$this->fournisseur_glob.'mom') . ' "' . $lang['iso_code'] . '"',
                'required' => true,
                'condition' => true,
                'options' => array(
                    'query' => $ftp_files_list,
                    'id' => 'id',
                    'name' => 'name',
                ),
                'col' => '2',
            );
        }
        $fields_form_input[] = array(
            'type' => 'select',
            'name' => 'ftp_stk_name',
            'label'=> $mod->l('Name of the stock file on FTP', 'ec'.$this->fournisseur_glob.'mom'),
            'required' => true,
            'condition' => true,
            'options' => array(
                'query' => $ftp_files_list,
                'id' => 'id',
                'name' => 'name',
            ),
            'col' => '2',
        );
        $fields_form_input[] = array(
            'type' => 'text',
            'name' => 'min_product_price',
            'label'=> $mod->l('Minimal product price', 'ec'.$this->fournisseur_glob.'mom'),
            'desc'=> $mod->l('0 or else', 'ec'.$this->fournisseur_glob.'mom'),
            'suffix' => '€',
            'condition' => true,
            'col' => '2',
        );
        $fields_form_input[] = array(
            'type' => 'text',
            'name' => 'max_product_price',
            'label'=> $mod->l('Maximal product price', 'ec'.$this->fournisseur_glob.'mom'),
            'desc'=> $mod->l('0 to disable functionality', 'ec'.$this->fournisseur_glob.'mom'),
            'suffix' => '€',
            'condition' => true,
            'col' => '2',
        );
        $fields_form_input[] = array(
            'type' => 'select',
            'name' => 'long_desc',
            'label'=> $mod->l('Put CDP description in', 'ec'.$this->fournisseur_glob.'mom'),
            'condition' => true,
            'options' => array(
                'query' => array(
                    array(
                        'name' => $mod->l('PS long description', 'ec'.$this->fournisseur_glob.'mom'),
                        'value' => 'l'
                    ),
                    array(
                        'name' => $mod->l('PS short description', 'ec'.$this->fournisseur_glob.'mom'),
                        'value' => 'c'
                    ),
                ),
                'id' => 'value',
                'name' => 'name',
            ),
            'col' => '2',
        );
        if (!isset($this->config['stock_val']) || '' === $this->config['stock_val']) {
            $this->config['stock_val'] = 5;
        }
        $fields_form_input[] = array(
            'type' => 'text',
            'name' => 'stock_val',
            'label'=> $mod->l('Default stock value if not specified in stock file', 'ec'.$this->fournisseur_glob.'mom'),
            'desc'=> $mod->l('If the stock files does not specify the quantity, this will be the quantity of available products', 'ec'.$this->fournisseur_glob.'mom'),
            'condition' => true,
            'col' => '2',
        );
        $fields_form_input[] = array(
            'type' => 'html',
            'name' => 'title3',
            'label' => '',
            'html_content' => '<h3>' . $mod->l('Shipping parameters', 'ec'.$this->fournisseur_glob.'mom') . '</h3>',
            'col' => '12'
        );
        $fields_form_input[] = array(
            'type' => 'switch',
            'name' => 'use_typed_carriers',
            'label'=> $mod->l('Associate appropriate carriers for each product type', 'ec'.$this->fournisseur_glob.'mom'),
            'is_bool' => true,
            'condition' => true,
            'values' => array(
                array(
                    'id' => 'use_typed_carriers_on',
                    'value' => 1,
                ),
                array(
                    'id' => 'use_typed_carriers_off',
                    'value' => 0,
                ),
            ),
            'col' => '2',
        );
        $fields_form_input[] = array(
            'type' => 'select',
            'multiple' => true,
            'name' => 'carriers_list[]',
            'label'=> $mod->l('Select possible carriers', 'ec'.$this->fournisseur_glob.'mom'),
            'condition' => true,
            'options' => array(
                'query' => $carriers,
                'id' => 'id',
                'name' => 'name',
            ),
            'col' => '2',
        );
        $fields_form_input[] = array(
            'type' => 'select',
            'name' => 'dflt_carrier_M30',
            'label'=> $mod->l('Select default carrier for M30', 'ec'.$this->fournisseur_glob.'mom'),
            'condition' => true,
            'options' => array(
                'query' => $carriers_M30,
                'id' => 'id',
                'name' => 'name',
            ),
            'col' => '2',
        );
        $fields_form_input[] = array(
            'type' => 'select',
            'name' => 'dflt_carrier_P30',
            'label'=> $mod->l('Select default carrier for P30', 'ec'.$this->fournisseur_glob.'mom'),
            'condition' => true,
            'options' => array(
                'query' => $carriers_P30,
                'id' => 'id',
                'name' => 'name',
            ),
            'col' => '2',
        );
        foreach ($carriers as $carrier) {
            if (!isset($this->config['ship_cost_first'][$carrier['id']]) || '' === $this->config['ship_cost_first'][$carrier['id']]) {
                $this->config['ship_cost_first'][$carrier['id']] = 0;
            }
            if (!isset($this->config['ship_cost_plus'][$carrier['id']]) || '' === $this->config['ship_cost_plus'][$carrier['id']]) {
                $this->config['ship_cost_plus'][$carrier['id']] = 0;
            }
            $fields_form_input[] = array(
                'type' => 'text',
                'name' => 'ship_cost_first[' . $carrier['id'] . ']',
                'label'=> $mod->l('Shipping cost of the first product for carrier ', 'ec'.$this->fournisseur_glob.'mom') . '"' . $carrier['name'] . '" (' . $carrier['id'] . ')',
                'suffix' => '€',
                'required' => true,
                'condition' => true,
                'col' => '2',
            );
            $fields_form_input[] = array(
                'type' => 'text',
                'name' => 'ship_cost_plus[' . $carrier['id'] . ']',
                'label'=> $mod->l('Shipping cost of any additional product for carrier ', 'ec'.$this->fournisseur_glob.'mom') . '"' . $carrier['name'] . '" (' . $carrier['id'] . ')',
                'suffix' => '€',
                'required' => true,
                'condition' => true,
                'col' => '2',
            );
        }
        $fields_form_input[] = array(
            'type' => 'html',
            'name' => 'title4',
            'label' => '',
            'html_content' => '<h3>' . $mod->l('Orders parameters', 'ec'.$this->fournisseur_glob.'mom') . '</h3>',
            'col' => '12'
        );
        $fields_form_input[] = array(
            'type' => 'switch',
            'name' => 'sendorderbymail',
            'label'=> $mod->l('Send orders also by email', 'ec'.$this->fournisseur_glob.'mom'),
            'is_bool' => true,
            'condition' => true,
            'values' => array(
                array(
                    'id' => 'sendorderbymail_on',
                    'value' => 1,
                ),
                array(
                    'id' => 'sendorderbymail_off',
                    'value' => 0,
                ),
            ),
            'col' => '2',
        );
        $fields_form_input[] = array(
            'type' => 'text',
            'name' => 'orderemail',
            'label'=> $mod->l('Send orders to these emails', 'ec'.$this->fournisseur_glob.'mom'),
            'desc'=> $mod->l('Use semicolon between recipients', 'ec'.$this->fournisseur_glob.'mom'),
            'condition' => true,
            'col' => '2',
        );
        $fields_form_input[] = array(
            'type' => 'select',
            'name' => 'id_trans',
            'label'=> $mod->l('Status of order transmitted to CDP', 'ec'.$this->fournisseur_glob.'mom'),
            'condition' => true,
            'options' => array(
                'query' => $orderStates,
                'id' => 'id_order_state',
                'name' => 'name',
            ),
            'col' => '2',
        );
        $fields_form_input[] = array(
            'type' => 'select',
            'name' => 'id_notrans',
            'label'=> $mod->l('Status of order NOT transmitted to CDP', 'ec'.$this->fournisseur_glob.'mom'),
            'condition' => true,
            'options' => array(
                'query' => $orderStates,
                'id' => 'id_order_state',
                'name' => 'name',
            ),
            'col' => '2',
        );
        $fields_form_input[] = array(
            'type' => 'select',
            'name' => 'id_rupt',
            'label'=> $mod->l('Status of order with out of stock products', 'ec'.$this->fournisseur_glob.'mom'),
            'condition' => true,
            'options' => array(
                'query' => $orderStates,
                'id' => 'id_order_state',
                'name' => 'name',
            ),
            'col' => '2',
        );
        $fields_form_input[] = array(
            'type' => 'select',
            'name' => 'id_ok',
            'label'=> $mod->l('Status of order OK with CDP', 'ec'.$this->fournisseur_glob.'mom'),
            'condition' => true,
            'options' => array(
                'query' => $orderStates,
                'id' => 'id_order_state',
                'name' => 'name',
            ),
            'col' => '2',
        );
        $fields_form_input[] = array(
            'type' => 'select',
            'name' => 'id_err',
            'label'=> $mod->l('Status of order with errors', 'ec'.$this->fournisseur_glob.'mom'),
            'condition' => true,
            'options' => array(
                'query' => $orderStates,
                'id' => 'id_order_state',
                'name' => 'name',
            ),
            'col' => '2',
        );

        return $fields_form_input;
    }

    public function getFile($type = 'catalog', $fichier = '', $slice = 0, $extparam = false)
    {
        $logger = Catalog::logStart(__FUNCTION__);
        $dir = dirname(__FILE__) . '/../../../files/';
//        if ('catalog' == $type || 'stock' == $type) {
//            if (true && preg_match('/^ec.*test$/', Configuration::get('PS_SHOP_NAME'))) {
//                sleep(5);
//                return array('rc' => 1);
//            }
//        }
        $eciClass = 'eci' . $this->fournisseur_glob;
        if ($eciClass::DEMO || 'tracking' == $type) {
            return array('rc' => 1);
        }

        //en tête pour l'internationnal si on a un fichier stock spécifique
//        if (!empty($this->config['ftp_stk_name']) && !empty($this->config['ftp_cat_name']) && is_array($this->config['ftp_cat_name']) && !in_array($this->config['ftp_stk_name'], $this->config['ftp_cat_name'])) {
//            $this->catalog_et = $this->catalog_et_3;
//        } else {
//            //nouvel en-tête dropshipping
//            $this->catalog_et = $this->catalog_et_2;
//        }
        //en tête pour l'internationnal si on a un fichier stock spécifique
        if (isset($this->config['cat_format']) && 3 == $this->config['cat_format']) {
            $this->catalog_et = $this->catalog_et_3;
        } elseif (isset($this->config['cat_format']) && 2 == $this->config['cat_format']) {
            //nouvel en-tête dropshipping
            $this->catalog_et = $this->catalog_et_2;
        }

        $urls = array(
            'catalog' => array(
            ),
            'stock' => array(
                $this->stock_name => array(
                    'file' => $this->config['ftp_stk_name'],
                    'et' => 1,
                    'icallback' => array(get_class(), 'filterStockFields')
                ),
            ),
            'carriers' => array(
                $this->carriers_name => array(
                    'file' => $this->ftp_carriers_name,
                    'et' => 1,
                    'icallback' => null
                )
            ),
        );
        switch ($type) {
            case 'catalog':
                $id_lang = Configuration::get('PS_LANG_DEFAULT');
                $iso_lang = Language::getIsoById($id_lang);
                $default_language_added = false;
                foreach (Language::getIsoIds(true) as $language) {
                    if (empty($this->config['ftp_cat_name'][$language['iso_code']])) {
                        continue;
                    }
                    $urls['catalog'][$this->catalog_name . '_' . $language['iso_code']] = array(
                        'file' => $this->config['ftp_cat_name'][$language['iso_code']],
                        'et' => explode(';', $this->catalog_et),
                        'icallback' => null
                    );
                    if ($iso_lang == $language['iso_code']) {
                        $default_language_added = true;
                    }
                }
                if (!$default_language_added) {
                    return array('rc' => 0, 'message' => 'Please chose a file for the default language "'.$iso_lang.'"');
                }
                break;
            case 'stock':
                $id_lang = Configuration::get('PS_LANG_DEFAULT');
                $iso_lang = Language::getIsoById($id_lang);
                if (!empty($this->config['ftp_cat_name'][$iso_lang])) {
                    $urls['stock'][$this->stockprice_name] = array(
                        'file' => $this->config['ftp_cat_name'][$iso_lang],
                        'et' => explode(';', $this->catalog_et),
                        'icallback' => array(array(get_class(), 'filterFields'), array(array_values($this->tab_stock))),
                    );
                } else {
                    return array('rc' => 0, 'message' => 'Please chose a file for the default language "'.$iso_lang.'"');
                }
                break;
        }

        if (empty($urls[$type])) {
            return array('rc' => 0, 'message' => 'Aucun fichier');
        }

        $work = $urls[$type];
        $nTraite = 0;
        $tabFiles = array_keys($work);
        $file_to_get = ('' === $fichier) ? $tabFiles[$nTraite] : $fichier;
        foreach ($urls[$type] as $fileName => $fileInfos) {
            if ($file_to_get && ($file_to_get !== $fileName)) {
                $nTraite++;
                continue;
            }

            $finished = false;
            if (!$slice) {
                $already_dl = false;
                if ('catalog' == $type) {
                    $first_file = array_search($fileInfos['file'], array_combine(array_keys($urls[$type]), array_column($urls[$type], 'file')), true); // attention PHP >= 5.5.0
                    if ($fileName !== $first_file) {
                        copy($dir . $first_file . Bigjson::DX, $dir . $fileName . Bigjson::DX);
                        copy($dir . $first_file . Bigjson::IX, $dir . $fileName . Bigjson::IX);
                        $already_dl = true;
                        $finished = true;
                    }
                }

                if (!$already_dl) {
                    if (false === $this->transLogin()) {
                        return array('rc' => 0, 'message' => 'Connexion impossible au FTP '.$this->fournisseur_glob);
                    }
                    if (false === $this->transGetFile($dir . $fileName . '.csv', $fileInfos['file'])) {
                        return array('rc' => 0, 'message' => 'Impossible de récupérer le fichier ' . $fileInfos['file']);
                    }
                    $this->transLogoff();

                    $old_files = glob($dir . $fileName . '_*.csv');
                    if ($old_files) {
                        foreach ($old_files as $old_file) {
                            unlink($old_file);
                        }
                    }
                    $in = fopen($dir . $fileName . '.csv', 'r');
                    $out = false;
                    $n = $s = 0;
                    $h = '';
                    while (($line = stream_get_line($in, 8192, "\n"))) {
                        $n++;
                        if (1 === $n) {
                            $h = $line;
                        }
                        $s = 1 + intdiv($n, $this->slice_size);
                        if (!$out) {
                            if (file_exists($dir . $fileName . '_' . $s . '.csv')) {
                                unlink($dir . $fileName . '_' . $s . '.csv');
                            }
                            $out = fopen($dir . $fileName . '_' . $s . '.csv', 'w');
                            if (1 !== $s) {
                                fwrite($out, $h . "\n");
                            }
                        }
                        fwrite($out, $line . "\n");
                        if (0 === ($n % $this->slice_size)) {
                            fclose($out);
                            $out = false;
                        }
                    }
                    //count files to get
                    $cnt = 0;
                    $items = array();
                    foreach ($work as $item) {
                        $s_item = Tools::jsonEncode($item);
                        if (!in_array($s_item, $items)) {
                            $items[] = $s_item;
                            $cnt++;
                        }
                    }
                    Catalog::jUpdateValue('ECI_' . ('catalog' == $type ? 'CC' : 'STK') . '_PROGRESSMAX_' . $this->fournisseur_glob, ((1 + $s) * $cnt));
                }
            } else {
                $file_to_convert = $dir . $fileName . '_' . $slice . '.csv';
                if (file_exists($file_to_convert)) {
                    rename($file_to_convert, $dir . $fileName . '.csv');
                    $bjret = Bigjson::csv2bjs($dir . $fileName . '.csv', $fileInfos['et'], ';', '"', '"', "\n", 'cp1252', null, 'trim', ((bool)($slice - 1)), $fileInfos['icallback']);
                    if (!is_numeric($bjret)) {
                        return array('rc' => 0, 'message' => 'Erreur lors du passge du fichier ' . $type . ' en bigjson : ' . $bjret);
                    }
                } else {
                    $finished = true;
                }
            }

            if (!$finished) {
                return array('rc' => 2, 'fichier' => $file_to_get, 'slice' => (1 + $slice));
            }

            if (isset($tabFiles[($nTraite + 1)])) {
                return array('rc' => 2, 'fichier' => $tabFiles[($nTraite + 1)]);
            }
        }

        return array('rc' => 1);
    }

    public static function filterFields($item, $filter)
    {
        return array_intersect_key($item, array_flip($filter));
    }

    public static function filterStockFields($item)
    {
        return array(
            'Sku' => isset($item['sku']) ? $item['sku'] : $item['Sku'],
            'Indication stock' => $item['Indication stock'],
        );
    }
    
    public function getAbo()
    {
        if (3 == $this->config['cat_format']) {
            return 'I';
        }
        
        return 'D';
    }

    public function transLogin($com = false)
    {
        if (empty($this->config['trans_mode'])) {
            return $this->ftpLogin($com);
        }

        return $this->sftpLogin($com);
    }

    public function transChDir($path = '/')
    {
        if (is_null($this->trans_stream)) {
            $this->transLogin();
        }

        if (!$this->trans_stream) {
            return false;
        }

        if (empty($this->config['trans_mode'])) {
            return $this->ftpChDir($path);
        }

        return $this->sftpChDir($path);
    }

    public function transGetList($path = '/')
    {
        if (is_null($this->trans_stream)) {
            $this->transLogin();
        }

        if (!$this->trans_stream) {
            return false;
        }

        if (empty($this->config['trans_mode'])) {
            return $this->ftpGetList($path);
        }

        return $this->sftpGetList($path);
    }

    public function transGetFile($fileName, $url)
    {
        if (is_null($this->trans_stream)) {
            $this->transLogin();
        }

        if (!$this->trans_stream) {
            return false;
        }

        if (empty($this->config['trans_mode'])) {
            return $this->ftpGet($fileName, $url);
        }

        return $this->sftpGet($fileName, $url);
    }

    public function transFileRename($old_url, $new_url)
    {
        if (is_null($this->trans_stream)) {
            $this->transLogin();
        }

        if (!$this->trans_stream) {
            return false;
        }

        if (empty($this->config['trans_mode'])) {
            return $this->ftpRename($old_url, $new_url);
        }

        return $this->sftpRename($old_url, $new_url);
    }

    public function transFileDelete($url)
    {
        if (is_null($this->trans_stream)) {
            $this->transLogin();
        }

        if (!$this->trans_stream) {
            return false;
        }

        if (empty($this->config['trans_mode'])) {
            return $this->ftpDelete($url);
        }

        return $this->sftpDelete($url);
    }

    public function transSendFile($filename, $targetPath = '/')
    {
        if (is_null($this->trans_stream)) {
            $this->transLogin();
        }

        if (!$this->trans_stream) {
            return false;
        }

        if (empty($this->config['trans_mode'])) {
            return $this->ftpSend($filename, $targetPath);
        }

        return $this->sftpSend($filename, $targetPath);
    }

    public function transLogoff()
    {
        if (is_null($this->trans_stream)) {
            return true;
        }

        if (empty($this->config['trans_mode'])) {
            ftp_close($this->trans_stream);
        }
        $this->trans_stream = null;

        return true;
    }

    public function ftpLogin($com = false)
    {
        $logger = Catalog::logStart(__FUNCTION__);

        // get parameters from configuration
        $ftp_address = 'ftp.cdiscount.com';
        if ($com) {
            $ftp_login = empty($this->config['ftp_com_log']) ? false : $this->config['ftp_com_log'];
            $ftp_pswd = empty($this->config['ftp_com_pas']) ? false : $this->config['ftp_com_pas'];
        } else {
            $ftp_login = empty($this->config['ftp_cat_log']) ? false : $this->config['ftp_cat_log'];
            $ftp_pswd = empty($this->config['ftp_cat_pas']) ? false : $this->config['ftp_cat_pas'];
        }
        if (!$ftp_login || !$ftp_pswd) {
            return false;
        }

        if (false === ($ftp_stream = ftp_connect($ftp_address))) {
            Catalog::logError($logger, 'Connexion impossible au FTP ' . $ftp_address);
            return false;
        }
        if (false === @ftp_login($ftp_stream, $ftp_login, $ftp_pswd)) {
            Catalog::logError($logger, 'Authentification impossible au FTP ' . $ftp_address);
            return false;
        }
        ftp_pasv($ftp_stream, true);

        $this->trans_stream = $ftp_stream;

        return true;
    }

    public function sftpLogin($com = false)
    {
        $logger = Catalog::logStart(__FUNCTION__);
        set_include_path(dirname(__FILE__) . '/phpseclib' . PATH_SEPARATOR . get_include_path());
        require_once 'Net/SFTP.php';
//        define('NET_SFTP_LOGGING', NET_SFTP_LOG_COMPLEX);
//        require_once 'Net/SSH2.php';
        require_once 'Crypt/RSA.php';
        $dir = dirname(__FILE__) . '/../../../files/import/';

        // get parameters from configuration
        $sftp_address = 'ftp.cdiscount.com';
        if ($com) {
            $sftp_login = empty($this->config['ftp_com_log']) ? false : $this->config['ftp_com_log'];
            $sftp_pswd = (empty($this->config['ftp_com_pas']) || !empty($this->config['sftp_use_key'])) ? false : $this->config['ftp_com_pas'];
            if (empty($this->config['sftp_use_key'])) {
                $sftp_key = false;
            } else {
                if (file_exists($dir . $this->uploaded_files['com_rsa_file'])) {
                    $sftp_key = Tools::file_get_contents($dir . $this->uploaded_files['com_rsa_file']);
                } else {
                    $sftp_key = false;
                }
            }
        } else {
            $sftp_login = empty($this->config['ftp_cat_log']) ? false : $this->config['ftp_cat_log'];
            $sftp_pswd = (empty($this->config['ftp_cat_pas']) || !empty($this->config['sftp_use_key'])) ? false : $this->config['ftp_cat_pas'];
            if (empty($this->config['sftp_use_key'])) {
                $sftp_key = false;
            } else {
                if (file_exists($dir . $this->uploaded_files['cat_rsa_file'])) {
                    $sftp_key = Tools::file_get_contents($dir . $this->uploaded_files['cat_rsa_file']);
                } else {
                    $sftp_key = false;
                }
            }
        }
        if (!$sftp_login || (!$sftp_pswd && !$sftp_key)) {
            return false;
        }

        if (false === ($sftp_stream = new Net_SFTP($sftp_address))) {
            Catalog::logError($logger, 'Connexion impossible au SFTP "' . $sftp_address . '" : ' . var_export(error_get_last(), true));
            return false;
        }
        if (!empty($this->config['sftp_use_key']) && $sftp_key) {
            $key = new Crypt_RSA();
            $key->loadKey($sftp_key);
        } else {
            $key = false;
        }
        if (false === $sftp_stream->login($sftp_login, ($key ? $key : $sftp_pswd))) {
            Catalog::logError($logger, 'Authentification impossible au SFTP "' . $sftp_address . '" : ' . var_export(error_get_last(), true));
            return false;
        }

        $this->trans_stream = $sftp_stream;

        return true;
    }

    public function ftpChDir($path = '/')
    {
        $logger = Catalog::logStart(__FUNCTION__);

        if (is_null($this->trans_stream)) {
            return false;
        }

        if (false === ftp_chdir($this->trans_stream, $path)) {
            Catalog::logError($logger, 'Impossible de trouver le dossier ' . $path);
            return false;
        }

        return true;
    }

    public function sftpChDir($path = '/')
    {
        $logger = Catalog::logStart(__FUNCTION__);

        if (is_null($this->trans_stream)) {
            return false;
        }

        if (false === $this->trans_stream->chdir($path)) {
            Catalog::logError($logger, 'Impossible de trouver le dossier ' . $path);
            return false;
        }

        return true;
    }

    public function ftpGetList($path = '/')
    {
        $logger = Catalog::logStart(__FUNCTION__);

        if (is_null($this->trans_stream)) {
            return false;
        }

        if (false === ($files = ftp_nlist($this->trans_stream, $path))) {
            Catalog::logError($logger, 'Impossible de scanner le dossier ' . $path);
            return false;
        }

        return $files;
    }

    public function sftpGetList($path = '/')
    {
        $logger = Catalog::logStart(__FUNCTION__);

        if (is_null($this->trans_stream)) {
            return false;
        }

        if (false === ($files = $this->trans_stream->nlist($path))) {
            Catalog::logError($logger, 'Impossible de scanner le dossier ' . $path);
            return false;
        }

        return $files;
    }

    public function ftpGet($fileName, $url, $mode = FTP_BINARY)
    {
        $logger = Catalog::logStart(__FUNCTION__);

        if (is_null($this->trans_stream)) {
            return false;
        }

        if (false === ftp_get($this->trans_stream, $fileName, $url, $mode)) {
            Catalog::logError($logger, 'Impossible de récupérer le fichier ' . $url);
            return false;
        }

        return true;
    }

    public function sftpGet($fileName, $url)
    {
        $logger = Catalog::logStart(__FUNCTION__);

        if (is_null($this->trans_stream)) {
            return false;
        }

        if (false === $this->trans_stream->get($url, $fileName)) {
            Catalog::logError($logger, 'Impossible de récupérer le fichier ' . $url);
            return false;
        }

        return true;
    }

    public function ftpDelete($url)
    {
        $logger = Catalog::logStart(__FUNCTION__);

        if (is_null($this->trans_stream)) {
            return false;
        }

        if (false === ftp_delete($this->trans_stream, $url)) {
            Catalog::logError($logger, 'Impossible de supprimer le fichier ' . $url);
            return false;
        }

        return true;
    }

    public function sftpDelete($url)
    {
        $logger = Catalog::logStart(__FUNCTION__);

        if (is_null($this->trans_stream)) {
            return false;
        }

        if (false === $this->trans_stream->delete($url)) {
            Catalog::logError($logger, 'Impossible de supprimer le fichier ' . $url);
            return false;
        }

        return true;
    }

    public function ftpRename($old_url, $new_url)
    {
        $logger = Catalog::logStart(__FUNCTION__);

        if (is_null($this->trans_stream)) {
            return false;
        }

        if (false === ftp_rename($this->trans_stream, $old_url, $new_url)) {
            Catalog::logError($logger, 'Impossible de renommer le fichier ' . $old_url);
            return false;
        }

        return true;
    }

    public function sftpRename($old_url, $new_url)
    {
        $logger = Catalog::logStart(__FUNCTION__);

        if (is_null($this->trans_stream)) {
            return false;
        }

        if (false === $this->trans_stream->rename($old_url, $new_url)) {
            Catalog::logError($logger, 'Impossible de renommer le fichier ' . $old_url);
            return false;
        }

        return true;
    }

    public function ftpSend($filename, $targetPath = '/', $mode = FTP_BINARY)
    {
        $logger = Catalog::logStart(__FUNCTION__);

        if (is_null($this->trans_stream)) {
            return false;
        }

        $name = basename($filename);

        if (false === ftp_put($this->trans_stream, $targetPath . $name, $filename, $mode)) {
            Catalog::logError($logger, 'Impossible de poser le fichier ' . $filename);
            return false;
        }

        return true;
    }

    public function sftpSend($filename, $targetPath = '/')
    {
        $logger = Catalog::logStart(__FUNCTION__);

        if (is_null($this->trans_stream)) {
            return false;
        }

        $name = basename($filename);

        if (false === $this->trans_stream->put($targetPath . $name, $filename, NET_SFTP_LOCAL_FILE)) {
            Catalog::logError($logger, 'Impossible de poser le fichier ' . $filename);
            return false;
        }

        return true;
    }

    public function fillCatalog($row = 0, $rowRef = '', $tentative = 0, $fichierActu = 0, $nbFichier = 0, $lignesTot = 0, $tot = 0, $act = 0)
    {
        $catalog = new Catalog();
        $stopTime = time() + $catalog->limitMax;
        $langId = Configuration::get('PS_LANG_DEFAULT');
        $langIso = Language::getIsoById($langId);
        $langTag = $langIso . ':://::';
        $coef = Catalog::getCurrConvFactor($this->fournisseur_glob);
        $ps_weight_unit = Tools::strtolower(Configuration::get('PS_WEIGHT_UNIT'));
        $decline = (bool) Combination::isFeatureActive();
        $thisAbo = $this->getAbo();

        $logger = Catalog::logStart('catalog');

        $products = array();
        if (!is_array($this->config['ftp_cat_name'])) {
            return 'Bad configuration for language files';
        }
        try {
            foreach ($this->config['ftp_cat_name'] as $iso => $cat_file) {
                if ($cat_file) {
                    $products[$iso] = new Bigjson($this->catalog_name . '_' . $iso, Catalog::FILES_PATH, false);
                }
            }
        } catch (Exception $e) {
            return $e->getMessage();
        }
        if (empty($products[$langIso])) {
            return 'No file for default language ' . $langIso;
        }

        if (0 >= $row) {
            $row = 0;
            foreach ($products as $product) {
                $product->deleteIndex();
            }
            $lignesTot = $products[$langIso]->maxLine;
        }

        //cache only combinations
        $insert_parents = array();
        $insert_combinations = array();
        $products[$langIso]->go($row);
        while ($item = $products[$langIso]->read()) {
            if (((time() > $stopTime) && ($products[$langIso]->currentLine > $row)) ||
                (count($insert_parents) >= $catalog->limitRowMax) ||
                (count($insert_combinations) >= $catalog->limitRowMax)) {
                break;
            }

            $tab_prod = array();
            foreach ($this->tab_catalogue as $field => $tag) {
                if ($tag && array_key_exists($tag, $item)) {
                    $tab_prod[$field] = trim($item[$tag]);
                }
            }

            if (!trim($tab_prod['reference']) || ($this->tab_catalogue['reference'] === $tab_prod['reference'])) {
                continue;
            }

            //get type of line : declined or not and parent or not
//            $declined = $decline && (bool) trim($tab_prod['sku_parent']); // en attendant les vraies déclinaisons
            $declined = false; // en attendant de vérifier la présence de taille ou apparition d'un autre attribut
            $parent = $declined && ($products[$langIso]->currentLine === $products[$langIso]->searchOne('Sku parent', $tab_prod['sku_parent']));

            // correction of type
            $tab_prod['prod_type'] = !empty($tab_prod['prod_type']) ? Tools::substr($tab_prod['prod_type'], 0, 3) : '';

            // rate correction
            $tab_prod['rate'] = str_replace(',', '.', $tab_prod['rate']);
            if (is_numeric($tab_prod['rate']) && $tab_prod['rate'] > 0) {
                if ($tab_prod['rate'] < 1) {
                    $tab_prod['rate'] *= 100;
                }
                array_push($catalog->tabTVA, $tab_prod['rate']);
            } else {
                $tab_prod['rate'] = 0;
            }

            //correction of ecotax
            $tab_prod['ecotax'] = str_replace(',', '.', $tab_prod['ecotax']);

            //correction of ean13
            if (!preg_match('/^[0-9]{13}$/', $tab_prod['ean13'])) {
                $tab_prod['ean13'] = '';
            }

            // correction of prices
            $tab_prices = $this->calculatePrices(
                $coef * (float) $tab_prod['price'],
                $coef * (float) ((empty($tab_prod['pmvc']) ? $tab_prod['price'] : $tab_prod['pmvc'])),
                $tab_prod['rate'],
                $coef * $tab_prod['ecotax'],
                (empty($tab_prod['discount']) ? 0 : $tab_prod['discount']),
                $coef * ((empty($tab_prod['discounted']) ? 0 : $tab_prod['discounted']))
            );
            $tab_prod['price'] = $tab_prices['price'];
            $tab_prod['pmvc'] = $tab_prices['pmvc'];

            // filter from prices out of range
            if (!empty($this->config['max_product_price']) && '' !== $this->config['min_product_price'] &&
                ($tab_prod['price'] < $this->config['min_product_price'] || $tab_prod['price'] > $this->config['max_product_price'])) {
                continue;
            }

            // correction of weight
            $su_weight_unit = Tools::strtolower($this->poids_unite);
            if ($ps_weight_unit !== $su_weight_unit) {
                if ($ps_weight_unit === 'k' . $su_weight_unit) {
                    $tab_prod['weight'] /= 1000;
                } elseif ('k' . $ps_weight_unit === $su_weight_unit) {
                    $tab_prod['weight'] *= 1000;
                }
            }

            // get infos from all configured languages
            $tab_prod_lang = array();
            foreach ($this->config['ftp_cat_name'] as $iso => $cat_file) {
                if ($cat_file) {
                    $idpl = $products[$iso]->searchOne($this->tab_catalogue['product_reference'], $tab_prod['reference'], Bigjson::EXACT);
                    if (false !== $idpl) {
                        $tab_prod_lang[$iso] = $products[$iso]->get($idpl);
                    }
                }
            }

            // combination case -> creation of attributes
            if ($declined) {
                //récupération des données à injecter dans la table attributs :
                $tab_attr = array();
                foreach ($catalog->tabInsert_attribute as $key => $val) {
                    $tab_attr[$key] = isset($tab_prod[$key]) ? $tab_prod[$key] : '';
                }
                // correction of references
                $tab_attr['reference_attribute'] = $tab_prod['reference'];
                $tab_attr['product_reference'] = $tab_prod['sku_parent'];
                $tab_prod['reference'] = $tab_prod['sku_parent'];
                $tab_prod['product_reference'] = $tab_prod['sku_parent'];
                $tab_prod['ean13'] = '';
                // correction of prices and weight
                if ($parent) {
                    $tab_prod['price'] = 0;
                    $tab_prod['pmvc'] = 0;
                    $tab_prod['weight'] = 0;
                }
                // building attribute table
                $attribute = array();
                foreach ($tab_prod_lang as $iso => $prod_lang) {
                    $taille = preg_replace('/\W/', ' ', $prod_lang[$this->tab_catalogue['taille']]);
                    $attribute[$this->tab_catalogue['taille']][$iso][$this->tab_catalogue['taille']] = $taille;
                    array_push($catalog->tabAttributes_value, Language::getIdByIso($iso) . '|' . $this->tab_catalogue['taille'] . '|' . $taille);
                }
                $tab_attr['attribute'] = Tools::jsonEncode($attribute);
                array_push($catalog->tabAttributes, $langId . '|' . $this->tab_catalogue['taille']);
                // insertion dans la BDD
                $tab_insert = array();
                foreach ($catalog->tabInsert_attribute as $key => $val) {
                    $tab_insert[$key] = isset($tab_attr[$key]) ? pSQL($tab_attr[$key]) : '';
                }
                $tab_insert['fournisseur'] = pSQL($this->fournisseur_glob);

                $insert_combinations[] = $tab_insert;

                // if parent exists, next line
                if (!$parent) {
                    continue;
                }
            }

            //Category
            $cat = array();
            $cat_dflt = array();
            foreach ($this->tab_categorie as $tag) {
                $cat_level = array();
                foreach ($tab_prod_lang as $iso => $prod_lang) {
                    $cat_level[$iso] = empty($prod_lang[$tag]) ? 'default' : $prod_lang[$tag];
                }
                if ((!count($cat)) || (count($cat) && $cat[count($cat) - 1] !== $cat_level)) {
                    $cat[] = $cat_level;
                    $cat_dflt[] = $cat_level[$langIso];
                }
            }
            $tab_prod['category'] = $langTag . implode('::>>::', $cat_dflt);
            $tab_prod['category_group'] = Tools::jsonEncode(array($cat));

            // name
            $names = array();
            $descs = array();
            foreach ($tab_prod_lang as $iso => $prod_lang) {
                $names[] = $iso . ':://::' . $prod_lang[$this->tab_catalogue['name']];
                $descs[] = $iso . ':://::' . $prod_lang[$this->tab_catalogue['description']];
            }
            $tab_prod['name'] = implode('//:://', $names);

            // description
            if ('l' == $this->config['long_desc']) {
                $tab_prod['description'] = implode('//:://', $descs);
            } else {
                $tab_prod['short_description'] = implode('//:://', $descs);
                $tab_prod['description'] = '';
            }

            // features
            $feat = array();
            foreach ($this->tab_features_cat as $name => $tag) {
                foreach ($tab_prod_lang as $iso => $prod_lang) {
                    $val = trim($prod_lang[$tag]);
                    if ('' !== $val) {
                        $feat[$name][$iso] = array($name => $val);
                        if ($langIso == $iso) {
                            array_push($catalog->tabFeatures, $langId . '|' . $name);
                            array_push($catalog->tabFeatures_value, $langId . '|' . $name . '|' . $val);
                        }
                    }
                }
            }
            $tab_prod['feature'] = Tools::jsonEncode($feat);

            // dimensions
            $dims = array();
            foreach ($this->tab_dims as $field => $tag) {
                if (!empty($item[$tag])) {
                    $dims[$field] = str_replace(',', '.', $item[$tag]);
                }
            }
            $tab_prod['dimensions'] = Tools::jsonEncode($dims);

            // pictures
            $pics = array();
            foreach ($this->tab_pictures as $tag) {
                if (!empty($item[$tag])) {
                    $pics[] = array('url' => $item[$tag]);
                }
            }
            $tab_prod['pictures'] = Tools::jsonEncode($pics);
            
            //mode_livraison by default because International does not have it
            $tab_prod['mode_livraison'] = $tab_prod['mode_livraison'] ?? 1;

            // keep type and promo flag in a json table in the keep field of the catalog table
            $keep = array();
            foreach ($this->tab_keep as $key => $tag) {
                if (!empty($tab_prod[$tag])) {
                    $keep[$key] = $tab_prod[$tag];
                }
            }
            $tab_prod['keep'] = Tools::jsonEncode($keep);

            //carriers
            $carriers = array();
            if ($this->config['use_typed_carriers']) { // attention, problème de shop
                //get list of carriers from the mod_liv field and the property carriers_by_modliv
                $prod_type = $tab_prod['prod_type'];
                $mod_liv = $tab_prod['mode_livraison'] ?? false;
                if ($mod_liv) {
                    $list_carriers_codes = $this->carriers_by_modliv[$thisAbo][$mod_liv][$prod_type] ?? false;
                    if ($list_carriers_codes) {
                        $list_car_names = array();
                        foreach ($list_carriers_codes as $carrier_code) {
                            $list_car_names[] = array_search($carrier_code, $this->carriers[$thisAbo], true);
                        }
                        if ($list_car_names) {
                            $carriers = array(
                                'replace' => 1,
                                'list' => $list_car_names
                            );
                        }
                    }
                }
            }
            $tab_prod['carriers'] = $carriers ? json_encode($carriers) : '';

            // clean fields
            $tab_insert = array();
            foreach ($catalog->tabInsert as $key => $val) {
                $tab_insert[$key] = isset($tab_prod[$key]) ? pSQL($tab_prod[$key], isset($this->html_fields[$key])) : '';
            }
            $tab_insert['fournisseur'] = pSQL($this->fournisseur_glob);

            // insert
            $insert_parents[] = $tab_insert;
        }
        $row = $products[$langIso]->currentLine;

        if ($insert_parents) {
            try {
                Db::getInstance()->insert('eci_catalog', $insert_parents, false, false, Db::INSERT_IGNORE);
            } catch (Exception $e) {
                Catalog::logInfo(
                    $logger,
                    $this->fournisseur_glob . ' erreur mysql ' . $e->getMessage()
                );
            }
        }

        if ($insert_combinations) {
            try {
                Db::getInstance()->insert('eci_catalog_attribute', $insert_combinations, false, false, Db::INSERT_IGNORE);
            } catch (Exception $e) {
                Catalog::logInfo(
                    $logger,
                    $this->fournisseur_glob . ' erreur mysql ' . $e->getMessage()
                );
            }
        }

        $catalog->tabTVA = array_unique($catalog->tabTVA);
        $catalog->matchTax();
        $catalog->tabAttributes = array_unique($catalog->tabAttributes);
        $catalog->tabAttributes_value = array_unique($catalog->tabAttributes_value);
        $catalog->insertAttributes($this->fournisseur_glob);
        $catalog->matchAttributes();
        $catalog->tabFeatures = array_unique($catalog->tabFeatures);
        $catalog->tabFeatures_value = array_unique($catalog->tabFeatures_value);
        $catalog->insertFeatures($this->fournisseur_glob);
        $catalog->matchFeatures();

        return array(
            'row' => $row,
            'rowRef' => $rowRef,
            'tentative' => $tentative,
            'fichierActu' => $fichierActu,
            'nbFichier' => $nbFichier,
            'lignesTot' => $lignesTot,
            'tot' => $tot,
            'act' => $act
        );
    }

    public function calculatePrices($pa, $pv, $tr, $et, $dr = 0, $dd = 0, $pc = 0)
    {
        $marge = isset($this->config['MARGE']) ? $this->config['MARGE'] : 0;
        $notva = isset($this->config['NOTVA']) ? (bool) $this->config['NOTVA'] : false;
        $pa = str_replace(',', '.', $pa);
        $pv = str_replace(',', '.', $pv);
        $tr = str_replace(',', '.', $tr);
        $et = str_replace(',', '.', $et);
        $dr = str_replace(',', '.', $dr);
        $dd = str_replace(',', '.', $dd);
        $pc = str_replace(',', '.', $pc);
        $taxrate = is_numeric($tr) ? $tr : 0;
        $ecotax = is_numeric($et) ? $et : 0;
        $discount = is_numeric($dr) ? $dr : 0;
        $discounted = is_numeric($dd) ? $dd : 0;
        $private_copy = is_numeric($pc) ? $pc : 0; // il me faut une règle pour utiliser ça

        // correction of wholesale price
        if ($discounted > 0) {
            $price_0 = is_numeric($discounted) ? $discounted - $ecotax : 0;
            $price = ($notva ? $price_0 + $price_0 * $taxrate / 100 : $price_0);
        } else {
            $price_0 = is_numeric($pa) ? $pa - $ecotax : 0;
            $price = ($notva ? $price_0 + $price_0 * $taxrate / 100 : $price_0) * (100 - $discount) / 100;
        }
        //see if we can use discount : it would modify the wholesale price but we need it for orders

        // correction of selling price
        $pmvc = $marge != 0 ? $price + $price * $marge / 100 : (is_numeric($pv) && $pv > 0 ? ($notva ? $pv + $pv * $taxrate / 100 : $pv) : $price);

        return array('price' => round($price, 2), 'pmvc' => round($pmvc, 2));
    }

    public function updateStock_old($stopTime, $id_shop, $nbc = 0)
    {
        Shop::setContext(Shop::CONTEXT_SHOP, (int) $id_shop);
        Context::getContext()->employee = new Employee(1);
        Context::getContext()->shop->id = (int) $id_shop;
        $id_fournisseur = Catalog::getEciFournId($this->fournisseur_glob);
        $sw_update_prices = $this->config['PRICEWSTOCK'];
        $sw_update_pmvcs = $this->config['PMVCWSTOCK'];
        $stock_val = empty($this->config['stock_val']) ? 5 : $this->config['stock_val'];
        $pricerulesClass = 'Pricerules' . $this->fournisseur_glob;
        $dir = dirname(__FILE__) . '/../../../files/';

        $logger = Catalog::logStart('stock');

        $stocks = new Bigjson($this->stock_name, $dir, false);
        $prices = new Bigjson($this->stockprice_name, $dir, false);

        if (!$nbc) {
            $stocks->deleteIndex();
            $prices->deleteIndex();
            Catalog::resetStock($this->fournisseur_glob, $id_shop);
            Catalog::resetEciStock($this->fournisseur_glob, $id_shop);
            Catalog::jUpdateValue('ECI_STK_PROGRESSMAX_' . $this->fournisseur_glob, $stocks->maxLine);
            $prices->buildIndex($this->tab_catalogue['product_reference']);
        }

        $stocks->go($nbc);
        while ($item = $stocks->read()) {
            if ((time() > $stopTime) && ($stocks->currentLine > $nbc)) {
                return $stocks->currentLine;
            }

            if (0 === ($stocks->currentLine % 50)) {
                Catalog::jUpdateValue('ECI_STK_PROGRESS_' . $this->fournisseur_glob, $stocks->currentLine);
            }

            $tab_stk = array();
            foreach ($this->tab_stock as $field => $tag) {
                if ($tag && array_key_exists($tag, $item)) {
                    $tab_stk[$field] = trim($item[$tag]);
                }
            }

            if (empty($tab_stk['reference']) || ($tab_stk['reference'] === $this->tab_catalogue['reference'])) {
                continue;
            }

            if (empty($tab_stk['stock']) || '' === $tab_stk['stock']) {
                $tab_stk['stock'] = $stock_val;
            }

            //get product prices from catalog if available
            $id_prices = ($sw_update_prices || $sw_update_pmvcs) ? $prices->searchOne($this->tab_catalogue['product_reference'], $tab_stk['reference'], Bigjson::EXACT) : false;
            if (false !== $id_prices) {
                $prod_infos = $prices->get($id_prices);
                foreach ($this->tab_stock as $field => $tag) {
                    if ($tag && array_key_exists($tag, $prod_infos) && !isset($tab_stk[$field])) {
                        $tab_stk[$field] = trim($prod_infos[$tag]);
                    }
                }
            }

            //set catalog stock if possible
            if ((false !== $id_prices) && empty($prod_infos[$this->tab_catalogue['sku_parent']])) {
                Catalog::setEciStock($tab_stk['reference'], (int)$tab_stk['stock'], null, $this->fournisseur_glob, $id_shop);
            } elseif (false !== $id_prices) {
                Catalog::setEciStock($tab_stk['reference'], (int)$tab_stk['stock'], $prod_infos[$this->tab_catalogue['sku_parent']], $this->fournisseur_glob, $id_shop);
            }

            $ids = new ImporterReference($tab_stk['reference'], $id_fournisseur, $id_shop);
            if (!$ids->id_product) {
                continue;
            }

            $supdated = false;
            Catalog::setStock($ids->id_product, $tab_stk['reference'], $id_shop, $this->fournisseur_glob, $ids->id_product_attribute, (int)$tab_stk['stock']);
            try {
                $stock = StockAvailable::getQuantityAvailableByProduct((int) $ids->id_product, (int) $ids->id_product_attribute, (int) $id_shop);
                if ($stock != (int) $tab_stk['stock']) {
                    StockAvailable::setQuantity((int) $ids->id_product, (int) $ids->id_product_attribute, (int) $tab_stk['stock'], (int) $id_shop, false);
                    $supdated = true;
                }
            } catch (Exception $e) {
                Catalog::logError(
                    $logger,
                    __FUNCTION__ . ' ' . $this->fournisseur_glob . ' erreur MAJ stock pid ' . $ids->id_product . ' ref ' . $tab_stk['reference'] . ' : ' . $e->getMessage()
                );
            }

            $pupdated = $cupdated = false;
            if (($sw_update_prices || $sw_update_pmvcs) && (false !== $id_prices)) {
                try {
                    // calculate product or combination prices (no diff)
                    $tab_price = $this->calculatePrices(
                        $tab_stk['price'],
                        (empty($tab_stk['pmvc']) ? $tab_stk['price'] : $tab_stk['pmvc']),
                        $tab_stk['rate'],
                        $tab_stk['ecotax'],
                        (empty($tab_stk['discount']) ? 0 : $tab_stk['discount']),
                        (empty($tab_stk['discounted']) ? 0 : $tab_stk['discounted'])
                    );
                    $tab_price['pmvc'] = $pricerulesClass::calculatePriceRules(
                        $tab_stk['reference'],
                        $this->fournisseur_glob,
                        $id_shop,
                        (float) $tab_price['price'],
                        (float) $tab_price['pmvc']
                    );
                    if ($ids->id_product_attribute) {
                        $prod = new Product($ids->id_product, false, null, $id_shop);
                        // update prices if necessary
                        if ($sw_update_prices && (0 != $prod->wholesale_price)) {
                            $prod->wholesale_price = 0;
                            $pupdated = true;
                        }
                        if ($sw_update_pmvcs && (0 != $prod->price)) {
                            $prod->price = 0;
                            $pupdated = true;
                        }
                        if ($pupdated) {
                            $prod->update();
                        }
                        unset($prod);
                    }
                    if ($ids->id_product_attribute) {
                        $comb = new Combination($ids->id_product_attribute, null, $id_shop);
                        // update prices if necessary
                        if ($sw_update_prices && ($tab_price['price'] != $comb->wholesale_price)) {
                            $comb->wholesale_price = $tab_price['price'];
                            $cupdated = true;
                        }
                        if ($sw_update_pmvcs && ($tab_price['pmvc'] != $comb->price)) {
                            $comb->price = $tab_price['pmvc'];
                            $cupdated = true;
                        }
                        if ($cupdated) {
                            $comb->update();
                        }
                        unset($comb);
                    } else {
                        $prod = new Product($ids->id_product, false, null, $id_shop);
                        // update prices if necessary
                        if ($sw_update_prices && ($tab_price['price'] != $prod->wholesale_price)) {
                            $prod->wholesale_price = $tab_price['price'];
                            $pupdated = true;
                        }
                        if ($sw_update_pmvcs && ($tab_price['pmvc'] != $prod->price)) {
                            $prod->price = $tab_price['pmvc'];
                            $pupdated = true;
                        }
                        if ($pupdated) {
                            $prod->update();
                        }
                        unset($prod);
                    }
                } catch (Exception $e) {
                    Catalog::logInfo(
                        $logger,
                        __FUNCTION__ . ' ' . $this->fournisseur_glob . ' erreur MAJ prix pid ' . $ids->id_product . ' ref ' . $tab_stk['reference'] . ' : ' . $e->getMessage()
                    );
                }
            }

            if (0 !== ($stocks->currentLine % 50) && ($supdated || $pupdated || $cupdated)) {
                Catalog::jUpdateValue('ECI_STK_PROGRESS_' . $this->fournisseur_glob, $stocks->currentLine);
            }
        }

        return true;
    }

    public function updateStock($stopTime, $id_shop, $nbc = 0)
    {
        $id_fournisseur = Catalog::getEciFournId($this->fournisseur_glob);
        $stock_val = (int) ($this->config['stock_val'] ?? 5);

        $logger = Catalog::logStart('stock');

        $stocks = new Bigjson($this->stock_name, Catalog::FILES_PATH, false);
        $prices = new Bigjson($this->stockprice_name, Catalog::FILES_PATH, false);

        if (!$nbc) {
            $stocks->deleteIndex();
            $prices->deleteIndex();
            Catalog::resetStock($this->fournisseur_glob, $id_shop);
            Catalog::resetEciStock($this->fournisseur_glob, $id_shop);
            Catalog::cleanEciStock($this->fournisseur_glob, $id_shop);
            Catalog::jUpdateValue('ECI_STK_PROGRESSMAX_' . $this->fournisseur_glob, $stocks->maxLine);
            $prices->buildIndex($this->tab_catalogue['product_reference']);
        }

        $stocks->go($nbc);
        while ($item = $stocks->read()) {
            if ((time() > $stopTime) && ($stocks->currentLine > $nbc)) {
                return $stocks->currentLine;
            }

            if (!($stocks->currentLine % 50)) {
                Catalog::jUpdateValue('ECI_STK_PROGRESS_' . $this->fournisseur_glob, $stocks->currentLine);
            }

            $tab_stk = array();
            foreach ($this->tab_stock as $field => $tag) {
                if ($tag && array_key_exists($tag, $item)) {
                    $tab_stk[$field] = trim($item[$tag]);
                }
            }

            if (empty($tab_stk['reference']) || ($tab_stk['reference'] === $this->tab_catalogue['reference'])) {
                continue;
            }

            if ('' === $tab_stk['stock']) {
                $tab_stk['stock'] = $stock_val;
            } elseif (!is_numeric($tab_stk['stock']) || 0 > $tab_stk['stock']) {
                $tab_stk['stock'] = 0;
            }

            //get product prices from catalog if available
            $id_prices = $prices->searchOne($this->tab_catalogue['product_reference'], $tab_stk['reference'], Bigjson::EXACT);
            if (false === $id_prices) {
                continue;
            }
            $prod_infos = $prices->get($id_prices);
            foreach ($this->tab_stock as $field => $tag) {
                if ($tag && array_key_exists($tag, $prod_infos) && !isset($tab_stk[$field])) {
                    $tab_stk[$field] = trim($prod_infos[$tag]);
                }
            }

            //set catalog stock if possible
            if (empty($prod_infos[$this->tab_catalogue['sku_parent']])) {
                Catalog::setEciStock($tab_stk['reference'], (int)$tab_stk['stock'], null, $this->fournisseur_glob, $id_shop);
            } else {
                Catalog::setEciStock($tab_stk['reference'], (int)$tab_stk['stock'], $prod_infos[$this->tab_catalogue['sku_parent']], $this->fournisseur_glob, $id_shop);
            }

            $ids = new ImporterReference($tab_stk['reference'], $id_fournisseur, $id_shop);
            if (!$ids->id_product) {
                continue;
            }
            
            $tab_price = $this->calculatePrices(
                $tab_stk['price'],
                (empty($tab_stk['pmvc']) ? $tab_stk['price'] : $tab_stk['pmvc']),
                $tab_stk['rate'],
                $tab_stk['ecotax'],
                (empty($tab_stk['discount']) ? 0 : $tab_stk['discount']),
                (empty($tab_stk['discounted']) ? 0 : $tab_stk['discounted'])
            );

            Catalog::setStock($ids->id_product, $tab_stk['reference'], $id_shop, $this->fournisseur_glob, $ids->id_product_attribute, (int)$tab_stk['stock'], null, $tab_price['price'], $tab_price['pmvc']);

            if ($stocks->currentLine % 50) {
                Catalog::jUpdateValue('ECI_STK_PROGRESS_' . $this->fournisseur_glob, $stocks->currentLine);
            }
        }

        return true;
    }

    public function sendOrder($com, $simulation = false)
    {
        require_once dirname(__FILE__) . '/PHPExcel.php';
        require_once dirname(__FILE__) . '/PHPExcel/Writer/Excel2007.php';
        $id_order = $com['id_order'];
        //instanciation de la commande
        $order = new Order((int) $id_order);
        $id_shop = $order->id_shop;
        //$langId = Configuration::get('PS_LANG_DEFAULT');
        //$langIso = Language::getIsoById($langId);
        //$id_supplier = Catalog::getEciFournId($this->fournisseur_glob);
        $logger = Catalog::logStart('order');
        $scClass = 'Suppcarrier'. $this->fournisseur_glob;
        //$suppcarriers = $class::getSuppcarriers($this->fournisseur_glob);
        $thisAbo = $this->getAbo();

        $file = ($simulation?'simulation_':'') . 'commandes_' . Configuration::get('PS_SHOP_NAME') . '_' . date("Ymd_His") . '_' . $id_order . '.xlsx';
        $records = Catalog::EXPORT_PATH . $file;

        $customer = new Customer($order->id_customer);
        $gender = new Gender($customer->id_gender, $customer->id_lang, $order->id_shop);
        $address_delivery = new Address($order->id_address_delivery);
        //$address_invoice = new Address($order->id_address_invoice);
        $country_delivery = new Country($address_delivery->id_country);
        $phone = $address_delivery->phone
            ? $address_delivery->phone
            : ($address_delivery->phone_mobile ? $address_delivery->phone_mobile : Configuration::get('PS_SHOP_PHONE'));
        //$messages = Message::getMessagesByOrderId($id_order);
        //$carrier = new Carrier($order->id_carrier);
        $details = $order->getOrderDetailList();
        // filter details
        $ok_order = true;
        $message = '';
        foreach ($details as $i => $detail) {
            $eci_ps_data = Catalog::getProductShopDataStatic($detail['product_id'], $detail['product_attribute_id'], $this->fournisseur_glob, $id_shop);
            if (!$eci_ps_data) {
                unset($details[$i]);
                continue;
            }
            $keep = isset($eci_ps_data['keep']) ? $eci_ps_data['keep'] : array();
            $details[$i]['keep'] = $keep;
            if (empty($keep['type'])) {
                $message .= 'Le type du produit ' . $detail['product_id'] . ' est indéfini. ';
                $ok_order = false;
            }
        }
        if (!$details) {
            return true;
        }

        if ($ok_order) {
            // mode de livraison
            // PS casse les commandes par transporteur s'il y a plusieurs transporteurs
            // donc ici on voit arriver les commandes comportant x lignes compatibles avec un seul transporteur ECI ou pas
            // CDP prend le mode de livraison par ligne
            // si le transporteur de la commande est ECI on calcule le transporteur à chaque ligne mais s'il est compatible on prend celui de la commande
            // si le transporteur n'est pas ECI, on doit calculer le transporteur à chaque ligne suivant la config : défaut pour la classe d'article
            $suppcarrier = $scClass::getSuppcarrierByIdCarrier($order->id_carrier);
            $mode_liv_order = $suppcarrier['supplier_key'] ?? false;

            $workbook = new PHPExcel();
            $sheet = $workbook->getActiveSheet();

            $sheet->SetCellValue('A1', 'N° de Cmd');
            $sheet->SetCellValue('B1', 'Date de commande');
            $sheet->SetCellValue('C1', 'Référence campagne');
            $sheet->SetCellValue('D1', 'ID Cdiscount');
            $sheet->SetCellValue('E1', 'Civilité');
            $sheet->SetCellValue('F1', 'Nom Bénéficiaire');
            $sheet->SetCellValue('G1', 'Prénom Bénéficiaire');
            $sheet->SetCellValue('H1', 'Adresse');
            $sheet->SetCellValue('I1', 'Complément d\'adresse');
            $sheet->SetCellValue('J1', 'BP / Lieu-dit');
            $sheet->SetCellValue('K1', 'Raison sociale');
            $sheet->SetCellValue('L1', 'Code postal');
            $sheet->SetCellValue('M1', 'Ville');
            $sheet->SetCellValue('N1', 'Pays');
            $sheet->SetCellValue('O1', 'Téléphone fixe');
            $sheet->SetCellValue('P1', 'Téléphone portable');
            $sheet->SetCellValue('Q1', 'Email client');
            $sheet->SetCellValue('R1', 'Mode livraison');
            $sheet->SetCellValue('S1', 'Frais de port');
            $sheet->SetCellValue('T1', 'Code produit');
            $sheet->SetCellValue('U1', 'Prix HT de vente');
            $sheet->SetCellValue('V1', 'Quantité');
    //        $sheet->SetCellValue('W1', 'ID point retrait');
    //        $sheet->SetCellValue('X1', 'Type de point retrait');

            //infos répétables
            $nCmd = Tools::substr($id_order, 0, 20);
            $dateCommande = date('Ymd', strtotime($order->date_add));
            $refCamp = 'CDiscount' . date("Y");
            $idCDiscount = $this->config['id_cdiscountpro'];
            $civilite = Tools::strtoupper($gender->name) ?: 'M';
            $nomBeneficiaire = Tools::substr($address_delivery->lastname, 0, 35);
            $prenomBeneficiaire = Tools::substr($address_delivery->firstname, 0, 35);
            $adresse = Tools::substr($address_delivery->address1, 0, 35);
            $complementAdresse = Tools::substr($address_delivery->address2, 0, 35);
            $bp = '';
            $raisonSociale = $address_delivery->company;
            $cp = $address_delivery->postcode;
            $ville = $address_delivery->city;
            $pays = $country_delivery->iso_code ?: 'FR';
            $tel_fixe = $this->nettTel($phone);
            $tel_portable = $tel_fixe;
            $emailClient = $customer->email ?: Configuration::get('PS_SHOP_EMAIL');
    //        $id_retrait = '';
    //        $type_point_retrait = '';

            $nhl = 2;
            foreach ($details as $detail) {
                //Génération de la ligne
                $code_produit = !empty($detail['product_supplier_reference']) ? $detail['product_supplier_reference'] : $detail['product_reference'];
                $prix_ht = $detail['original_wholesale_price']; // purchase_supplier_price is the supplier price
                $quantite = $detail['product_quantity'];

                // livraison
                $fraisPort = 0; //obsolete
                $modeLiv = '';
                if ($mode_liv_order) {
                    if (!empty($detail['keep']['type'])) {
                        if (!empty($detail['keep']['liv'])) {
                            if (!empty($this->carriers_by_modliv[$thisAbo][$detail['keep']['liv']][$detail['keep']['type']]) && in_array($mode_liv_order, $this->carriers_by_modliv[$thisAbo][$detail['keep']['liv']][$detail['keep']['type']])) {
                                $modeLiv = $mode_liv_order;
                            } else {
                                $modeLiv = $this->config['dflt_carrier_' . $detail['keep']['type']] ?? $this->dflt_carrier[$thisAbo][$detail['keep']['type']];
                            }
                        } else {
                            if (isset($this->carriers_type[$mode_liv_order]) && in_array($detail['keep']['type'], $this->carriers_type[$mode_liv_order])) {
                                $modeLiv = $mode_liv_order;
                            } else {
                                $modeLiv = $this->config['dflt_carrier_' . $detail['keep']['type']] ?? $this->dflt_carrier[$thisAbo][$detail['keep']['type']];
                            }
                        }
                    }
                } else {
                    if (!empty($detail['keep']['type'])) {
                        $modeLiv = $this->config['dflt_carrier_' . $detail['keep']['type']] ?? $this->dflt_carrier[$thisAbo][$detail['keep']['type']];
                    }
                }
                if (!$modeLiv) {
                    $message .= 'Erreur imprévue trouvée pour le produit ' . $detail['product_id'] . ' : ' . json_encode($detail);
                    $ok_order = false;
                    break;
                }

                $sheet->SetCellValue('A' . $nhl, $nCmd);
                $sheet->SetCellValue('B' . $nhl, $dateCommande);
                $sheet->SetCellValue('C' . $nhl, $refCamp);
                $sheet->SetCellValue('D' . $nhl, $idCDiscount);
                $sheet->SetCellValue('E' . $nhl, $civilite);
                $sheet->SetCellValue('F' . $nhl, $nomBeneficiaire);
                $sheet->SetCellValue('G' . $nhl, $prenomBeneficiaire);
                $sheet->SetCellValue('H' . $nhl, $adresse);
                $sheet->SetCellValue('I' . $nhl, $complementAdresse);
                $sheet->SetCellValue('J' . $nhl, $bp);
                $sheet->SetCellValue('K' . $nhl, $raisonSociale);
                $sheet->setCellValueExplicit('L' . $nhl, str_pad($cp, 5, "0", STR_PAD_LEFT), PHPExcel_Cell_DataType :: TYPE_STRING);
                $sheet->getStyle('L' . $nhl)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
                $sheet->SetCellValue('M' . $nhl, $ville);
                $sheet->SetCellValue('N' . $nhl, $pays);
                $sheet->setCellValueExplicit('O' . $nhl, $tel_fixe, PHPExcel_Cell_DataType :: TYPE_STRING);
                $sheet->getStyle('O' . $nhl)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
                $sheet->setCellValueExplicit('P' . $nhl, $tel_portable, PHPExcel_Cell_DataType :: TYPE_STRING);
                $sheet->getStyle('P' . $nhl)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
                $sheet->SetCellValue('Q' . $nhl, $emailClient);
                $sheet->SetCellValue('R' . $nhl, $modeLiv);
                $sheet->SetCellValue('S' . $nhl, $fraisPort);
                $sheet->setCellValueExplicit('T' . $nhl, $code_produit, PHPExcel_Cell_DataType :: TYPE_STRING);
                $sheet->SetCellValue('U' . $nhl, round($prix_ht, 2));
                $sheet->SetCellValue('V' . $nhl, $quantite);
    //            $sheet->SetCellValue('W' . $nhl, $id_retrait);
    //            $sheet->SetCellValue('X' . $nhl, $type_point_retrait);
                $nhl++;
            }

            $sheet->setTitle('Dropshipment');
            $writer = new PHPExcel_Writer_Excel2007($workbook);
            $writer->save($records);
        }
        
        if ($simulation) {
            return [
                'ok_order' => $ok_order,
                'message' => $message,
                'file' => $file,
            ];
        }
        
        if ($ok_order && $this->config['sendorderbymail'] && !empty($this->config['orderemail'])) {
            //send by mail
            $recipients = explode(';', $this->config['orderemail']);
            foreach ($recipients as $recipient) {
                $target = trim($recipient);
                if (!Validate::isEmail($target)) {
                    continue;
                }
                Mail::Send(
                    (int) Configuration::get('PS_LANG_DEFAULT'),                // langue
                    'commande',                                                 // nom du fichier template SANS L'EXTENSION
                    Mail::l(
                        'Commande '.$this->fournisseur_glob,                    // sujet à traduire dans les langues du module
                        (int) Configuration::get('PS_LANG_DEFAULT')
                    ) . ' ' . $id_order,
                    array(                                                      // templatevars personnelles
                        '{refCmd}' => $id_order,
                    ),
                    $target,    // destinataire mail
                    null,                                                       // destinataire nom
                    Configuration::get('PS_SHOP_EMAIL'),                        // expéditeur
                    Configuration::get('PS_SHOP_NAME'),                         // expéditeur nom
                    array(                                                      // fichier joint
                        'content' => Tools::file_get_contents($records),
                        'mime' => 'application/octet-stream',
                        'name' => $file
                    ),
                    null,                                                       // Choix SMTP, non traité par le coeur < PS 1.4.6.1
                    Catalog::MAILS_PATH                                         // répertoire des mails templates
                );
            }
        }

        $eciClass = 'Eci' . $this->fournisseur_glob;
        if ($eciClass::DEMO) {
            return true;
        }

        //Envoyer le fichier
        if ($ok_order) {
            try {
                if ($this->transLogin(true)) {
                    if ($this->transSendFile($records, 'Commandes/')) {
                        $this->transLogoff();
                    } else {
                        $message = 'La commande ' . $id_order . ' n\'a pas pu être transmise : transfert impossible. ';
                        $ok_order = false;
                    }

                } else {
                    $message = 'La commande ' . $id_order . ' n\'a pas pu être transmise : connexion impossible. ';
                    $ok_order = false;
                }
            } catch (Exception $e) {
                $message = 'La commande ' . $id_order . ' n\'a pas pu être transmise : ' . $e->getMessage();
                $ok_order = false;
            }
        }

        //état + message + tracking
        if ($ok_order) {
            if (!empty($this->config['id_trans'])) {
                Catalog::setOrderState($id_order, $this->config['id_trans'], 'Commande transmise à ' . $this->fournisseur_glob);
            } else {
                Catalog::setOrderMessage($id_order, 'Commande transmise à ' . $this->fournisseur_glob, 1, 'closed');
            }
            Catalog::setTracking(
                $id_order,
                $this->config['id_trans'] ?? 0,
                'Transmis',
                $mode_liv_order ?: '',
                '',
                date('Y-m-d H:i:s'),
                '',
                '',
                '',
                $this->fournisseur_glob
            );
        } else {
            if (!empty($this->config['id_notrans'])) {
                Catalog::setOrderState($id_order, $this->config['id_notrans'], 'Commande NON transmise à ' . $this->fournisseur_glob . ' : ' . $message);
            } else {
                Catalog::setOrderMessage($id_order, 'Commande NON transmise à ' . $this->fournisseur_glob . ' : ' . $message, 1, 'closed');
            }
        }

        return $ok_order;
    }

    public static function nettTel($tel)
    {
        return '0' . Tools::substr(preg_replace('/[^0-9]/', '', $tel), -9);
    }

    public function updateTracking($nbC = 0, $stopTime = null)
    {
        /**
         * à réécrire
         * le fichier est mis à jour mais les lignes restent 20 jours
         * vérifier que l'état est bien nouveau et si oui, le rajouter
         */

        require_once dirname(__FILE__) . '/PHPExcel.php';
        require_once dirname(__FILE__) . '/PHPExcel/Writer/Excel2007.php';
        $logger = Catalog::logStart('tracking');
//        $id_lang = Configuration::get('PS_LANG_DEFAULT');

        $file_name_glob = 'SuiviCommandes.xls';
        $url = 'Suivi/' . $file_name_glob;
        $fl = Catalog::FILES_PATH . $file_name_glob;
        $fl2 = Catalog::FILES_PATH . 'SuiviCommandes.csv';
//        $response = '';
        try {
            if ($this->transLogin(true)) {
                if ($this->transGetFile($fl, $url)) {
                    $this->transLogoff();
                } else {
                    return 'Impossible de récupérer le fichier suivi';
                }
            } else {
                return 'Impossible de se connecter';
            }
            $fileType = PHPExcel_IOFactory::identify($fl);
            $objReader = PHPExcel_IOFactory::createReader($fileType);
            $objReader->setReadDataOnly(true);
            $objPHPExcel = $objReader->load($fl);
            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'CSV');
            $objWriter->save($fl2);

            $handle = fopen($fl2, 'r');
            if (false === $handle) {
                return 'Lecture fichier tampon impossible';
            }

            $kol = 0;
            while (($data = fgetcsv($handle, 10000, ',')) !== false) {
                $kol++;
                if (1 === $kol) {
                    continue;
                }
                if (count($data) < count($this->tab_track)) {
                    continue;
                }
                if (count($data) > count($this->tab_track)) {
                    $data = array_slice($data, 0, count($this->tab_track));
                }
                $infos = array_combine($this->tab_track, $data);
                $order = new Order((int) $infos['refPS']);
                if (!Validate::isLoadedObject($order) || $order->id != $infos['refPS']) {
                    Catalog::logWarning($logger, 'Commande inconnue '.$infos['refPS']);
                    continue;
                }
                $state_code = ($infos['stCom'] ?: 'X') . ($infos['stBP'] ?: 'X');
                if (isset($this->cdp_states[$state_code])) {
                    $state = $this->cdp_states[$state_code];
                } else {
                    Catalog::logError($logger, 'Etat "'.$state_code.'" inconnu dans le fichier pour la commande '.$infos['refPS']);
                    continue;
                }
                $res_track = Db::getInstance()->getValue(
                    'SELECT id_order
                    FROM ' . _DB_PREFIX_ . 'eci_tracking
                    WHERE id_order = ' . (int) $infos['refPS'] . '
                    AND fournisseur = "' . pSQL($this->fournisseur_glob) . '"
                    AND order_state_name = "' . pSQL($state) . '"
                    AND numero = "' . pSQL($infos['numero']) . '"'
                );
                if ($res_track) {
                    continue;
                }
                $id_order_state = $this->config[$this->ost_cdp_to_ps[$state_code]] ?? 0;
                Catalog::setTracking(
                    (int)$infos['refPS'],
                    $id_order_state,
                    $state,
                    '',//transport à déterminer ?
                    $infos['numero'],
                    date('Y-m-d H:i:s'),
                    $infos['dtExp'],
                    '',//pdts infos
                    $infos['trkLink'],
                    $this->fournisseur_glob
                );
                if ($id_order_state) {
                    Catalog::setOrderState((int)$infos['refPS'], $id_order_state);
                }
                if ($infos['numero']) {
                    Catalog::setTrackingNumber($order, $infos['numero'], $logger);
                }
            }
            fclose($handle);
        } catch (Exception $e) {
            return 'Error: ' . $e->getMessage();
        }

        return true;
    }

    public function setCarriers()
    {
        $class = 'Suppcarrier'. $this->fournisseur_glob;
        $suppcarriers = $class::getSuppcarriers($this->fournisseur_glob);
        $thisAbo = $this->getAbo();

        //create if new
        foreach ($this->carriers[$thisAbo] as $name => $key) {
            $eci_carrier = $class::getSuppcarrierBySupplierKey($this->fournisseur_glob, $key);
            $ps_carrier = null;
            if ($eci_carrier) {
                $ps_carrier = new Carrier($eci_carrier['id_carrier']);
                if ($ps_carrier->deleted) {
                    $Suppcarrier = new $class($eci_carrier['id_suppcarrier']);
                    $Suppcarrier->delete();
                }
            }
            if (!$eci_carrier || (!is_null($ps_carrier) && $ps_carrier->deleted)) {
               Catalog::registerCarrier(
                    array(
                        'name' => $name,
                        'delay' => Catalog::createMultiLangField('quelques jours'),
                        'supplier_key' => $key,
                        'use_real' => 0,
                    ),
                    $this->fournisseur_glob
                );
            }
        }

        //delete if no more in list
        foreach ($suppcarriers as $suppcarrier) {
            if (!in_array($suppcarrier['supplier_key'], $this->carriers[$thisAbo])) {
                $PScarrier = new Carrier($suppcarrier['id_carrier']);
                $PScarrier->delete();
                $Suppcarrier = new $class($suppcarrier['id_suppcarrier']);
                $Suppcarrier->delete();
            }
        }

        return true;
    }

    public function getPackageShippingCost($cart, $shipping_cost, $products, $suppcarrier, $t_cache_key = null)
    {
        $logger = Catalog::logStart(__FUNCTION__);
        $id_supplier = Catalog::getEciFournId($this->fournisseur_glob);
        $thisAbo = $this->getAbo();

        $head_cache_key = is_null($t_cache_key) ? implode('_', array($this->fournisseur_glob, $cart->id, $cart->id_currency, $cart->id_address_delivery)) . '_' : implode('_', $t_cache_key['header']) . '_';
        $tail = '';
        $list_products = array();
        foreach ($products as $product) {
            if ($product['id_supplier'] == $id_supplier) {
                $list_products[] = array(
                    'reference' => $product['supplier_reference'],
                    'quantity' => $product['cart_quantity']
                );
            }
            $tail .= '_' . $product['id_product']
                . '_' . $product['id_product_attribute']
                . '_' . $product['cart_quantity'];
        }
        $tail_cache_key = is_null($t_cache_key) ? $tail : '_' . implode('_', $t_cache_key['products']);
//        $req_key = $head_cache_key . $suppcarrier['supplier_key'] . $tail_cache_key;

        //no products found
        if (!count($list_products)) {
            $suppcarrierClass = 'Suppcarrier' . $this->fournisseur_glob;
            $suppcarriers = $suppcarrierClass::getSuppcarriers($this->fournisseur_glob);
            $cache = array();
            foreach ($suppcarriers as $suppcarrier) {
                $supplier_key = $suppcarrier['supplier_key'];
                $cache_key = $head_cache_key . $supplier_key . $tail_cache_key;
                $cache[$cache_key] = false;
            }
            Catalog::cacheStoreMore($cache);

            return false;
        }

        //carrier not choosen
        if (!empty($this->config['carriers_list']) && is_array($this->config['carriers_list']) && !in_array($suppcarrier['supplier_key'], $this->config['carriers_list'])) {
            return false;
        }

        // get class of carrier
        if (!isset($this->carriers_type[$suppcarrier['supplier_key']])) {
            //configuration error
            return false;
        }

        //inactive
        if (!$suppcarrier['active']) {
            return false;
        }

        //free
        $carrier = new Carrier($suppcarrier['id_carrier']);
        if ($carrier->is_free) {
            return 0;
        }

        //Franco
        $franco = 0;
        $franco_default_currency = Configuration::get('PS_SHIPPING_FREE_PRICE');
        if (!empty($franco_default_currency)) {
            $franco = Tools::convertPrice((float) $franco_default_currency, $cart->id_currency);
        }
        $orderTotalwithDiscounts = $cart->getOrderTotal(true, Cart::BOTH_WITHOUT_SHIPPING, null, null, false);
        if ($franco && ($orderTotalwithDiscounts >= $franco)) {
            return 0;
        }
        $franco_wt = (float) Configuration::get('PS_SHIPPING_FREE_WEIGHT');
        if (empty($franco_wt)) {
            $franco_wt = 0;
        }
        $orderTotalWeight = $cart->getTotalWeight();
        if ($franco_wt && ($orderTotalWeight >= $franco_wt)) {
            return 0;
        }

        // get prices for first and nth product
        $first = is_numeric($this->config['ship_cost_first'][$suppcarrier['supplier_key']] ?? '') ? $this->config['ship_cost_first'][$suppcarrier['supplier_key']] : false;
        $nth = is_numeric($this->config['ship_cost_plus'][$suppcarrier['supplier_key']] ?? '') ? $this->config['ship_cost_plus'][$suppcarrier['supplier_key']] : false;
        if (false === $first || false === $nth) {
            //configuration error
            return false;
        }

        // get list of products this carrier can carry
        // and calculate price if not use_real
        $cost = 0;
        $nprods = 0;
        $tprods = 0;
        foreach ($products as &$product) {
            $eci_ps_data = Catalog::getProductShopDataStatic($product['id_product'], $product['id_product_attribute'], $this->fournisseur_glob, $cart->id_shop);
            if (!$eci_ps_data) {
                continue;
            }

            $ptype = $eci_ps_data['keep']['type'] ?? false;
            $pmodliv = $eci_ps_data['keep']['liv'] ?? false;
            if (!$ptype) {
                continue;
            }
            if (!$pmodliv) {
                if (!in_array($ptype, $this->carriers_type[$suppcarrier['supplier_key']])) {
                    continue;
                }
            } else {
                $prod_carriers_list = $this->carriers_by_modliv[$thisAbo][$pmodliv][$ptype] ?? false;
                if (!$prod_carriers_list || !in_array($suppcarrier['supplier_key'], $prod_carriers_list)) {
                    continue;
                }
            }
            $tprods++;

            if ($suppcarrier['use_real']) {
                continue;
            }
            $product_quantity_to_send = $product['cart_quantity'];
            while (0 < $product_quantity_to_send) {
                $nprods++;
                $cost += (1 == $nprods ? $first : $nth);
                $product_quantity_to_send--;
            }
        }
        if (!$tprods) {
            return false;
        }
        $base_price = $suppcarrier['use_real'] ? $shipping_cost : $cost;

        $increment = 0;
        if ($suppcarrier['use_increment']) {
            if ('amount' === $suppcarrier['increment_type']) {
                $increment = $suppcarrier['increment_value'];
            } else {
                $increment = ($suppcarrier['increment_value'] / 100) * $base_price;
            }
        }

        $shippingCost = $base_price + $increment;

        return Tools::convertPrice($shippingCost, $cart->id_currency);
    }

    public function getAdditionnalCrons()
    {
        //return;
        $class = 'eci' . $this->fournisseur_glob;
        $mod = new $class();
        return array(
            $mod->l('Treat order errors', 'ec' . $this->fournisseur_glob . 'mom') => array(
                'link' => 'gen/' . $this->fournisseur_glob . '/' . $this->fournisseur_glob . '_error.php',
                'prefix' => 'none',
                'suffix' => $this->fournisseur_glob,
            ),
//            $mod->l('Update delivery modes', 'ec' . $this->fournisseur_glob . 'mom') => array(
//                'link' => 'gen/' . $this->fournisseur_glob . '/' . $this->fournisseur_glob . '_carriers.php',
//                'prefix' => 'ECI_CAR_',
//                'suffix' => $this->fournisseur_glob,
//            ),
        );
    }

    public function refreshLoopRule($data)
    {
        return (($data['row'] >= $data['lignesTot']) ? true : $data['row']);
    }
}
