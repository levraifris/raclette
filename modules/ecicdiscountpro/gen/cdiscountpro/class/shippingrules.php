<?php
/**
 * 2007-2017 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to a commercial license from SARL Ether Création
 * Use, copy, modification or distribution of this source file without written
 * license agreement from the SARL Ether Création is strictly forbidden.
 * In order to obtain a license, please contact us: contact@ethercreation.com
 * ...........................................................................
 * INFORMATION SUR LA LICENCE D'UTILISATION
 *
 * L'utilisation de ce fichier source est soumise a une licence commerciale
 * concedee par la societe Ether Création
 * Toute utilisation, reproduction, modification ou distribution du present
 * fichier source sans contrat de licence ecrit de la part de la SARL Ether Création est
 * expressement interdite.
 * Pour obtenir une licence, veuillez contacter la SARL Ether Création a l'adresse: contact@ethercreation.com
 * ...........................................................................
 *
 * @author    Ether Création SARL <contact@ethercreation.com>
 * @copyright 2008-2018 Ether Création SARL
 * @license   Commercial license
 * International Registered Trademark & Property of PrestaShop Ether Création SARL
 */

/**
 * Description
 *
 * shipping rules
 *
 * @author Alec
 */
class Shippingrules
{

    /**
     * @var array shipping rules, the first one being the default
     */
    public $shipping_rules = array(
        'M30' => array(
            'mode' => 'COL',
            'qty' => 0,
            'first' => 9.5,
            'more' => 1
        ),
        'P30' => array(
            'mode' => 'LSP',
            'qty' => 0,
            'first' => 80,
            'more' => 35
        ),
    );

    /**
     * @var string dafault rule, will be initialized by construct
     */
    public $default_type = 'M30';

    /**
     * @var float shipping cost, calculated
     */
    public $shipping_cost = 0;

    /**
     * @var string shipping mode, guessed from rules
     */
    public $shipping_mode = '';

    public function __construct()
    {
        foreach ($this->shipping_rules as $type => $rule) {
            $this->shipping_rules[$type]['qty'] = 0;
        }
        $this->default_type = array_keys($this->shipping_rules)[0];
    }

    /**
     * add a line of the order to the object
     * updates quantity of considered type
     * updates shipping cost
     * updates shipping mode
     *
     * @param string $ref the product reference
     * @param int $qty the order line quantity
     * @return boolean true if params are ok, false otherwise
     */
    public function addProduct($ref, $qty)
    {
        if (empty($ref) || empty($qty) || !is_numeric($qty)) {
            return false;
        }

        if (!($type = $this->getProdType($ref))) {
            $type = $this->default_type;
        }
        $this->shipping_cost = $this->getShippingCost($type, $qty);
        $this->shipping_rules[$type]['qty'] += (int) $qty;
        $this->shipping_mode = $this->shipping_rules[$type]['mode'];

        return true;
    }

    /**
     * calculates shipping cost according to rules
     *
     * @param string $type product type
     * @param int $qty order line quantity
     * @return float shipping cost of the order line
     */
    public function getShippingCost($type, $qty)
    {
        if (isset($this->shipping_rules[$type])) {
            if ($this->shipping_rules[$type]['qty'] == 0) {
                return (float) ($this->shipping_rules[$type]['first'] + (((int) $qty - 1) * $this->shipping_rules[$type]['more']));
            } else {
                return (float) (((int) $qty) * $this->shipping_rules[$type]['more']);
            }
        } else {
            return (float) $this->getShippingCost($this->default_type, $qty);
        }
    }

    /**
     * get product type from the appropriate table
     *
     * @param string $ref the product reference
     * @return string the product type
     */
    public function getProdType($ref)
    {
        $keep = Db::getInstance()->getValue(
            'SELECT `keep`
            FROM `' . _DB_PREFIX_ . 'eci_product_shop`
            WHERE `reference` = "' . pSQL($ref) . '"
            AND fournisseur = "cdiscountpro"'
        );
        if (!$keep) {
            return false;
        }

        if (!is_null($tab_spe = Tools::jsonDecode($keep, true))) {
            if (is_array($tab_spe) && array_key_exists('type', $tab_spe)) {
                return $tab_spe['type'];
            }
        }

        return false;
    }
}
