<?php
/**
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to a commercial license from SARL Ether Creation
 * Use, copy, modification or distribution of this source file without written
 * license agreement from the SARL Ether Creation is strictly forbidden.
 * In order to obtain a license, please contact us: contact@ethercreation.com
 * ...........................................................................
 * INFORMATION SUR LA LICENCE D'UTILISATION
 *
 * L'utilisation de ce fichier source est soumise a une licence commerciale
 * concedee par la societe Ether Creation
 * Toute utilisation, reproduction, modification ou distribution du present
 * fichier source sans contrat de licence ecrit de la part de la SARL Ether Creation est
 * expressement interdite.
 * Pour obtenir une licence, veuillez contacter la SARL Ether Creation a l'adresse: contact@ethercreation.com
 * ...........................................................................
 *
 * @author    Ether Creation SARL <contact@ethercreation.com>
 * @copyright 2008-2021 Ether Creation SARL
 * @license   Commercial license
 * International Registered Trademark & Property of Ether Creation SARL
 */

require_once dirname(__FILE__) . '/../../config/config.inc.php';
require_once dirname(__FILE__) . '/class/catalog.class.php';
use ecicdiscountpro\Catalog;

ignore_user_abort(true);
set_error_handler("exception_error_handler");

$paramHelp = Tools::getValue('help', null);
if (!is_null($paramHelp)) {
    $help = array(
        'ec_token' => array(
            'fr' => 'Token du module. Obligatoire.',
            'en' => 'Module\'s token. Required.'
            ),
        'connecteur' => array(
            'fr' => 'Connecteur à traiter. Obligatoire.',
            'en' => 'Module\'s token. Required.'
            ),
        'kill' => array(
            'fr' => 'Arrête l\'opération en cours. Facultatif.',
            'en' => 'Stops the current update. Optional.'
            ),
    );
    Catalog::answer(Tools::jsonEncode($help));
    exit();
}

$token = Tools::getValue('ec_token');
if ($token != Catalog::getInfoEco('ECO_TOKEN')) {
    header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
    header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
    header('Cache-Control: no-store, no-cache, must-revalidate');
    header('Cache-Control: post-check=0, pre-check=0', false);
    header('Pragma: no-cache');
    Tools::redirect('Location: ../');
    exit();
}

$logger = Catalog::logStart('stock');
$prefix = 'ECI_STK_';

$stopTime = time() + 35;
$listStages = array(
    'started',
    'getfile',
    'updatestock',
    'updatepriceruleprices',
    'updatestockps',
    'updatestockpsprice',
    'updatestockafter',
    'reactivate',
    'stockenable',
);

// paramètres
$paramFourn = Tools::getValue('connecteur');
if (!empty($paramFourn)) {
    $fourn = Db::getInstance()->getRow(
        'SELECT *
        FROM `' . _DB_PREFIX_ . 'eci_fournisseur`
        WHERE `perso`=1
        AND `name`=\'' . pSQL($paramFourn) . '\''
    );
}
if (empty($fourn)) {
    $fourn = array();
    $fourn['name'] = false;
}
$connecteur = $fourn['name'];
// on peut aussi envisager :
//  si pas de connecteur en paramètre -> s'autoappelle avec les mêmes paramètres pour tous les connecteurs actifs

$paramNbCron = Tools::getValue('nbC', null);
$nbCron = !$connecteur ? 0 : (is_null($paramNbCron) ? 0 : (int) $paramNbCron);

$paramJobID = Tools::getValue('jid', null);

$paramAct = Tools::getValue('act', null);
$action = (Catalog::jGet($prefix . 'ACT_' . $connecteur) === 'die') ? 'die' : ((is_null($paramAct)) ? 'go' : $paramAct);

$paramSpy = Tools::getValue('spy', null);
$spy = is_null($paramSpy) ? false : true;

$paramSpy2 = Tools::getValue('spytwo', null);
$spy2 = is_null($paramSpy2) ? false : true;
$who = $spy ? ($spy2 ? 'spy2' : 'spy') : 'normal';

$paramKill = Tools::getValue('kill', null);
$kill = is_null($paramKill) ? false : true;

$paramNoPrices = Tools::getValue('noprices', null);
$noprices = is_null($paramNoPrices) ? '' : '&noprices';

$paramPrg = Tools::getValue('prg', null);
$paramPos = Tools::getValue('pos', null);
if (!is_null($paramPrg)) {
    $prg = (int) $paramPrg;
    $pos = is_null($paramPos) ? 1 : (int) $paramPos;
    $chain = '&prg=' . $prg . '&pos=' . $pos;
} else {
    $prg = false;
    $chain = '';
}

//links
$eci_base_uri = implode('/', explode('\\', (((Configuration::get('PS_SSL_ENABLED') == 1) && (Configuration::get('PS_SSL_ENABLED_EVERYWHERE') == 1)) ? 'https://' : 'http://' ) .
    Tools::getShopDomain() . __PS_BASE_URI__ . str_replace(_PS_ROOT_DIR_ . DIRECTORY_SEPARATOR, '', dirname(__FILE__) . '/')));
$ts = preg_replace('/0\.([0-9]{6}).*? ([0-9]+)/', '$2$1', microtime());
$jid = is_null($paramJobID) ? $ts : $paramJobID;
$params = '?ec_token=' . $token . '&ts=' . $ts . '&jid=' . $jid . $noprices;
$base_uri = $eci_base_uri . basename(__FILE__) . $params . $chain;


/*
Catalog::logInfo(
    $logger,
    'stock '
    . $who . ' entered, parameters '
    . $connecteur . ','
    . $nbCron . ','
    . $action . ','
    . $spy . ','
    . $spy2 . ','
    . $kill . ','
);
*/


// kill
if ($kill) {
    if ($connecteur) {
        if (Catalog::jGet($prefix . 'STATE_' . $connecteur) != 'done') {
            Catalog::jUpdateValue($prefix . 'ACT_' . $connecteur, 'die');
        }
    } else {
        $listFourns = Db::getInstance()->executeS(
            'SELECT `name`
            FROM `' . _DB_PREFIX_ . 'eci_fournisseur`
            WHERE `perso`=1'
        );
        foreach ($listFourns as $fourn) {
            $connecteur = $fourn['name'];
            if (Catalog::jGet($prefix . 'STATE_' . $connecteur) != 'done') {
                Catalog::jUpdateValue($prefix . 'ACT_' . $connecteur, 'die');
            }
        }
    }
    exit('die');
}


// fournisseur invalide
if (!$connecteur) {
    exit('noconnector');
}
if (file_exists(dirname(__FILE__) . '/gen/' . $connecteur . '/class/ec' . $connecteur . '.php')) {
    require_once dirname(__FILE__) . '/gen/' . $connecteur . '/class/ec' . $connecteur . '.php';
} else {
    exit('noclass');
}
$genClass = 'Ec' . $connecteur;
$genStages = Catalog::getClassConstant($genClass, 'STK_STAGES');
if (!is_null($genStages) && $genStages) {
    $listStages_r = is_null(Tools::jsonDecode($genStages, true)) ? $listStages : Tools::jsonDecode($genStages, true);
    $listStages = array_diff($listStages_r, array_filter($listStages_r, 'intval'));
}
if ($listStages[0] !== 'started') {
    array_unshift($listStages, 'started');
}


// espion
if ($spy) {
    Catalog::answer('spy');
    sleep(19);
    $state = Catalog::jGet($prefix . 'STATE_' . $connecteur);
    $progress = Catalog::jGet($prefix . 'PROGRESS_' . $connecteur);
    if ($nbCron == $progress) {
        if ($spy2) {
            if ($state != 'done') {
                Catalog::jUpdateValue($prefix . 'STATE_' . $connecteur, 'still');
            }
        } else {
            Catalog::followLink($base_uri . '&spy=1&spytwo=1&connecteur=' . $connecteur . '&nbC=' . $progress);
        }
    } else {
        Catalog::followLink($base_uri . '&spy=1&connecteur=' . $connecteur . '&nbC=' . $progress);
    }
    exit('bond');
}


// abandon ou initialisation
$etat = Catalog::jGet($prefix . 'STATE_' . $connecteur);
$starting = ((bool) $token) & ((bool) $paramFourn) & is_null($paramSpy) & is_null($paramNbCron) & is_null($paramKill) & is_null($paramAct);
if (!$starting && ($action === 'die')) {
    // abandon demandé par un kill
    Catalog::jUpdateValue($prefix . 'STATE_' . $connecteur, 'done');
    Catalog::jUpdateValue($prefix . 'END_TIME_' . $connecteur, date('Y-m-d H:i:s'));
    Catalog::jUpdateValue($prefix . 'ACT_' . $connecteur, 'go');
    exit('dead');
}
if ($starting && ($etat === 'running')) {
    // tentative de double lancement à éviter
    $progress = Catalog::jGet($prefix . 'PROGRESS_' . $connecteur);
    // envoi d'espion pour déjouer un plantage de serveur pendant une mise à jour
    Catalog::followLink($base_uri . '&spy=1&connecteur=' . $connecteur . '&nbC=' . (int) $progress);
    exit('nodoubleplease');
}
if (!$starting && ($etat === 'still')) {
    // un espion a pensé à tort qu'on était planté mais on est là !
    Catalog::jUpdateValue($prefix . 'STATE_' . $connecteur, 'running');
    Catalog::followLink($base_uri . '&spy=1&connecteur=' . $connecteur . '&nbC=' . $nbCron);
}
if ($starting) {
    // initialisation du process
    $listShops = Shop::getShops(true, null, true);
    sort($listShops);
    Catalog::jUpdateValue($prefix . 'START_TIME_' . $connecteur, date('Y-m-d H:i:s'));
    Catalog::jUpdateValue($prefix . 'END_TIME_' . $connecteur, '');
    Catalog::jUpdateValue($prefix . 'SHOPS_TODO_' . $connecteur, Tools::jsonEncode($listShops));
    Catalog::jUpdateValue($prefix . 'SHOP_' . $connecteur, reset($listShops));
    Catalog::jUpdateValue($prefix . 'STAGE_' . $connecteur, reset($listStages));
    Catalog::jUpdateValue($prefix . 'DOGETFILE_' . $connecteur, 1);
    Catalog::jUpdateValue($prefix . 'PROGRESS_' . $connecteur, 0);
    Catalog::jUpdateValue($prefix . 'PROGRESSMAX_' . $connecteur, 0);
    Catalog::jUpdateValue($prefix . 'LOOPS_' . $connecteur, 0);
    Catalog::jUpdateValue($prefix . 'STATE_' . $connecteur, 'running');
    Catalog::jUpdateValue($prefix . 'ACT_' . $connecteur, 'go');
    Catalog::jUpdateValue($prefix . 'MESSAGE_' . $connecteur, '');
    // lancement de l'espion
    Catalog::followLink($base_uri . '&spy=1&connecteur=' . $connecteur . '&nbC=0');
} else {
    Catalog::jUpdateValue($prefix . 'PROGRESS_' . $connecteur, $nbCron);
}
$listShopsTodo = Tools::jsonDecode(Catalog::jGet($prefix . 'SHOPS_TODO_' . $connecteur), true);
$id_shop = (int) Catalog::jGet($prefix . 'SHOP_' . $connecteur);
$ec_four = new $genClass($id_shop);
$stage = Catalog::jGet($prefix . 'STAGE_' . $connecteur);

Shop::setContext(Shop::CONTEXT_SHOP, (int) $id_shop);
$context = Context::getContext();
$context->shop = new Shop($id_shop, null, $id_shop);
$context->employee = new Employee(Catalog::getEciEmployeeId());
//$catalog = new Catalog($context);

//if shop not first to treat, drop GETFILE
if ($id_shop != Catalog::getFirstActiveShop($connecteur)) {
    $id_getfile = array_search('getfile', $listStages, true);
    if (false !== $id_getfile) {
        unset($listStages[$id_getfile]);
    }
}

// gestion des reprises, ruptures, fin
if ($action === 'next') {
    $action = 'go';
    Catalog::jUpdateValue($prefix . 'ACT_' . $connecteur, 'go');

    $numStage = array_search($stage, $listStages, true);
    $keys = array_keys($listStages);
    $next = $nextKey = false;
    foreach ($keys as $key) {
        if ($next) {
            $nextKey = $key;
            break;
        }
        if ($numStage == $key) {
            $next = true;
        }
    }

    if ($nextKey) {
        $stage = $listStages[$nextKey];
        $nbCron = 0;
    } else {
        $ShopJustDone = array_shift($listShopsTodo);
        if (count($listShopsTodo) > 0) {
            $id_shop = reset($listShopsTodo);
            $stage = reset($listStages);
            $nbCron = 0;
        } else {
            Catalog::jUpdateValue($prefix . 'SHOPS_TODO_' . $connecteur, '');
            Catalog::jUpdateValue($prefix . 'STATE_' . $connecteur, 'done');
            Catalog::jUpdateValue($prefix . 'END_TIME_' . $connecteur, date('Y-m-d H:i:s'));
            if ($prg) {
                //chaining with other task
                $nextCron = Catalog::getNextCron($prg, $pos);
                if ($nextCron) {
                    Catalog::followLink($nextCron['link'] . '&prg=' . $prg . '&pos=' . $nextCron['position']);
                }
            }

            exit('alldone');
        }
    }
    Catalog::jUpdateValue($prefix . 'SHOPS_TODO_' . $connecteur, Tools::jsonEncode($listShopsTodo));
    Catalog::jUpdateValue($prefix . 'SHOP_' . $connecteur, $id_shop);
    Catalog::jUpdateValue($prefix . 'STAGE_' . $connecteur, $stage);
    Catalog::jUpdateValue($prefix . 'LOOPS_' . $connecteur, 0);
    Catalog::jUpdateValue($prefix . 'PROGRESS_' . $connecteur, 0);
    Catalog::jUpdateValue($prefix . 'PROGRESSMAX_' . $connecteur, 0);
}


// aiguillage
$reps = null;
if (Catalog::getECIConfigValue('ACTIVE', $id_shop, $connecteur)) {
    Catalog::answer($stage);
    $can_use_method = in_array($stage, get_class_methods($ec_four));
    if ($can_use_method || function_exists($stage)) {
        try {
            $reps = $can_use_method ? $ec_four->$stage($stopTime, $id_shop, $nbCron) : $stage($prefix, $connecteur, $nbCron, $stopTime, $logger, $id_shop, $ec_four);
        } catch (Exception $e) {
            $reps = 'In "' . $stage . '" : ' . $e->getMessage() . ' in line ' . $e->getLine() . ' of file ' . $e->getFile();
        }
        if ($reps === true) {
            Catalog::jUpdateValue($prefix . 'ACT_' . $connecteur, 'next');
            Catalog::followLink($base_uri . '&connecteur=' . $connecteur . '&nbC=0&act=next');
        } elseif (is_numeric($reps)) {
            Catalog::jUpdateValue($prefix . 'LOOPS_' . $connecteur, Catalog::jGet($prefix . 'LOOPS_' . $connecteur) + 1);
            Catalog::followLink($base_uri . '&connecteur=' . $connecteur . '&nbC=' . $reps);
        }
    } else {
        $reps = 'Phase "' . $stage . '" does not exist';
    }
} else {
    // il faut sauter ce shop
    Catalog::jUpdateValue($prefix . 'ACT_' . $connecteur, 'next');
    Catalog::followLink($base_uri . '&connecteur=' . $connecteur . '&nbC=0&act=next');
}

if ((!is_null($reps)) && (true !== $reps) && (!is_numeric($reps))) {
    Catalog::jUpdateValue($prefix . 'MESSAGE_' . $connecteur, date('Y-m-d H:i:s ') . var_export($reps, true));
    Catalog::logInfo(
        $logger,
        'stock ' . $who . ', ' . $connecteur . ', stage ' . $stage . ', ' . var_export($reps, true)
    );
}

exit('Bye');


function started($prefix, $connecteur, $nbCron, $stopTime, $logger, $id_shop, $ec_four)
{
    // check that the supplier exists
    $id_supplier = Catalog::getEciFournId($connecteur);
    if (!Supplier::supplierExists($id_supplier)) {
        Catalog::logError($logger, 'Le fournisseur ' . $connecteur . ' a été supprimé');
        return 'Le fournisseur ' . $connecteur . ' a été supprimé';
    }
    
    echo 'Task successfully started.';

    return true;
}

function getfile($prefix, $connecteur, $nbCron, $stopTime, $logger, $id_shop, $ec_four)
{
    //get files
    $type = 'stock';
    if (!$nbCron) {
        Catalog::jUpdateValue($prefix . 'DATA_' . $connecteur, '');
        try {
            $retour = $ec_four->getFile($type);
        } catch (Exception $ex) {
            return 'Error in getFile : ' . $ex->getMessage() . ' line ' . $ex->getLine();
        }
    } else {
        $json_param = Catalog::jGet($prefix . 'DATA_' . $connecteur);
        $tab_param = Tools::jsonDecode($json_param, true);
        if (!is_null($tab_param)) {
            try {
                $retour = $ec_four->getFile(
                    $type,
                    isset($tab_param['fichier'])?$tab_param['fichier']:'',
                    isset($tab_param['slice'])?$tab_param['slice']:0,
                    isset($tab_param['extparam'])?$tab_param['extparam']:''
                );
            } catch (Exception $ex) {
                return 'Error in getFile : ' . $ex->getMessage() . ' line ' . $ex->getLine();
            }
        } else {
            try {
                $retour = $ec_four->getFile($type);
            } catch (Exception $ex) {
                return 'Error in getFile : ' . $ex->getMessage() . ' line ' . $ex->getLine();
            }
        }
    }

    if (is_array($retour)) {
        if (!isset($retour['rc'])) {
            return var_export($retour, true);
        } else {
            switch ((int)$retour['rc']) {
                case 1:
                    return true;
                case 2:
                    Catalog::jUpdateValue($prefix . 'DATA_' . $connecteur, Tools::jsonEncode($retour));
                    return (1 + $nbCron);
                default:
                    if (isset($retour['message'])) {
                        return $retour['message'];
                    } else {
                        return var_export($retour, true);
                    }
            }
        }
    } else {
        $tab_ret = explode(',', $retour);
        if (2 > count($tab_ret)) {
            return 'Too few infos in return from getFile for flux "' . $type . '" : ' . $retour;
        }
        switch ((int)$tab_ret[0]) {
            case 1:
                return true;
            case 2:
                $tab_param = array();
                parse_str($tab_ret[1], $tab_param);
                /*Catalog::logInfo($logger, var_export($retour, true));
                Catalog::logInfo($logger, var_export($tab_ret, true));
                Catalog::logInfo($logger, var_export($tab_param, true));*/
                Catalog::jUpdateValue($prefix . 'DATA_' . $connecteur, Tools::jsonEncode($tab_param));
                return (1 + $nbCron);
            case 0:
                return $tab_ret[1];
            default:
                return 'Unknown error in getFile : ' . $retour;
        }
    }
}

/*
 * update eci_stock
 */
function updatestock($prefix, $connecteur, $nbCron, $stopTime, $logger, $id_shop, $ec_four)
{
    //call gen function
    $reps = true;
    if (method_exists($ec_four, 'updateStock')) {
        $reps = $ec_four->updateStock($stopTime, $id_shop, $nbCron);
    }

    return $reps;
}

/*
 * update PS stocks from eci_stock
 */
function updatestockps($prefix, $connecteur, $nbCron, $stopTime, $logger, $id_shop, $ec_four)
{
    //update PS from eci_stock table
    $gen_choose_stock = method_exists($ec_four, 'calculateStock');

    if (!$nbCron) {
        $max = Db::getInstance()->getValue(
            'SELECT COUNT(*)
            FROM (
                SELECT s.*
                FROM ' . _DB_PREFIX_ . 'eci_stock s
                LEFT JOIN ' . _DB_PREFIX_ . 'eci_product_shop p
                ON p.id_product = s.id_product
                AND p.id_product_attribute = s.id_pa
                AND p.id_shop = s.id_shop
                AND p.fournisseur = s.fournisseur
                WHERE s.fournisseur = "' . pSQL($connecteur) . '"
                AND s.id_shop = ' . (int) $id_shop . '
                AND s.upd_flag = 1
                AND p.id_product IS NOT NULL'
                . ($gen_choose_stock ? '' : ' GROUP BY s.id_product, s.id_pa')
            . ') t'
        );
        Catalog::jUpdateValue($prefix . 'PROGRESSMAX_' . $connecteur, $max);
    }

    $prods = Db::getInstance()->executeS(
        'SELECT s.*' . ($gen_choose_stock ? '' : ', SUM(s.qt) as qt') . '
        FROM ' . _DB_PREFIX_ . 'eci_stock s
        LEFT JOIN ' . _DB_PREFIX_ . 'eci_product_shop p
        ON p.id_product = s.id_product
        AND p.id_product_attribute = s.id_pa
        AND p.id_shop = s.id_shop
        AND p.fournisseur = s.fournisseur
        WHERE s.fournisseur = "' . pSQL($connecteur) . '"
        AND s.id_shop = ' . (int) $id_shop . '
        AND s.upd_flag = 1
        AND p.id_product IS NOT NULL'
        . ($gen_choose_stock ? '' : ' GROUP BY s.id_product, s.id_pa') . '
        ORDER BY s.reference
        LIMIT ' . (int) $nbCron . ',500'
    );
    if (!$prods) {
        return true;
    }
    foreach ($prods as $prod) {
        if (($stopTime - time()) < 15) {
            break;
        }
        $nbCron++;

        if (!($nbCron % 50)) {
            Catalog::jUpdateValue($prefix . 'PROGRESS_' . $connecteur, $nbCron);
        }

        if ($gen_choose_stock) {
            $prod = $ec_four->calculateStock($prod);
            if (false === $prod) {
                continue;
            }
        }

        // Stock
        try {
            $quantity = StockAvailable::getQuantityAvailableByProduct((int) $prod['id_product'], (int) $prod['id_pa'], (int) $id_shop);
            if ($quantity != $prod['qt']) {
                StockAvailable::setQuantity((int) $prod['id_product'], (int) $prod['id_pa'], (int) $prod['qt'], (int) $id_shop, false);
            }
        } catch (Exception $e) {
            Catalog::logError($logger, __FUNCTION__ . ' error UPD stock pid ' . $prod['id_product'] . '-' . $prod['id_pa'] . ' ref ' . $prod['reference'] . ' : ' . $e->getMessage());
        }

        if ($nbCron % 50) {
            Catalog::jUpdateValue($prefix . 'PROGRESS_' . $connecteur, $nbCron);
        }
    }

    return $nbCron;
}

/*
 * set combined parent product prices to 0
 */
function parentstozero($prefix, $connecteur, $nbCron, $stopTime, $logger, $id_shop, $ec_four)
{
    
    
    return true;
}

/*
 * update eci_stock prices with pricerules
 */
function updatepriceruleprices($prefix, $connecteur, $nbCron, $stopTime, $logger, $id_shop, $ec_four)
{
    //update eci_stock prices using pricerules
    //avoid if GET says noprices
    $noprices = Tools::getValue('noprices', null);
    if (!is_null($noprices)) {
        sleep(1);
        return true;
    }
    
    $sw_update_pmvcs = $ec_four->config['PMVCWSTOCK'];
    $pricerulesClass = 'Pricerules' . $connecteur;

    if (!$sw_update_pmvcs) {
        return true;
    }

    if (!$nbCron) {
        $max = Db::getInstance()->getValue(
            'SELECT COUNT(*)
            FROM ' . _DB_PREFIX_ . 'eci_stock s
            WHERE s.fournisseur = "' . pSQL($connecteur) . '"
            AND s.id_shop = ' . (int) $id_shop . '
            AND s.upd_flag = 1'
        );
        Catalog::jUpdateValue($prefix . 'PROGRESSMAX_' . $connecteur, $max);
    }

    $prods = Db::getInstance()->executeS(
        'SELECT s.*
        FROM ' . _DB_PREFIX_ . 'eci_stock s
        WHERE s.fournisseur = "' . pSQL($connecteur) . '"
        AND s.id_shop = ' . (int) $id_shop . '
        AND s.upd_flag = 1
        ORDER BY s.reference
        LIMIT ' . (int) $nbCron . ',100'
    );
    if (!$prods) {
        return true;
    }
    
    $jobLine = $nbCron;
    foreach ($prods as $prod) {
        if (($jobLine > $nbCron) && (($stopTime - time()) < 15)) {
            break;
        }
        $jobLine++;

        if (!($jobLine % 50)) {
            Catalog::jUpdateValue($prefix . 'PROGRESS_' . $connecteur, $jobLine);
        }

        $pmvc = $pricerulesClass::calculatePriceRules(
            $prod['reference'],
            $connecteur,
            $id_shop,
            (float) $prod['price'],
            (float) $prod['pmvc']
        );
        
        // update prices if necessary
        if ($pmvc != $prod['pmvc']) {
            Catalog::setStockPrices($prod['id_product'], $prod['price'], $pmvc, $connecteur, $prod['id_pa'], $id_shop, $prod['location']);
        }

        if ($jobLine % 50) {
            Catalog::jUpdateValue($prefix . 'PROGRESS_' . $connecteur, $jobLine);
        }
    }

    return $jobLine;
}

/*
 * update PS prices from eci_stock where prices are different
 */
function updatestockpsprice($prefix, $connecteur, $nbCron, $stopTime, $logger, $id_shop, $ec_four)
{
    //update PS prices from eci_stock
    //avoid if GET says noprices
    $noprices = Tools::getValue('noprices', null);
    if (!is_null($noprices)) {
        sleep(1);
        return true;
    }
    
    //daily prices update after configured hour
    if (Catalog::getECIConfigValue('DAILY_PRICE_UPDATE', $id_shop, $connecteur)) {
        $today = date('z');
        $hour = (int) date('H');
        $last_update = Catalog::getECIConfigValue('last_price_update', $id_shop, $connecteur);
        $update_time = (int) Catalog::getECIConfigValue('DAILY_PRICE_UPDATE_TIME', $id_shop, $connecteur);
        
        if (($today != $last_update) && ($hour >= $update_time)) {
            Catalog::setECIConfigValue('last_price_update', $today, $id_shop, $connecteur);
        } elseif ($today == $last_update) {
            sleep(1);
            return true;
        }
    }
    
    $gen_choose_stockprice = method_exists($ec_four, 'calculateStockPrice');
    $sw_update_prices = (bool)$ec_four->config['PRICEWSTOCK'];
    $sw_update_pmvcs = (bool)$ec_four->config['PMVCWSTOCK'];

    if (!$sw_update_prices && !$sw_update_pmvcs) {
        return true;
    }
    
    if (!$nbCron) {
        Catalog::resetStockPrxFlag($connecteur, $id_shop);
        $max = Db::getInstance()->getValue(
            'SELECT COUNT(*)
            FROM ' . _DB_PREFIX_ . 'product_shop ps
            LEFT JOIN ' . _DB_PREFIX_ . 'product_attribute_shop pas
            ON ps.id_product = pas.id_product AND ps.id_shop = pas.id_shop
            LEFT JOIN ' . _DB_PREFIX_ . 'eci_stock s
            ON ps.id_product = s.id_product AND ps.id_shop = s.id_shop AND IF(pas.id_product_attribute IS NULL, 0, pas.id_product_attribute) = s.id_pa
            WHERE 1
            AND ps.id_shop = ' . (int) $id_shop . '
            AND s.fournisseur = "' . pSQL($connecteur) . '"
            AND (('.(int)$sw_update_pmvcs.' AND ((pas.id_product_attribute IS NULL AND ROUND(ps.price, 6) != ROUND(s.pmvc, 6)) OR (pas.id_product_attribute IS NOT NULL and ROUND(pas.price, 6) != ROUND(s.pmvc, 6))))
                OR ('.(int)$sw_update_prices.' AND ((pas.id_product_attribute IS NULL AND ROUND(ps.wholesale_price, 6) != ROUND(s.price, 6)) OR (pas.id_product_attribute IS NOT NULL and ROUND(pas.wholesale_price, 6) != ROUND(s.price, 6)))))
            AND s.upd_flag = 1
            AND s.prx_flag = 0'
        );
        Catalog::jUpdateValue($prefix . 'PROGRESSMAX_' . $connecteur, $max);
    }

    $prods = Db::getInstance()->executeS(
        'SELECT s.*
        FROM ' . _DB_PREFIX_ . 'product_shop ps
        LEFT JOIN ' . _DB_PREFIX_ . 'product_attribute_shop pas
        ON ps.id_product = pas.id_product AND ps.id_shop = pas.id_shop
        LEFT JOIN ' . _DB_PREFIX_ . 'eci_stock s
        ON ps.id_product = s.id_product AND ps.id_shop = s.id_shop AND IF(pas.id_product_attribute IS NULL, 0, pas.id_product_attribute) = s.id_pa
        WHERE 1
        AND ps.id_shop = ' . (int) $id_shop . '
        AND s.fournisseur = "' . pSQL($connecteur) . '"
        AND (('.(int)$sw_update_pmvcs.' AND ((pas.id_product_attribute IS NULL AND ROUND(ps.price, 6) != ROUND(s.pmvc, 6)) OR (pas.id_product_attribute IS NOT NULL and ROUND(pas.price, 6) != ROUND(s.pmvc, 6))))
            OR ('.(int)$sw_update_prices.' AND ((pas.id_product_attribute IS NULL AND ROUND(ps.wholesale_price, 6) != ROUND(s.price, 6)) OR (pas.id_product_attribute IS NOT NULL and ROUND(pas.wholesale_price, 6) != ROUND(s.price, 6)))))
        AND s.upd_flag = 1
        AND s.prx_flag = 0
        /*ORDER BY s.reference*/
        LIMIT 0,50'
    );
    if (!$prods) {
        return true;
    }
    
    $jobLine = $nbCron;
    foreach ($prods as $prod) {
        if (($jobLine > $nbCron) && ($stopTime - time()) < 15) {
            break;
        }
        $jobLine++;

        if (!($jobLine % 50)) {
            Catalog::jUpdateValue($prefix . 'PROGRESS_' . $connecteur, $jobLine);
        }

        Catalog::setStockPrxFlag($prod['id_product'], $prod['id_pa']?:null, $id_shop, $connecteur, $prod['location']?:null);
        
        if ($gen_choose_stockprice) {
            $prod = $ec_four->calculateStockPrice($prod);
            if (false === $prod) {
                continue;
            }
        }
        
        // Price Product
        if ($sw_update_prices || $sw_update_pmvcs) {
            // if the product is a combination or has combinations : zero in product prices
            if ($prod['id_pa']) {
                try {
                    $product = new Product($prod['id_product'], false, null, $id_shop);
                    $fields_to_update = array();
                    if (Validate::isLoadedObject($product)) {
                        if ($sw_update_prices && ($product->wholesale_price != 0)) {
                            $product->wholesale_price = 0;
                            $fields_to_update['wholesale_price'] = true;
                        }
                        if ($sw_update_pmvcs && ($product->price != 0)) {
                            $product->price = 0;
                            $fields_to_update['price'] = true;
                        }
                        if ($fields_to_update) {
                            $product->setFieldsToUpdate($fields_to_update);
                            $product->update();
                        }
                    }
                } catch (Exception $e) {
                    Catalog::logError(
                        $logger,
                        __FUNCTION__.' error UPD price pid '.$prod['id_product'].'-'.$prod['id_pa'].' ref '.$prod['reference'].' shop '.$id_shop.' : '.$e->getMessage()
                    );
                }
            } elseif (!$prod['id_pa']) {
                try {
                    $product = new Product($prod['id_product'], false, null, $id_shop);
                    $fields_to_update = array();
                    if (Validate::isLoadedObject($product)) {
                        // update prices if necessary
                        if ($sw_update_prices && ($product->wholesale_price != $prod['price'])) {
                            $product->wholesale_price = $prod['price'];
                            $fields_to_update['wholesale_price'] = true;
                        }
                        if ($sw_update_pmvcs && ($product->price != $prod['pmvc'])) {
                            $product->price = $prod['pmvc'];
                            $fields_to_update['price'] = true;
                        }
                        if ($fields_to_update) {
                            $product->setFieldsToUpdate($fields_to_update);
                            $product->update();
                        }
                    }
                } catch (Exception $e) {
                    Catalog::logInfo(
                        $logger,
                        __FUNCTION__ . ' error UPD price pid '.$prod['id_product'].' ref '.$prod['reference'].' shop '.$id_shop.' : '.$e->getMessage()
                    );
                }
            }

            if ($prod['id_pa']) {
                try {
                    $combination = new Combination((int) $prod['id_pa'], null, $id_shop);
                    $fields_to_update = array();
                    if (Validate::isLoadedObject($combination)) {
                        // update prices if necessary
                        if ($sw_update_prices && ($combination->wholesale_price != $prod['price'])) {
                            $combination->wholesale_price = $prod['price'];
                            $fields_to_update['wholesale_price'] = true;
                        }
                        if ($sw_update_pmvcs && ($combination->price != $prod['pmvc'])) {
                            $combination->price = $prod['pmvc'];
                            $fields_to_update['price'] = true;
                        }
                        if ($fields_to_update) {
                            $combination->setFieldsToUpdate($fields_to_update);
                            $combination->update();
                        }
                    }
                } catch (Exception $e) {
                    Catalog::logInfo(
                        $logger,
                        __FUNCTION__ . ' error UPD price pid '.$prod['id_product'].'-'.$prod['id_pa'].' ref '.$prod['reference'].' shop '.$id_shop.' : '.$e->getMessage()
                    );
                }
            }
        }
        if ($jobLine % 50) {
            Catalog::jUpdateValue($prefix . 'PROGRESS_' . $connecteur, $jobLine);
        }
    }

    return $jobLine;
}

/*
 * set null stock in PS if product not updated
 */
function updatestockafter($prefix, $connecteur, $nbCron, $stopTime, $logger, $id_shop, $ec_four)
{
    //see if we share stocks
    $id_shop_group = Shop::getGroupFromShop($id_shop);
    $shop_group = new ShopGroup($id_shop_group);
    if ($shop_group->share_stock) {
        $end_req = ' AND a.id_shop_group = ' . (int) $id_shop_group;
    } else {
        $end_req = ' AND a.id_shop = ' . (int) $id_shop;
    }
    
    //null stock if product not updated
    if (!$nbCron) {
        $max = Db::getInstance()->getValue(
            'SELECT COUNT(*)
            FROM ' . _DB_PREFIX_ . 'eci_product_shop ps
            LEFT JOIN (
                SELECT id_product, sum(upd_flag) AS upd_flag, fournisseur, id_shop
                FROM ' . _DB_PREFIX_ . 'eci_stock
                WHERE fournisseur = "' . pSQL($connecteur) . '"
                GROUP BY id_product, id_shop
            ) s
            ON ps.id_product = s.id_product AND ps.fournisseur = s.fournisseur AND ps.id_shop = s.id_shop
            LEFT JOIN (
                SELECT id_product, id_shop, id_shop_group, SUM(quantity) AS quantity
                FROM ' . _DB_PREFIX_ . 'stock_available
                GROUP BY id_product, id_shop, id_shop_group
            ) a
            ON ps.id_product = a.id_product
            WHERE ps.fournisseur = "' . pSQL($connecteur) . '"
            AND ps.id_product_attribute = 0
            AND ps.id_shop = ' . (int) $id_shop . '
            AND (s.id_product IS NULL OR s.upd_flag = 0)' . $end_req . '
            AND a.quantity > 0'
        );
        Catalog::jUpdateValue($prefix . 'PROGRESSMAX_' . $connecteur, $max);
    }

    $jobLine = $nbCron;
    while (true) {
        if (($stopTime - time()) < 15) {
            return $jobLine;
        }

        $product_to_verify = Db::getInstance()->executeS(
            'SELECT ps.id_product
            FROM ' . _DB_PREFIX_ . 'eci_product_shop ps
            LEFT JOIN (
                SELECT id_product, sum(upd_flag) AS upd_flag, fournisseur, id_shop
                FROM ' . _DB_PREFIX_ . 'eci_stock
                WHERE fournisseur = "' . pSQL($connecteur) . '"
                GROUP BY id_product, id_shop
            ) s
            ON ps.id_product = s.id_product AND ps.fournisseur = s.fournisseur AND ps.id_shop = s.id_shop
            LEFT JOIN (
                SELECT id_product, id_shop, id_shop_group, SUM(quantity) AS quantity
                FROM ' . _DB_PREFIX_ . 'stock_available
                GROUP BY id_product, id_shop, id_shop_group
            ) a
            ON ps.id_product = a.id_product
            WHERE ps.fournisseur = "' . pSQL($connecteur) . '"
            AND ps.id_product_attribute = 0
            AND ps.id_shop = ' . (int) $id_shop . '
            AND (s.id_product IS NULL OR s.upd_flag = 0)' . $end_req . '
            AND a.quantity > 0
            ORDER BY ps.id_product
            LIMIT ' . (int) $jobLine . ',1'
        );

        if (!$product_to_verify) {
            break;
        }

        $jobLine++;

        $id_product = $product_to_verify[0]['id_product'];
        Catalog::setNullStock($id_product, $id_shop);

        Catalog::jUpdateValue($prefix . 'PROGRESS_' . $connecteur, $jobLine);
    }

    return true;
}

/*
 * activate products with stock > 0
 */
function reactivate($prefix, $connecteur, $nbCron, $stopTime, $logger, $id_shop, $ec_four)
{
    if (empty($ec_four->config['REACTIVATE'])) {
        return true;
    }

    if (0 == $nbCron) {
        try {
            $nbLines = Db::getInstance()->getValue(
                'SELECT COUNT(*)
                FROM ' . _DB_PREFIX_ . 'eci_catalog c
                INNER JOIN ' . _DB_PREFIX_ . 'eci_product_shop s
                ON c.product_reference = s.reference
                AND c.fournisseur = s.fournisseur
                WHERE s.fournisseur = "' . pSQL($connecteur) . '"
                AND s.id_product_attribute = 0
                AND s.id_shop = ' . (int) $id_shop
            );
        } catch (Exception $e) {
            return 'stock ' . __FUNCTION__ . ' erreur SQL ' . $e->getMessage();
        }
        Catalog::jUpdateValue($prefix . 'PROGRESSMAX_' . $connecteur, $nbLines);
    }

    $lines_p = Db::getInstance()->executeS(
        'SELECT s.id_product, s.reference
        FROM ' . _DB_PREFIX_ . 'eci_catalog c
        INNER JOIN ' . _DB_PREFIX_ . 'eci_product_shop s
        ON c.product_reference = s.reference
        AND c.fournisseur = s.fournisseur
        WHERE s.fournisseur = "' . pSQL($connecteur) . '"
        AND s.id_product_attribute = 0
        AND s.id_shop = ' . (int) $id_shop . '
        LIMIT ' . (int) $nbCron . ',100'
    );

    if (!$lines_p) {
        Catalog::jUpdateValue($prefix . 'PROGRESSMAX_' . $connecteur, 0);
        return true;
    }

    $jobLine = $nbCron;
    foreach ($lines_p as $prod) {
        if ($stopTime - time() < 15 && $jobLine > $nbCron) {
            break;
        }
        $jobLine++;
        if (productIsPresentInStock((int) $prod['id_product'])) {
            continue;
        }
        $eci_product = Catalog::getProduct($prod['reference']);
        if (isset($eci_product['prestashop']['produit'][$id_shop]['keep']['active']) &&
            0 == $eci_product['prestashop']['produit'][$id_shop]['keep']['active']) {
            continue;
        }
        if (!Catalog::getActStateFromProduct($eci_product, $id_shop)) {
            continue;
        }
        try {
            $qty = StockAvailable::getQuantityAvailableByProduct((int) $prod['id_product'], null, (int) $id_shop);
            if ($qty > 0) {
                $product = new Product((int) $prod['id_product'], false, null, (int) $id_shop, null);
                $fields_to_update = [];
                if (!$product->active) {
                    $product->active = 1;
                    $fields_to_update['active'] = true;
                }
                /*if ($product->visibility != 'both') {
                    $product->visibility = 'both';
                    $fields_to_update['visibility'] = true;
                }*/
                if ($fields_to_update) {
                    $product->setFieldsToUpdate($fields_to_update);
                    $product->update();
                    Catalog::jUpdateValue($prefix . 'PROGRESS_' . $connecteur, $jobLine);
                }
            }
        } catch (Exception $e) {
            Catalog::logInfo(
                $logger,
                'stock ' . __FUNCTION__ . ' erreur PID ' . $prod['id_product'] . ';' . $e->getMessage()
            );
        }
    }

    return $jobLine;
}

/*
 * disable products with null stock
 */
function stockenable($prefix, $connecteur, $nbCron, $stopTime, $logger, $id_shop, $ec_four)
{
    // desactivate products that have zero stock

    if (empty($ec_four->config['STOCK_ENABLE'])) {
        return true;
    }

    if (0 == $nbCron) {
        Catalog::jUpdateValue($prefix . 'PROGRESSMAX_' . $connecteur, Catalog::getNbEciPs($connecteur, $id_shop));
    }

    $lines_ps = Catalog::getListProdInShop($connecteur, $nbCron, 100, false, $id_shop);
    if (!$lines_ps) {
        Catalog::jUpdateValue($prefix . 'PROGRESSMAX_' . $connecteur, 0);
        return true;
    }

    $jobLine = $nbCron;
    foreach ($lines_ps as $prod) {
        if ($stopTime - time() < 15 && $jobLine > $nbCron) {
            break;
        }
        $jobLine++;
        if (productIsPresentInStock((int) $prod['id_product'])) {
            continue;
        }
        try {
            $stock = StockAvailable::getQuantityAvailableByProduct((int) $prod['id_product'], null, (int) $id_shop);
            if ($stock <= 0) {
                $product = new Product((int) $prod['id_product'], false, null, (int) $id_shop, null);
                $fields_to_update = [];
                if ($product->active) {
                    $product->active = 0;
                    $fields_to_update['active'] = true;
                }
                /*if ($product->visibility != 'search') {
                    $product->visibility = 'search';
                    $fields_to_update['visibility'] = true;
                }*/
                if ($fields_to_update) {
                    $product->setFieldsToUpdate($fields_to_update);
                    $product->update();
                    Catalog::jUpdateValue($prefix . 'PROGRESS_' . $connecteur, $jobLine);
                }
            }
        } catch (Exception $e) {
            Catalog::logInfo(
                $logger,
                'stock ' . __FUNCTION__ . ' erreur PID ' . $prod['id_product'] . ' ; ' . $e->getMessage()
            );
        }
    }

    return $jobLine;
}

function productIsPresentInStock($id_product)
{
    $result = Db::getInstance()->executeS(
        'SELECT id_stock FROM ' . _DB_PREFIX_ . 'stock
        WHERE id_product = ' . (int) $id_product
    );

    return ((is_array($result) && !empty($result)) ? true : false);
}

function exception_error_handler($severity, $message, $file, $line)
{
    if (!(error_reporting() & $severity)) {
        // Ce code d'erreur n'est pas inclu dans error_reporting

        return;
    }
    throw new ErrorException($message, 0, $severity, $file, $line);
}
