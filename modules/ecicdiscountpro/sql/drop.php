<?php
/**
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to a commercial license from SARL Ether Creation
 * Use, copy, modification or distribution of this source file without written
 * license agreement from the SARL Ether Creation is strictly forbidden.
 * In order to obtain a license, please contact us: contact@ethercreation.com
 * ...........................................................................
 * INFORMATION SUR LA LICENCE D'UTILISATION
 *
 * L'utilisation de ce fichier source est soumise a une licence commerciale
 * concedee par la societe Ether Creation
 * Toute utilisation, reproduction, modification ou distribution du present
 * fichier source sans contrat de licence ecrit de la part de la SARL Ether Creation est
 * expressement interdite.
 * Pour obtenir une licence, veuillez contacter la SARL Ether Creation a l'adresse: contact@ethercreation.com
 * ...........................................................................
 *
 * @author    Ether Creation SARL <contact@ethercreation.com>
 * @copyright 2008-2021 Ether Creation SARL
 * @license   Commercial license
 * International Registered Trademark & Property of Ether Creation SARL
 */

if (!defined('_PS_VERSION_')) {
    exit();
}

$requests = array(
    'DROP TABLE IF EXISTS `' . _DB_PREFIX_ . 'eci_attribute`;',
    'DROP TABLE IF EXISTS `' . _DB_PREFIX_ . 'eci_attribute_value`;',
    'DROP TABLE IF EXISTS `' . _DB_PREFIX_ . 'eci_attribute_value_shop`;',
    'DROP TABLE IF EXISTS `' . _DB_PREFIX_ . 'eci_attribute_shop`;',
    'DROP TABLE IF EXISTS `' . _DB_PREFIX_ . 'eci_feature`;',
    'DROP TABLE IF EXISTS `' . _DB_PREFIX_ . 'eci_feature_value`;',
    'DROP TABLE IF EXISTS `' . _DB_PREFIX_ . 'eci_feature_shop`;',
    'DROP TABLE IF EXISTS `' . _DB_PREFIX_ . 'eci_feature_value_shop`;',
    'DROP TABLE IF EXISTS `' . _DB_PREFIX_ . 'eci_carrier`;',
    'DROP TABLE IF EXISTS `' . _DB_PREFIX_ . 'eci_catalog`;',
    'DROP TABLE IF EXISTS `' . _DB_PREFIX_ . 'eci_catalog_old`;',
    'DROP TABLE IF EXISTS `' . _DB_PREFIX_ . 'eci_catalog_attribute`;',
    'DROP TABLE IF EXISTS `' . _DB_PREFIX_ . 'eci_catalog_attribute_old`;',
    'DROP TABLE IF EXISTS `' . _DB_PREFIX_ . 'eci_catalog_pack`;',
    'DROP TABLE IF EXISTS `' . _DB_PREFIX_ . 'eci_catalog_stock`;',
    'DROP TABLE IF EXISTS `' . _DB_PREFIX_ . 'eci_category_shop`;',
    'DROP TABLE IF EXISTS `' . _DB_PREFIX_ . 'eci_config`;',
    'DROP TABLE IF EXISTS `' . _DB_PREFIX_ . 'eci_export_com`;',
    'DROP TABLE IF EXISTS `' . _DB_PREFIX_ . 'eci_fournisseur`;',
    'DROP TABLE IF EXISTS `' . _DB_PREFIX_ . 'eci_info`;',
    'DROP TABLE IF EXISTS `' . _DB_PREFIX_ . 'eci_price`;',
    'DROP TABLE IF EXISTS `' . _DB_PREFIX_ . 'eci_product_imported`;',
    'DROP TABLE IF EXISTS `' . _DB_PREFIX_ . 'eci_product_blacklist`;',
    'DROP TABLE IF EXISTS `' . _DB_PREFIX_ . 'eci_product_shop`;',
    'DROP TABLE IF EXISTS `' . _DB_PREFIX_ . 'eci_tax`;',
    'DROP TABLE IF EXISTS `' . _DB_PREFIX_ . 'eci_tax_shop`;',
    'DROP TABLE IF EXISTS `' . _DB_PREFIX_ . 'eci_tracking`;',
    'DROP TABLE IF EXISTS `' . _DB_PREFIX_ . 'eci_stock`;',
    'DROP TABLE IF EXISTS `' . _DB_PREFIX_ . 'eci_jobs`;',
    'DROP TABLE IF EXISTS `' . _DB_PREFIX_ . 'eci_price_spe`;',
    'DROP TABLE IF EXISTS `' . _DB_PREFIX_ . 'eci_op`;',
    'DROP TABLE IF EXISTS `' . _DB_PREFIX_ . 'eci_prg_names`;',
    'DROP TABLE IF EXISTS `' . _DB_PREFIX_ . 'eci_prg_cron`;',
    'DROP TABLE IF EXISTS `' . _DB_PREFIX_ . 'eci_cpanel`;',
    'DROP TABLE IF EXISTS `' . _DB_PREFIX_ . 'eci_cache`;',
    'DROP TABLE IF EXISTS `' . _DB_PREFIX_ . 'eci_customer_hash`;',
    'DROP TABLE IF EXISTS `' . _DB_PREFIX_ . 'eci_address_hash`;',
    'DROP TABLE IF EXISTS `' . _DB_PREFIX_ . 'eci_jobs_history`;',
    'DROP TABLE IF EXISTS `' . _DB_PREFIX_ . 'eci_apicrossid`;',
);
