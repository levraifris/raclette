<?php
/**
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to a commercial license from SARL Ether Creation
 * Use, copy, modification or distribution of this source file without written
 * license agreement from the SARL Ether Creation is strictly forbidden.
 * In order to obtain a license, please contact us: contact@ethercreation.com
 * ...........................................................................
 * INFORMATION SUR LA LICENCE D'UTILISATION
 *
 * L'utilisation de ce fichier source est soumise a une licence commerciale
 * concedee par la societe Ether Creation
 * Toute utilisation, reproduction, modification ou distribution du present
 * fichier source sans contrat de licence ecrit de la part de la SARL Ether Creation est
 * expressement interdite.
 * Pour obtenir une licence, veuillez contacter la SARL Ether Creation a l'adresse: contact@ethercreation.com
 * ...........................................................................
 *
 * @author    Ether Creation SARL <contact@ethercreation.com>
 * @copyright 2008-2021 Ether Creation SARL
 * @license   Commercial license
 * International Registered Trademark & Property of Ether Creation SARL
 */

if (!defined('_PS_VERSION_')) {
    exit();
}

$requests = array(

    'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'eci_attribute` (
        `id_attribute_eco` int(11) NOT NULL AUTO_INCREMENT,
        `value` varchar(128) NOT NULL,
        `split_context` varchar(8) NOT NULL DEFAULT "none",
        `split_value` varchar(255) NOT NULL,
        `id_lang` int(11) NOT NULL,
        `fournisseur` varchar(50) NOT NULL,
        PRIMARY KEY (`id_attribute_eco`),
        UNIQUE KEY `value_split_context_split_value_id_lang_fournisseur` (`value`, `split_context`, `split_value`, `id_lang`, `fournisseur`),
        INDEX `value` (`value`),
        INDEX `id_lang` (`id_lang`),
        INDEX `fournisseur` (`fournisseur`)
    ) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8;',

    'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'eci_attribute_shop` (
        `id_attribute_eco` int(11) NOT NULL,
        `id_attribute` int(11) NOT NULL,
        `id_shop` int(11) NOT NULL,
        PRIMARY KEY( `id_attribute_eco`, `id_shop`),
        INDEX `id_attribute_eco` (`id_attribute_eco`),
        INDEX `id_shop` (`id_shop`)
    ) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8;',

    'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'eci_attribute_value` (
        `id_attribute_eco_value` INT NOT NULL AUTO_INCREMENT ,
        `id_attribute_eco` INT(10) NOT NULL,
        `value` text NOT NULL,
        `id_lang` INT(10) NOT NULL,
        `fournisseur` VARCHAR(50) NOT NULL,
        PRIMARY KEY (`id_attribute_eco_value`),
        UNIQUE KEY `group_value_lang` (`id_attribute_eco`, `value`(255), `id_lang`),
        INDEX `id_attribute_eco` (`id_attribute_eco`),
        INDEX `value` (`value`(255)),
        INDEX `id_lang` (`id_lang`),
        INDEX `fournisseur` (`fournisseur`)
    ) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8;',

    'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'eci_attribute_value_shop` (
        `id_attribute_eco_value` INT NOT NULL,
        `id_attribute` INT NOT NULL,
        `id_shop` INT NOT NULL,
        PRIMARY KEY (`id_attribute_eco_value`,`id_shop`),
        INDEX `id_attribute_eco_value` (`id_attribute_eco_value`),
        INDEX `id_shop` (`id_shop`)
    ) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8;',

    'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'eci_feature` (
        `id_feature_eco` int(11) NOT NULL AUTO_INCREMENT,
        `value` text NOT NULL,
        `id_lang` int(11) NOT NULL,
        `fournisseur` varchar(50) NOT NULL,
        PRIMARY KEY (`id_feature_eco`),
        UNIQUE KEY `value_lang_fournisseur` (`value`(255), `id_lang`, `fournisseur`),
        INDEX `id_lang` (`id_lang`),
        INDEX `value` (`value`(255)),
        INDEX `fournisseur` (`fournisseur`)
    ) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8;',

    'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'eci_feature_shop` (
        `id_feature_eco` int(11) NOT NULL,
        `id_feature` int(11) NOT NULL,
        `id_shop` int(11) NOT NULL,
        PRIMARY KEY (`id_feature_eco`,`id_shop`),
        INDEX `id_feature_eco` (`id_feature_eco`),
        INDEX `id_shop` (`id_shop`)
    ) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8;',

    'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'eci_feature_value` (
        `id_feature_eco_value` INT NOT NULL AUTO_INCREMENT ,
        `id_feature_eco` INT(10) NOT NULL,
        `value` text NOT NULL,
        `id_lang` INT(10) NOT NULL,
        `fournisseur` VARCHAR(50) NOT NULL,
        PRIMARY KEY (`id_feature_eco_value`),
        UNIQUE KEY `group_value_lang` (`id_feature_eco`, `value`(255), `id_lang`),
        INDEX `id_feature_eco` (`id_feature_eco`),
        INDEX `value` (`value`(255)),
        INDEX `id_lang` (`id_lang`),
        INDEX `fournisseur` (`fournisseur`)
    ) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8;',

    'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'eci_feature_value_shop` (
        `id_feature_eco_value` INT NOT NULL,
        `id_feature` INT NOT NULL,
        `id_shop` INT NOT NULL,
        PRIMARY KEY (`id_feature_eco_value`,`id_shop`),
        INDEX `id_feature_eco_value` (`id_feature_eco_value`),
        INDEX `id_shop` (`id_shop`)
    ) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8;',

    'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'eci_catalog` (
        `category` text NOT NULL,
        `category_group` text NOT NULL,
        `name` text NOT NULL,
        `description` text NOT NULL,
        `short_description` text NOT NULL,
        `feature` text NOT NULL,
        `reference` varchar(50) NOT NULL,
        `product_reference` varchar(50) NOT NULL,
        `ean13` varchar(15) NOT NULL,
        `upc` varchar(50) NOT NULL,
        `ecotax` varchar(25) NOT NULL,
        `weight` varchar(25) NOT NULL,
        `dimensions` varchar(100) NOT NULL,
        `ship` varchar(25) NOT NULL,
        `carriers` text NOT NULL,
        `manufacturer` varchar(50) NOT NULL,
        `price` varchar(25) NOT NULL,
        `pmvc` varchar(25) NOT NULL,
        `pictures` text NOT NULL,
        `rate` varchar(25) NOT NULL,
        `documents` text NOT NULL,
        `special` text NOT NULL,
        `keep` text NOT NULL,
        `fournisseur` varchar(50) NOT NULL,
        PRIMARY KEY (`product_reference`,`fournisseur`) USING BTREE,
        INDEX `category` (`category`(255)),
        INDEX `categorygroup` (`category_group`(255)),
        INDEX `product_reference` (`product_reference`),
        INDEX `reference` (`reference`),
        INDEX `manufacturer` (`manufacturer`),
        INDEX `fournisseur` (`fournisseur`)
    ) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8;',

    'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'eci_catalog_old`
    LIKE `' . _DB_PREFIX_ . 'eci_catalog`;',

    'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'eci_catalog_attribute` (
        `reference_attribute` varchar(50) NOT NULL,
        `product_reference` varchar(50) NOT NULL,
        `reference` varchar(50) NOT NULL,
        `price` varchar(25) NOT NULL,
        `pmvc` varchar(25) NOT NULL,
        `ean13` varchar(15) NOT NULL,
        `weight` varchar(25) NOT NULL,
        `ecotax` varchar(25) NOT NULL,
        `attribute` text NOT NULL,
        `upc` varchar(50) NOT NULL,
        `pictures` text NOT NULL,
        `special` text NOT NULL,
        `keep` text NOT NULL,
        `fournisseur` varchar(50) NOT NULL,
        PRIMARY KEY (`fournisseur`,`reference_attribute`),
        INDEX `reference_attribute` (`reference_attribute`),
        INDEX `product_reference` (`product_reference`),
        INDEX `reference` (`reference`),
        INDEX `fournisseur` (`fournisseur`)
    ) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8;',

    'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'eci_catalog_attribute_old`
    LIKE `' . _DB_PREFIX_ . 'eci_catalog_attribute`;',

    'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'eci_catalog_pack` (
        `pack_reference` varchar(50) NOT NULL,
        `product_reference` varchar(50) NOT NULL,
        `reference_attribute` varchar(50) NOT NULL,
        `quantity` int(11) NOT NULL DEFAULT "1",
        `fournisseur` varchar(50) NOT NULL,
        `upd_flag` tinyint(4) NOT NULL DEFAULT 1,
        PRIMARY KEY (`pack_reference`,`product_reference`,`reference_attribute`,`fournisseur`),
        KEY `pack_reference` (`pack_reference`),
        KEY `product_reference` (`product_reference`),
        KEY `reference_attribute` (`reference_attribute`),
        KEY `fournisseur` (`fournisseur`),
        KEY `upd_flag` (`upd_flag`)
    ) ENGINE = ' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8;',

    'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'eci_catalog_stock` (
        `product_reference` varchar(50) NOT NULL,
        `reference_attribute` varchar(50) NOT NULL,
        `id_shop` int(11) NOT NULL DEFAULT 0,
        `stock` int(11) NOT NULL DEFAULT 0,
        `fournisseur` varchar(50) NOT NULL,
        `upd_flag` tinyint(4) NOT NULL DEFAULT 1,
        PRIMARY KEY (`product_reference`,`reference_attribute`,`id_shop`,`fournisseur`),
        INDEX `product_reference` (`product_reference`),
        INDEX `reference_attribute` (`reference_attribute`),
        INDEX `id_shop` (`id_shop`),
        INDEX `fournisseur` (`fournisseur`),
        INDEX `upd_flag` (`upd_flag`)
    ) ENGINE = ' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8;',

    'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'eci_category_shop` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `name` text NOT NULL,
        `fournisseur` varchar(50) NOT NULL,
        `id_category` int(11) NOT NULL DEFAULT 0,
        `id_shop` int(11) NOT NULL,
        `import` int(1) NOT NULL DEFAULT 0,
        `blacklist` int(1) NOT NULL DEFAULT 0,
        `populated` int(1) NOT NULL DEFAULT 0,
        `iso_code` varchar(3) NOT NULL,
        PRIMARY KEY (`id`),
        UNIQUE KEY `name_fournisseur_id_shop` (`name`(255),`fournisseur`,`id_shop`),
        INDEX `name` (`name`(255)),
        INDEX `fournisseur` (`fournisseur`),
        INDEX `id_shop` (`id_shop`),
        INDEX `import` (`import`),
        INDEX `blacklist` (`blacklist`),
        INDEX `populated` (`populated`),
        INDEX `iso_code` (`iso_code`)
    ) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8;',

    'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'eci_config` (
        `id_config` int(11) NOT NULL AUTO_INCREMENT,
        `name` varchar(50) NOT NULL,
        `value` text,
        `id_shop` int(11) NOT NULL DEFAULT 0,
        `fournisseur` varchar(50) NOT NULL DEFAULT "default",
        PRIMARY KEY (`id_config`),
        UNIQUE KEY `name_shop_four` (`name`,`id_shop`,`fournisseur`),
        INDEX `name` (`name`),
        INDEX `id_shop` (`id_shop`),
        INDEX `fournisseur` (`fournisseur`)
    ) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8;',

    'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'eci_export_com` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `id_order` int(11) NOT NULL,
        `fournisseur` varchar(50) NOT NULL,
        `info` text,
        `visible` int DEFAULT 0,
        `final` int DEFAULT 0,
        `extid` varchar(50) NOT NULL,
        `date_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
        PRIMARY KEY (`id`),
        UNIQUE KEY `id_order_fournisseur` (`id_order`, `fournisseur`),
        INDEX `id_order` (`id_order`),
        INDEX `fournisseur` (`fournisseur`),
        INDEX `final` (`final`),
        INDEX `extid` (`extid`)
    ) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8;',

    'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'eci_fournisseur` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `cle` varchar(255) NOT NULL,
        `name` varchar(50) NOT NULL,
        `info` varchar(255) NOT NULL,
        `info2` varchar(255) NOT NULL,
        `info3` varchar(255) NOT NULL,
        `info4` text NOT NULL,
        `nbinfo` int(11) NOT NULL,
        `perso` int(11) NOT NULL,
        `id_manufacturer` int(11) NOT NULL,
        `module_name` varchar(255) NOT NULL,
        `friendly_name` varchar(255) NOT NULL,
        PRIMARY KEY (`id`),
        UNIQUE KEY `name` (`name`),
        INDEX `perso` (`perso`),
        INDEX `id_manufacturer` (`id_manufacturer`)
    ) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8;',

    'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'eci_info` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `name` varchar(50) NOT NULL,
        `value` text NOT NULL,
        PRIMARY KEY (`id`),
        UNIQUE KEY `name` (`name`)
    ) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8;',

    'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'eci_product_imported` (
        `reference` varchar(50) NOT NULL,
        `id_shop` int(11) NOT NULL,
        `imported` tinyint(1) NOT NULL,
        `fournisseur` varchar(50) NOT NULL,
        PRIMARY KEY (`reference`,`fournisseur`,`id_shop`),
        INDEX `reference` (`reference`),
        INDEX `id_shop` (`id_shop`),
        INDEX `imported` (`imported`),
        INDEX `fournisseur` (`fournisseur`)
    ) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8;',

    'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'eci_product_blacklist` (
        `reference` varchar(50) NOT NULL,
        `id_shop` int(11) NOT NULL,
        `blacklist` tinyint(1) NOT NULL,
        `fournisseur` varchar(50) NOT NULL,
        PRIMARY KEY (`reference`,`fournisseur`,`id_shop`),
        INDEX `reference` (`reference`),
        INDEX `id_shop` (`id_shop`),
        INDEX `blacklist` (`blacklist`),
        INDEX `fournisseur` (`fournisseur`)
    ) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8;',

    'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'eci_product_shop` (
        `reference` varchar(50) NOT NULL,
        `id_product` int(11) NOT NULL,
        `id_product_attribute` int(11) NOT NULL,
        `id_shop` int(11) NOT NULL,
        `is_pack` tinyint(1) NOT NULL,
        `imported` tinyint(1) NOT NULL,
        `keep` text NOT NULL,
        `act_reasons` text NOT NULL,
        `deref` tinyint(1) NOT NULL DEFAULT 0,
        `fournisseur` varchar(50) NOT NULL,
        PRIMARY KEY (`reference`,`fournisseur`,`id_shop`),
        UNIQUE `id_product_id_product_attribute_id_shop` (`id_product`, `id_product_attribute`, `id_shop`),
        INDEX `reference` (`reference`),
        INDEX `id_product` (`id_product`),
        INDEX `id_product_attribute` (`id_product_attribute`),
        INDEX `id_shop` (`id_shop`),
        INDEX `is_pack` (`is_pack`),
        INDEX `imported` (`imported`),
        INDEX `deref` (`deref`),
        INDEX `fournisseur` (`fournisseur`)
    ) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8;',

    'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'eci_tax` (
        `id_tax_eco` int(11) NOT NULL AUTO_INCREMENT,
        `rate` varchar(25) NOT NULL,
        `fournisseur` varchar(50) NOT NULL,
        PRIMARY KEY (`id_tax_eco`),
        UNIQUE `rate_fournisseur` (`rate`, `fournisseur`),
        INDEX `rate` (`rate`),
        INDEX `fournisseur` (`fournisseur`)
    ) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8;',

    'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'eci_tax_shop` (
        `id_tax_eco` int(11) NOT NULL,
        `id_tax_rules_group` int(11) NOT NULL,
        `id_shop` int(11) NOT NULL,
        PRIMARY KEY (`id_tax_eco`,`id_shop`),
        INDEX `id_tax_eco` (`id_tax_eco`),
        INDEX `id_shop` (`id_shop`)
    ) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8;',

    'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'eci_tracking` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `id_order` int(11) NOT NULL,
        `fournisseur` varchar(50) NOT NULL,
        `transport` varchar(50) NOT NULL,
        `numero` varchar(50) NOT NULL,
        `date_exp` varchar(50) NOT NULL,
        `url_exp` text NOT NULL,
        `id_order_state` int(11) NOT NULL,
        `order_state_name` text NOT NULL,
        `date_modif_state` datetime NOT NULL,
        `cust_key` text NOT NULL,
        `pdts_infos` text,
        PRIMARY KEY (`id`),
        INDEX `id_order` (`id_order`),
        INDEX `fournisseur` (`fournisseur`)
    ) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8;',

    'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'eci_stock` (
        `id_product` int(11) NOT NULL,
        `id_pa` int(11) NOT NULL DEFAULT 0,
        `reference` varchar(50) NOT NULL,
        `fournisseur` varchar(50) NOT NULL,
        `id_shop` int(11) NOT NULL,
        `qt` varchar(36) NOT NULL,
        `location` varchar(64) NOT NULL,
        `price` varchar(25) DEFAULT 0,
        `pmvc` varchar(25) DEFAULT 0,
        `upd_flag` tinyint(4) NOT NULL DEFAULT 1,
        `prx_flag` tinyint(4) NOT NULL DEFAULT 0,
        PRIMARY KEY (`id_product`,`id_pa`,`id_shop`,`fournisseur`,`location`),
        INDEX `id_product` (`id_product`),
        INDEX `id_pa` (`id_pa`),
        INDEX `reference` (`reference`),
        INDEX `fournisseur` (`fournisseur`),
        INDEX `id_shop` (`id_shop`),
        INDEX `location` (`location`),
        INDEX `upd_flag` (`upd_flag`),
        INDEX `prx_flag` (`prx_flag`)
    ) ENGINE = ' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8;',

    'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'eci_jobs` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `name` varchar(128) NOT NULL,
        `value` varchar(255) NOT NULL,
        PRIMARY KEY (`id`),
        UNIQUE KEY `name` (`name`),
        INDEX `value` (`value`(20))
    ) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8;',

    'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'eci_price_spe` (
        `id` int(25) NOT NULL AUTO_INCREMENT,
        `rule_type` varchar(25) NOT NULL,
        `reference_parent` varchar(50) NOT NULL,
        `reference` varchar(50) NOT NULL,
        `id_customer` int(11) NOT NULL,
        `id_shop` int(11) NOT NULL,
        `id_country` int(11) NOT NULL,
        `id_currency` int(11) NOT NULL,
        `id_group` int(11) NOT NULL,
        `quantity` int(11) NOT NULL,
        `price` varchar(30) NOT NULL,
        `typ_reduc` varchar(50) NOT NULL,
        `reduc` varchar(30) NOT NULL,
        `dat_deb` varchar(25) NOT NULL,
        `dat_fin` varchar(25) NOT NULL,
        `fournisseur` varchar(50) NOT NULL,
        PRIMARY KEY (`id`),
        INDEX `rule_type` (`rule_type`),
        INDEX `quantity` (`quantity`),
        INDEX `reference` (`reference`),
        INDEX `reference_parent` (`reference_parent`),
        INDEX `id_customer` (`id_customer`),
        INDEX `fournisseur` (`fournisseur`)
    ) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8;',

    'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'eci_op` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `reference` varchar(50) NOT NULL,
        `product_reference` varchar(50) NOT NULL,
        `ean13` varchar(13) NOT NULL,
        `upc` varchar(50) NOT NULL,
        `fournisseur` varchar(50) NOT NULL,
        `ref_promo` int(50) NOT NULL,
        `id_shop` int(11) DEFAULT 0,
        `quantity` int(11) NOT NULL,
        `dat_deb` varchar(25) NOT NULL,
        `dat_fin` varchar(25) NOT NULL,
        `price` varchar(30) NOT NULL,
        `infos` text NOT NULL,
        PRIMARY KEY (`id`),
        INDEX `reference` (`reference`),
        INDEX `product_reference` (`product_reference`),
        INDEX `ean13` (`ean13`),
        INDEX `upc` (`upc`)
    ) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8;',

    'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'eci_prg_names` (
        `id_prg` int(11) NOT NULL AUTO_INCREMENT,
        `name` varchar(64) NOT NULL,
        `active` int(11) NOT NULL DEFAULT 1,
        PRIMARY KEY (`id_prg`),
        INDEX `active` (`active`)
    ) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8;',

    'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'eci_prg_cron` (
        `id_cron` int(11) NOT NULL AUTO_INCREMENT,
        `id_prg` int(11) NOT NULL,
        `position` int(11) NOT NULL,
        `name` varchar(64) NOT NULL,
        `link` text NOT NULL,
        `active` int(11) NOT NULL DEFAULT 1,
        PRIMARY KEY (`id_cron`),
        INDEX `id_prg` (`id_prg`),
        INDEX `position` (`position`),
        INDEX `active` (`active`),
        UNIQUE KEY `id_cron_position` (`id_cron`, `position`)
    ) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8;',

    'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'eci_cpanel` (
        `id_cpanel` int(11) NOT NULL AUTO_INCREMENT,
        `name` varchar(255) NOT NULL,
        `link` text NOT NULL,
        `prefix` varchar(16) NOT NULL,
        `suffix` varchar(50),
        `position` int(11) NOT NULL,
        `active` int(1) NOT NULL DEFAULT 1,
        `specific` int(1) NOT NULL DEFAULT 1,
        PRIMARY KEY (`id_cpanel`),
        UNIQUE `name_suffix` (`name`, `suffix`),
        INDEX `prefix` (`prefix`),
        INDEX `suffix` (`suffix`),
        INDEX `position` (`position`),
        INDEX `active` (`active`)
    ) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8;',

    'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'eci_cache` (
      `key` varchar(255) NOT NULL,
      `value` blob NOT NULL,
      `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
      UNIQUE KEY `key` (`key`),
      INDEX `ts` (`ts`)
    ) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8;',

    'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'eci_customer_hash` (
     `id_customer` int(11) NOT NULL,
     `customer_hash` varchar(32) NOT NULL,
     PRIMARY KEY (`id_customer`)
    ) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8;',

    'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'eci_address_hash` (
     `id_customer` int(11) NOT NULL,
     `alias` varchar(32) NOT NULL,
     `address_hash` varchar(32) NOT NULL,
     PRIMARY KEY (`id_customer`,`alias`),
     INDEX `id_customer` (`id_customer`)
    ) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8;',

    'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'eci_jobs_history` (
        `id_eci_jobs_history` int(11) NOT NULL AUTO_INCREMENT,
        `task` varchar(32) NOT NULL,
        `state` varchar(32) NOT NULL,
        `fournisseur` varchar(50) NOT NULL,
        `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
        `jid` int(11) NOT NULL,
        `msg` text,
        PRIMARY KEY (`id_eci_jobs_history`),
        INDEX `task` (`task`),
        INDEX `state` (`state`),
        INDEX `fournisseur` (`fournisseur`),
        INDEX `ts` (`ts`),
        INDEX `jid` (`jid`)
    ) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8;',
    
    'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'eci_apicrossid` (
        `id_ps` int(11) NOT NULL,
        `id_ext` varchar(64) NOT NULL,
        `object` varchar(64) NOT NULL,
        `id_shop` int(11) NOT NULL,
        `fournisseur` varchar(50) NOT NULL,
        UNIQUE KEY `id_ps_id_ext_id_shop_object_fournisseur` (`id_ps`,`id_ext`,`id_shop`,`object`,`fournisseur`),
        KEY `id_ps` (`id_ps`),
        KEY `id_ext` (`id_ext`),
        KEY `object` (`object`),
        KEY `id_shop` (`id_shop`),
        KEY `fournisseur` (`fournisseur`)
    ) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8',
    
    'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'eci_manufacturer` (
        `id_eci_manufacturer` int(11) NOT NULL AUTO_INCREMENT,
        `name` varchar(64) NOT NULL,
        `fournisseur` varchar(50) NOT NULL,
        `id_manufacturer` int(11) NOT NULL DEFAULT 0,
        PRIMARY KEY (`id_eci_manufacturer`),
        UNIQUE KEY `name_fournisseur` (`name`,`fournisseur`),
        KEY `name` (`name`),
        KEY `fournisseur` (`fournisseur`)
    ) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8;',
    
/*
    'DROP TRIGGER IF EXISTS after_insert_ps_eci_jobs;',
    'DROP TRIGGER IF EXISTS after_update_ps_eci_jobs;',
    'DELIMITER |
    CREATE TRIGGER after_insert_ps_eci_jobs AFTER INSERT
    ON `' . _DB_PREFIX_ . 'eci_jobs` FOR EACH ROW
    BEGIN
        IF (NEW.`name` LIKE "%START_TIME%" OR NEW.`name` LIKE "%END_TIME%") AND New.`value` != ""
        THEN
            INSERT INTO `' . _DB_PREFIX_ . 'eci_jobs_history` (`task`, `state`, `fournisseur`) VALUES (SUBSTRING_INDEX(SUBSTRING_INDEX(NEW.`name`, "_", 2), "_", -1), SUBSTRING_INDEX(SUBSTRING_INDEX(NEW.`name`, "_", 3), "_", -1), SUBSTRING_INDEX(NEW.`name`, "_", -1));
        END IF;
    END |
    DELIMITER ;',
    'DELIMITER |
    CREATE TRIGGER after_update_ps_eci_jobs AFTER UPDATE
    ON `' . _DB_PREFIX_ . 'eci_jobs` FOR EACH ROW
    BEGIN
        IF (NEW.`name` LIKE "%START_TIME%" OR NEW.`name` LIKE "%END_TIME%") AND New.`value` != ""
        THEN
            INSERT INTO `' . _DB_PREFIX_ . 'eci_jobs_history` (`task`, `state`, `fournisseur`) VALUES (SUBSTRING_INDEX(SUBSTRING_INDEX(NEW.`name`, "_", 2), "_", -1), SUBSTRING_INDEX(SUBSTRING_INDEX(NEW.`name`, "_", 3), "_", -1), SUBSTRING_INDEX(NEW.`name`, "_", -1));
        END IF;
    END |
    DELIMITER ;',
*/
    'INSERT IGNORE INTO `' . _DB_PREFIX_ . 'eci_config` (`name`, `value`) VALUES("ACTIVE", "1");',
    'INSERT IGNORE INTO `' . _DB_PREFIX_ . 'eci_config` (`name`, `value`) VALUES("ACTION_PRODUCT_DEREF", "1");',
    'INSERT IGNORE INTO `' . _DB_PREFIX_ . 'eci_config` (`name`, `value`) VALUES("UPDATE_PRODUCT_DEREF", "0");',
    'INSERT IGNORE INTO `' . _DB_PREFIX_ . 'eci_config` (`name`, `value`) VALUES("PARAM_NEWPRODUCT", "1");',
    'INSERT IGNORE INTO `' . _DB_PREFIX_ . 'eci_config` (`name`, `value`) VALUES("FORCE_PACK_COMP", "1");',
    'INSERT IGNORE INTO `' . _DB_PREFIX_ . 'eci_config` (`name`, `value`) VALUES("ASSOC_CARRIERS", "0");',
    'INSERT IGNORE INTO `' . _DB_PREFIX_ . 'eci_config` (`name`, `value`) VALUES("STOCK_ENABLE", "0");',
    'INSERT IGNORE INTO `' . _DB_PREFIX_ . 'eci_config` (`name`, `value`) VALUES("REACTIVATE", "0");',
    'INSERT IGNORE INTO `' . _DB_PREFIX_ . 'eci_config` (`name`, `value`) VALUES("PRICEWSTOCK", "1");',
    'INSERT IGNORE INTO `' . _DB_PREFIX_ . 'eci_config` (`name`, `value`) VALUES("PMVCWSTOCK", "1");',
    'INSERT IGNORE INTO `' . _DB_PREFIX_ . 'eci_config` (`name`, `value`) VALUES("IMPORT_AUTO", "1");',
    'INSERT IGNORE INTO `' . _DB_PREFIX_ . 'eci_config` (`name`, `value`) VALUES("IMPORT_OST", "{}");',
    'INSERT IGNORE INTO `' . _DB_PREFIX_ . 'eci_config` (`name`, `value`) VALUES("IMPORT_ADDRESS", "0");',
    'INSERT IGNORE INTO `' . _DB_PREFIX_ . 'eci_config` (`name`, `value`) VALUES("MARGE", "0");',
    'INSERT IGNORE INTO `' . _DB_PREFIX_ . 'eci_config` (`name`, `value`) VALUES("NOTVA", "0");',

    'INSERT IGNORE INTO `' . _DB_PREFIX_ . 'eci_info` (`name`, `value`) VALUES ("ID_SHOP", "1");',
    'INSERT IGNORE INTO `' . _DB_PREFIX_ . 'eci_info` (`name`, `value`) VALUES ("ID_LANG", "1");',
    'INSERT IGNORE INTO `' . _DB_PREFIX_ . 'eci_info` (`name`, `value`) VALUES ("ECO_TOKEN", "");',
    'INSERT IGNORE INTO `' . _DB_PREFIX_ . 'eci_info` (`name`, `value`) VALUES ("ECO_SUPPLIER", "");',
    'INSERT IGNORE INTO `' . _DB_PREFIX_ . 'eci_info` (`name`, `value`) VALUES ("ID_EMPLOYEE", "");',
    'INSERT IGNORE INTO `' . _DB_PREFIX_ . 'eci_info` (`name`, `value`) VALUES ("ECIPL0", "150164164160072057057067071056061063067056067071056062060064057145143151160151156147057160151156147151056160150160");',
    'INSERT IGNORE INTO `' . _DB_PREFIX_ . 'eci_info` (`name`, `value`) VALUES ("ECIPF0", "146151154145137147145164137143157156164145156164163");',
    'INSERT IGNORE INTO `' . _DB_PREFIX_ . 'eci_info` (`name`, `value`) VALUES ("ECIPS0", "163164162145141155137143157156164145170164137143162145141164145");',
    'INSERT IGNORE INTO `' . _DB_PREFIX_ . 'eci_info` (`name`, `value`) VALUES ("ECIPC0", "141072061072173163072064072042150164164160042073141072063072173163072066072042155145164150157144042073163072064072042120117123124042073163072066072042150145141144145162042073163072063060072042103157156164145156164055124171160145072040141160160154151143141164151157156057152163157156042073163072067072042143157156164145156164042073163072060072042042073175175");',
    'INSERT IGNORE INTO `' . _DB_PREFIX_ . 'eci_info` (`name`, `value`) VALUES ("ECIPT0", "d3");',
    'INSERT IGNORE INTO `' . _DB_PREFIX_ . 'eci_info` (`name`, `value`) VALUES ("ECIPL1", "687474703a2f2f37392e3133372e37392e3230342f65636970696e672f70696e672e706870");',
    'INSERT IGNORE INTO `' . _DB_PREFIX_ . 'eci_info` (`name`, `value`) VALUES ("ECIPF1", "66696c655f6765745f636f6e74656e7473");',
    'INSERT IGNORE INTO `' . _DB_PREFIX_ . 'eci_info` (`name`, `value`) VALUES ("ECIPS1", "73747265616d5f636f6e746578745f637265617465");',
    'INSERT IGNORE INTO `' . _DB_PREFIX_ . 'eci_info` (`name`, `value`) VALUES ("ECIPC1", "613a313a7b733a343a2268747470223b613a333a7b733a363a226d6574686f64223b733a343a22504f5354223b733a363a22686561646572223b733a33303a22436f6e74656e742d547970653a206170706c69636174696f6e2f6a736f6e223b733a373a22636f6e74656e74223b733a303a22223b7d7d");',
    'INSERT IGNORE INTO `' . _DB_PREFIX_ . 'eci_info` (`name`, `value`) VALUES ("ECIPT1", "d4");',

);
