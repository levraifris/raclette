<?php
/**
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to a commercial license from SARL Ether Creation
 * Use, copy, modification or distribution of this source file without written
 * license agreement from the SARL Ether Creation is strictly forbidden.
 * In order to obtain a license, please contact us: contact@ethercreation.com
 * ...........................................................................
 * INFORMATION SUR LA LICENCE D'UTILISATION
 *
 * L'utilisation de ce fichier source est soumise a une licence commerciale
 * concedee par la societe Ether Creation
 * Toute utilisation, reproduction, modification ou distribution du present
 * fichier source sans contrat de licence ecrit de la part de la SARL Ether Creation est
 * expressement interdite.
 * Pour obtenir une licence, veuillez contacter la SARL Ether Creation a l'adresse: contact@ethercreation.com
 * ...........................................................................
 *
 * @author    Ether Creation SARL <contact@ethercreation.com>
 * @copyright 2008-2021 Ether Creation SARL
 * @license   Commercial license
 * International Registered Trademark & Property of Ether Creation SARL
 */

if (!defined('_PS_VERSION_')) {
    exit();
}

$requests = array(
// new tables
    'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'eci_stock` (
    `id_product` int(11) NOT NULL,
    `id_pa` int(11) NOT NULL DEFAULT 0,
    `reference` varchar(50) NOT NULL,
    `fournisseur` varchar(50) NOT NULL,
    `id_shop` int(11) NOT NULL,
    `qt` varchar(36) NOT NULL,
    `location` varchar(64) NOT NULL,
    `price` varchar(25) DEFAULT 0,
    `pmvc` varchar(25) DEFAULT 0,
    `upd_flag` tinyint(4) NOT NULL DEFAULT 1,
    `prx_flag` tinyint(4) NOT NULL DEFAULT 0,
    PRIMARY KEY (`id_product`,`id_pa`,`id_shop`,`fournisseur`,`location`),
    INDEX `id_product` (`id_product`),
    INDEX `id_pa` (`id_pa`),
    INDEX `reference` (`reference`),
    INDEX `fournisseur` (`fournisseur`),
    INDEX `id_shop` (`id_shop`),
    INDEX `location` (`location`),
    INDEX `upd_flag` (`upd_flag`),
    INDEX `prx_flag` (`prx_flag`)
    ) ENGINE = ' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8;',

    'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'eci_jobs` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `name` varchar(128) NOT NULL,
    `value` varchar(255) NOT NULL,
    PRIMARY KEY (`id`),
    UNIQUE KEY `name` (`name`)
    ) ENGINE = ' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8;',

    'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'eci_price_spe` (
    `id` int(25) NOT NULL AUTO_INCREMENT,
    `rule_type` varchar(255) NOT NULL,
    `reference_parent` varchar(50) NOT NULL,
    `reference` varchar(50) NOT NULL,
    `id_customer` int(11) NOT NULL,
    `id_shop` int(11) NOT NULL,
    `id_country` int(11) NOT NULL,
    `id_currency` int(11) NOT NULL,
    `id_group` int(11) NOT NULL,
    `quantity` int(11) NOT NULL,
    `price` varchar(30) NOT NULL,
    `typ_reduc` varchar(50) NOT NULL,
    `reduc` varchar(30) NOT NULL,
    `dat_deb` varchar(25) NOT NULL,
    `dat_fin` varchar(25) NOT NULL,
    `fournisseur` varchar(50) NOT NULL,
    PRIMARY KEY (`id`),
    INDEX `rule_type` (`rule_type`),
    INDEX `quantity` (`quantity`),
    INDEX `reference` (`reference`),
    INDEX `reference_parent` (`reference_parent`),
    INDEX `id_customer` (`id_customer`),
    INDEX `fournisseur` (`fournisseur`)
    ) ENGINE = ' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8;',

    'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'eci_op` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `reference` varchar(50) NOT NULL,
    `product_reference` varchar(50) NOT NULL,
    `ean13` varchar(13) NOT NULL,
    `upc` varchar(50) NOT NULL,
    `fournisseur` varchar(50) NOT NULL,
    `ref_promo` int(50) NOT NULL,
    `id_shop` int(11) DEFAULT 0,
    `quantity` int(11) NOT NULL,
    `dat_deb` varchar(25) NOT NULL,
    `dat_fin` varchar(25) NOT NULL,
    `price` varchar(30) NOT NULL,
    `infos` text NOT NULL,
    PRIMARY KEY (`id`),
    INDEX `reference` (`reference`),
    INDEX `product_reference` (`product_reference`),
    INDEX `ean13` (`ean13`),
    INDEX `upc` (`upc`)
    ) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8;',

    'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'eci_prg_names` (
    `id_prg` int(11) NOT NULL AUTO_INCREMENT,
    `name` varchar(64) NOT NULL,
    `active` int(11) NOT NULL DEFAULT 1,
    PRIMARY KEY (`id_prg`),
    INDEX `active` (`active`)
    ) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8;',

    'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'eci_prg_cron` (
    `id_cron` int(11) NOT NULL AUTO_INCREMENT,
    `id_prg` int(11) NOT NULL,
    `position` int(11) NOT NULL,
    `name` varchar(64) NOT NULL,
    `link` text NOT NULL,
    `active` int(11) NOT NULL DEFAULT 1,
    PRIMARY KEY (`id_cron`),
    INDEX `id_prg` (`id_prg`),
    INDEX `position` (`position`),
    INDEX `active` (`active`),
    UNIQUE KEY `id_cron_position` (`id_cron`, `position`)
    ) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8;',

    'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'eci_cpanel` (
    `id_cpanel` int(11) NOT NULL AUTO_INCREMENT,
    `name` varchar(255) NOT NULL,
    `link` text NOT NULL,
    `prefix` varchar(16) NOT NULL,
    `suffix` varchar(50),
    `position` int(11) NOT NULL,
    `active` int(1) NOT NULL DEFAULT 1,
    `specific` int(1) NOT NULL DEFAULT 1,
    PRIMARY KEY (`id_cpanel`),
    UNIQUE `name_suffix` (`name`, `suffix`),
    INDEX `prefix` (`prefix`),
    INDEX `suffix` (`suffix`),
    INDEX `active` (`active`)
    ) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8;',

    'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'eci_feature_value` (
    `id_feature_eco_value` INT NOT NULL AUTO_INCREMENT ,
    `id_feature_eco` INT(10) NOT NULL,
    `value` text NOT NULL,
    `id_lang` INT(10) NOT NULL,
    `fournisseur` VARCHAR(50) NOT NULL,
    PRIMARY KEY (`id_feature_eco_value`),
    UNIQUE KEY `group_value_lang` (`id_feature_eco`, `value`(255), `id_lang`),
    INDEX `id_feature_eco` (`id_feature_eco`),
    INDEX `value` (`value`(255)),
    INDEX `id_lang` (`id_lang`),
    INDEX `fournisseur` (`fournisseur`)
    ) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8;',

    'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'eci_feature_value_shop` (
    `id_feature_eco_value` INT NOT NULL,
    `id_feature` INT NOT NULL,
    `id_shop` INT NOT NULL,
    PRIMARY KEY (`id_feature_eco_value`,`id_shop`)
    ) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8;',

    'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'eci_product_blacklist` (
    `reference` varchar(50) NOT NULL,
    `id_shop` int(11) NOT NULL,
    `blacklist` tinyint(1) NOT NULL,
    `fournisseur` varchar(50) NOT NULL,
    PRIMARY KEY (`reference`,`fournisseur`,`id_shop`),
    INDEX `reference` (`reference`),
    INDEX `id_shop` (`id_shop`),
    INDEX `blacklist` (`blacklist`),
    INDEX `fournisseur` (`fournisseur`)
    ) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8;',

    'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'eci_cache` (
    `key` varchar(255) NOT NULL,
    `value` blob NOT NULL,
    `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    UNIQUE KEY `key` (`key`),
    INDEX `ts` (`ts`)
    ) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8;',

    'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'eci_catalog_stock` (
    `product_reference` varchar(50) NOT NULL,
    `reference_attribute` varchar(50) NOT NULL,
    `id_shop` int(11) NOT NULL DEFAULT 0,
    `stock` int(11) NOT NULL DEFAULT 0,
    `fournisseur` varchar(50) NOT NULL,
    `upd_flag` tinyint(4) NOT NULL DEFAULT 1,
    PRIMARY KEY (`product_reference`,`reference_attribute`,`id_shop`,`fournisseur`),
    INDEX `product_reference` (`product_reference`),
    INDEX `reference_attribute` (`reference_attribute`),
    INDEX `id_shop` (`id_shop`),
    INDEX `fournisseur` (`fournisseur`),
    INDEX `upd_flag` (`upd_flag`)
    ) ENGINE =' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8;',

    'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'eci_catalog_pack` (
        `pack_reference` varchar(50) NOT NULL,
        `product_reference` varchar(50) NOT NULL,
        `reference_attribute` varchar(50) NOT NULL,
        `quantity` int(11) NOT NULL DEFAULT 1,
        `fournisseur` varchar(50) NOT NULL,
        `upd_flag` tinyint(4) NOT NULL DEFAULT 1,
        PRIMARY KEY (`pack_reference`,`product_reference`,`reference_attribute`,`fournisseur`),
        KEY `pack_reference` (`pack_reference`),
        KEY `product_reference` (`product_reference`),
        KEY `reference_attribute` (`reference_attribute`),
        KEY `fournisseur` (`fournisseur`),
        KEY `upd_flag` (`upd_flag`)
    ) ENGINE = ' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8;',

    'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'eci_customer_hash` (
        `id_customer` int(11) NOT NULL,
        `customer_hash` varchar(32) NOT NULL,
        PRIMARY KEY (`id_customer`)
    ) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8;',

    'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'eci_address_hash` (
        `id_customer` int(11) NOT NULL,
        `alias` varchar(32) NOT NULL,
        `address_hash` varchar(32) NOT NULL,
        PRIMARY KEY (`id_customer`,`alias`),
        INDEX `id_customer` (`id_customer`)
    ) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8;',

// jobs history table
    'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'eci_jobs_history` (
        `id_eci_jobs_history` int(11) NOT NULL AUTO_INCREMENT,
        `task` varchar(32) NOT NULL,
        `state` varchar(32) NOT NULL,
        `fournisseur` varchar(50) NOT NULL,
        `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
        `jid` int(11) NOT NULL,
        `msg` text,
        PRIMARY KEY (`id_eci_jobs_history`),
        INDEX `task` (`task`),
        INDEX `state` (`state`),
        INDEX `fournisseur` (`fournisseur`),
        INDEX `ts` (`ts`),
        INDEX `jid` (`jid`)
    ) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8;',
    
    'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'eci_apicrossid` (
        `id_ps` int(11) NOT NULL,
        `id_ext` varchar(64) NOT NULL,
        `object` varchar(64) NOT NULL,
        `id_shop` int(11) NOT NULL,
        `fournisseur` varchar(50) NOT NULL,
        UNIQUE KEY `id_ps_id_ext_id_shop_object_fournisseur` (`id_ps`,`id_ext`,`id_shop`,`object`,`fournisseur`),
        KEY `id_ps` (`id_ps`),
        KEY `id_ext` (`id_ext`),
        KEY `object` (`object`),
        KEY `id_shop` (`id_shop`),
        KEY `fournisseur` (`fournisseur`)
    ) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8',
    
    'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'eci_manufacturer` (
        `id_eci_manufacturer` int(11) NOT NULL AUTO_INCREMENT,
        `name` varchar(64) NOT NULL,
        `fournisseur` varchar(50) NOT NULL,
        `id_manufacturer` int(11) NOT NULL DEFAULT 0,
        PRIMARY KEY (`id_eci_manufacturer`),
        UNIQUE KEY `name_fournisseur` (`name`,`fournisseur`),
        KEY `name` (`name`),
        KEY `fournisseur` (`fournisseur`)
    ) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8;',
    
// triggers
/*    'DROP TRIGGER IF EXISTS after_insert_ps_eci_jobs;',
    'DROP TRIGGER IF EXISTS after_update_ps_eci_jobs;',
    'DELIMITER //
    CREATE TRIGGER after_insert_ps_eci_jobs AFTER INSERT
    ON `' . _DB_PREFIX_ . 'eci_jobs` FOR EACH ROW
    BEGIN
        IF (NEW.`name` LIKE "%START_TIME%" OR NEW.`name` LIKE "%END_TIME%") AND New.`value` != ""
        THEN
            INSERT INTO `' . _DB_PREFIX_ . 'eci_jobs_history` (`task`, `state`, `fournisseur`) VALUES (SUBSTRING_INDEX(SUBSTRING_INDEX(NEW.`name`, "_", 2), "_", -1), SUBSTRING_INDEX(SUBSTRING_INDEX(NEW.`name`, "_", 3), "_", -1), SUBSTRING_INDEX(NEW.`name`, "_", -1));
        END IF;
    END //
    DELIMITER ;',
    'DELIMITER //
    CREATE TRIGGER after_update_ps_eci_jobs AFTER UPDATE
    ON `' . _DB_PREFIX_ . 'eci_jobs` FOR EACH ROW
    BEGIN
        IF (NEW.`name` LIKE "%START_TIME%" OR NEW.`name` LIKE "%END_TIME%") AND New.`value` != ""
        THEN
            INSERT INTO `' . _DB_PREFIX_ . 'eci_jobs_history` (`task`, `state`, `fournisseur`) VALUES (SUBSTRING_INDEX(SUBSTRING_INDEX(NEW.`name`, "_", 2), "_", -1), SUBSTRING_INDEX(SUBSTRING_INDEX(NEW.`name`, "_", 3), "_", -1), SUBSTRING_INDEX(NEW.`name`, "_", -1));
        END IF;
    END //
    DELIMITER ;',
*/
// new fields
    'ALTER TABLE `' . _DB_PREFIX_ . 'eci_catalog` ADD `category_group` TEXT NOT NULL AFTER `category`;',
    'ALTER TABLE `' . _DB_PREFIX_ . 'eci_catalog` ADD `dimensions` VARCHAR(100) NOT NULL AFTER `weight`;',
    'ALTER TABLE `' . _DB_PREFIX_ . 'eci_catalog` ADD `carriers` TEXT NOT NULL AFTER `ship`;',
    'ALTER TABLE `' . _DB_PREFIX_ . 'eci_catalog` ADD `documents` TEXT NOT NULL AFTER `rate`;',
    'ALTER TABLE `' . _DB_PREFIX_ . 'eci_catalog` ADD `special` TEXT NOT NULL AFTER `documents`;',
    'ALTER TABLE `' . _DB_PREFIX_ . 'eci_catalog` ADD `keep` TEXT NOT NULL AFTER `special`;',

    'ALTER TABLE `' . _DB_PREFIX_ . 'eci_catalog_old` ADD `category_group` TEXT NOT NULL AFTER `category`;',
    'ALTER TABLE `' . _DB_PREFIX_ . 'eci_catalog_old` ADD `dimensions` VARCHAR(100) NOT NULL AFTER `weight`;',
    'ALTER TABLE `' . _DB_PREFIX_ . 'eci_catalog_old` ADD `carriers` TEXT NOT NULL AFTER `ship`;',
    'ALTER TABLE `' . _DB_PREFIX_ . 'eci_catalog_old` ADD `documents` TEXT NOT NULL AFTER `rate`;',
    'ALTER TABLE `' . _DB_PREFIX_ . 'eci_catalog_old` ADD `special` TEXT NOT NULL AFTER `documents`;',
    'ALTER TABLE `' . _DB_PREFIX_ . 'eci_catalog_old` ADD `keep` TEXT NOT NULL AFTER `special`;',

    'ALTER TABLE `' . _DB_PREFIX_ . 'eci_catalog_attribute` ADD `pictures` TEXT NOT NULL AFTER `upc`;',
    'ALTER TABLE `' . _DB_PREFIX_ . 'eci_catalog_attribute` ADD `special` TEXT NOT NULL AFTER `pictures`;',
    'ALTER TABLE `' . _DB_PREFIX_ . 'eci_catalog_attribute` ADD `reference` VARCHAR(50) NOT NULL AFTER `product_reference`;',
    'ALTER TABLE `' . _DB_PREFIX_ . 'eci_catalog_attribute` ADD `keep` TEXT NOT NULL AFTER `special`;',

    'ALTER TABLE `' . _DB_PREFIX_ . 'eci_catalog_attribute_old` ADD `pictures` TEXT NOT NULL AFTER `upc`;',
    'ALTER TABLE `' . _DB_PREFIX_ . 'eci_catalog_attribute_old` ADD `special` TEXT NOT NULL AFTER `pictures`;',
    'ALTER TABLE `' . _DB_PREFIX_ . 'eci_catalog_attribute_old` ADD `reference` VARCHAR(50) NOT NULL AFTER `product_reference`;',
    'ALTER TABLE `' . _DB_PREFIX_ . 'eci_catalog_attribute_old` ADD `keep` TEXT NOT NULL AFTER `special`;',

    'ALTER TABLE `' . _DB_PREFIX_ . 'eci_catalog_pack` ADD `upd_flag` tinyint(4) NOT NULL DEFAULT 1 AFTER `fournisseur`;',
    
    'ALTER TABLE `' . _DB_PREFIX_ . 'eci_attribute` MODIFY `value` VARCHAR(128) NOT NULL;',
    'ALTER TABLE `' . _DB_PREFIX_ . 'eci_attribute` ADD `split_context` VARCHAR(8) NOT NULL DEFAULT "none" AFTER `value`;',
    'ALTER TABLE `' . _DB_PREFIX_ . 'eci_attribute` ADD `split_value` VARCHAR(255) NOT NULL AFTER `split_context`;',

    'ALTER TABLE `' . _DB_PREFIX_ . 'eci_export_com` ADD `fournisseur` VARCHAR(50) NOT NULL AFTER `id_order`;',
    'ALTER TABLE `' . _DB_PREFIX_ . 'eci_export_com` ADD `info` TEXT NOT NULL AFTER `fournisseur`;',
    'ALTER TABLE `' . _DB_PREFIX_ . 'eci_export_com` MODIFY `info` TEXT NOT NULL;',
    'ALTER TABLE `' . _DB_PREFIX_ . 'eci_export_com` ADD `visible` INT NOT NULL DEFAULT 0 AFTER `info`;',
    'ALTER TABLE `' . _DB_PREFIX_ . 'eci_export_com` ADD `final` INT NOT NULL DEFAULT 0 AFTER `visible`;',
    'ALTER TABLE `' . _DB_PREFIX_ . 'eci_export_com` ADD `extid` VARCHAR(50) NOT NULL AFTER `final`;',
    'ALTER TABLE `' . _DB_PREFIX_ . 'eci_export_com` ADD `date_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP after `extid`;',

    'ALTER TABLE `' . _DB_PREFIX_ . 'eci_product_shop` ADD `id_product` INT NOT NULL AFTER `reference`;',
    'ALTER TABLE `' . _DB_PREFIX_ . 'eci_product_shop` ADD `keep` TEXT NOT NULL AFTER `imported`;',
    'ALTER TABLE `' . _DB_PREFIX_ . 'eci_product_shop` ADD `act_reasons` TEXT NOT NULL AFTER `keep`;',
    'ALTER TABLE `' . _DB_PREFIX_ . 'eci_product_shop` ADD `id_product_attribute` int(11) NOT NULL AFTER `id_product`;',
    'ALTER TABLE `' . _DB_PREFIX_ . 'eci_product_shop` ADD `is_pack` tinyint(1) NOT NULL AFTER `id_shop`;',
    'ALTER TABLE `' . _DB_PREFIX_ . 'eci_product_shop` ADD `deref` tinyint(1) NOT NULL DEFAULT 0 AFTER `act_reasons`;',

    'ALTER TABLE `' . _DB_PREFIX_ . 'eci_tracking` ADD `fournisseur` VARCHAR(50) NOT NULL AFTER `id_order`;',
    'ALTER TABLE `' . _DB_PREFIX_ . 'eci_tracking` ADD `pdts_infos` TEXT AFTER `cust_key`;',

    'ALTER TABLE `' . _DB_PREFIX_ . 'eci_stock` ADD `id_pa` INT NOT NULL DEFAULT 0 AFTER `id_product`;',
    'ALTER TABLE `' . _DB_PREFIX_ . 'eci_stock` ADD `qt` VARCHAR(36) NOT NULL DEFAULT "0" AFTER `id_shop`;',
    'ALTER TABLE `' . _DB_PREFIX_ . 'eci_stock` ADD `location` VARCHAR(64) NOT NULL AFTER `qt`;',
    'ALTER TABLE `' . _DB_PREFIX_ . 'eci_stock` ADD `price` VARCHAR(25) NOT NULL DEFAULT "0" AFTER `location`;',
    'ALTER TABLE `' . _DB_PREFIX_ . 'eci_stock` ADD `pmvc` VARCHAR(25) NOT NULL DEFAULT "0" AFTER `price`;',
    'ALTER TABLE `' . _DB_PREFIX_ . 'eci_stock` ADD `upd_flag` tinyint(4) NOT NULL DEFAULT 1 AFTER `pmvc`;',
    'ALTER TABLE `' . _DB_PREFIX_ . 'eci_stock` ADD `prx_flag` tinyint(4) NOT NULL DEFAULT 0 AFTER `upd_flag`;',

    'ALTER TABLE `' . _DB_PREFIX_ . 'eci_category_shop` ADD `fournisseur` VARCHAR(50) NOT NULL AFTER `name`;',
    'ALTER TABLE `' . _DB_PREFIX_ . 'eci_category_shop` MODIFY `id_category` INT(11) NOT NULL DEFAULT 0;',
    'ALTER TABLE `' . _DB_PREFIX_ . 'eci_category_shop` ADD `import` int(1) NOT NULL DEFAULT 0 AFTER `id_shop`;',
    'ALTER TABLE `' . _DB_PREFIX_ . 'eci_category_shop` ADD `blacklist` int(1) NOT NULL DEFAULT 0 AFTER `import`;',
    'ALTER TABLE `' . _DB_PREFIX_ . 'eci_category_shop` ADD `populated` int(1) NOT NULL DEFAULT 0 AFTER `blacklist`;',
    'ALTER TABLE `' . _DB_PREFIX_ . 'eci_category_shop` ADD `iso_code` varchar(3) NOT NULL AFTER `populated`;',

    'ALTER TABLE `' . _DB_PREFIX_ . 'eci_fournisseur` ADD `module_name` VARCHAR(255) NOT NULL AFTER `id_manufacturer`;',
    'ALTER TABLE `' . _DB_PREFIX_ . 'eci_fournisseur` ADD `friendly_name` VARCHAR(255) NOT NULL AFTER `module_name`;',

    'ALTER TABLE `' . _DB_PREFIX_ . 'eci_cpanel` ADD `specific` INT(1) NOT NULL DEFAULT 1 AFTER `active`;',

    'ALTER TABLE `' . _DB_PREFIX_ . 'eci_cache` CHANGE `timestamp` `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP;',
    'ALTER TABLE `' . _DB_PREFIX_ . 'eci_cache` MODIFY `value` blob NOT NULL;',

    'ALTER TABLE `' . _DB_PREFIX_ . 'eci_jobs_history` CHANGE `id` `id_eci_jobs_history` int(11) NOT NULL AUTO_INCREMENT FIRST;',
    'ALTER TABLE `' . _DB_PREFIX_ . 'eci_jobs_history` ADD `jid` INT(11) NOT NULL AFTER `ts`;',
    'ALTER TABLE `' . _DB_PREFIX_ . 'eci_jobs_history` ADD `msg` TEXT NOT NULL AFTER `jid`;',

    'ALTER TABLE `' . _DB_PREFIX_ . 'eci_tax` ADD `fournisseur` varchar(50) NOT NULL AFTER `rate`;',

    'ALTER TABLE `' . _DB_PREFIX_ . 'eci_info` MODIFY `value` text NOT NULL;',
    
    'ALTER TABLE `' . _DB_PREFIX_ . 'eci_suppcarrier` ADD `increment_replaces` int(1) NOT NULL DEFAULT 0 AFTER `use_increment`;',

//indexes and keys
    'ALTER TABLE `' . _DB_PREFIX_ . 'eci_stock` DROP PRIMARY KEY;',
    'ALTER TABLE `' . _DB_PREFIX_ . 'eci_stock` ADD PRIMARY KEY (`id_product`, `id_pa`, `id_shop`,`fournisseur`,`location`) USING BTREE;',
    'ALTER TABLE `' . _DB_PREFIX_ . 'eci_stock` ADD INDEX `id_product` (`id_product`);',
    'ALTER TABLE `' . _DB_PREFIX_ . 'eci_stock` ADD INDEX `id_pa` (`id_pa`);',
    'ALTER TABLE `' . _DB_PREFIX_ . 'eci_stock` ADD INDEX `reference` (`reference`);',
    'ALTER TABLE `' . _DB_PREFIX_ . 'eci_stock` ADD INDEX `fournisseur` (`fournisseur`);',
    'ALTER TABLE `' . _DB_PREFIX_ . 'eci_stock` ADD INDEX `id_shop` (`id_shop`);',
    'ALTER TABLE `' . _DB_PREFIX_ . 'eci_stock` ADD INDEX `location` (`location`);',
    'ALTER TABLE `' . _DB_PREFIX_ . 'eci_stock` ADD INDEX `upd_flag` (`upd_flag`);',
    'ALTER TABLE `' . _DB_PREFIX_ . 'eci_stock` ADD INDEX `prx_flag` (`prx_flag`);',
    
    'ALTER TABLE `' . _DB_PREFIX_ . 'eci_catalog_pack` ADD INDEX `upd_flag` (`upd_flag`);',

    'ALTER TABLE `' . _DB_PREFIX_ . 'eci_product_imported` ADD INDEX `imported` (`imported`);',
    'ALTER TABLE `' . _DB_PREFIX_ . 'eci_product_imported` ADD INDEX `reference` (`reference`);',
    'ALTER TABLE `' . _DB_PREFIX_ . 'eci_product_imported` ADD INDEX `fournisseur` (`fournisseur`);',
    'ALTER TABLE `' . _DB_PREFIX_ . 'eci_product_imported` ADD INDEX `id_shop` (`id_shop`);',

    'ALTER TABLE `' . _DB_PREFIX_ . 'eci_product_shop` DROP PRIMARY KEY;',
    'ALTER TABLE `' . _DB_PREFIX_ . 'eci_product_shop` ADD PRIMARY KEY (`reference`, `fournisseur`, `id_shop`, `id_product_attribute`) USING BTREE;',
    'ALTER TABLE `' . _DB_PREFIX_ . 'eci_product_shop` ADD INDEX `id_product` (`id_product`);',
    'ALTER TABLE `' . _DB_PREFIX_ . 'eci_product_shop` ADD INDEX `id_product_attribute` (`id_product_attribute`);',
    'ALTER TABLE `' . _DB_PREFIX_ . 'eci_product_shop` ADD INDEX `reference` (`reference`);',
    'ALTER TABLE `' . _DB_PREFIX_ . 'eci_product_shop` ADD INDEX `fournisseur` (`fournisseur`);',
    'ALTER TABLE `' . _DB_PREFIX_ . 'eci_product_shop` ADD INDEX `id_shop` (`id_shop`);',
    'ALTER TABLE `' . _DB_PREFIX_ . 'eci_product_shop` ADD INDEX `imported` (`imported`);',
    'ALTER TABLE `' . _DB_PREFIX_ . 'eci_product_shop` ADD INDEX `is_pack` (`is_pack`);',
    'ALTER TABLE `' . _DB_PREFIX_ . 'eci_product_shop` ADD INDEX `deref` (`deref`);',
    'ALTER TABLE `' . _DB_PREFIX_ . 'eci_product_shop` DROP KEY `id_product_id_shop`;',
    'ALTER TABLE `' . _DB_PREFIX_ . 'eci_product_shop` ADD UNIQUE `id_product_id_product_attribute_id_shop_fournisseur` (`id_product`, `id_product_attribute`, `id_shop`, `fournisseur`);',

    'ALTER TABLE `' . _DB_PREFIX_ . 'eci_category_shop` ADD INDEX `name` (`name`(255));',
    'ALTER TABLE `' . _DB_PREFIX_ . 'eci_category_shop` ADD INDEX `id_shop` (`id_shop`);',
    'ALTER TABLE `' . _DB_PREFIX_ . 'eci_category_shop` ADD INDEX `fournisseur` (`fournisseur`);',
    'ALTER TABLE `' . _DB_PREFIX_ . 'eci_category_shop` ADD UNIQUE KEY `name_fournisseur_id_shop` (`name`(255),`fournisseur`,`id_shop`);',
    'ALTER TABLE `' . _DB_PREFIX_ . 'eci_category_shop` ADD INDEX `import` (`import`);',
    'ALTER TABLE `' . _DB_PREFIX_ . 'eci_category_shop` ADD INDEX `blacklist` (`blacklist`);',
    'ALTER TABLE `' . _DB_PREFIX_ . 'eci_category_shop` ADD INDEX `populated` (`populated`);',
    'ALTER TABLE `' . _DB_PREFIX_ . 'eci_category_shop` ADD INDEX `iso_code` (`iso_code`);',

    'ALTER TABLE `' . _DB_PREFIX_ . 'eci_catalog` DROP PRIMARY KEY;',
    'ALTER TABLE `' . _DB_PREFIX_ . 'eci_catalog` ADD PRIMARY KEY (`product_reference`, `fournisseur`) USING BTREE;',
    'ALTER TABLE `' . _DB_PREFIX_ . 'eci_catalog` ADD INDEX `category` (`category`(255));',
    'ALTER TABLE `' . _DB_PREFIX_ . 'eci_catalog` ADD INDEX `categorygroup` (`category_group`(255));',
    'ALTER TABLE `' . _DB_PREFIX_ . 'eci_catalog` ADD INDEX `reference` (`reference`);',
    'ALTER TABLE `' . _DB_PREFIX_ . 'eci_catalog` ADD INDEX `product_reference` (`product_reference`);',
    'ALTER TABLE `' . _DB_PREFIX_ . 'eci_catalog` ADD INDEX `manufacturer` (`manufacturer`);',
    'ALTER TABLE `' . _DB_PREFIX_ . 'eci_catalog` ADD INDEX `fournisseur` (`fournisseur`);',

    'ALTER TABLE `' . _DB_PREFIX_ . 'eci_catalog_old` DROP PRIMARY KEY;',
    'ALTER TABLE `' . _DB_PREFIX_ . 'eci_catalog_old` ADD PRIMARY KEY (`product_reference`, `fournisseur`) USING BTREE;',
    'ALTER TABLE `' . _DB_PREFIX_ . 'eci_catalog_old` ADD INDEX `category` (`category`(255));',
    'ALTER TABLE `' . _DB_PREFIX_ . 'eci_catalog_old` ADD INDEX `categorygroup` (`category_group`(255));',
    'ALTER TABLE `' . _DB_PREFIX_ . 'eci_catalog_old` ADD INDEX `reference` (`reference`);',
    'ALTER TABLE `' . _DB_PREFIX_ . 'eci_catalog_old` ADD INDEX `product_reference` (`product_reference`);',
    'ALTER TABLE `' . _DB_PREFIX_ . 'eci_catalog_old` ADD INDEX `manufacturer` (`manufacturer`);',
    'ALTER TABLE `' . _DB_PREFIX_ . 'eci_catalog_old` ADD INDEX `fournisseur` (`fournisseur`);',

    'ALTER TABLE `' . _DB_PREFIX_ . 'eci_catalog_attribute` ADD INDEX `reference_attribute` (`reference_attribute`);',
    'ALTER TABLE `' . _DB_PREFIX_ . 'eci_catalog_attribute` ADD INDEX `product_reference` (`product_reference`);',
    'ALTER TABLE `' . _DB_PREFIX_ . 'eci_catalog_attribute` ADD INDEX `fournisseur` (`fournisseur`);',
    'ALTER TABLE `' . _DB_PREFIX_ . 'eci_catalog_attribute` ADD INDEX `reference` (`reference`);',

    'ALTER TABLE `' . _DB_PREFIX_ . 'eci_catalog_attribute_old` ADD INDEX `reference_attribute` (`reference_attribute`);',
    'ALTER TABLE `' . _DB_PREFIX_ . 'eci_catalog_attribute_old` ADD INDEX `product_reference` (`product_reference`);',
    'ALTER TABLE `' . _DB_PREFIX_ . 'eci_catalog_attribute_old` ADD INDEX `fournisseur` (`fournisseur`);',
    'ALTER TABLE `' . _DB_PREFIX_ . 'eci_catalog_attribute_old` ADD INDEX `reference` (`reference`);',

    'ALTER TABLE `' . _DB_PREFIX_ . 'eci_export_com` ADD UNIQUE `id_order_fournisseur` (`id_order`, `fournisseur`);',
    'ALTER TABLE `' . _DB_PREFIX_ . 'eci_export_com` ADD INDEX `id_order` (`id_order`);',
    'ALTER TABLE `' . _DB_PREFIX_ . 'eci_export_com` ADD INDEX `fournisseur` (`fournisseur`);',
    'ALTER TABLE `' . _DB_PREFIX_ . 'eci_export_com` ADD INDEX `final` (`final`);',
    'ALTER TABLE `' . _DB_PREFIX_ . 'eci_export_com` ADD INDEX `extid` (`extid`);',

    'ALTER TABLE `' . _DB_PREFIX_ . 'eci_tracking` DROP INDEX `id_order_fournisseur`;',
    'ALTER TABLE `' . _DB_PREFIX_ . 'eci_tracking` ADD INDEX `id_order` (`id_order`);',
    'ALTER TABLE `' . _DB_PREFIX_ . 'eci_tracking` ADD INDEX `fournisseur` (`fournisseur`);',

    'ALTER TABLE `' . _DB_PREFIX_ . 'eci_price_spe` ADD INDEX `rule_type` (`rule_type`);',
    'ALTER TABLE `' . _DB_PREFIX_ . 'eci_price_spe` ADD INDEX `reference_parent` (`reference_parent`);',
    'ALTER TABLE `' . _DB_PREFIX_ . 'eci_price_spe` ADD INDEX `reference` (`reference`);',
    'ALTER TABLE `' . _DB_PREFIX_ . 'eci_price_spe` ADD INDEX `id_customer` (`id_customer`);',
    'ALTER TABLE `' . _DB_PREFIX_ . 'eci_price_spe` ADD INDEX `quantity` (`quantity`);',
    'ALTER TABLE `' . _DB_PREFIX_ . 'eci_price_spe` ADD INDEX `fournisseur` (`fournisseur`);',

    'ALTER TABLE `' . _DB_PREFIX_ . 'eci_attribute` DROP KEY `value_lang`;',
    'ALTER TABLE `' . _DB_PREFIX_ . 'eci_attribute` DROP KEY `value_lang_fournisseur`;',
    'ALTER TABLE `' . _DB_PREFIX_ . 'eci_attribute` DROP KEY `value_id_lang_fournisseur`;',
    'ALTER TABLE `' . _DB_PREFIX_ . 'eci_attribute` ADD UNIQUE KEY `value_split_context_split_value_id_lang_fournisseur` (`value`, `split_context`, `split_value`, `id_lang`, `fournisseur`);',
    'ALTER TABLE `' . _DB_PREFIX_ . 'eci_attribute` ADD INDEX `value` (`value`);',
    'ALTER TABLE `' . _DB_PREFIX_ . 'eci_attribute` ADD INDEX `split_context` (`split_context`);',
    'ALTER TABLE `' . _DB_PREFIX_ . 'eci_attribute` ADD INDEX `split_value` (`split_value`);',
    'ALTER TABLE `' . _DB_PREFIX_ . 'eci_attribute` ADD INDEX `id_lang` (`id_lang`);',
    'ALTER TABLE `' . _DB_PREFIX_ . 'eci_attribute` ADD INDEX `fournisseur` (`fournisseur`);',

    'ALTER TABLE `' . _DB_PREFIX_ . 'eci_attribute_value` ADD UNIQUE KEY `group_value_lang` (`id_attribute_eco`, `value`(255), `id_lang`);',
    'ALTER TABLE `' . _DB_PREFIX_ . 'eci_attribute_value` ADD INDEX `id_attribute_eco` (`id_attribute_eco`);',
    'ALTER TABLE `' . _DB_PREFIX_ . 'eci_attribute_value` ADD INDEX `value` (`value`(255));',
    'ALTER TABLE `' . _DB_PREFIX_ . 'eci_attribute_value` ADD INDEX `id_lang` (`id_lang`);',
    'ALTER TABLE `' . _DB_PREFIX_ . 'eci_attribute_value` ADD INDEX `fournisseur` (`fournisseur`);',

    'ALTER TABLE `' . _DB_PREFIX_ . 'eci_feature` ADD UNIQUE KEY `value_lang_fournisseur` (`value`(255), `id_lang`, `fournisseur`);',
    'ALTER TABLE `' . _DB_PREFIX_ . 'eci_feature` ADD INDEX `value` (`value`(255));',

    'ALTER TABLE `' . _DB_PREFIX_ . 'eci_feature_shop` DROP PRIMARY KEY;',
    'DELETE fs
    FROM (SELECT *, COUNT(*)
        FROM `' . _DB_PREFIX_ . 'eci_feature_shop`
        GROUP BY id_feature_eco, id_shop
        HAVING COUNT(*) > 1) d
    LEFT JOIN `' . _DB_PREFIX_ . 'eci_feature_shop` fs
    ON fs.id_feature_eco = d.id_feature_eco
    AND fs.id_shop = d.id_shop
    WHERE fs.id_feature = 0;',
    'ALTER TABLE `' . _DB_PREFIX_ . 'eci_feature_shop` ADD PRIMARY KEY (`id_feature_eco`,`id_shop`) USING BTREE;',
    'ALTER TABLE `' . _DB_PREFIX_ . 'eci_feature_shop` ADD INDEX `id_shop` (`id_shop`);',

    'ALTER TABLE `' . _DB_PREFIX_ . 'eci_fournisseur` ADD INDEX `id_manufacturer` (`id_manufacturer`);',

    'ALTER TABLE `' . _DB_PREFIX_ . 'eci_cpanel` DROP KEY `name`;',
    'ALTER TABLE `' . _DB_PREFIX_ . 'eci_cpanel` ADD UNIQUE KEY `name_suffix` (`name`, `suffix`);',

    'ALTER TABLE `' . _DB_PREFIX_ . 'eci_jobs` ADD INDEX `value` (`value`(20));',

    'ALTER TABLE `' . _DB_PREFIX_ . 'eci_jobs_history` ADD INDEX `task` (`task`);',
    'ALTER TABLE `' . _DB_PREFIX_ . 'eci_jobs_history` ADD INDEX `state` (`state`);',
    'ALTER TABLE `' . _DB_PREFIX_ . 'eci_jobs_history` ADD INDEX `fournisseur` (`fournisseur`);',
    'ALTER TABLE `' . _DB_PREFIX_ . 'eci_jobs_history` ADD INDEX `ts` (`ts`);',
    'ALTER TABLE `' . _DB_PREFIX_ . 'eci_jobs_history` ADD INDEX `jid` (`jid`);',

    'ALTER TABLE `' . _DB_PREFIX_ . 'eci_tax` ADD UNIQUE `rate_fournisseur` (`rate`, `fournisseur`);',
    'ALTER TABLE `' . _DB_PREFIX_ . 'eci_tax` ADD INDEX `rate` (`rate`);',
    'ALTER TABLE `' . _DB_PREFIX_ . 'eci_tax` ADD INDEX `fournisseur` (`fournisseur`);',
);
