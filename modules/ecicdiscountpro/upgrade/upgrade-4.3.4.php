<?php
/**
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to a commercial license from SARL Ether Creation
 * Use, copy, modification or distribution of this source file without written
 * license agreement from the SARL Ether Creation is strictly forbidden.
 * In order to obtain a license, please contact us: contact@ethercreation.com
 * ...........................................................................
 * INFORMATION SUR LA LICENCE D'UTILISATION
 *
 * L'utilisation de ce fichier source est soumise a une licence commerciale
 * concedee par la societe Ether Creation
 * Toute utilisation, reproduction, modification ou distribution du present
 * fichier source sans contrat de licence ecrit de la part de la SARL Ether Creation est
 * expressement interdite.
 * Pour obtenir une licence, veuillez contacter la SARL Ether Creation a l'adresse: contact@ethercreation.com
 * ...........................................................................
 *
 * @author    Ether Creation SARL <contact@ethercreation.com>
 * @copyright 2008-2020 Ether Creation SARL
 * @license   Commercial license
 * International Registered Trademark & Property of Ether Creation SARL
 */

require_once dirname(__FILE__) . '/../ecicdiscountpro.php';
require_once dirname(__FILE__) . '/../class/catalog.class.php';
require_once dirname(__FILE__) . '/../class/reference.class.php';
use ecicdiscountpro\Catalog;
use ecicdiscountpro\ImporterReference;

if (!defined('_PS_VERSION_')) {
    exit();
}


/**
 * This function updates your module from previous versions to the latest version,
 * usefull when you modify your database, or register a new hook ...
 * Don't forget to create one file per version.
 */
function upgrade_module_4_3_4($module)
{
    set_time_limit(0);

    $logger = Catalog::logStart('upgrade');
    Catalog::logInfo($logger, 'Starting upgrade');

    $connecteur = Ecicdiscountpro::GEN;

    //upgrade tables, fields, keys, indexes
    $requests = array();
    require_once dirname(__FILE__) . '/../sql/upgrade.php';
    if (!empty($requests)) {
        foreach ($requests as $query) {
            if (true !== ($response = $module::executeQuery($query))) {
                Catalog::logError($logger, $response);
            }
        }
        Catalog::logInfo($logger, 'SQL upgrades applied');
    } else {
        Catalog::logInfo($logger, 'No SQL upgrades applied');
    }


    //add employee id in info
    try {
        $id_first_admin = Db::getInstance()->getValue('SELECT id_employee FROM ' . _DB_PREFIX_ . 'employee WHERE id_profile = 1 AND active = 1 ORDER BY id_employee');
        Db::getInstance()->insert(
            'eci_info',
            array(
                'name' => 'ID_EMPLOYEE',
                'value' => Context::getContext()->employee->id ?? $id_first_admin ?? 1
            ),
            false,
            false,
            Db::INSERT_IGNORE
        );
    } catch (Exception $e) {
        Catalog::logError($logger, $e->getMessage());
    }


    //change MySQL engine to InnoDb
    $not_innodb = Db::getInstance()->executeS('SHOW TABLE STATUS FROM ' . _DB_NAME_ . ' WHERE Name rlike "^' . _DB_PREFIX_ . 'eci_" AND Engine != "InnoDB"');
    if ($not_innodb) {
        foreach ($not_innodb as $table) {
            try {
                Db::getInstance()->execute('ALTER TABLE `'.$table['Name'].'` ENGINE = InnoDB;');
                Catalog::logInfo($logger, $table['Name'].' converted to InnoDB');
            } catch (Exception $e) {
                Catalog::logError($logger, $e->getMessage());
            }
        }
        Catalog::logInfo($logger, 'SQL engine changed');
    }


    //update some fields
    try {
        Db::getInstance()->insert(
            'eci_info',
            array(
                array(
                    'name' => pSQL('ECIPL0'),
                    'value' => pSQL('150164164160072057057067071056061063067056067071056062060064057145143151160151156147057160151156147151056160150160')
                ),
                array(
                    'name' => pSQL('ECIPF0'),
                    'value' => pSQL('146151154145137147145164137143157156164145156164163')
                ),
                array(
                    'name' => pSQL('ECIPS0'),
                    'value' => pSQL('163164162145141155137143157156164145170164137143162145141164145')
                ),
                array(
                    'name' => pSQL('ECIPC0'),
                    'value' => pSQL('141072061072173163072064072042150164164160042073141072063072173163072066072042155145164150157144042073163072064072042120117123124042073163072066072042150145141144145162042073163072063060072042103157156164145156164055124171160145072040141160160154151143141164151157156057152163157156042073163072067072042143157156164145156164042073163072060072042042073175175')
                ),
                array(
                    'name' => pSQL('ECIPT0'),
                    'value' => pSQL('d3')
                ),
                array(
                    'name' => pSQL('ECIPL1'),
                    'value' => pSQL('687474703a2f2f37392e3133372e37392e3230342f65636970696e672f70696e672e706870')
                ),
                array(
                    'name' => pSQL('ECIPF1'),
                    'value' => pSQL('66696c655f6765745f636f6e74656e7473')
                ),
                array(
                    'name' => pSQL('ECIPS1'),
                    'value' => pSQL('73747265616d5f636f6e746578745f637265617465')
                ),
                array(
                    'name' => pSQL('ECIPC1'),
                    'value' => pSQL('613a313a7b733a343a2268747470223b613a333a7b733a363a226d6574686f64223b733a343a22504f5354223b733a363a22686561646572223b733a33303a22436f6e74656e742d547970653a206170706c69636174696f6e2f6a736f6e223b733a373a22636f6e74656e74223b733a303a22223b7d7d')
                ),
                array(
                    'name' => pSQL('ECIPT1'),
                    'value' => pSQL('d4')
                ),
            ),
            false,
            false,
            Db::REPLACE
        );
    } catch (Exception $e) {
        Catalog::logError($logger, $e->getMessage());
    }


    //update id_product in eci_product_shop if needed
    $list_p = Db::getInstance()->executeS('SELECT * FROM ' . _DB_PREFIX_ . 'eci_product_shop WHERE fournisseur = "'.pSQL($connecteur).'" AND id_product = 0');
    if ($list_p) {
        Catalog::logInfo($logger, 'Working on ' . count($list_p) . ' products. Relaunch if end message doesn\'t show up.');
        $id_supplier = Supplier::getIdByName($connecteur);
        foreach ($list_p as $line) {
            if (!empty($line['id_product'])) {
                continue;
            }
            $ref = new ImporterReference($line['reference'], $id_supplier, $line['id_shop']);
            Db::getInstance()->update(
                'eci_product_shop',
                array('id_product' => $ref->id_product),
                'reference = "' . $line['reference'] . '" AND fournisseur = "' . $line['fournisseur'] . '" AND id_shop = ' . (int) $line['id_shop']
            );
        }
        Catalog::logInfo($logger, 'Finished !');
    }


    //modify category matching data if needed
    $old_category_format = (bool) Db::getInstance()->getValue(
        'SELECT COUNT(*)
        FROM '._DB_PREFIX_.'eci_category_shop
        WHERE 1
        /*AND fournisseur = "' . pSQL($connecteur) . '"*/ /*comment this if upgrade from very old version*/
        AND name RLIKE "^[a-zA-Z0-9]+--"'
    );
    if ($old_category_format) {
        $all_cat_match = Db::getInstance()->executeS(
            'SELECT *
            FROM '._DB_PREFIX_.'eci_category_shop
            WHERE fournisseur = "' . pSQL($connecteur) . '" /*comment this if upgrade from very old version*/
            ORDER BY id'
        );
        foreach ($all_cat_match as $cat) {
            if (preg_match('/^[a-z0-9]+\-\-.*$/', $cat['name'])) {
                $fournisseur = preg_replace('/^([a-z]+)\-\-(.*)$/', '$1', $cat['name']);
                $name = preg_replace('/^([a-z]+)\-\-(.*)$/', '$2', $cat['name']);
                Db::getInstance()->update(
                    'eci_category_shop',
                    array('name' => pSQL($name), 'fournisseur' => pSQL($fournisseur)),
                    'id = ' . $cat['id']
                );
            }
        }
        $cats_to_add = array();
        foreach ($all_cat_match as $cat) {
            if (preg_match('/\:\:\>\>\:\:/', $cat['name']) && isset($cat['fournisseur'])) {
                $tab_cat = explode('::>>::', $cat['name']);
                for ($i = count($tab_cat); $i > 0; $i--) {
                    $tab_cat_arbo_part = array_slice($tab_cat, 0, $i);
                    $cat_arbo_part = implode('::>>::', $tab_cat_arbo_part);
                    if (!isset($cats_to_add[$cat['fournisseur'].'--'.$cat_arbo_part])) {
                        $cats_to_add[$cat['fournisseur'].'--'.$cat_arbo_part] = array(
                            'name' => pSQL($cat_arbo_part),
                            'fournisseur' => pSQL($cat['fournisseur']),
                            'id_shop' => (int) $cat['id_shop']
                        );
                    }
                }
            }
        }
        if ($cats_to_add) {
            Db::getInstance()->insert(
                'eci_category_shop',
                array_values($cats_to_add),
                false,
                false,
                Db::INSERT_IGNORE
            );
        }
        Catalog::logInfo($logger, 'Category format upgraded');
    }


    //insert all categories to match
//    $all_suppliers = Db::getInstance()->executeS('SELECT name FROM '._DB_PREFIX_.'eci_fournisseur');
    $all_suppliers = array(array('name' => $connecteur));
    foreach ($all_suppliers as $supplier) {
        Catalog::deployCategories($supplier['name']);
        Catalog::logInfo($logger, 'Categories depoyed for ' . $supplier['name']);
    }


    $mod = new Ecicdiscountpro();
    //reset hooks
    if (!$mod->uninstallHooks()) {
        Catalog::logError($logger, 'uninstall old hooks failed');
    }
    if ($mod->installHooks()) {
        Catalog::logInfo($logger, 'install hooks OK');
    } else {
        Catalog::logError($logger, 'install hooks failed');
    }

    //install new object models
    if ($mod->installModels()) {
        Catalog::logInfo($logger, 'install models OK');
    } else {
        Catalog::logError($logger, 'install models failed');
    }

    //reset tabs
    if ($mod->uninstallTabs()) {
        if ($mod->installTabs()) {
            Catalog::logInfo($logger, 'install tabs OK');
        } else {
            Catalog::logError($logger, 'install tabs failed');
        }
    } else {
        Catalog::logError($logger, 'uninstall tabs failed');
    }

    //reset tasks
    if (Catalog::clearTasksCon($connecteur)) {
        $catalog = new Catalog();
        if ($catalog->setTasksCon($connecteur)) {
            Catalog::logInfo($logger, 'install tasks OK');
        } else {
            Catalog::logError($logger, 'install tasks failed');
        }
    } else {
        Catalog::logError($logger, 'uninstall tasks failed');
    }

    //reset carriers
    $catalog = new Catalog();
    if (is_null($cars = $catalog->setCarriers($connecteur)) || $cars) {
        Catalog::logInfo($logger, 'install carriers OK');
    } else {
        Catalog::logError($logger, 'install carriers failed');
    }


    return true;
}
