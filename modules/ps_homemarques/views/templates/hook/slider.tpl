{*
 * 2007-2020 PrestaShop.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2020 PrestaShop SA
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}

{if $homeslider.slides}
  {literal}
     <script>
     var $jq = jQuery.noConflict();
     $jq(document).ready(function() { 
      $jq('.slide-logo').slick({
      infinite: true,
      slidesToShow: 4,
      slidesToScroll: 1,
      responsive: [
    {
      breakpoint: 768,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2,
        infinite: true,
        autoplay: true,
  autoplaySpeed: 2000,
      }
    },
      ] 
    });
    });
</script>
  {/literal}
  <div class="homeslider-container">
  <div class="container">
  <div class="row">
  <div class="col-xs-12">
  <h2 class="slidem">Les plus grandes marques de barbecues pour vous</h2></div>
  <div class="col-sm-1"></div>
    <div class="slide-logo col-sm-10">
      {foreach from=$homeslider.slides item=slide}
        <div class="col-sm-3 col-xs-6">
            {if $slide.url && $slide.url != 'http://none'}<a href="{$slide.url}"><img src="{$slide.image_url}" /></a>
              {else}
                <img src="{$slide.image_url}" />
            {/if}
        </div>
      {/foreach}
    </div>
    <div class="col-sm-1"></div>
  </div></div>
  </div>



{/if}
