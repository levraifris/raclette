<?php
/**
 * 2011 - 2020 StorePrestaModules SPM LLC.
 *
 * MODULE blockblog
 *
 * @author    SPM <spm.presto@gmail.com>
 * @copyright Copyright (c) permanent, SPM
 * @license   Addons PrestaShop license limitation
 * @version   2.4.3
 * @link      http://addons.prestashop.com/en/2_community-developer?contributor=790166
 *
 * NOTICE OF LICENSE
 *
 * Don't use this module on several shops. The license provided by PrestaShop Addons
 * for all its modules is valid only once for a single shop.
 */

class BlockblogAuthorModuleFrontController extends ModuleFrontController
{
    public $php_self;
    private $_name_module = "blockblog";

    public $display_column_left = false;
    public $display_column_right = false;

    public function __construct()
    {
        parent::__construct();
        if(Configuration::get($this->_name_module.'sidebar_posblog_author_alias')==2)
            $this->display_column_right=true;
        if(Configuration::get($this->_name_module.'sidebar_posblog_author_alias')==1)
            $this->display_column_left =true;

    }

    public function init()
    {

        parent::init();
    }

    public function setMedia()
    {
        parent::setMedia();


        $name_module = $this->_name_module;
        if(Configuration::get($name_module.'blog_post_effect') != "disable_all_effects") {

            $this->context->controller->addJs(__PS_BASE_URI__ . 'modules/' . $name_module . '/views/js/wow.js');
            $this->context->controller->addCSS(__PS_BASE_URI__ . 'modules/' . $name_module . '/views/css/animate.css');
        }

        $this->context->controller->addJs(__PS_BASE_URI__ . 'modules/' . $name_module . '/views/js/owl.carousel.js');
        $this->context->controller->addCSS(__PS_BASE_URI__ . 'modules/' . $name_module . '/views/css/owl.carousel.css');
        $this->context->controller->addCSS(__PS_BASE_URI__ . 'modules/' . $name_module . '/views/css/owl.theme.default.css');
    }



    /**
     * @see FrontController::initContent()
     */
    public function initContent()
    {

        $name_module = "blockblog";

        $this->php_self = 'module-'.$name_module.'-author';
        parent::initContent();

        $author_id = Tools::getValue('author_id');


        $original_author_id = $author_id;
        $author_id_data = explode("-",$author_id);
        $author_id = (int)current($author_id_data);




        include_once(_PS_MODULE_DIR_.$name_module.'/classes/blogspm.class.php');
        $obj_blog = new blogspm();


        include_once(_PS_MODULE_DIR_.$name_module.'/blockblog.php');
        $obj_blockblog = new blockblog();

        $obj_blockblog->setSEOUrls();


        $info_path = $obj_blog->getAvatarPath(array('id_customer'=>$author_id));


        $avatar = $info_path['avatar'];
        $is_show_ava = $info_path['is_show'];

        $_info_author = $obj_blog->isExistsAuthor(array('author_id' => $author_id));


        $ava_on = Configuration::get($name_module.'ava_on');


        if(
            $_info_author === NULL ||
            ($is_show_ava == 0  AND $is_show_ava !== NULL) || //customer can be deleted from admin panel, but his posts will be displayed
            !$ava_on
        ){
            $data_url = $obj_blog->getSEOURLs();
            $authors_url = $data_url['authors_url'];

            Tools::redirect($authors_url);
        }






        $obj_blockblog->setControllersSettings();



        $p = (int)Tools::getValue('p');
        $step = (int) Configuration::get($name_module.'perpage_posts');

        $start = (int)(($p - 1)*$step);
        if($start<0)
            $start = 0;

        $_data = $obj_blog->getPostsForAuthor(array('start'=>$start,'step'=>$step,'author_id'=>$author_id));




        $paging = $obj_blog->PageNav($start,$_data['count_all'],$step,array('is_author'=>1,'author_id'=>$author_id));

        // strip authors for content
        foreach($_data['posts'] as $_k => $_item){
            $_data['posts'][$_k]['content'] = strip_tags($_item['content']);

        }


        $data_customer_info = $obj_blog->getCustomerInfo(array('id_customer'=>$author_id));
        $author_name = $data_customer_info['customer_name'];



        include_once(_PS_MODULE_DIR_.$name_module.'/classes/userprofileblockblog.class.php');
        $obj = new userprofileblockblog();

        // fixed bug, when customer was been deleted from admin panel
        if(Tools::strlen($author_name)==0){

            $author_name  = isset($_data['posts'][0]['author'])?$_data['posts'][0]['author']:$obj->getAdminName();
        }
        // fixed bug, when customer was been deleted from admin panel


        $info_customer = $obj->getCustomerInfo(array('id_customer'=>$author_id));
        $author_info = $info_customer['info'];



        $_data_translate = $obj_blockblog->translateItems();

        $title = $_data_translate['meta_title_author'];
        $seo_description = $_data_translate['meta_description_author'];
        $seo_keywords = $_data_translate['meta_keywords_author'];


        if(version_compare(_PS_VERSION_, '1.7', '>')) {
            $this->context->smarty->tpl_vars['page']->value['meta']['title'] = $title.' '.$author_name;
            $this->context->smarty->tpl_vars['page']->value['meta']['description'] = $seo_description.' '.$author_name;
            $this->context->smarty->tpl_vars['page']->value['meta']['keywords'] = $seo_keywords.' '.$author_name;
        }

        $this->context->smarty->assign('meta_title', $title.' '.$author_name);
        $this->context->smarty->assign('meta_description', $seo_description.' '.$author_name);
        $this->context->smarty->assign('meta_keywords', $seo_keywords.' '.$author_name);

        

        $this->context->smarty->assign(
            array(
                'posts' => $_data['posts'],
                'count_all' => $_data['count_all'],
                'paging' => $paging,
                $name_module.'author_id'=>$original_author_id,
                $name_module.'avatar'=>$avatar,
                $name_module.'author_name'=>$author_name,
                $name_module.'author_info'=>$author_info,
            )
        );

        if(version_compare(_PS_VERSION_, '1.7', '>')) {
            $this->setTemplate('module:'.$name_module.'/views/templates/front/author17.tpl');
        }else {
            $this->setTemplate('author.tpl');
        }

    }
}