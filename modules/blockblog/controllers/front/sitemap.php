<?php
/**
 * 2011 - 2020 StorePrestaModules SPM LLC.
 *
 * MODULE blockblog
 *
 * @author    SPM <spm.presto@gmail.com>
 * @copyright Copyright (c) permanent, SPM
 * @license   Addons PrestaShop license limitation
 * @version   2.4.3
 * @link      http://addons.prestashop.com/en/2_community-developer?contributor=790166
 *
 * NOTICE OF LICENSE
 *
 * Don't use this module on several shops. The license provided by PrestaShop Addons
 * for all its modules is valid only once for a single shop.
 */

class BlockblogSitemapModuleFrontController extends ModuleFrontController
{

    public function initContent()
    {

        $name = "blockblog";



        $token = Tools::getValue('token');

        if(md5($name._PS_BASE_URL_SSL_) == $token){

            if(Configuration::get($name.'sitemapon') == 1) {

                include_once(_PS_MODULE_DIR_ . $name . '/classes/blogspm.class.php');
                $obj_blog = new blogspm();

                $obj_blog->generateSitemap();
                echo 'Success: Sitemap succesfully generated!';
            } else {
                echo 'Sitemap disabled!';
            }

        } else {
            echo 'Error: Access denien! Invalid token!';
        }

        exit;
    }
}
?>