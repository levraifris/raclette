<?php
/**
 * 2011 - 2020 StorePrestaModules SPM LLC.
 *
 * MODULE blockblog
 *
 * @author    SPM <spm.presto@gmail.com>
 * @copyright Copyright (c) permanent, SPM
 * @license   Addons PrestaShop license limitation
 * @version   2.4.3
 * @link      http://addons.prestashop.com/en/2_community-developer?contributor=790166
 *
 * NOTICE OF LICENSE
 *
 * Don't use this module on several shops. The license provided by PrestaShop Addons
 * for all its modules is valid only once for a single shop.
 */

class BlockblogCategoryModuleFrontController extends ModuleFrontController
{
    public $php_self;
	public function init()
	{

		parent::init();
	}
	
	public function setMedia()
	{
        parent::setMedia();
    }

    public function initContent()
    {
        $name_module = "blockblog";

        $this->php_self = 'module-'.$name_module.'-category';

        parent::initContent();


        include_once(_PS_MODULE_DIR_.$name_module.'/classes/blogspm.class.php');
        $obj_blog = new blogspm();

        $category_id = Tools::getValue('category_id');

        if(is_numeric($category_id)) {
            $seo_url_cat = $obj_blog->getSEOURLForCategory(array('id'=>$category_id));
        } else {
            $seo_url_cat = $category_id;
        }

        $data_url = $obj_blog->getSEOURLs();
        $category_url = $data_url['category_url'];


        Tools::redirect($category_url.$seo_url_cat);
    }


}