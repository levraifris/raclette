<?php
/**
 * 2011 - 2020 StorePrestaModules SPM LLC.
 *
 * MODULE blockblog
 *
 * @author    SPM <spm.presto@gmail.com>
 * @copyright Copyright (c) permanent, SPM
 * @license   Addons PrestaShop license limitation
 * @version   2.4.3
 * @link      http://addons.prestashop.com/en/2_community-developer?contributor=790166
 *
 * NOTICE OF LICENSE
 *
 * Don't use this module on several shops. The license provided by PrestaShop Addons
 * for all its modules is valid only once for a single shop.
 */

class BlockblogRssModuleFrontController extends ModuleFrontController
{

    public function initContent()
    {
        $_name = "blockblog";

        include_once(_PS_MODULE_DIR_.$_name.'/classes/blogspm.class.php');
        $obj_blog = new blogspm();



        $_http_host = Tools::getShopDomainSsl(true, true).__PS_BASE_URI__;

        $cookie = Context::getContext()->cookie;

        $id_lang = (int)$cookie->id_lang;
        $data_language = $obj_blog->getfacebooklib($id_lang);
        $rss_title =  Configuration::get($_name.'rssname_'.$id_lang);;
        $rss_description =  Configuration::get($_name.'rssdesc_'.$id_lang);;

        if (Configuration::get('PS_SSL_ENABLED') == 1)
            $url = "https://";
        else
            $url = "http://";

        $site = $_SERVER['HTTP_HOST'];

        // Lets build the page

        $latestBuild = date("r");

        // Lets define the the type of doc we're creating.
        header('Content-Type:text/xml; charset=utf-8');
        $createXML = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";


        if(Configuration::get($_name.'rsson') == 1){

            $rss_title = str_replace('&','&amp;', $rss_title);
            $rss_description = str_replace('&','&amp;', $rss_description);

            $createXML .= "<rss xmlns:slash=\"http://purl.org/rss/1.0/modules/slash/\" version=\"2.0\">\n";
            $createXML .= "<channel>
                            <title>".$rss_title."</title>
                            <description>".$rss_description."</description>
                            <link>$url$site</link>
                            <lastBuildDate>$latestBuild</lastBuildDate>
                        ";



            $data_rss_items = $obj_blog->getItemsForRSS();

            //echo "<pre>"; var_dump($data_rss_items); exit;

            foreach($data_rss_items['items'] as $_item)
            {
                $page = str_replace('&','&amp;', $_item['page']);
                $description = $_item['seo_description'];
                $title = $_item['title'];
                $count_comments = $_item['count_comments'];
                $pubdate = $_item['pubdate'];
                if(Tools::strlen($_item['img'])>0){
                    if (defined('_PS_HOST_MODE_'))
                        $img = $_http_host."modules/".$_name."/upload/".$_item['img'];
                    else
                        $img = $_http_host."upload/".$_name."/".$_item['img'];

                }else{
                    $img = '';
                }

                $author = $_item['author'];

                $createXML .= $obj_blog->createRSSFile($title,$description,$page,$pubdate,$img,$author,$count_comments);
            }
            $createXML .= "</channel>\n </rss>";
        // Finish it up
        }

        echo $createXML;
        exit;
    }

}