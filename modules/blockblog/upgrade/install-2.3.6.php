<?php
/**
 * 2011 - 2020 StorePrestaModules SPM LLC.
 *
 * MODULE blockblog
 *
 * @author    SPM <spm.presto@gmail.com>
 * @copyright Copyright (c) permanent, SPM
 * @license   Addons PrestaShop license limitation
 * @version   2.4.3
 * @link      http://addons.prestashop.com/en/2_community-developer?contributor=790166
 *
 * NOTICE OF LICENSE
 *
 * Don't use this module on several shops. The license provided by PrestaShop Addons
 * for all its modules is valid only once for a single shop.
 */

function upgrade_module_2_3_6($module)
{

    $name_module = "blockblog";

    foreach($module->getAvailablePages() as $available_pages_array) {
        $alias_position = $available_pages_array['alias'];
        $is_layout = $available_pages_array['is_layout'];
        Configuration::updateValue($name_module . 'sidebar_pos' . $alias_position, 3);

        if($is_layout) {
            Configuration::updateValue($name_module . 'blog_layout_type' . $alias_position, 1);
        }
    }

    //Configuration::updateValue($name_module.'lists_img_height', 200);



    /// clear smarty cache ///

    Tools::clearSmartyCache();

    /// clear smarty cache ///

    return true;

}