<?php
/**
 * 2011 - 2020 StorePrestaModules SPM LLC.
 *
 * MODULE blockblog
 *
 * @author    SPM <spm.presto@gmail.com>
 * @copyright Copyright (c) permanent, SPM
 * @license   Addons PrestaShop license limitation
 * @version   2.4.3
 * @link      http://addons.prestashop.com/en/2_community-developer?contributor=790166
 *
 * NOTICE OF LICENSE
 *
 * Don't use this module on several shops. The license provided by PrestaShop Addons
 * for all its modules is valid only once for a single shop.
 */

function upgrade_module_1_9_5($module)
{

    if(version_compare(_PS_VERSION_, '1.6', '>')) {
        $module->registerHook('blogCategoriesSPM');
        $module->registerHook('blogPostsSPM');
        $module->registerHook('blogCommentsSPM');
    }


    return true;
}
?>