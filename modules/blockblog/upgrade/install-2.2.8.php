<?php
/**
 * 2011 - 2020 StorePrestaModules SPM LLC.
 *
 * MODULE blockblog
 *
 * @author    SPM <spm.presto@gmail.com>
 * @copyright Copyright (c) permanent, SPM
 * @license   Addons PrestaShop license limitation
 * @version   2.4.3
 * @link      http://addons.prestashop.com/en/2_community-developer?contributor=790166
 *
 * NOTICE OF LICENSE
 *
 * Don't use this module on several shops. The license provided by PrestaShop Addons
 * for all its modules is valid only once for a single shop.
 */

function upgrade_module_2_2_8($module)
{

    $name_module = "blockblog";
    #### posts api ###
    $languages = Language::getLanguages(false);
    foreach ($languages as $language){
        $i = $language['id_lang'];
        $iso = Tools::strtoupper(Language::getIsoById($i));
        Configuration::updateValue($name_module.'twdesc_'.$i, 'New blog post has been added in our shop '.$iso);
        Configuration::updateValue($name_module.'idesc_'.$i, 'New blog post has been added in our shop '.$iso);
        Configuration::updateValue($name_module.'pdesc_'.$i, 'New blog post has been added in our shop '.$iso);
        Configuration::updateValue($name_module.'vkdesc_'.$i, 'New blog post has been added in our shop '.$iso);
    }

    #### posts api ###


    /// clear smarty cache ///

    Tools::clearSmartyCache();

    /// clear smarty cache ///

    return true;

}