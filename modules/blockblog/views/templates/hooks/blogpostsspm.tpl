{*
/**
 * StorePrestaModules SPM LLC.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 *
 /*
 * 
 * @author    StorePrestaModules SPM
 * @category content_management
 * @package blockblog
 * @copyright Copyright StorePrestaModules SPM
 * @license   StorePrestaModules SPM
 */
*}

<div id="blockblogposts_block_left_spm" class="block {if $blockblogis17 == 1}block-categories{/if} {if $blockblogis16 == 1}blockmanufacturer16{else}blockmanufacturer{/if} blockblog-block" >
		<h4 class="title_block {if $blockblogis17 == 1}text-uppercase{/if}">
			
				{l s='Blog Posts recents' mod='blockblog'}

			{if $blockblogrsson == 1}
				<a  class="margin-left-left-10" href="{$blockblogrss_url|escape:'htmlall':'UTF-8'}" title="{l s='RSS Feed' mod='blockblog'}" target="_blank">
					<img src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/blockblog/views/img/feed.png" alt="{l s='RSS Feed' mod='blockblog'}" />
				</a>
			{/if}
			
		</h4>
		<div class="block_content">

            {if count($blockblogposts) > 0}
            <div class="items-articles-block">

                {foreach from=$blockblogposts item=items name=myLoop1}
                {foreach from=$items.data item=blog name=myLoop}
                <div class="current-item-block">

                    {if $blockblogblock_display_img == 1}
                        {if strlen($blog.img)>0}
                            <div class="block-side float-left margin-right-10">
                                  <img src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}{$blockblogpic|escape:'htmlall':'UTF-8'}{$blog.img|escape:'htmlall':'UTF-8'}"
                                       title="{$blog.title|escape:'htmlall':'UTF-8'}" alt="{$blog.title|escape:'htmlall':'UTF-8'}"  />
                            </div>
                        {/if}
                    {/if}

                    <div class="block-content">
                        <a class="item-article" title="{$blog.title|escape:'htmlall':'UTF-8'}"
                           href="{if $blockblogurlrewrite_on == 1}{$blockblogpost_url|escape:'htmlall':'UTF-8'}{$blog.seo_url|escape:'htmlall':'UTF-8'}{else}{$blockblogpost_url|escape:'htmlall':'UTF-8'}{$blog.id|escape:'htmlall':'UTF-8'}{/if}"
                                >{$blog.title|escape:'htmlall':'UTF-8'}</a>

                        <div class="clr"></div>
                        {if $blockblogblock_display_date == 1}
                        <span class="float-left block-blog-date"><i class="fa fa-clock-o fa-lg"></i>&nbsp;{$blog.time_add|date_format:$blockblogblog_date|escape:'htmlall':'UTF-8'}</span>
                        {/if}
                        <span class="float-right comment block-blog-like">

                            {if $blockblograting_bl == 1}
                                {if $blog.avg_rating != 0}
                                <span class="rating-input float-left margin-right-10">

                                        {for $foo=0 to 4}
                                            {if $foo < $blog.avg_rating}
                                                <i class="fa fa-star" data-value="{$foo|escape:'htmlall':'UTF-8'}"></i>

                                            {else}
                                                <i class="fa fa-star-o" data-value="{$foo|escape:'htmlall':'UTF-8'}"></i>
                                            {/if}

                                        {/for}


                                </span>
                                {/if}
                            {/if}

                            {if $blockblogpostbl_views}
                                <i class="fa fa-eye fa-lg"></i>&nbsp;<span class="blockblog-views">({$blog.count_views|escape:'htmlall':'UTF-8'})</span>&nbsp;&nbsp;
                            {/if}


                            {if $blog.is_liked_post}
                                <i class="fa fa-thumbs-up fa-lg"></i>&nbsp;(<span class="the-number">{$blog.count_like|escape:'htmlall':'UTF-8'}</span>)
                            {else}
                                <span class="post-like-{$blog.id|escape:'htmlall':'UTF-8'}">
                                <a onclick="blockblog_like_post({$blog.id|escape:'htmlall':'UTF-8'},1)"
                                   href="javascript:void(0)"><i class="fa fa-thumbs-o-up fa-lg"></i>&nbsp;(<span class="the-number">{$blog.count_like|escape:'htmlall':'UTF-8'}</span>)</a>
                                </span>

                                {* loyalty program *}
                                            {if $blockblogloyality_onl == 1}
                                {if $blockblogloyality_like_blog_post_statusl == "loyality_like_blog_post"}
                                    {if $blockblogloyality_like_blog_post_timesul < $blockblogloyality_like_blog_post_timesl}
                                        <span class="blockblog-loyalty-question"
                                              onmouseover="blockblog_loyalty_question({$blog.id|escape:'htmlall':'UTF-8'},'loyality_like_blog_post{$blockblogalias|escape:'htmlall':'UTF-8'}',1)"
                                              onmouseout="blockblog_loyalty_question({$blog.id|escape:'htmlall':'UTF-8'},'loyality_like_blog_post{$blockblogalias|escape:'htmlall':'UTF-8'}',0)">
                                                    <img src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/blockblog/views/img/question_white.png"
                                                         alt ="{$blockblogloyality_like_blog_post_pointsl|escape:'htmlall':'UTF-8'}"
                                                            />
                                                            <span class="blockblog-loyalty-tooltip" id="blockblog-loyalty-tooltip{$blog.id|escape:'htmlall':'UTF-8'}loyality_like_blog_post{$blockblogalias|escape:'htmlall':'UTF-8'}" style="display: none;">
                                                                {$blockblogloyality_like_blog_post_descriptionl|replace:'[points]':$blockblogloyality_like_blog_post_pointsl nofilter}
                                                            </span>
                                            </span>
                                    {/if}
                                {/if}
                            {/if}
                                {* loyalty program *}
                            {/if}

                            {if $blog.is_comments || $blog.count_comments > 0}
                            &nbsp;
                            <a title="{$blog.title|escape:'htmlall':'UTF-8'}"
                               href="{if $blockblogurlrewrite_on == 1}{$blockblogpost_url|escape:'htmlall':'UTF-8'}{$blog.seo_url|escape:'htmlall':'UTF-8'}#blogcomments{else}{$blockblogpost_url|escape:'htmlall':'UTF-8'}{$blog.id|escape:'htmlall':'UTF-8'}#blogcomments{/if}">
                                <i class="fa fa-comments-o fa-lg"></i>&nbsp;(<span class="the-number">{$blog.count_comments|escape:'htmlall':'UTF-8'}</span>)</a>
                            {/if}
                        </span>

                        <div class="clr"></div>
                    </div>
                </div>

                {/foreach}
                {/foreach}
                <p class="block-view-all">
                        <a href="{$blockblogposts_url|escape:'htmlall':'UTF-8'}" title="{l s='View all posts' mod='blockblog'}" class="{if $blockblogis17 == 1}btn btn-default button button-small-blockblog{/if} button"
                                ><b>{l s='View all posts' mod='blockblog'}</b></a>
                </p>

            </div>
            {else}
                <div class="block-no-items">
                    {l s='There are not Posts yet.' mod='blockblog'}
                </div>
            {/if}

		</div>
	</div>



