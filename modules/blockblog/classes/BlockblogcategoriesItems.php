<?php
/**
 * 2011 - 2020 StorePrestaModules SPM LLC.
 *
 * MODULE blockblog
 *
 * @author    SPM <spm.presto@gmail.com>
 * @copyright Copyright (c) permanent, SPM
 * @license   Addons PrestaShop license limitation
 * @version   2.4.3
 * @link      http://addons.prestashop.com/en/2_community-developer?contributor=790166
 *
 * NOTICE OF LICENSE
 *
 * Don't use this module on several shops. The license provided by PrestaShop Addons
 * for all its modules is valid only once for a single shop.
 */

class BlockblogcategoriesItems extends ObjectModel
{

    /** @var string Name */
    public $id;
    public $title;
    public $count_posts;
    public $position;
    public $shop_name;
    public $language;
    public $time_add;
    public $status;

    /**
     * @see ObjectModel::$definition
     */
    public static $definition = array(
        'table' => 'blog_category',
        'primary' => 'id',
        'fields' => array(
            'id' => array('type' => self::TYPE_INT,'validate' => 'isUnsignedInt','required' => true,),

            'title' => array('type' => self::TYPE_STRING, 'required' => true, 'validate' => 'isGenericName', 'size' => 128),
            'count_posts' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt'),
            'position' =>        array('type' => self::TYPE_INT, 'validate' => 'isInt'),
            'shop_name' => array('type' => self::TYPE_STRING, 'required' => true, 'validate' => 'isGenericName', 'size' => 128),
            'language' => array('type' => self::TYPE_STRING, 'required' => true, 'validate' => 'isGenericName', 'size' => 128),
            'time_add' => array('type' => self::TYPE_STRING, 'required' => true, 'validate' => 'isGenericName', 'size' => 128),
            'status' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt'),

        ),

    );




    public function deleteSelection($selection)
    {
        foreach ($selection as $value) {
            $obj = new BlockblogcategoriesItems($value);
            if (!$obj->delete()) {
                return false;
            }
        }
        return true;
    }

    public function delete()
    {
        $return = false;

        if (!$this->hasMultishopEntries() || Shop::getContext() == Shop::CONTEXT_ALL) {

            require_once(_PS_MODULE_DIR_ . 'blockblog/classes/blogspm.class.php');
            $blog_obj = new blogspm();
            $blog_obj->deleteCategory(array('id'=>(int)$this->id));


            $return = true;
        }
        return $return;
    }


    public function updatePosition($way, $position)
    {
        if (!$res = Db::getInstance()->executeS('
			SELECT ag.`position`, ag.`id` as id_attribute_group
			FROM `'._DB_PREFIX_.'blog_category` ag
			WHERE ag.`id` = '.(int)Tools::getValue('id', 1).'
			ORDER BY ag.`position` ASC'
        )) {
            return false;
        }

        foreach ($res as $group_attribute) {
            if ((int)$group_attribute['id_attribute_group'] == (int)$this->id) {
                $moved_group_attribute = $group_attribute;
            }
        }

        if (!isset($moved_group_attribute) || !isset($position)) {
            return false;
        }

        // < and > statements rather than BETWEEN operator
        // since BETWEEN is treated differently according to databases
        return (Db::getInstance()->execute('
			UPDATE `'._DB_PREFIX_.'blog_category`
			SET `position`= `position` '.($way ? '- 1' : '+ 1').'
			WHERE `position`
			'.($way
                    ? '> '.(int)$moved_group_attribute['position'].' AND `position` <= '.(int)$position
                    : '< '.(int)$moved_group_attribute['position'].' AND `position` >= '.(int)$position)
            ) && Db::getInstance()->execute('
			UPDATE `'._DB_PREFIX_.'blog_category`
			SET `position` = '.(int)$position.'
			WHERE `id`='.(int)$moved_group_attribute['id_attribute_group'])
        );
    }


    
}
?>
