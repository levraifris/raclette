<?php
/**
 * 2011 - 2020 StorePrestaModules SPM LLC.
 *
 * MODULE blockblog
 *
 * @author    SPM <spm.presto@gmail.com>
 * @copyright Copyright (c) permanent, SPM
 * @license   Addons PrestaShop license limitation
 * @version   2.4.3
 * @link      http://addons.prestashop.com/en/2_community-developer?contributor=790166
 *
 * NOTICE OF LICENSE
 *
 * Don't use this module on several shops. The license provided by PrestaShop Addons
 * for all its modules is valid only once for a single shop.
 */

class pagenavhelp extends Module {


    private $_name = 'blockblog';

    public function pagenav($start,$count,$step, $_data =null )
    {


        $prefix = isset($_data['prefix'])?$_data['prefix']:'';
        $action = isset($_data['action'])?$_data['action']:'';
        $ajax_url = isset($_data['ajax_url'])?$_data['ajax_url']:'';



        ob_start();
        include(dirname(__FILE__).'/../views/templates/hooks/'.$this->_name.'/pagenav_loyalty_account.phtml');
        $res = ob_get_clean();

        return $res;
    }
}