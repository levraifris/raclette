<?php
/**
 * 2011 - 2020 StorePrestaModules SPM LLC.
 *
 * MODULE blockblog
 *
 * @author    SPM <spm.presto@gmail.com>
 * @copyright Copyright (c) permanent, SPM
 * @license   Addons PrestaShop license limitation
 * @version   2.4.3
 * @link      http://addons.prestashop.com/en/2_community-developer?contributor=790166
 *
 * NOTICE OF LICENSE
 *
 * Don't use this module on several shops. The license provided by PrestaShop Addons
 * for all its modules is valid only once for a single shop.
 */

class StatisticsblogspmItems extends ObjectModel
{


    /** @var string Name */
    public $id_customer;

    /*private $prefix_db;
    public function __construct(){
        $this->prefix_db = $this->getPrefixDB(); // error is on this line
    }*/

    /**
     * @see ObjectModel::$definition
     */
    public static $definition = array(
        'table' => 'spm_loyalty',
        'primary' => 'id_customer',
        'fields' => array(
            'id_customer' => array('type' => self::TYPE_INT,'validate' => 'isUnsignedInt','required' => true,),
        ),

    );


   /* public function getPrefixDB()
    {
        include_once(_PS_MODULE_DIR_ .  'blockblog/blockblog.php');
        $obj_blockblog = new blockblog();
        $_prefix = $obj_blockblog->getLoyalityDbPrefix();
        return $_prefix;

    }*/



}
?>
