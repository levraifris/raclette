<?php
/**
 * 2011 - 2020 StorePrestaModules SPM LLC.
 *
 * MODULE blockblog
 *
 * @author    SPM <spm.presto@gmail.com>
 * @copyright Copyright (c) permanent, SPM
 * @license   Addons PrestaShop license limitation
 * @version   2.4.3
 * @link      http://addons.prestashop.com/en/2_community-developer?contributor=790166
 *
 * NOTICE OF LICENSE
 *
 * Don't use this module on several shops. The license provided by PrestaShop Addons
 * for all its modules is valid only once for a single shop.
 */

class BlogAvatarCustomer extends ObjectModel
{


    /** @var string Name */
    public $id_customer;
    public $avatar_thumb;
    public $info;
    public $is_show;
    public $id_shop;
    public $status;


    /**
     * @see ObjectModel::$definition
     */
    public static $definition = array(
        'table' => 'blog_avatar2customer',
        'primary' => 'id_customer',
        'fields' => array(
            'id_customer' => array('type' => self::TYPE_INT,'validate' => 'isUnsignedInt','required' => true,),

            'avatar_thumb' => array('type' => self::TYPE_STRING, 'required' => false, 'validate' => 'isGenericName', 'size' => 128),
            'info' => array('type' => self::TYPE_STRING, 'required' => false, 'validate' => 'isGenericName', 'size' => 128),

            'is_show' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt'),
            'id_shop' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt'),
            'status' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt'),

        ),

    );

}
?>
